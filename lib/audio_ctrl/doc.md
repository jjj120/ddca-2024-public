
# Audio Controller
The `audio_ctrl` module implements a simple synthetic sound generator that interfaces with the board's audio DAC (digital to analog converter) WM8731.
This chip has two separate (serial) interfaces, one for configuration purposes (control interface) and another one to receive the actual audio samples (digital audio interface).
The control interface is only required during start-up to configure the sampling rate and set up the digital audio interface.
The Figure below shows the general structure of the audio controller.

![Audio Control Structure](.mdata/audio_ctrl_structure.svg)

The audio controller must be clocked by a 12 MHz clock (which will internally be forwarded to the `xck` output). 
The `synth_ctrl` signals can be written from any clock domain since the core uses synchronizers to bring the required signals into its (12~MHz) clock domain (see Secion `Audio Controller Interface Description`).

Note that the audio controller is provided as a precompiled module with two synthesizers (hence the suffix `_2s`). 

## Dependencies
* Since the audio controller is provided as a precompiled module, there are no external dependencies. 


## Required Files
 * `precompiled_audio_ctrl_2s_top.vho` (use **only for simulation**, not for synthesis!)
 * `audio_ctrl_pkg.vhd`
 * `precompiled_audio_ctrl_2s.vhd`
 * `precompiled_audio_ctrl_2s_top.qxp` (use **only for synthesis** (i.e., in quartus), not for simulation!)

## Audio Controller Component Declaration

```vhdl
component precompiled_audio_ctrl_2s is
	port (
		clk   : in std_logic;
		res_n : in std_logic;

		wm8731_xck     : out std_logic;

		--interface to wm8731: i2c configuration interface
		wm8731_sdat : inout std_logic;
		wm8731_sclk : inout std_logic;

		--samples 
		wm8731_dacdat  : out std_logic;
		wm8731_daclrck : out std_logic; -- DAC Left/Right Clock
		wm8731_bclk    : out std_logic;

		--internal interface to the stynthesizers
		synth_ctrl : in synth_ctrl_vec_t(0 to 1)
	);
end component;
```

## Audio Controller Interface Description
To interface with the audio controller, the `synth_ctrl` input is used, which allows to control the individual
synthesizers. This signal is a 2-element vector of the record type `synth_ctrl_t` shown below.

```vhdl
type synth_ctrl_t is record
	play : std_logic;
	high_time : std_logic_vector (7 downto 0);
	low_time : std_logic_vector (7 downto 0);
end record;
```

Every synthesizer produces a PWM signal which can be configured via the `high_time` and `low_time` entries of this record.
These values have to be interpreted with respect to the sampling frequency of the DAC (in this case 8 KHz).
If both values are 1, the maximum frequency output signal is generated.
This means that in this case the actual samples that are sent to the DAC switch between the maximum and minimum value at every sampling period.

The active-high `play` signal controls the sound play-back, i.e., as long as this signal is high, the respective sound is played.
When the `play` signal switches from low to high, the synthesizer reads the current values of `high_time` and `low_time` and uses those values to generate the PWM signal until `play` returns to zero again (this means that changing those values while `play` is high has no effect).
Hence, to change the PWM signal, the `play` signal must be low for at least one clock cycle (of the 12MHz input clock of the audio controller).

Since the audio controller can be controlled from any clock domain, care must be taken, to correctly handle the clock domain crossing.
For that purpose, the core uses 3-stage synchronizers on the `play` signals. 
The `high_time` and `low_time` are not synchronized!
This means that whenever these values are changed, it must be made sure that they are stable long enough such that the audio controller can sample them.
Hence, one has to take the synchronization delay into account.
