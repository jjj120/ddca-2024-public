library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.rv_core_pkg.all;
    use work.rv_sys_pkg.all;

package rv_util_pkg is
    -- OPCODE_BRANCH
    constant FUNCT3_BEQ  : funct3_t := "000";
    constant FUNCT3_BNE  : funct3_t := "001";
    constant FUNCT3_BLT  : funct3_t := "100";
    constant FUNCT3_BGE  : funct3_t := "101";
    constant FUNCT3_BLTU : funct3_t := "110";
    constant FUNCT3_BGEU : funct3_t := "111";

    -- OPCODE_LOAD
    constant FUNCT3_LB  : funct3_t := "000";
    constant FUNCT3_LH  : funct3_t := "001";
    constant FUNCT3_LW  : funct3_t := "010";
    constant FUNCT3_LBU : funct3_t := "100";
    constant FUNCT3_LHU : funct3_t := "101";

    -- OPCODE_STORE
    constant FUNCT3_SB : funct3_t := "000";
    constant FUNCT3_SH : funct3_t := "001";
    constant FUNCT3_SW : funct3_t := "010";

    -- OPCODE_OP_IMM
    constant FUNCT3_ADDI      : funct3_t := "000";
    constant FUNCT3_SLTI      : funct3_t := "010";
    constant FUNCT3_SLTIU     : funct3_t := "011";
    constant FUNCT3_XORI      : funct3_t := "100";
    constant FUNCT3_ORI       : funct3_t := "110";
    constant FUNCT3_ANDI      : funct3_t := "111";
    constant FUNCT3_SLLI      : funct3_t := "001";
    constant FUNCT3_SRLI_SRAI : funct3_t := "101";

    -- OPCODE_OP
    constant FUNCT3_ADD_SUB : funct3_t := "000";
    constant FUNCT3_SLL     : funct3_t := "001";
    constant FUNCT3_SLT     : funct3_t := "010";
    constant FUNCT3_SLTU    : funct3_t := "011";
    constant FUNCT3_XOR     : funct3_t := "100";
    constant FUNCT3_SRL_SRA : funct3_t := "101";
    constant FUNCT3_OR      : funct3_t := "110";
    constant FUNCT3_AND     : funct3_t := "111";

    constant FUNCT7_ADD : funct7_t := "0000000";
    constant FUNCT7_SUB : funct7_t := "0100000";
    constant FUNCT7_SRL : funct7_t := "0000000";
    constant FUNCT7_SRA : funct7_t := "0100000";

    pure function byte_to_word_addr(data : data_t) return mem_address_t;
    pure function get_immediate(instr : instr_t) return data_t;
    pure function get_inst_type(instr : instr_t) return instr_format_t;
    pure function get_memu_access_type(instr_in : instr_t) return memu_access_type_t;
    pure function flip_endianness(instr_in : instr_t) return instr_t;

    -- pragma translate_off
    pure function to_inst_string(instr : instr_t) return string;
    pure function gen_dbg_string(pc : std_logic_vector; instr : instr_t; num_tabs : natural := 1) return string;
    -- pragma translate_on
end package;

package body rv_util_pkg is
    pure function byte_to_word_addr(data : data_t) return mem_address_t is
    begin
        return data(RV_SYS_ADDR_WIDTH + 1 downto 2);
    end function;

    pure function get_immediate(instr : instr_t) return data_t is
        variable imm : data_t := (others => '0');
    begin
        case get_inst_type(instr) is
            when FORMAT_I =>
                imm(31 downto 11) := (others => instr(31));
                imm(10 downto 5) := instr(30 downto 25);
                imm(4 downto 1) := instr(24 downto 21);
                imm(0) := instr(20);
            when FORMAT_S =>
                imm(31 downto 11) := (others => instr(31));
                imm(10 downto 5) := instr(30 downto 25);
                imm(4 downto 1) := instr(11 downto 8);
                imm(0) := instr(7);
            when FORMAT_B =>
                imm(31 downto 12) := (others => instr(31));
                imm(11) := instr(7);
                imm(10 downto 5) := instr(30 downto 25);
                imm(4 downto 1) := instr(11 downto 8);
                imm(0) := '0';
            when FORMAT_U =>
                imm(31 downto 12) := instr(31 downto 12);
                imm(11 downto 0) := (others => '0');
            when FORMAT_J =>
                imm(31 downto 20) := (others => instr(31));
                imm(19 downto 12) := instr(19 downto 12);
                imm(11) := instr(20);
                imm(10 downto 5) := instr(30 downto 25);
                imm(4 downto 1) := instr(24 downto 21);
                imm(0) := '0';
            when others =>
                imm(data_t'range) := (others => '0');
        end case;

        return imm;
    end function;

    pure function get_inst_type(instr : instr_t) return instr_format_t is
    begin
        case get_opcode(instr) is
            when OPCODE_LUI | OPCODE_AUIPC => return FORMAT_U;
            when OPCODE_JAL => return FORMAT_J;
            when OPCODE_JALR | OPCODE_LOAD | OPCODE_OP_IMM | OPCODE_FENCE => return FORMAT_I;
            when OPCODE_BRANCH => return FORMAT_B;
            when OPCODE_STORE => return FORMAT_S;
            when OPCODE_OP => return FORMAT_R;
            when others => return FORMAT_INVALID;
        end case;
    end function;

    pure function get_memu_access_type(instr_in : instr_t) return memu_access_type_t is
    begin
        case get_funct3(instr_in) is
            when FUNCT3_LB => return MEM_B; -- FUNCT3_LB = FUNCT3_SB
            when FUNCT3_LH => return MEM_H; -- FUNCT3_LH = FUNCT3_SH
            when FUNCT3_LW => return MEM_W; -- FUNCT3_LW = FUNCT3_SW
            when FUNCT3_LBU => return MEM_BU;
            when FUNCT3_LHU => return MEM_HU;
            when others => assert 1 = 0 report "Invalid funct3 in get_memu_access_type" severity failure;
        end case;
        return MEM_W;
    end function;

    pure function flip_endianness(instr_in : instr_t) return instr_t is
    begin
        return instr_t'(instr_in(7 downto 0) & instr_in(15 downto 8) & instr_in(23 downto 16) & instr_in(31 downto 24));
    end function;

    -- pragma translate_off
    pure function to_dec_string(num : signed) return string is
    begin
        return to_string(to_integer(num));
    end function;

    pure function to_dec_string(num : unsigned) return string is
    begin
        return to_string(to_integer(num));
    end function;

    pure function to_dec_string(num : std_logic_vector) return string is
    begin
        return to_dec_string(unsigned(num));
    end function;

    pure function to_inst_string(instr : instr_t) return string is
        constant PRE_STRING : string := "Exec:  ";
    begin
        if instr = 32x"13" then
            return PRE_STRING & "NOP";
        end if;

        case get_opcode(instr) is
            when OPCODE_LOAD =>
                case get_funct3(instr) is
                    when FUNCT3_LB => return PRE_STRING & "LB" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(signed(get_immediate(instr)));
                    when FUNCT3_LH => return PRE_STRING & "LH" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(signed(get_immediate(instr)));
                    when FUNCT3_LW => return PRE_STRING & "LW" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(signed(get_immediate(instr)));
                    when FUNCT3_LBU => return PRE_STRING & "LBU" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(signed(get_immediate(instr)));
                    when FUNCT3_LHU => return PRE_STRING & "LHU" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(signed(get_immediate(instr)));
                    when others =>
                        report "Got unknown funct3 for store." severity failure;
                        return PRE_STRING & "";
                end case;

            when OPCODE_STORE =>
                case get_funct3(instr) is
                    when FUNCT3_SB => return PRE_STRING & "SB" & ht & " x" & to_dec_string(get_rs1(instr)) & ", x" & to_dec_string(get_rs2(instr)) & ", " & to_dec_string(signed(get_immediate(instr)));
                    when FUNCT3_SH => return PRE_STRING & "SH" & ht & " x" & to_dec_string(get_rs1(instr)) & ", x" & to_dec_string(get_rs2(instr)) & ", " & to_dec_string(signed(get_immediate(instr)));
                    when FUNCT3_SW => return PRE_STRING & "SW" & ht & " x" & to_dec_string(get_rs1(instr)) & ", x" & to_dec_string(get_rs2(instr)) & ", " & to_dec_string(signed(get_immediate(instr)));
                    when others =>
                        report "Got unknown funct3 for store." severity failure;
                        return PRE_STRING & "";
                end case;

            when OPCODE_BRANCH =>
                case get_funct3(instr) is
                    when FUNCT3_BEQ => return PRE_STRING & "BEQ" & ht & " x" & to_dec_string(get_rs1(instr)) & ", x" & to_dec_string(get_rs2(instr)) & ", " & to_dec_string(signed(get_immediate(instr)));
                    when FUNCT3_BNE => return PRE_STRING & "BNE" & ht & " x" & to_dec_string(get_rs1(instr)) & ", x" & to_dec_string(get_rs2(instr)) & ", " & to_dec_string(signed(get_immediate(instr)));
                    when FUNCT3_BLT => return PRE_STRING & "BLT" & ht & " x" & to_dec_string(get_rs1(instr)) & ", x" & to_dec_string(get_rs2(instr)) & ", " & to_dec_string(signed(get_immediate(instr)));
                    when FUNCT3_BGE => return PRE_STRING & "BGE" & ht & " x" & to_dec_string(get_rs1(instr)) & ", x" & to_dec_string(get_rs2(instr)) & ", " & to_dec_string(signed(get_immediate(instr)));
                    when FUNCT3_BLTU => return PRE_STRING & "BLTU" & ht & " x" & to_dec_string(get_rs1(instr)) & ", x" & to_dec_string(get_rs2(instr)) & ", " & to_dec_string(signed(get_immediate(instr)));
                    when FUNCT3_BGEU => return PRE_STRING & "BGEU" & ht & " x" & to_dec_string(get_rs1(instr)) & ", x" & to_dec_string(get_rs2(instr)) & ", " & to_dec_string(signed(get_immediate(instr)));
                    when others =>
                        report "Got unknown funct3 for branch." severity failure;
                        return PRE_STRING & "";
                end case;

            when OPCODE_JALR =>
                return PRE_STRING & "JALR" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(signed(get_immediate(instr)));

            when OPCODE_JAL =>
                return PRE_STRING & "JAL" & ht & " x" & to_dec_string(get_rd(instr)) & ", " & to_dec_string(signed(get_immediate(instr)));

            when OPCODE_OP_IMM =>
                case get_funct3(instr) is
                    when FUNCT3_ADDI => return PRE_STRING & "ADDI" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(signed(get_immediate(instr)));
                    when FUNCT3_SLTI => return PRE_STRING & "SLTI" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(signed(get_immediate(instr)));
                    when FUNCT3_SLTIU => return PRE_STRING & "SLTIU" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(signed(get_immediate(instr)));
                    when FUNCT3_XORI => return PRE_STRING & "XORI" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(signed(get_immediate(instr)));
                    when FUNCT3_ORI => return PRE_STRING & "ORI" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(signed(get_immediate(instr)));
                    when FUNCT3_ANDI => return PRE_STRING & "ANDI" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(signed(get_immediate(instr)));
                    when FUNCT3_SLLI => return PRE_STRING & "SLLI" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(get_immediate(instr)(4 downto 0));
                    when FUNCT3_SRLI_SRAI =>
                        if get_immediate(instr)(10) then
                            return PRE_STRING & "SRAI" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(get_immediate(instr)(4 downto 0));
                        else
                            return PRE_STRING & "SRLI" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(get_immediate(instr)(4 downto 0));
                        end if;
                    when others =>
                        report "Got unknown funct3 for op imm." severity failure;
                        return PRE_STRING & "";
                end case;

            when OPCODE_OP =>
                if get_funct7(instr) = FUNCT7_EXT_M then
                    case get_funct3(instr) is
                        when FUNCT3_MUL => return PRE_STRING & "MUL" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(get_rs2(instr));
                        when FUNCT3_MULH => return PRE_STRING & "MULH" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(get_rs2(instr));
                        when FUNCT3_MULHSU => return PRE_STRING & "MULHSU" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(get_rs2(instr));
                        when FUNCT3_MULHU => return PRE_STRING & "MULHU" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(get_rs2(instr));
                        when FUNCT3_DIV => return PRE_STRING & "DIV" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(get_rs2(instr));
                        when FUNCT3_DIVU => return PRE_STRING & "DIVU" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(get_rs2(instr));
                        when FUNCT3_REM => return PRE_STRING & "REM" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(get_rs2(instr));
                        when FUNCT3_REMU => return PRE_STRING & "REMU" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(get_rs2(instr));
                        when others => report "Got unknown funct3 for op-mul." severity failure;
                    end case;
                else
                    case get_funct3(instr) is
                        when FUNCT3_ADD_SUB =>
                            if get_funct7(instr) = FUNCT7_ADD then
                                return PRE_STRING & "ADD" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(get_rs2(instr));
                            elsif get_funct7(instr) = FUNCT7_SUB then
                                return PRE_STRING & "SUB" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(get_rs2(instr));
                            else
                                report "Got unknown funct7 for op." severity failure;
                                return PRE_STRING & "";
                            end if;

                        when FUNCT3_SLL => return PRE_STRING & "SLL" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(get_rs2(instr));
                        when FUNCT3_SLT => return PRE_STRING & "SLT" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(get_rs2(instr));
                        when FUNCT3_SLTU => return PRE_STRING & "SLTU" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(get_rs2(instr));
                        when FUNCT3_XOR => return PRE_STRING & "XOR" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(get_rs2(instr));
                        when FUNCT3_AND => return PRE_STRING & "AND" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(get_rs2(instr));
                        when FUNCT3_OR => return PRE_STRING & "OR" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(get_rs2(instr));
                        when FUNCT3_SRL_SRA =>
                            if get_funct7(instr) = FUNCT7_SRA then
                                return PRE_STRING & "SRA" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(get_rs2(instr));
                            elsif get_funct7(instr) = FUNCT7_SRL then
                                return PRE_STRING & "SRL" & ht & " x" & to_dec_string(get_rd(instr)) & ", x" & to_dec_string(get_rs1(instr)) & ", " & to_dec_string(get_rs2(instr));
                            else
                                report "Got unknown funct7 for op." severity failure;
                                return PRE_STRING & "";
                            end if;
                        when others =>
                            report "Got unknown funct3 for op." severity failure;
                            return PRE_STRING & "";
                    end case;
                end if;

            when OPCODE_AUIPC =>
                return PRE_STRING & "AUIPC" & ht & " x" & to_dec_string(get_rd(instr)) & ", 0x" & to_hstring(get_immediate(instr));
            when OPCODE_LUI =>
                return PRE_STRING & "LUI" & ht & " x" & to_dec_string(get_rd(instr)) & ", " & to_dec_string(signed(get_immediate(instr)));
            when OPCODE_FENCE =>
                return PRE_STRING & "FENCE";
            when others =>
                return PRE_STRING & "";
        end case;
    end function;

    pure function gen_dbg_string(pc : std_logic_vector; instr : instr_t; num_tabs : natural := 1) return string is
        variable tabs_to_return : string(1 to num_tabs) := (others => ht);
    begin
        return tabs_to_return & "at 0x" & to_hstring(std_logic_vector(pc)) & ", funct3: " & to_string(get_funct3(instr)) & ", funct7: " & to_string(get_funct7(instr)) & ", rd: 0x" & to_string(get_rd(instr)) & ", rs1: 0x" & to_string(get_rs1(instr)) & ", rs2: 0x" & to_string(get_rs2(instr)) & ", " & to_string(instr);
    end function;
    -- pragma translate_on
end package body;
