
# Decimal Printer
The `decimal_printer` can be used to print unsigned 16-bit numbers in their 5-digit decimal representation by interfacing with the `vga_gfx_ctrl`.
It always prints leading zeros (e.g. the number 1 will be presented as 00001).
The core requires a bitmap index which refers to a bitmap containing the numbers 0-9 (in that order) as 8 pixel wide characters.
The figure below shows how such a bitmap can look like.

![Example Font Bitmap](.mdata/example_bitmap.svg)

It is fine if the bitmap contains further characters, but the first 10 must be the numbers 0-9 (as shown in the figure).

The number is printed at the current location of the Graphics Pointer of the `vga_gfx_ctrl`.
Afterwards the x coordinate of Graphics Pointer is incremented by 40 (5 chars, each 8 pixels wide).


## Dependencies
* Graphics command package (`gfx_cmd_pkg`) 
* Mathematical support package (`math_pkg`) 


## Required Files
 * `decimal_printer.vhd`
 * `decimal_printer_pkg.vhd`
 * `decimal_printer_top.vho` (use **only for simulation**, not for synthesis!)
 * `decimal_printer_top.qarlog`

## Decimal Printer Component Declaration

```vhdl
component decimal_printer is
	port (
		clk : in std_logic;
		res_n : in std_logic;
		gfx_cmd : out std_logic_vector(GFX_CMD_WIDTH-1 downto 0);
		gfx_cmd_wr : out std_logic;
		gfx_cmd_full : in std_logic;
		start : in std_logic;
		busy : out std_logic;
		number : in std_logic_vector(15 downto 0);
		bmpidx : in std_logic_vector(WIDTH_BMPIDX-1 downto 0)
	);
end component;
```

## Decimal Printer Interface Description
The below figure shows an example timing diagram for the `decimal_printer`.
After the `start` signal is asserted, the core starts with the conversion process.
When it is done converting the provided `number` to its decimal representation, it outputs exactly 5 `BB_CHAR` commands.
As soon as the `busy` signal goes low a new drawing operation can be started.

The `decimal_printer` is designed to communicate (directly) with the `vga_gfx_ctrl`. 
Hence, it obeys its interface protocol with respect to the `gfx_*` signals.

![Example Timing Diagram](.mdata/decimal_printer_timing.svg)
