-- Copyright (C) 2023  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 22.1std.2 Build 922 07/20/2023 SC Standard Edition"

-- DATE "04/10/2024 14:02:44"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim (VHDL) only
-- 

LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_ASDO_DATA1~	=>  Location: PIN_F4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_FLASH_nCE_nCSO~	=>  Location: PIN_E2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DCLK~	=>  Location: PIN_P3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DATA0~	=>  Location: PIN_N7,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~padout\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~padout\ : std_logic;
SIGNAL \~ALTERA_DATA0~~padout\ : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_DATA0~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	decimal_printer_top IS
    PORT (
	clk : IN std_logic;
	res_n : IN std_logic;
	gfx_cmd : OUT std_logic_vector(15 DOWNTO 0);
	gfx_cmd_wr : OUT std_logic;
	gfx_cmd_full : IN std_logic;
	start : IN std_logic;
	busy : OUT std_logic;
	number : IN std_logic_vector(15 DOWNTO 0);
	bmpidx : IN std_logic_vector(2 DOWNTO 0)
	);
END decimal_printer_top;

-- Design Ports Information
-- gfx_cmd[0]	=>  Location: PIN_F11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- gfx_cmd[1]	=>  Location: PIN_H13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- gfx_cmd[2]	=>  Location: PIN_G13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- gfx_cmd[3]	=>  Location: PIN_C8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- gfx_cmd[4]	=>  Location: PIN_A3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- gfx_cmd[5]	=>  Location: PIN_AD22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- gfx_cmd[6]	=>  Location: PIN_AE3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- gfx_cmd[7]	=>  Location: PIN_E22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- gfx_cmd[8]	=>  Location: PIN_C6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- gfx_cmd[9]	=>  Location: PIN_D6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- gfx_cmd[10]	=>  Location: PIN_C7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- gfx_cmd[11]	=>  Location: PIN_E10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- gfx_cmd[12]	=>  Location: PIN_H10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- gfx_cmd[13]	=>  Location: PIN_AE1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- gfx_cmd[14]	=>  Location: PIN_B3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- gfx_cmd[15]	=>  Location: PIN_H8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- gfx_cmd_wr	=>  Location: PIN_D9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- busy	=>  Location: PIN_AF8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- bmpidx[0]	=>  Location: PIN_A10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- gfx_cmd_full	=>  Location: PIN_A8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- bmpidx[1]	=>  Location: PIN_D10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- bmpidx[2]	=>  Location: PIN_B10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clk	=>  Location: PIN_J1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- res_n	=>  Location: PIN_Y2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- start	=>  Location: PIN_C11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- number[4]	=>  Location: PIN_A7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- number[3]	=>  Location: PIN_D11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- number[7]	=>  Location: PIN_E12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- number[2]	=>  Location: PIN_A6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- number[6]	=>  Location: PIN_G11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- number[5]	=>  Location: PIN_F12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- number[15]	=>  Location: PIN_C10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- number[14]	=>  Location: PIN_J10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- number[12]	=>  Location: PIN_G10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- number[11]	=>  Location: PIN_F10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- number[13]	=>  Location: PIN_E11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- number[10]	=>  Location: PIN_B6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- number[9]	=>  Location: PIN_B7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- number[8]	=>  Location: PIN_G12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- number[1]	=>  Location: PIN_C9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- number[0]	=>  Location: PIN_H12,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF decimal_printer_top IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_res_n : std_logic;
SIGNAL ww_gfx_cmd : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_gfx_cmd_wr : std_logic;
SIGNAL ww_gfx_cmd_full : std_logic;
SIGNAL ww_start : std_logic;
SIGNAL ww_busy : std_logic;
SIGNAL ww_number : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_bmpidx : std_logic_vector(2 DOWNTO 0);
SIGNAL \res_n~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \clk~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \gfx_cmd[0]~output_o\ : std_logic;
SIGNAL \gfx_cmd[1]~output_o\ : std_logic;
SIGNAL \gfx_cmd[2]~output_o\ : std_logic;
SIGNAL \gfx_cmd[3]~output_o\ : std_logic;
SIGNAL \gfx_cmd[4]~output_o\ : std_logic;
SIGNAL \gfx_cmd[5]~output_o\ : std_logic;
SIGNAL \gfx_cmd[6]~output_o\ : std_logic;
SIGNAL \gfx_cmd[7]~output_o\ : std_logic;
SIGNAL \gfx_cmd[8]~output_o\ : std_logic;
SIGNAL \gfx_cmd[9]~output_o\ : std_logic;
SIGNAL \gfx_cmd[10]~output_o\ : std_logic;
SIGNAL \gfx_cmd[11]~output_o\ : std_logic;
SIGNAL \gfx_cmd[12]~output_o\ : std_logic;
SIGNAL \gfx_cmd[13]~output_o\ : std_logic;
SIGNAL \gfx_cmd[14]~output_o\ : std_logic;
SIGNAL \gfx_cmd[15]~output_o\ : std_logic;
SIGNAL \gfx_cmd_wr~output_o\ : std_logic;
SIGNAL \busy~output_o\ : std_logic;
SIGNAL \clk~input_o\ : std_logic;
SIGNAL \clk~inputclkctrl_outclk\ : std_logic;
SIGNAL \gfx_cmd_full~input_o\ : std_logic;
SIGNAL \decimal_printer_inst|Add3~17\ : std_logic;
SIGNAL \decimal_printer_inst|Add3~18_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add1~0_combout\ : std_logic;
SIGNAL \start~input_o\ : std_logic;
SIGNAL \decimal_printer_inst|Selector0~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector0~1_combout\ : std_logic;
SIGNAL \res_n~input_o\ : std_logic;
SIGNAL \res_n~inputclkctrl_outclk\ : std_logic;
SIGNAL \decimal_printer_inst|state.fsm_state.IDLE~q\ : std_logic;
SIGNAL \decimal_printer_inst|Add1~1\ : std_logic;
SIGNAL \decimal_printer_inst|Add1~3\ : std_logic;
SIGNAL \decimal_printer_inst|Add1~5\ : std_logic;
SIGNAL \decimal_printer_inst|Add1~7\ : std_logic;
SIGNAL \decimal_printer_inst|Add1~8_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~5\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~7\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~9\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~11\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~12_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[3][0]~4_combout\ : std_logic;
SIGNAL \decimal_printer_inst|LessThan2~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|LessThan2~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state_nxt~7_combout\ : std_logic;
SIGNAL \decimal_printer_inst|LessThan3~2_combout\ : std_logic;
SIGNAL \decimal_printer_inst|LessThan3~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|LessThan3~3_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector19~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.number[1]~2_combout\ : std_logic;
SIGNAL \number[1]~input_o\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~1\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~2_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state_nxt~12_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state_nxt~13_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector18~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.number[2]~1_combout\ : std_logic;
SIGNAL \number[2]~input_o\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~3\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~5\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~7\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~9\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~11\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~13\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~14_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector12~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add3~9\ : std_logic;
SIGNAL \decimal_printer_inst|Add3~10_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector12~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.number[8]~feeder_combout\ : std_logic;
SIGNAL \number[8]~input_o\ : std_logic;
SIGNAL \decimal_printer_inst|state.number[15]~6_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~15\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~16_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add1~9\ : std_logic;
SIGNAL \decimal_printer_inst|Add1~10_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~13\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~14_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector11~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add3~11\ : std_logic;
SIGNAL \decimal_printer_inst|Add3~12_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector11~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.number[9]~feeder_combout\ : std_logic;
SIGNAL \number[9]~input_o\ : std_logic;
SIGNAL \decimal_printer_inst|LessThan3~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.number[15]~3_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector1~2_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector1~3_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[4][0]~5_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add3~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~1\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~2_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state_nxt~10_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~4_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state_nxt~9_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state_nxt~11_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.number[3]~0_combout\ : std_logic;
SIGNAL \number[3]~input_o\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~3\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~4_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector16~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add3~1\ : std_logic;
SIGNAL \decimal_printer_inst|Add3~2_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~6_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector16~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.number[4]~feeder_combout\ : std_logic;
SIGNAL \number[4]~input_o\ : std_logic;
SIGNAL \decimal_printer_inst|Add3~3\ : std_logic;
SIGNAL \decimal_printer_inst|Add3~4_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add1~2_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~8_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~6_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector15~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector15~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.number[5]~feeder_combout\ : std_logic;
SIGNAL \number[5]~input_o\ : std_logic;
SIGNAL \decimal_printer_inst|Add3~5\ : std_logic;
SIGNAL \decimal_printer_inst|Add3~6_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~8_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~10_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector14~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add1~4_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector14~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.number[6]~feeder_combout\ : std_logic;
SIGNAL \number[6]~input_o\ : std_logic;
SIGNAL \decimal_printer_inst|Add3~7\ : std_logic;
SIGNAL \decimal_printer_inst|Add3~8_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~12_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add1~6_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~10_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector13~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector13~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.number[7]~feeder_combout\ : std_logic;
SIGNAL \number[7]~input_o\ : std_logic;
SIGNAL \decimal_printer_inst|LessThan1~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|LessThan1~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.number[15]~5_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~17\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~19\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~21\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~22_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~15\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~17\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~19\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~20_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector8~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add1~11\ : std_logic;
SIGNAL \decimal_printer_inst|Add1~13\ : std_logic;
SIGNAL \decimal_printer_inst|Add1~15\ : std_logic;
SIGNAL \decimal_printer_inst|Add1~16_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector8~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.number[12]~feeder_combout\ : std_logic;
SIGNAL \number[12]~input_o\ : std_logic;
SIGNAL \decimal_printer_inst|Add3~19\ : std_logic;
SIGNAL \decimal_printer_inst|Add3~20_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add1~17\ : std_logic;
SIGNAL \decimal_printer_inst|Add1~18_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~21\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~22_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector7~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~23\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~24_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector7~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.number[13]~feeder_combout\ : std_logic;
SIGNAL \number[13]~input_o\ : std_logic;
SIGNAL \decimal_printer_inst|Add3~21\ : std_logic;
SIGNAL \decimal_printer_inst|Add3~22_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~25\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~26_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~23\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~24_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector6~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add1~19\ : std_logic;
SIGNAL \decimal_printer_inst|Add1~20_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector6~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.number[14]~feeder_combout\ : std_logic;
SIGNAL \number[14]~input_o\ : std_logic;
SIGNAL \number[15]~input_o\ : std_logic;
SIGNAL \decimal_printer_inst|state.number[15]~7_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~27\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~28_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~25\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~26_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.number[15]~8_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add3~23\ : std_logic;
SIGNAL \decimal_printer_inst|Add3~24_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add1~21\ : std_logic;
SIGNAL \decimal_printer_inst|Add1~22_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.number[15]~9_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.number[15]~10_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.number[15]~11_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state_nxt~5_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state_nxt~6_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state_nxt~8_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add3~13\ : std_logic;
SIGNAL \decimal_printer_inst|Add3~14_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~18_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~16_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector10~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add1~12_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector10~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.number[10]~feeder_combout\ : std_logic;
SIGNAL \number[10]~input_o\ : std_logic;
SIGNAL \decimal_printer_inst|Add3~15\ : std_logic;
SIGNAL \decimal_printer_inst|Add3~16_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add5~18_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add1~14_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector9~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add7~20_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector9~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.number[11]~feeder_combout\ : std_logic;
SIGNAL \number[11]~input_o\ : std_logic;
SIGNAL \decimal_printer_inst|LessThan0~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|LessThan0~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.number[15]~4_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector2~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|gfx_cmd[2]~3_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector4~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector3~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector3~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.fsm_state.BB_CHAR_ARG~q\ : std_logic;
SIGNAL \decimal_printer_inst|Selector4~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\ : std_logic;
SIGNAL \decimal_printer_inst|Selector42~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector41~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector40~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.digit_cnt[2]~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector40~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector42~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector2~2_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector2~3_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.fsm_state.BB_CHAR~q\ : std_logic;
SIGNAL \bmpidx[0]~input_o\ : std_logic;
SIGNAL \decimal_printer_inst|gfx_cmd[0]~0_combout\ : std_logic;
SIGNAL \bmpidx[1]~input_o\ : std_logic;
SIGNAL \decimal_printer_inst|gfx_cmd[1]~1_combout\ : std_logic;
SIGNAL \bmpidx[2]~input_o\ : std_logic;
SIGNAL \decimal_printer_inst|gfx_cmd[2]~2_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector45~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|WideOr2~0_combout\ : std_logic;
SIGNAL \number[0]~input_o\ : std_logic;
SIGNAL \decimal_printer_inst|state.number[0]~12_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector39~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector39~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[0][0]~q\ : std_logic;
SIGNAL \decimal_printer_inst|Selector35~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[1][3]~8_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[1][0]~q\ : std_logic;
SIGNAL \decimal_printer_inst|Selector31~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[2][0]~7_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[2][0]~q\ : std_logic;
SIGNAL \decimal_printer_inst|Selector27~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[3][0]~6_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[3][0]~10_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[3][0]~q\ : std_logic;
SIGNAL \decimal_printer_inst|Selector23~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[4][0]~9_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[4][0]~q\ : std_logic;
SIGNAL \decimal_printer_inst|gfx_cmd[9]~4_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector38~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector38~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[0][1]~q\ : std_logic;
SIGNAL \decimal_printer_inst|Selector34~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector34~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[1][1]~q\ : std_logic;
SIGNAL \decimal_printer_inst|Selector30~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector30~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[2][1]~q\ : std_logic;
SIGNAL \decimal_printer_inst|Selector26~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector26~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[3][1]~q\ : std_logic;
SIGNAL \decimal_printer_inst|Selector22~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector22~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[4][1]~q\ : std_logic;
SIGNAL \decimal_printer_inst|gfx_cmd[10]~5_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector29~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector33~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector37~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector37~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[0][2]~q\ : std_logic;
SIGNAL \decimal_printer_inst|Selector33~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[1][2]~q\ : std_logic;
SIGNAL \decimal_printer_inst|Selector29~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[2][2]~q\ : std_logic;
SIGNAL \decimal_printer_inst|Selector25~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector25~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[3][2]~q\ : std_logic;
SIGNAL \decimal_printer_inst|Selector21~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector21~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[4][2]~q\ : std_logic;
SIGNAL \decimal_printer_inst|gfx_cmd[11]~6_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Add4~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector36~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector36~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[0][3]~q\ : std_logic;
SIGNAL \decimal_printer_inst|Add6~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector32~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[1][3]~q\ : std_logic;
SIGNAL \decimal_printer_inst|Selector28~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[2][3]~q\ : std_logic;
SIGNAL \decimal_printer_inst|Add2~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector24~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[3][3]~q\ : std_logic;
SIGNAL \decimal_printer_inst|Add0~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector20~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.bcd_data[4][3]~q\ : std_logic;
SIGNAL \decimal_printer_inst|Selector45~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|Selector44~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.busy~0_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.busy~1_combout\ : std_logic;
SIGNAL \decimal_printer_inst|state.busy~q\ : std_logic;
SIGNAL \decimal_printer_inst|state.digit_cnt\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \decimal_printer_inst|state.number\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \decimal_printer_inst|ALT_INV_state.fsm_state.IDLE~q\ : std_logic;
SIGNAL \decimal_printer_inst|ALT_INV_state.fsm_state.DIGIT_DONE~q\ : std_logic;
SIGNAL \decimal_printer_inst|ALT_INV_state.fsm_state.CALC_DIGITS~q\ : std_logic;
SIGNAL \decimal_printer_inst|ALT_INV_gfx_cmd[2]~3_combout\ : std_logic;

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

ww_clk <= clk;
ww_res_n <= res_n;
gfx_cmd <= ww_gfx_cmd;
gfx_cmd_wr <= ww_gfx_cmd_wr;
ww_gfx_cmd_full <= gfx_cmd_full;
ww_start <= start;
busy <= ww_busy;
ww_number <= number;
ww_bmpidx <= bmpidx;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\res_n~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \res_n~input_o\);

\clk~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \clk~input_o\);
\decimal_printer_inst|ALT_INV_state.fsm_state.IDLE~q\ <= NOT \decimal_printer_inst|state.fsm_state.IDLE~q\;
\decimal_printer_inst|ALT_INV_state.fsm_state.DIGIT_DONE~q\ <= NOT \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\;
\decimal_printer_inst|ALT_INV_state.fsm_state.CALC_DIGITS~q\ <= NOT \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\;
\decimal_printer_inst|ALT_INV_gfx_cmd[2]~3_combout\ <= NOT \decimal_printer_inst|gfx_cmd[2]~3_combout\;
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: IOOBUF_X31_Y73_N9
\gfx_cmd[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decimal_printer_inst|gfx_cmd[0]~0_combout\,
	devoe => ww_devoe,
	o => \gfx_cmd[0]~output_o\);

-- Location: IOOBUF_X38_Y73_N23
\gfx_cmd[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decimal_printer_inst|gfx_cmd[1]~1_combout\,
	devoe => ww_devoe,
	o => \gfx_cmd[1]~output_o\);

-- Location: IOOBUF_X38_Y73_N16
\gfx_cmd[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decimal_printer_inst|gfx_cmd[2]~2_combout\,
	devoe => ww_devoe,
	o => \gfx_cmd[2]~output_o\);

-- Location: IOOBUF_X16_Y73_N9
\gfx_cmd[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decimal_printer_inst|Selector45~0_combout\,
	devoe => ww_devoe,
	o => \gfx_cmd[3]~output_o\);

-- Location: IOOBUF_X5_Y73_N9
\gfx_cmd[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decimal_printer_inst|ALT_INV_gfx_cmd[2]~3_combout\,
	devoe => ww_devoe,
	o => \gfx_cmd[4]~output_o\);

-- Location: IOOBUF_X111_Y0_N9
\gfx_cmd[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \gfx_cmd[5]~output_o\);

-- Location: IOOBUF_X0_Y7_N9
\gfx_cmd[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \gfx_cmd[6]~output_o\);

-- Location: IOOBUF_X111_Y73_N9
\gfx_cmd[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \gfx_cmd[7]~output_o\);

-- Location: IOOBUF_X5_Y73_N23
\gfx_cmd[8]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \gfx_cmd[8]~output_o\);

-- Location: IOOBUF_X13_Y73_N16
\gfx_cmd[9]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decimal_printer_inst|gfx_cmd[9]~4_combout\,
	devoe => ww_devoe,
	o => \gfx_cmd[9]~output_o\);

-- Location: IOOBUF_X16_Y73_N23
\gfx_cmd[10]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decimal_printer_inst|gfx_cmd[10]~5_combout\,
	devoe => ww_devoe,
	o => \gfx_cmd[10]~output_o\);

-- Location: IOOBUF_X18_Y73_N16
\gfx_cmd[11]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decimal_printer_inst|gfx_cmd[11]~6_combout\,
	devoe => ww_devoe,
	o => \gfx_cmd[11]~output_o\);

-- Location: IOOBUF_X20_Y73_N16
\gfx_cmd[12]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decimal_printer_inst|Selector45~1_combout\,
	devoe => ww_devoe,
	o => \gfx_cmd[12]~output_o\);

-- Location: IOOBUF_X0_Y16_N16
\gfx_cmd[13]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \gfx_cmd[13]~output_o\);

-- Location: IOOBUF_X5_Y73_N2
\gfx_cmd[14]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decimal_printer_inst|ALT_INV_gfx_cmd[2]~3_combout\,
	devoe => ww_devoe,
	o => \gfx_cmd[14]~output_o\);

-- Location: IOOBUF_X11_Y73_N23
\gfx_cmd[15]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decimal_printer_inst|ALT_INV_gfx_cmd[2]~3_combout\,
	devoe => ww_devoe,
	o => \gfx_cmd[15]~output_o\);

-- Location: IOOBUF_X23_Y73_N23
\gfx_cmd_wr~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decimal_printer_inst|Selector44~0_combout\,
	devoe => ww_devoe,
	o => \gfx_cmd_wr~output_o\);

-- Location: IOOBUF_X23_Y0_N16
\busy~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decimal_printer_inst|state.busy~q\,
	devoe => ww_devoe,
	o => \busy~output_o\);

-- Location: IOIBUF_X0_Y36_N8
\clk~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk,
	o => \clk~input_o\);

-- Location: CLKCTRL_G2
\clk~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clk~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clk~inputclkctrl_outclk\);

-- Location: IOIBUF_X18_Y73_N22
\gfx_cmd_full~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_gfx_cmd_full,
	o => \gfx_cmd_full~input_o\);

-- Location: LCCOMB_X27_Y69_N22
\decimal_printer_inst|Add3~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add3~16_combout\ = (\decimal_printer_inst|state.number\(11) & ((GND) # (!\decimal_printer_inst|Add3~15\))) # (!\decimal_printer_inst|state.number\(11) & (\decimal_printer_inst|Add3~15\ $ (GND)))
-- \decimal_printer_inst|Add3~17\ = CARRY((\decimal_printer_inst|state.number\(11)) # (!\decimal_printer_inst|Add3~15\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(11),
	datad => VCC,
	cin => \decimal_printer_inst|Add3~15\,
	combout => \decimal_printer_inst|Add3~16_combout\,
	cout => \decimal_printer_inst|Add3~17\);

-- Location: LCCOMB_X27_Y69_N24
\decimal_printer_inst|Add3~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add3~18_combout\ = (\decimal_printer_inst|state.number\(12) & (\decimal_printer_inst|Add3~17\ & VCC)) # (!\decimal_printer_inst|state.number\(12) & (!\decimal_printer_inst|Add3~17\))
-- \decimal_printer_inst|Add3~19\ = CARRY((!\decimal_printer_inst|state.number\(12) & !\decimal_printer_inst|Add3~17\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.number\(12),
	datad => VCC,
	cin => \decimal_printer_inst|Add3~17\,
	combout => \decimal_printer_inst|Add3~18_combout\,
	cout => \decimal_printer_inst|Add3~19\);

-- Location: LCCOMB_X27_Y70_N4
\decimal_printer_inst|Add1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add1~0_combout\ = \decimal_printer_inst|state.number\(4) $ (VCC)
-- \decimal_printer_inst|Add1~1\ = CARRY(\decimal_printer_inst|state.number\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.number\(4),
	datad => VCC,
	combout => \decimal_printer_inst|Add1~0_combout\,
	cout => \decimal_printer_inst|Add1~1\);

-- Location: IOIBUF_X23_Y73_N1
\start~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_start,
	o => \start~input_o\);

-- Location: LCCOMB_X26_Y70_N26
\decimal_printer_inst|Selector0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector0~0_combout\ = (\decimal_printer_inst|state.fsm_state.IDLE~q\) # ((\decimal_printer_inst|Selector2~1_combout\) # ((\start~input_o\ & !\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \start~input_o\,
	datab => \decimal_printer_inst|state.fsm_state.IDLE~q\,
	datac => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	datad => \decimal_printer_inst|Selector2~1_combout\,
	combout => \decimal_printer_inst|Selector0~0_combout\);

-- Location: LCCOMB_X26_Y70_N0
\decimal_printer_inst|Selector0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector0~1_combout\ = (\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\ & (((\decimal_printer_inst|state.digit_cnt\(0))) # (!\decimal_printer_inst|Selector42~0_combout\))) # (!\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\ & 
-- (((\decimal_printer_inst|Selector0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110001110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Selector42~0_combout\,
	datab => \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\,
	datac => \decimal_printer_inst|Selector0~0_combout\,
	datad => \decimal_printer_inst|state.digit_cnt\(0),
	combout => \decimal_printer_inst|Selector0~1_combout\);

-- Location: IOIBUF_X0_Y36_N15
\res_n~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_res_n,
	o => \res_n~input_o\);

-- Location: CLKCTRL_G4
\res_n~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \res_n~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \res_n~inputclkctrl_outclk\);

-- Location: FF_X26_Y70_N1
\decimal_printer_inst|state.fsm_state.IDLE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|Selector0~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.fsm_state.IDLE~q\);

-- Location: LCCOMB_X27_Y70_N6
\decimal_printer_inst|Add1~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add1~2_combout\ = (\decimal_printer_inst|state.number\(5) & (\decimal_printer_inst|Add1~1\ & VCC)) # (!\decimal_printer_inst|state.number\(5) & (!\decimal_printer_inst|Add1~1\))
-- \decimal_printer_inst|Add1~3\ = CARRY((!\decimal_printer_inst|state.number\(5) & !\decimal_printer_inst|Add1~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(5),
	datad => VCC,
	cin => \decimal_printer_inst|Add1~1\,
	combout => \decimal_printer_inst|Add1~2_combout\,
	cout => \decimal_printer_inst|Add1~3\);

-- Location: LCCOMB_X27_Y70_N8
\decimal_printer_inst|Add1~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add1~4_combout\ = (\decimal_printer_inst|state.number\(6) & ((GND) # (!\decimal_printer_inst|Add1~3\))) # (!\decimal_printer_inst|state.number\(6) & (\decimal_printer_inst|Add1~3\ $ (GND)))
-- \decimal_printer_inst|Add1~5\ = CARRY((\decimal_printer_inst|state.number\(6)) # (!\decimal_printer_inst|Add1~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.number\(6),
	datad => VCC,
	cin => \decimal_printer_inst|Add1~3\,
	combout => \decimal_printer_inst|Add1~4_combout\,
	cout => \decimal_printer_inst|Add1~5\);

-- Location: LCCOMB_X27_Y70_N10
\decimal_printer_inst|Add1~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add1~6_combout\ = (\decimal_printer_inst|state.number\(7) & (\decimal_printer_inst|Add1~5\ & VCC)) # (!\decimal_printer_inst|state.number\(7) & (!\decimal_printer_inst|Add1~5\))
-- \decimal_printer_inst|Add1~7\ = CARRY((!\decimal_printer_inst|state.number\(7) & !\decimal_printer_inst|Add1~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(7),
	datad => VCC,
	cin => \decimal_printer_inst|Add1~5\,
	combout => \decimal_printer_inst|Add1~6_combout\,
	cout => \decimal_printer_inst|Add1~7\);

-- Location: LCCOMB_X27_Y70_N12
\decimal_printer_inst|Add1~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add1~8_combout\ = (\decimal_printer_inst|state.number\(8) & (\decimal_printer_inst|Add1~7\ $ (GND))) # (!\decimal_printer_inst|state.number\(8) & (!\decimal_printer_inst|Add1~7\ & VCC))
-- \decimal_printer_inst|Add1~9\ = CARRY((\decimal_printer_inst|state.number\(8) & !\decimal_printer_inst|Add1~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(8),
	datad => VCC,
	cin => \decimal_printer_inst|Add1~7\,
	combout => \decimal_printer_inst|Add1~8_combout\,
	cout => \decimal_printer_inst|Add1~9\);

-- Location: LCCOMB_X29_Y70_N6
\decimal_printer_inst|Add5~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add5~4_combout\ = (\decimal_printer_inst|state.number\(4) & ((GND) # (!\decimal_printer_inst|Add5~3\))) # (!\decimal_printer_inst|state.number\(4) & (\decimal_printer_inst|Add5~3\ $ (GND)))
-- \decimal_printer_inst|Add5~5\ = CARRY((\decimal_printer_inst|state.number\(4)) # (!\decimal_printer_inst|Add5~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.number\(4),
	datad => VCC,
	cin => \decimal_printer_inst|Add5~3\,
	combout => \decimal_printer_inst|Add5~4_combout\,
	cout => \decimal_printer_inst|Add5~5\);

-- Location: LCCOMB_X29_Y70_N8
\decimal_printer_inst|Add5~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add5~6_combout\ = (\decimal_printer_inst|state.number\(5) & (!\decimal_printer_inst|Add5~5\)) # (!\decimal_printer_inst|state.number\(5) & ((\decimal_printer_inst|Add5~5\) # (GND)))
-- \decimal_printer_inst|Add5~7\ = CARRY((!\decimal_printer_inst|Add5~5\) # (!\decimal_printer_inst|state.number\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(5),
	datad => VCC,
	cin => \decimal_printer_inst|Add5~5\,
	combout => \decimal_printer_inst|Add5~6_combout\,
	cout => \decimal_printer_inst|Add5~7\);

-- Location: LCCOMB_X29_Y70_N10
\decimal_printer_inst|Add5~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add5~8_combout\ = (\decimal_printer_inst|state.number\(6) & (\decimal_printer_inst|Add5~7\ $ (GND))) # (!\decimal_printer_inst|state.number\(6) & (!\decimal_printer_inst|Add5~7\ & VCC))
-- \decimal_printer_inst|Add5~9\ = CARRY((\decimal_printer_inst|state.number\(6) & !\decimal_printer_inst|Add5~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(6),
	datad => VCC,
	cin => \decimal_printer_inst|Add5~7\,
	combout => \decimal_printer_inst|Add5~8_combout\,
	cout => \decimal_printer_inst|Add5~9\);

-- Location: LCCOMB_X29_Y70_N12
\decimal_printer_inst|Add5~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add5~10_combout\ = (\decimal_printer_inst|state.number\(7) & (\decimal_printer_inst|Add5~9\ & VCC)) # (!\decimal_printer_inst|state.number\(7) & (!\decimal_printer_inst|Add5~9\))
-- \decimal_printer_inst|Add5~11\ = CARRY((!\decimal_printer_inst|state.number\(7) & !\decimal_printer_inst|Add5~9\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.number\(7),
	datad => VCC,
	cin => \decimal_printer_inst|Add5~9\,
	combout => \decimal_printer_inst|Add5~10_combout\,
	cout => \decimal_printer_inst|Add5~11\);

-- Location: LCCOMB_X29_Y70_N14
\decimal_printer_inst|Add5~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add5~12_combout\ = (\decimal_printer_inst|state.number\(8) & ((GND) # (!\decimal_printer_inst|Add5~11\))) # (!\decimal_printer_inst|state.number\(8) & (\decimal_printer_inst|Add5~11\ $ (GND)))
-- \decimal_printer_inst|Add5~13\ = CARRY((\decimal_printer_inst|state.number\(8)) # (!\decimal_printer_inst|Add5~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(8),
	datad => VCC,
	cin => \decimal_printer_inst|Add5~11\,
	combout => \decimal_printer_inst|Add5~12_combout\,
	cout => \decimal_printer_inst|Add5~13\);

-- Location: LCCOMB_X28_Y68_N2
\decimal_printer_inst|Add7~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add7~0_combout\ = \decimal_printer_inst|state.number\(1) $ (VCC)
-- \decimal_printer_inst|Add7~1\ = CARRY(\decimal_printer_inst|state.number\(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.number\(1),
	datad => VCC,
	combout => \decimal_printer_inst|Add7~0_combout\,
	cout => \decimal_printer_inst|Add7~1\);

-- Location: LCCOMB_X28_Y70_N10
\decimal_printer_inst|state.bcd_data[3][0]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.bcd_data[3][0]~4_combout\ = (!\decimal_printer_inst|state.number\(10) & (!\decimal_printer_inst|state.number\(11) & (!\decimal_printer_inst|state.number\(13) & !\decimal_printer_inst|state.number\(12))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(10),
	datab => \decimal_printer_inst|state.number\(11),
	datac => \decimal_printer_inst|state.number\(13),
	datad => \decimal_printer_inst|state.number\(12),
	combout => \decimal_printer_inst|state.bcd_data[3][0]~4_combout\);

-- Location: LCCOMB_X29_Y69_N24
\decimal_printer_inst|LessThan2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|LessThan2~0_combout\ = (!\decimal_printer_inst|state.number\(2) & (!\decimal_printer_inst|state.number\(3) & (!\decimal_printer_inst|state.number\(7) & !\decimal_printer_inst|state.number\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(2),
	datab => \decimal_printer_inst|state.number\(3),
	datac => \decimal_printer_inst|state.number\(7),
	datad => \decimal_printer_inst|state.number\(4),
	combout => \decimal_printer_inst|LessThan2~0_combout\);

-- Location: LCCOMB_X29_Y69_N2
\decimal_printer_inst|LessThan2~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|LessThan2~1_combout\ = (\decimal_printer_inst|LessThan2~0_combout\) # ((!\decimal_printer_inst|state.number\(7) & ((!\decimal_printer_inst|state.number\(5)) # (!\decimal_printer_inst|state.number\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110111001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(6),
	datab => \decimal_printer_inst|LessThan2~0_combout\,
	datac => \decimal_printer_inst|state.number\(7),
	datad => \decimal_printer_inst|state.number\(5),
	combout => \decimal_printer_inst|LessThan2~1_combout\);

-- Location: LCCOMB_X28_Y71_N16
\decimal_printer_inst|state_nxt~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state_nxt~7_combout\ = (\decimal_printer_inst|LessThan3~0_combout\ & (\decimal_printer_inst|state_nxt~5_combout\ & (\decimal_printer_inst|state.bcd_data[3][0]~4_combout\ & \decimal_printer_inst|LessThan2~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|LessThan3~0_combout\,
	datab => \decimal_printer_inst|state_nxt~5_combout\,
	datac => \decimal_printer_inst|state.bcd_data[3][0]~4_combout\,
	datad => \decimal_printer_inst|LessThan2~1_combout\,
	combout => \decimal_printer_inst|state_nxt~7_combout\);

-- Location: LCCOMB_X27_Y71_N12
\decimal_printer_inst|LessThan3~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|LessThan3~2_combout\ = ((!\decimal_printer_inst|state.number\(1) & !\decimal_printer_inst|state.number\(2))) # (!\decimal_printer_inst|state.number\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.number\(3),
	datac => \decimal_printer_inst|state.number\(1),
	datad => \decimal_printer_inst|state.number\(2),
	combout => \decimal_printer_inst|LessThan3~2_combout\);

-- Location: LCCOMB_X28_Y68_N0
\decimal_printer_inst|LessThan3~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|LessThan3~1_combout\ = (!\decimal_printer_inst|state.number\(4) & (!\decimal_printer_inst|state.number\(6) & (!\decimal_printer_inst|state.number\(7) & !\decimal_printer_inst|state.number\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(4),
	datab => \decimal_printer_inst|state.number\(6),
	datac => \decimal_printer_inst|state.number\(7),
	datad => \decimal_printer_inst|state.number\(5),
	combout => \decimal_printer_inst|LessThan3~1_combout\);

-- Location: LCCOMB_X28_Y71_N28
\decimal_printer_inst|LessThan3~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|LessThan3~3_combout\ = (\decimal_printer_inst|LessThan3~2_combout\ & (\decimal_printer_inst|state_nxt~6_combout\ & (\decimal_printer_inst|LessThan3~1_combout\ & \decimal_printer_inst|LessThan3~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|LessThan3~2_combout\,
	datab => \decimal_printer_inst|state_nxt~6_combout\,
	datac => \decimal_printer_inst|LessThan3~1_combout\,
	datad => \decimal_printer_inst|LessThan3~0_combout\,
	combout => \decimal_printer_inst|LessThan3~3_combout\);

-- Location: LCCOMB_X28_Y71_N0
\decimal_printer_inst|Selector19~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector19~0_combout\ = (\decimal_printer_inst|state_nxt~7_combout\ & (\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & !\decimal_printer_inst|LessThan3~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state_nxt~7_combout\,
	datac => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	datad => \decimal_printer_inst|LessThan3~3_combout\,
	combout => \decimal_printer_inst|Selector19~0_combout\);

-- Location: LCCOMB_X27_Y71_N18
\decimal_printer_inst|state.number[1]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.number[1]~2_combout\ = (\decimal_printer_inst|Selector19~0_combout\ & (\decimal_printer_inst|Add7~0_combout\)) # (!\decimal_printer_inst|Selector19~0_combout\ & ((\decimal_printer_inst|state.number\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Add7~0_combout\,
	datab => \decimal_printer_inst|Selector19~0_combout\,
	datac => \decimal_printer_inst|state.number\(1),
	combout => \decimal_printer_inst|state.number[1]~2_combout\);

-- Location: IOIBUF_X23_Y73_N15
\number[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_number(1),
	o => \number[1]~input_o\);

-- Location: FF_X27_Y71_N19
\decimal_printer_inst|state.number[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|state.number[1]~2_combout\,
	asdata => \number[1]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \decimal_printer_inst|ALT_INV_state.fsm_state.IDLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.number\(1));

-- Location: LCCOMB_X28_Y68_N4
\decimal_printer_inst|Add7~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add7~2_combout\ = (\decimal_printer_inst|state.number\(2) & (\decimal_printer_inst|Add7~1\ & VCC)) # (!\decimal_printer_inst|state.number\(2) & (!\decimal_printer_inst|Add7~1\))
-- \decimal_printer_inst|Add7~3\ = CARRY((!\decimal_printer_inst|state.number\(2) & !\decimal_printer_inst|Add7~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(2),
	datad => VCC,
	cin => \decimal_printer_inst|Add7~1\,
	combout => \decimal_printer_inst|Add7~2_combout\,
	cout => \decimal_printer_inst|Add7~3\);

-- Location: LCCOMB_X28_Y71_N18
\decimal_printer_inst|state_nxt~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state_nxt~12_combout\ = (\decimal_printer_inst|state_nxt~7_combout\ & ((\decimal_printer_inst|LessThan3~3_combout\ & ((\decimal_printer_inst|state.number\(2)))) # (!\decimal_printer_inst|LessThan3~3_combout\ & 
-- (\decimal_printer_inst|Add7~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Add7~2_combout\,
	datab => \decimal_printer_inst|state_nxt~7_combout\,
	datac => \decimal_printer_inst|state.number\(2),
	datad => \decimal_printer_inst|LessThan3~3_combout\,
	combout => \decimal_printer_inst|state_nxt~12_combout\);

-- Location: LCCOMB_X29_Y70_N2
\decimal_printer_inst|Add5~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add5~0_combout\ = \decimal_printer_inst|state.number\(2) $ (VCC)
-- \decimal_printer_inst|Add5~1\ = CARRY(\decimal_printer_inst|state.number\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(2),
	datad => VCC,
	combout => \decimal_printer_inst|Add5~0_combout\,
	cout => \decimal_printer_inst|Add5~1\);

-- Location: LCCOMB_X28_Y71_N12
\decimal_printer_inst|state_nxt~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state_nxt~13_combout\ = (\decimal_printer_inst|state_nxt~12_combout\) # ((\decimal_printer_inst|Add5~0_combout\ & !\decimal_printer_inst|state_nxt~7_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state_nxt~12_combout\,
	datac => \decimal_printer_inst|Add5~0_combout\,
	datad => \decimal_printer_inst|state_nxt~7_combout\,
	combout => \decimal_printer_inst|state_nxt~13_combout\);

-- Location: LCCOMB_X28_Y71_N6
\decimal_printer_inst|Selector18~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector18~0_combout\ = ((!\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\) # (!\decimal_printer_inst|state_nxt~6_combout\)) # (!\decimal_printer_inst|LessThan1~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|LessThan1~1_combout\,
	datac => \decimal_printer_inst|state_nxt~6_combout\,
	datad => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	combout => \decimal_printer_inst|Selector18~0_combout\);

-- Location: LCCOMB_X28_Y71_N8
\decimal_printer_inst|state.number[2]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.number[2]~1_combout\ = (\decimal_printer_inst|Selector18~0_combout\ & ((\decimal_printer_inst|state.number\(2)))) # (!\decimal_printer_inst|Selector18~0_combout\ & (\decimal_printer_inst|state_nxt~13_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state_nxt~13_combout\,
	datac => \decimal_printer_inst|state.number\(2),
	datad => \decimal_printer_inst|Selector18~0_combout\,
	combout => \decimal_printer_inst|state.number[2]~1_combout\);

-- Location: IOIBUF_X27_Y73_N15
\number[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_number(2),
	o => \number[2]~input_o\);

-- Location: FF_X28_Y71_N9
\decimal_printer_inst|state.number[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|state.number[2]~1_combout\,
	asdata => \number[2]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \decimal_printer_inst|ALT_INV_state.fsm_state.IDLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.number\(2));

-- Location: LCCOMB_X28_Y68_N6
\decimal_printer_inst|Add7~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add7~4_combout\ = (\decimal_printer_inst|state.number\(3) & (\decimal_printer_inst|Add7~3\ $ (GND))) # (!\decimal_printer_inst|state.number\(3) & (!\decimal_printer_inst|Add7~3\ & VCC))
-- \decimal_printer_inst|Add7~5\ = CARRY((\decimal_printer_inst|state.number\(3) & !\decimal_printer_inst|Add7~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(3),
	datad => VCC,
	cin => \decimal_printer_inst|Add7~3\,
	combout => \decimal_printer_inst|Add7~4_combout\,
	cout => \decimal_printer_inst|Add7~5\);

-- Location: LCCOMB_X28_Y68_N8
\decimal_printer_inst|Add7~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add7~6_combout\ = (\decimal_printer_inst|state.number\(4) & (\decimal_printer_inst|Add7~5\ & VCC)) # (!\decimal_printer_inst|state.number\(4) & (!\decimal_printer_inst|Add7~5\))
-- \decimal_printer_inst|Add7~7\ = CARRY((!\decimal_printer_inst|state.number\(4) & !\decimal_printer_inst|Add7~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(4),
	datad => VCC,
	cin => \decimal_printer_inst|Add7~5\,
	combout => \decimal_printer_inst|Add7~6_combout\,
	cout => \decimal_printer_inst|Add7~7\);

-- Location: LCCOMB_X28_Y68_N10
\decimal_printer_inst|Add7~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add7~8_combout\ = (\decimal_printer_inst|state.number\(5) & ((GND) # (!\decimal_printer_inst|Add7~7\))) # (!\decimal_printer_inst|state.number\(5) & (\decimal_printer_inst|Add7~7\ $ (GND)))
-- \decimal_printer_inst|Add7~9\ = CARRY((\decimal_printer_inst|state.number\(5)) # (!\decimal_printer_inst|Add7~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(5),
	datad => VCC,
	cin => \decimal_printer_inst|Add7~7\,
	combout => \decimal_printer_inst|Add7~8_combout\,
	cout => \decimal_printer_inst|Add7~9\);

-- Location: LCCOMB_X28_Y68_N12
\decimal_printer_inst|Add7~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add7~10_combout\ = (\decimal_printer_inst|state.number\(6) & (\decimal_printer_inst|Add7~9\ & VCC)) # (!\decimal_printer_inst|state.number\(6) & (!\decimal_printer_inst|Add7~9\))
-- \decimal_printer_inst|Add7~11\ = CARRY((!\decimal_printer_inst|state.number\(6) & !\decimal_printer_inst|Add7~9\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.number\(6),
	datad => VCC,
	cin => \decimal_printer_inst|Add7~9\,
	combout => \decimal_printer_inst|Add7~10_combout\,
	cout => \decimal_printer_inst|Add7~11\);

-- Location: LCCOMB_X28_Y68_N14
\decimal_printer_inst|Add7~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add7~12_combout\ = (\decimal_printer_inst|state.number\(7) & ((GND) # (!\decimal_printer_inst|Add7~11\))) # (!\decimal_printer_inst|state.number\(7) & (\decimal_printer_inst|Add7~11\ $ (GND)))
-- \decimal_printer_inst|Add7~13\ = CARRY((\decimal_printer_inst|state.number\(7)) # (!\decimal_printer_inst|Add7~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.number\(7),
	datad => VCC,
	cin => \decimal_printer_inst|Add7~11\,
	combout => \decimal_printer_inst|Add7~12_combout\,
	cout => \decimal_printer_inst|Add7~13\);

-- Location: LCCOMB_X28_Y68_N16
\decimal_printer_inst|Add7~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add7~14_combout\ = (\decimal_printer_inst|state.number\(8) & (\decimal_printer_inst|Add7~13\ & VCC)) # (!\decimal_printer_inst|state.number\(8) & (!\decimal_printer_inst|Add7~13\))
-- \decimal_printer_inst|Add7~15\ = CARRY((!\decimal_printer_inst|state.number\(8) & !\decimal_printer_inst|Add7~13\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(8),
	datad => VCC,
	cin => \decimal_printer_inst|Add7~13\,
	combout => \decimal_printer_inst|Add7~14_combout\,
	cout => \decimal_printer_inst|Add7~15\);

-- Location: LCCOMB_X28_Y69_N28
\decimal_printer_inst|Selector12~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector12~0_combout\ = (\decimal_printer_inst|state.number[15]~5_combout\ & (((\decimal_printer_inst|Add7~14_combout\)) # (!\decimal_printer_inst|state_nxt~8_combout\))) # (!\decimal_printer_inst|state.number[15]~5_combout\ & 
-- (\decimal_printer_inst|state_nxt~8_combout\ & (\decimal_printer_inst|Add5~12_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number[15]~5_combout\,
	datab => \decimal_printer_inst|state_nxt~8_combout\,
	datac => \decimal_printer_inst|Add5~12_combout\,
	datad => \decimal_printer_inst|Add7~14_combout\,
	combout => \decimal_printer_inst|Selector12~0_combout\);

-- Location: LCCOMB_X27_Y69_N14
\decimal_printer_inst|Add3~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add3~8_combout\ = (\decimal_printer_inst|state.number\(7) & (\decimal_printer_inst|Add3~7\ $ (GND))) # (!\decimal_printer_inst|state.number\(7) & (!\decimal_printer_inst|Add3~7\ & VCC))
-- \decimal_printer_inst|Add3~9\ = CARRY((\decimal_printer_inst|state.number\(7) & !\decimal_printer_inst|Add3~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.number\(7),
	datad => VCC,
	cin => \decimal_printer_inst|Add3~7\,
	combout => \decimal_printer_inst|Add3~8_combout\,
	cout => \decimal_printer_inst|Add3~9\);

-- Location: LCCOMB_X27_Y69_N16
\decimal_printer_inst|Add3~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add3~10_combout\ = (\decimal_printer_inst|state.number\(8) & (!\decimal_printer_inst|Add3~9\)) # (!\decimal_printer_inst|state.number\(8) & ((\decimal_printer_inst|Add3~9\) # (GND)))
-- \decimal_printer_inst|Add3~11\ = CARRY((!\decimal_printer_inst|Add3~9\) # (!\decimal_printer_inst|state.number\(8)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.number\(8),
	datad => VCC,
	cin => \decimal_printer_inst|Add3~9\,
	combout => \decimal_printer_inst|Add3~10_combout\,
	cout => \decimal_printer_inst|Add3~11\);

-- Location: LCCOMB_X28_Y69_N14
\decimal_printer_inst|Selector12~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector12~1_combout\ = (\decimal_printer_inst|Selector12~0_combout\ & (((\decimal_printer_inst|Add3~10_combout\) # (\decimal_printer_inst|state_nxt~8_combout\)))) # (!\decimal_printer_inst|Selector12~0_combout\ & 
-- (\decimal_printer_inst|Add1~8_combout\ & ((!\decimal_printer_inst|state_nxt~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Add1~8_combout\,
	datab => \decimal_printer_inst|Selector12~0_combout\,
	datac => \decimal_printer_inst|Add3~10_combout\,
	datad => \decimal_printer_inst|state_nxt~8_combout\,
	combout => \decimal_printer_inst|Selector12~1_combout\);

-- Location: LCCOMB_X28_Y70_N26
\decimal_printer_inst|state.number[8]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.number[8]~feeder_combout\ = \decimal_printer_inst|Selector12~1_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \decimal_printer_inst|Selector12~1_combout\,
	combout => \decimal_printer_inst|state.number[8]~feeder_combout\);

-- Location: IOIBUF_X27_Y73_N8
\number[8]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_number(8),
	o => \number[8]~input_o\);

-- Location: LCCOMB_X28_Y69_N6
\decimal_printer_inst|state.number[15]~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.number[15]~6_combout\ = (\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & ((!\decimal_printer_inst|LessThan3~3_combout\))) # (!\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & 
-- (!\decimal_printer_inst|state.fsm_state.IDLE~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111011101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.fsm_state.IDLE~q\,
	datab => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	datad => \decimal_printer_inst|LessThan3~3_combout\,
	combout => \decimal_printer_inst|state.number[15]~6_combout\);

-- Location: FF_X28_Y70_N27
\decimal_printer_inst|state.number[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|state.number[8]~feeder_combout\,
	asdata => \number[8]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \decimal_printer_inst|ALT_INV_state.fsm_state.CALC_DIGITS~q\,
	ena => \decimal_printer_inst|state.number[15]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.number\(8));

-- Location: LCCOMB_X28_Y68_N18
\decimal_printer_inst|Add7~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add7~16_combout\ = (\decimal_printer_inst|state.number\(9) & ((GND) # (!\decimal_printer_inst|Add7~15\))) # (!\decimal_printer_inst|state.number\(9) & (\decimal_printer_inst|Add7~15\ $ (GND)))
-- \decimal_printer_inst|Add7~17\ = CARRY((\decimal_printer_inst|state.number\(9)) # (!\decimal_printer_inst|Add7~15\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(9),
	datad => VCC,
	cin => \decimal_printer_inst|Add7~15\,
	combout => \decimal_printer_inst|Add7~16_combout\,
	cout => \decimal_printer_inst|Add7~17\);

-- Location: LCCOMB_X27_Y70_N14
\decimal_printer_inst|Add1~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add1~10_combout\ = (\decimal_printer_inst|state.number\(9) & (!\decimal_printer_inst|Add1~9\)) # (!\decimal_printer_inst|state.number\(9) & ((\decimal_printer_inst|Add1~9\) # (GND)))
-- \decimal_printer_inst|Add1~11\ = CARRY((!\decimal_printer_inst|Add1~9\) # (!\decimal_printer_inst|state.number\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.number\(9),
	datad => VCC,
	cin => \decimal_printer_inst|Add1~9\,
	combout => \decimal_printer_inst|Add1~10_combout\,
	cout => \decimal_printer_inst|Add1~11\);

-- Location: LCCOMB_X29_Y70_N16
\decimal_printer_inst|Add5~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add5~14_combout\ = (\decimal_printer_inst|state.number\(9) & (\decimal_printer_inst|Add5~13\ & VCC)) # (!\decimal_printer_inst|state.number\(9) & (!\decimal_printer_inst|Add5~13\))
-- \decimal_printer_inst|Add5~15\ = CARRY((!\decimal_printer_inst|state.number\(9) & !\decimal_printer_inst|Add5~13\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(9),
	datad => VCC,
	cin => \decimal_printer_inst|Add5~13\,
	combout => \decimal_printer_inst|Add5~14_combout\,
	cout => \decimal_printer_inst|Add5~15\);

-- Location: LCCOMB_X29_Y70_N30
\decimal_printer_inst|Selector11~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector11~0_combout\ = (\decimal_printer_inst|state.number[15]~5_combout\ & (!\decimal_printer_inst|state_nxt~8_combout\)) # (!\decimal_printer_inst|state.number[15]~5_combout\ & ((\decimal_printer_inst|state_nxt~8_combout\ & 
-- ((\decimal_printer_inst|Add5~14_combout\))) # (!\decimal_printer_inst|state_nxt~8_combout\ & (\decimal_printer_inst|Add1~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011000110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number[15]~5_combout\,
	datab => \decimal_printer_inst|state_nxt~8_combout\,
	datac => \decimal_printer_inst|Add1~10_combout\,
	datad => \decimal_printer_inst|Add5~14_combout\,
	combout => \decimal_printer_inst|Selector11~0_combout\);

-- Location: LCCOMB_X27_Y69_N18
\decimal_printer_inst|Add3~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add3~12_combout\ = (\decimal_printer_inst|state.number\(9) & (\decimal_printer_inst|Add3~11\ $ (GND))) # (!\decimal_printer_inst|state.number\(9) & (!\decimal_printer_inst|Add3~11\ & VCC))
-- \decimal_printer_inst|Add3~13\ = CARRY((\decimal_printer_inst|state.number\(9) & !\decimal_printer_inst|Add3~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(9),
	datad => VCC,
	cin => \decimal_printer_inst|Add3~11\,
	combout => \decimal_printer_inst|Add3~12_combout\,
	cout => \decimal_printer_inst|Add3~13\);

-- Location: LCCOMB_X28_Y70_N22
\decimal_printer_inst|Selector11~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector11~1_combout\ = (\decimal_printer_inst|state.number[15]~5_combout\ & ((\decimal_printer_inst|Selector11~0_combout\ & ((\decimal_printer_inst|Add3~12_combout\))) # (!\decimal_printer_inst|Selector11~0_combout\ & 
-- (\decimal_printer_inst|Add7~16_combout\)))) # (!\decimal_printer_inst|state.number[15]~5_combout\ & (((\decimal_printer_inst|Selector11~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100000111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Add7~16_combout\,
	datab => \decimal_printer_inst|state.number[15]~5_combout\,
	datac => \decimal_printer_inst|Selector11~0_combout\,
	datad => \decimal_printer_inst|Add3~12_combout\,
	combout => \decimal_printer_inst|Selector11~1_combout\);

-- Location: LCCOMB_X28_Y70_N8
\decimal_printer_inst|state.number[9]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.number[9]~feeder_combout\ = \decimal_printer_inst|Selector11~1_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Selector11~1_combout\,
	combout => \decimal_printer_inst|state.number[9]~feeder_combout\);

-- Location: IOIBUF_X29_Y73_N8
\number[9]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_number(9),
	o => \number[9]~input_o\);

-- Location: FF_X28_Y70_N9
\decimal_printer_inst|state.number[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|state.number[9]~feeder_combout\,
	asdata => \number[9]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \decimal_printer_inst|ALT_INV_state.fsm_state.CALC_DIGITS~q\,
	ena => \decimal_printer_inst|state.number[15]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.number\(9));

-- Location: LCCOMB_X28_Y70_N14
\decimal_printer_inst|LessThan3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|LessThan3~0_combout\ = (!\decimal_printer_inst|state.number\(9) & !\decimal_printer_inst|state.number\(8))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.number\(9),
	datac => \decimal_printer_inst|state.number\(8),
	combout => \decimal_printer_inst|LessThan3~0_combout\);

-- Location: LCCOMB_X28_Y71_N2
\decimal_printer_inst|state.number[15]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.number[15]~3_combout\ = (\decimal_printer_inst|LessThan3~0_combout\ & (\decimal_printer_inst|state_nxt~6_combout\ & (\decimal_printer_inst|LessThan1~1_combout\ & \decimal_printer_inst|LessThan2~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|LessThan3~0_combout\,
	datab => \decimal_printer_inst|state_nxt~6_combout\,
	datac => \decimal_printer_inst|LessThan1~1_combout\,
	datad => \decimal_printer_inst|LessThan2~1_combout\,
	combout => \decimal_printer_inst|state.number[15]~3_combout\);

-- Location: LCCOMB_X28_Y71_N30
\decimal_printer_inst|Selector1~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector1~2_combout\ = (\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & (((!\decimal_printer_inst|LessThan3~3_combout\) # (!\decimal_printer_inst|state.number[15]~3_combout\)) # 
-- (!\decimal_printer_inst|state.number[15]~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number[15]~4_combout\,
	datab => \decimal_printer_inst|state.number[15]~3_combout\,
	datac => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	datad => \decimal_printer_inst|LessThan3~3_combout\,
	combout => \decimal_printer_inst|Selector1~2_combout\);

-- Location: LCCOMB_X25_Y70_N0
\decimal_printer_inst|Selector1~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector1~3_combout\ = (\decimal_printer_inst|Selector1~2_combout\) # ((\start~input_o\ & !\decimal_printer_inst|state.fsm_state.IDLE~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001011110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \start~input_o\,
	datab => \decimal_printer_inst|state.fsm_state.IDLE~q\,
	datac => \decimal_printer_inst|Selector1~2_combout\,
	combout => \decimal_printer_inst|Selector1~3_combout\);

-- Location: FF_X25_Y70_N1
\decimal_printer_inst|state.fsm_state.CALC_DIGITS\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|Selector1~3_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\);

-- Location: LCCOMB_X27_Y71_N24
\decimal_printer_inst|state.bcd_data[4][0]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.bcd_data[4][0]~5_combout\ = (\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & \decimal_printer_inst|state.number[15]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	datad => \decimal_printer_inst|state.number[15]~4_combout\,
	combout => \decimal_printer_inst|state.bcd_data[4][0]~5_combout\);

-- Location: LCCOMB_X27_Y69_N6
\decimal_printer_inst|Add3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add3~0_combout\ = \decimal_printer_inst|state.number\(3) $ (VCC)
-- \decimal_printer_inst|Add3~1\ = CARRY(\decimal_printer_inst|state.number\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.number\(3),
	datad => VCC,
	combout => \decimal_printer_inst|Add3~0_combout\,
	cout => \decimal_printer_inst|Add3~1\);

-- Location: LCCOMB_X29_Y70_N4
\decimal_printer_inst|Add5~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add5~2_combout\ = (\decimal_printer_inst|state.number\(3) & (\decimal_printer_inst|Add5~1\ & VCC)) # (!\decimal_printer_inst|state.number\(3) & (!\decimal_printer_inst|Add5~1\))
-- \decimal_printer_inst|Add5~3\ = CARRY((!\decimal_printer_inst|state.number\(3) & !\decimal_printer_inst|Add5~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(3),
	datad => VCC,
	cin => \decimal_printer_inst|Add5~1\,
	combout => \decimal_printer_inst|Add5~2_combout\,
	cout => \decimal_printer_inst|Add5~3\);

-- Location: LCCOMB_X28_Y71_N24
\decimal_printer_inst|state_nxt~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state_nxt~10_combout\ = (\decimal_printer_inst|state_nxt~8_combout\ & (((!\decimal_printer_inst|state_nxt~7_combout\ & \decimal_printer_inst|Add5~2_combout\)))) # (!\decimal_printer_inst|state_nxt~8_combout\ & 
-- (\decimal_printer_inst|Add3~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Add3~0_combout\,
	datab => \decimal_printer_inst|state_nxt~7_combout\,
	datac => \decimal_printer_inst|Add5~2_combout\,
	datad => \decimal_printer_inst|state_nxt~8_combout\,
	combout => \decimal_printer_inst|state_nxt~10_combout\);

-- Location: LCCOMB_X28_Y71_N22
\decimal_printer_inst|state_nxt~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state_nxt~9_combout\ = (\decimal_printer_inst|state.number[15]~3_combout\ & ((\decimal_printer_inst|LessThan3~3_combout\ & ((\decimal_printer_inst|state.number\(3)))) # (!\decimal_printer_inst|LessThan3~3_combout\ & 
-- (\decimal_printer_inst|Add7~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Add7~4_combout\,
	datab => \decimal_printer_inst|state.number[15]~3_combout\,
	datac => \decimal_printer_inst|state.number\(3),
	datad => \decimal_printer_inst|LessThan3~3_combout\,
	combout => \decimal_printer_inst|state_nxt~9_combout\);

-- Location: LCCOMB_X27_Y71_N6
\decimal_printer_inst|state_nxt~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state_nxt~11_combout\ = (\decimal_printer_inst|state_nxt~10_combout\) # (\decimal_printer_inst|state_nxt~9_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state_nxt~10_combout\,
	datad => \decimal_printer_inst|state_nxt~9_combout\,
	combout => \decimal_printer_inst|state_nxt~11_combout\);

-- Location: LCCOMB_X27_Y71_N16
\decimal_printer_inst|state.number[3]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.number[3]~0_combout\ = (\decimal_printer_inst|state.bcd_data[4][0]~5_combout\ & ((\decimal_printer_inst|state_nxt~11_combout\))) # (!\decimal_printer_inst|state.bcd_data[4][0]~5_combout\ & 
-- (\decimal_printer_inst|state.number\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.bcd_data[4][0]~5_combout\,
	datac => \decimal_printer_inst|state.number\(3),
	datad => \decimal_printer_inst|state_nxt~11_combout\,
	combout => \decimal_printer_inst|state.number[3]~0_combout\);

-- Location: IOIBUF_X23_Y73_N8
\number[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_number(3),
	o => \number[3]~input_o\);

-- Location: FF_X27_Y71_N17
\decimal_printer_inst|state.number[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|state.number[3]~0_combout\,
	asdata => \number[3]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \decimal_printer_inst|ALT_INV_state.fsm_state.IDLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.number\(3));

-- Location: LCCOMB_X28_Y69_N26
\decimal_printer_inst|Selector16~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector16~0_combout\ = (\decimal_printer_inst|state_nxt~8_combout\ & (((!\decimal_printer_inst|state.number[15]~5_combout\ & \decimal_printer_inst|Add5~4_combout\)))) # (!\decimal_printer_inst|state_nxt~8_combout\ & 
-- ((\decimal_printer_inst|Add1~0_combout\) # ((\decimal_printer_inst|state.number[15]~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111000110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Add1~0_combout\,
	datab => \decimal_printer_inst|state_nxt~8_combout\,
	datac => \decimal_printer_inst|state.number[15]~5_combout\,
	datad => \decimal_printer_inst|Add5~4_combout\,
	combout => \decimal_printer_inst|Selector16~0_combout\);

-- Location: LCCOMB_X27_Y69_N8
\decimal_printer_inst|Add3~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add3~2_combout\ = (\decimal_printer_inst|state.number\(4) & (\decimal_printer_inst|Add3~1\ & VCC)) # (!\decimal_printer_inst|state.number\(4) & (!\decimal_printer_inst|Add3~1\))
-- \decimal_printer_inst|Add3~3\ = CARRY((!\decimal_printer_inst|state.number\(4) & !\decimal_printer_inst|Add3~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(4),
	datad => VCC,
	cin => \decimal_printer_inst|Add3~1\,
	combout => \decimal_printer_inst|Add3~2_combout\,
	cout => \decimal_printer_inst|Add3~3\);

-- Location: LCCOMB_X28_Y69_N12
\decimal_printer_inst|Selector16~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector16~1_combout\ = (\decimal_printer_inst|Selector16~0_combout\ & ((\decimal_printer_inst|Add3~2_combout\) # ((!\decimal_printer_inst|state.number[15]~5_combout\)))) # (!\decimal_printer_inst|Selector16~0_combout\ & 
-- (((\decimal_printer_inst|state.number[15]~5_combout\ & \decimal_printer_inst|Add7~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101010001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Selector16~0_combout\,
	datab => \decimal_printer_inst|Add3~2_combout\,
	datac => \decimal_printer_inst|state.number[15]~5_combout\,
	datad => \decimal_printer_inst|Add7~6_combout\,
	combout => \decimal_printer_inst|Selector16~1_combout\);

-- Location: LCCOMB_X29_Y69_N16
\decimal_printer_inst|state.number[4]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.number[4]~feeder_combout\ = \decimal_printer_inst|Selector16~1_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \decimal_printer_inst|Selector16~1_combout\,
	combout => \decimal_printer_inst|state.number[4]~feeder_combout\);

-- Location: IOIBUF_X29_Y73_N1
\number[4]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_number(4),
	o => \number[4]~input_o\);

-- Location: FF_X29_Y69_N17
\decimal_printer_inst|state.number[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|state.number[4]~feeder_combout\,
	asdata => \number[4]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \decimal_printer_inst|ALT_INV_state.fsm_state.CALC_DIGITS~q\,
	ena => \decimal_printer_inst|state.number[15]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.number\(4));

-- Location: LCCOMB_X27_Y69_N10
\decimal_printer_inst|Add3~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add3~4_combout\ = (\decimal_printer_inst|state.number\(5) & (\decimal_printer_inst|Add3~3\ $ (GND))) # (!\decimal_printer_inst|state.number\(5) & (!\decimal_printer_inst|Add3~3\ & VCC))
-- \decimal_printer_inst|Add3~5\ = CARRY((\decimal_printer_inst|state.number\(5) & !\decimal_printer_inst|Add3~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.number\(5),
	datad => VCC,
	cin => \decimal_printer_inst|Add3~3\,
	combout => \decimal_printer_inst|Add3~4_combout\,
	cout => \decimal_printer_inst|Add3~5\);

-- Location: LCCOMB_X28_Y69_N22
\decimal_printer_inst|Selector15~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector15~0_combout\ = (\decimal_printer_inst|state.number[15]~5_combout\ & (((\decimal_printer_inst|Add7~8_combout\)) # (!\decimal_printer_inst|state_nxt~8_combout\))) # (!\decimal_printer_inst|state.number[15]~5_combout\ & 
-- (\decimal_printer_inst|state_nxt~8_combout\ & ((\decimal_printer_inst|Add5~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number[15]~5_combout\,
	datab => \decimal_printer_inst|state_nxt~8_combout\,
	datac => \decimal_printer_inst|Add7~8_combout\,
	datad => \decimal_printer_inst|Add5~6_combout\,
	combout => \decimal_printer_inst|Selector15~0_combout\);

-- Location: LCCOMB_X28_Y69_N24
\decimal_printer_inst|Selector15~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector15~1_combout\ = (\decimal_printer_inst|Selector15~0_combout\ & ((\decimal_printer_inst|Add3~4_combout\) # ((\decimal_printer_inst|state_nxt~8_combout\)))) # (!\decimal_printer_inst|Selector15~0_combout\ & 
-- (((\decimal_printer_inst|Add1~2_combout\ & !\decimal_printer_inst|state_nxt~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Add3~4_combout\,
	datab => \decimal_printer_inst|Add1~2_combout\,
	datac => \decimal_printer_inst|Selector15~0_combout\,
	datad => \decimal_printer_inst|state_nxt~8_combout\,
	combout => \decimal_printer_inst|Selector15~1_combout\);

-- Location: LCCOMB_X29_Y69_N30
\decimal_printer_inst|state.number[5]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.number[5]~feeder_combout\ = \decimal_printer_inst|Selector15~1_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \decimal_printer_inst|Selector15~1_combout\,
	combout => \decimal_printer_inst|state.number[5]~feeder_combout\);

-- Location: IOIBUF_X33_Y73_N8
\number[5]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_number(5),
	o => \number[5]~input_o\);

-- Location: FF_X29_Y69_N31
\decimal_printer_inst|state.number[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|state.number[5]~feeder_combout\,
	asdata => \number[5]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \decimal_printer_inst|ALT_INV_state.fsm_state.CALC_DIGITS~q\,
	ena => \decimal_printer_inst|state.number[15]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.number\(5));

-- Location: LCCOMB_X27_Y69_N12
\decimal_printer_inst|Add3~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add3~6_combout\ = (\decimal_printer_inst|state.number\(6) & (!\decimal_printer_inst|Add3~5\)) # (!\decimal_printer_inst|state.number\(6) & ((\decimal_printer_inst|Add3~5\) # (GND)))
-- \decimal_printer_inst|Add3~7\ = CARRY((!\decimal_printer_inst|Add3~5\) # (!\decimal_printer_inst|state.number\(6)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.number\(6),
	datad => VCC,
	cin => \decimal_printer_inst|Add3~5\,
	combout => \decimal_printer_inst|Add3~6_combout\,
	cout => \decimal_printer_inst|Add3~7\);

-- Location: LCCOMB_X28_Y69_N18
\decimal_printer_inst|Selector14~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector14~0_combout\ = (\decimal_printer_inst|state.number[15]~5_combout\ & (((\decimal_printer_inst|Add7~10_combout\)) # (!\decimal_printer_inst|state_nxt~8_combout\))) # (!\decimal_printer_inst|state.number[15]~5_combout\ & 
-- (\decimal_printer_inst|state_nxt~8_combout\ & (\decimal_printer_inst|Add5~8_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number[15]~5_combout\,
	datab => \decimal_printer_inst|state_nxt~8_combout\,
	datac => \decimal_printer_inst|Add5~8_combout\,
	datad => \decimal_printer_inst|Add7~10_combout\,
	combout => \decimal_printer_inst|Selector14~0_combout\);

-- Location: LCCOMB_X28_Y69_N4
\decimal_printer_inst|Selector14~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector14~1_combout\ = (\decimal_printer_inst|Selector14~0_combout\ & ((\decimal_printer_inst|Add3~6_combout\) # ((\decimal_printer_inst|state_nxt~8_combout\)))) # (!\decimal_printer_inst|Selector14~0_combout\ & 
-- (((\decimal_printer_inst|Add1~4_combout\ & !\decimal_printer_inst|state_nxt~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Add3~6_combout\,
	datab => \decimal_printer_inst|Selector14~0_combout\,
	datac => \decimal_printer_inst|Add1~4_combout\,
	datad => \decimal_printer_inst|state_nxt~8_combout\,
	combout => \decimal_printer_inst|Selector14~1_combout\);

-- Location: LCCOMB_X29_Y69_N12
\decimal_printer_inst|state.number[6]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.number[6]~feeder_combout\ = \decimal_printer_inst|Selector14~1_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|Selector14~1_combout\,
	combout => \decimal_printer_inst|state.number[6]~feeder_combout\);

-- Location: IOIBUF_X25_Y73_N15
\number[6]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_number(6),
	o => \number[6]~input_o\);

-- Location: FF_X29_Y69_N13
\decimal_printer_inst|state.number[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|state.number[6]~feeder_combout\,
	asdata => \number[6]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \decimal_printer_inst|ALT_INV_state.fsm_state.CALC_DIGITS~q\,
	ena => \decimal_printer_inst|state.number[15]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.number\(6));

-- Location: LCCOMB_X29_Y69_N22
\decimal_printer_inst|Selector13~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector13~0_combout\ = (\decimal_printer_inst|state_nxt~8_combout\ & (((!\decimal_printer_inst|state.number[15]~5_combout\ & \decimal_printer_inst|Add5~10_combout\)))) # (!\decimal_printer_inst|state_nxt~8_combout\ & 
-- ((\decimal_printer_inst|Add1~6_combout\) # ((\decimal_printer_inst|state.number[15]~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111001010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state_nxt~8_combout\,
	datab => \decimal_printer_inst|Add1~6_combout\,
	datac => \decimal_printer_inst|state.number[15]~5_combout\,
	datad => \decimal_printer_inst|Add5~10_combout\,
	combout => \decimal_printer_inst|Selector13~0_combout\);

-- Location: LCCOMB_X28_Y69_N8
\decimal_printer_inst|Selector13~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector13~1_combout\ = (\decimal_printer_inst|state.number[15]~5_combout\ & ((\decimal_printer_inst|Selector13~0_combout\ & (\decimal_printer_inst|Add3~8_combout\)) # (!\decimal_printer_inst|Selector13~0_combout\ & 
-- ((\decimal_printer_inst|Add7~12_combout\))))) # (!\decimal_printer_inst|state.number[15]~5_combout\ & (((\decimal_printer_inst|Selector13~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Add3~8_combout\,
	datab => \decimal_printer_inst|Add7~12_combout\,
	datac => \decimal_printer_inst|state.number[15]~5_combout\,
	datad => \decimal_printer_inst|Selector13~0_combout\,
	combout => \decimal_printer_inst|Selector13~1_combout\);

-- Location: LCCOMB_X29_Y69_N26
\decimal_printer_inst|state.number[7]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.number[7]~feeder_combout\ = \decimal_printer_inst|Selector13~1_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \decimal_printer_inst|Selector13~1_combout\,
	combout => \decimal_printer_inst|state.number[7]~feeder_combout\);

-- Location: IOIBUF_X33_Y73_N1
\number[7]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_number(7),
	o => \number[7]~input_o\);

-- Location: FF_X29_Y69_N27
\decimal_printer_inst|state.number[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|state.number[7]~feeder_combout\,
	asdata => \number[7]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \decimal_printer_inst|ALT_INV_state.fsm_state.CALC_DIGITS~q\,
	ena => \decimal_printer_inst|state.number[15]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.number\(7));

-- Location: LCCOMB_X27_Y69_N0
\decimal_printer_inst|LessThan1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|LessThan1~0_combout\ = (((!\decimal_printer_inst|state.number\(3) & !\decimal_printer_inst|state.number\(4))) # (!\decimal_printer_inst|state.number\(8))) # (!\decimal_printer_inst|state.number\(9))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011101111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(9),
	datab => \decimal_printer_inst|state.number\(8),
	datac => \decimal_printer_inst|state.number\(3),
	datad => \decimal_printer_inst|state.number\(4),
	combout => \decimal_printer_inst|LessThan1~0_combout\);

-- Location: LCCOMB_X27_Y69_N2
\decimal_printer_inst|LessThan1~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|LessThan1~1_combout\ = (((\decimal_printer_inst|LessThan1~0_combout\) # (!\decimal_printer_inst|state.number\(5))) # (!\decimal_printer_inst|state.number\(6))) # (!\decimal_printer_inst|state.number\(7))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111011111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(7),
	datab => \decimal_printer_inst|state.number\(6),
	datac => \decimal_printer_inst|LessThan1~0_combout\,
	datad => \decimal_printer_inst|state.number\(5),
	combout => \decimal_printer_inst|LessThan1~1_combout\);

-- Location: LCCOMB_X28_Y71_N26
\decimal_printer_inst|state.number[15]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.number[15]~5_combout\ = (\decimal_printer_inst|state_nxt~6_combout\ & ((\decimal_printer_inst|LessThan1~1_combout\ & ((\decimal_printer_inst|state_nxt~7_combout\))) # (!\decimal_printer_inst|LessThan1~1_combout\ & 
-- (\decimal_printer_inst|state.number[15]~4_combout\)))) # (!\decimal_printer_inst|state_nxt~6_combout\ & (\decimal_printer_inst|state.number[15]~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101000101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number[15]~4_combout\,
	datab => \decimal_printer_inst|state_nxt~6_combout\,
	datac => \decimal_printer_inst|LessThan1~1_combout\,
	datad => \decimal_printer_inst|state_nxt~7_combout\,
	combout => \decimal_printer_inst|state.number[15]~5_combout\);

-- Location: LCCOMB_X28_Y68_N20
\decimal_printer_inst|Add7~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add7~18_combout\ = (\decimal_printer_inst|state.number\(10) & (\decimal_printer_inst|Add7~17\ & VCC)) # (!\decimal_printer_inst|state.number\(10) & (!\decimal_printer_inst|Add7~17\))
-- \decimal_printer_inst|Add7~19\ = CARRY((!\decimal_printer_inst|state.number\(10) & !\decimal_printer_inst|Add7~17\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(10),
	datad => VCC,
	cin => \decimal_printer_inst|Add7~17\,
	combout => \decimal_printer_inst|Add7~18_combout\,
	cout => \decimal_printer_inst|Add7~19\);

-- Location: LCCOMB_X28_Y68_N22
\decimal_printer_inst|Add7~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add7~20_combout\ = (\decimal_printer_inst|state.number\(11) & ((GND) # (!\decimal_printer_inst|Add7~19\))) # (!\decimal_printer_inst|state.number\(11) & (\decimal_printer_inst|Add7~19\ $ (GND)))
-- \decimal_printer_inst|Add7~21\ = CARRY((\decimal_printer_inst|state.number\(11)) # (!\decimal_printer_inst|Add7~19\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(11),
	datad => VCC,
	cin => \decimal_printer_inst|Add7~19\,
	combout => \decimal_printer_inst|Add7~20_combout\,
	cout => \decimal_printer_inst|Add7~21\);

-- Location: LCCOMB_X28_Y68_N24
\decimal_printer_inst|Add7~22\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add7~22_combout\ = (\decimal_printer_inst|state.number\(12) & (\decimal_printer_inst|Add7~21\ & VCC)) # (!\decimal_printer_inst|state.number\(12) & (!\decimal_printer_inst|Add7~21\))
-- \decimal_printer_inst|Add7~23\ = CARRY((!\decimal_printer_inst|state.number\(12) & !\decimal_printer_inst|Add7~21\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.number\(12),
	datad => VCC,
	cin => \decimal_printer_inst|Add7~21\,
	combout => \decimal_printer_inst|Add7~22_combout\,
	cout => \decimal_printer_inst|Add7~23\);

-- Location: LCCOMB_X29_Y70_N18
\decimal_printer_inst|Add5~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add5~16_combout\ = (\decimal_printer_inst|state.number\(10) & ((GND) # (!\decimal_printer_inst|Add5~15\))) # (!\decimal_printer_inst|state.number\(10) & (\decimal_printer_inst|Add5~15\ $ (GND)))
-- \decimal_printer_inst|Add5~17\ = CARRY((\decimal_printer_inst|state.number\(10)) # (!\decimal_printer_inst|Add5~15\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.number\(10),
	datad => VCC,
	cin => \decimal_printer_inst|Add5~15\,
	combout => \decimal_printer_inst|Add5~16_combout\,
	cout => \decimal_printer_inst|Add5~17\);

-- Location: LCCOMB_X29_Y70_N20
\decimal_printer_inst|Add5~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add5~18_combout\ = (\decimal_printer_inst|state.number\(11) & (\decimal_printer_inst|Add5~17\ & VCC)) # (!\decimal_printer_inst|state.number\(11) & (!\decimal_printer_inst|Add5~17\))
-- \decimal_printer_inst|Add5~19\ = CARRY((!\decimal_printer_inst|state.number\(11) & !\decimal_printer_inst|Add5~17\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(11),
	datad => VCC,
	cin => \decimal_printer_inst|Add5~17\,
	combout => \decimal_printer_inst|Add5~18_combout\,
	cout => \decimal_printer_inst|Add5~19\);

-- Location: LCCOMB_X29_Y70_N22
\decimal_printer_inst|Add5~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add5~20_combout\ = (\decimal_printer_inst|state.number\(12) & ((GND) # (!\decimal_printer_inst|Add5~19\))) # (!\decimal_printer_inst|state.number\(12) & (\decimal_printer_inst|Add5~19\ $ (GND)))
-- \decimal_printer_inst|Add5~21\ = CARRY((\decimal_printer_inst|state.number\(12)) # (!\decimal_printer_inst|Add5~19\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(12),
	datad => VCC,
	cin => \decimal_printer_inst|Add5~19\,
	combout => \decimal_printer_inst|Add5~20_combout\,
	cout => \decimal_printer_inst|Add5~21\);

-- Location: LCCOMB_X28_Y69_N16
\decimal_printer_inst|Selector8~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector8~0_combout\ = (\decimal_printer_inst|state.number[15]~5_combout\ & (((\decimal_printer_inst|Add7~22_combout\)) # (!\decimal_printer_inst|state_nxt~8_combout\))) # (!\decimal_printer_inst|state.number[15]~5_combout\ & 
-- (\decimal_printer_inst|state_nxt~8_combout\ & ((\decimal_printer_inst|Add5~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number[15]~5_combout\,
	datab => \decimal_printer_inst|state_nxt~8_combout\,
	datac => \decimal_printer_inst|Add7~22_combout\,
	datad => \decimal_printer_inst|Add5~20_combout\,
	combout => \decimal_printer_inst|Selector8~0_combout\);

-- Location: LCCOMB_X27_Y70_N16
\decimal_printer_inst|Add1~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add1~12_combout\ = (\decimal_printer_inst|state.number\(10) & (\decimal_printer_inst|Add1~11\ $ (GND))) # (!\decimal_printer_inst|state.number\(10) & (!\decimal_printer_inst|Add1~11\ & VCC))
-- \decimal_printer_inst|Add1~13\ = CARRY((\decimal_printer_inst|state.number\(10) & !\decimal_printer_inst|Add1~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(10),
	datad => VCC,
	cin => \decimal_printer_inst|Add1~11\,
	combout => \decimal_printer_inst|Add1~12_combout\,
	cout => \decimal_printer_inst|Add1~13\);

-- Location: LCCOMB_X27_Y70_N18
\decimal_printer_inst|Add1~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add1~14_combout\ = (\decimal_printer_inst|state.number\(11) & (\decimal_printer_inst|Add1~13\ & VCC)) # (!\decimal_printer_inst|state.number\(11) & (!\decimal_printer_inst|Add1~13\))
-- \decimal_printer_inst|Add1~15\ = CARRY((!\decimal_printer_inst|state.number\(11) & !\decimal_printer_inst|Add1~13\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(11),
	datad => VCC,
	cin => \decimal_printer_inst|Add1~13\,
	combout => \decimal_printer_inst|Add1~14_combout\,
	cout => \decimal_printer_inst|Add1~15\);

-- Location: LCCOMB_X27_Y70_N20
\decimal_printer_inst|Add1~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add1~16_combout\ = (\decimal_printer_inst|state.number\(12) & ((GND) # (!\decimal_printer_inst|Add1~15\))) # (!\decimal_printer_inst|state.number\(12) & (\decimal_printer_inst|Add1~15\ $ (GND)))
-- \decimal_printer_inst|Add1~17\ = CARRY((\decimal_printer_inst|state.number\(12)) # (!\decimal_printer_inst|Add1~15\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.number\(12),
	datad => VCC,
	cin => \decimal_printer_inst|Add1~15\,
	combout => \decimal_printer_inst|Add1~16_combout\,
	cout => \decimal_printer_inst|Add1~17\);

-- Location: LCCOMB_X28_Y69_N10
\decimal_printer_inst|Selector8~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector8~1_combout\ = (\decimal_printer_inst|Selector8~0_combout\ & ((\decimal_printer_inst|Add3~18_combout\) # ((\decimal_printer_inst|state_nxt~8_combout\)))) # (!\decimal_printer_inst|Selector8~0_combout\ & 
-- (((\decimal_printer_inst|Add1~16_combout\ & !\decimal_printer_inst|state_nxt~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Add3~18_combout\,
	datab => \decimal_printer_inst|Selector8~0_combout\,
	datac => \decimal_printer_inst|Add1~16_combout\,
	datad => \decimal_printer_inst|state_nxt~8_combout\,
	combout => \decimal_printer_inst|Selector8~1_combout\);

-- Location: LCCOMB_X28_Y70_N16
\decimal_printer_inst|state.number[12]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.number[12]~feeder_combout\ = \decimal_printer_inst|Selector8~1_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|Selector8~1_combout\,
	combout => \decimal_printer_inst|state.number[12]~feeder_combout\);

-- Location: IOIBUF_X20_Y73_N8
\number[12]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_number(12),
	o => \number[12]~input_o\);

-- Location: FF_X28_Y70_N17
\decimal_printer_inst|state.number[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|state.number[12]~feeder_combout\,
	asdata => \number[12]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \decimal_printer_inst|ALT_INV_state.fsm_state.CALC_DIGITS~q\,
	ena => \decimal_printer_inst|state.number[15]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.number\(12));

-- Location: LCCOMB_X27_Y69_N26
\decimal_printer_inst|Add3~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add3~20_combout\ = (\decimal_printer_inst|state.number\(13) & ((GND) # (!\decimal_printer_inst|Add3~19\))) # (!\decimal_printer_inst|state.number\(13) & (\decimal_printer_inst|Add3~19\ $ (GND)))
-- \decimal_printer_inst|Add3~21\ = CARRY((\decimal_printer_inst|state.number\(13)) # (!\decimal_printer_inst|Add3~19\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(13),
	datad => VCC,
	cin => \decimal_printer_inst|Add3~19\,
	combout => \decimal_printer_inst|Add3~20_combout\,
	cout => \decimal_printer_inst|Add3~21\);

-- Location: LCCOMB_X27_Y70_N22
\decimal_printer_inst|Add1~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add1~18_combout\ = (\decimal_printer_inst|state.number\(13) & (!\decimal_printer_inst|Add1~17\)) # (!\decimal_printer_inst|state.number\(13) & ((\decimal_printer_inst|Add1~17\) # (GND)))
-- \decimal_printer_inst|Add1~19\ = CARRY((!\decimal_printer_inst|Add1~17\) # (!\decimal_printer_inst|state.number\(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(13),
	datad => VCC,
	cin => \decimal_printer_inst|Add1~17\,
	combout => \decimal_printer_inst|Add1~18_combout\,
	cout => \decimal_printer_inst|Add1~19\);

-- Location: LCCOMB_X29_Y70_N24
\decimal_printer_inst|Add5~22\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add5~22_combout\ = (\decimal_printer_inst|state.number\(13) & (\decimal_printer_inst|Add5~21\ & VCC)) # (!\decimal_printer_inst|state.number\(13) & (!\decimal_printer_inst|Add5~21\))
-- \decimal_printer_inst|Add5~23\ = CARRY((!\decimal_printer_inst|state.number\(13) & !\decimal_printer_inst|Add5~21\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.number\(13),
	datad => VCC,
	cin => \decimal_printer_inst|Add5~21\,
	combout => \decimal_printer_inst|Add5~22_combout\,
	cout => \decimal_printer_inst|Add5~23\);

-- Location: LCCOMB_X28_Y70_N0
\decimal_printer_inst|Selector7~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector7~0_combout\ = (\decimal_printer_inst|state_nxt~8_combout\ & (((!\decimal_printer_inst|state.number[15]~5_combout\ & \decimal_printer_inst|Add5~22_combout\)))) # (!\decimal_printer_inst|state_nxt~8_combout\ & 
-- ((\decimal_printer_inst|Add1~18_combout\) # ((\decimal_printer_inst|state.number[15]~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111001010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state_nxt~8_combout\,
	datab => \decimal_printer_inst|Add1~18_combout\,
	datac => \decimal_printer_inst|state.number[15]~5_combout\,
	datad => \decimal_printer_inst|Add5~22_combout\,
	combout => \decimal_printer_inst|Selector7~0_combout\);

-- Location: LCCOMB_X28_Y68_N26
\decimal_printer_inst|Add7~24\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add7~24_combout\ = (\decimal_printer_inst|state.number\(13) & ((GND) # (!\decimal_printer_inst|Add7~23\))) # (!\decimal_printer_inst|state.number\(13) & (\decimal_printer_inst|Add7~23\ $ (GND)))
-- \decimal_printer_inst|Add7~25\ = CARRY((\decimal_printer_inst|state.number\(13)) # (!\decimal_printer_inst|Add7~23\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.number\(13),
	datad => VCC,
	cin => \decimal_printer_inst|Add7~23\,
	combout => \decimal_printer_inst|Add7~24_combout\,
	cout => \decimal_printer_inst|Add7~25\);

-- Location: LCCOMB_X28_Y70_N18
\decimal_printer_inst|Selector7~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector7~1_combout\ = (\decimal_printer_inst|Selector7~0_combout\ & ((\decimal_printer_inst|Add3~20_combout\) # ((!\decimal_printer_inst|state.number[15]~5_combout\)))) # (!\decimal_printer_inst|Selector7~0_combout\ & 
-- (((\decimal_printer_inst|state.number[15]~5_combout\ & \decimal_printer_inst|Add7~24_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Add3~20_combout\,
	datab => \decimal_printer_inst|Selector7~0_combout\,
	datac => \decimal_printer_inst|state.number[15]~5_combout\,
	datad => \decimal_printer_inst|Add7~24_combout\,
	combout => \decimal_printer_inst|Selector7~1_combout\);

-- Location: LCCOMB_X28_Y70_N20
\decimal_printer_inst|state.number[13]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.number[13]~feeder_combout\ = \decimal_printer_inst|Selector7~1_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \decimal_printer_inst|Selector7~1_combout\,
	combout => \decimal_printer_inst|state.number[13]~feeder_combout\);

-- Location: IOIBUF_X31_Y73_N1
\number[13]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_number(13),
	o => \number[13]~input_o\);

-- Location: FF_X28_Y70_N21
\decimal_printer_inst|state.number[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|state.number[13]~feeder_combout\,
	asdata => \number[13]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \decimal_printer_inst|ALT_INV_state.fsm_state.CALC_DIGITS~q\,
	ena => \decimal_printer_inst|state.number[15]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.number\(13));

-- Location: LCCOMB_X27_Y69_N28
\decimal_printer_inst|Add3~22\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add3~22_combout\ = (\decimal_printer_inst|state.number\(14) & (\decimal_printer_inst|Add3~21\ & VCC)) # (!\decimal_printer_inst|state.number\(14) & (!\decimal_printer_inst|Add3~21\))
-- \decimal_printer_inst|Add3~23\ = CARRY((!\decimal_printer_inst|state.number\(14) & !\decimal_printer_inst|Add3~21\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.number\(14),
	datad => VCC,
	cin => \decimal_printer_inst|Add3~21\,
	combout => \decimal_printer_inst|Add3~22_combout\,
	cout => \decimal_printer_inst|Add3~23\);

-- Location: LCCOMB_X28_Y68_N28
\decimal_printer_inst|Add7~26\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add7~26_combout\ = (\decimal_printer_inst|state.number\(14) & (\decimal_printer_inst|Add7~25\ & VCC)) # (!\decimal_printer_inst|state.number\(14) & (!\decimal_printer_inst|Add7~25\))
-- \decimal_printer_inst|Add7~27\ = CARRY((!\decimal_printer_inst|state.number\(14) & !\decimal_printer_inst|Add7~25\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.number\(14),
	datad => VCC,
	cin => \decimal_printer_inst|Add7~25\,
	combout => \decimal_printer_inst|Add7~26_combout\,
	cout => \decimal_printer_inst|Add7~27\);

-- Location: LCCOMB_X29_Y70_N26
\decimal_printer_inst|Add5~24\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add5~24_combout\ = (\decimal_printer_inst|state.number\(14) & ((GND) # (!\decimal_printer_inst|Add5~23\))) # (!\decimal_printer_inst|state.number\(14) & (\decimal_printer_inst|Add5~23\ $ (GND)))
-- \decimal_printer_inst|Add5~25\ = CARRY((\decimal_printer_inst|state.number\(14)) # (!\decimal_printer_inst|Add5~23\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.number\(14),
	datad => VCC,
	cin => \decimal_printer_inst|Add5~23\,
	combout => \decimal_printer_inst|Add5~24_combout\,
	cout => \decimal_printer_inst|Add5~25\);

-- Location: LCCOMB_X28_Y69_N20
\decimal_printer_inst|Selector6~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector6~0_combout\ = (\decimal_printer_inst|state.number[15]~5_combout\ & (((\decimal_printer_inst|Add7~26_combout\)) # (!\decimal_printer_inst|state_nxt~8_combout\))) # (!\decimal_printer_inst|state.number[15]~5_combout\ & 
-- (\decimal_printer_inst|state_nxt~8_combout\ & ((\decimal_printer_inst|Add5~24_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number[15]~5_combout\,
	datab => \decimal_printer_inst|state_nxt~8_combout\,
	datac => \decimal_printer_inst|Add7~26_combout\,
	datad => \decimal_printer_inst|Add5~24_combout\,
	combout => \decimal_printer_inst|Selector6~0_combout\);

-- Location: LCCOMB_X27_Y70_N24
\decimal_printer_inst|Add1~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add1~20_combout\ = (\decimal_printer_inst|state.number\(14) & ((GND) # (!\decimal_printer_inst|Add1~19\))) # (!\decimal_printer_inst|state.number\(14) & (\decimal_printer_inst|Add1~19\ $ (GND)))
-- \decimal_printer_inst|Add1~21\ = CARRY((\decimal_printer_inst|state.number\(14)) # (!\decimal_printer_inst|Add1~19\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(14),
	datad => VCC,
	cin => \decimal_printer_inst|Add1~19\,
	combout => \decimal_printer_inst|Add1~20_combout\,
	cout => \decimal_printer_inst|Add1~21\);

-- Location: LCCOMB_X28_Y69_N30
\decimal_printer_inst|Selector6~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector6~1_combout\ = (\decimal_printer_inst|Selector6~0_combout\ & ((\decimal_printer_inst|Add3~22_combout\) # ((\decimal_printer_inst|state_nxt~8_combout\)))) # (!\decimal_printer_inst|Selector6~0_combout\ & 
-- (((\decimal_printer_inst|Add1~20_combout\ & !\decimal_printer_inst|state_nxt~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Add3~22_combout\,
	datab => \decimal_printer_inst|Selector6~0_combout\,
	datac => \decimal_printer_inst|Add1~20_combout\,
	datad => \decimal_printer_inst|state_nxt~8_combout\,
	combout => \decimal_printer_inst|Selector6~1_combout\);

-- Location: LCCOMB_X28_Y69_N0
\decimal_printer_inst|state.number[14]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.number[14]~feeder_combout\ = \decimal_printer_inst|Selector6~1_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Selector6~1_combout\,
	combout => \decimal_printer_inst|state.number[14]~feeder_combout\);

-- Location: IOIBUF_X20_Y73_N22
\number[14]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_number(14),
	o => \number[14]~input_o\);

-- Location: FF_X28_Y69_N1
\decimal_printer_inst|state.number[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|state.number[14]~feeder_combout\,
	asdata => \number[14]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \decimal_printer_inst|ALT_INV_state.fsm_state.CALC_DIGITS~q\,
	ena => \decimal_printer_inst|state.number[15]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.number\(14));

-- Location: IOIBUF_X35_Y73_N15
\number[15]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_number(15),
	o => \number[15]~input_o\);

-- Location: LCCOMB_X28_Y69_N2
\decimal_printer_inst|state.number[15]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.number[15]~7_combout\ = (!\decimal_printer_inst|state.fsm_state.IDLE~q\ & (\number[15]~input_o\ & !\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.fsm_state.IDLE~q\,
	datab => \number[15]~input_o\,
	datad => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	combout => \decimal_printer_inst|state.number[15]~7_combout\);

-- Location: LCCOMB_X28_Y68_N30
\decimal_printer_inst|Add7~28\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add7~28_combout\ = \decimal_printer_inst|state.number\(15) $ (\decimal_printer_inst|Add7~27\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(15),
	cin => \decimal_printer_inst|Add7~27\,
	combout => \decimal_printer_inst|Add7~28_combout\);

-- Location: LCCOMB_X29_Y70_N28
\decimal_printer_inst|Add5~26\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add5~26_combout\ = \decimal_printer_inst|state.number\(15) $ (!\decimal_printer_inst|Add5~25\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001111000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.number\(15),
	cin => \decimal_printer_inst|Add5~25\,
	combout => \decimal_printer_inst|Add5~26_combout\);

-- Location: LCCOMB_X29_Y69_N8
\decimal_printer_inst|state.number[15]~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.number[15]~8_combout\ = (\decimal_printer_inst|state.number[15]~5_combout\ & ((\decimal_printer_inst|Add7~28_combout\) # ((!\decimal_printer_inst|state_nxt~8_combout\)))) # (!\decimal_printer_inst|state.number[15]~5_combout\ & 
-- (((\decimal_printer_inst|state_nxt~8_combout\ & \decimal_printer_inst|Add5~26_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101010001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number[15]~5_combout\,
	datab => \decimal_printer_inst|Add7~28_combout\,
	datac => \decimal_printer_inst|state_nxt~8_combout\,
	datad => \decimal_printer_inst|Add5~26_combout\,
	combout => \decimal_printer_inst|state.number[15]~8_combout\);

-- Location: LCCOMB_X27_Y69_N30
\decimal_printer_inst|Add3~24\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add3~24_combout\ = \decimal_printer_inst|state.number\(15) $ (\decimal_printer_inst|Add3~23\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.number\(15),
	cin => \decimal_printer_inst|Add3~23\,
	combout => \decimal_printer_inst|Add3~24_combout\);

-- Location: LCCOMB_X27_Y70_N26
\decimal_printer_inst|Add1~22\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add1~22_combout\ = \decimal_printer_inst|Add1~21\ $ (!\decimal_printer_inst|state.number\(15))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \decimal_printer_inst|state.number\(15),
	cin => \decimal_printer_inst|Add1~21\,
	combout => \decimal_printer_inst|Add1~22_combout\);

-- Location: LCCOMB_X29_Y69_N10
\decimal_printer_inst|state.number[15]~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.number[15]~9_combout\ = (\decimal_printer_inst|state.number[15]~8_combout\ & (\decimal_printer_inst|Add3~24_combout\)) # (!\decimal_printer_inst|state.number[15]~8_combout\ & ((\decimal_printer_inst|Add1~22_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Add3~24_combout\,
	datab => \decimal_printer_inst|state.number[15]~8_combout\,
	datac => \decimal_printer_inst|Add1~22_combout\,
	combout => \decimal_printer_inst|state.number[15]~9_combout\);

-- Location: LCCOMB_X29_Y69_N28
\decimal_printer_inst|state.number[15]~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.number[15]~10_combout\ = (\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & ((\decimal_printer_inst|state_nxt~8_combout\ & (\decimal_printer_inst|state.number[15]~8_combout\)) # (!\decimal_printer_inst|state_nxt~8_combout\ 
-- & ((\decimal_printer_inst|state.number[15]~9_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state_nxt~8_combout\,
	datab => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	datac => \decimal_printer_inst|state.number[15]~8_combout\,
	datad => \decimal_printer_inst|state.number[15]~9_combout\,
	combout => \decimal_printer_inst|state.number[15]~10_combout\);

-- Location: LCCOMB_X29_Y69_N4
\decimal_printer_inst|state.number[15]~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.number[15]~11_combout\ = (\decimal_printer_inst|state.number[15]~7_combout\) # ((\decimal_printer_inst|state.number[15]~6_combout\ & ((\decimal_printer_inst|state.number[15]~10_combout\))) # 
-- (!\decimal_printer_inst|state.number[15]~6_combout\ & (\decimal_printer_inst|state.number\(15))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111010111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number[15]~7_combout\,
	datab => \decimal_printer_inst|state.number[15]~6_combout\,
	datac => \decimal_printer_inst|state.number\(15),
	datad => \decimal_printer_inst|state.number[15]~10_combout\,
	combout => \decimal_printer_inst|state.number[15]~11_combout\);

-- Location: FF_X29_Y69_N5
\decimal_printer_inst|state.number[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|state.number[15]~11_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.number\(15));

-- Location: LCCOMB_X27_Y70_N0
\decimal_printer_inst|state_nxt~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state_nxt~5_combout\ = (!\decimal_printer_inst|state.number\(14) & !\decimal_printer_inst|state.number\(15))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \decimal_printer_inst|state.number\(14),
	datad => \decimal_printer_inst|state.number\(15),
	combout => \decimal_printer_inst|state_nxt~5_combout\);

-- Location: LCCOMB_X27_Y70_N28
\decimal_printer_inst|state_nxt~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state_nxt~6_combout\ = (!\decimal_printer_inst|state.number\(10) & (\decimal_printer_inst|state_nxt~5_combout\ & (!\decimal_printer_inst|state.number\(13) & \decimal_printer_inst|LessThan0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(10),
	datab => \decimal_printer_inst|state_nxt~5_combout\,
	datac => \decimal_printer_inst|state.number\(13),
	datad => \decimal_printer_inst|LessThan0~0_combout\,
	combout => \decimal_printer_inst|state_nxt~6_combout\);

-- Location: LCCOMB_X28_Y71_N20
\decimal_printer_inst|state_nxt~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state_nxt~8_combout\ = (\decimal_printer_inst|state_nxt~6_combout\ & \decimal_printer_inst|LessThan1~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state_nxt~6_combout\,
	datac => \decimal_printer_inst|LessThan1~1_combout\,
	combout => \decimal_printer_inst|state_nxt~8_combout\);

-- Location: LCCOMB_X27_Y69_N20
\decimal_printer_inst|Add3~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add3~14_combout\ = (\decimal_printer_inst|state.number\(10) & (\decimal_printer_inst|Add3~13\ & VCC)) # (!\decimal_printer_inst|state.number\(10) & (!\decimal_printer_inst|Add3~13\))
-- \decimal_printer_inst|Add3~15\ = CARRY((!\decimal_printer_inst|state.number\(10) & !\decimal_printer_inst|Add3~13\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(10),
	datad => VCC,
	cin => \decimal_printer_inst|Add3~13\,
	combout => \decimal_printer_inst|Add3~14_combout\,
	cout => \decimal_printer_inst|Add3~15\);

-- Location: LCCOMB_X29_Y70_N0
\decimal_printer_inst|Selector10~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector10~0_combout\ = (\decimal_printer_inst|state.number[15]~5_combout\ & (((\decimal_printer_inst|Add7~18_combout\)) # (!\decimal_printer_inst|state_nxt~8_combout\))) # (!\decimal_printer_inst|state.number[15]~5_combout\ & 
-- (\decimal_printer_inst|state_nxt~8_combout\ & ((\decimal_printer_inst|Add5~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number[15]~5_combout\,
	datab => \decimal_printer_inst|state_nxt~8_combout\,
	datac => \decimal_printer_inst|Add7~18_combout\,
	datad => \decimal_printer_inst|Add5~16_combout\,
	combout => \decimal_printer_inst|Selector10~0_combout\);

-- Location: LCCOMB_X28_Y70_N12
\decimal_printer_inst|Selector10~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector10~1_combout\ = (\decimal_printer_inst|state_nxt~8_combout\ & (((\decimal_printer_inst|Selector10~0_combout\)))) # (!\decimal_printer_inst|state_nxt~8_combout\ & ((\decimal_printer_inst|Selector10~0_combout\ & 
-- (\decimal_printer_inst|Add3~14_combout\)) # (!\decimal_printer_inst|Selector10~0_combout\ & ((\decimal_printer_inst|Add1~12_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state_nxt~8_combout\,
	datab => \decimal_printer_inst|Add3~14_combout\,
	datac => \decimal_printer_inst|Selector10~0_combout\,
	datad => \decimal_printer_inst|Add1~12_combout\,
	combout => \decimal_printer_inst|Selector10~1_combout\);

-- Location: LCCOMB_X28_Y70_N6
\decimal_printer_inst|state.number[10]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.number[10]~feeder_combout\ = \decimal_printer_inst|Selector10~1_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \decimal_printer_inst|Selector10~1_combout\,
	combout => \decimal_printer_inst|state.number[10]~feeder_combout\);

-- Location: IOIBUF_X27_Y73_N22
\number[10]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_number(10),
	o => \number[10]~input_o\);

-- Location: FF_X28_Y70_N7
\decimal_printer_inst|state.number[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|state.number[10]~feeder_combout\,
	asdata => \number[10]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \decimal_printer_inst|ALT_INV_state.fsm_state.CALC_DIGITS~q\,
	ena => \decimal_printer_inst|state.number[15]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.number\(10));

-- Location: LCCOMB_X28_Y70_N28
\decimal_printer_inst|Selector9~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector9~0_combout\ = (\decimal_printer_inst|state_nxt~8_combout\ & (!\decimal_printer_inst|state.number[15]~5_combout\ & (\decimal_printer_inst|Add5~18_combout\))) # (!\decimal_printer_inst|state_nxt~8_combout\ & 
-- ((\decimal_printer_inst|state.number[15]~5_combout\) # ((\decimal_printer_inst|Add1~14_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010101100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state_nxt~8_combout\,
	datab => \decimal_printer_inst|state.number[15]~5_combout\,
	datac => \decimal_printer_inst|Add5~18_combout\,
	datad => \decimal_printer_inst|Add1~14_combout\,
	combout => \decimal_printer_inst|Selector9~0_combout\);

-- Location: LCCOMB_X28_Y70_N30
\decimal_printer_inst|Selector9~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector9~1_combout\ = (\decimal_printer_inst|Selector9~0_combout\ & ((\decimal_printer_inst|Add3~16_combout\) # ((!\decimal_printer_inst|state.number[15]~5_combout\)))) # (!\decimal_printer_inst|Selector9~0_combout\ & 
-- (((\decimal_printer_inst|state.number[15]~5_combout\ & \decimal_printer_inst|Add7~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Add3~16_combout\,
	datab => \decimal_printer_inst|Selector9~0_combout\,
	datac => \decimal_printer_inst|state.number[15]~5_combout\,
	datad => \decimal_printer_inst|Add7~20_combout\,
	combout => \decimal_printer_inst|Selector9~1_combout\);

-- Location: LCCOMB_X28_Y70_N2
\decimal_printer_inst|state.number[11]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.number[11]~feeder_combout\ = \decimal_printer_inst|Selector9~1_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Selector9~1_combout\,
	combout => \decimal_printer_inst|state.number[11]~feeder_combout\);

-- Location: IOIBUF_X20_Y73_N1
\number[11]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_number(11),
	o => \number[11]~input_o\);

-- Location: FF_X28_Y70_N3
\decimal_printer_inst|state.number[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|state.number[11]~feeder_combout\,
	asdata => \number[11]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \decimal_printer_inst|ALT_INV_state.fsm_state.CALC_DIGITS~q\,
	ena => \decimal_printer_inst|state.number[15]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.number\(11));

-- Location: LCCOMB_X27_Y70_N2
\decimal_printer_inst|LessThan0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|LessThan0~0_combout\ = (!\decimal_printer_inst|state.number\(11) & !\decimal_printer_inst|state.number\(12))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(11),
	datad => \decimal_printer_inst|state.number\(12),
	combout => \decimal_printer_inst|LessThan0~0_combout\);

-- Location: LCCOMB_X28_Y70_N24
\decimal_printer_inst|LessThan0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|LessThan0~1_combout\ = ((\decimal_printer_inst|LessThan3~1_combout\) # ((!\decimal_printer_inst|state.number\(10)) # (!\decimal_printer_inst|state.number\(9)))) # (!\decimal_printer_inst|state.number\(8))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number\(8),
	datab => \decimal_printer_inst|LessThan3~1_combout\,
	datac => \decimal_printer_inst|state.number\(9),
	datad => \decimal_printer_inst|state.number\(10),
	combout => \decimal_printer_inst|LessThan0~1_combout\);

-- Location: LCCOMB_X27_Y70_N30
\decimal_printer_inst|state.number[15]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.number[15]~4_combout\ = (\decimal_printer_inst|state_nxt~5_combout\ & (((\decimal_printer_inst|LessThan0~0_combout\ & \decimal_printer_inst|LessThan0~1_combout\)) # (!\decimal_printer_inst|state.number\(13))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|LessThan0~0_combout\,
	datab => \decimal_printer_inst|state_nxt~5_combout\,
	datac => \decimal_printer_inst|state.number\(13),
	datad => \decimal_printer_inst|LessThan0~1_combout\,
	combout => \decimal_printer_inst|state.number[15]~4_combout\);

-- Location: LCCOMB_X25_Y70_N2
\decimal_printer_inst|Selector2~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector2~1_combout\ = (\decimal_printer_inst|state.number[15]~4_combout\ & (\decimal_printer_inst|state.number[15]~3_combout\ & (\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & \decimal_printer_inst|LessThan3~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number[15]~4_combout\,
	datab => \decimal_printer_inst|state.number[15]~3_combout\,
	datac => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	datad => \decimal_printer_inst|LessThan3~3_combout\,
	combout => \decimal_printer_inst|Selector2~1_combout\);

-- Location: LCCOMB_X28_Y70_N4
\decimal_printer_inst|gfx_cmd[2]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|gfx_cmd[2]~3_combout\ = (\gfx_cmd_full~input_o\) # (!\decimal_printer_inst|state.fsm_state.BB_CHAR~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111110101111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gfx_cmd_full~input_o\,
	datac => \decimal_printer_inst|state.fsm_state.BB_CHAR~q\,
	combout => \decimal_printer_inst|gfx_cmd[2]~3_combout\);

-- Location: LCCOMB_X25_Y70_N12
\decimal_printer_inst|Selector4~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector4~0_combout\ = (\start~input_o\ & !\decimal_printer_inst|state.fsm_state.IDLE~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \start~input_o\,
	datac => \decimal_printer_inst|state.fsm_state.IDLE~q\,
	combout => \decimal_printer_inst|Selector4~0_combout\);

-- Location: LCCOMB_X26_Y70_N18
\decimal_printer_inst|Selector3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector3~0_combout\ = (\gfx_cmd_full~input_o\ & (\decimal_printer_inst|state.fsm_state.BB_CHAR_ARG~q\ & (!\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\ & !\decimal_printer_inst|Selector4~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gfx_cmd_full~input_o\,
	datab => \decimal_printer_inst|state.fsm_state.BB_CHAR_ARG~q\,
	datac => \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\,
	datad => \decimal_printer_inst|Selector4~0_combout\,
	combout => \decimal_printer_inst|Selector3~0_combout\);

-- Location: LCCOMB_X26_Y70_N20
\decimal_printer_inst|Selector3~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector3~1_combout\ = (\decimal_printer_inst|Selector2~1_combout\ & (!\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\ & (!\decimal_printer_inst|gfx_cmd[2]~3_combout\))) # (!\decimal_printer_inst|Selector2~1_combout\ & 
-- ((\decimal_printer_inst|Selector3~0_combout\) # ((!\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\ & !\decimal_printer_inst|gfx_cmd[2]~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101011100000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Selector2~1_combout\,
	datab => \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\,
	datac => \decimal_printer_inst|gfx_cmd[2]~3_combout\,
	datad => \decimal_printer_inst|Selector3~0_combout\,
	combout => \decimal_printer_inst|Selector3~1_combout\);

-- Location: FF_X26_Y70_N21
\decimal_printer_inst|state.fsm_state.BB_CHAR_ARG\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|Selector3~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.fsm_state.BB_CHAR_ARG~q\);

-- Location: LCCOMB_X23_Y70_N4
\decimal_printer_inst|Selector4~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector4~1_combout\ = (!\gfx_cmd_full~input_o\ & (!\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\ & \decimal_printer_inst|state.fsm_state.BB_CHAR_ARG~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gfx_cmd_full~input_o\,
	datac => \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\,
	datad => \decimal_printer_inst|state.fsm_state.BB_CHAR_ARG~q\,
	combout => \decimal_printer_inst|Selector4~1_combout\);

-- Location: FF_X23_Y70_N5
\decimal_printer_inst|state.fsm_state.DIGIT_DONE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|Selector4~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\);

-- Location: LCCOMB_X26_Y70_N10
\decimal_printer_inst|Selector42~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector42~1_combout\ = (\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\ & (!\decimal_printer_inst|Selector42~0_combout\ & (!\decimal_printer_inst|state.digit_cnt\(0)))) # (!\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\ & 
-- (((\decimal_printer_inst|state.digit_cnt\(0) & \decimal_printer_inst|state.fsm_state.IDLE~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011010000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Selector42~0_combout\,
	datab => \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\,
	datac => \decimal_printer_inst|state.digit_cnt\(0),
	datad => \decimal_printer_inst|state.fsm_state.IDLE~q\,
	combout => \decimal_printer_inst|Selector42~1_combout\);

-- Location: FF_X26_Y70_N11
\decimal_printer_inst|state.digit_cnt[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|Selector42~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.digit_cnt\(0));

-- Location: LCCOMB_X26_Y70_N12
\decimal_printer_inst|Selector41~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector41~0_combout\ = (\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\ & (\decimal_printer_inst|state.digit_cnt\(0) $ ((\decimal_printer_inst|state.digit_cnt\(1))))) # (!\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\ & 
-- (((\decimal_printer_inst|state.digit_cnt\(1) & \decimal_printer_inst|state.fsm_state.IDLE~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100001001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.digit_cnt\(0),
	datab => \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\,
	datac => \decimal_printer_inst|state.digit_cnt\(1),
	datad => \decimal_printer_inst|state.fsm_state.IDLE~q\,
	combout => \decimal_printer_inst|Selector41~0_combout\);

-- Location: FF_X26_Y70_N13
\decimal_printer_inst|state.digit_cnt[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|Selector41~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.digit_cnt\(1));

-- Location: LCCOMB_X26_Y70_N14
\decimal_printer_inst|Selector40~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector40~0_combout\ = \decimal_printer_inst|state.digit_cnt\(2) $ (\decimal_printer_inst|state.digit_cnt\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.digit_cnt\(2),
	datad => \decimal_printer_inst|state.digit_cnt\(0),
	combout => \decimal_printer_inst|Selector40~0_combout\);

-- Location: LCCOMB_X26_Y70_N16
\decimal_printer_inst|state.digit_cnt[2]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.digit_cnt[2]~0_combout\ = (\decimal_printer_inst|state.digit_cnt\(1) & (\decimal_printer_inst|Selector40~0_combout\)) # (!\decimal_printer_inst|state.digit_cnt\(1) & ((\decimal_printer_inst|state.digit_cnt\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.digit_cnt\(1),
	datab => \decimal_printer_inst|Selector40~0_combout\,
	datac => \decimal_printer_inst|state.digit_cnt\(2),
	combout => \decimal_printer_inst|state.digit_cnt[2]~0_combout\);

-- Location: LCCOMB_X26_Y70_N8
\decimal_printer_inst|Selector40~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector40~1_combout\ = (\decimal_printer_inst|state.digit_cnt\(2) & \decimal_printer_inst|state.fsm_state.IDLE~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.digit_cnt\(2),
	datad => \decimal_printer_inst|state.fsm_state.IDLE~q\,
	combout => \decimal_printer_inst|Selector40~1_combout\);

-- Location: FF_X26_Y70_N17
\decimal_printer_inst|state.digit_cnt[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|state.digit_cnt[2]~0_combout\,
	asdata => \decimal_printer_inst|Selector40~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \decimal_printer_inst|ALT_INV_state.fsm_state.DIGIT_DONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.digit_cnt\(2));

-- Location: LCCOMB_X26_Y70_N30
\decimal_printer_inst|Selector42~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector42~0_combout\ = (\decimal_printer_inst|state.digit_cnt\(2) & !\decimal_printer_inst|state.digit_cnt\(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.digit_cnt\(2),
	datad => \decimal_printer_inst|state.digit_cnt\(1),
	combout => \decimal_printer_inst|Selector42~0_combout\);

-- Location: LCCOMB_X26_Y70_N24
\decimal_printer_inst|Selector2~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector2~2_combout\ = (!\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\ & ((\decimal_printer_inst|Selector2~1_combout\) # ((\gfx_cmd_full~input_o\ & \decimal_printer_inst|state.fsm_state.BB_CHAR~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gfx_cmd_full~input_o\,
	datab => \decimal_printer_inst|state.fsm_state.BB_CHAR~q\,
	datac => \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\,
	datad => \decimal_printer_inst|Selector2~1_combout\,
	combout => \decimal_printer_inst|Selector2~2_combout\);

-- Location: LCCOMB_X26_Y70_N2
\decimal_printer_inst|Selector2~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector2~3_combout\ = (\decimal_printer_inst|Selector2~2_combout\) # ((\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\ & ((\decimal_printer_inst|state.digit_cnt\(0)) # (!\decimal_printer_inst|Selector42~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Selector42~0_combout\,
	datab => \decimal_printer_inst|Selector2~2_combout\,
	datac => \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\,
	datad => \decimal_printer_inst|state.digit_cnt\(0),
	combout => \decimal_printer_inst|Selector2~3_combout\);

-- Location: FF_X26_Y70_N3
\decimal_printer_inst|state.fsm_state.BB_CHAR\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|Selector2~3_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.fsm_state.BB_CHAR~q\);

-- Location: IOIBUF_X38_Y73_N1
\bmpidx[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bmpidx(0),
	o => \bmpidx[0]~input_o\);

-- Location: LCCOMB_X32_Y71_N0
\decimal_printer_inst|gfx_cmd[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|gfx_cmd[0]~0_combout\ = (\decimal_printer_inst|state.fsm_state.BB_CHAR~q\ & (!\gfx_cmd_full~input_o\ & \bmpidx[0]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.fsm_state.BB_CHAR~q\,
	datac => \gfx_cmd_full~input_o\,
	datad => \bmpidx[0]~input_o\,
	combout => \decimal_printer_inst|gfx_cmd[0]~0_combout\);

-- Location: IOIBUF_X35_Y73_N22
\bmpidx[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bmpidx(1),
	o => \bmpidx[1]~input_o\);

-- Location: LCCOMB_X32_Y71_N26
\decimal_printer_inst|gfx_cmd[1]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|gfx_cmd[1]~1_combout\ = (\decimal_printer_inst|state.fsm_state.BB_CHAR~q\ & (!\gfx_cmd_full~input_o\ & \bmpidx[1]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.fsm_state.BB_CHAR~q\,
	datac => \gfx_cmd_full~input_o\,
	datad => \bmpidx[1]~input_o\,
	combout => \decimal_printer_inst|gfx_cmd[1]~1_combout\);

-- Location: IOIBUF_X38_Y73_N8
\bmpidx[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bmpidx(2),
	o => \bmpidx[2]~input_o\);

-- Location: LCCOMB_X32_Y71_N28
\decimal_printer_inst|gfx_cmd[2]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|gfx_cmd[2]~2_combout\ = (\bmpidx[2]~input_o\ & (\decimal_printer_inst|state.fsm_state.BB_CHAR~q\ & !\gfx_cmd_full~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \bmpidx[2]~input_o\,
	datab => \decimal_printer_inst|state.fsm_state.BB_CHAR~q\,
	datac => \gfx_cmd_full~input_o\,
	combout => \decimal_printer_inst|gfx_cmd[2]~2_combout\);

-- Location: LCCOMB_X23_Y70_N16
\decimal_printer_inst|Selector45~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector45~0_combout\ = (!\gfx_cmd_full~input_o\ & \decimal_printer_inst|state.fsm_state.BB_CHAR_ARG~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gfx_cmd_full~input_o\,
	datad => \decimal_printer_inst|state.fsm_state.BB_CHAR_ARG~q\,
	combout => \decimal_printer_inst|Selector45~0_combout\);

-- Location: LCCOMB_X25_Y70_N14
\decimal_printer_inst|WideOr2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|WideOr2~0_combout\ = (\decimal_printer_inst|state.fsm_state.BB_CHAR~q\) # (\decimal_printer_inst|state.fsm_state.BB_CHAR_ARG~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.fsm_state.BB_CHAR~q\,
	datac => \decimal_printer_inst|state.fsm_state.BB_CHAR_ARG~q\,
	combout => \decimal_printer_inst|WideOr2~0_combout\);

-- Location: IOIBUF_X25_Y73_N22
\number[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_number(0),
	o => \number[0]~input_o\);

-- Location: LCCOMB_X26_Y70_N4
\decimal_printer_inst|state.number[0]~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.number[0]~12_combout\ = (\decimal_printer_inst|state.fsm_state.IDLE~q\ & ((\decimal_printer_inst|state.number\(0)))) # (!\decimal_printer_inst|state.fsm_state.IDLE~q\ & (\number[0]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \number[0]~input_o\,
	datac => \decimal_printer_inst|state.number\(0),
	datad => \decimal_printer_inst|state.fsm_state.IDLE~q\,
	combout => \decimal_printer_inst|state.number[0]~12_combout\);

-- Location: FF_X26_Y70_N5
\decimal_printer_inst|state.number[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|state.number[0]~12_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.number\(0));

-- Location: LCCOMB_X26_Y70_N22
\decimal_printer_inst|Selector39~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector39~0_combout\ = (\decimal_printer_inst|Selector2~1_combout\ & \decimal_printer_inst|state.number\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Selector2~1_combout\,
	datac => \decimal_printer_inst|state.number\(0),
	combout => \decimal_printer_inst|Selector39~0_combout\);

-- Location: LCCOMB_X25_Y70_N16
\decimal_printer_inst|Selector39~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector39~1_combout\ = (\decimal_printer_inst|Selector39~0_combout\) # ((\decimal_printer_inst|state.bcd_data[0][0]~q\ & ((\decimal_printer_inst|WideOr2~0_combout\) # (\decimal_printer_inst|Selector1~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|WideOr2~0_combout\,
	datab => \decimal_printer_inst|Selector1~2_combout\,
	datac => \decimal_printer_inst|state.bcd_data[0][0]~q\,
	datad => \decimal_printer_inst|Selector39~0_combout\,
	combout => \decimal_printer_inst|Selector39~1_combout\);

-- Location: FF_X25_Y70_N17
\decimal_printer_inst|state.bcd_data[0][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|Selector39~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.bcd_data[0][0]~q\);

-- Location: LCCOMB_X25_Y70_N24
\decimal_printer_inst|Selector35~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector35~0_combout\ = (\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & (((\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\ & \decimal_printer_inst|state.bcd_data[0][0]~q\)) # 
-- (!\decimal_printer_inst|state.bcd_data[1][0]~q\))) # (!\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & (\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\ & ((\decimal_printer_inst|state.bcd_data[0][0]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	datab => \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\,
	datac => \decimal_printer_inst|state.bcd_data[1][0]~q\,
	datad => \decimal_printer_inst|state.bcd_data[0][0]~q\,
	combout => \decimal_printer_inst|Selector35~0_combout\);

-- Location: LCCOMB_X28_Y71_N14
\decimal_printer_inst|state.bcd_data[1][3]~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.bcd_data[1][3]~8_combout\ = (!\decimal_printer_inst|WideOr2~0_combout\ & (((\decimal_printer_inst|state_nxt~7_combout\ & !\decimal_printer_inst|LessThan3~3_combout\)) # (!\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010101000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|WideOr2~0_combout\,
	datab => \decimal_printer_inst|state_nxt~7_combout\,
	datac => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	datad => \decimal_printer_inst|LessThan3~3_combout\,
	combout => \decimal_printer_inst|state.bcd_data[1][3]~8_combout\);

-- Location: FF_X25_Y70_N25
\decimal_printer_inst|state.bcd_data[1][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|Selector35~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \decimal_printer_inst|state.bcd_data[1][3]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.bcd_data[1][0]~q\);

-- Location: LCCOMB_X24_Y70_N8
\decimal_printer_inst|Selector31~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector31~0_combout\ = (\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\ & ((\decimal_printer_inst|state.bcd_data[1][0]~q\) # ((\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & 
-- !\decimal_printer_inst|state.bcd_data[2][0]~q\)))) # (!\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\ & (\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & (!\decimal_printer_inst|state.bcd_data[2][0]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\,
	datab => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	datac => \decimal_printer_inst|state.bcd_data[2][0]~q\,
	datad => \decimal_printer_inst|state.bcd_data[1][0]~q\,
	combout => \decimal_printer_inst|Selector31~0_combout\);

-- Location: LCCOMB_X28_Y71_N4
\decimal_printer_inst|state.bcd_data[2][0]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.bcd_data[2][0]~7_combout\ = (!\decimal_printer_inst|WideOr2~0_combout\ & (((!\decimal_printer_inst|state_nxt~7_combout\ & \decimal_printer_inst|state_nxt~8_combout\)) # (!\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001010100000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|WideOr2~0_combout\,
	datab => \decimal_printer_inst|state_nxt~7_combout\,
	datac => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	datad => \decimal_printer_inst|state_nxt~8_combout\,
	combout => \decimal_printer_inst|state.bcd_data[2][0]~7_combout\);

-- Location: FF_X24_Y70_N9
\decimal_printer_inst|state.bcd_data[2][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|Selector31~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \decimal_printer_inst|state.bcd_data[2][0]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.bcd_data[2][0]~q\);

-- Location: LCCOMB_X24_Y70_N24
\decimal_printer_inst|Selector27~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector27~0_combout\ = (\decimal_printer_inst|state.bcd_data[2][0]~q\ & ((\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\) # ((\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & 
-- !\decimal_printer_inst|state.bcd_data[3][0]~q\)))) # (!\decimal_printer_inst|state.bcd_data[2][0]~q\ & (\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & (!\decimal_printer_inst|state.bcd_data[3][0]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.bcd_data[2][0]~q\,
	datab => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	datac => \decimal_printer_inst|state.bcd_data[3][0]~q\,
	datad => \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\,
	combout => \decimal_printer_inst|Selector27~0_combout\);

-- Location: LCCOMB_X28_Y71_N10
\decimal_printer_inst|state.bcd_data[3][0]~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.bcd_data[3][0]~6_combout\ = (\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & (((\decimal_printer_inst|LessThan1~1_combout\ & \decimal_printer_inst|state.bcd_data[3][0]~4_combout\)) # 
-- (!\decimal_printer_inst|state.number[15]~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number[15]~4_combout\,
	datab => \decimal_printer_inst|LessThan1~1_combout\,
	datac => \decimal_printer_inst|state.bcd_data[3][0]~4_combout\,
	datad => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	combout => \decimal_printer_inst|state.bcd_data[3][0]~6_combout\);

-- Location: LCCOMB_X24_Y70_N14
\decimal_printer_inst|state.bcd_data[3][0]~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.bcd_data[3][0]~10_combout\ = (!\decimal_printer_inst|state.fsm_state.BB_CHAR_ARG~q\ & (!\decimal_printer_inst|state.fsm_state.BB_CHAR~q\ & !\decimal_printer_inst|state.bcd_data[3][0]~6_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.fsm_state.BB_CHAR_ARG~q\,
	datac => \decimal_printer_inst|state.fsm_state.BB_CHAR~q\,
	datad => \decimal_printer_inst|state.bcd_data[3][0]~6_combout\,
	combout => \decimal_printer_inst|state.bcd_data[3][0]~10_combout\);

-- Location: FF_X24_Y70_N25
\decimal_printer_inst|state.bcd_data[3][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|Selector27~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \decimal_printer_inst|state.bcd_data[3][0]~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.bcd_data[3][0]~q\);

-- Location: LCCOMB_X23_Y70_N10
\decimal_printer_inst|Selector23~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector23~0_combout\ = (\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\ & ((\decimal_printer_inst|state.bcd_data[3][0]~q\) # ((\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & 
-- !\decimal_printer_inst|state.bcd_data[4][0]~q\)))) # (!\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\ & (\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & (!\decimal_printer_inst|state.bcd_data[4][0]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\,
	datab => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	datac => \decimal_printer_inst|state.bcd_data[4][0]~q\,
	datad => \decimal_printer_inst|state.bcd_data[3][0]~q\,
	combout => \decimal_printer_inst|Selector23~0_combout\);

-- Location: LCCOMB_X23_Y70_N30
\decimal_printer_inst|state.bcd_data[4][0]~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.bcd_data[4][0]~9_combout\ = (!\decimal_printer_inst|state.fsm_state.BB_CHAR~q\ & (!\decimal_printer_inst|state.fsm_state.BB_CHAR_ARG~q\ & ((!\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\) # 
-- (!\decimal_printer_inst|state.number[15]~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.number[15]~4_combout\,
	datab => \decimal_printer_inst|state.fsm_state.BB_CHAR~q\,
	datac => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	datad => \decimal_printer_inst|state.fsm_state.BB_CHAR_ARG~q\,
	combout => \decimal_printer_inst|state.bcd_data[4][0]~9_combout\);

-- Location: FF_X23_Y70_N11
\decimal_printer_inst|state.bcd_data[4][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|Selector23~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \decimal_printer_inst|state.bcd_data[4][0]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.bcd_data[4][0]~q\);

-- Location: LCCOMB_X23_Y70_N28
\decimal_printer_inst|gfx_cmd[9]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|gfx_cmd[9]~4_combout\ = (!\gfx_cmd_full~input_o\ & (\decimal_printer_inst|state.fsm_state.BB_CHAR_ARG~q\ & \decimal_printer_inst|state.bcd_data[4][0]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gfx_cmd_full~input_o\,
	datab => \decimal_printer_inst|state.fsm_state.BB_CHAR_ARG~q\,
	datad => \decimal_printer_inst|state.bcd_data[4][0]~q\,
	combout => \decimal_printer_inst|gfx_cmd[9]~4_combout\);

-- Location: LCCOMB_X25_Y70_N10
\decimal_printer_inst|Selector38~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector38~0_combout\ = (\decimal_printer_inst|Selector2~1_combout\ & \decimal_printer_inst|state.number\(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|Selector2~1_combout\,
	datad => \decimal_printer_inst|state.number\(1),
	combout => \decimal_printer_inst|Selector38~0_combout\);

-- Location: LCCOMB_X25_Y70_N26
\decimal_printer_inst|Selector38~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector38~1_combout\ = (\decimal_printer_inst|Selector38~0_combout\) # ((\decimal_printer_inst|state.bcd_data[0][1]~q\ & ((\decimal_printer_inst|Selector1~2_combout\) # (\decimal_printer_inst|WideOr2~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Selector38~0_combout\,
	datab => \decimal_printer_inst|Selector1~2_combout\,
	datac => \decimal_printer_inst|state.bcd_data[0][1]~q\,
	datad => \decimal_printer_inst|WideOr2~0_combout\,
	combout => \decimal_printer_inst|Selector38~1_combout\);

-- Location: FF_X25_Y70_N27
\decimal_printer_inst|state.bcd_data[0][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|Selector38~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.bcd_data[0][1]~q\);

-- Location: LCCOMB_X25_Y70_N28
\decimal_printer_inst|Selector34~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector34~0_combout\ = (\decimal_printer_inst|state.bcd_data[0][1]~q\ & \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.bcd_data[0][1]~q\,
	datac => \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\,
	combout => \decimal_printer_inst|Selector34~0_combout\);

-- Location: LCCOMB_X25_Y70_N18
\decimal_printer_inst|Selector34~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector34~1_combout\ = (\decimal_printer_inst|Selector34~0_combout\) # ((\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & (\decimal_printer_inst|state.bcd_data[1][1]~q\ $ (\decimal_printer_inst|state.bcd_data[1][0]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	datab => \decimal_printer_inst|Selector34~0_combout\,
	datac => \decimal_printer_inst|state.bcd_data[1][1]~q\,
	datad => \decimal_printer_inst|state.bcd_data[1][0]~q\,
	combout => \decimal_printer_inst|Selector34~1_combout\);

-- Location: FF_X25_Y70_N19
\decimal_printer_inst|state.bcd_data[1][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|Selector34~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \decimal_printer_inst|state.bcd_data[1][3]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.bcd_data[1][1]~q\);

-- Location: LCCOMB_X24_Y70_N28
\decimal_printer_inst|Selector30~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector30~0_combout\ = (\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\ & \decimal_printer_inst|state.bcd_data[1][1]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\,
	datad => \decimal_printer_inst|state.bcd_data[1][1]~q\,
	combout => \decimal_printer_inst|Selector30~0_combout\);

-- Location: LCCOMB_X24_Y70_N2
\decimal_printer_inst|Selector30~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector30~1_combout\ = (\decimal_printer_inst|Selector30~0_combout\) # ((\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & (\decimal_printer_inst|state.bcd_data[2][0]~q\ $ (\decimal_printer_inst|state.bcd_data[2][1]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.bcd_data[2][0]~q\,
	datab => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	datac => \decimal_printer_inst|state.bcd_data[2][1]~q\,
	datad => \decimal_printer_inst|Selector30~0_combout\,
	combout => \decimal_printer_inst|Selector30~1_combout\);

-- Location: FF_X24_Y70_N3
\decimal_printer_inst|state.bcd_data[2][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|Selector30~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \decimal_printer_inst|state.bcd_data[2][0]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.bcd_data[2][1]~q\);

-- Location: LCCOMB_X24_Y70_N20
\decimal_printer_inst|Selector26~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector26~0_combout\ = (\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\ & \decimal_printer_inst|state.bcd_data[2][1]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\,
	datad => \decimal_printer_inst|state.bcd_data[2][1]~q\,
	combout => \decimal_printer_inst|Selector26~0_combout\);

-- Location: LCCOMB_X24_Y70_N18
\decimal_printer_inst|Selector26~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector26~1_combout\ = (\decimal_printer_inst|Selector26~0_combout\) # ((\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & (\decimal_printer_inst|state.bcd_data[3][0]~q\ $ (\decimal_printer_inst|state.bcd_data[3][1]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.bcd_data[3][0]~q\,
	datab => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	datac => \decimal_printer_inst|state.bcd_data[3][1]~q\,
	datad => \decimal_printer_inst|Selector26~0_combout\,
	combout => \decimal_printer_inst|Selector26~1_combout\);

-- Location: FF_X24_Y70_N19
\decimal_printer_inst|state.bcd_data[3][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|Selector26~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \decimal_printer_inst|state.bcd_data[3][0]~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.bcd_data[3][1]~q\);

-- Location: LCCOMB_X23_Y70_N6
\decimal_printer_inst|Selector22~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector22~0_combout\ = (\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\ & \decimal_printer_inst|state.bcd_data[3][1]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\,
	datad => \decimal_printer_inst|state.bcd_data[3][1]~q\,
	combout => \decimal_printer_inst|Selector22~0_combout\);

-- Location: LCCOMB_X23_Y70_N14
\decimal_printer_inst|Selector22~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector22~1_combout\ = (\decimal_printer_inst|Selector22~0_combout\) # ((\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & (\decimal_printer_inst|state.bcd_data[4][0]~q\ $ (\decimal_printer_inst|state.bcd_data[4][1]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.bcd_data[4][0]~q\,
	datab => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	datac => \decimal_printer_inst|state.bcd_data[4][1]~q\,
	datad => \decimal_printer_inst|Selector22~0_combout\,
	combout => \decimal_printer_inst|Selector22~1_combout\);

-- Location: FF_X23_Y70_N15
\decimal_printer_inst|state.bcd_data[4][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|Selector22~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \decimal_printer_inst|state.bcd_data[4][0]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.bcd_data[4][1]~q\);

-- Location: LCCOMB_X23_Y70_N8
\decimal_printer_inst|gfx_cmd[10]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|gfx_cmd[10]~5_combout\ = (!\gfx_cmd_full~input_o\ & (\decimal_printer_inst|state.bcd_data[4][1]~q\ & \decimal_printer_inst|state.fsm_state.BB_CHAR_ARG~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gfx_cmd_full~input_o\,
	datac => \decimal_printer_inst|state.bcd_data[4][1]~q\,
	datad => \decimal_printer_inst|state.fsm_state.BB_CHAR_ARG~q\,
	combout => \decimal_printer_inst|gfx_cmd[10]~5_combout\);

-- Location: LCCOMB_X24_Y70_N30
\decimal_printer_inst|Selector29~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector29~0_combout\ = (\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & (\decimal_printer_inst|state.bcd_data[2][2]~q\ $ (((\decimal_printer_inst|state.bcd_data[2][0]~q\ & \decimal_printer_inst|state.bcd_data[2][1]~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.bcd_data[2][2]~q\,
	datab => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	datac => \decimal_printer_inst|state.bcd_data[2][0]~q\,
	datad => \decimal_printer_inst|state.bcd_data[2][1]~q\,
	combout => \decimal_printer_inst|Selector29~0_combout\);

-- Location: LCCOMB_X24_Y70_N10
\decimal_printer_inst|Selector33~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector33~0_combout\ = (\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & (\decimal_printer_inst|state.bcd_data[1][2]~q\ $ (((\decimal_printer_inst|state.bcd_data[1][0]~q\ & \decimal_printer_inst|state.bcd_data[1][1]~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	datab => \decimal_printer_inst|state.bcd_data[1][0]~q\,
	datac => \decimal_printer_inst|state.bcd_data[1][2]~q\,
	datad => \decimal_printer_inst|state.bcd_data[1][1]~q\,
	combout => \decimal_printer_inst|Selector33~0_combout\);

-- Location: LCCOMB_X25_Y70_N20
\decimal_printer_inst|Selector37~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector37~0_combout\ = (\decimal_printer_inst|Selector2~1_combout\ & \decimal_printer_inst|state.number\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|Selector2~1_combout\,
	datac => \decimal_printer_inst|state.number\(2),
	combout => \decimal_printer_inst|Selector37~0_combout\);

-- Location: LCCOMB_X25_Y70_N6
\decimal_printer_inst|Selector37~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector37~1_combout\ = (\decimal_printer_inst|Selector37~0_combout\) # ((\decimal_printer_inst|state.bcd_data[0][2]~q\ & ((\decimal_printer_inst|WideOr2~0_combout\) # (\decimal_printer_inst|Selector1~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|WideOr2~0_combout\,
	datab => \decimal_printer_inst|Selector1~2_combout\,
	datac => \decimal_printer_inst|state.bcd_data[0][2]~q\,
	datad => \decimal_printer_inst|Selector37~0_combout\,
	combout => \decimal_printer_inst|Selector37~1_combout\);

-- Location: FF_X25_Y70_N7
\decimal_printer_inst|state.bcd_data[0][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|Selector37~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.bcd_data[0][2]~q\);

-- Location: LCCOMB_X24_Y70_N4
\decimal_printer_inst|Selector33~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector33~1_combout\ = (\decimal_printer_inst|Selector33~0_combout\) # ((\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\ & \decimal_printer_inst|state.bcd_data[0][2]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Selector33~0_combout\,
	datac => \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\,
	datad => \decimal_printer_inst|state.bcd_data[0][2]~q\,
	combout => \decimal_printer_inst|Selector33~1_combout\);

-- Location: FF_X25_Y70_N5
\decimal_printer_inst|state.bcd_data[1][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \decimal_printer_inst|Selector33~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \decimal_printer_inst|state.bcd_data[1][3]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.bcd_data[1][2]~q\);

-- Location: LCCOMB_X24_Y70_N22
\decimal_printer_inst|Selector29~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector29~1_combout\ = (\decimal_printer_inst|Selector29~0_combout\) # ((\decimal_printer_inst|state.bcd_data[1][2]~q\ & \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Selector29~0_combout\,
	datab => \decimal_printer_inst|state.bcd_data[1][2]~q\,
	datac => \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\,
	combout => \decimal_printer_inst|Selector29~1_combout\);

-- Location: FF_X24_Y70_N23
\decimal_printer_inst|state.bcd_data[2][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|Selector29~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \decimal_printer_inst|state.bcd_data[2][0]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.bcd_data[2][2]~q\);

-- Location: LCCOMB_X23_Y70_N12
\decimal_printer_inst|Selector25~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector25~0_combout\ = (\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & (\decimal_printer_inst|state.bcd_data[3][2]~q\ $ (((\decimal_printer_inst|state.bcd_data[3][1]~q\ & \decimal_printer_inst|state.bcd_data[3][0]~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.bcd_data[3][2]~q\,
	datab => \decimal_printer_inst|state.bcd_data[3][1]~q\,
	datac => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	datad => \decimal_printer_inst|state.bcd_data[3][0]~q\,
	combout => \decimal_printer_inst|Selector25~0_combout\);

-- Location: LCCOMB_X24_Y70_N12
\decimal_printer_inst|Selector25~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector25~1_combout\ = (\decimal_printer_inst|Selector25~0_combout\) # ((\decimal_printer_inst|state.bcd_data[2][2]~q\ & \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.bcd_data[2][2]~q\,
	datac => \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\,
	datad => \decimal_printer_inst|Selector25~0_combout\,
	combout => \decimal_printer_inst|Selector25~1_combout\);

-- Location: FF_X24_Y70_N13
\decimal_printer_inst|state.bcd_data[3][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|Selector25~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \decimal_printer_inst|state.bcd_data[3][0]~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.bcd_data[3][2]~q\);

-- Location: LCCOMB_X23_Y70_N24
\decimal_printer_inst|Selector21~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector21~0_combout\ = (\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & (\decimal_printer_inst|state.bcd_data[4][2]~q\ $ (((\decimal_printer_inst|state.bcd_data[4][1]~q\ & \decimal_printer_inst|state.bcd_data[4][0]~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.bcd_data[4][2]~q\,
	datab => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	datac => \decimal_printer_inst|state.bcd_data[4][1]~q\,
	datad => \decimal_printer_inst|state.bcd_data[4][0]~q\,
	combout => \decimal_printer_inst|Selector21~0_combout\);

-- Location: LCCOMB_X23_Y70_N26
\decimal_printer_inst|Selector21~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector21~1_combout\ = (\decimal_printer_inst|Selector21~0_combout\) # ((\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\ & \decimal_printer_inst|state.bcd_data[3][2]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\,
	datac => \decimal_printer_inst|state.bcd_data[3][2]~q\,
	datad => \decimal_printer_inst|Selector21~0_combout\,
	combout => \decimal_printer_inst|Selector21~1_combout\);

-- Location: FF_X23_Y70_N27
\decimal_printer_inst|state.bcd_data[4][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|Selector21~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \decimal_printer_inst|state.bcd_data[4][0]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.bcd_data[4][2]~q\);

-- Location: LCCOMB_X23_Y70_N20
\decimal_printer_inst|gfx_cmd[11]~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|gfx_cmd[11]~6_combout\ = (!\gfx_cmd_full~input_o\ & (\decimal_printer_inst|state.bcd_data[4][2]~q\ & \decimal_printer_inst|state.fsm_state.BB_CHAR_ARG~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gfx_cmd_full~input_o\,
	datac => \decimal_printer_inst|state.bcd_data[4][2]~q\,
	datad => \decimal_printer_inst|state.fsm_state.BB_CHAR_ARG~q\,
	combout => \decimal_printer_inst|gfx_cmd[11]~6_combout\);

-- Location: LCCOMB_X24_Y70_N16
\decimal_printer_inst|Add4~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add4~0_combout\ = \decimal_printer_inst|state.bcd_data[2][3]~q\ $ (((\decimal_printer_inst|state.bcd_data[2][2]~q\ & (\decimal_printer_inst|state.bcd_data[2][1]~q\ & \decimal_printer_inst|state.bcd_data[2][0]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.bcd_data[2][2]~q\,
	datab => \decimal_printer_inst|state.bcd_data[2][1]~q\,
	datac => \decimal_printer_inst|state.bcd_data[2][3]~q\,
	datad => \decimal_printer_inst|state.bcd_data[2][0]~q\,
	combout => \decimal_printer_inst|Add4~0_combout\);

-- Location: LCCOMB_X25_Y70_N30
\decimal_printer_inst|Selector36~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector36~0_combout\ = (\decimal_printer_inst|Selector2~1_combout\ & \decimal_printer_inst|state.number\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \decimal_printer_inst|Selector2~1_combout\,
	datac => \decimal_printer_inst|state.number\(3),
	combout => \decimal_printer_inst|Selector36~0_combout\);

-- Location: LCCOMB_X25_Y70_N8
\decimal_printer_inst|Selector36~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector36~1_combout\ = (\decimal_printer_inst|Selector36~0_combout\) # ((\decimal_printer_inst|state.bcd_data[0][3]~q\ & ((\decimal_printer_inst|Selector1~2_combout\) # (\decimal_printer_inst|WideOr2~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Selector36~0_combout\,
	datab => \decimal_printer_inst|Selector1~2_combout\,
	datac => \decimal_printer_inst|state.bcd_data[0][3]~q\,
	datad => \decimal_printer_inst|WideOr2~0_combout\,
	combout => \decimal_printer_inst|Selector36~1_combout\);

-- Location: FF_X25_Y70_N9
\decimal_printer_inst|state.bcd_data[0][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|Selector36~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.bcd_data[0][3]~q\);

-- Location: LCCOMB_X25_Y70_N4
\decimal_printer_inst|Add6~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add6~0_combout\ = \decimal_printer_inst|state.bcd_data[1][3]~q\ $ (((\decimal_printer_inst|state.bcd_data[1][0]~q\ & (\decimal_printer_inst|state.bcd_data[1][2]~q\ & \decimal_printer_inst|state.bcd_data[1][1]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.bcd_data[1][3]~q\,
	datab => \decimal_printer_inst|state.bcd_data[1][0]~q\,
	datac => \decimal_printer_inst|state.bcd_data[1][2]~q\,
	datad => \decimal_printer_inst|state.bcd_data[1][1]~q\,
	combout => \decimal_printer_inst|Add6~0_combout\);

-- Location: LCCOMB_X25_Y70_N22
\decimal_printer_inst|Selector32~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector32~0_combout\ = (\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & ((\decimal_printer_inst|Add6~0_combout\) # ((\decimal_printer_inst|state.bcd_data[0][3]~q\ & \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\)))) # 
-- (!\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & (\decimal_printer_inst|state.bcd_data[0][3]~q\ & (\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	datab => \decimal_printer_inst|state.bcd_data[0][3]~q\,
	datac => \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\,
	datad => \decimal_printer_inst|Add6~0_combout\,
	combout => \decimal_printer_inst|Selector32~0_combout\);

-- Location: FF_X25_Y70_N23
\decimal_printer_inst|state.bcd_data[1][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|Selector32~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \decimal_printer_inst|state.bcd_data[1][3]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.bcd_data[1][3]~q\);

-- Location: LCCOMB_X24_Y70_N26
\decimal_printer_inst|Selector28~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector28~0_combout\ = (\decimal_printer_inst|Add4~0_combout\ & ((\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\) # ((\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\ & \decimal_printer_inst|state.bcd_data[1][3]~q\)))) # 
-- (!\decimal_printer_inst|Add4~0_combout\ & (\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\ & ((\decimal_printer_inst|state.bcd_data[1][3]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Add4~0_combout\,
	datab => \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\,
	datac => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	datad => \decimal_printer_inst|state.bcd_data[1][3]~q\,
	combout => \decimal_printer_inst|Selector28~0_combout\);

-- Location: FF_X24_Y70_N27
\decimal_printer_inst|state.bcd_data[2][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|Selector28~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \decimal_printer_inst|state.bcd_data[2][0]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.bcd_data[2][3]~q\);

-- Location: LCCOMB_X24_Y70_N0
\decimal_printer_inst|Add2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add2~0_combout\ = \decimal_printer_inst|state.bcd_data[3][3]~q\ $ (((\decimal_printer_inst|state.bcd_data[3][2]~q\ & (\decimal_printer_inst|state.bcd_data[3][1]~q\ & \decimal_printer_inst|state.bcd_data[3][0]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.bcd_data[3][2]~q\,
	datab => \decimal_printer_inst|state.bcd_data[3][1]~q\,
	datac => \decimal_printer_inst|state.bcd_data[3][0]~q\,
	datad => \decimal_printer_inst|state.bcd_data[3][3]~q\,
	combout => \decimal_printer_inst|Add2~0_combout\);

-- Location: LCCOMB_X24_Y70_N6
\decimal_printer_inst|Selector24~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector24~0_combout\ = (\decimal_printer_inst|state.bcd_data[2][3]~q\ & ((\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\) # ((\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & \decimal_printer_inst|Add2~0_combout\)))) # 
-- (!\decimal_printer_inst|state.bcd_data[2][3]~q\ & (((\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & \decimal_printer_inst|Add2~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.bcd_data[2][3]~q\,
	datab => \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\,
	datac => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	datad => \decimal_printer_inst|Add2~0_combout\,
	combout => \decimal_printer_inst|Selector24~0_combout\);

-- Location: FF_X24_Y70_N7
\decimal_printer_inst|state.bcd_data[3][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|Selector24~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \decimal_printer_inst|state.bcd_data[3][0]~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.bcd_data[3][3]~q\);

-- Location: LCCOMB_X23_Y70_N18
\decimal_printer_inst|Add0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Add0~0_combout\ = \decimal_printer_inst|state.bcd_data[4][3]~q\ $ (((\decimal_printer_inst|state.bcd_data[4][2]~q\ & (\decimal_printer_inst|state.bcd_data[4][1]~q\ & \decimal_printer_inst|state.bcd_data[4][0]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.bcd_data[4][2]~q\,
	datab => \decimal_printer_inst|state.bcd_data[4][1]~q\,
	datac => \decimal_printer_inst|state.bcd_data[4][3]~q\,
	datad => \decimal_printer_inst|state.bcd_data[4][0]~q\,
	combout => \decimal_printer_inst|Add0~0_combout\);

-- Location: LCCOMB_X23_Y70_N22
\decimal_printer_inst|Selector20~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector20~0_combout\ = (\decimal_printer_inst|state.bcd_data[3][3]~q\ & ((\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\) # ((\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & \decimal_printer_inst|Add0~0_combout\)))) # 
-- (!\decimal_printer_inst|state.bcd_data[3][3]~q\ & (((\decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\ & \decimal_printer_inst|Add0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.bcd_data[3][3]~q\,
	datab => \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\,
	datac => \decimal_printer_inst|state.fsm_state.CALC_DIGITS~q\,
	datad => \decimal_printer_inst|Add0~0_combout\,
	combout => \decimal_printer_inst|Selector20~0_combout\);

-- Location: FF_X23_Y70_N23
\decimal_printer_inst|state.bcd_data[4][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|Selector20~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \decimal_printer_inst|state.bcd_data[4][0]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.bcd_data[4][3]~q\);

-- Location: LCCOMB_X23_Y70_N0
\decimal_printer_inst|Selector45~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector45~1_combout\ = (!\gfx_cmd_full~input_o\ & ((\decimal_printer_inst|state.fsm_state.BB_CHAR~q\) # ((\decimal_printer_inst|state.bcd_data[4][3]~q\ & \decimal_printer_inst|state.fsm_state.BB_CHAR_ARG~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gfx_cmd_full~input_o\,
	datab => \decimal_printer_inst|state.fsm_state.BB_CHAR~q\,
	datac => \decimal_printer_inst|state.bcd_data[4][3]~q\,
	datad => \decimal_printer_inst|state.fsm_state.BB_CHAR_ARG~q\,
	combout => \decimal_printer_inst|Selector45~1_combout\);

-- Location: LCCOMB_X23_Y70_N2
\decimal_printer_inst|Selector44~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|Selector44~0_combout\ = (!\gfx_cmd_full~input_o\ & ((\decimal_printer_inst|state.fsm_state.BB_CHAR~q\) # (\decimal_printer_inst|state.fsm_state.BB_CHAR_ARG~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gfx_cmd_full~input_o\,
	datac => \decimal_printer_inst|state.fsm_state.BB_CHAR~q\,
	datad => \decimal_printer_inst|state.fsm_state.BB_CHAR_ARG~q\,
	combout => \decimal_printer_inst|Selector44~0_combout\);

-- Location: LCCOMB_X26_Y70_N28
\decimal_printer_inst|state.busy~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.busy~0_combout\ = (\decimal_printer_inst|state.digit_cnt\(0)) # (((\decimal_printer_inst|state.digit_cnt\(1)) # (!\decimal_printer_inst|state.fsm_state.IDLE~q\)) # (!\decimal_printer_inst|state.digit_cnt\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|state.digit_cnt\(0),
	datab => \decimal_printer_inst|state.digit_cnt\(2),
	datac => \decimal_printer_inst|state.digit_cnt\(1),
	datad => \decimal_printer_inst|state.fsm_state.IDLE~q\,
	combout => \decimal_printer_inst|state.busy~0_combout\);

-- Location: LCCOMB_X26_Y70_N6
\decimal_printer_inst|state.busy~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \decimal_printer_inst|state.busy~1_combout\ = (\decimal_printer_inst|Selector4~0_combout\) # ((\decimal_printer_inst|state.busy~q\ & ((\decimal_printer_inst|state.busy~0_combout\) # (!\decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101010111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \decimal_printer_inst|Selector4~0_combout\,
	datab => \decimal_printer_inst|state.fsm_state.DIGIT_DONE~q\,
	datac => \decimal_printer_inst|state.busy~q\,
	datad => \decimal_printer_inst|state.busy~0_combout\,
	combout => \decimal_printer_inst|state.busy~1_combout\);

-- Location: FF_X26_Y70_N7
\decimal_printer_inst|state.busy\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \decimal_printer_inst|state.busy~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \decimal_printer_inst|state.busy~q\);

ww_gfx_cmd(0) <= \gfx_cmd[0]~output_o\;

ww_gfx_cmd(1) <= \gfx_cmd[1]~output_o\;

ww_gfx_cmd(2) <= \gfx_cmd[2]~output_o\;

ww_gfx_cmd(3) <= \gfx_cmd[3]~output_o\;

ww_gfx_cmd(4) <= \gfx_cmd[4]~output_o\;

ww_gfx_cmd(5) <= \gfx_cmd[5]~output_o\;

ww_gfx_cmd(6) <= \gfx_cmd[6]~output_o\;

ww_gfx_cmd(7) <= \gfx_cmd[7]~output_o\;

ww_gfx_cmd(8) <= \gfx_cmd[8]~output_o\;

ww_gfx_cmd(9) <= \gfx_cmd[9]~output_o\;

ww_gfx_cmd(10) <= \gfx_cmd[10]~output_o\;

ww_gfx_cmd(11) <= \gfx_cmd[11]~output_o\;

ww_gfx_cmd(12) <= \gfx_cmd[12]~output_o\;

ww_gfx_cmd(13) <= \gfx_cmd[13]~output_o\;

ww_gfx_cmd(14) <= \gfx_cmd[14]~output_o\;

ww_gfx_cmd(15) <= \gfx_cmd[15]~output_o\;

ww_gfx_cmd_wr <= \gfx_cmd_wr~output_o\;

ww_busy <= \busy~output_o\;
END structure;


