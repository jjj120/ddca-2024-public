
# GameCube Controller

This module provides an interface controller for the [GameCube](https://en.wikipedia.org/wiki/GameCube) controller.

## Dependencies
* None

## Required Files
 * `precompiled_gc_ctrl.vhd`
 * `gc_ctrl_top.qxp` (use **only for synthesis** (i.e., in quartus), not for simulation!)
 * `gc_ctrl_top.vho` (use **only for simulation**, not for synthesis!)
 * `gc_ctrl_pkg.vhd`

## GameCube Controller Component Declaration

```vhdl
component precompiled_gc_ctrl is
	port (
		clk : in std_logic;
		res_n : in std_logic;
		data : inout std_logic;
		ctrl_state : out gc_ctrl_state_t;
		rumble : in std_logic
	);
end component;
```

## GameCube Controller Interface Description

The `precompiled_gc_ctrl` implements the serial single-wire GameCube controller communication protocol on the external signal wire `gc_data`.
To satisfy the timing constraints of this protocol it expects that the clock frequency on the `clk` input is 50 MHz.
The `res_n` input is an active-low reset input.

The internal interface to the `precompiled_snes_ctrl` is fairly simple.
The core continuously polls the controller state every few milliseconds and updates its output `ctrl_state` accordingly.
This output is a record type with the following structure:

```vhdl
type gc_ctrl_state_t is record
	-- buttons
	btn_up : std_logic;
	btn_down : std_logic;
	btn_left : std_logic;
	btn_right : std_logic;
	btn_a : std_logic;
	btn_b : std_logic;
	btn_x : std_logic;
	btn_y : std_logic;
	btn_z : std_logic;
	btn_start : std_logic;
	btn_l : std_logic;
	btn_r : std_logic;
	-- joysticks
	joy_x : std_logic_vector(7 downto 0);
	joy_y : std_logic_vector(7 downto 0);
	c_x : std_logic_vector(7 downto 0);
	c_y : std_logic_vector(7 downto 0);
	-- trigger
	trigger_l : std_logic_vector(7 downto 0);
	trigger_r : std_logic_vector(7 downto 0);
end record;
```

Each of the 12 controller buttons corresponds to one of the `btn_*` signals in the record.
Hence, if the X button is pressed, `btn_x` will be set to `'1'`.

The left and right analog triggers are represented by the two unsigned 8-bit values `trigger_l` and `trigger_r`, respectively.
A value of zero indicates that the trigger is not actuated at all, while 255 corresponds to a fully pressed trigger.

Each of the two joysticks has to two unsigned 8-bit values associated with it, representing its current X and Y displacements.
In the idle position of the joysticks both values should be around 127.
