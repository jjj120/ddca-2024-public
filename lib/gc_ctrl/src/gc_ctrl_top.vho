-- Copyright (C) 2023  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 22.1std.2 Build 922 07/20/2023 SC Standard Edition"

-- DATE "03/01/2024 21:05:24"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim (VHDL) only
-- 

LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_ASDO_DATA1~	=>  Location: PIN_F4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_FLASH_nCE_nCSO~	=>  Location: PIN_E2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DCLK~	=>  Location: PIN_P3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DATA0~	=>  Location: PIN_N7,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~padout\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~padout\ : std_logic;
SIGNAL \~ALTERA_DATA0~~padout\ : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_DATA0~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	gc_ctrl_top IS
    PORT (
	clk : IN std_logic;
	res_n : IN std_logic;
	data : INOUT std_logic;
	ctrl_state : OUT std_logic_vector(63 DOWNTO 0);
	rumble : IN std_logic
	);
END gc_ctrl_top;

-- Design Ports Information
-- ctrl_state[0]	=>  Location: PIN_AE11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[1]	=>  Location: PIN_R4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[2]	=>  Location: PIN_AC10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[3]	=>  Location: PIN_AF11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[4]	=>  Location: PIN_R5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[5]	=>  Location: PIN_AG11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[6]	=>  Location: PIN_T7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[7]	=>  Location: PIN_T4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[8]	=>  Location: PIN_R7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[9]	=>  Location: PIN_D14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[10]	=>  Location: PIN_U4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[11]	=>  Location: PIN_AB10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[12]	=>  Location: PIN_U2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[13]	=>  Location: PIN_AC14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[14]	=>  Location: PIN_AA14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[15]	=>  Location: PIN_AA13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[16]	=>  Location: PIN_AB14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[17]	=>  Location: PIN_Y13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[18]	=>  Location: PIN_AD14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[19]	=>  Location: PIN_Y14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[20]	=>  Location: PIN_Y15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[21]	=>  Location: PIN_D12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[22]	=>  Location: PIN_C16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[23]	=>  Location: PIN_C14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[24]	=>  Location: PIN_J16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[25]	=>  Location: PIN_A17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[26]	=>  Location: PIN_C15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[27]	=>  Location: PIN_D15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[28]	=>  Location: PIN_F15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[29]	=>  Location: PIN_C13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[30]	=>  Location: PIN_B17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[31]	=>  Location: PIN_J15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[32]	=>  Location: PIN_E15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[33]	=>  Location: PIN_D13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[34]	=>  Location: PIN_H15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[35]	=>  Location: PIN_D16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[36]	=>  Location: PIN_A12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[37]	=>  Location: PIN_F14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[38]	=>  Location: PIN_AB13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[39]	=>  Location: PIN_AC12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[40]	=>  Location: PIN_AH11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[41]	=>  Location: PIN_AE13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[42]	=>  Location: PIN_AD12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[43]	=>  Location: PIN_AB12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[44]	=>  Location: PIN_AF12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[45]	=>  Location: PIN_AF13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[46]	=>  Location: PIN_AH12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[47]	=>  Location: PIN_AA12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[48]	=>  Location: PIN_AC11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[49]	=>  Location: PIN_AF14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[50]	=>  Location: PIN_AE14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[51]	=>  Location: PIN_AD11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[52]	=>  Location: PIN_Y12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[53]	=>  Location: PIN_AG12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[54]	=>  Location: PIN_J14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[55]	=>  Location: PIN_J10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[56]	=>  Location: PIN_E14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[57]	=>  Location: PIN_H14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[58]	=>  Location: PIN_C12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[59]	=>  Location: PIN_B11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[60]	=>  Location: PIN_G14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[61]	=>  Location: PIN_B10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[62]	=>  Location: PIN_V28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[63]	=>  Location: PIN_U24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data	=>  Location: PIN_T3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clk	=>  Location: PIN_J1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- res_n	=>  Location: PIN_Y2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rumble	=>  Location: PIN_AG15,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF gc_ctrl_top IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_res_n : std_logic;
SIGNAL ww_ctrl_state : std_logic_vector(63 DOWNTO 0);
SIGNAL ww_rumble : std_logic;
SIGNAL \res_n~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \clk~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \data~output_o\ : std_logic;
SIGNAL \ctrl_state[0]~output_o\ : std_logic;
SIGNAL \ctrl_state[1]~output_o\ : std_logic;
SIGNAL \ctrl_state[2]~output_o\ : std_logic;
SIGNAL \ctrl_state[3]~output_o\ : std_logic;
SIGNAL \ctrl_state[4]~output_o\ : std_logic;
SIGNAL \ctrl_state[5]~output_o\ : std_logic;
SIGNAL \ctrl_state[6]~output_o\ : std_logic;
SIGNAL \ctrl_state[7]~output_o\ : std_logic;
SIGNAL \ctrl_state[8]~output_o\ : std_logic;
SIGNAL \ctrl_state[9]~output_o\ : std_logic;
SIGNAL \ctrl_state[10]~output_o\ : std_logic;
SIGNAL \ctrl_state[11]~output_o\ : std_logic;
SIGNAL \ctrl_state[12]~output_o\ : std_logic;
SIGNAL \ctrl_state[13]~output_o\ : std_logic;
SIGNAL \ctrl_state[14]~output_o\ : std_logic;
SIGNAL \ctrl_state[15]~output_o\ : std_logic;
SIGNAL \ctrl_state[16]~output_o\ : std_logic;
SIGNAL \ctrl_state[17]~output_o\ : std_logic;
SIGNAL \ctrl_state[18]~output_o\ : std_logic;
SIGNAL \ctrl_state[19]~output_o\ : std_logic;
SIGNAL \ctrl_state[20]~output_o\ : std_logic;
SIGNAL \ctrl_state[21]~output_o\ : std_logic;
SIGNAL \ctrl_state[22]~output_o\ : std_logic;
SIGNAL \ctrl_state[23]~output_o\ : std_logic;
SIGNAL \ctrl_state[24]~output_o\ : std_logic;
SIGNAL \ctrl_state[25]~output_o\ : std_logic;
SIGNAL \ctrl_state[26]~output_o\ : std_logic;
SIGNAL \ctrl_state[27]~output_o\ : std_logic;
SIGNAL \ctrl_state[28]~output_o\ : std_logic;
SIGNAL \ctrl_state[29]~output_o\ : std_logic;
SIGNAL \ctrl_state[30]~output_o\ : std_logic;
SIGNAL \ctrl_state[31]~output_o\ : std_logic;
SIGNAL \ctrl_state[32]~output_o\ : std_logic;
SIGNAL \ctrl_state[33]~output_o\ : std_logic;
SIGNAL \ctrl_state[34]~output_o\ : std_logic;
SIGNAL \ctrl_state[35]~output_o\ : std_logic;
SIGNAL \ctrl_state[36]~output_o\ : std_logic;
SIGNAL \ctrl_state[37]~output_o\ : std_logic;
SIGNAL \ctrl_state[38]~output_o\ : std_logic;
SIGNAL \ctrl_state[39]~output_o\ : std_logic;
SIGNAL \ctrl_state[40]~output_o\ : std_logic;
SIGNAL \ctrl_state[41]~output_o\ : std_logic;
SIGNAL \ctrl_state[42]~output_o\ : std_logic;
SIGNAL \ctrl_state[43]~output_o\ : std_logic;
SIGNAL \ctrl_state[44]~output_o\ : std_logic;
SIGNAL \ctrl_state[45]~output_o\ : std_logic;
SIGNAL \ctrl_state[46]~output_o\ : std_logic;
SIGNAL \ctrl_state[47]~output_o\ : std_logic;
SIGNAL \ctrl_state[48]~output_o\ : std_logic;
SIGNAL \ctrl_state[49]~output_o\ : std_logic;
SIGNAL \ctrl_state[50]~output_o\ : std_logic;
SIGNAL \ctrl_state[51]~output_o\ : std_logic;
SIGNAL \ctrl_state[52]~output_o\ : std_logic;
SIGNAL \ctrl_state[53]~output_o\ : std_logic;
SIGNAL \ctrl_state[54]~output_o\ : std_logic;
SIGNAL \ctrl_state[55]~output_o\ : std_logic;
SIGNAL \ctrl_state[56]~output_o\ : std_logic;
SIGNAL \ctrl_state[57]~output_o\ : std_logic;
SIGNAL \ctrl_state[58]~output_o\ : std_logic;
SIGNAL \ctrl_state[59]~output_o\ : std_logic;
SIGNAL \ctrl_state[60]~output_o\ : std_logic;
SIGNAL \ctrl_state[61]~output_o\ : std_logic;
SIGNAL \ctrl_state[62]~output_o\ : std_logic;
SIGNAL \ctrl_state[63]~output_o\ : std_logic;
SIGNAL \clk~input_o\ : std_logic;
SIGNAL \clk~inputclkctrl_outclk\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector63~0_combout\ : std_logic;
SIGNAL \res_n~input_o\ : std_logic;
SIGNAL \res_n~inputclkctrl_outclk\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector1~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal0~6_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal1~3_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal0~7_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal0~8_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal0~9_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal0~10_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector95~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~7\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~8_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector91~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal4~11_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|LessThan0~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~9\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~10_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector90~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal3~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal3~1_combout\ : std_logic;
SIGNAL \data~input_o\ : std_logic;
SIGNAL \gc_ctrl_inst|data_synced~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|data_synced~q\ : std_logic;
SIGNAL \gc_ctrl_inst|state.data_synced_old~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.data_synced_old~q\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector5~1_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.fsm_state.WAIT_FALLING_EDGE_DATA~q\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector6~5_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector4~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.fsm_state.WAIT_SAMPLE~q\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~11\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~12_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector89~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~13\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~14_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector88~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~15\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~16_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector87~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~17\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~18_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector86~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~19\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~20_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector85~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~21\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~22_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector84~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~23\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~24_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector83~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~25\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~26_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector82~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~27\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~28_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector81~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~29\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~30_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector80~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~31\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~32_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector79~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~33\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~34_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector78~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~35\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~36_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector77~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~37\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~38_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector76~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~39\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~40_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector75~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~41\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~42_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector74~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~43\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~44_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector73~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~45\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~46_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector72~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~47\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~48_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector71~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~49\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~50_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector70~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~51\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~52_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector69~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~53\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~54_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector68~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~55\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~56_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector67~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~57\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~58_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector66~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~59\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~60_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector65~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~61\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~62_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector64~2_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector64~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector64~1_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector64~3_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal4~2_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal4~3_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal4~1_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal4~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal4~4_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal4~6_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal4~5_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal4~7_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|LessThan0~1_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector5~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.data_cnt[26]~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~1\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~2_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector94~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~3\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~4_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector93~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~5\ : std_logic;
SIGNAL \gc_ctrl_inst|Add1~6_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector92~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal4~8_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal4~9_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal4~10_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector0~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector6~7_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.poll_cmd[1]~2_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector31~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.poll_cmd[5]~0_combout\ : std_logic;
SIGNAL \rumble~input_o\ : std_logic;
SIGNAL \gc_ctrl_inst|state.poll_cmd[1]~1_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.poll_cmd[1]~3_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.poll_cmd[1]~4_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector29~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector28~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector27~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector26~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector25~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector24~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector23~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector22~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector21~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector20~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector19~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector18~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector17~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector16~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector15~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector14~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector13~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector12~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector11~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector10~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector9~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector8~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector7~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|next_state_logic~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector6~2_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector3~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector6~3_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector6~1_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector6~4_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector6~6_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\ : std_logic;
SIGNAL \gc_ctrl_inst|WideOr1~combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~1\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~2_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector62~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~3\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~4_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector61~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~5\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~6_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector60~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~7\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~8_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector59~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~9\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~10_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector58~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~11\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~12_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector57~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~13\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~14_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector56~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~15\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~16_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector55~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~17\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~18_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector54~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~19\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~20_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector53~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~21\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~22_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector52~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~23\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~24_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector51~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~25\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~26_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector50~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~27\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~28_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector49~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~29\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~31\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~32_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector47~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~33\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~34_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector46~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~35\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~36_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector45~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~37\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~38_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector44~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~39\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~40_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector43~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~41\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~42_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector42~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~43\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~44_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector41~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~45\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~46_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector40~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal0~3_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal0~4_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~47\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~48_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector39~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~49\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~50_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector38~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~51\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~52_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector37~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~53\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~54_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector36~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal0~2_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~55\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~56_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector35~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~57\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~58_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector34~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~59\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~60_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector33~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector32~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector2~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~61\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~62_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector32~1_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector32~2_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal0~1_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal0~5_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal1~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal1~2_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal2~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|next_state_logic~1_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.clk_cnt[6]~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.clk_cnt[6]~1_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Add0~30_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector48~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal0~0_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Equal1~1_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector1~1_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector1~2_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.fsm_state.POLL~q\ : std_logic;
SIGNAL \gc_ctrl_inst|Selector2~1_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.fsm_state.SEND_LOW~q\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[62]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.trigger_r[2]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[60]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.trigger_r[3]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[59]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.trigger_r[4]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[58]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.trigger_r[5]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[57]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.trigger_r[6]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.trigger_l[0]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.trigger_l[2]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[52]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.trigger_l[3]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[51]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.trigger_l[4]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[50]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.trigger_l[7]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[47]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.c_y[0]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[46]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.c_y[2]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[44]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.c_y[3]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[43]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.c_y[4]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[42]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.c_y[5]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[41]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.c_y[6]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[40]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.c_y[7]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[39]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.c_x[1]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[37]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.c_x[2]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[36]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.c_x[3]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[35]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.c_x[4]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[34]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.c_x[6]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[32]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.c_x[7]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[31]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.joy_y[1]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[29]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.joy_y[2]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[28]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.joy_y[3]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[27]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.joy_y[5]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[25]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.joy_y[6]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[24]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.joy_y[7]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[23]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.joy_x[0]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[22]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.joy_x[1]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[21]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.joy_x[3]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[19]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.joy_x[4]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[18]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.joy_x[5]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[17]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.btn_left~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.btn_left~q\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[14]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.btn_right~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.btn_right~q\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[13]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.btn_down~q\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.btn_up~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.btn_up~q\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[11]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.btn_z~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.btn_z~q\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[10]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.btn_r~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.btn_r~q\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[9]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.btn_l~q\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[7]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.btn_a~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.btn_a~q\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[6]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.btn_b~q\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.btn_x~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.btn_x~q\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[4]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.btn_y~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.btn_y~q\ : std_logic;
SIGNAL \gc_ctrl_inst|state.response[3]~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.btn_start~feeder_combout\ : std_logic;
SIGNAL \gc_ctrl_inst|state.ctrl_state.btn_start~q\ : std_logic;
SIGNAL \gc_ctrl_inst|state.poll_cmd\ : std_logic_vector(24 DOWNTO 0);
SIGNAL \gc_ctrl_inst|sync_shiftreg\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \gc_ctrl_inst|state.clk_cnt\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \gc_ctrl_inst|state.data_cnt\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \gc_ctrl_inst|state.response\ : std_logic_vector(63 DOWNTO 0);
SIGNAL \gc_ctrl_inst|state.ctrl_state.trigger_l\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \gc_ctrl_inst|state.ctrl_state.trigger_r\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \gc_ctrl_inst|state.ctrl_state.c_y\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \gc_ctrl_inst|state.ctrl_state.c_x\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \gc_ctrl_inst|state.ctrl_state.joy_y\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \gc_ctrl_inst|state.ctrl_state.joy_x\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \gc_ctrl_inst|ALT_INV_state.fsm_state.SEND_LOW~q\ : std_logic;
SIGNAL \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\ : std_logic;

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

ww_clk <= clk;
ww_res_n <= res_n;
ctrl_state <= ww_ctrl_state;
ww_rumble <= rumble;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\res_n~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \res_n~input_o\);

\clk~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \clk~input_o\);
\gc_ctrl_inst|ALT_INV_state.fsm_state.SEND_LOW~q\ <= NOT \gc_ctrl_inst|state.fsm_state.SEND_LOW~q\;
\gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\ <= NOT \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\;
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: IOOBUF_X0_Y32_N16
\data~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|ALT_INV_state.fsm_state.SEND_LOW~q\,
	oe => VCC,
	devoe => ww_devoe,
	o => \data~output_o\);

-- Location: IOOBUF_X35_Y0_N23
\ctrl_state[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.trigger_r\(0),
	devoe => ww_devoe,
	o => \ctrl_state[0]~output_o\);

-- Location: IOOBUF_X0_Y33_N16
\ctrl_state[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.trigger_r\(1),
	devoe => ww_devoe,
	o => \ctrl_state[1]~output_o\);

-- Location: IOOBUF_X38_Y0_N2
\ctrl_state[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.trigger_r\(2),
	devoe => ww_devoe,
	o => \ctrl_state[2]~output_o\);

-- Location: IOOBUF_X35_Y0_N16
\ctrl_state[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.trigger_r\(3),
	devoe => ww_devoe,
	o => \ctrl_state[3]~output_o\);

-- Location: IOOBUF_X0_Y32_N23
\ctrl_state[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.trigger_r\(4),
	devoe => ww_devoe,
	o => \ctrl_state[4]~output_o\);

-- Location: IOOBUF_X40_Y0_N23
\ctrl_state[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.trigger_r\(5),
	devoe => ww_devoe,
	o => \ctrl_state[5]~output_o\);

-- Location: IOOBUF_X0_Y31_N16
\ctrl_state[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.trigger_r\(6),
	devoe => ww_devoe,
	o => \ctrl_state[6]~output_o\);

-- Location: IOOBUF_X0_Y33_N23
\ctrl_state[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.trigger_r\(7),
	devoe => ww_devoe,
	o => \ctrl_state[7]~output_o\);

-- Location: IOOBUF_X0_Y35_N16
\ctrl_state[8]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.trigger_l\(0),
	devoe => ww_devoe,
	o => \ctrl_state[8]~output_o\);

-- Location: IOOBUF_X52_Y73_N9
\ctrl_state[9]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.trigger_l\(1),
	devoe => ww_devoe,
	o => \ctrl_state[9]~output_o\);

-- Location: IOOBUF_X0_Y34_N16
\ctrl_state[10]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.trigger_l\(2),
	devoe => ww_devoe,
	o => \ctrl_state[10]~output_o\);

-- Location: IOOBUF_X38_Y0_N9
\ctrl_state[11]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.trigger_l\(3),
	devoe => ww_devoe,
	o => \ctrl_state[11]~output_o\);

-- Location: IOOBUF_X0_Y30_N2
\ctrl_state[12]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.trigger_l\(4),
	devoe => ww_devoe,
	o => \ctrl_state[12]~output_o\);

-- Location: IOOBUF_X56_Y0_N23
\ctrl_state[13]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.trigger_l\(5),
	devoe => ww_devoe,
	o => \ctrl_state[13]~output_o\);

-- Location: IOOBUF_X54_Y0_N23
\ctrl_state[14]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.trigger_l\(6),
	devoe => ww_devoe,
	o => \ctrl_state[14]~output_o\);

-- Location: IOOBUF_X52_Y0_N2
\ctrl_state[15]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.trigger_l\(7),
	devoe => ww_devoe,
	o => \ctrl_state[15]~output_o\);

-- Location: IOOBUF_X54_Y0_N16
\ctrl_state[16]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.c_y\(0),
	devoe => ww_devoe,
	o => \ctrl_state[16]~output_o\);

-- Location: IOOBUF_X52_Y0_N9
\ctrl_state[17]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.c_y\(1),
	devoe => ww_devoe,
	o => \ctrl_state[17]~output_o\);

-- Location: IOOBUF_X56_Y0_N16
\ctrl_state[18]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.c_y\(2),
	devoe => ww_devoe,
	o => \ctrl_state[18]~output_o\);

-- Location: IOOBUF_X56_Y0_N9
\ctrl_state[19]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.c_y\(3),
	devoe => ww_devoe,
	o => \ctrl_state[19]~output_o\);

-- Location: IOOBUF_X56_Y0_N2
\ctrl_state[20]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.c_y\(4),
	devoe => ww_devoe,
	o => \ctrl_state[20]~output_o\);

-- Location: IOOBUF_X52_Y73_N23
\ctrl_state[21]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.c_y\(5),
	devoe => ww_devoe,
	o => \ctrl_state[21]~output_o\);

-- Location: IOOBUF_X62_Y73_N16
\ctrl_state[22]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.c_y\(6),
	devoe => ww_devoe,
	o => \ctrl_state[22]~output_o\);

-- Location: IOOBUF_X52_Y73_N2
\ctrl_state[23]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.c_y\(7),
	devoe => ww_devoe,
	o => \ctrl_state[23]~output_o\);

-- Location: IOOBUF_X65_Y73_N16
\ctrl_state[24]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.c_x\(0),
	devoe => ww_devoe,
	o => \ctrl_state[24]~output_o\);

-- Location: IOOBUF_X60_Y73_N2
\ctrl_state[25]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.c_x\(1),
	devoe => ww_devoe,
	o => \ctrl_state[25]~output_o\);

-- Location: IOOBUF_X58_Y73_N16
\ctrl_state[26]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.c_x\(2),
	devoe => ww_devoe,
	o => \ctrl_state[26]~output_o\);

-- Location: IOOBUF_X58_Y73_N23
\ctrl_state[27]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.c_x\(3),
	devoe => ww_devoe,
	o => \ctrl_state[27]~output_o\);

-- Location: IOOBUF_X58_Y73_N2
\ctrl_state[28]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.c_x\(4),
	devoe => ww_devoe,
	o => \ctrl_state[28]~output_o\);

-- Location: IOOBUF_X54_Y73_N2
\ctrl_state[29]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.c_x\(5),
	devoe => ww_devoe,
	o => \ctrl_state[29]~output_o\);

-- Location: IOOBUF_X60_Y73_N9
\ctrl_state[30]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.c_x\(6),
	devoe => ww_devoe,
	o => \ctrl_state[30]~output_o\);

-- Location: IOOBUF_X60_Y73_N23
\ctrl_state[31]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.c_x\(7),
	devoe => ww_devoe,
	o => \ctrl_state[31]~output_o\);

-- Location: IOOBUF_X58_Y73_N9
\ctrl_state[32]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.joy_y\(0),
	devoe => ww_devoe,
	o => \ctrl_state[32]~output_o\);

-- Location: IOOBUF_X54_Y73_N9
\ctrl_state[33]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.joy_y\(1),
	devoe => ww_devoe,
	o => \ctrl_state[33]~output_o\);

-- Location: IOOBUF_X60_Y73_N16
\ctrl_state[34]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.joy_y\(2),
	devoe => ww_devoe,
	o => \ctrl_state[34]~output_o\);

-- Location: IOOBUF_X62_Y73_N23
\ctrl_state[35]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.joy_y\(3),
	devoe => ww_devoe,
	o => \ctrl_state[35]~output_o\);

-- Location: IOOBUF_X47_Y73_N2
\ctrl_state[36]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.joy_y\(4),
	devoe => ww_devoe,
	o => \ctrl_state[36]~output_o\);

-- Location: IOOBUF_X45_Y73_N2
\ctrl_state[37]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.joy_y\(5),
	devoe => ww_devoe,
	o => \ctrl_state[37]~output_o\);

-- Location: IOOBUF_X47_Y0_N9
\ctrl_state[38]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.joy_y\(6),
	devoe => ww_devoe,
	o => \ctrl_state[38]~output_o\);

-- Location: IOOBUF_X45_Y0_N23
\ctrl_state[39]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.joy_y\(7),
	devoe => ww_devoe,
	o => \ctrl_state[39]~output_o\);

-- Location: IOOBUF_X40_Y0_N16
\ctrl_state[40]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.joy_x\(0),
	devoe => ww_devoe,
	o => \ctrl_state[40]~output_o\);

-- Location: IOOBUF_X42_Y0_N23
\ctrl_state[41]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.joy_x\(1),
	devoe => ww_devoe,
	o => \ctrl_state[41]~output_o\);

-- Location: IOOBUF_X47_Y0_N2
\ctrl_state[42]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.joy_x\(2),
	devoe => ww_devoe,
	o => \ctrl_state[42]~output_o\);

-- Location: IOOBUF_X45_Y0_N16
\ctrl_state[43]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.joy_x\(3),
	devoe => ww_devoe,
	o => \ctrl_state[43]~output_o\);

-- Location: IOOBUF_X33_Y0_N2
\ctrl_state[44]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.joy_x\(4),
	devoe => ww_devoe,
	o => \ctrl_state[44]~output_o\);

-- Location: IOOBUF_X42_Y0_N16
\ctrl_state[45]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.joy_x\(5),
	devoe => ww_devoe,
	o => \ctrl_state[45]~output_o\);

-- Location: IOOBUF_X54_Y0_N2
\ctrl_state[46]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.joy_x\(6),
	devoe => ww_devoe,
	o => \ctrl_state[46]~output_o\);

-- Location: IOOBUF_X52_Y0_N16
\ctrl_state[47]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.joy_x\(7),
	devoe => ww_devoe,
	o => \ctrl_state[47]~output_o\);

-- Location: IOOBUF_X49_Y0_N9
\ctrl_state[48]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.btn_left~q\,
	devoe => ww_devoe,
	o => \ctrl_state[48]~output_o\);

-- Location: IOOBUF_X49_Y0_N16
\ctrl_state[49]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.btn_right~q\,
	devoe => ww_devoe,
	o => \ctrl_state[49]~output_o\);

-- Location: IOOBUF_X49_Y0_N23
\ctrl_state[50]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.btn_down~q\,
	devoe => ww_devoe,
	o => \ctrl_state[50]~output_o\);

-- Location: IOOBUF_X49_Y0_N2
\ctrl_state[51]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.btn_up~q\,
	devoe => ww_devoe,
	o => \ctrl_state[51]~output_o\);

-- Location: IOOBUF_X52_Y0_N23
\ctrl_state[52]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.btn_z~q\,
	devoe => ww_devoe,
	o => \ctrl_state[52]~output_o\);

-- Location: IOOBUF_X54_Y0_N9
\ctrl_state[53]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.btn_r~q\,
	devoe => ww_devoe,
	o => \ctrl_state[53]~output_o\);

-- Location: IOOBUF_X49_Y73_N23
\ctrl_state[54]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.btn_l~q\,
	devoe => ww_devoe,
	o => \ctrl_state[54]~output_o\);

-- Location: IOOBUF_X20_Y73_N23
\ctrl_state[55]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \ctrl_state[55]~output_o\);

-- Location: IOOBUF_X45_Y73_N9
\ctrl_state[56]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.btn_a~q\,
	devoe => ww_devoe,
	o => \ctrl_state[56]~output_o\);

-- Location: IOOBUF_X49_Y73_N16
\ctrl_state[57]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.btn_b~q\,
	devoe => ww_devoe,
	o => \ctrl_state[57]~output_o\);

-- Location: IOOBUF_X52_Y73_N16
\ctrl_state[58]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.btn_x~q\,
	devoe => ww_devoe,
	o => \ctrl_state[58]~output_o\);

-- Location: IOOBUF_X42_Y73_N9
\ctrl_state[59]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.btn_y~q\,
	devoe => ww_devoe,
	o => \ctrl_state[59]~output_o\);

-- Location: IOOBUF_X47_Y73_N16
\ctrl_state[60]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \gc_ctrl_inst|state.ctrl_state.btn_start~q\,
	devoe => ww_devoe,
	o => \ctrl_state[60]~output_o\);

-- Location: IOOBUF_X38_Y73_N9
\ctrl_state[61]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \ctrl_state[61]~output_o\);

-- Location: IOOBUF_X115_Y22_N23
\ctrl_state[62]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \ctrl_state[62]~output_o\);

-- Location: IOOBUF_X115_Y28_N9
\ctrl_state[63]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \ctrl_state[63]~output_o\);

-- Location: IOIBUF_X0_Y36_N8
\clk~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk,
	o => \clk~input_o\);

-- Location: CLKCTRL_G2
\clk~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clk~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clk~inputclkctrl_outclk\);

-- Location: LCCOMB_X40_Y31_N0
\gc_ctrl_inst|Add0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~0_combout\ = \gc_ctrl_inst|state.clk_cnt\(0) $ (VCC)
-- \gc_ctrl_inst|Add0~1\ = CARRY(\gc_ctrl_inst|state.clk_cnt\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt\(0),
	datad => VCC,
	combout => \gc_ctrl_inst|Add0~0_combout\,
	cout => \gc_ctrl_inst|Add0~1\);

-- Location: LCCOMB_X41_Y31_N20
\gc_ctrl_inst|Selector63~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector63~0_combout\ = (\gc_ctrl_inst|Add0~0_combout\ & !\gc_ctrl_inst|state.clk_cnt[6]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|Add0~0_combout\,
	datad => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	combout => \gc_ctrl_inst|Selector63~0_combout\);

-- Location: IOIBUF_X0_Y36_N15
\res_n~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_res_n,
	o => \res_n~input_o\);

-- Location: CLKCTRL_G4
\res_n~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \res_n~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \res_n~inputclkctrl_outclk\);

-- Location: LCCOMB_X41_Y32_N22
\gc_ctrl_inst|Selector1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector1~0_combout\ = (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & !\gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000100010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datab => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	combout => \gc_ctrl_inst|Selector1~0_combout\);

-- Location: LCCOMB_X41_Y31_N14
\gc_ctrl_inst|Equal0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal0~6_combout\ = (!\gc_ctrl_inst|state.clk_cnt\(3) & (\gc_ctrl_inst|state.clk_cnt\(4) & !\gc_ctrl_inst|state.clk_cnt\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt\(3),
	datac => \gc_ctrl_inst|state.clk_cnt\(4),
	datad => \gc_ctrl_inst|state.clk_cnt\(0),
	combout => \gc_ctrl_inst|Equal0~6_combout\);

-- Location: LCCOMB_X40_Y32_N28
\gc_ctrl_inst|Equal1~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal1~3_combout\ = (\gc_ctrl_inst|Equal0~6_combout\ & (!\gc_ctrl_inst|state.clk_cnt\(5) & (\gc_ctrl_inst|state.clk_cnt\(2) & \gc_ctrl_inst|state.clk_cnt\(7))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|Equal0~6_combout\,
	datab => \gc_ctrl_inst|state.clk_cnt\(5),
	datac => \gc_ctrl_inst|state.clk_cnt\(2),
	datad => \gc_ctrl_inst|state.clk_cnt\(7),
	combout => \gc_ctrl_inst|Equal1~3_combout\);

-- Location: LCCOMB_X40_Y32_N22
\gc_ctrl_inst|Equal0~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal0~7_combout\ = (\gc_ctrl_inst|Equal0~6_combout\ & (\gc_ctrl_inst|state.clk_cnt\(5) & (!\gc_ctrl_inst|state.clk_cnt\(2) & !\gc_ctrl_inst|state.clk_cnt\(7))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|Equal0~6_combout\,
	datab => \gc_ctrl_inst|state.clk_cnt\(5),
	datac => \gc_ctrl_inst|state.clk_cnt\(2),
	datad => \gc_ctrl_inst|state.clk_cnt\(7),
	combout => \gc_ctrl_inst|Equal0~7_combout\);

-- Location: LCCOMB_X39_Y31_N2
\gc_ctrl_inst|Equal0~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal0~8_combout\ = (\gc_ctrl_inst|state.clk_cnt\(9) & (\gc_ctrl_inst|state.clk_cnt\(8) & (\gc_ctrl_inst|state.clk_cnt\(10) & \gc_ctrl_inst|state.clk_cnt\(12))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt\(9),
	datab => \gc_ctrl_inst|state.clk_cnt\(8),
	datac => \gc_ctrl_inst|state.clk_cnt\(10),
	datad => \gc_ctrl_inst|state.clk_cnt\(12),
	combout => \gc_ctrl_inst|Equal0~8_combout\);

-- Location: LCCOMB_X40_Y32_N8
\gc_ctrl_inst|Equal0~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal0~9_combout\ = (\gc_ctrl_inst|state.clk_cnt\(6) & (\gc_ctrl_inst|Equal0~8_combout\ & !\gc_ctrl_inst|state.clk_cnt\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt\(6),
	datac => \gc_ctrl_inst|Equal0~8_combout\,
	datad => \gc_ctrl_inst|state.clk_cnt\(1),
	combout => \gc_ctrl_inst|Equal0~9_combout\);

-- Location: LCCOMB_X40_Y32_N26
\gc_ctrl_inst|Equal0~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal0~10_combout\ = (\gc_ctrl_inst|Equal0~5_combout\ & (\gc_ctrl_inst|Equal0~9_combout\ & (\gc_ctrl_inst|Equal0~7_combout\ & \gc_ctrl_inst|Equal0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|Equal0~5_combout\,
	datab => \gc_ctrl_inst|Equal0~9_combout\,
	datac => \gc_ctrl_inst|Equal0~7_combout\,
	datad => \gc_ctrl_inst|Equal0~0_combout\,
	combout => \gc_ctrl_inst|Equal0~10_combout\);

-- Location: LCCOMB_X42_Y35_N0
\gc_ctrl_inst|Add1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~0_combout\ = \gc_ctrl_inst|state.data_cnt\(0) $ (VCC)
-- \gc_ctrl_inst|Add1~1\ = CARRY(\gc_ctrl_inst|state.data_cnt\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_cnt\(0),
	datad => VCC,
	combout => \gc_ctrl_inst|Add1~0_combout\,
	cout => \gc_ctrl_inst|Add1~1\);

-- Location: LCCOMB_X41_Y35_N16
\gc_ctrl_inst|Selector95~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector95~0_combout\ = (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & (\gc_ctrl_inst|Add1~0_combout\ & \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datac => \gc_ctrl_inst|Add1~0_combout\,
	datad => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	combout => \gc_ctrl_inst|Selector95~0_combout\);

-- Location: LCCOMB_X42_Y35_N6
\gc_ctrl_inst|Add1~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~6_combout\ = (\gc_ctrl_inst|state.data_cnt\(3) & (!\gc_ctrl_inst|Add1~5\)) # (!\gc_ctrl_inst|state.data_cnt\(3) & ((\gc_ctrl_inst|Add1~5\) # (GND)))
-- \gc_ctrl_inst|Add1~7\ = CARRY((!\gc_ctrl_inst|Add1~5\) # (!\gc_ctrl_inst|state.data_cnt\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.data_cnt\(3),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~5\,
	combout => \gc_ctrl_inst|Add1~6_combout\,
	cout => \gc_ctrl_inst|Add1~7\);

-- Location: LCCOMB_X42_Y35_N8
\gc_ctrl_inst|Add1~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~8_combout\ = (\gc_ctrl_inst|state.data_cnt\(4) & (\gc_ctrl_inst|Add1~7\ $ (GND))) # (!\gc_ctrl_inst|state.data_cnt\(4) & (!\gc_ctrl_inst|Add1~7\ & VCC))
-- \gc_ctrl_inst|Add1~9\ = CARRY((\gc_ctrl_inst|state.data_cnt\(4) & !\gc_ctrl_inst|Add1~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_cnt\(4),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~7\,
	combout => \gc_ctrl_inst|Add1~8_combout\,
	cout => \gc_ctrl_inst|Add1~9\);

-- Location: LCCOMB_X41_Y35_N12
\gc_ctrl_inst|Selector91~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector91~0_combout\ = (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & (\gc_ctrl_inst|Add1~8_combout\ & \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datac => \gc_ctrl_inst|Add1~8_combout\,
	datad => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	combout => \gc_ctrl_inst|Selector91~0_combout\);

-- Location: FF_X41_Y35_N13
\gc_ctrl_inst|state.data_cnt[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector91~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(4));

-- Location: LCCOMB_X41_Y35_N30
\gc_ctrl_inst|Equal4~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal4~11_combout\ = (\gc_ctrl_inst|state.data_cnt\(3) & \gc_ctrl_inst|state.data_cnt\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_cnt\(3),
	datad => \gc_ctrl_inst|state.data_cnt\(4),
	combout => \gc_ctrl_inst|Equal4~11_combout\);

-- Location: LCCOMB_X41_Y35_N8
\gc_ctrl_inst|LessThan0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|LessThan0~0_combout\ = ((!\gc_ctrl_inst|state.data_cnt\(2) & (!\gc_ctrl_inst|state.data_cnt\(1) & !\gc_ctrl_inst|state.data_cnt\(0)))) # (!\gc_ctrl_inst|Equal4~11_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101010111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|Equal4~11_combout\,
	datab => \gc_ctrl_inst|state.data_cnt\(2),
	datac => \gc_ctrl_inst|state.data_cnt\(1),
	datad => \gc_ctrl_inst|state.data_cnt\(0),
	combout => \gc_ctrl_inst|LessThan0~0_combout\);

-- Location: LCCOMB_X42_Y35_N10
\gc_ctrl_inst|Add1~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~10_combout\ = (\gc_ctrl_inst|state.data_cnt\(5) & (!\gc_ctrl_inst|Add1~9\)) # (!\gc_ctrl_inst|state.data_cnt\(5) & ((\gc_ctrl_inst|Add1~9\) # (GND)))
-- \gc_ctrl_inst|Add1~11\ = CARRY((!\gc_ctrl_inst|Add1~9\) # (!\gc_ctrl_inst|state.data_cnt\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_cnt\(5),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~9\,
	combout => \gc_ctrl_inst|Add1~10_combout\,
	cout => \gc_ctrl_inst|Add1~11\);

-- Location: LCCOMB_X41_Y35_N0
\gc_ctrl_inst|Selector90~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector90~0_combout\ = (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & (\gc_ctrl_inst|Add1~10_combout\ & \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datac => \gc_ctrl_inst|Add1~10_combout\,
	datad => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	combout => \gc_ctrl_inst|Selector90~0_combout\);

-- Location: FF_X41_Y35_N1
\gc_ctrl_inst|state.data_cnt[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector90~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(5));

-- Location: LCCOMB_X40_Y32_N30
\gc_ctrl_inst|Equal3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal3~0_combout\ = (!\gc_ctrl_inst|state.clk_cnt\(2) & (\gc_ctrl_inst|state.clk_cnt\(6) & (!\gc_ctrl_inst|state.clk_cnt\(5) & !\gc_ctrl_inst|state.clk_cnt\(7))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt\(2),
	datab => \gc_ctrl_inst|state.clk_cnt\(6),
	datac => \gc_ctrl_inst|state.clk_cnt\(5),
	datad => \gc_ctrl_inst|state.clk_cnt\(7),
	combout => \gc_ctrl_inst|Equal3~0_combout\);

-- Location: LCCOMB_X41_Y31_N28
\gc_ctrl_inst|Equal3~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal3~1_combout\ = (\gc_ctrl_inst|state.clk_cnt\(3) & (!\gc_ctrl_inst|state.clk_cnt\(4) & (\gc_ctrl_inst|Equal3~0_combout\ & \gc_ctrl_inst|state.clk_cnt\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt\(3),
	datab => \gc_ctrl_inst|state.clk_cnt\(4),
	datac => \gc_ctrl_inst|Equal3~0_combout\,
	datad => \gc_ctrl_inst|state.clk_cnt\(0),
	combout => \gc_ctrl_inst|Equal3~1_combout\);

-- Location: IOIBUF_X0_Y32_N15
\data~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => data,
	o => \data~input_o\);

-- Location: FF_X41_Y32_N23
\gc_ctrl_inst|sync_shiftreg[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \data~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|sync_shiftreg\(0));

-- Location: FF_X41_Y32_N17
\gc_ctrl_inst|sync_shiftreg[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|sync_shiftreg\(0),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|sync_shiftreg\(1));

-- Location: LCCOMB_X41_Y32_N28
\gc_ctrl_inst|data_synced~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|data_synced~feeder_combout\ = \gc_ctrl_inst|sync_shiftreg\(1)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|sync_shiftreg\(1),
	combout => \gc_ctrl_inst|data_synced~feeder_combout\);

-- Location: FF_X41_Y32_N29
\gc_ctrl_inst|data_synced\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|data_synced~feeder_combout\,
	ena => \res_n~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|data_synced~q\);

-- Location: LCCOMB_X42_Y32_N30
\gc_ctrl_inst|state.data_synced_old~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.data_synced_old~0_combout\ = !\gc_ctrl_inst|data_synced~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|data_synced~q\,
	combout => \gc_ctrl_inst|state.data_synced_old~0_combout\);

-- Location: FF_X42_Y32_N31
\gc_ctrl_inst|state.data_synced_old\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.data_synced_old~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_synced_old~q\);

-- Location: LCCOMB_X42_Y32_N4
\gc_ctrl_inst|Selector5~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector5~1_combout\ = ((\gc_ctrl_inst|state.fsm_state.WAIT_FALLING_EDGE_DATA~q\ & ((\gc_ctrl_inst|state.data_synced_old~q\) # (\gc_ctrl_inst|data_synced~q\)))) # (!\gc_ctrl_inst|Selector5~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001110110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_synced_old~q\,
	datab => \gc_ctrl_inst|Selector5~0_combout\,
	datac => \gc_ctrl_inst|state.fsm_state.WAIT_FALLING_EDGE_DATA~q\,
	datad => \gc_ctrl_inst|data_synced~q\,
	combout => \gc_ctrl_inst|Selector5~1_combout\);

-- Location: FF_X42_Y32_N5
\gc_ctrl_inst|state.fsm_state.WAIT_FALLING_EDGE_DATA\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector5~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.fsm_state.WAIT_FALLING_EDGE_DATA~q\);

-- Location: LCCOMB_X42_Y32_N24
\gc_ctrl_inst|Selector6~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector6~5_combout\ = (!\gc_ctrl_inst|state.data_synced_old~q\ & (\gc_ctrl_inst|state.fsm_state.WAIT_FALLING_EDGE_DATA~q\ & !\gc_ctrl_inst|data_synced~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_synced_old~q\,
	datab => \gc_ctrl_inst|state.fsm_state.WAIT_FALLING_EDGE_DATA~q\,
	datad => \gc_ctrl_inst|data_synced~q\,
	combout => \gc_ctrl_inst|Selector6~5_combout\);

-- Location: LCCOMB_X41_Y32_N24
\gc_ctrl_inst|Selector4~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector4~0_combout\ = (\gc_ctrl_inst|Selector6~5_combout\) # ((\gc_ctrl_inst|state.fsm_state.WAIT_SAMPLE~q\ & ((!\gc_ctrl_inst|Equal1~1_combout\) # (!\gc_ctrl_inst|Equal3~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|Equal3~1_combout\,
	datab => \gc_ctrl_inst|Selector6~5_combout\,
	datac => \gc_ctrl_inst|state.fsm_state.WAIT_SAMPLE~q\,
	datad => \gc_ctrl_inst|Equal1~1_combout\,
	combout => \gc_ctrl_inst|Selector4~0_combout\);

-- Location: FF_X41_Y32_N25
\gc_ctrl_inst|state.fsm_state.WAIT_SAMPLE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector4~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.fsm_state.WAIT_SAMPLE~q\);

-- Location: LCCOMB_X42_Y35_N12
\gc_ctrl_inst|Add1~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~12_combout\ = (\gc_ctrl_inst|state.data_cnt\(6) & (\gc_ctrl_inst|Add1~11\ $ (GND))) # (!\gc_ctrl_inst|state.data_cnt\(6) & (!\gc_ctrl_inst|Add1~11\ & VCC))
-- \gc_ctrl_inst|Add1~13\ = CARRY((\gc_ctrl_inst|state.data_cnt\(6) & !\gc_ctrl_inst|Add1~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_cnt\(6),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~11\,
	combout => \gc_ctrl_inst|Add1~12_combout\,
	cout => \gc_ctrl_inst|Add1~13\);

-- Location: LCCOMB_X43_Y35_N20
\gc_ctrl_inst|Selector89~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector89~0_combout\ = (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & (\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & \gc_ctrl_inst|Add1~12_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datac => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datad => \gc_ctrl_inst|Add1~12_combout\,
	combout => \gc_ctrl_inst|Selector89~0_combout\);

-- Location: FF_X43_Y35_N21
\gc_ctrl_inst|state.data_cnt[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector89~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(6));

-- Location: LCCOMB_X42_Y35_N14
\gc_ctrl_inst|Add1~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~14_combout\ = (\gc_ctrl_inst|state.data_cnt\(7) & (!\gc_ctrl_inst|Add1~13\)) # (!\gc_ctrl_inst|state.data_cnt\(7) & ((\gc_ctrl_inst|Add1~13\) # (GND)))
-- \gc_ctrl_inst|Add1~15\ = CARRY((!\gc_ctrl_inst|Add1~13\) # (!\gc_ctrl_inst|state.data_cnt\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.data_cnt\(7),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~13\,
	combout => \gc_ctrl_inst|Add1~14_combout\,
	cout => \gc_ctrl_inst|Add1~15\);

-- Location: LCCOMB_X43_Y35_N24
\gc_ctrl_inst|Selector88~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector88~0_combout\ = (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & (\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & \gc_ctrl_inst|Add1~14_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datac => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datad => \gc_ctrl_inst|Add1~14_combout\,
	combout => \gc_ctrl_inst|Selector88~0_combout\);

-- Location: FF_X43_Y35_N25
\gc_ctrl_inst|state.data_cnt[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector88~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(7));

-- Location: LCCOMB_X42_Y35_N16
\gc_ctrl_inst|Add1~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~16_combout\ = (\gc_ctrl_inst|state.data_cnt\(8) & (\gc_ctrl_inst|Add1~15\ $ (GND))) # (!\gc_ctrl_inst|state.data_cnt\(8) & (!\gc_ctrl_inst|Add1~15\ & VCC))
-- \gc_ctrl_inst|Add1~17\ = CARRY((\gc_ctrl_inst|state.data_cnt\(8) & !\gc_ctrl_inst|Add1~15\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.data_cnt\(8),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~15\,
	combout => \gc_ctrl_inst|Add1~16_combout\,
	cout => \gc_ctrl_inst|Add1~17\);

-- Location: LCCOMB_X43_Y35_N14
\gc_ctrl_inst|Selector87~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector87~0_combout\ = (\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & (\gc_ctrl_inst|Add1~16_combout\ & !\gc_ctrl_inst|state.fsm_state.POLL~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datac => \gc_ctrl_inst|Add1~16_combout\,
	datad => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	combout => \gc_ctrl_inst|Selector87~0_combout\);

-- Location: FF_X43_Y35_N15
\gc_ctrl_inst|state.data_cnt[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector87~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(8));

-- Location: LCCOMB_X42_Y35_N18
\gc_ctrl_inst|Add1~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~18_combout\ = (\gc_ctrl_inst|state.data_cnt\(9) & (!\gc_ctrl_inst|Add1~17\)) # (!\gc_ctrl_inst|state.data_cnt\(9) & ((\gc_ctrl_inst|Add1~17\) # (GND)))
-- \gc_ctrl_inst|Add1~19\ = CARRY((!\gc_ctrl_inst|Add1~17\) # (!\gc_ctrl_inst|state.data_cnt\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_cnt\(9),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~17\,
	combout => \gc_ctrl_inst|Add1~18_combout\,
	cout => \gc_ctrl_inst|Add1~19\);

-- Location: LCCOMB_X43_Y35_N4
\gc_ctrl_inst|Selector86~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector86~0_combout\ = (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & (\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & \gc_ctrl_inst|Add1~18_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datac => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datad => \gc_ctrl_inst|Add1~18_combout\,
	combout => \gc_ctrl_inst|Selector86~0_combout\);

-- Location: FF_X43_Y35_N5
\gc_ctrl_inst|state.data_cnt[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector86~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(9));

-- Location: LCCOMB_X42_Y35_N20
\gc_ctrl_inst|Add1~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~20_combout\ = (\gc_ctrl_inst|state.data_cnt\(10) & (\gc_ctrl_inst|Add1~19\ $ (GND))) # (!\gc_ctrl_inst|state.data_cnt\(10) & (!\gc_ctrl_inst|Add1~19\ & VCC))
-- \gc_ctrl_inst|Add1~21\ = CARRY((\gc_ctrl_inst|state.data_cnt\(10) & !\gc_ctrl_inst|Add1~19\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.data_cnt\(10),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~19\,
	combout => \gc_ctrl_inst|Add1~20_combout\,
	cout => \gc_ctrl_inst|Add1~21\);

-- Location: LCCOMB_X43_Y35_N10
\gc_ctrl_inst|Selector85~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector85~0_combout\ = (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & (\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & \gc_ctrl_inst|Add1~20_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datac => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datad => \gc_ctrl_inst|Add1~20_combout\,
	combout => \gc_ctrl_inst|Selector85~0_combout\);

-- Location: FF_X43_Y35_N11
\gc_ctrl_inst|state.data_cnt[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector85~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(10));

-- Location: LCCOMB_X42_Y35_N22
\gc_ctrl_inst|Add1~22\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~22_combout\ = (\gc_ctrl_inst|state.data_cnt\(11) & (!\gc_ctrl_inst|Add1~21\)) # (!\gc_ctrl_inst|state.data_cnt\(11) & ((\gc_ctrl_inst|Add1~21\) # (GND)))
-- \gc_ctrl_inst|Add1~23\ = CARRY((!\gc_ctrl_inst|Add1~21\) # (!\gc_ctrl_inst|state.data_cnt\(11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_cnt\(11),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~21\,
	combout => \gc_ctrl_inst|Add1~22_combout\,
	cout => \gc_ctrl_inst|Add1~23\);

-- Location: LCCOMB_X43_Y35_N6
\gc_ctrl_inst|Selector84~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector84~0_combout\ = (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & (\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & \gc_ctrl_inst|Add1~22_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datac => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datad => \gc_ctrl_inst|Add1~22_combout\,
	combout => \gc_ctrl_inst|Selector84~0_combout\);

-- Location: FF_X43_Y35_N7
\gc_ctrl_inst|state.data_cnt[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector84~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(11));

-- Location: LCCOMB_X42_Y35_N24
\gc_ctrl_inst|Add1~24\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~24_combout\ = (\gc_ctrl_inst|state.data_cnt\(12) & (\gc_ctrl_inst|Add1~23\ $ (GND))) # (!\gc_ctrl_inst|state.data_cnt\(12) & (!\gc_ctrl_inst|Add1~23\ & VCC))
-- \gc_ctrl_inst|Add1~25\ = CARRY((\gc_ctrl_inst|state.data_cnt\(12) & !\gc_ctrl_inst|Add1~23\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.data_cnt\(12),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~23\,
	combout => \gc_ctrl_inst|Add1~24_combout\,
	cout => \gc_ctrl_inst|Add1~25\);

-- Location: LCCOMB_X43_Y35_N28
\gc_ctrl_inst|Selector83~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector83~0_combout\ = (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & (\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & \gc_ctrl_inst|Add1~24_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datac => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datad => \gc_ctrl_inst|Add1~24_combout\,
	combout => \gc_ctrl_inst|Selector83~0_combout\);

-- Location: FF_X43_Y35_N29
\gc_ctrl_inst|state.data_cnt[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector83~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(12));

-- Location: LCCOMB_X42_Y35_N26
\gc_ctrl_inst|Add1~26\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~26_combout\ = (\gc_ctrl_inst|state.data_cnt\(13) & (!\gc_ctrl_inst|Add1~25\)) # (!\gc_ctrl_inst|state.data_cnt\(13) & ((\gc_ctrl_inst|Add1~25\) # (GND)))
-- \gc_ctrl_inst|Add1~27\ = CARRY((!\gc_ctrl_inst|Add1~25\) # (!\gc_ctrl_inst|state.data_cnt\(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_cnt\(13),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~25\,
	combout => \gc_ctrl_inst|Add1~26_combout\,
	cout => \gc_ctrl_inst|Add1~27\);

-- Location: LCCOMB_X43_Y35_N2
\gc_ctrl_inst|Selector82~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector82~0_combout\ = (\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & (\gc_ctrl_inst|Add1~26_combout\ & !\gc_ctrl_inst|state.fsm_state.POLL~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datac => \gc_ctrl_inst|Add1~26_combout\,
	datad => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	combout => \gc_ctrl_inst|Selector82~0_combout\);

-- Location: FF_X43_Y35_N3
\gc_ctrl_inst|state.data_cnt[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector82~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(13));

-- Location: LCCOMB_X42_Y35_N28
\gc_ctrl_inst|Add1~28\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~28_combout\ = (\gc_ctrl_inst|state.data_cnt\(14) & (\gc_ctrl_inst|Add1~27\ $ (GND))) # (!\gc_ctrl_inst|state.data_cnt\(14) & (!\gc_ctrl_inst|Add1~27\ & VCC))
-- \gc_ctrl_inst|Add1~29\ = CARRY((\gc_ctrl_inst|state.data_cnt\(14) & !\gc_ctrl_inst|Add1~27\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.data_cnt\(14),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~27\,
	combout => \gc_ctrl_inst|Add1~28_combout\,
	cout => \gc_ctrl_inst|Add1~29\);

-- Location: LCCOMB_X43_Y35_N0
\gc_ctrl_inst|Selector81~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector81~0_combout\ = (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & (\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & \gc_ctrl_inst|Add1~28_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datac => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datad => \gc_ctrl_inst|Add1~28_combout\,
	combout => \gc_ctrl_inst|Selector81~0_combout\);

-- Location: FF_X43_Y35_N1
\gc_ctrl_inst|state.data_cnt[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector81~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(14));

-- Location: LCCOMB_X42_Y35_N30
\gc_ctrl_inst|Add1~30\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~30_combout\ = (\gc_ctrl_inst|state.data_cnt\(15) & (!\gc_ctrl_inst|Add1~29\)) # (!\gc_ctrl_inst|state.data_cnt\(15) & ((\gc_ctrl_inst|Add1~29\) # (GND)))
-- \gc_ctrl_inst|Add1~31\ = CARRY((!\gc_ctrl_inst|Add1~29\) # (!\gc_ctrl_inst|state.data_cnt\(15)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_cnt\(15),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~29\,
	combout => \gc_ctrl_inst|Add1~30_combout\,
	cout => \gc_ctrl_inst|Add1~31\);

-- Location: LCCOMB_X41_Y35_N26
\gc_ctrl_inst|Selector80~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector80~0_combout\ = (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & (\gc_ctrl_inst|Add1~30_combout\ & \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datac => \gc_ctrl_inst|Add1~30_combout\,
	datad => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	combout => \gc_ctrl_inst|Selector80~0_combout\);

-- Location: FF_X41_Y35_N27
\gc_ctrl_inst|state.data_cnt[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector80~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(15));

-- Location: LCCOMB_X42_Y34_N0
\gc_ctrl_inst|Add1~32\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~32_combout\ = (\gc_ctrl_inst|state.data_cnt\(16) & (\gc_ctrl_inst|Add1~31\ $ (GND))) # (!\gc_ctrl_inst|state.data_cnt\(16) & (!\gc_ctrl_inst|Add1~31\ & VCC))
-- \gc_ctrl_inst|Add1~33\ = CARRY((\gc_ctrl_inst|state.data_cnt\(16) & !\gc_ctrl_inst|Add1~31\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_cnt\(16),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~31\,
	combout => \gc_ctrl_inst|Add1~32_combout\,
	cout => \gc_ctrl_inst|Add1~33\);

-- Location: LCCOMB_X43_Y34_N2
\gc_ctrl_inst|Selector79~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector79~0_combout\ = (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & (\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & \gc_ctrl_inst|Add1~32_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datac => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datad => \gc_ctrl_inst|Add1~32_combout\,
	combout => \gc_ctrl_inst|Selector79~0_combout\);

-- Location: FF_X43_Y34_N3
\gc_ctrl_inst|state.data_cnt[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector79~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(16));

-- Location: LCCOMB_X42_Y34_N2
\gc_ctrl_inst|Add1~34\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~34_combout\ = (\gc_ctrl_inst|state.data_cnt\(17) & (!\gc_ctrl_inst|Add1~33\)) # (!\gc_ctrl_inst|state.data_cnt\(17) & ((\gc_ctrl_inst|Add1~33\) # (GND)))
-- \gc_ctrl_inst|Add1~35\ = CARRY((!\gc_ctrl_inst|Add1~33\) # (!\gc_ctrl_inst|state.data_cnt\(17)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_cnt\(17),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~33\,
	combout => \gc_ctrl_inst|Add1~34_combout\,
	cout => \gc_ctrl_inst|Add1~35\);

-- Location: LCCOMB_X43_Y34_N0
\gc_ctrl_inst|Selector78~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector78~0_combout\ = (\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & \gc_ctrl_inst|Add1~34_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datac => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datad => \gc_ctrl_inst|Add1~34_combout\,
	combout => \gc_ctrl_inst|Selector78~0_combout\);

-- Location: FF_X43_Y34_N1
\gc_ctrl_inst|state.data_cnt[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector78~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(17));

-- Location: LCCOMB_X42_Y34_N4
\gc_ctrl_inst|Add1~36\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~36_combout\ = (\gc_ctrl_inst|state.data_cnt\(18) & (\gc_ctrl_inst|Add1~35\ $ (GND))) # (!\gc_ctrl_inst|state.data_cnt\(18) & (!\gc_ctrl_inst|Add1~35\ & VCC))
-- \gc_ctrl_inst|Add1~37\ = CARRY((\gc_ctrl_inst|state.data_cnt\(18) & !\gc_ctrl_inst|Add1~35\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_cnt\(18),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~35\,
	combout => \gc_ctrl_inst|Add1~36_combout\,
	cout => \gc_ctrl_inst|Add1~37\);

-- Location: LCCOMB_X43_Y34_N30
\gc_ctrl_inst|Selector77~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector77~0_combout\ = (\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & (\gc_ctrl_inst|Add1~36_combout\ & !\gc_ctrl_inst|state.fsm_state.POLL~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datac => \gc_ctrl_inst|Add1~36_combout\,
	datad => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	combout => \gc_ctrl_inst|Selector77~0_combout\);

-- Location: FF_X43_Y34_N31
\gc_ctrl_inst|state.data_cnt[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector77~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(18));

-- Location: LCCOMB_X42_Y34_N6
\gc_ctrl_inst|Add1~38\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~38_combout\ = (\gc_ctrl_inst|state.data_cnt\(19) & (!\gc_ctrl_inst|Add1~37\)) # (!\gc_ctrl_inst|state.data_cnt\(19) & ((\gc_ctrl_inst|Add1~37\) # (GND)))
-- \gc_ctrl_inst|Add1~39\ = CARRY((!\gc_ctrl_inst|Add1~37\) # (!\gc_ctrl_inst|state.data_cnt\(19)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.data_cnt\(19),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~37\,
	combout => \gc_ctrl_inst|Add1~38_combout\,
	cout => \gc_ctrl_inst|Add1~39\);

-- Location: LCCOMB_X43_Y34_N18
\gc_ctrl_inst|Selector76~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector76~0_combout\ = (\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & (\gc_ctrl_inst|Add1~38_combout\ & !\gc_ctrl_inst|state.fsm_state.POLL~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datac => \gc_ctrl_inst|Add1~38_combout\,
	datad => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	combout => \gc_ctrl_inst|Selector76~0_combout\);

-- Location: FF_X43_Y34_N19
\gc_ctrl_inst|state.data_cnt[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector76~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(19));

-- Location: LCCOMB_X42_Y34_N8
\gc_ctrl_inst|Add1~40\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~40_combout\ = (\gc_ctrl_inst|state.data_cnt\(20) & (\gc_ctrl_inst|Add1~39\ $ (GND))) # (!\gc_ctrl_inst|state.data_cnt\(20) & (!\gc_ctrl_inst|Add1~39\ & VCC))
-- \gc_ctrl_inst|Add1~41\ = CARRY((\gc_ctrl_inst|state.data_cnt\(20) & !\gc_ctrl_inst|Add1~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.data_cnt\(20),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~39\,
	combout => \gc_ctrl_inst|Add1~40_combout\,
	cout => \gc_ctrl_inst|Add1~41\);

-- Location: LCCOMB_X43_Y34_N16
\gc_ctrl_inst|Selector75~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector75~0_combout\ = (\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & \gc_ctrl_inst|Add1~40_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datac => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datad => \gc_ctrl_inst|Add1~40_combout\,
	combout => \gc_ctrl_inst|Selector75~0_combout\);

-- Location: FF_X43_Y34_N17
\gc_ctrl_inst|state.data_cnt[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector75~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(20));

-- Location: LCCOMB_X42_Y34_N10
\gc_ctrl_inst|Add1~42\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~42_combout\ = (\gc_ctrl_inst|state.data_cnt\(21) & (!\gc_ctrl_inst|Add1~41\)) # (!\gc_ctrl_inst|state.data_cnt\(21) & ((\gc_ctrl_inst|Add1~41\) # (GND)))
-- \gc_ctrl_inst|Add1~43\ = CARRY((!\gc_ctrl_inst|Add1~41\) # (!\gc_ctrl_inst|state.data_cnt\(21)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_cnt\(21),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~41\,
	combout => \gc_ctrl_inst|Add1~42_combout\,
	cout => \gc_ctrl_inst|Add1~43\);

-- Location: LCCOMB_X43_Y34_N6
\gc_ctrl_inst|Selector74~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector74~0_combout\ = (\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & \gc_ctrl_inst|Add1~42_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datac => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datad => \gc_ctrl_inst|Add1~42_combout\,
	combout => \gc_ctrl_inst|Selector74~0_combout\);

-- Location: FF_X43_Y34_N7
\gc_ctrl_inst|state.data_cnt[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector74~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(21));

-- Location: LCCOMB_X42_Y34_N12
\gc_ctrl_inst|Add1~44\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~44_combout\ = (\gc_ctrl_inst|state.data_cnt\(22) & (\gc_ctrl_inst|Add1~43\ $ (GND))) # (!\gc_ctrl_inst|state.data_cnt\(22) & (!\gc_ctrl_inst|Add1~43\ & VCC))
-- \gc_ctrl_inst|Add1~45\ = CARRY((\gc_ctrl_inst|state.data_cnt\(22) & !\gc_ctrl_inst|Add1~43\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_cnt\(22),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~43\,
	combout => \gc_ctrl_inst|Add1~44_combout\,
	cout => \gc_ctrl_inst|Add1~45\);

-- Location: LCCOMB_X43_Y34_N20
\gc_ctrl_inst|Selector73~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector73~0_combout\ = (\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & \gc_ctrl_inst|Add1~44_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datac => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datad => \gc_ctrl_inst|Add1~44_combout\,
	combout => \gc_ctrl_inst|Selector73~0_combout\);

-- Location: FF_X43_Y34_N21
\gc_ctrl_inst|state.data_cnt[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector73~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(22));

-- Location: LCCOMB_X42_Y34_N14
\gc_ctrl_inst|Add1~46\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~46_combout\ = (\gc_ctrl_inst|state.data_cnt\(23) & (!\gc_ctrl_inst|Add1~45\)) # (!\gc_ctrl_inst|state.data_cnt\(23) & ((\gc_ctrl_inst|Add1~45\) # (GND)))
-- \gc_ctrl_inst|Add1~47\ = CARRY((!\gc_ctrl_inst|Add1~45\) # (!\gc_ctrl_inst|state.data_cnt\(23)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_cnt\(23),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~45\,
	combout => \gc_ctrl_inst|Add1~46_combout\,
	cout => \gc_ctrl_inst|Add1~47\);

-- Location: LCCOMB_X43_Y35_N12
\gc_ctrl_inst|Selector72~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector72~0_combout\ = (\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & (\gc_ctrl_inst|Add1~46_combout\ & !\gc_ctrl_inst|state.fsm_state.POLL~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datac => \gc_ctrl_inst|Add1~46_combout\,
	datad => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	combout => \gc_ctrl_inst|Selector72~0_combout\);

-- Location: FF_X43_Y35_N13
\gc_ctrl_inst|state.data_cnt[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector72~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(23));

-- Location: LCCOMB_X42_Y34_N16
\gc_ctrl_inst|Add1~48\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~48_combout\ = (\gc_ctrl_inst|state.data_cnt\(24) & (\gc_ctrl_inst|Add1~47\ $ (GND))) # (!\gc_ctrl_inst|state.data_cnt\(24) & (!\gc_ctrl_inst|Add1~47\ & VCC))
-- \gc_ctrl_inst|Add1~49\ = CARRY((\gc_ctrl_inst|state.data_cnt\(24) & !\gc_ctrl_inst|Add1~47\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.data_cnt\(24),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~47\,
	combout => \gc_ctrl_inst|Add1~48_combout\,
	cout => \gc_ctrl_inst|Add1~49\);

-- Location: LCCOMB_X43_Y34_N26
\gc_ctrl_inst|Selector71~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector71~0_combout\ = (\gc_ctrl_inst|Add1~48_combout\ & (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|Add1~48_combout\,
	datab => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datac => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	combout => \gc_ctrl_inst|Selector71~0_combout\);

-- Location: FF_X43_Y34_N27
\gc_ctrl_inst|state.data_cnt[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector71~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(24));

-- Location: LCCOMB_X42_Y34_N18
\gc_ctrl_inst|Add1~50\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~50_combout\ = (\gc_ctrl_inst|state.data_cnt\(25) & (!\gc_ctrl_inst|Add1~49\)) # (!\gc_ctrl_inst|state.data_cnt\(25) & ((\gc_ctrl_inst|Add1~49\) # (GND)))
-- \gc_ctrl_inst|Add1~51\ = CARRY((!\gc_ctrl_inst|Add1~49\) # (!\gc_ctrl_inst|state.data_cnt\(25)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_cnt\(25),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~49\,
	combout => \gc_ctrl_inst|Add1~50_combout\,
	cout => \gc_ctrl_inst|Add1~51\);

-- Location: LCCOMB_X43_Y35_N18
\gc_ctrl_inst|Selector70~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector70~0_combout\ = (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & (\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & \gc_ctrl_inst|Add1~50_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datac => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datad => \gc_ctrl_inst|Add1~50_combout\,
	combout => \gc_ctrl_inst|Selector70~0_combout\);

-- Location: FF_X43_Y35_N19
\gc_ctrl_inst|state.data_cnt[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector70~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(25));

-- Location: LCCOMB_X42_Y34_N20
\gc_ctrl_inst|Add1~52\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~52_combout\ = (\gc_ctrl_inst|state.data_cnt\(26) & (\gc_ctrl_inst|Add1~51\ $ (GND))) # (!\gc_ctrl_inst|state.data_cnt\(26) & (!\gc_ctrl_inst|Add1~51\ & VCC))
-- \gc_ctrl_inst|Add1~53\ = CARRY((\gc_ctrl_inst|state.data_cnt\(26) & !\gc_ctrl_inst|Add1~51\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.data_cnt\(26),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~51\,
	combout => \gc_ctrl_inst|Add1~52_combout\,
	cout => \gc_ctrl_inst|Add1~53\);

-- Location: LCCOMB_X43_Y35_N8
\gc_ctrl_inst|Selector69~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector69~0_combout\ = (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & (\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & \gc_ctrl_inst|Add1~52_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datac => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datad => \gc_ctrl_inst|Add1~52_combout\,
	combout => \gc_ctrl_inst|Selector69~0_combout\);

-- Location: FF_X43_Y35_N9
\gc_ctrl_inst|state.data_cnt[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector69~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(26));

-- Location: LCCOMB_X42_Y34_N22
\gc_ctrl_inst|Add1~54\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~54_combout\ = (\gc_ctrl_inst|state.data_cnt\(27) & (!\gc_ctrl_inst|Add1~53\)) # (!\gc_ctrl_inst|state.data_cnt\(27) & ((\gc_ctrl_inst|Add1~53\) # (GND)))
-- \gc_ctrl_inst|Add1~55\ = CARRY((!\gc_ctrl_inst|Add1~53\) # (!\gc_ctrl_inst|state.data_cnt\(27)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.data_cnt\(27),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~53\,
	combout => \gc_ctrl_inst|Add1~54_combout\,
	cout => \gc_ctrl_inst|Add1~55\);

-- Location: LCCOMB_X43_Y34_N14
\gc_ctrl_inst|Selector68~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector68~0_combout\ = (\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & \gc_ctrl_inst|Add1~54_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datac => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datad => \gc_ctrl_inst|Add1~54_combout\,
	combout => \gc_ctrl_inst|Selector68~0_combout\);

-- Location: FF_X43_Y34_N15
\gc_ctrl_inst|state.data_cnt[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector68~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(27));

-- Location: LCCOMB_X42_Y34_N24
\gc_ctrl_inst|Add1~56\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~56_combout\ = (\gc_ctrl_inst|state.data_cnt\(28) & (\gc_ctrl_inst|Add1~55\ $ (GND))) # (!\gc_ctrl_inst|state.data_cnt\(28) & (!\gc_ctrl_inst|Add1~55\ & VCC))
-- \gc_ctrl_inst|Add1~57\ = CARRY((\gc_ctrl_inst|state.data_cnt\(28) & !\gc_ctrl_inst|Add1~55\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_cnt\(28),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~55\,
	combout => \gc_ctrl_inst|Add1~56_combout\,
	cout => \gc_ctrl_inst|Add1~57\);

-- Location: LCCOMB_X43_Y34_N4
\gc_ctrl_inst|Selector67~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector67~0_combout\ = (\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & \gc_ctrl_inst|Add1~56_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datac => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datad => \gc_ctrl_inst|Add1~56_combout\,
	combout => \gc_ctrl_inst|Selector67~0_combout\);

-- Location: FF_X43_Y34_N5
\gc_ctrl_inst|state.data_cnt[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector67~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(28));

-- Location: LCCOMB_X42_Y34_N26
\gc_ctrl_inst|Add1~58\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~58_combout\ = (\gc_ctrl_inst|state.data_cnt\(29) & (!\gc_ctrl_inst|Add1~57\)) # (!\gc_ctrl_inst|state.data_cnt\(29) & ((\gc_ctrl_inst|Add1~57\) # (GND)))
-- \gc_ctrl_inst|Add1~59\ = CARRY((!\gc_ctrl_inst|Add1~57\) # (!\gc_ctrl_inst|state.data_cnt\(29)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.data_cnt\(29),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~57\,
	combout => \gc_ctrl_inst|Add1~58_combout\,
	cout => \gc_ctrl_inst|Add1~59\);

-- Location: LCCOMB_X43_Y34_N10
\gc_ctrl_inst|Selector66~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector66~0_combout\ = (\gc_ctrl_inst|Add1~58_combout\ & (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|Add1~58_combout\,
	datab => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datac => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	combout => \gc_ctrl_inst|Selector66~0_combout\);

-- Location: FF_X43_Y34_N11
\gc_ctrl_inst|state.data_cnt[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector66~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(29));

-- Location: LCCOMB_X42_Y34_N28
\gc_ctrl_inst|Add1~60\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~60_combout\ = (\gc_ctrl_inst|state.data_cnt\(30) & (\gc_ctrl_inst|Add1~59\ $ (GND))) # (!\gc_ctrl_inst|state.data_cnt\(30) & (!\gc_ctrl_inst|Add1~59\ & VCC))
-- \gc_ctrl_inst|Add1~61\ = CARRY((\gc_ctrl_inst|state.data_cnt\(30) & !\gc_ctrl_inst|Add1~59\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.data_cnt\(30),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~59\,
	combout => \gc_ctrl_inst|Add1~60_combout\,
	cout => \gc_ctrl_inst|Add1~61\);

-- Location: LCCOMB_X43_Y34_N8
\gc_ctrl_inst|Selector65~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector65~0_combout\ = (\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & \gc_ctrl_inst|Add1~60_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datac => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datad => \gc_ctrl_inst|Add1~60_combout\,
	combout => \gc_ctrl_inst|Selector65~0_combout\);

-- Location: FF_X43_Y34_N9
\gc_ctrl_inst|state.data_cnt[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector65~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(30));

-- Location: LCCOMB_X42_Y34_N30
\gc_ctrl_inst|Add1~62\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~62_combout\ = \gc_ctrl_inst|state.data_cnt\(31) $ (\gc_ctrl_inst|Add1~61\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.data_cnt\(31),
	cin => \gc_ctrl_inst|Add1~61\,
	combout => \gc_ctrl_inst|Add1~62_combout\);

-- Location: LCCOMB_X42_Y32_N20
\gc_ctrl_inst|Selector64~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector64~2_combout\ = (\gc_ctrl_inst|Add1~62_combout\ & ((\gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\) # ((\gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\ & \gc_ctrl_inst|next_state_logic~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	datab => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	datac => \gc_ctrl_inst|next_state_logic~1_combout\,
	datad => \gc_ctrl_inst|Add1~62_combout\,
	combout => \gc_ctrl_inst|Selector64~2_combout\);

-- Location: LCCOMB_X42_Y32_N16
\gc_ctrl_inst|Selector64~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector64~0_combout\ = (\gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\ & (((!\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & !\gc_ctrl_inst|Equal0~10_combout\)) # (!\gc_ctrl_inst|next_state_logic~1_combout\))) # 
-- (!\gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\ & (((!\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & !\gc_ctrl_inst|Equal0~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001000101111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	datab => \gc_ctrl_inst|next_state_logic~1_combout\,
	datac => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datad => \gc_ctrl_inst|Equal0~10_combout\,
	combout => \gc_ctrl_inst|Selector64~0_combout\);

-- Location: LCCOMB_X42_Y32_N18
\gc_ctrl_inst|Selector64~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector64~1_combout\ = (\gc_ctrl_inst|state.fsm_state.SEND_LOW~q\) # ((\gc_ctrl_inst|Selector64~0_combout\) # ((\gc_ctrl_inst|state.fsm_state.WAIT_FALLING_EDGE_DATA~q\) # (\gc_ctrl_inst|state.fsm_state.POLL~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.SEND_LOW~q\,
	datab => \gc_ctrl_inst|Selector64~0_combout\,
	datac => \gc_ctrl_inst|state.fsm_state.WAIT_FALLING_EDGE_DATA~q\,
	datad => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	combout => \gc_ctrl_inst|Selector64~1_combout\);

-- Location: LCCOMB_X42_Y32_N8
\gc_ctrl_inst|Selector64~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector64~3_combout\ = (\gc_ctrl_inst|Selector64~2_combout\) # ((\gc_ctrl_inst|state.data_cnt\(31) & ((\gc_ctrl_inst|state.fsm_state.WAIT_SAMPLE~q\) # (\gc_ctrl_inst|Selector64~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.WAIT_SAMPLE~q\,
	datab => \gc_ctrl_inst|Selector64~2_combout\,
	datac => \gc_ctrl_inst|state.data_cnt\(31),
	datad => \gc_ctrl_inst|Selector64~1_combout\,
	combout => \gc_ctrl_inst|Selector64~3_combout\);

-- Location: FF_X42_Y32_N9
\gc_ctrl_inst|state.data_cnt[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector64~3_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(31));

-- Location: LCCOMB_X43_Y34_N12
\gc_ctrl_inst|Equal4~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal4~2_combout\ = (!\gc_ctrl_inst|state.data_cnt\(21) & (!\gc_ctrl_inst|state.data_cnt\(20) & (!\gc_ctrl_inst|state.data_cnt\(22) & !\gc_ctrl_inst|state.data_cnt\(19))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_cnt\(21),
	datab => \gc_ctrl_inst|state.data_cnt\(20),
	datac => \gc_ctrl_inst|state.data_cnt\(22),
	datad => \gc_ctrl_inst|state.data_cnt\(19),
	combout => \gc_ctrl_inst|Equal4~2_combout\);

-- Location: LCCOMB_X43_Y34_N28
\gc_ctrl_inst|Equal4~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal4~3_combout\ = (!\gc_ctrl_inst|state.data_cnt\(18) & (!\gc_ctrl_inst|state.data_cnt\(17) & (!\gc_ctrl_inst|state.data_cnt\(15) & !\gc_ctrl_inst|state.data_cnt\(16))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_cnt\(18),
	datab => \gc_ctrl_inst|state.data_cnt\(17),
	datac => \gc_ctrl_inst|state.data_cnt\(15),
	datad => \gc_ctrl_inst|state.data_cnt\(16),
	combout => \gc_ctrl_inst|Equal4~3_combout\);

-- Location: LCCOMB_X43_Y35_N30
\gc_ctrl_inst|Equal4~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal4~1_combout\ = (!\gc_ctrl_inst|state.data_cnt\(23) & (!\gc_ctrl_inst|state.data_cnt\(26) & (!\gc_ctrl_inst|state.data_cnt\(24) & !\gc_ctrl_inst|state.data_cnt\(25))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_cnt\(23),
	datab => \gc_ctrl_inst|state.data_cnt\(26),
	datac => \gc_ctrl_inst|state.data_cnt\(24),
	datad => \gc_ctrl_inst|state.data_cnt\(25),
	combout => \gc_ctrl_inst|Equal4~1_combout\);

-- Location: LCCOMB_X43_Y34_N24
\gc_ctrl_inst|Equal4~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal4~0_combout\ = (!\gc_ctrl_inst|state.data_cnt\(29) & (!\gc_ctrl_inst|state.data_cnt\(28) & (!\gc_ctrl_inst|state.data_cnt\(27) & !\gc_ctrl_inst|state.data_cnt\(30))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_cnt\(29),
	datab => \gc_ctrl_inst|state.data_cnt\(28),
	datac => \gc_ctrl_inst|state.data_cnt\(27),
	datad => \gc_ctrl_inst|state.data_cnt\(30),
	combout => \gc_ctrl_inst|Equal4~0_combout\);

-- Location: LCCOMB_X43_Y34_N22
\gc_ctrl_inst|Equal4~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal4~4_combout\ = (\gc_ctrl_inst|Equal4~2_combout\ & (\gc_ctrl_inst|Equal4~3_combout\ & (\gc_ctrl_inst|Equal4~1_combout\ & \gc_ctrl_inst|Equal4~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|Equal4~2_combout\,
	datab => \gc_ctrl_inst|Equal4~3_combout\,
	datac => \gc_ctrl_inst|Equal4~1_combout\,
	datad => \gc_ctrl_inst|Equal4~0_combout\,
	combout => \gc_ctrl_inst|Equal4~4_combout\);

-- Location: LCCOMB_X43_Y35_N26
\gc_ctrl_inst|Equal4~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal4~6_combout\ = (!\gc_ctrl_inst|state.data_cnt\(10) & (!\gc_ctrl_inst|state.data_cnt\(8) & (!\gc_ctrl_inst|state.data_cnt\(9) & !\gc_ctrl_inst|state.data_cnt\(7))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_cnt\(10),
	datab => \gc_ctrl_inst|state.data_cnt\(8),
	datac => \gc_ctrl_inst|state.data_cnt\(9),
	datad => \gc_ctrl_inst|state.data_cnt\(7),
	combout => \gc_ctrl_inst|Equal4~6_combout\);

-- Location: LCCOMB_X43_Y35_N16
\gc_ctrl_inst|Equal4~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal4~5_combout\ = (!\gc_ctrl_inst|state.data_cnt\(11) & (!\gc_ctrl_inst|state.data_cnt\(13) & (!\gc_ctrl_inst|state.data_cnt\(12) & !\gc_ctrl_inst|state.data_cnt\(14))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_cnt\(11),
	datab => \gc_ctrl_inst|state.data_cnt\(13),
	datac => \gc_ctrl_inst|state.data_cnt\(12),
	datad => \gc_ctrl_inst|state.data_cnt\(14),
	combout => \gc_ctrl_inst|Equal4~5_combout\);

-- Location: LCCOMB_X43_Y35_N22
\gc_ctrl_inst|Equal4~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal4~7_combout\ = (!\gc_ctrl_inst|state.data_cnt\(6) & (\gc_ctrl_inst|Equal4~4_combout\ & (\gc_ctrl_inst|Equal4~6_combout\ & \gc_ctrl_inst|Equal4~5_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_cnt\(6),
	datab => \gc_ctrl_inst|Equal4~4_combout\,
	datac => \gc_ctrl_inst|Equal4~6_combout\,
	datad => \gc_ctrl_inst|Equal4~5_combout\,
	combout => \gc_ctrl_inst|Equal4~7_combout\);

-- Location: LCCOMB_X41_Y35_N2
\gc_ctrl_inst|LessThan0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|LessThan0~1_combout\ = (\gc_ctrl_inst|state.data_cnt\(31)) # ((\gc_ctrl_inst|LessThan0~0_combout\ & (!\gc_ctrl_inst|state.data_cnt\(5) & \gc_ctrl_inst|Equal4~7_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|LessThan0~0_combout\,
	datab => \gc_ctrl_inst|state.data_cnt\(5),
	datac => \gc_ctrl_inst|state.data_cnt\(31),
	datad => \gc_ctrl_inst|Equal4~7_combout\,
	combout => \gc_ctrl_inst|LessThan0~1_combout\);

-- Location: LCCOMB_X41_Y35_N20
\gc_ctrl_inst|Selector5~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector5~0_combout\ = (\gc_ctrl_inst|state.fsm_state.POLL~q\ & (\gc_ctrl_inst|LessThan0~1_combout\ & ((\gc_ctrl_inst|Equal4~10_combout\) # (!\gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\)))) # (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & 
-- ((\gc_ctrl_inst|Equal4~10_combout\) # ((!\gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010011110101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datab => \gc_ctrl_inst|Equal4~10_combout\,
	datac => \gc_ctrl_inst|LessThan0~1_combout\,
	datad => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	combout => \gc_ctrl_inst|Selector5~0_combout\);

-- Location: LCCOMB_X39_Y33_N24
\gc_ctrl_inst|state.data_cnt[26]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.data_cnt[26]~0_combout\ = ((\gc_ctrl_inst|Selector6~7_combout\) # ((\gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\ & \gc_ctrl_inst|next_state_logic~1_combout\))) # (!\gc_ctrl_inst|Selector5~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	datab => \gc_ctrl_inst|Selector5~0_combout\,
	datac => \gc_ctrl_inst|next_state_logic~1_combout\,
	datad => \gc_ctrl_inst|Selector6~7_combout\,
	combout => \gc_ctrl_inst|state.data_cnt[26]~0_combout\);

-- Location: FF_X41_Y35_N17
\gc_ctrl_inst|state.data_cnt[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector95~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(0));

-- Location: LCCOMB_X42_Y35_N2
\gc_ctrl_inst|Add1~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~2_combout\ = (\gc_ctrl_inst|state.data_cnt\(1) & (!\gc_ctrl_inst|Add1~1\)) # (!\gc_ctrl_inst|state.data_cnt\(1) & ((\gc_ctrl_inst|Add1~1\) # (GND)))
-- \gc_ctrl_inst|Add1~3\ = CARRY((!\gc_ctrl_inst|Add1~1\) # (!\gc_ctrl_inst|state.data_cnt\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_cnt\(1),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~1\,
	combout => \gc_ctrl_inst|Add1~2_combout\,
	cout => \gc_ctrl_inst|Add1~3\);

-- Location: LCCOMB_X41_Y35_N14
\gc_ctrl_inst|Selector94~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector94~0_combout\ = (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & (\gc_ctrl_inst|Add1~2_combout\ & \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datac => \gc_ctrl_inst|Add1~2_combout\,
	datad => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	combout => \gc_ctrl_inst|Selector94~0_combout\);

-- Location: FF_X41_Y35_N15
\gc_ctrl_inst|state.data_cnt[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector94~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(1));

-- Location: LCCOMB_X42_Y35_N4
\gc_ctrl_inst|Add1~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add1~4_combout\ = (\gc_ctrl_inst|state.data_cnt\(2) & (\gc_ctrl_inst|Add1~3\ $ (GND))) # (!\gc_ctrl_inst|state.data_cnt\(2) & (!\gc_ctrl_inst|Add1~3\ & VCC))
-- \gc_ctrl_inst|Add1~5\ = CARRY((\gc_ctrl_inst|state.data_cnt\(2) & !\gc_ctrl_inst|Add1~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.data_cnt\(2),
	datad => VCC,
	cin => \gc_ctrl_inst|Add1~3\,
	combout => \gc_ctrl_inst|Add1~4_combout\,
	cout => \gc_ctrl_inst|Add1~5\);

-- Location: LCCOMB_X41_Y35_N4
\gc_ctrl_inst|Selector93~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector93~0_combout\ = (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & (\gc_ctrl_inst|Add1~4_combout\ & \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datac => \gc_ctrl_inst|Add1~4_combout\,
	datad => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	combout => \gc_ctrl_inst|Selector93~0_combout\);

-- Location: FF_X41_Y35_N5
\gc_ctrl_inst|state.data_cnt[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector93~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(2));

-- Location: LCCOMB_X41_Y35_N6
\gc_ctrl_inst|Selector92~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector92~0_combout\ = (\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & \gc_ctrl_inst|Add1~6_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datac => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datad => \gc_ctrl_inst|Add1~6_combout\,
	combout => \gc_ctrl_inst|Selector92~0_combout\);

-- Location: FF_X41_Y35_N7
\gc_ctrl_inst|state.data_cnt[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector92~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.data_cnt[26]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.data_cnt\(3));

-- Location: LCCOMB_X41_Y35_N10
\gc_ctrl_inst|Equal4~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal4~8_combout\ = (\gc_ctrl_inst|state.data_cnt\(3) & (\gc_ctrl_inst|state.data_cnt\(5) & (!\gc_ctrl_inst|state.data_cnt\(31) & \gc_ctrl_inst|state.data_cnt\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.data_cnt\(3),
	datab => \gc_ctrl_inst|state.data_cnt\(5),
	datac => \gc_ctrl_inst|state.data_cnt\(31),
	datad => \gc_ctrl_inst|state.data_cnt\(4),
	combout => \gc_ctrl_inst|Equal4~8_combout\);

-- Location: LCCOMB_X41_Y35_N18
\gc_ctrl_inst|Equal4~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal4~9_combout\ = (\gc_ctrl_inst|state.data_cnt\(1) & (\gc_ctrl_inst|state.data_cnt\(2) & \gc_ctrl_inst|state.data_cnt\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.data_cnt\(1),
	datac => \gc_ctrl_inst|state.data_cnt\(2),
	datad => \gc_ctrl_inst|state.data_cnt\(0),
	combout => \gc_ctrl_inst|Equal4~9_combout\);

-- Location: LCCOMB_X41_Y35_N28
\gc_ctrl_inst|Equal4~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal4~10_combout\ = (\gc_ctrl_inst|Equal4~8_combout\ & (\gc_ctrl_inst|Equal4~9_combout\ & \gc_ctrl_inst|Equal4~7_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|Equal4~8_combout\,
	datab => \gc_ctrl_inst|Equal4~9_combout\,
	datad => \gc_ctrl_inst|Equal4~7_combout\,
	combout => \gc_ctrl_inst|Equal4~10_combout\);

-- Location: LCCOMB_X41_Y32_N6
\gc_ctrl_inst|Selector0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector0~0_combout\ = (\gc_ctrl_inst|Equal4~10_combout\ & (!\gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\ & ((\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\) # (\gc_ctrl_inst|Equal0~10_combout\)))) # (!\gc_ctrl_inst|Equal4~10_combout\ & 
-- (((\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\) # (\gc_ctrl_inst|Equal0~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011101110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|Equal4~10_combout\,
	datab => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	datac => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datad => \gc_ctrl_inst|Equal0~10_combout\,
	combout => \gc_ctrl_inst|Selector0~0_combout\);

-- Location: FF_X41_Y32_N7
\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector0~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\);

-- Location: LCCOMB_X43_Y32_N2
\gc_ctrl_inst|Selector6~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector6~7_combout\ = (\gc_ctrl_inst|Equal0~10_combout\ & !\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \gc_ctrl_inst|Equal0~10_combout\,
	datad => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	combout => \gc_ctrl_inst|Selector6~7_combout\);

-- Location: LCCOMB_X43_Y32_N8
\gc_ctrl_inst|state.poll_cmd[1]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.poll_cmd[1]~2_combout\ = (\gc_ctrl_inst|next_state_logic~1_combout\ & (\gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\ & \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|next_state_logic~1_combout\,
	datac => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	datad => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	combout => \gc_ctrl_inst|state.poll_cmd[1]~2_combout\);

-- Location: LCCOMB_X43_Y32_N20
\gc_ctrl_inst|Selector31~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector31~0_combout\ = (\gc_ctrl_inst|state.poll_cmd\(24)) # (!\gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	datad => \gc_ctrl_inst|state.poll_cmd\(24),
	combout => \gc_ctrl_inst|Selector31~0_combout\);

-- Location: LCCOMB_X42_Y32_N26
\gc_ctrl_inst|state.poll_cmd[5]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.poll_cmd[5]~0_combout\ = (\gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\ & ((\gc_ctrl_inst|next_state_logic~1_combout\) # ((!\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & \gc_ctrl_inst|Equal0~10_combout\)))) # 
-- (!\gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\ & (((!\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & \gc_ctrl_inst|Equal0~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000111110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	datab => \gc_ctrl_inst|next_state_logic~1_combout\,
	datac => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datad => \gc_ctrl_inst|Equal0~10_combout\,
	combout => \gc_ctrl_inst|state.poll_cmd[5]~0_combout\);

-- Location: FF_X43_Y32_N21
\gc_ctrl_inst|state.poll_cmd[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector31~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.poll_cmd[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.poll_cmd\(0));

-- Location: IOIBUF_X58_Y0_N8
\rumble~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rumble,
	o => \rumble~input_o\);

-- Location: LCCOMB_X43_Y32_N6
\gc_ctrl_inst|state.poll_cmd[1]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.poll_cmd[1]~1_combout\ = (\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & (\gc_ctrl_inst|state.poll_cmd\(0))) # (!\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & ((\rumble~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	datab => \gc_ctrl_inst|state.poll_cmd\(0),
	datad => \rumble~input_o\,
	combout => \gc_ctrl_inst|state.poll_cmd[1]~1_combout\);

-- Location: LCCOMB_X43_Y32_N10
\gc_ctrl_inst|state.poll_cmd[1]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.poll_cmd[1]~3_combout\ = (\gc_ctrl_inst|Selector6~7_combout\ & (((\gc_ctrl_inst|state.poll_cmd[1]~1_combout\)))) # (!\gc_ctrl_inst|Selector6~7_combout\ & (\gc_ctrl_inst|state.poll_cmd\(1) & 
-- (!\gc_ctrl_inst|state.poll_cmd[1]~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.poll_cmd\(1),
	datab => \gc_ctrl_inst|Selector6~7_combout\,
	datac => \gc_ctrl_inst|state.poll_cmd[1]~2_combout\,
	datad => \gc_ctrl_inst|state.poll_cmd[1]~1_combout\,
	combout => \gc_ctrl_inst|state.poll_cmd[1]~3_combout\);

-- Location: LCCOMB_X43_Y32_N26
\gc_ctrl_inst|state.poll_cmd[1]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.poll_cmd[1]~4_combout\ = (\gc_ctrl_inst|state.poll_cmd[1]~3_combout\) # ((\gc_ctrl_inst|state.poll_cmd[1]~2_combout\ & \gc_ctrl_inst|state.poll_cmd\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.poll_cmd[1]~3_combout\,
	datac => \gc_ctrl_inst|state.poll_cmd[1]~2_combout\,
	datad => \gc_ctrl_inst|state.poll_cmd\(0),
	combout => \gc_ctrl_inst|state.poll_cmd[1]~4_combout\);

-- Location: FF_X43_Y32_N27
\gc_ctrl_inst|state.poll_cmd[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.poll_cmd[1]~4_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.poll_cmd\(1));

-- Location: LCCOMB_X43_Y32_N0
\gc_ctrl_inst|Selector29~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector29~0_combout\ = (\gc_ctrl_inst|state.poll_cmd\(1) & \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \gc_ctrl_inst|state.poll_cmd\(1),
	datad => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	combout => \gc_ctrl_inst|Selector29~0_combout\);

-- Location: FF_X43_Y32_N1
\gc_ctrl_inst|state.poll_cmd[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector29~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.poll_cmd[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.poll_cmd\(2));

-- Location: LCCOMB_X43_Y32_N30
\gc_ctrl_inst|Selector28~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector28~0_combout\ = (\gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\ & \gc_ctrl_inst|state.poll_cmd\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	datad => \gc_ctrl_inst|state.poll_cmd\(2),
	combout => \gc_ctrl_inst|Selector28~0_combout\);

-- Location: FF_X43_Y32_N31
\gc_ctrl_inst|state.poll_cmd[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector28~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.poll_cmd[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.poll_cmd\(3));

-- Location: LCCOMB_X43_Y32_N28
\gc_ctrl_inst|Selector27~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector27~0_combout\ = (\gc_ctrl_inst|state.poll_cmd\(3) & \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \gc_ctrl_inst|state.poll_cmd\(3),
	datad => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	combout => \gc_ctrl_inst|Selector27~0_combout\);

-- Location: FF_X43_Y32_N29
\gc_ctrl_inst|state.poll_cmd[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector27~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.poll_cmd[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.poll_cmd\(4));

-- Location: LCCOMB_X43_Y32_N18
\gc_ctrl_inst|Selector26~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector26~0_combout\ = (\gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\ & \gc_ctrl_inst|state.poll_cmd\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	datad => \gc_ctrl_inst|state.poll_cmd\(4),
	combout => \gc_ctrl_inst|Selector26~0_combout\);

-- Location: FF_X43_Y32_N19
\gc_ctrl_inst|state.poll_cmd[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector26~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.poll_cmd[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.poll_cmd\(5));

-- Location: LCCOMB_X43_Y32_N24
\gc_ctrl_inst|Selector25~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector25~0_combout\ = (\gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\ & \gc_ctrl_inst|state.poll_cmd\(5))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	datad => \gc_ctrl_inst|state.poll_cmd\(5),
	combout => \gc_ctrl_inst|Selector25~0_combout\);

-- Location: FF_X43_Y32_N25
\gc_ctrl_inst|state.poll_cmd[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector25~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.poll_cmd[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.poll_cmd\(6));

-- Location: LCCOMB_X39_Y32_N6
\gc_ctrl_inst|Selector24~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector24~0_combout\ = (\gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\ & \gc_ctrl_inst|state.poll_cmd\(6))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	datad => \gc_ctrl_inst|state.poll_cmd\(6),
	combout => \gc_ctrl_inst|Selector24~0_combout\);

-- Location: FF_X39_Y32_N7
\gc_ctrl_inst|state.poll_cmd[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector24~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.poll_cmd[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.poll_cmd\(7));

-- Location: LCCOMB_X39_Y32_N28
\gc_ctrl_inst|Selector23~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector23~0_combout\ = (\gc_ctrl_inst|state.poll_cmd\(7) & \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.poll_cmd\(7),
	datac => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	combout => \gc_ctrl_inst|Selector23~0_combout\);

-- Location: FF_X39_Y32_N29
\gc_ctrl_inst|state.poll_cmd[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector23~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.poll_cmd[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.poll_cmd\(8));

-- Location: LCCOMB_X39_Y32_N26
\gc_ctrl_inst|Selector22~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector22~0_combout\ = (\gc_ctrl_inst|state.poll_cmd\(8)) # (!\gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.poll_cmd\(8),
	datac => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	combout => \gc_ctrl_inst|Selector22~0_combout\);

-- Location: FF_X39_Y32_N27
\gc_ctrl_inst|state.poll_cmd[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector22~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.poll_cmd[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.poll_cmd\(9));

-- Location: LCCOMB_X39_Y32_N8
\gc_ctrl_inst|Selector21~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector21~0_combout\ = (\gc_ctrl_inst|state.poll_cmd\(9)) # (!\gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	datac => \gc_ctrl_inst|state.poll_cmd\(9),
	combout => \gc_ctrl_inst|Selector21~0_combout\);

-- Location: FF_X39_Y32_N9
\gc_ctrl_inst|state.poll_cmd[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector21~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.poll_cmd[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.poll_cmd\(10));

-- Location: LCCOMB_X39_Y32_N14
\gc_ctrl_inst|Selector20~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector20~0_combout\ = (\gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\ & \gc_ctrl_inst|state.poll_cmd\(10))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	datac => \gc_ctrl_inst|state.poll_cmd\(10),
	combout => \gc_ctrl_inst|Selector20~0_combout\);

-- Location: FF_X39_Y32_N15
\gc_ctrl_inst|state.poll_cmd[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector20~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.poll_cmd[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.poll_cmd\(11));

-- Location: LCCOMB_X39_Y32_N4
\gc_ctrl_inst|Selector19~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector19~0_combout\ = (\gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\ & \gc_ctrl_inst|state.poll_cmd\(11))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	datac => \gc_ctrl_inst|state.poll_cmd\(11),
	combout => \gc_ctrl_inst|Selector19~0_combout\);

-- Location: FF_X39_Y32_N5
\gc_ctrl_inst|state.poll_cmd[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector19~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.poll_cmd[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.poll_cmd\(12));

-- Location: LCCOMB_X39_Y32_N10
\gc_ctrl_inst|Selector18~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector18~0_combout\ = (\gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\ & \gc_ctrl_inst|state.poll_cmd\(12))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	datac => \gc_ctrl_inst|state.poll_cmd\(12),
	combout => \gc_ctrl_inst|Selector18~0_combout\);

-- Location: FF_X39_Y32_N11
\gc_ctrl_inst|state.poll_cmd[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector18~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.poll_cmd[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.poll_cmd\(13));

-- Location: LCCOMB_X39_Y32_N16
\gc_ctrl_inst|Selector17~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector17~0_combout\ = (\gc_ctrl_inst|state.poll_cmd\(13) & \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.poll_cmd\(13),
	datac => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	combout => \gc_ctrl_inst|Selector17~0_combout\);

-- Location: FF_X39_Y32_N17
\gc_ctrl_inst|state.poll_cmd[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector17~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.poll_cmd[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.poll_cmd\(14));

-- Location: LCCOMB_X39_Y32_N30
\gc_ctrl_inst|Selector16~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector16~0_combout\ = (\gc_ctrl_inst|state.poll_cmd\(14) & \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.poll_cmd\(14),
	datac => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	combout => \gc_ctrl_inst|Selector16~0_combout\);

-- Location: FF_X39_Y32_N31
\gc_ctrl_inst|state.poll_cmd[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector16~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.poll_cmd[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.poll_cmd\(15));

-- Location: LCCOMB_X39_Y32_N20
\gc_ctrl_inst|Selector15~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector15~0_combout\ = (\gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\ & \gc_ctrl_inst|state.poll_cmd\(15))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	datac => \gc_ctrl_inst|state.poll_cmd\(15),
	combout => \gc_ctrl_inst|Selector15~0_combout\);

-- Location: FF_X39_Y32_N21
\gc_ctrl_inst|state.poll_cmd[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector15~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.poll_cmd[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.poll_cmd\(16));

-- Location: LCCOMB_X39_Y32_N2
\gc_ctrl_inst|Selector14~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector14~0_combout\ = (\gc_ctrl_inst|state.poll_cmd\(16) & \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.poll_cmd\(16),
	datac => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	combout => \gc_ctrl_inst|Selector14~0_combout\);

-- Location: FF_X39_Y32_N3
\gc_ctrl_inst|state.poll_cmd[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector14~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.poll_cmd[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.poll_cmd\(17));

-- Location: LCCOMB_X39_Y32_N24
\gc_ctrl_inst|Selector13~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector13~0_combout\ = (\gc_ctrl_inst|state.poll_cmd\(17) & \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.poll_cmd\(17),
	datac => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	combout => \gc_ctrl_inst|Selector13~0_combout\);

-- Location: FF_X39_Y32_N25
\gc_ctrl_inst|state.poll_cmd[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector13~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.poll_cmd[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.poll_cmd\(18));

-- Location: LCCOMB_X39_Y32_N22
\gc_ctrl_inst|Selector12~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector12~0_combout\ = (\gc_ctrl_inst|state.poll_cmd\(18) & \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.poll_cmd\(18),
	datac => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	combout => \gc_ctrl_inst|Selector12~0_combout\);

-- Location: FF_X39_Y32_N23
\gc_ctrl_inst|state.poll_cmd[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector12~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.poll_cmd[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.poll_cmd\(19));

-- Location: LCCOMB_X39_Y32_N12
\gc_ctrl_inst|Selector11~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector11~0_combout\ = (\gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\ & \gc_ctrl_inst|state.poll_cmd\(19))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	datac => \gc_ctrl_inst|state.poll_cmd\(19),
	combout => \gc_ctrl_inst|Selector11~0_combout\);

-- Location: FF_X39_Y32_N13
\gc_ctrl_inst|state.poll_cmd[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector11~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.poll_cmd[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.poll_cmd\(20));

-- Location: LCCOMB_X39_Y32_N18
\gc_ctrl_inst|Selector10~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector10~0_combout\ = (\gc_ctrl_inst|state.poll_cmd\(20) & \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.poll_cmd\(20),
	datac => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	combout => \gc_ctrl_inst|Selector10~0_combout\);

-- Location: FF_X39_Y32_N19
\gc_ctrl_inst|state.poll_cmd[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector10~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.poll_cmd[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.poll_cmd\(21));

-- Location: LCCOMB_X39_Y32_N0
\gc_ctrl_inst|Selector9~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector9~0_combout\ = (\gc_ctrl_inst|state.poll_cmd\(21) & \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.poll_cmd\(21),
	datac => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	combout => \gc_ctrl_inst|Selector9~0_combout\);

-- Location: FF_X39_Y32_N1
\gc_ctrl_inst|state.poll_cmd[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector9~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.poll_cmd[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.poll_cmd\(22));

-- Location: LCCOMB_X43_Y32_N22
\gc_ctrl_inst|Selector8~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector8~0_combout\ = (\gc_ctrl_inst|state.poll_cmd\(22)) # (!\gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \gc_ctrl_inst|state.poll_cmd\(22),
	datad => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	combout => \gc_ctrl_inst|Selector8~0_combout\);

-- Location: FF_X43_Y32_N23
\gc_ctrl_inst|state.poll_cmd[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector8~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.poll_cmd[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.poll_cmd\(23));

-- Location: LCCOMB_X43_Y32_N16
\gc_ctrl_inst|Selector7~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector7~0_combout\ = (\gc_ctrl_inst|state.poll_cmd\(23) & \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \gc_ctrl_inst|state.poll_cmd\(23),
	datad => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	combout => \gc_ctrl_inst|Selector7~0_combout\);

-- Location: FF_X43_Y32_N17
\gc_ctrl_inst|state.poll_cmd[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector7~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.poll_cmd[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.poll_cmd\(24));

-- Location: LCCOMB_X40_Y32_N6
\gc_ctrl_inst|next_state_logic~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|next_state_logic~0_combout\ = (\gc_ctrl_inst|Equal2~0_combout\ & ((\gc_ctrl_inst|state.poll_cmd\(24) & ((\gc_ctrl_inst|Equal0~7_combout\))) # (!\gc_ctrl_inst|state.poll_cmd\(24) & (\gc_ctrl_inst|Equal1~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|Equal2~0_combout\,
	datab => \gc_ctrl_inst|Equal1~3_combout\,
	datac => \gc_ctrl_inst|Equal0~7_combout\,
	datad => \gc_ctrl_inst|state.poll_cmd\(24),
	combout => \gc_ctrl_inst|next_state_logic~0_combout\);

-- Location: LCCOMB_X41_Y32_N18
\gc_ctrl_inst|Selector6~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector6~2_combout\ = (\gc_ctrl_inst|state.fsm_state.SEND_LOW~q\ & \gc_ctrl_inst|next_state_logic~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.fsm_state.SEND_LOW~q\,
	datac => \gc_ctrl_inst|next_state_logic~0_combout\,
	combout => \gc_ctrl_inst|Selector6~2_combout\);

-- Location: LCCOMB_X41_Y32_N0
\gc_ctrl_inst|Selector3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector3~0_combout\ = (!\gc_ctrl_inst|Selector6~5_combout\ & ((\gc_ctrl_inst|Selector6~4_combout\ & (\gc_ctrl_inst|Selector6~2_combout\)) # (!\gc_ctrl_inst|Selector6~4_combout\ & ((\gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|Selector6~4_combout\,
	datab => \gc_ctrl_inst|Selector6~2_combout\,
	datac => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	datad => \gc_ctrl_inst|Selector6~5_combout\,
	combout => \gc_ctrl_inst|Selector3~0_combout\);

-- Location: FF_X41_Y32_N1
\gc_ctrl_inst|state.fsm_state.SEND_HIGH\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector3~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\);

-- Location: LCCOMB_X41_Y32_N20
\gc_ctrl_inst|Selector6~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector6~3_combout\ = (\gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\ & (((\gc_ctrl_inst|next_state_logic~1_combout\)))) # (!\gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\ & (\gc_ctrl_inst|Equal0~10_combout\ & 
-- ((!\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	datab => \gc_ctrl_inst|Equal0~10_combout\,
	datac => \gc_ctrl_inst|next_state_logic~1_combout\,
	datad => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	combout => \gc_ctrl_inst|Selector6~3_combout\);

-- Location: LCCOMB_X41_Y32_N4
\gc_ctrl_inst|Selector6~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector6~1_combout\ = (\gc_ctrl_inst|Equal1~1_combout\ & (\gc_ctrl_inst|Equal3~1_combout\ & \gc_ctrl_inst|state.fsm_state.WAIT_SAMPLE~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|Equal1~1_combout\,
	datac => \gc_ctrl_inst|Equal3~1_combout\,
	datad => \gc_ctrl_inst|state.fsm_state.WAIT_SAMPLE~q\,
	combout => \gc_ctrl_inst|Selector6~1_combout\);

-- Location: LCCOMB_X41_Y32_N12
\gc_ctrl_inst|Selector6~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector6~4_combout\ = ((\gc_ctrl_inst|Selector6~3_combout\) # ((\gc_ctrl_inst|Selector6~1_combout\) # (\gc_ctrl_inst|Selector6~2_combout\))) # (!\gc_ctrl_inst|Selector1~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|Selector1~0_combout\,
	datab => \gc_ctrl_inst|Selector6~3_combout\,
	datac => \gc_ctrl_inst|Selector6~1_combout\,
	datad => \gc_ctrl_inst|Selector6~2_combout\,
	combout => \gc_ctrl_inst|Selector6~4_combout\);

-- Location: LCCOMB_X41_Y32_N8
\gc_ctrl_inst|Selector6~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector6~6_combout\ = (\gc_ctrl_inst|Selector6~4_combout\ & (\gc_ctrl_inst|Selector6~1_combout\ & !\gc_ctrl_inst|Selector6~5_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|Selector6~4_combout\,
	datac => \gc_ctrl_inst|Selector6~1_combout\,
	datad => \gc_ctrl_inst|Selector6~5_combout\,
	combout => \gc_ctrl_inst|Selector6~6_combout\);

-- Location: FF_X41_Y32_N9
\gc_ctrl_inst|state.fsm_state.SAMPLE_DATA\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector6~6_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\);

-- Location: LCCOMB_X41_Y32_N16
\gc_ctrl_inst|WideOr1\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|WideOr1~combout\ = (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & (!\gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\ & !\gc_ctrl_inst|state.fsm_state.WAIT_FALLING_EDGE_DATA~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datab => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	datad => \gc_ctrl_inst|state.fsm_state.WAIT_FALLING_EDGE_DATA~q\,
	combout => \gc_ctrl_inst|WideOr1~combout\);

-- Location: FF_X41_Y31_N21
\gc_ctrl_inst|state.clk_cnt[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector63~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(0));

-- Location: LCCOMB_X40_Y31_N2
\gc_ctrl_inst|Add0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~2_combout\ = (\gc_ctrl_inst|state.clk_cnt\(1) & (!\gc_ctrl_inst|Add0~1\)) # (!\gc_ctrl_inst|state.clk_cnt\(1) & ((\gc_ctrl_inst|Add0~1\) # (GND)))
-- \gc_ctrl_inst|Add0~3\ = CARRY((!\gc_ctrl_inst|Add0~1\) # (!\gc_ctrl_inst|state.clk_cnt\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt\(1),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~1\,
	combout => \gc_ctrl_inst|Add0~2_combout\,
	cout => \gc_ctrl_inst|Add0~3\);

-- Location: LCCOMB_X41_Y31_N10
\gc_ctrl_inst|Selector62~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector62~0_combout\ = (!\gc_ctrl_inst|state.clk_cnt[6]~1_combout\ & \gc_ctrl_inst|Add0~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	datad => \gc_ctrl_inst|Add0~2_combout\,
	combout => \gc_ctrl_inst|Selector62~0_combout\);

-- Location: FF_X41_Y31_N11
\gc_ctrl_inst|state.clk_cnt[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector62~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(1));

-- Location: LCCOMB_X40_Y31_N4
\gc_ctrl_inst|Add0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~4_combout\ = (\gc_ctrl_inst|state.clk_cnt\(2) & (\gc_ctrl_inst|Add0~3\ $ (GND))) # (!\gc_ctrl_inst|state.clk_cnt\(2) & (!\gc_ctrl_inst|Add0~3\ & VCC))
-- \gc_ctrl_inst|Add0~5\ = CARRY((\gc_ctrl_inst|state.clk_cnt\(2) & !\gc_ctrl_inst|Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt\(2),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~3\,
	combout => \gc_ctrl_inst|Add0~4_combout\,
	cout => \gc_ctrl_inst|Add0~5\);

-- Location: LCCOMB_X39_Y31_N0
\gc_ctrl_inst|Selector61~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector61~0_combout\ = (\gc_ctrl_inst|Add0~4_combout\ & !\gc_ctrl_inst|state.clk_cnt[6]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|Add0~4_combout\,
	datad => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	combout => \gc_ctrl_inst|Selector61~0_combout\);

-- Location: FF_X39_Y31_N1
\gc_ctrl_inst|state.clk_cnt[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector61~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(2));

-- Location: LCCOMB_X40_Y31_N6
\gc_ctrl_inst|Add0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~6_combout\ = (\gc_ctrl_inst|state.clk_cnt\(3) & (!\gc_ctrl_inst|Add0~5\)) # (!\gc_ctrl_inst|state.clk_cnt\(3) & ((\gc_ctrl_inst|Add0~5\) # (GND)))
-- \gc_ctrl_inst|Add0~7\ = CARRY((!\gc_ctrl_inst|Add0~5\) # (!\gc_ctrl_inst|state.clk_cnt\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt\(3),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~5\,
	combout => \gc_ctrl_inst|Add0~6_combout\,
	cout => \gc_ctrl_inst|Add0~7\);

-- Location: LCCOMB_X41_Y31_N2
\gc_ctrl_inst|Selector60~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector60~0_combout\ = (\gc_ctrl_inst|Add0~6_combout\ & !\gc_ctrl_inst|state.clk_cnt[6]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|Add0~6_combout\,
	datad => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	combout => \gc_ctrl_inst|Selector60~0_combout\);

-- Location: FF_X41_Y31_N3
\gc_ctrl_inst|state.clk_cnt[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector60~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(3));

-- Location: LCCOMB_X40_Y31_N8
\gc_ctrl_inst|Add0~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~8_combout\ = (\gc_ctrl_inst|state.clk_cnt\(4) & (\gc_ctrl_inst|Add0~7\ $ (GND))) # (!\gc_ctrl_inst|state.clk_cnt\(4) & (!\gc_ctrl_inst|Add0~7\ & VCC))
-- \gc_ctrl_inst|Add0~9\ = CARRY((\gc_ctrl_inst|state.clk_cnt\(4) & !\gc_ctrl_inst|Add0~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt\(4),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~7\,
	combout => \gc_ctrl_inst|Add0~8_combout\,
	cout => \gc_ctrl_inst|Add0~9\);

-- Location: LCCOMB_X41_Y31_N8
\gc_ctrl_inst|Selector59~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector59~0_combout\ = (!\gc_ctrl_inst|state.clk_cnt[6]~1_combout\ & \gc_ctrl_inst|Add0~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	datad => \gc_ctrl_inst|Add0~8_combout\,
	combout => \gc_ctrl_inst|Selector59~0_combout\);

-- Location: FF_X41_Y31_N9
\gc_ctrl_inst|state.clk_cnt[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector59~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(4));

-- Location: LCCOMB_X40_Y31_N10
\gc_ctrl_inst|Add0~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~10_combout\ = (\gc_ctrl_inst|state.clk_cnt\(5) & (!\gc_ctrl_inst|Add0~9\)) # (!\gc_ctrl_inst|state.clk_cnt\(5) & ((\gc_ctrl_inst|Add0~9\) # (GND)))
-- \gc_ctrl_inst|Add0~11\ = CARRY((!\gc_ctrl_inst|Add0~9\) # (!\gc_ctrl_inst|state.clk_cnt\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt\(5),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~9\,
	combout => \gc_ctrl_inst|Add0~10_combout\,
	cout => \gc_ctrl_inst|Add0~11\);

-- Location: LCCOMB_X40_Y32_N4
\gc_ctrl_inst|Selector58~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector58~0_combout\ = (!\gc_ctrl_inst|state.clk_cnt[6]~1_combout\ & \gc_ctrl_inst|Add0~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	datac => \gc_ctrl_inst|Add0~10_combout\,
	combout => \gc_ctrl_inst|Selector58~0_combout\);

-- Location: FF_X40_Y32_N5
\gc_ctrl_inst|state.clk_cnt[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector58~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(5));

-- Location: LCCOMB_X40_Y31_N12
\gc_ctrl_inst|Add0~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~12_combout\ = (\gc_ctrl_inst|state.clk_cnt\(6) & (\gc_ctrl_inst|Add0~11\ $ (GND))) # (!\gc_ctrl_inst|state.clk_cnt\(6) & (!\gc_ctrl_inst|Add0~11\ & VCC))
-- \gc_ctrl_inst|Add0~13\ = CARRY((\gc_ctrl_inst|state.clk_cnt\(6) & !\gc_ctrl_inst|Add0~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt\(6),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~11\,
	combout => \gc_ctrl_inst|Add0~12_combout\,
	cout => \gc_ctrl_inst|Add0~13\);

-- Location: LCCOMB_X39_Y31_N12
\gc_ctrl_inst|Selector57~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector57~0_combout\ = (\gc_ctrl_inst|Add0~12_combout\ & !\gc_ctrl_inst|state.clk_cnt[6]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \gc_ctrl_inst|Add0~12_combout\,
	datad => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	combout => \gc_ctrl_inst|Selector57~0_combout\);

-- Location: FF_X39_Y31_N13
\gc_ctrl_inst|state.clk_cnt[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector57~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(6));

-- Location: LCCOMB_X40_Y31_N14
\gc_ctrl_inst|Add0~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~14_combout\ = (\gc_ctrl_inst|state.clk_cnt\(7) & (!\gc_ctrl_inst|Add0~13\)) # (!\gc_ctrl_inst|state.clk_cnt\(7) & ((\gc_ctrl_inst|Add0~13\) # (GND)))
-- \gc_ctrl_inst|Add0~15\ = CARRY((!\gc_ctrl_inst|Add0~13\) # (!\gc_ctrl_inst|state.clk_cnt\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt\(7),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~13\,
	combout => \gc_ctrl_inst|Add0~14_combout\,
	cout => \gc_ctrl_inst|Add0~15\);

-- Location: LCCOMB_X41_Y31_N0
\gc_ctrl_inst|Selector56~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector56~0_combout\ = (!\gc_ctrl_inst|state.clk_cnt[6]~1_combout\ & \gc_ctrl_inst|Add0~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	datad => \gc_ctrl_inst|Add0~14_combout\,
	combout => \gc_ctrl_inst|Selector56~0_combout\);

-- Location: FF_X41_Y31_N1
\gc_ctrl_inst|state.clk_cnt[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector56~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(7));

-- Location: LCCOMB_X40_Y31_N16
\gc_ctrl_inst|Add0~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~16_combout\ = (\gc_ctrl_inst|state.clk_cnt\(8) & (\gc_ctrl_inst|Add0~15\ $ (GND))) # (!\gc_ctrl_inst|state.clk_cnt\(8) & (!\gc_ctrl_inst|Add0~15\ & VCC))
-- \gc_ctrl_inst|Add0~17\ = CARRY((\gc_ctrl_inst|state.clk_cnt\(8) & !\gc_ctrl_inst|Add0~15\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt\(8),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~15\,
	combout => \gc_ctrl_inst|Add0~16_combout\,
	cout => \gc_ctrl_inst|Add0~17\);

-- Location: LCCOMB_X39_Y31_N16
\gc_ctrl_inst|Selector55~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector55~0_combout\ = (\gc_ctrl_inst|Add0~16_combout\ & !\gc_ctrl_inst|state.clk_cnt[6]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|Add0~16_combout\,
	datad => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	combout => \gc_ctrl_inst|Selector55~0_combout\);

-- Location: FF_X39_Y31_N17
\gc_ctrl_inst|state.clk_cnt[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector55~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(8));

-- Location: LCCOMB_X40_Y31_N18
\gc_ctrl_inst|Add0~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~18_combout\ = (\gc_ctrl_inst|state.clk_cnt\(9) & (!\gc_ctrl_inst|Add0~17\)) # (!\gc_ctrl_inst|state.clk_cnt\(9) & ((\gc_ctrl_inst|Add0~17\) # (GND)))
-- \gc_ctrl_inst|Add0~19\ = CARRY((!\gc_ctrl_inst|Add0~17\) # (!\gc_ctrl_inst|state.clk_cnt\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt\(9),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~17\,
	combout => \gc_ctrl_inst|Add0~18_combout\,
	cout => \gc_ctrl_inst|Add0~19\);

-- Location: LCCOMB_X39_Y31_N6
\gc_ctrl_inst|Selector54~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector54~0_combout\ = (\gc_ctrl_inst|Add0~18_combout\ & !\gc_ctrl_inst|state.clk_cnt[6]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|Add0~18_combout\,
	datad => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	combout => \gc_ctrl_inst|Selector54~0_combout\);

-- Location: FF_X39_Y31_N7
\gc_ctrl_inst|state.clk_cnt[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector54~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(9));

-- Location: LCCOMB_X40_Y31_N20
\gc_ctrl_inst|Add0~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~20_combout\ = (\gc_ctrl_inst|state.clk_cnt\(10) & (\gc_ctrl_inst|Add0~19\ $ (GND))) # (!\gc_ctrl_inst|state.clk_cnt\(10) & (!\gc_ctrl_inst|Add0~19\ & VCC))
-- \gc_ctrl_inst|Add0~21\ = CARRY((\gc_ctrl_inst|state.clk_cnt\(10) & !\gc_ctrl_inst|Add0~19\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt\(10),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~19\,
	combout => \gc_ctrl_inst|Add0~20_combout\,
	cout => \gc_ctrl_inst|Add0~21\);

-- Location: LCCOMB_X39_Y31_N4
\gc_ctrl_inst|Selector53~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector53~0_combout\ = (\gc_ctrl_inst|Add0~20_combout\ & !\gc_ctrl_inst|state.clk_cnt[6]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \gc_ctrl_inst|Add0~20_combout\,
	datad => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	combout => \gc_ctrl_inst|Selector53~0_combout\);

-- Location: FF_X39_Y31_N5
\gc_ctrl_inst|state.clk_cnt[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector53~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(10));

-- Location: LCCOMB_X40_Y31_N22
\gc_ctrl_inst|Add0~22\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~22_combout\ = (\gc_ctrl_inst|state.clk_cnt\(11) & (!\gc_ctrl_inst|Add0~21\)) # (!\gc_ctrl_inst|state.clk_cnt\(11) & ((\gc_ctrl_inst|Add0~21\) # (GND)))
-- \gc_ctrl_inst|Add0~23\ = CARRY((!\gc_ctrl_inst|Add0~21\) # (!\gc_ctrl_inst|state.clk_cnt\(11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt\(11),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~21\,
	combout => \gc_ctrl_inst|Add0~22_combout\,
	cout => \gc_ctrl_inst|Add0~23\);

-- Location: LCCOMB_X41_Y31_N18
\gc_ctrl_inst|Selector52~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector52~0_combout\ = (!\gc_ctrl_inst|state.clk_cnt[6]~1_combout\ & \gc_ctrl_inst|Add0~22_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	datad => \gc_ctrl_inst|Add0~22_combout\,
	combout => \gc_ctrl_inst|Selector52~0_combout\);

-- Location: FF_X41_Y31_N19
\gc_ctrl_inst|state.clk_cnt[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector52~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(11));

-- Location: LCCOMB_X40_Y31_N24
\gc_ctrl_inst|Add0~24\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~24_combout\ = (\gc_ctrl_inst|state.clk_cnt\(12) & (\gc_ctrl_inst|Add0~23\ $ (GND))) # (!\gc_ctrl_inst|state.clk_cnt\(12) & (!\gc_ctrl_inst|Add0~23\ & VCC))
-- \gc_ctrl_inst|Add0~25\ = CARRY((\gc_ctrl_inst|state.clk_cnt\(12) & !\gc_ctrl_inst|Add0~23\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt\(12),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~23\,
	combout => \gc_ctrl_inst|Add0~24_combout\,
	cout => \gc_ctrl_inst|Add0~25\);

-- Location: LCCOMB_X39_Y31_N10
\gc_ctrl_inst|Selector51~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector51~0_combout\ = (\gc_ctrl_inst|Add0~24_combout\ & !\gc_ctrl_inst|state.clk_cnt[6]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|Add0~24_combout\,
	datad => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	combout => \gc_ctrl_inst|Selector51~0_combout\);

-- Location: FF_X39_Y31_N11
\gc_ctrl_inst|state.clk_cnt[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector51~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(12));

-- Location: LCCOMB_X40_Y31_N26
\gc_ctrl_inst|Add0~26\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~26_combout\ = (\gc_ctrl_inst|state.clk_cnt\(13) & (!\gc_ctrl_inst|Add0~25\)) # (!\gc_ctrl_inst|state.clk_cnt\(13) & ((\gc_ctrl_inst|Add0~25\) # (GND)))
-- \gc_ctrl_inst|Add0~27\ = CARRY((!\gc_ctrl_inst|Add0~25\) # (!\gc_ctrl_inst|state.clk_cnt\(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt\(13),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~25\,
	combout => \gc_ctrl_inst|Add0~26_combout\,
	cout => \gc_ctrl_inst|Add0~27\);

-- Location: LCCOMB_X41_Y31_N16
\gc_ctrl_inst|Selector50~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector50~0_combout\ = (\gc_ctrl_inst|Add0~26_combout\ & !\gc_ctrl_inst|state.clk_cnt[6]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|Add0~26_combout\,
	datad => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	combout => \gc_ctrl_inst|Selector50~0_combout\);

-- Location: FF_X41_Y31_N17
\gc_ctrl_inst|state.clk_cnt[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector50~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(13));

-- Location: LCCOMB_X40_Y31_N28
\gc_ctrl_inst|Add0~28\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~28_combout\ = (\gc_ctrl_inst|state.clk_cnt\(14) & (\gc_ctrl_inst|Add0~27\ $ (GND))) # (!\gc_ctrl_inst|state.clk_cnt\(14) & (!\gc_ctrl_inst|Add0~27\ & VCC))
-- \gc_ctrl_inst|Add0~29\ = CARRY((\gc_ctrl_inst|state.clk_cnt\(14) & !\gc_ctrl_inst|Add0~27\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt\(14),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~27\,
	combout => \gc_ctrl_inst|Add0~28_combout\,
	cout => \gc_ctrl_inst|Add0~29\);

-- Location: LCCOMB_X41_Y31_N6
\gc_ctrl_inst|Selector49~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector49~0_combout\ = (!\gc_ctrl_inst|state.clk_cnt[6]~1_combout\ & \gc_ctrl_inst|Add0~28_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	datad => \gc_ctrl_inst|Add0~28_combout\,
	combout => \gc_ctrl_inst|Selector49~0_combout\);

-- Location: FF_X41_Y31_N7
\gc_ctrl_inst|state.clk_cnt[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector49~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(14));

-- Location: LCCOMB_X40_Y31_N30
\gc_ctrl_inst|Add0~30\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~30_combout\ = (\gc_ctrl_inst|state.clk_cnt\(15) & (!\gc_ctrl_inst|Add0~29\)) # (!\gc_ctrl_inst|state.clk_cnt\(15) & ((\gc_ctrl_inst|Add0~29\) # (GND)))
-- \gc_ctrl_inst|Add0~31\ = CARRY((!\gc_ctrl_inst|Add0~29\) # (!\gc_ctrl_inst|state.clk_cnt\(15)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt\(15),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~29\,
	combout => \gc_ctrl_inst|Add0~30_combout\,
	cout => \gc_ctrl_inst|Add0~31\);

-- Location: LCCOMB_X40_Y30_N0
\gc_ctrl_inst|Add0~32\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~32_combout\ = (\gc_ctrl_inst|state.clk_cnt\(16) & (\gc_ctrl_inst|Add0~31\ $ (GND))) # (!\gc_ctrl_inst|state.clk_cnt\(16) & (!\gc_ctrl_inst|Add0~31\ & VCC))
-- \gc_ctrl_inst|Add0~33\ = CARRY((\gc_ctrl_inst|state.clk_cnt\(16) & !\gc_ctrl_inst|Add0~31\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt\(16),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~31\,
	combout => \gc_ctrl_inst|Add0~32_combout\,
	cout => \gc_ctrl_inst|Add0~33\);

-- Location: LCCOMB_X41_Y30_N20
\gc_ctrl_inst|Selector47~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector47~0_combout\ = (!\gc_ctrl_inst|state.clk_cnt[6]~1_combout\ & \gc_ctrl_inst|Add0~32_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	datad => \gc_ctrl_inst|Add0~32_combout\,
	combout => \gc_ctrl_inst|Selector47~0_combout\);

-- Location: FF_X41_Y30_N21
\gc_ctrl_inst|state.clk_cnt[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector47~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(16));

-- Location: LCCOMB_X40_Y30_N2
\gc_ctrl_inst|Add0~34\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~34_combout\ = (\gc_ctrl_inst|state.clk_cnt\(17) & (!\gc_ctrl_inst|Add0~33\)) # (!\gc_ctrl_inst|state.clk_cnt\(17) & ((\gc_ctrl_inst|Add0~33\) # (GND)))
-- \gc_ctrl_inst|Add0~35\ = CARRY((!\gc_ctrl_inst|Add0~33\) # (!\gc_ctrl_inst|state.clk_cnt\(17)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt\(17),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~33\,
	combout => \gc_ctrl_inst|Add0~34_combout\,
	cout => \gc_ctrl_inst|Add0~35\);

-- Location: LCCOMB_X41_Y30_N10
\gc_ctrl_inst|Selector46~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector46~0_combout\ = (\gc_ctrl_inst|Add0~34_combout\ & !\gc_ctrl_inst|state.clk_cnt[6]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|Add0~34_combout\,
	datac => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	combout => \gc_ctrl_inst|Selector46~0_combout\);

-- Location: FF_X41_Y30_N11
\gc_ctrl_inst|state.clk_cnt[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector46~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(17));

-- Location: LCCOMB_X40_Y30_N4
\gc_ctrl_inst|Add0~36\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~36_combout\ = (\gc_ctrl_inst|state.clk_cnt\(18) & (\gc_ctrl_inst|Add0~35\ $ (GND))) # (!\gc_ctrl_inst|state.clk_cnt\(18) & (!\gc_ctrl_inst|Add0~35\ & VCC))
-- \gc_ctrl_inst|Add0~37\ = CARRY((\gc_ctrl_inst|state.clk_cnt\(18) & !\gc_ctrl_inst|Add0~35\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt\(18),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~35\,
	combout => \gc_ctrl_inst|Add0~36_combout\,
	cout => \gc_ctrl_inst|Add0~37\);

-- Location: LCCOMB_X41_Y30_N8
\gc_ctrl_inst|Selector45~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector45~0_combout\ = (!\gc_ctrl_inst|state.clk_cnt[6]~1_combout\ & \gc_ctrl_inst|Add0~36_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	datac => \gc_ctrl_inst|Add0~36_combout\,
	combout => \gc_ctrl_inst|Selector45~0_combout\);

-- Location: FF_X41_Y30_N9
\gc_ctrl_inst|state.clk_cnt[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector45~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(18));

-- Location: LCCOMB_X40_Y30_N6
\gc_ctrl_inst|Add0~38\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~38_combout\ = (\gc_ctrl_inst|state.clk_cnt\(19) & (!\gc_ctrl_inst|Add0~37\)) # (!\gc_ctrl_inst|state.clk_cnt\(19) & ((\gc_ctrl_inst|Add0~37\) # (GND)))
-- \gc_ctrl_inst|Add0~39\ = CARRY((!\gc_ctrl_inst|Add0~37\) # (!\gc_ctrl_inst|state.clk_cnt\(19)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt\(19),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~37\,
	combout => \gc_ctrl_inst|Add0~38_combout\,
	cout => \gc_ctrl_inst|Add0~39\);

-- Location: LCCOMB_X41_Y30_N6
\gc_ctrl_inst|Selector44~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector44~0_combout\ = (!\gc_ctrl_inst|state.clk_cnt[6]~1_combout\ & \gc_ctrl_inst|Add0~38_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	datac => \gc_ctrl_inst|Add0~38_combout\,
	combout => \gc_ctrl_inst|Selector44~0_combout\);

-- Location: FF_X41_Y30_N7
\gc_ctrl_inst|state.clk_cnt[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector44~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(19));

-- Location: LCCOMB_X40_Y30_N8
\gc_ctrl_inst|Add0~40\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~40_combout\ = (\gc_ctrl_inst|state.clk_cnt\(20) & (\gc_ctrl_inst|Add0~39\ $ (GND))) # (!\gc_ctrl_inst|state.clk_cnt\(20) & (!\gc_ctrl_inst|Add0~39\ & VCC))
-- \gc_ctrl_inst|Add0~41\ = CARRY((\gc_ctrl_inst|state.clk_cnt\(20) & !\gc_ctrl_inst|Add0~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt\(20),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~39\,
	combout => \gc_ctrl_inst|Add0~40_combout\,
	cout => \gc_ctrl_inst|Add0~41\);

-- Location: LCCOMB_X39_Y30_N24
\gc_ctrl_inst|Selector43~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector43~0_combout\ = (\gc_ctrl_inst|Add0~40_combout\ & !\gc_ctrl_inst|state.clk_cnt[6]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \gc_ctrl_inst|Add0~40_combout\,
	datad => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	combout => \gc_ctrl_inst|Selector43~0_combout\);

-- Location: FF_X39_Y30_N25
\gc_ctrl_inst|state.clk_cnt[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector43~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(20));

-- Location: LCCOMB_X40_Y30_N10
\gc_ctrl_inst|Add0~42\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~42_combout\ = (\gc_ctrl_inst|state.clk_cnt\(21) & (!\gc_ctrl_inst|Add0~41\)) # (!\gc_ctrl_inst|state.clk_cnt\(21) & ((\gc_ctrl_inst|Add0~41\) # (GND)))
-- \gc_ctrl_inst|Add0~43\ = CARRY((!\gc_ctrl_inst|Add0~41\) # (!\gc_ctrl_inst|state.clk_cnt\(21)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt\(21),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~41\,
	combout => \gc_ctrl_inst|Add0~42_combout\,
	cout => \gc_ctrl_inst|Add0~43\);

-- Location: LCCOMB_X39_Y30_N6
\gc_ctrl_inst|Selector42~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector42~0_combout\ = (\gc_ctrl_inst|Add0~42_combout\ & !\gc_ctrl_inst|state.clk_cnt[6]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \gc_ctrl_inst|Add0~42_combout\,
	datad => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	combout => \gc_ctrl_inst|Selector42~0_combout\);

-- Location: FF_X39_Y30_N7
\gc_ctrl_inst|state.clk_cnt[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector42~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(21));

-- Location: LCCOMB_X40_Y30_N12
\gc_ctrl_inst|Add0~44\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~44_combout\ = (\gc_ctrl_inst|state.clk_cnt\(22) & (\gc_ctrl_inst|Add0~43\ $ (GND))) # (!\gc_ctrl_inst|state.clk_cnt\(22) & (!\gc_ctrl_inst|Add0~43\ & VCC))
-- \gc_ctrl_inst|Add0~45\ = CARRY((\gc_ctrl_inst|state.clk_cnt\(22) & !\gc_ctrl_inst|Add0~43\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt\(22),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~43\,
	combout => \gc_ctrl_inst|Add0~44_combout\,
	cout => \gc_ctrl_inst|Add0~45\);

-- Location: LCCOMB_X39_Y30_N20
\gc_ctrl_inst|Selector41~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector41~0_combout\ = (\gc_ctrl_inst|Add0~44_combout\ & !\gc_ctrl_inst|state.clk_cnt[6]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \gc_ctrl_inst|Add0~44_combout\,
	datad => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	combout => \gc_ctrl_inst|Selector41~0_combout\);

-- Location: FF_X39_Y30_N21
\gc_ctrl_inst|state.clk_cnt[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector41~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(22));

-- Location: LCCOMB_X40_Y30_N14
\gc_ctrl_inst|Add0~46\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~46_combout\ = (\gc_ctrl_inst|state.clk_cnt\(23) & (!\gc_ctrl_inst|Add0~45\)) # (!\gc_ctrl_inst|state.clk_cnt\(23) & ((\gc_ctrl_inst|Add0~45\) # (GND)))
-- \gc_ctrl_inst|Add0~47\ = CARRY((!\gc_ctrl_inst|Add0~45\) # (!\gc_ctrl_inst|state.clk_cnt\(23)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt\(23),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~45\,
	combout => \gc_ctrl_inst|Add0~46_combout\,
	cout => \gc_ctrl_inst|Add0~47\);

-- Location: LCCOMB_X39_Y30_N10
\gc_ctrl_inst|Selector40~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector40~0_combout\ = (\gc_ctrl_inst|Add0~46_combout\ & !\gc_ctrl_inst|state.clk_cnt[6]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \gc_ctrl_inst|Add0~46_combout\,
	datad => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	combout => \gc_ctrl_inst|Selector40~0_combout\);

-- Location: FF_X39_Y30_N11
\gc_ctrl_inst|state.clk_cnt[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector40~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(23));

-- Location: LCCOMB_X39_Y30_N26
\gc_ctrl_inst|Equal0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal0~3_combout\ = (!\gc_ctrl_inst|state.clk_cnt\(23) & (!\gc_ctrl_inst|state.clk_cnt\(20) & (!\gc_ctrl_inst|state.clk_cnt\(22) & !\gc_ctrl_inst|state.clk_cnt\(21))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt\(23),
	datab => \gc_ctrl_inst|state.clk_cnt\(20),
	datac => \gc_ctrl_inst|state.clk_cnt\(22),
	datad => \gc_ctrl_inst|state.clk_cnt\(21),
	combout => \gc_ctrl_inst|Equal0~3_combout\);

-- Location: LCCOMB_X41_Y30_N22
\gc_ctrl_inst|Equal0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal0~4_combout\ = (!\gc_ctrl_inst|state.clk_cnt\(17) & (!\gc_ctrl_inst|state.clk_cnt\(16) & (!\gc_ctrl_inst|state.clk_cnt\(18) & !\gc_ctrl_inst|state.clk_cnt\(19))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt\(17),
	datab => \gc_ctrl_inst|state.clk_cnt\(16),
	datac => \gc_ctrl_inst|state.clk_cnt\(18),
	datad => \gc_ctrl_inst|state.clk_cnt\(19),
	combout => \gc_ctrl_inst|Equal0~4_combout\);

-- Location: LCCOMB_X40_Y30_N16
\gc_ctrl_inst|Add0~48\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~48_combout\ = (\gc_ctrl_inst|state.clk_cnt\(24) & (\gc_ctrl_inst|Add0~47\ $ (GND))) # (!\gc_ctrl_inst|state.clk_cnt\(24) & (!\gc_ctrl_inst|Add0~47\ & VCC))
-- \gc_ctrl_inst|Add0~49\ = CARRY((\gc_ctrl_inst|state.clk_cnt\(24) & !\gc_ctrl_inst|Add0~47\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt\(24),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~47\,
	combout => \gc_ctrl_inst|Add0~48_combout\,
	cout => \gc_ctrl_inst|Add0~49\);

-- Location: LCCOMB_X39_Y30_N22
\gc_ctrl_inst|Selector39~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector39~0_combout\ = (\gc_ctrl_inst|Add0~48_combout\ & !\gc_ctrl_inst|state.clk_cnt[6]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|Add0~48_combout\,
	datad => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	combout => \gc_ctrl_inst|Selector39~0_combout\);

-- Location: FF_X39_Y30_N23
\gc_ctrl_inst|state.clk_cnt[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector39~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(24));

-- Location: LCCOMB_X40_Y30_N18
\gc_ctrl_inst|Add0~50\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~50_combout\ = (\gc_ctrl_inst|state.clk_cnt\(25) & (!\gc_ctrl_inst|Add0~49\)) # (!\gc_ctrl_inst|state.clk_cnt\(25) & ((\gc_ctrl_inst|Add0~49\) # (GND)))
-- \gc_ctrl_inst|Add0~51\ = CARRY((!\gc_ctrl_inst|Add0~49\) # (!\gc_ctrl_inst|state.clk_cnt\(25)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt\(25),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~49\,
	combout => \gc_ctrl_inst|Add0~50_combout\,
	cout => \gc_ctrl_inst|Add0~51\);

-- Location: LCCOMB_X39_Y30_N12
\gc_ctrl_inst|Selector38~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector38~0_combout\ = (\gc_ctrl_inst|Add0~50_combout\ & !\gc_ctrl_inst|state.clk_cnt[6]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|Add0~50_combout\,
	datad => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	combout => \gc_ctrl_inst|Selector38~0_combout\);

-- Location: FF_X39_Y30_N13
\gc_ctrl_inst|state.clk_cnt[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector38~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(25));

-- Location: LCCOMB_X40_Y30_N20
\gc_ctrl_inst|Add0~52\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~52_combout\ = (\gc_ctrl_inst|state.clk_cnt\(26) & (\gc_ctrl_inst|Add0~51\ $ (GND))) # (!\gc_ctrl_inst|state.clk_cnt\(26) & (!\gc_ctrl_inst|Add0~51\ & VCC))
-- \gc_ctrl_inst|Add0~53\ = CARRY((\gc_ctrl_inst|state.clk_cnt\(26) & !\gc_ctrl_inst|Add0~51\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt\(26),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~51\,
	combout => \gc_ctrl_inst|Add0~52_combout\,
	cout => \gc_ctrl_inst|Add0~53\);

-- Location: LCCOMB_X39_Y30_N18
\gc_ctrl_inst|Selector37~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector37~0_combout\ = (\gc_ctrl_inst|Add0~52_combout\ & !\gc_ctrl_inst|state.clk_cnt[6]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \gc_ctrl_inst|Add0~52_combout\,
	datad => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	combout => \gc_ctrl_inst|Selector37~0_combout\);

-- Location: FF_X39_Y30_N19
\gc_ctrl_inst|state.clk_cnt[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector37~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(26));

-- Location: LCCOMB_X40_Y30_N22
\gc_ctrl_inst|Add0~54\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~54_combout\ = (\gc_ctrl_inst|state.clk_cnt\(27) & (!\gc_ctrl_inst|Add0~53\)) # (!\gc_ctrl_inst|state.clk_cnt\(27) & ((\gc_ctrl_inst|Add0~53\) # (GND)))
-- \gc_ctrl_inst|Add0~55\ = CARRY((!\gc_ctrl_inst|Add0~53\) # (!\gc_ctrl_inst|state.clk_cnt\(27)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt\(27),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~53\,
	combout => \gc_ctrl_inst|Add0~54_combout\,
	cout => \gc_ctrl_inst|Add0~55\);

-- Location: LCCOMB_X39_Y30_N16
\gc_ctrl_inst|Selector36~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector36~0_combout\ = (\gc_ctrl_inst|Add0~54_combout\ & !\gc_ctrl_inst|state.clk_cnt[6]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|Add0~54_combout\,
	datad => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	combout => \gc_ctrl_inst|Selector36~0_combout\);

-- Location: FF_X39_Y30_N17
\gc_ctrl_inst|state.clk_cnt[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector36~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(27));

-- Location: LCCOMB_X39_Y30_N8
\gc_ctrl_inst|Equal0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal0~2_combout\ = (!\gc_ctrl_inst|state.clk_cnt\(25) & (!\gc_ctrl_inst|state.clk_cnt\(26) & (!\gc_ctrl_inst|state.clk_cnt\(24) & !\gc_ctrl_inst|state.clk_cnt\(27))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt\(25),
	datab => \gc_ctrl_inst|state.clk_cnt\(26),
	datac => \gc_ctrl_inst|state.clk_cnt\(24),
	datad => \gc_ctrl_inst|state.clk_cnt\(27),
	combout => \gc_ctrl_inst|Equal0~2_combout\);

-- Location: LCCOMB_X40_Y30_N24
\gc_ctrl_inst|Add0~56\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~56_combout\ = (\gc_ctrl_inst|state.clk_cnt\(28) & (\gc_ctrl_inst|Add0~55\ $ (GND))) # (!\gc_ctrl_inst|state.clk_cnt\(28) & (!\gc_ctrl_inst|Add0~55\ & VCC))
-- \gc_ctrl_inst|Add0~57\ = CARRY((\gc_ctrl_inst|state.clk_cnt\(28) & !\gc_ctrl_inst|Add0~55\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt\(28),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~55\,
	combout => \gc_ctrl_inst|Add0~56_combout\,
	cout => \gc_ctrl_inst|Add0~57\);

-- Location: LCCOMB_X41_Y30_N2
\gc_ctrl_inst|Selector35~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector35~0_combout\ = (\gc_ctrl_inst|Add0~56_combout\ & !\gc_ctrl_inst|state.clk_cnt[6]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|Add0~56_combout\,
	datac => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	combout => \gc_ctrl_inst|Selector35~0_combout\);

-- Location: FF_X41_Y30_N3
\gc_ctrl_inst|state.clk_cnt[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector35~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(28));

-- Location: LCCOMB_X40_Y30_N26
\gc_ctrl_inst|Add0~58\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~58_combout\ = (\gc_ctrl_inst|state.clk_cnt\(29) & (!\gc_ctrl_inst|Add0~57\)) # (!\gc_ctrl_inst|state.clk_cnt\(29) & ((\gc_ctrl_inst|Add0~57\) # (GND)))
-- \gc_ctrl_inst|Add0~59\ = CARRY((!\gc_ctrl_inst|Add0~57\) # (!\gc_ctrl_inst|state.clk_cnt\(29)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt\(29),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~57\,
	combout => \gc_ctrl_inst|Add0~58_combout\,
	cout => \gc_ctrl_inst|Add0~59\);

-- Location: LCCOMB_X41_Y30_N0
\gc_ctrl_inst|Selector34~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector34~0_combout\ = (!\gc_ctrl_inst|state.clk_cnt[6]~1_combout\ & \gc_ctrl_inst|Add0~58_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	datac => \gc_ctrl_inst|Add0~58_combout\,
	combout => \gc_ctrl_inst|Selector34~0_combout\);

-- Location: FF_X41_Y30_N1
\gc_ctrl_inst|state.clk_cnt[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector34~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(29));

-- Location: LCCOMB_X40_Y30_N28
\gc_ctrl_inst|Add0~60\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~60_combout\ = (\gc_ctrl_inst|state.clk_cnt\(30) & (\gc_ctrl_inst|Add0~59\ $ (GND))) # (!\gc_ctrl_inst|state.clk_cnt\(30) & (!\gc_ctrl_inst|Add0~59\ & VCC))
-- \gc_ctrl_inst|Add0~61\ = CARRY((\gc_ctrl_inst|state.clk_cnt\(30) & !\gc_ctrl_inst|Add0~59\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt\(30),
	datad => VCC,
	cin => \gc_ctrl_inst|Add0~59\,
	combout => \gc_ctrl_inst|Add0~60_combout\,
	cout => \gc_ctrl_inst|Add0~61\);

-- Location: LCCOMB_X41_Y31_N30
\gc_ctrl_inst|Selector33~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector33~0_combout\ = (!\gc_ctrl_inst|state.clk_cnt[6]~1_combout\ & \gc_ctrl_inst|Add0~60_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	datad => \gc_ctrl_inst|Add0~60_combout\,
	combout => \gc_ctrl_inst|Selector33~0_combout\);

-- Location: FF_X41_Y31_N31
\gc_ctrl_inst|state.clk_cnt[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector33~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(30));

-- Location: LCCOMB_X42_Y32_N6
\gc_ctrl_inst|Selector32~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector32~0_combout\ = (\gc_ctrl_inst|state.clk_cnt\(31) & ((\gc_ctrl_inst|state.fsm_state.WAIT_FALLING_EDGE_DATA~q\) # ((\gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\) # (\gc_ctrl_inst|state.fsm_state.POLL~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt\(31),
	datab => \gc_ctrl_inst|state.fsm_state.WAIT_FALLING_EDGE_DATA~q\,
	datac => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	datad => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	combout => \gc_ctrl_inst|Selector32~0_combout\);

-- Location: LCCOMB_X42_Y32_N0
\gc_ctrl_inst|Selector2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector2~0_combout\ = (!\gc_ctrl_inst|next_state_logic~0_combout\ & \gc_ctrl_inst|state.fsm_state.SEND_LOW~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|next_state_logic~0_combout\,
	datad => \gc_ctrl_inst|state.fsm_state.SEND_LOW~q\,
	combout => \gc_ctrl_inst|Selector2~0_combout\);

-- Location: LCCOMB_X40_Y30_N30
\gc_ctrl_inst|Add0~62\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Add0~62_combout\ = \gc_ctrl_inst|Add0~61\ $ (\gc_ctrl_inst|state.clk_cnt\(31))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.clk_cnt\(31),
	cin => \gc_ctrl_inst|Add0~61\,
	combout => \gc_ctrl_inst|Add0~62_combout\);

-- Location: LCCOMB_X43_Y32_N4
\gc_ctrl_inst|Selector32~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector32~1_combout\ = (\gc_ctrl_inst|state.fsm_state.WAIT_SAMPLE~q\) # (((!\gc_ctrl_inst|next_state_logic~1_combout\ & \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\)) # (!\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|next_state_logic~1_combout\,
	datab => \gc_ctrl_inst|state.fsm_state.SEND_HIGH~q\,
	datac => \gc_ctrl_inst|state.fsm_state.WAIT_SAMPLE~q\,
	datad => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	combout => \gc_ctrl_inst|Selector32~1_combout\);

-- Location: LCCOMB_X42_Y32_N10
\gc_ctrl_inst|Selector32~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector32~2_combout\ = (\gc_ctrl_inst|Selector32~0_combout\) # ((\gc_ctrl_inst|Add0~62_combout\ & ((\gc_ctrl_inst|Selector2~0_combout\) # (\gc_ctrl_inst|Selector32~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|Selector32~0_combout\,
	datab => \gc_ctrl_inst|Selector2~0_combout\,
	datac => \gc_ctrl_inst|Add0~62_combout\,
	datad => \gc_ctrl_inst|Selector32~1_combout\,
	combout => \gc_ctrl_inst|Selector32~2_combout\);

-- Location: FF_X42_Y32_N11
\gc_ctrl_inst|state.clk_cnt[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector32~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(31));

-- Location: LCCOMB_X41_Y30_N4
\gc_ctrl_inst|Equal0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal0~1_combout\ = (!\gc_ctrl_inst|state.clk_cnt\(28) & (!\gc_ctrl_inst|state.clk_cnt\(29) & (!\gc_ctrl_inst|state.clk_cnt\(30) & !\gc_ctrl_inst|state.clk_cnt\(31))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt\(28),
	datab => \gc_ctrl_inst|state.clk_cnt\(29),
	datac => \gc_ctrl_inst|state.clk_cnt\(30),
	datad => \gc_ctrl_inst|state.clk_cnt\(31),
	combout => \gc_ctrl_inst|Equal0~1_combout\);

-- Location: LCCOMB_X39_Y30_N28
\gc_ctrl_inst|Equal0~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal0~5_combout\ = (\gc_ctrl_inst|Equal0~3_combout\ & (\gc_ctrl_inst|Equal0~4_combout\ & (\gc_ctrl_inst|Equal0~2_combout\ & \gc_ctrl_inst|Equal0~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|Equal0~3_combout\,
	datab => \gc_ctrl_inst|Equal0~4_combout\,
	datac => \gc_ctrl_inst|Equal0~2_combout\,
	datad => \gc_ctrl_inst|Equal0~1_combout\,
	combout => \gc_ctrl_inst|Equal0~5_combout\);

-- Location: LCCOMB_X39_Y31_N30
\gc_ctrl_inst|Equal1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal1~0_combout\ = (!\gc_ctrl_inst|state.clk_cnt\(9) & (!\gc_ctrl_inst|state.clk_cnt\(8) & (!\gc_ctrl_inst|state.clk_cnt\(10) & !\gc_ctrl_inst|state.clk_cnt\(12))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt\(9),
	datab => \gc_ctrl_inst|state.clk_cnt\(8),
	datac => \gc_ctrl_inst|state.clk_cnt\(10),
	datad => \gc_ctrl_inst|state.clk_cnt\(12),
	combout => \gc_ctrl_inst|Equal1~0_combout\);

-- Location: LCCOMB_X40_Y32_N24
\gc_ctrl_inst|Equal1~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal1~2_combout\ = (\gc_ctrl_inst|Equal1~0_combout\ & \gc_ctrl_inst|state.clk_cnt\(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|Equal1~0_combout\,
	datad => \gc_ctrl_inst|state.clk_cnt\(1),
	combout => \gc_ctrl_inst|Equal1~2_combout\);

-- Location: LCCOMB_X40_Y32_N10
\gc_ctrl_inst|Equal2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal2~0_combout\ = (\gc_ctrl_inst|Equal0~5_combout\ & (\gc_ctrl_inst|Equal1~2_combout\ & (!\gc_ctrl_inst|state.clk_cnt\(6) & \gc_ctrl_inst|Equal0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|Equal0~5_combout\,
	datab => \gc_ctrl_inst|Equal1~2_combout\,
	datac => \gc_ctrl_inst|state.clk_cnt\(6),
	datad => \gc_ctrl_inst|Equal0~0_combout\,
	combout => \gc_ctrl_inst|Equal2~0_combout\);

-- Location: LCCOMB_X40_Y32_N0
\gc_ctrl_inst|next_state_logic~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|next_state_logic~1_combout\ = (\gc_ctrl_inst|Equal2~0_combout\ & ((\gc_ctrl_inst|state.poll_cmd\(24) & (\gc_ctrl_inst|Equal1~3_combout\)) # (!\gc_ctrl_inst|state.poll_cmd\(24) & ((\gc_ctrl_inst|Equal0~7_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|Equal2~0_combout\,
	datab => \gc_ctrl_inst|Equal1~3_combout\,
	datac => \gc_ctrl_inst|Equal0~7_combout\,
	datad => \gc_ctrl_inst|state.poll_cmd\(24),
	combout => \gc_ctrl_inst|next_state_logic~1_combout\);

-- Location: LCCOMB_X41_Y32_N26
\gc_ctrl_inst|state.clk_cnt[6]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.clk_cnt[6]~0_combout\ = (!\gc_ctrl_inst|state.fsm_state.SEND_LOW~q\ & ((\gc_ctrl_inst|Selector6~1_combout\) # ((\gc_ctrl_inst|next_state_logic~1_combout\ & !\gc_ctrl_inst|state.fsm_state.WAIT_SAMPLE~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|next_state_logic~1_combout\,
	datab => \gc_ctrl_inst|state.fsm_state.SEND_LOW~q\,
	datac => \gc_ctrl_inst|Selector6~1_combout\,
	datad => \gc_ctrl_inst|state.fsm_state.WAIT_SAMPLE~q\,
	combout => \gc_ctrl_inst|state.clk_cnt[6]~0_combout\);

-- Location: LCCOMB_X41_Y32_N2
\gc_ctrl_inst|state.clk_cnt[6]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.clk_cnt[6]~1_combout\ = (\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & ((\gc_ctrl_inst|state.clk_cnt[6]~0_combout\) # ((\gc_ctrl_inst|Selector6~2_combout\)))) # (!\gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\ & 
-- (((\gc_ctrl_inst|Equal0~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt[6]~0_combout\,
	datab => \gc_ctrl_inst|Equal0~10_combout\,
	datac => \gc_ctrl_inst|Selector6~2_combout\,
	datad => \gc_ctrl_inst|state.fsm_state.WAIT_TIMEOUT~q\,
	combout => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\);

-- Location: LCCOMB_X41_Y31_N12
\gc_ctrl_inst|Selector48~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector48~0_combout\ = (!\gc_ctrl_inst|state.clk_cnt[6]~1_combout\ & \gc_ctrl_inst|Add0~30_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \gc_ctrl_inst|state.clk_cnt[6]~1_combout\,
	datad => \gc_ctrl_inst|Add0~30_combout\,
	combout => \gc_ctrl_inst|Selector48~0_combout\);

-- Location: FF_X41_Y31_N13
\gc_ctrl_inst|state.clk_cnt[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector48~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|WideOr1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.clk_cnt\(15));

-- Location: LCCOMB_X41_Y31_N4
\gc_ctrl_inst|Equal0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal0~0_combout\ = (!\gc_ctrl_inst|state.clk_cnt\(15) & (!\gc_ctrl_inst|state.clk_cnt\(11) & (!\gc_ctrl_inst|state.clk_cnt\(13) & !\gc_ctrl_inst|state.clk_cnt\(14))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.clk_cnt\(15),
	datab => \gc_ctrl_inst|state.clk_cnt\(11),
	datac => \gc_ctrl_inst|state.clk_cnt\(13),
	datad => \gc_ctrl_inst|state.clk_cnt\(14),
	combout => \gc_ctrl_inst|Equal0~0_combout\);

-- Location: LCCOMB_X40_Y32_N12
\gc_ctrl_inst|Equal1~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Equal1~1_combout\ = (\gc_ctrl_inst|Equal0~0_combout\ & (\gc_ctrl_inst|Equal1~0_combout\ & (\gc_ctrl_inst|state.clk_cnt\(1) & \gc_ctrl_inst|Equal0~5_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|Equal0~0_combout\,
	datab => \gc_ctrl_inst|Equal1~0_combout\,
	datac => \gc_ctrl_inst|state.clk_cnt\(1),
	datad => \gc_ctrl_inst|Equal0~5_combout\,
	combout => \gc_ctrl_inst|Equal1~1_combout\);

-- Location: LCCOMB_X41_Y32_N30
\gc_ctrl_inst|Selector1~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector1~1_combout\ = (\gc_ctrl_inst|Selector1~0_combout\ & (((!\gc_ctrl_inst|Equal3~1_combout\) # (!\gc_ctrl_inst|state.fsm_state.WAIT_SAMPLE~q\)) # (!\gc_ctrl_inst|Equal1~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|Equal1~1_combout\,
	datab => \gc_ctrl_inst|state.fsm_state.WAIT_SAMPLE~q\,
	datac => \gc_ctrl_inst|Equal3~1_combout\,
	datad => \gc_ctrl_inst|Selector1~0_combout\,
	combout => \gc_ctrl_inst|Selector1~1_combout\);

-- Location: LCCOMB_X41_Y32_N10
\gc_ctrl_inst|Selector1~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector1~2_combout\ = (\gc_ctrl_inst|Selector1~1_combout\ & (\gc_ctrl_inst|Selector6~3_combout\ & (!\gc_ctrl_inst|Selector6~2_combout\ & !\gc_ctrl_inst|Selector6~5_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|Selector1~1_combout\,
	datab => \gc_ctrl_inst|Selector6~3_combout\,
	datac => \gc_ctrl_inst|Selector6~2_combout\,
	datad => \gc_ctrl_inst|Selector6~5_combout\,
	combout => \gc_ctrl_inst|Selector1~2_combout\);

-- Location: FF_X41_Y32_N11
\gc_ctrl_inst|state.fsm_state.POLL\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector1~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.fsm_state.POLL~q\);

-- Location: LCCOMB_X41_Y32_N14
\gc_ctrl_inst|Selector2~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|Selector2~1_combout\ = (\gc_ctrl_inst|state.fsm_state.POLL~q\ & ((\gc_ctrl_inst|LessThan0~1_combout\) # ((!\gc_ctrl_inst|next_state_logic~0_combout\ & \gc_ctrl_inst|state.fsm_state.SEND_LOW~q\)))) # (!\gc_ctrl_inst|state.fsm_state.POLL~q\ & 
-- (!\gc_ctrl_inst|next_state_logic~0_combout\ & (\gc_ctrl_inst|state.fsm_state.SEND_LOW~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \gc_ctrl_inst|state.fsm_state.POLL~q\,
	datab => \gc_ctrl_inst|next_state_logic~0_combout\,
	datac => \gc_ctrl_inst|state.fsm_state.SEND_LOW~q\,
	datad => \gc_ctrl_inst|LessThan0~1_combout\,
	combout => \gc_ctrl_inst|Selector2~1_combout\);

-- Location: FF_X41_Y32_N15
\gc_ctrl_inst|state.fsm_state.SEND_LOW\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|Selector2~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.fsm_state.SEND_LOW~q\);

-- Location: FF_X42_Y31_N13
\gc_ctrl_inst|state.response[63]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|data_synced~q\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(63));

-- Location: FF_X42_Y32_N25
\gc_ctrl_inst|state.ctrl_state.trigger_r[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(63),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.trigger_r\(0));

-- Location: LCCOMB_X42_Y31_N30
\gc_ctrl_inst|state.response[62]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[62]~feeder_combout\ = \gc_ctrl_inst|state.response\(63)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(63),
	combout => \gc_ctrl_inst|state.response[62]~feeder_combout\);

-- Location: FF_X42_Y31_N31
\gc_ctrl_inst|state.response[62]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[62]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(62));

-- Location: FF_X42_Y32_N3
\gc_ctrl_inst|state.ctrl_state.trigger_r[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(62),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.trigger_r\(1));

-- Location: FF_X42_Y31_N1
\gc_ctrl_inst|state.response[61]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(62),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(61));

-- Location: LCCOMB_X40_Y32_N16
\gc_ctrl_inst|state.ctrl_state.trigger_r[2]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.trigger_r[2]~feeder_combout\ = \gc_ctrl_inst|state.response\(61)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(61),
	combout => \gc_ctrl_inst|state.ctrl_state.trigger_r[2]~feeder_combout\);

-- Location: FF_X40_Y32_N17
\gc_ctrl_inst|state.ctrl_state.trigger_r[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.trigger_r[2]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.trigger_r\(2));

-- Location: LCCOMB_X42_Y31_N2
\gc_ctrl_inst|state.response[60]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[60]~feeder_combout\ = \gc_ctrl_inst|state.response\(61)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(61),
	combout => \gc_ctrl_inst|state.response[60]~feeder_combout\);

-- Location: FF_X42_Y31_N3
\gc_ctrl_inst|state.response[60]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[60]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(60));

-- Location: LCCOMB_X42_Y32_N28
\gc_ctrl_inst|state.ctrl_state.trigger_r[3]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.trigger_r[3]~feeder_combout\ = \gc_ctrl_inst|state.response\(60)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(60),
	combout => \gc_ctrl_inst|state.ctrl_state.trigger_r[3]~feeder_combout\);

-- Location: FF_X42_Y32_N29
\gc_ctrl_inst|state.ctrl_state.trigger_r[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.trigger_r[3]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.trigger_r\(3));

-- Location: LCCOMB_X42_Y31_N28
\gc_ctrl_inst|state.response[59]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[59]~feeder_combout\ = \gc_ctrl_inst|state.response\(60)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(60),
	combout => \gc_ctrl_inst|state.response[59]~feeder_combout\);

-- Location: FF_X42_Y31_N29
\gc_ctrl_inst|state.response[59]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[59]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(59));

-- Location: LCCOMB_X40_Y32_N18
\gc_ctrl_inst|state.ctrl_state.trigger_r[4]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.trigger_r[4]~feeder_combout\ = \gc_ctrl_inst|state.response\(59)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(59),
	combout => \gc_ctrl_inst|state.ctrl_state.trigger_r[4]~feeder_combout\);

-- Location: FF_X40_Y32_N19
\gc_ctrl_inst|state.ctrl_state.trigger_r[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.trigger_r[4]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.trigger_r\(4));

-- Location: LCCOMB_X42_Y31_N6
\gc_ctrl_inst|state.response[58]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[58]~feeder_combout\ = \gc_ctrl_inst|state.response\(59)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(59),
	combout => \gc_ctrl_inst|state.response[58]~feeder_combout\);

-- Location: FF_X42_Y31_N7
\gc_ctrl_inst|state.response[58]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[58]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(58));

-- Location: LCCOMB_X41_Y31_N24
\gc_ctrl_inst|state.ctrl_state.trigger_r[5]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.trigger_r[5]~feeder_combout\ = \gc_ctrl_inst|state.response\(58)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(58),
	combout => \gc_ctrl_inst|state.ctrl_state.trigger_r[5]~feeder_combout\);

-- Location: FF_X41_Y31_N25
\gc_ctrl_inst|state.ctrl_state.trigger_r[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.trigger_r[5]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.trigger_r\(5));

-- Location: LCCOMB_X42_Y31_N8
\gc_ctrl_inst|state.response[57]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[57]~feeder_combout\ = \gc_ctrl_inst|state.response\(58)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(58),
	combout => \gc_ctrl_inst|state.response[57]~feeder_combout\);

-- Location: FF_X42_Y31_N9
\gc_ctrl_inst|state.response[57]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[57]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(57));

-- Location: LCCOMB_X38_Y31_N24
\gc_ctrl_inst|state.ctrl_state.trigger_r[6]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.trigger_r[6]~feeder_combout\ = \gc_ctrl_inst|state.response\(57)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(57),
	combout => \gc_ctrl_inst|state.ctrl_state.trigger_r[6]~feeder_combout\);

-- Location: FF_X38_Y31_N25
\gc_ctrl_inst|state.ctrl_state.trigger_r[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.trigger_r[6]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.trigger_r\(6));

-- Location: FF_X42_Y31_N27
\gc_ctrl_inst|state.response[56]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(57),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(56));

-- Location: FF_X42_Y32_N23
\gc_ctrl_inst|state.ctrl_state.trigger_r[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(56),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.trigger_r\(7));

-- Location: FF_X42_Y31_N5
\gc_ctrl_inst|state.response[55]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(56),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(55));

-- Location: LCCOMB_X41_Y35_N24
\gc_ctrl_inst|state.ctrl_state.trigger_l[0]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.trigger_l[0]~feeder_combout\ = \gc_ctrl_inst|state.response\(55)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(55),
	combout => \gc_ctrl_inst|state.ctrl_state.trigger_l[0]~feeder_combout\);

-- Location: FF_X41_Y35_N25
\gc_ctrl_inst|state.ctrl_state.trigger_l[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.trigger_l[0]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.trigger_l\(0));

-- Location: FF_X42_Y31_N15
\gc_ctrl_inst|state.response[54]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(55),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(54));

-- Location: FF_X54_Y69_N1
\gc_ctrl_inst|state.ctrl_state.trigger_l[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(54),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.trigger_l\(1));

-- Location: FF_X42_Y31_N25
\gc_ctrl_inst|state.response[53]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(54),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(53));

-- Location: LCCOMB_X41_Y31_N26
\gc_ctrl_inst|state.ctrl_state.trigger_l[2]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.trigger_l[2]~feeder_combout\ = \gc_ctrl_inst|state.response\(53)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(53),
	combout => \gc_ctrl_inst|state.ctrl_state.trigger_l[2]~feeder_combout\);

-- Location: FF_X41_Y31_N27
\gc_ctrl_inst|state.ctrl_state.trigger_l[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.trigger_l[2]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.trigger_l\(2));

-- Location: LCCOMB_X42_Y31_N10
\gc_ctrl_inst|state.response[52]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[52]~feeder_combout\ = \gc_ctrl_inst|state.response\(53)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(53),
	combout => \gc_ctrl_inst|state.response[52]~feeder_combout\);

-- Location: FF_X42_Y31_N11
\gc_ctrl_inst|state.response[52]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[52]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(52));

-- Location: LCCOMB_X42_Y31_N16
\gc_ctrl_inst|state.ctrl_state.trigger_l[3]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.trigger_l[3]~feeder_combout\ = \gc_ctrl_inst|state.response\(52)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(52),
	combout => \gc_ctrl_inst|state.ctrl_state.trigger_l[3]~feeder_combout\);

-- Location: FF_X42_Y31_N17
\gc_ctrl_inst|state.ctrl_state.trigger_l[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.trigger_l[3]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.trigger_l\(3));

-- Location: LCCOMB_X42_Y31_N20
\gc_ctrl_inst|state.response[51]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[51]~feeder_combout\ = \gc_ctrl_inst|state.response\(52)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(52),
	combout => \gc_ctrl_inst|state.response[51]~feeder_combout\);

-- Location: FF_X42_Y31_N21
\gc_ctrl_inst|state.response[51]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[51]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(51));

-- Location: LCCOMB_X42_Y31_N18
\gc_ctrl_inst|state.ctrl_state.trigger_l[4]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.trigger_l[4]~feeder_combout\ = \gc_ctrl_inst|state.response\(51)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(51),
	combout => \gc_ctrl_inst|state.ctrl_state.trigger_l[4]~feeder_combout\);

-- Location: FF_X42_Y31_N19
\gc_ctrl_inst|state.ctrl_state.trigger_l[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.trigger_l[4]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.trigger_l\(4));

-- Location: LCCOMB_X42_Y31_N22
\gc_ctrl_inst|state.response[50]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[50]~feeder_combout\ = \gc_ctrl_inst|state.response\(51)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(51),
	combout => \gc_ctrl_inst|state.response[50]~feeder_combout\);

-- Location: FF_X42_Y31_N23
\gc_ctrl_inst|state.response[50]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[50]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(50));

-- Location: FF_X53_Y4_N25
\gc_ctrl_inst|state.ctrl_state.trigger_l[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(50),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.trigger_l\(5));

-- Location: FF_X53_Y4_N9
\gc_ctrl_inst|state.response[49]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(50),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(49));

-- Location: FF_X53_Y4_N3
\gc_ctrl_inst|state.ctrl_state.trigger_l[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(49),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.trigger_l\(6));

-- Location: FF_X53_Y4_N11
\gc_ctrl_inst|state.response[48]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(49),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(48));

-- Location: LCCOMB_X53_Y4_N4
\gc_ctrl_inst|state.ctrl_state.trigger_l[7]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.trigger_l[7]~feeder_combout\ = \gc_ctrl_inst|state.response\(48)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(48),
	combout => \gc_ctrl_inst|state.ctrl_state.trigger_l[7]~feeder_combout\);

-- Location: FF_X53_Y4_N5
\gc_ctrl_inst|state.ctrl_state.trigger_l[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.trigger_l[7]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.trigger_l\(7));

-- Location: LCCOMB_X53_Y4_N28
\gc_ctrl_inst|state.response[47]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[47]~feeder_combout\ = \gc_ctrl_inst|state.response\(48)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(48),
	combout => \gc_ctrl_inst|state.response[47]~feeder_combout\);

-- Location: FF_X53_Y4_N29
\gc_ctrl_inst|state.response[47]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[47]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(47));

-- Location: LCCOMB_X53_Y4_N30
\gc_ctrl_inst|state.ctrl_state.c_y[0]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.c_y[0]~feeder_combout\ = \gc_ctrl_inst|state.response\(47)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(47),
	combout => \gc_ctrl_inst|state.ctrl_state.c_y[0]~feeder_combout\);

-- Location: FF_X53_Y4_N31
\gc_ctrl_inst|state.ctrl_state.c_y[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.c_y[0]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.c_y\(0));

-- Location: LCCOMB_X53_Y4_N22
\gc_ctrl_inst|state.response[46]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[46]~feeder_combout\ = \gc_ctrl_inst|state.response\(47)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(47),
	combout => \gc_ctrl_inst|state.response[46]~feeder_combout\);

-- Location: FF_X53_Y4_N23
\gc_ctrl_inst|state.response[46]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[46]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(46));

-- Location: FF_X53_Y4_N17
\gc_ctrl_inst|state.ctrl_state.c_y[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(46),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.c_y\(1));

-- Location: FF_X53_Y4_N1
\gc_ctrl_inst|state.response[45]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(46),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(45));

-- Location: LCCOMB_X53_Y4_N26
\gc_ctrl_inst|state.ctrl_state.c_y[2]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.c_y[2]~feeder_combout\ = \gc_ctrl_inst|state.response\(45)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(45),
	combout => \gc_ctrl_inst|state.ctrl_state.c_y[2]~feeder_combout\);

-- Location: FF_X53_Y4_N27
\gc_ctrl_inst|state.ctrl_state.c_y[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.c_y[2]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.c_y\(2));

-- Location: LCCOMB_X53_Y4_N18
\gc_ctrl_inst|state.response[44]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[44]~feeder_combout\ = \gc_ctrl_inst|state.response\(45)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(45),
	combout => \gc_ctrl_inst|state.response[44]~feeder_combout\);

-- Location: FF_X53_Y4_N19
\gc_ctrl_inst|state.response[44]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[44]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(44));

-- Location: LCCOMB_X53_Y4_N12
\gc_ctrl_inst|state.ctrl_state.c_y[3]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.c_y[3]~feeder_combout\ = \gc_ctrl_inst|state.response\(44)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(44),
	combout => \gc_ctrl_inst|state.ctrl_state.c_y[3]~feeder_combout\);

-- Location: FF_X53_Y4_N13
\gc_ctrl_inst|state.ctrl_state.c_y[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.c_y[3]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.c_y\(3));

-- Location: LCCOMB_X53_Y4_N20
\gc_ctrl_inst|state.response[43]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[43]~feeder_combout\ = \gc_ctrl_inst|state.response\(44)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(44),
	combout => \gc_ctrl_inst|state.response[43]~feeder_combout\);

-- Location: FF_X53_Y4_N21
\gc_ctrl_inst|state.response[43]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[43]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(43));

-- Location: LCCOMB_X53_Y4_N6
\gc_ctrl_inst|state.ctrl_state.c_y[4]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.c_y[4]~feeder_combout\ = \gc_ctrl_inst|state.response\(43)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(43),
	combout => \gc_ctrl_inst|state.ctrl_state.c_y[4]~feeder_combout\);

-- Location: FF_X53_Y4_N7
\gc_ctrl_inst|state.ctrl_state.c_y[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.c_y[4]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.c_y\(4));

-- Location: LCCOMB_X53_Y4_N14
\gc_ctrl_inst|state.response[42]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[42]~feeder_combout\ = \gc_ctrl_inst|state.response\(43)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(43),
	combout => \gc_ctrl_inst|state.response[42]~feeder_combout\);

-- Location: FF_X53_Y4_N15
\gc_ctrl_inst|state.response[42]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[42]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(42));

-- Location: LCCOMB_X54_Y69_N2
\gc_ctrl_inst|state.ctrl_state.c_y[5]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.c_y[5]~feeder_combout\ = \gc_ctrl_inst|state.response\(42)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(42),
	combout => \gc_ctrl_inst|state.ctrl_state.c_y[5]~feeder_combout\);

-- Location: FF_X54_Y69_N3
\gc_ctrl_inst|state.ctrl_state.c_y[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.c_y[5]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.c_y\(5));

-- Location: LCCOMB_X54_Y69_N10
\gc_ctrl_inst|state.response[41]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[41]~feeder_combout\ = \gc_ctrl_inst|state.response\(42)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(42),
	combout => \gc_ctrl_inst|state.response[41]~feeder_combout\);

-- Location: FF_X54_Y69_N11
\gc_ctrl_inst|state.response[41]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[41]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(41));

-- Location: LCCOMB_X54_Y69_N20
\gc_ctrl_inst|state.ctrl_state.c_y[6]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.c_y[6]~feeder_combout\ = \gc_ctrl_inst|state.response\(41)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(41),
	combout => \gc_ctrl_inst|state.ctrl_state.c_y[6]~feeder_combout\);

-- Location: FF_X54_Y69_N21
\gc_ctrl_inst|state.ctrl_state.c_y[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.c_y[6]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.c_y\(6));

-- Location: LCCOMB_X54_Y69_N28
\gc_ctrl_inst|state.response[40]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[40]~feeder_combout\ = \gc_ctrl_inst|state.response\(41)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(41),
	combout => \gc_ctrl_inst|state.response[40]~feeder_combout\);

-- Location: FF_X54_Y69_N29
\gc_ctrl_inst|state.response[40]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[40]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(40));

-- Location: LCCOMB_X54_Y69_N14
\gc_ctrl_inst|state.ctrl_state.c_y[7]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.c_y[7]~feeder_combout\ = \gc_ctrl_inst|state.response\(40)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(40),
	combout => \gc_ctrl_inst|state.ctrl_state.c_y[7]~feeder_combout\);

-- Location: FF_X54_Y69_N15
\gc_ctrl_inst|state.ctrl_state.c_y[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.c_y[7]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.c_y\(7));

-- Location: LCCOMB_X54_Y69_N30
\gc_ctrl_inst|state.response[39]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[39]~feeder_combout\ = \gc_ctrl_inst|state.response\(40)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(40),
	combout => \gc_ctrl_inst|state.response[39]~feeder_combout\);

-- Location: FF_X54_Y69_N31
\gc_ctrl_inst|state.response[39]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[39]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(39));

-- Location: FF_X54_Y69_N9
\gc_ctrl_inst|state.ctrl_state.c_x[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(39),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.c_x\(0));

-- Location: FF_X54_Y69_N17
\gc_ctrl_inst|state.response[38]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(39),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(38));

-- Location: LCCOMB_X54_Y69_N26
\gc_ctrl_inst|state.ctrl_state.c_x[1]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.c_x[1]~feeder_combout\ = \gc_ctrl_inst|state.response\(38)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(38),
	combout => \gc_ctrl_inst|state.ctrl_state.c_x[1]~feeder_combout\);

-- Location: FF_X54_Y69_N27
\gc_ctrl_inst|state.ctrl_state.c_x[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.c_x[1]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.c_x\(1));

-- Location: LCCOMB_X54_Y69_N18
\gc_ctrl_inst|state.response[37]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[37]~feeder_combout\ = \gc_ctrl_inst|state.response\(38)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(38),
	combout => \gc_ctrl_inst|state.response[37]~feeder_combout\);

-- Location: FF_X54_Y69_N19
\gc_ctrl_inst|state.response[37]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[37]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(37));

-- Location: LCCOMB_X54_Y69_N4
\gc_ctrl_inst|state.ctrl_state.c_x[2]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.c_x[2]~feeder_combout\ = \gc_ctrl_inst|state.response\(37)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(37),
	combout => \gc_ctrl_inst|state.ctrl_state.c_x[2]~feeder_combout\);

-- Location: FF_X54_Y69_N5
\gc_ctrl_inst|state.ctrl_state.c_x[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.c_x[2]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.c_x\(2));

-- Location: LCCOMB_X54_Y69_N12
\gc_ctrl_inst|state.response[36]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[36]~feeder_combout\ = \gc_ctrl_inst|state.response\(37)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(37),
	combout => \gc_ctrl_inst|state.response[36]~feeder_combout\);

-- Location: FF_X54_Y69_N13
\gc_ctrl_inst|state.response[36]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[36]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(36));

-- Location: LCCOMB_X54_Y69_N6
\gc_ctrl_inst|state.ctrl_state.c_x[3]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.c_x[3]~feeder_combout\ = \gc_ctrl_inst|state.response\(36)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(36),
	combout => \gc_ctrl_inst|state.ctrl_state.c_x[3]~feeder_combout\);

-- Location: FF_X54_Y69_N7
\gc_ctrl_inst|state.ctrl_state.c_x[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.c_x[3]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.c_x\(3));

-- Location: LCCOMB_X55_Y69_N6
\gc_ctrl_inst|state.response[35]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[35]~feeder_combout\ = \gc_ctrl_inst|state.response\(36)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(36),
	combout => \gc_ctrl_inst|state.response[35]~feeder_combout\);

-- Location: FF_X55_Y69_N7
\gc_ctrl_inst|state.response[35]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[35]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(35));

-- Location: LCCOMB_X55_Y69_N24
\gc_ctrl_inst|state.ctrl_state.c_x[4]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.c_x[4]~feeder_combout\ = \gc_ctrl_inst|state.response\(35)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(35),
	combout => \gc_ctrl_inst|state.ctrl_state.c_x[4]~feeder_combout\);

-- Location: FF_X55_Y69_N25
\gc_ctrl_inst|state.ctrl_state.c_x[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.c_x[4]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.c_x\(4));

-- Location: LCCOMB_X55_Y69_N8
\gc_ctrl_inst|state.response[34]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[34]~feeder_combout\ = \gc_ctrl_inst|state.response\(35)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(35),
	combout => \gc_ctrl_inst|state.response[34]~feeder_combout\);

-- Location: FF_X55_Y69_N9
\gc_ctrl_inst|state.response[34]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[34]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(34));

-- Location: FF_X55_Y69_N3
\gc_ctrl_inst|state.ctrl_state.c_x[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(34),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.c_x\(5));

-- Location: FF_X55_Y69_N19
\gc_ctrl_inst|state.response[33]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(34),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(33));

-- Location: LCCOMB_X54_Y69_N24
\gc_ctrl_inst|state.ctrl_state.c_x[6]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.c_x[6]~feeder_combout\ = \gc_ctrl_inst|state.response\(33)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(33),
	combout => \gc_ctrl_inst|state.ctrl_state.c_x[6]~feeder_combout\);

-- Location: FF_X54_Y69_N25
\gc_ctrl_inst|state.ctrl_state.c_x[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.c_x[6]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.c_x\(6));

-- Location: LCCOMB_X55_Y69_N28
\gc_ctrl_inst|state.response[32]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[32]~feeder_combout\ = \gc_ctrl_inst|state.response\(33)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(33),
	combout => \gc_ctrl_inst|state.response[32]~feeder_combout\);

-- Location: FF_X55_Y69_N29
\gc_ctrl_inst|state.response[32]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[32]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(32));

-- Location: LCCOMB_X55_Y69_N4
\gc_ctrl_inst|state.ctrl_state.c_x[7]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.c_x[7]~feeder_combout\ = \gc_ctrl_inst|state.response\(32)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(32),
	combout => \gc_ctrl_inst|state.ctrl_state.c_x[7]~feeder_combout\);

-- Location: FF_X55_Y69_N5
\gc_ctrl_inst|state.ctrl_state.c_x[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.c_x[7]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.c_x\(7));

-- Location: LCCOMB_X55_Y69_N14
\gc_ctrl_inst|state.response[31]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[31]~feeder_combout\ = \gc_ctrl_inst|state.response\(32)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(32),
	combout => \gc_ctrl_inst|state.response[31]~feeder_combout\);

-- Location: FF_X55_Y69_N15
\gc_ctrl_inst|state.response[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[31]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(31));

-- Location: FF_X55_Y69_N31
\gc_ctrl_inst|state.ctrl_state.joy_y[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(31),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.joy_y\(0));

-- Location: FF_X55_Y69_N17
\gc_ctrl_inst|state.response[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(31),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(30));

-- Location: LCCOMB_X55_Y69_N0
\gc_ctrl_inst|state.ctrl_state.joy_y[1]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.joy_y[1]~feeder_combout\ = \gc_ctrl_inst|state.response\(30)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(30),
	combout => \gc_ctrl_inst|state.ctrl_state.joy_y[1]~feeder_combout\);

-- Location: FF_X55_Y69_N1
\gc_ctrl_inst|state.ctrl_state.joy_y[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.joy_y[1]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.joy_y\(1));

-- Location: LCCOMB_X55_Y69_N10
\gc_ctrl_inst|state.response[29]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[29]~feeder_combout\ = \gc_ctrl_inst|state.response\(30)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(30),
	combout => \gc_ctrl_inst|state.response[29]~feeder_combout\);

-- Location: FF_X55_Y69_N11
\gc_ctrl_inst|state.response[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[29]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(29));

-- Location: LCCOMB_X55_Y69_N26
\gc_ctrl_inst|state.ctrl_state.joy_y[2]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.joy_y[2]~feeder_combout\ = \gc_ctrl_inst|state.response\(29)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(29),
	combout => \gc_ctrl_inst|state.ctrl_state.joy_y[2]~feeder_combout\);

-- Location: FF_X55_Y69_N27
\gc_ctrl_inst|state.ctrl_state.joy_y[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.joy_y[2]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.joy_y\(2));

-- Location: LCCOMB_X55_Y69_N20
\gc_ctrl_inst|state.response[28]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[28]~feeder_combout\ = \gc_ctrl_inst|state.response\(29)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(29),
	combout => \gc_ctrl_inst|state.response[28]~feeder_combout\);

-- Location: FF_X55_Y69_N21
\gc_ctrl_inst|state.response[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[28]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(28));

-- Location: LCCOMB_X55_Y69_N12
\gc_ctrl_inst|state.ctrl_state.joy_y[3]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.joy_y[3]~feeder_combout\ = \gc_ctrl_inst|state.response\(28)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(28),
	combout => \gc_ctrl_inst|state.ctrl_state.joy_y[3]~feeder_combout\);

-- Location: FF_X55_Y69_N13
\gc_ctrl_inst|state.ctrl_state.joy_y[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.joy_y[3]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.joy_y\(3));

-- Location: LCCOMB_X55_Y69_N22
\gc_ctrl_inst|state.response[27]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[27]~feeder_combout\ = \gc_ctrl_inst|state.response\(28)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(28),
	combout => \gc_ctrl_inst|state.response[27]~feeder_combout\);

-- Location: FF_X55_Y69_N23
\gc_ctrl_inst|state.response[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[27]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(27));

-- Location: FF_X48_Y69_N1
\gc_ctrl_inst|state.ctrl_state.joy_y[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(27),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.joy_y\(4));

-- Location: FF_X48_Y69_N17
\gc_ctrl_inst|state.response[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(27),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(26));

-- Location: LCCOMB_X48_Y69_N26
\gc_ctrl_inst|state.ctrl_state.joy_y[5]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.joy_y[5]~feeder_combout\ = \gc_ctrl_inst|state.response\(26)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(26),
	combout => \gc_ctrl_inst|state.ctrl_state.joy_y[5]~feeder_combout\);

-- Location: FF_X48_Y69_N27
\gc_ctrl_inst|state.ctrl_state.joy_y[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.joy_y[5]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.joy_y\(5));

-- Location: LCCOMB_X48_Y69_N10
\gc_ctrl_inst|state.response[25]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[25]~feeder_combout\ = \gc_ctrl_inst|state.response\(26)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(26),
	combout => \gc_ctrl_inst|state.response[25]~feeder_combout\);

-- Location: FF_X48_Y69_N11
\gc_ctrl_inst|state.response[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[25]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(25));

-- Location: LCCOMB_X45_Y4_N8
\gc_ctrl_inst|state.ctrl_state.joy_y[6]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.joy_y[6]~feeder_combout\ = \gc_ctrl_inst|state.response\(25)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(25),
	combout => \gc_ctrl_inst|state.ctrl_state.joy_y[6]~feeder_combout\);

-- Location: FF_X45_Y4_N9
\gc_ctrl_inst|state.ctrl_state.joy_y[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.joy_y[6]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.joy_y\(6));

-- Location: LCCOMB_X45_Y4_N0
\gc_ctrl_inst|state.response[24]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[24]~feeder_combout\ = \gc_ctrl_inst|state.response\(25)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(25),
	combout => \gc_ctrl_inst|state.response[24]~feeder_combout\);

-- Location: FF_X45_Y4_N1
\gc_ctrl_inst|state.response[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[24]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(24));

-- Location: LCCOMB_X45_Y4_N26
\gc_ctrl_inst|state.ctrl_state.joy_y[7]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.joy_y[7]~feeder_combout\ = \gc_ctrl_inst|state.response\(24)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(24),
	combout => \gc_ctrl_inst|state.ctrl_state.joy_y[7]~feeder_combout\);

-- Location: FF_X45_Y4_N27
\gc_ctrl_inst|state.ctrl_state.joy_y[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.joy_y[7]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.joy_y\(7));

-- Location: LCCOMB_X45_Y4_N10
\gc_ctrl_inst|state.response[23]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[23]~feeder_combout\ = \gc_ctrl_inst|state.response\(24)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(24),
	combout => \gc_ctrl_inst|state.response[23]~feeder_combout\);

-- Location: FF_X45_Y4_N11
\gc_ctrl_inst|state.response[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[23]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(23));

-- Location: LCCOMB_X45_Y4_N12
\gc_ctrl_inst|state.ctrl_state.joy_x[0]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.joy_x[0]~feeder_combout\ = \gc_ctrl_inst|state.response\(23)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(23),
	combout => \gc_ctrl_inst|state.ctrl_state.joy_x[0]~feeder_combout\);

-- Location: FF_X45_Y4_N13
\gc_ctrl_inst|state.ctrl_state.joy_x[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.joy_x[0]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.joy_x\(0));

-- Location: LCCOMB_X45_Y4_N28
\gc_ctrl_inst|state.response[22]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[22]~feeder_combout\ = \gc_ctrl_inst|state.response\(23)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(23),
	combout => \gc_ctrl_inst|state.response[22]~feeder_combout\);

-- Location: FF_X45_Y4_N29
\gc_ctrl_inst|state.response[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[22]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(22));

-- Location: LCCOMB_X45_Y4_N14
\gc_ctrl_inst|state.ctrl_state.joy_x[1]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.joy_x[1]~feeder_combout\ = \gc_ctrl_inst|state.response\(22)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(22),
	combout => \gc_ctrl_inst|state.ctrl_state.joy_x[1]~feeder_combout\);

-- Location: FF_X45_Y4_N15
\gc_ctrl_inst|state.ctrl_state.joy_x[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.joy_x[1]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.joy_x\(1));

-- Location: LCCOMB_X45_Y4_N30
\gc_ctrl_inst|state.response[21]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[21]~feeder_combout\ = \gc_ctrl_inst|state.response\(22)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(22),
	combout => \gc_ctrl_inst|state.response[21]~feeder_combout\);

-- Location: FF_X45_Y4_N31
\gc_ctrl_inst|state.response[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[21]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(21));

-- Location: FF_X45_Y4_N25
\gc_ctrl_inst|state.ctrl_state.joy_x[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(21),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.joy_x\(2));

-- Location: FF_X45_Y4_N17
\gc_ctrl_inst|state.response[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(21),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(20));

-- Location: LCCOMB_X45_Y4_N2
\gc_ctrl_inst|state.ctrl_state.joy_x[3]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.joy_x[3]~feeder_combout\ = \gc_ctrl_inst|state.response\(20)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(20),
	combout => \gc_ctrl_inst|state.ctrl_state.joy_x[3]~feeder_combout\);

-- Location: FF_X45_Y4_N3
\gc_ctrl_inst|state.ctrl_state.joy_x[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.joy_x[3]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.joy_x\(3));

-- Location: LCCOMB_X45_Y4_N18
\gc_ctrl_inst|state.response[19]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[19]~feeder_combout\ = \gc_ctrl_inst|state.response\(20)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(20),
	combout => \gc_ctrl_inst|state.response[19]~feeder_combout\);

-- Location: FF_X45_Y4_N19
\gc_ctrl_inst|state.response[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[19]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(19));

-- Location: LCCOMB_X45_Y4_N4
\gc_ctrl_inst|state.ctrl_state.joy_x[4]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.joy_x[4]~feeder_combout\ = \gc_ctrl_inst|state.response\(19)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(19),
	combout => \gc_ctrl_inst|state.ctrl_state.joy_x[4]~feeder_combout\);

-- Location: FF_X45_Y4_N5
\gc_ctrl_inst|state.ctrl_state.joy_x[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.joy_x[4]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.joy_x\(4));

-- Location: LCCOMB_X45_Y4_N20
\gc_ctrl_inst|state.response[18]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[18]~feeder_combout\ = \gc_ctrl_inst|state.response\(19)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(19),
	combout => \gc_ctrl_inst|state.response[18]~feeder_combout\);

-- Location: FF_X45_Y4_N21
\gc_ctrl_inst|state.response[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[18]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(18));

-- Location: LCCOMB_X45_Y4_N22
\gc_ctrl_inst|state.ctrl_state.joy_x[5]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.joy_x[5]~feeder_combout\ = \gc_ctrl_inst|state.response\(18)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(18),
	combout => \gc_ctrl_inst|state.ctrl_state.joy_x[5]~feeder_combout\);

-- Location: FF_X45_Y4_N23
\gc_ctrl_inst|state.ctrl_state.joy_x[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.joy_x[5]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.joy_x\(5));

-- Location: LCCOMB_X45_Y4_N6
\gc_ctrl_inst|state.response[17]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[17]~feeder_combout\ = \gc_ctrl_inst|state.response\(18)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(18),
	combout => \gc_ctrl_inst|state.response[17]~feeder_combout\);

-- Location: FF_X45_Y4_N7
\gc_ctrl_inst|state.response[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[17]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(17));

-- Location: FF_X49_Y4_N1
\gc_ctrl_inst|state.ctrl_state.joy_x[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(17),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.joy_x\(6));

-- Location: FF_X49_Y4_N9
\gc_ctrl_inst|state.response[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(17),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(16));

-- Location: FF_X49_Y4_N27
\gc_ctrl_inst|state.ctrl_state.joy_x[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(16),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.joy_x\(7));

-- Location: FF_X49_Y4_N11
\gc_ctrl_inst|state.response[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(16),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(15));

-- Location: LCCOMB_X49_Y4_N28
\gc_ctrl_inst|state.ctrl_state.btn_left~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.btn_left~feeder_combout\ = \gc_ctrl_inst|state.response\(15)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(15),
	combout => \gc_ctrl_inst|state.ctrl_state.btn_left~feeder_combout\);

-- Location: FF_X49_Y4_N29
\gc_ctrl_inst|state.ctrl_state.btn_left\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.btn_left~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.btn_left~q\);

-- Location: LCCOMB_X49_Y4_N12
\gc_ctrl_inst|state.response[14]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[14]~feeder_combout\ = \gc_ctrl_inst|state.response\(15)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(15),
	combout => \gc_ctrl_inst|state.response[14]~feeder_combout\);

-- Location: FF_X49_Y4_N13
\gc_ctrl_inst|state.response[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[14]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(14));

-- Location: LCCOMB_X49_Y4_N22
\gc_ctrl_inst|state.ctrl_state.btn_right~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.btn_right~feeder_combout\ = \gc_ctrl_inst|state.response\(14)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(14),
	combout => \gc_ctrl_inst|state.ctrl_state.btn_right~feeder_combout\);

-- Location: FF_X49_Y4_N23
\gc_ctrl_inst|state.ctrl_state.btn_right\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.btn_right~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.btn_right~q\);

-- Location: LCCOMB_X49_Y4_N14
\gc_ctrl_inst|state.response[13]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[13]~feeder_combout\ = \gc_ctrl_inst|state.response\(14)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(14),
	combout => \gc_ctrl_inst|state.response[13]~feeder_combout\);

-- Location: FF_X49_Y4_N15
\gc_ctrl_inst|state.response[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[13]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(13));

-- Location: FF_X49_Y4_N17
\gc_ctrl_inst|state.ctrl_state.btn_down\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(13),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.btn_down~q\);

-- Location: FF_X49_Y4_N25
\gc_ctrl_inst|state.response[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(13),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(12));

-- Location: LCCOMB_X49_Y4_N18
\gc_ctrl_inst|state.ctrl_state.btn_up~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.btn_up~feeder_combout\ = \gc_ctrl_inst|state.response\(12)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(12),
	combout => \gc_ctrl_inst|state.ctrl_state.btn_up~feeder_combout\);

-- Location: FF_X49_Y4_N19
\gc_ctrl_inst|state.ctrl_state.btn_up\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.btn_up~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.btn_up~q\);

-- Location: LCCOMB_X49_Y4_N2
\gc_ctrl_inst|state.response[11]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[11]~feeder_combout\ = \gc_ctrl_inst|state.response\(12)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(12),
	combout => \gc_ctrl_inst|state.response[11]~feeder_combout\);

-- Location: FF_X49_Y4_N3
\gc_ctrl_inst|state.response[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[11]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(11));

-- Location: LCCOMB_X49_Y4_N4
\gc_ctrl_inst|state.ctrl_state.btn_z~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.btn_z~feeder_combout\ = \gc_ctrl_inst|state.response\(11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(11),
	combout => \gc_ctrl_inst|state.ctrl_state.btn_z~feeder_combout\);

-- Location: FF_X49_Y4_N5
\gc_ctrl_inst|state.ctrl_state.btn_z\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.btn_z~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.btn_z~q\);

-- Location: LCCOMB_X49_Y4_N20
\gc_ctrl_inst|state.response[10]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[10]~feeder_combout\ = \gc_ctrl_inst|state.response\(11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(11),
	combout => \gc_ctrl_inst|state.response[10]~feeder_combout\);

-- Location: FF_X49_Y4_N21
\gc_ctrl_inst|state.response[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[10]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(10));

-- Location: LCCOMB_X49_Y4_N6
\gc_ctrl_inst|state.ctrl_state.btn_r~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.btn_r~feeder_combout\ = \gc_ctrl_inst|state.response\(10)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(10),
	combout => \gc_ctrl_inst|state.ctrl_state.btn_r~feeder_combout\);

-- Location: FF_X49_Y4_N7
\gc_ctrl_inst|state.ctrl_state.btn_r\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.btn_r~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.btn_r~q\);

-- Location: LCCOMB_X49_Y4_N30
\gc_ctrl_inst|state.response[9]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[9]~feeder_combout\ = \gc_ctrl_inst|state.response\(10)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(10),
	combout => \gc_ctrl_inst|state.response[9]~feeder_combout\);

-- Location: FF_X49_Y4_N31
\gc_ctrl_inst|state.response[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[9]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(9));

-- Location: FF_X48_Y69_N13
\gc_ctrl_inst|state.ctrl_state.btn_l\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(9),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.btn_l~q\);

-- Location: FF_X48_Y69_N7
\gc_ctrl_inst|state.response[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(9),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(8));

-- Location: LCCOMB_X48_Y69_N28
\gc_ctrl_inst|state.response[7]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[7]~feeder_combout\ = \gc_ctrl_inst|state.response\(8)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(8),
	combout => \gc_ctrl_inst|state.response[7]~feeder_combout\);

-- Location: FF_X48_Y69_N29
\gc_ctrl_inst|state.response[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[7]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(7));

-- Location: LCCOMB_X48_Y69_N30
\gc_ctrl_inst|state.ctrl_state.btn_a~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.btn_a~feeder_combout\ = \gc_ctrl_inst|state.response\(7)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(7),
	combout => \gc_ctrl_inst|state.ctrl_state.btn_a~feeder_combout\);

-- Location: FF_X48_Y69_N31
\gc_ctrl_inst|state.ctrl_state.btn_a\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.btn_a~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.btn_a~q\);

-- Location: LCCOMB_X48_Y69_N22
\gc_ctrl_inst|state.response[6]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[6]~feeder_combout\ = \gc_ctrl_inst|state.response\(7)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(7),
	combout => \gc_ctrl_inst|state.response[6]~feeder_combout\);

-- Location: FF_X48_Y69_N23
\gc_ctrl_inst|state.response[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[6]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(6));

-- Location: FF_X48_Y69_N9
\gc_ctrl_inst|state.ctrl_state.btn_b\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(6),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.btn_b~q\);

-- Location: FF_X48_Y69_N25
\gc_ctrl_inst|state.response[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \gc_ctrl_inst|state.response\(6),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(5));

-- Location: LCCOMB_X48_Y69_N2
\gc_ctrl_inst|state.ctrl_state.btn_x~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.btn_x~feeder_combout\ = \gc_ctrl_inst|state.response\(5)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(5),
	combout => \gc_ctrl_inst|state.ctrl_state.btn_x~feeder_combout\);

-- Location: FF_X48_Y69_N3
\gc_ctrl_inst|state.ctrl_state.btn_x\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.btn_x~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.btn_x~q\);

-- Location: LCCOMB_X48_Y69_N18
\gc_ctrl_inst|state.response[4]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[4]~feeder_combout\ = \gc_ctrl_inst|state.response\(5)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(5),
	combout => \gc_ctrl_inst|state.response[4]~feeder_combout\);

-- Location: FF_X48_Y69_N19
\gc_ctrl_inst|state.response[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[4]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(4));

-- Location: LCCOMB_X48_Y69_N4
\gc_ctrl_inst|state.ctrl_state.btn_y~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.btn_y~feeder_combout\ = \gc_ctrl_inst|state.response\(4)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(4),
	combout => \gc_ctrl_inst|state.ctrl_state.btn_y~feeder_combout\);

-- Location: FF_X48_Y69_N5
\gc_ctrl_inst|state.ctrl_state.btn_y\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.btn_y~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.btn_y~q\);

-- Location: LCCOMB_X48_Y69_N20
\gc_ctrl_inst|state.response[3]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.response[3]~feeder_combout\ = \gc_ctrl_inst|state.response\(4)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(4),
	combout => \gc_ctrl_inst|state.response[3]~feeder_combout\);

-- Location: FF_X48_Y69_N21
\gc_ctrl_inst|state.response[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.response[3]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|state.fsm_state.SAMPLE_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.response\(3));

-- Location: LCCOMB_X48_Y69_N14
\gc_ctrl_inst|state.ctrl_state.btn_start~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \gc_ctrl_inst|state.ctrl_state.btn_start~feeder_combout\ = \gc_ctrl_inst|state.response\(3)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \gc_ctrl_inst|state.response\(3),
	combout => \gc_ctrl_inst|state.ctrl_state.btn_start~feeder_combout\);

-- Location: FF_X48_Y69_N15
\gc_ctrl_inst|state.ctrl_state.btn_start\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \gc_ctrl_inst|state.ctrl_state.btn_start~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \gc_ctrl_inst|ALT_INV_state.fsm_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \gc_ctrl_inst|state.ctrl_state.btn_start~q\);

ww_ctrl_state(0) <= \ctrl_state[0]~output_o\;

ww_ctrl_state(1) <= \ctrl_state[1]~output_o\;

ww_ctrl_state(2) <= \ctrl_state[2]~output_o\;

ww_ctrl_state(3) <= \ctrl_state[3]~output_o\;

ww_ctrl_state(4) <= \ctrl_state[4]~output_o\;

ww_ctrl_state(5) <= \ctrl_state[5]~output_o\;

ww_ctrl_state(6) <= \ctrl_state[6]~output_o\;

ww_ctrl_state(7) <= \ctrl_state[7]~output_o\;

ww_ctrl_state(8) <= \ctrl_state[8]~output_o\;

ww_ctrl_state(9) <= \ctrl_state[9]~output_o\;

ww_ctrl_state(10) <= \ctrl_state[10]~output_o\;

ww_ctrl_state(11) <= \ctrl_state[11]~output_o\;

ww_ctrl_state(12) <= \ctrl_state[12]~output_o\;

ww_ctrl_state(13) <= \ctrl_state[13]~output_o\;

ww_ctrl_state(14) <= \ctrl_state[14]~output_o\;

ww_ctrl_state(15) <= \ctrl_state[15]~output_o\;

ww_ctrl_state(16) <= \ctrl_state[16]~output_o\;

ww_ctrl_state(17) <= \ctrl_state[17]~output_o\;

ww_ctrl_state(18) <= \ctrl_state[18]~output_o\;

ww_ctrl_state(19) <= \ctrl_state[19]~output_o\;

ww_ctrl_state(20) <= \ctrl_state[20]~output_o\;

ww_ctrl_state(21) <= \ctrl_state[21]~output_o\;

ww_ctrl_state(22) <= \ctrl_state[22]~output_o\;

ww_ctrl_state(23) <= \ctrl_state[23]~output_o\;

ww_ctrl_state(24) <= \ctrl_state[24]~output_o\;

ww_ctrl_state(25) <= \ctrl_state[25]~output_o\;

ww_ctrl_state(26) <= \ctrl_state[26]~output_o\;

ww_ctrl_state(27) <= \ctrl_state[27]~output_o\;

ww_ctrl_state(28) <= \ctrl_state[28]~output_o\;

ww_ctrl_state(29) <= \ctrl_state[29]~output_o\;

ww_ctrl_state(30) <= \ctrl_state[30]~output_o\;

ww_ctrl_state(31) <= \ctrl_state[31]~output_o\;

ww_ctrl_state(32) <= \ctrl_state[32]~output_o\;

ww_ctrl_state(33) <= \ctrl_state[33]~output_o\;

ww_ctrl_state(34) <= \ctrl_state[34]~output_o\;

ww_ctrl_state(35) <= \ctrl_state[35]~output_o\;

ww_ctrl_state(36) <= \ctrl_state[36]~output_o\;

ww_ctrl_state(37) <= \ctrl_state[37]~output_o\;

ww_ctrl_state(38) <= \ctrl_state[38]~output_o\;

ww_ctrl_state(39) <= \ctrl_state[39]~output_o\;

ww_ctrl_state(40) <= \ctrl_state[40]~output_o\;

ww_ctrl_state(41) <= \ctrl_state[41]~output_o\;

ww_ctrl_state(42) <= \ctrl_state[42]~output_o\;

ww_ctrl_state(43) <= \ctrl_state[43]~output_o\;

ww_ctrl_state(44) <= \ctrl_state[44]~output_o\;

ww_ctrl_state(45) <= \ctrl_state[45]~output_o\;

ww_ctrl_state(46) <= \ctrl_state[46]~output_o\;

ww_ctrl_state(47) <= \ctrl_state[47]~output_o\;

ww_ctrl_state(48) <= \ctrl_state[48]~output_o\;

ww_ctrl_state(49) <= \ctrl_state[49]~output_o\;

ww_ctrl_state(50) <= \ctrl_state[50]~output_o\;

ww_ctrl_state(51) <= \ctrl_state[51]~output_o\;

ww_ctrl_state(52) <= \ctrl_state[52]~output_o\;

ww_ctrl_state(53) <= \ctrl_state[53]~output_o\;

ww_ctrl_state(54) <= \ctrl_state[54]~output_o\;

ww_ctrl_state(55) <= \ctrl_state[55]~output_o\;

ww_ctrl_state(56) <= \ctrl_state[56]~output_o\;

ww_ctrl_state(57) <= \ctrl_state[57]~output_o\;

ww_ctrl_state(58) <= \ctrl_state[58]~output_o\;

ww_ctrl_state(59) <= \ctrl_state[59]~output_o\;

ww_ctrl_state(60) <= \ctrl_state[60]~output_o\;

ww_ctrl_state(61) <= \ctrl_state[61]~output_o\;

ww_ctrl_state(62) <= \ctrl_state[62]~output_o\;

ww_ctrl_state(63) <= \ctrl_state[63]~output_o\;

data <= \data~output_o\;
END structure;


