library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.gc_ctrl_pkg.all;

entity precompiled_gc_ctrl is
	port (
		clk : in std_logic;
		res_n : in std_logic;

		-- connection to the controller
		data : inout std_logic;

		-- internal connection
		ctrl_state : out gc_ctrl_state_t;
		rumble : in std_logic
	);
end entity;

architecture rtl of precompiled_gc_ctrl is
	component gc_ctrl_top is
		port (
			clk : in std_logic;
			res_n : in std_logic;
			data : inout std_logic;
			ctrl_state : out std_logic_vector(63 downto 0);
			rumble : in std_logic
		);
	end component;
	signal ctrl_state_int : std_logic_vector(63 downto 0);
begin
	gc_ctrl_top_inst : gc_ctrl_top
	port map (
		clk         => clk,
		res_n       => res_n,
		data        => data,
		ctrl_state  => ctrl_state_int,
		rumble      => rumble
	);
	ctrl_state <= to_gc_ctrl_state(ctrl_state_int);
end architecture;
