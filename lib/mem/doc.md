
# On-Chip Memory
Important components in nearly every integrated circuit are memories. 
If storage with full access speed is required, only on-chip memories are viable options.
This package provides an easy way to instantiate on-chip memory, with different access strategies.

Currently, there are three memories types available, a single-clock dual-port RAM with one read and one write port and two single-clock FIFOs with one read and one write port.
The FIFOs differ in how their read ports are operated. 
While one has a classic read port controlled by a read flag, where the data is then available in the next clock cycle, the other one exhibits first word fall through (FWFT) behavior. 


## Dependencies
* Mathematical support package (`math_pkg`) 


## Required Files
 * `mem_pkg.vhd`
 * `dp_ram_1c1r1w_rdw.vhd`
 * `dp_ram_2c2rw_byteen.vhd`
 * `fifo_1c1r1w_fwft.vhd`
 * `dp_ram_2c2rw.vhd`
 * `fifo_1c1r1w.vhd`
 * `dp_ram_1c1r1w.vhd`

## Components

### Single-clock dual-port RAM

#### Component Declaration

```vhdl
component dp_ram_1c1r1w is
	generic (
		ADDR_WIDTH : integer;
		DATA_WIDTH : integer
	);
	port (
		clk : in std_logic;
		-- read port
		rd1_addr : in std_logic_vector(ADDR_WIDTH - 1 downto 0);
		rd1_data : out std_logic_vector(DATA_WIDTH - 1 downto 0);
		rd1 : in std_logic;
		-- write port
		wr2_addr : in std_logic_vector(ADDR_WIDTH - 1 downto 0);
		wr2_data : in std_logic_vector(DATA_WIDTH - 1 downto 0);
		wr2 : in std_logic
	);
end component;
```

#### Interface Description
The size of the memory is defined by the generics `ADDR_WIDTH` and `DATA_WIDTH`. 
It has 2^`ADDR_WITH` memory location storing `DATA_WIDTH` bits each.
Hence, the total number of bits that can be stored is 2^`ADDR_WITH`*`DATA_WIDTH`.

A standard synchronous memory access protocol is used for accessing the RAM.
At any rising edge of the `clk` signal, when the `rd1` signal is high, the data word stored at the address `rd1_addr` is written to the `rd1_data` port.

![RAM read timing](.mdata/dp_ram_1c1r1w_read_timing.svg)

At any rising edge of the `clk` signal, when the `wr2` signal is high, the data word at `wr2_data` is written to address `wr2_addr`.

![RAM write timing](.mdata/dp_ram_1c1r1w_write_timing.svg)

### Single-clock dual-port RAM with New Data Read-During-Write

#### Component Declaration

```vhdl
component dp_ram_1c1r1w_rdw is
generic (
	ADDR_WIDTH : integer;
	DATA_WIDTH : integer
);
port (
	clk           : in std_logic;
	rd1_addr      : in std_logic_vector(ADDR_WIDTH-1 downto 0);
	rd1_data      : out std_logic_vector(DATA_WIDTH-1 downto 0);
	wr2_addr      : in std_logic_vector(ADDR_WIDTH-1 downto 0);
	wr2_data      : in std_logic_vector(DATA_WIDTH-1 downto 0);
	wr2           : in std_logic
);
end component;
```

#### Interface Description
This RAM mostly behaves exactly the same as the above single-clock dual-port RAM.
The main difference is that the applied data `wr2_data` is read at the read port `rd1_data` whenever the two addresses `wr2_addr` and `rd1_addr` are the same during a write (`wr` asserted).
The following timing diagram illustrates this:

![New data read during write timing](.mdata/dp_ram_1c1r1w_rdw_timing.svg)

### Single-clock FIFO

#### Component Declaration

```vhdl
component fifo_1c1r1w is
	generic (
		DEPTH : integer;
		DATA_WIDTH : integer
	);
	port (
		clk : in std_logic;
		res_n : in std_logic;
		-- read port
		rd_data : out std_logic_vector(DATA_WIDTH - 1 downto 0);
		rd : in std_logic;
		-- write port
		wr_data : in std_logic_vector(DATA_WIDTH - 1 downto 0);
		wr : in std_logic;
		-- status flags
		empty : out std_logic;
		full : out std_logic;
		half_full : out std_logic
	);
end component;
```

#### Interface Description
The FIFO memory uses a similar interface but does not require the address inputs.
The read operation is again initiated by asserting the `rd` signal.
If the FIFO is not empty the next data word is assigned to the output `rd_data` (see the figure below).
If the FIFO is empty, the result of the read operation is undefined.

![FIFO read timing](.mdata/fifo_1c1r1w_read_timing.svg)

Asserting the input `wr` performs a write operation on the FIFO.
The data word at `wr_data` is stored to the next free location of the internal memory (see the figure below).
While the FIFO is full, write operations are ignored.


![FIFO write timing](.mdata/fifo_1c1r1w_write_timing.svg)


If the first item is written to the FIFO, the `empty` signal becomes zero in parallel to the storage operation.
If the last item is read from the FIFO, the `empty` signal becomes one at the same time the output data is set (see the figure below).

![FIFO empty handling](.mdata/fifo_1c1r1w_empty_handling_timing.svg)


If the FIFO becomes full by a write operation, parallel to the storing process the `full` signal becomes one. 
If afterwards a data word is read, the `full` signals becomes zero again at the same time as the output port is set (see the figure below).

![FIFO full handling](.mdata/fifo_1c1r1w_full_handling_timing.svg)



### Single-clock FIFO with FWFT behavior

#### Component Declaration

```vhdl
component fifo_1c1r1w_fwft is
	generic (
		DEPTH : integer;
		DATA_WIDTH : integer
	);
	port (
		clk : in std_logic;
		res_n : in std_logic;
		-- read port
		rd_data : out std_logic_vector(DATA_WIDTH - 1 downto 0);
		rd_ack : in std_logic;
		rd_valid : out std_logic;
		-- write port
		wr_data : in std_logic_vector(DATA_WIDTH - 1 downto 0);
		wr : in std_logic;
		-- status flags
		full : out std_logic;
		half_full : out std_logic
	);
end component;
```

#### Interface Description
The write port of the `fifo_1c1r1w_fwft` behaves exactly to same as for the other FIFO.
The timing diagram below demonstrates how the read port is operated.

![FIFO FWFT read timing](.mdata/fifo_1c1r1w_fwft_read_timing.svg)

As soon as the FIFO contains data the `rd_valid` signal is asserted and the next data value can be retrieved at the `rd_data` output.
This data value will be kept as long as it is not acknowledged by a high signal level at `rd_ack`. 
If `rd_ack` is asserted, the FIFO will either deassert `rd_valid` in the next clock cycle, indicating that the FIFO is now empty, or output the next data value at `rd_data`.
Note that `rd_ack` must only be asserted if `rd_valid` is asserted.
The role of the `empty` signal is now covered by the `rd_valid` signal.

The name for the read behavior of this FIFO comes from the fact, that no interaction is necessary to retrieve the first data value of the FIFO, i.e., it simply "falls through" the FIFO.

