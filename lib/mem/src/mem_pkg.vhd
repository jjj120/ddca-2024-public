library ieee;
use ieee.std_logic_1164.all;
use work.math_pkg.all;

package mem_pkg is

	component dp_ram_1c1r1w is
		generic (
			ADDR_WIDTH : integer;
			DATA_WIDTH : integer
		);
		port (
			clk : in std_logic;
			-- read port
			rd1_addr : in std_logic_vector(ADDR_WIDTH - 1 downto 0);
			rd1_data : out std_logic_vector(DATA_WIDTH - 1 downto 0);
			rd1 : in std_logic;
			-- write port
			wr2_addr : in std_logic_vector(ADDR_WIDTH - 1 downto 0);
			wr2_data : in std_logic_vector(DATA_WIDTH - 1 downto 0);
			wr2 : in std_logic
		);
	end component;
	
	component dp_ram_1c1r1w_rdw is
	generic (
		ADDR_WIDTH : integer;
		DATA_WIDTH : integer
	);
	port (
		clk           : in std_logic;
		rd1_addr      : in std_logic_vector(ADDR_WIDTH-1 downto 0);
		rd1_data      : out std_logic_vector(DATA_WIDTH-1 downto 0);
		wr2_addr      : in std_logic_vector(ADDR_WIDTH-1 downto 0);
		wr2_data      : in std_logic_vector(DATA_WIDTH-1 downto 0);
		wr2           : in std_logic
	);
	end component;

	component dp_ram_2c2rw is
		generic (
			ADDR_WIDTH : integer;
			DATA_WIDTH : integer
		);
		port (
			clk1 : in std_logic;
			clk2 : in std_logic;
			-- read/write port 1
			rw1_addr : in std_logic_vector(ADDR_WIDTH-1 downto 0);
			rw1_rd_data : out std_logic_vector(DATA_WIDTH-1 downto 0) := (others=>'0');
			rw1_rd : in std_logic;
			rw1_wr_data : in std_logic_vector(DATA_WIDTH-1 downto 0);
			rw1_wr : in std_logic;
			-- read/write port 2
			rw2_addr : in std_logic_vector(ADDR_WIDTH-1 downto 0);
			rw2_rd_data : out std_logic_vector(DATA_WIDTH-1 downto 0) := (others=>'0');
			rw2_rd : in std_logic;
			rw2_wr_data : in std_logic_vector(DATA_WIDTH-1 downto 0);
			rw2_wr : in std_logic
		);
	end component;

	component dp_ram_2c2rw_byteen is
		generic (
			ADDR_WIDTH : integer;
			DATA_WIDTH : integer
		);
		port (
			clk1 : in std_logic;
			clk2 : in std_logic;
			-- read/write port 1
			rw1_addr : in std_logic_vector(ADDR_WIDTH-1 downto 0);
			rw1_rd_data : out std_logic_vector(DATA_WIDTH-1 downto 0) := (others=>'0');
			rw1_rd : in std_logic;
			rw1_wr_data : in std_logic_vector(DATA_WIDTH-1 downto 0);
			rw1_wr_ben : in std_logic_vector(DATA_WIDTH/8-1 downto 0);
			rw1_wr : in std_logic;
			-- read/write port 2
			rw2_addr : in std_logic_vector(ADDR_WIDTH-1 downto 0);
			rw2_rd_data : out std_logic_vector(DATA_WIDTH-1 downto 0) := (others=>'0');
			rw2_rd : in std_logic;
			rw2_wr_data : in std_logic_vector(DATA_WIDTH-1 downto 0);
			rw2_wr_ben : in std_logic_vector(DATA_WIDTH/8-1 downto 0);
			rw2_wr : in std_logic
		);
	end component;
	
	component fifo_1c1r1w is
		generic (
			DEPTH : integer;
			DATA_WIDTH : integer
		);
		port (
			clk : in std_logic;
			res_n : in std_logic;
			-- read port
			rd_data : out std_logic_vector(DATA_WIDTH - 1 downto 0);
			rd : in std_logic;
			-- write port
			wr_data : in std_logic_vector(DATA_WIDTH - 1 downto 0);
			wr : in std_logic;
			-- status flags
			empty : out std_logic;
			full : out std_logic;
			half_full : out std_logic
		);
	end component;
	
	component fifo_1c1r1w_fwft is
		generic (
			DEPTH : integer;
			DATA_WIDTH : integer
		);
		port (
			clk : in std_logic;
			res_n : in std_logic;
			-- read port
			rd_data : out std_logic_vector(DATA_WIDTH - 1 downto 0);
			rd_ack : in std_logic;
			rd_valid : out std_logic;
			-- write port
			wr_data : in std_logic_vector(DATA_WIDTH - 1 downto 0);
			wr : in std_logic;
			-- status flags
			full : out std_logic;
			half_full : out std_logic
		);
	end component;

end package;

