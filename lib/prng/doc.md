
# Pseudo Random Number Generator (PRNG)

The `prng` module implements a simple pseudo random number generator using a linear feedback shift register.

## Dependencies
* None 


## Required Files
 * `prng.vhd`
 * `prng_pkg.vhd`
 * `prng_top.vho` (use **only for simulation**, not for synthesis!)
 * `prng_top.qarlog`

## Pseudo Random Number Generator (PRNG) Component Declaration

```vhdl
component prng is
	port (
		clk : in std_logic;
		res_n : in std_logic;
		load_seed : in std_logic;
		seed	 : in std_logic_vector(7 downto 0) := (others=>'0');
		en : in std_logic;
		prdata : out std_logic
	);
end component;
```

## Pseudo Random Number Generator (PRNG) Interface Description

The timing diagram below demonstrates how to interact with the PRNG.
After reset the core is internally initialized with the seed value 0x00.
Asserting `en` yields new (pseudo) random bits at `prdata`.
In this case the core puts out the stream ($`r_0,r_1,r_2,r_3`$).
The timing diagram also shows how the PRNG is reinitialized with a seed value.
Since 0x00 is used, the PRNG outputs the same sequence of random bits as before.

![PRNG Example Timing Diagram](.mdata/prng_timing.svg)
