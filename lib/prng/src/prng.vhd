library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity prng is
	port(
		clk    : in std_logic;
		res_n  : in std_logic;
		load_seed : in std_logic;
		seed   : in std_logic_vector(7 downto 0) := (others=>'0');
		en     : in std_logic;
		prdata : out std_logic
	);
end entity;

architecture arch of prng is
	constant DATA_WIDTH : integer := 24;
	constant RESET_VALUE : std_logic_vector(DATA_WIDTH-1 downto 0) := x"00a5df";
	signal lfsr : std_logic_vector(DATA_WIDTH-1 downto 0) := RESET_VALUE;
begin

	sync : process(clk, res_n)
	begin
		if res_n = '0' then
			lfsr <= RESET_VALUE; 
		elsif rising_edge(clk) then
			if (load_seed = '1') then
				lfsr   <= RESET_VALUE;
				--only seed some of the bits to ensure that the seed cannot set the LFSR to zero 
				lfsr(15 downto 8)  <= RESET_VALUE(15 downto 8) xor seed;
			elsif (en = '1') then
				lfsr(DATA_WIDTH-1 downto 1) <= lfsr(DATA_WIDTH-2 downto 0);
				-- x^24 + x^23 + x^22 + x^17 + 1
				lfsr(0) <= lfsr(23) xor lfsr(22) xor lfsr(21) xor lfsr(16);
			end if;
		end if;
	end process;
	
	prdata <= lfsr(23);
end architecture;
