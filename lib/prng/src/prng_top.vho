-- Copyright (C) 2023  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 22.1std.2 Build 922 07/20/2023 SC Standard Edition"

-- DATE "04/10/2024 14:03:12"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim (VHDL) only
-- 

LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_ASDO_DATA1~	=>  Location: PIN_F4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_FLASH_nCE_nCSO~	=>  Location: PIN_E2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DCLK~	=>  Location: PIN_P3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DATA0~	=>  Location: PIN_N7,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~padout\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~padout\ : std_logic;
SIGNAL \~ALTERA_DATA0~~padout\ : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_DATA0~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	prng_top IS
    PORT (
	clk : IN std_logic;
	res_n : IN std_logic;
	load_seed : IN std_logic;
	seed : IN std_logic_vector(7 DOWNTO 0);
	en : IN std_logic;
	prdata : OUT std_logic
	);
END prng_top;

-- Design Ports Information
-- prdata	=>  Location: PIN_C17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- load_seed	=>  Location: PIN_C19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clk	=>  Location: PIN_J1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- res_n	=>  Location: PIN_Y2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- en	=>  Location: PIN_D19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seed[7]	=>  Location: PIN_E25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seed[6]	=>  Location: PIN_G17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seed[5]	=>  Location: PIN_D18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seed[4]	=>  Location: PIN_A19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seed[3]	=>  Location: PIN_B19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seed[2]	=>  Location: PIN_E24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seed[1]	=>  Location: PIN_C20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seed[0]	=>  Location: PIN_D20,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF prng_top IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_res_n : std_logic;
SIGNAL ww_load_seed : std_logic;
SIGNAL ww_seed : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_en : std_logic;
SIGNAL ww_prdata : std_logic;
SIGNAL \res_n~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \clk~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \prdata~output_o\ : std_logic;
SIGNAL \clk~input_o\ : std_logic;
SIGNAL \clk~inputclkctrl_outclk\ : std_logic;
SIGNAL \load_seed~input_o\ : std_logic;
SIGNAL \seed[7]~input_o\ : std_logic;
SIGNAL \seed[5]~input_o\ : std_logic;
SIGNAL \prng_inst|lfsr~24_combout\ : std_logic;
SIGNAL \prng_inst|lfsr~25_combout\ : std_logic;
SIGNAL \res_n~input_o\ : std_logic;
SIGNAL \res_n~inputclkctrl_outclk\ : std_logic;
SIGNAL \en~input_o\ : std_logic;
SIGNAL \prng_inst|lfsr[22]~1_combout\ : std_logic;
SIGNAL \prng_inst|lfsr~23_combout\ : std_logic;
SIGNAL \prng_inst|lfsr~22_combout\ : std_logic;
SIGNAL \prng_inst|lfsr~21_combout\ : std_logic;
SIGNAL \prng_inst|lfsr~20_combout\ : std_logic;
SIGNAL \prng_inst|lfsr~19_combout\ : std_logic;
SIGNAL \prng_inst|lfsr~18_combout\ : std_logic;
SIGNAL \prng_inst|lfsr~17_combout\ : std_logic;
SIGNAL \seed[0]~input_o\ : std_logic;
SIGNAL \prng_inst|lfsr~16_combout\ : std_logic;
SIGNAL \seed[1]~input_o\ : std_logic;
SIGNAL \prng_inst|lfsr~15_combout\ : std_logic;
SIGNAL \seed[2]~input_o\ : std_logic;
SIGNAL \prng_inst|lfsr~14_combout\ : std_logic;
SIGNAL \seed[3]~input_o\ : std_logic;
SIGNAL \prng_inst|lfsr~13_combout\ : std_logic;
SIGNAL \seed[4]~input_o\ : std_logic;
SIGNAL \prng_inst|lfsr~12_combout\ : std_logic;
SIGNAL \prng_inst|lfsr~11_combout\ : std_logic;
SIGNAL \seed[6]~input_o\ : std_logic;
SIGNAL \prng_inst|lfsr~10_combout\ : std_logic;
SIGNAL \prng_inst|lfsr~9_combout\ : std_logic;
SIGNAL \prng_inst|lfsr~8_combout\ : std_logic;
SIGNAL \prng_inst|lfsr~7_combout\ : std_logic;
SIGNAL \prng_inst|lfsr~6_combout\ : std_logic;
SIGNAL \prng_inst|lfsr~5_combout\ : std_logic;
SIGNAL \prng_inst|lfsr~4_combout\ : std_logic;
SIGNAL \prng_inst|lfsr~3_combout\ : std_logic;
SIGNAL \prng_inst|lfsr~2_combout\ : std_logic;
SIGNAL \prng_inst|lfsr~0_combout\ : std_logic;
SIGNAL \prng_inst|lfsr\ : std_logic_vector(23 DOWNTO 0);

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

ww_clk <= clk;
ww_res_n <= res_n;
ww_load_seed <= load_seed;
ww_seed <= seed;
ww_en <= en;
prdata <= ww_prdata;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\res_n~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \res_n~input_o\);

\clk~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \clk~input_o\);
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: IOOBUF_X81_Y73_N2
\prdata~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \prng_inst|lfsr\(23),
	devoe => ww_devoe,
	o => \prdata~output_o\);

-- Location: IOIBUF_X0_Y36_N8
\clk~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk,
	o => \clk~input_o\);

-- Location: CLKCTRL_G2
\clk~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clk~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clk~inputclkctrl_outclk\);

-- Location: IOIBUF_X83_Y73_N8
\load_seed~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_load_seed,
	o => \load_seed~input_o\);

-- Location: IOIBUF_X83_Y73_N1
\seed[7]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_seed(7),
	o => \seed[7]~input_o\);

-- Location: IOIBUF_X85_Y73_N1
\seed[5]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_seed(5),
	o => \seed[5]~input_o\);

-- Location: LCCOMB_X83_Y72_N10
\prng_inst|lfsr~24\ : cycloneive_lcell_comb
-- Equation(s):
-- \prng_inst|lfsr~24_combout\ = \prng_inst|lfsr\(21) $ (\prng_inst|lfsr\(23))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \prng_inst|lfsr\(21),
	datad => \prng_inst|lfsr\(23),
	combout => \prng_inst|lfsr~24_combout\);

-- Location: LCCOMB_X83_Y72_N8
\prng_inst|lfsr~25\ : cycloneive_lcell_comb
-- Equation(s):
-- \prng_inst|lfsr~25_combout\ = (!\load_seed~input_o\ & (\prng_inst|lfsr\(22) $ (\prng_inst|lfsr~24_combout\ $ (!\prng_inst|lfsr\(16)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011000001001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \prng_inst|lfsr\(22),
	datab => \prng_inst|lfsr~24_combout\,
	datac => \load_seed~input_o\,
	datad => \prng_inst|lfsr\(16),
	combout => \prng_inst|lfsr~25_combout\);

-- Location: IOIBUF_X0_Y36_N15
\res_n~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_res_n,
	o => \res_n~input_o\);

-- Location: CLKCTRL_G4
\res_n~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \res_n~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \res_n~inputclkctrl_outclk\);

-- Location: IOIBUF_X83_Y73_N15
\en~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_en,
	o => \en~input_o\);

-- Location: LCCOMB_X83_Y72_N4
\prng_inst|lfsr[22]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \prng_inst|lfsr[22]~1_combout\ = (\load_seed~input_o\) # (\en~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \load_seed~input_o\,
	datad => \en~input_o\,
	combout => \prng_inst|lfsr[22]~1_combout\);

-- Location: FF_X83_Y72_N9
\prng_inst|lfsr[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \prng_inst|lfsr~25_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \prng_inst|lfsr[22]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \prng_inst|lfsr\(0));

-- Location: LCCOMB_X84_Y72_N6
\prng_inst|lfsr~23\ : cycloneive_lcell_comb
-- Equation(s):
-- \prng_inst|lfsr~23_combout\ = (\prng_inst|lfsr\(0) & !\load_seed~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \prng_inst|lfsr\(0),
	datac => \load_seed~input_o\,
	combout => \prng_inst|lfsr~23_combout\);

-- Location: FF_X84_Y72_N7
\prng_inst|lfsr[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \prng_inst|lfsr~23_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \prng_inst|lfsr[22]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \prng_inst|lfsr\(1));

-- Location: LCCOMB_X84_Y72_N20
\prng_inst|lfsr~22\ : cycloneive_lcell_comb
-- Equation(s):
-- \prng_inst|lfsr~22_combout\ = (!\load_seed~input_o\ & \prng_inst|lfsr\(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \load_seed~input_o\,
	datad => \prng_inst|lfsr\(1),
	combout => \prng_inst|lfsr~22_combout\);

-- Location: FF_X84_Y72_N21
\prng_inst|lfsr[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \prng_inst|lfsr~22_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \prng_inst|lfsr[22]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \prng_inst|lfsr\(2));

-- Location: LCCOMB_X84_Y72_N10
\prng_inst|lfsr~21\ : cycloneive_lcell_comb
-- Equation(s):
-- \prng_inst|lfsr~21_combout\ = (!\load_seed~input_o\ & \prng_inst|lfsr\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \load_seed~input_o\,
	datad => \prng_inst|lfsr\(2),
	combout => \prng_inst|lfsr~21_combout\);

-- Location: FF_X84_Y72_N11
\prng_inst|lfsr[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \prng_inst|lfsr~21_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \prng_inst|lfsr[22]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \prng_inst|lfsr\(3));

-- Location: LCCOMB_X84_Y72_N0
\prng_inst|lfsr~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \prng_inst|lfsr~20_combout\ = (!\load_seed~input_o\ & \prng_inst|lfsr\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \load_seed~input_o\,
	datad => \prng_inst|lfsr\(3),
	combout => \prng_inst|lfsr~20_combout\);

-- Location: FF_X84_Y72_N1
\prng_inst|lfsr[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \prng_inst|lfsr~20_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \prng_inst|lfsr[22]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \prng_inst|lfsr\(4));

-- Location: LCCOMB_X84_Y72_N22
\prng_inst|lfsr~19\ : cycloneive_lcell_comb
-- Equation(s):
-- \prng_inst|lfsr~19_combout\ = (!\load_seed~input_o\ & !\prng_inst|lfsr\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \load_seed~input_o\,
	datad => \prng_inst|lfsr\(4),
	combout => \prng_inst|lfsr~19_combout\);

-- Location: FF_X84_Y72_N23
\prng_inst|lfsr[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \prng_inst|lfsr~19_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \prng_inst|lfsr[22]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \prng_inst|lfsr\(5));

-- Location: LCCOMB_X84_Y72_N28
\prng_inst|lfsr~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \prng_inst|lfsr~18_combout\ = (!\prng_inst|lfsr\(5) & !\load_seed~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010100000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \prng_inst|lfsr\(5),
	datac => \load_seed~input_o\,
	combout => \prng_inst|lfsr~18_combout\);

-- Location: FF_X84_Y72_N29
\prng_inst|lfsr[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \prng_inst|lfsr~18_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \prng_inst|lfsr[22]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \prng_inst|lfsr\(6));

-- Location: LCCOMB_X84_Y72_N2
\prng_inst|lfsr~17\ : cycloneive_lcell_comb
-- Equation(s):
-- \prng_inst|lfsr~17_combout\ = (!\load_seed~input_o\ & \prng_inst|lfsr\(6))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \load_seed~input_o\,
	datad => \prng_inst|lfsr\(6),
	combout => \prng_inst|lfsr~17_combout\);

-- Location: FF_X84_Y72_N3
\prng_inst|lfsr[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \prng_inst|lfsr~17_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \prng_inst|lfsr[22]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \prng_inst|lfsr\(7));

-- Location: IOIBUF_X85_Y73_N15
\seed[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_seed(0),
	o => \seed[0]~input_o\);

-- Location: LCCOMB_X84_Y72_N8
\prng_inst|lfsr~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \prng_inst|lfsr~16_combout\ = (\load_seed~input_o\ & ((\seed[0]~input_o\))) # (!\load_seed~input_o\ & (\prng_inst|lfsr\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \prng_inst|lfsr\(7),
	datac => \load_seed~input_o\,
	datad => \seed[0]~input_o\,
	combout => \prng_inst|lfsr~16_combout\);

-- Location: FF_X84_Y72_N9
\prng_inst|lfsr[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \prng_inst|lfsr~16_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \prng_inst|lfsr[22]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \prng_inst|lfsr\(8));

-- Location: IOIBUF_X85_Y73_N8
\seed[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_seed(1),
	o => \seed[1]~input_o\);

-- Location: LCCOMB_X84_Y72_N14
\prng_inst|lfsr~15\ : cycloneive_lcell_comb
-- Equation(s):
-- \prng_inst|lfsr~15_combout\ = (\load_seed~input_o\ & ((\seed[1]~input_o\))) # (!\load_seed~input_o\ & (!\prng_inst|lfsr\(8)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000110110001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \load_seed~input_o\,
	datab => \prng_inst|lfsr\(8),
	datac => \seed[1]~input_o\,
	combout => \prng_inst|lfsr~15_combout\);

-- Location: FF_X84_Y72_N15
\prng_inst|lfsr[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \prng_inst|lfsr~15_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \prng_inst|lfsr[22]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \prng_inst|lfsr\(9));

-- Location: IOIBUF_X85_Y73_N22
\seed[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_seed(2),
	o => \seed[2]~input_o\);

-- Location: LCCOMB_X84_Y72_N4
\prng_inst|lfsr~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \prng_inst|lfsr~14_combout\ = (\load_seed~input_o\ & ((\seed[2]~input_o\))) # (!\load_seed~input_o\ & (!\prng_inst|lfsr\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001100000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \prng_inst|lfsr\(9),
	datac => \load_seed~input_o\,
	datad => \seed[2]~input_o\,
	combout => \prng_inst|lfsr~14_combout\);

-- Location: FF_X84_Y72_N5
\prng_inst|lfsr[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \prng_inst|lfsr~14_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \prng_inst|lfsr[22]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \prng_inst|lfsr\(10));

-- Location: IOIBUF_X81_Y73_N22
\seed[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_seed(3),
	o => \seed[3]~input_o\);

-- Location: LCCOMB_X84_Y72_N26
\prng_inst|lfsr~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \prng_inst|lfsr~13_combout\ = (\load_seed~input_o\ & ((\seed[3]~input_o\))) # (!\load_seed~input_o\ & (!\prng_inst|lfsr\(10)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000110110001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \load_seed~input_o\,
	datab => \prng_inst|lfsr\(10),
	datac => \seed[3]~input_o\,
	combout => \prng_inst|lfsr~13_combout\);

-- Location: FF_X84_Y72_N27
\prng_inst|lfsr[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \prng_inst|lfsr~13_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \prng_inst|lfsr[22]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \prng_inst|lfsr\(11));

-- Location: IOIBUF_X81_Y73_N15
\seed[4]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_seed(4),
	o => \seed[4]~input_o\);

-- Location: LCCOMB_X84_Y72_N16
\prng_inst|lfsr~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \prng_inst|lfsr~12_combout\ = (\load_seed~input_o\ & ((\seed[4]~input_o\))) # (!\load_seed~input_o\ & (\prng_inst|lfsr\(11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \prng_inst|lfsr\(11),
	datab => \seed[4]~input_o\,
	datac => \load_seed~input_o\,
	combout => \prng_inst|lfsr~12_combout\);

-- Location: FF_X84_Y72_N17
\prng_inst|lfsr[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \prng_inst|lfsr~12_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \prng_inst|lfsr[22]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \prng_inst|lfsr\(12));

-- Location: LCCOMB_X84_Y72_N30
\prng_inst|lfsr~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \prng_inst|lfsr~11_combout\ = (\load_seed~input_o\ & (\seed[5]~input_o\)) # (!\load_seed~input_o\ & ((!\prng_inst|lfsr\(12))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001110100011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \seed[5]~input_o\,
	datab => \prng_inst|lfsr\(12),
	datac => \load_seed~input_o\,
	combout => \prng_inst|lfsr~11_combout\);

-- Location: FF_X84_Y72_N31
\prng_inst|lfsr[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \prng_inst|lfsr~11_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \prng_inst|lfsr[22]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \prng_inst|lfsr\(13));

-- Location: IOIBUF_X83_Y73_N22
\seed[6]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_seed(6),
	o => \seed[6]~input_o\);

-- Location: LCCOMB_X84_Y72_N12
\prng_inst|lfsr~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \prng_inst|lfsr~10_combout\ = (\load_seed~input_o\ & ((\seed[6]~input_o\))) # (!\load_seed~input_o\ & (!\prng_inst|lfsr\(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010111000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \prng_inst|lfsr\(13),
	datab => \seed[6]~input_o\,
	datac => \load_seed~input_o\,
	combout => \prng_inst|lfsr~10_combout\);

-- Location: FF_X84_Y72_N13
\prng_inst|lfsr[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \prng_inst|lfsr~10_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \prng_inst|lfsr[22]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \prng_inst|lfsr\(14));

-- Location: LCCOMB_X84_Y72_N18
\prng_inst|lfsr~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \prng_inst|lfsr~9_combout\ = (\load_seed~input_o\ & (\seed[7]~input_o\)) # (!\load_seed~input_o\ & ((!\prng_inst|lfsr\(14))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011110101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \load_seed~input_o\,
	datac => \seed[7]~input_o\,
	datad => \prng_inst|lfsr\(14),
	combout => \prng_inst|lfsr~9_combout\);

-- Location: FF_X84_Y72_N19
\prng_inst|lfsr[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \prng_inst|lfsr~9_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \prng_inst|lfsr[22]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \prng_inst|lfsr\(15));

-- Location: LCCOMB_X84_Y72_N24
\prng_inst|lfsr~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \prng_inst|lfsr~8_combout\ = (!\load_seed~input_o\ & !\prng_inst|lfsr\(15))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \load_seed~input_o\,
	datad => \prng_inst|lfsr\(15),
	combout => \prng_inst|lfsr~8_combout\);

-- Location: FF_X84_Y72_N25
\prng_inst|lfsr[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \prng_inst|lfsr~8_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \prng_inst|lfsr[22]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \prng_inst|lfsr\(16));

-- Location: LCCOMB_X83_Y72_N6
\prng_inst|lfsr~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \prng_inst|lfsr~7_combout\ = (!\load_seed~input_o\ & \prng_inst|lfsr\(16))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \load_seed~input_o\,
	datad => \prng_inst|lfsr\(16),
	combout => \prng_inst|lfsr~7_combout\);

-- Location: FF_X83_Y72_N7
\prng_inst|lfsr[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \prng_inst|lfsr~7_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \prng_inst|lfsr[22]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \prng_inst|lfsr\(17));

-- Location: LCCOMB_X83_Y72_N12
\prng_inst|lfsr~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \prng_inst|lfsr~6_combout\ = (\prng_inst|lfsr\(17) & !\load_seed~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \prng_inst|lfsr\(17),
	datac => \load_seed~input_o\,
	combout => \prng_inst|lfsr~6_combout\);

-- Location: FF_X83_Y72_N13
\prng_inst|lfsr[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \prng_inst|lfsr~6_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \prng_inst|lfsr[22]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \prng_inst|lfsr\(18));

-- Location: LCCOMB_X83_Y72_N2
\prng_inst|lfsr~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \prng_inst|lfsr~5_combout\ = (\prng_inst|lfsr\(18) & !\load_seed~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \prng_inst|lfsr\(18),
	datac => \load_seed~input_o\,
	combout => \prng_inst|lfsr~5_combout\);

-- Location: FF_X83_Y72_N3
\prng_inst|lfsr[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \prng_inst|lfsr~5_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \prng_inst|lfsr[22]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \prng_inst|lfsr\(19));

-- Location: LCCOMB_X83_Y72_N24
\prng_inst|lfsr~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \prng_inst|lfsr~4_combout\ = (\prng_inst|lfsr\(19) & !\load_seed~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \prng_inst|lfsr\(19),
	datac => \load_seed~input_o\,
	combout => \prng_inst|lfsr~4_combout\);

-- Location: FF_X83_Y72_N25
\prng_inst|lfsr[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \prng_inst|lfsr~4_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \prng_inst|lfsr[22]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \prng_inst|lfsr\(20));

-- Location: LCCOMB_X83_Y72_N22
\prng_inst|lfsr~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \prng_inst|lfsr~3_combout\ = (\prng_inst|lfsr\(20) & !\load_seed~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \prng_inst|lfsr\(20),
	datac => \load_seed~input_o\,
	combout => \prng_inst|lfsr~3_combout\);

-- Location: FF_X83_Y72_N23
\prng_inst|lfsr[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \prng_inst|lfsr~3_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \prng_inst|lfsr[22]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \prng_inst|lfsr\(21));

-- Location: LCCOMB_X83_Y72_N26
\prng_inst|lfsr~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \prng_inst|lfsr~2_combout\ = (!\load_seed~input_o\ & \prng_inst|lfsr\(21))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \load_seed~input_o\,
	datac => \prng_inst|lfsr\(21),
	combout => \prng_inst|lfsr~2_combout\);

-- Location: FF_X83_Y72_N27
\prng_inst|lfsr[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \prng_inst|lfsr~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \prng_inst|lfsr[22]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \prng_inst|lfsr\(22));

-- Location: LCCOMB_X83_Y72_N0
\prng_inst|lfsr~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \prng_inst|lfsr~0_combout\ = (!\load_seed~input_o\ & \prng_inst|lfsr\(22))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \load_seed~input_o\,
	datac => \prng_inst|lfsr\(22),
	combout => \prng_inst|lfsr~0_combout\);

-- Location: FF_X83_Y72_N1
\prng_inst|lfsr[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \prng_inst|lfsr~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \prng_inst|lfsr[22]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \prng_inst|lfsr\(23));

ww_prdata <= \prdata~output_o\;
END structure;


