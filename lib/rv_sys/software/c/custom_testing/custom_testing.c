#include "util.h"

void putd(int d)
{
    // print as decimal
    char buf[12];
    int i = 0;
    if (d == 0)
    {
        putchar('0');
        return;
    }
    if (d < 0)
    {
        putchar('-');
        d = -d;
    }
    while (d > 0)
    {
        buf[i++] = d % 10 + '0';
        d /= 10;
    }
    while (i > 0)
    {
        putchar(buf[--i]);
    }
    putchar('\n');
}

int test_lui()
{
    register unsigned result, expected = 0x1000;
    // inline assembly with a as and result as rd
    __asm__ __volatile__("lui %0, 0x1" : "=r"(result));
    if (expected != result)
    {
        puts("LUI test failed!");
        return 1;
    }
    return 0;
}

int test_alu_imm()
{
    register int a = 0x1, result,
                 expected_add = 0x4, expected_slti = 0x0, expected_sltiu = 0x1,
                 expected_xori = 0x2, expected_ori = 0x3, expected_andi = 0x1,
                 expected_slli = 0x2, expected_srli = 0x0, expected_srai = 0x0;
    // immediate: 3
    __asm__ __volatile__("addi %0, %1, 3" : "=r"(result) : "r"(a));
    if (expected_add != result)
    {
        puts("addi failed");
        return 1;
    }
    __asm__ __volatile__("slti %0, %1, -3" : "=r"(result) : "r"(a));
    if (expected_slti != result)
    {
        puts("slti failed");
        return 1;
    }
    __asm__ __volatile__("sltiu %0, %1, -3" : "=r"(result) : "r"(a));
    if (expected_sltiu != result)
    {
        puts("sltiu failed");
        return 1;
    }
    __asm__ __volatile__("xori %0, %1, 3" : "=r"(result) : "r"(a));
    if (expected_xori != result)
    {
        puts("xori failed");
        return 1;
    }
    __asm__ __volatile__("ori %0, %1, 3" : "=r"(result) : "r"(a));
    if (expected_ori != result)
    {
        puts("ori failed");
        return 1;
    }
    __asm__ __volatile__("andi %0, %1, 3" : "=r"(result) : "r"(a));
    if (expected_andi != result)
    {
        puts("andi failed");
        return 1;
    }
    __asm__ __volatile__("slli %0, %1, 1" : "=r"(result) : "r"(a));
    if (expected_slli != result)
    {
        puts("slli failed");
        return 1;
    }
    __asm__ __volatile__("srli %0, %1, 1" : "=r"(result) : "r"(a));
    if (expected_srli != result)
    {
        puts("srli failed");
        return 1;
    }
    __asm__ __volatile__("srai %0, %1, 1" : "=r"(result) : "r"(a));
    if (expected_srai != result)
    {
        puts("srai failed");
        return 1;
    }
    return 0;
}

int test_alu()
{
    register int a = 0x1, b_neg = -3, b = 3, result,
                 expected_add = -0x2, expected_sub = 4, expected_slt = 0x0, expected_sltu = 0x1,
                 expected_xor = 0x2, expected_or = 0x3, expected_and = 0x1,
                 expected_sll = 0x8, expected_srl = 0x0, expected_sra = 0x0;

    __asm__ __volatile__("add %0, %1, %2" : "=r"(result) : "r"(a), "r"(b_neg));
    if (expected_add != result)
    {
        puts("add failed");
        return 1;
    }
    __asm__ __volatile__("sub %0, %1, %2" : "=r"(result) : "r"(a), "r"(b_neg));
    if (expected_sub != result)
    {
        puts("sub failed");
        return 1;
    }
    __asm__ __volatile__("slt %0, %1, %2" : "=r"(result) : "r"(a), "r"(b_neg));
    if (expected_slt != result)
    {
        puts("slt failed");
        return 1;
    }
    __asm__ __volatile__("sltu %0, %1, %2" : "=r"(result) : "r"(a), "r"(b_neg));
    if (expected_sltu != result)
    {
        puts("sltu failed");
        return 1;
    }
    __asm__ __volatile__("xor %0, %1, %2" : "=r"(result) : "r"(a), "r"(b));
    if (expected_xor != result)
    {
        puts("xor failed");
        return 1;
    }
    __asm__ __volatile__("or %0, %1, %2" : "=r"(result) : "r"(a), "r"(b));
    if (expected_or != result)
    {
        puts("or failed");
        return 1;
    }
    __asm__ __volatile__("and %0, %1, %2" : "=r"(result) : "r"(a), "r"(b));
    if (expected_and != result)
    {
        puts("and failed");
        return 1;
    }
    __asm__ __volatile__("sll %0, %1, %2" : "=r"(result) : "r"(a), "r"(b));
    if (expected_sll != result)
    {
        puts("sll failed");
        return 1;
    }
    __asm__ __volatile__("srl %0, %1, %2" : "=r"(result) : "r"(a), "r"(b));
    if (expected_srl != result)
    {
        puts("srl failed");
        return 1;
    }
    __asm__ __volatile__("sra %0, %1, %2" : "=r"(result) : "r"(a), "r"(b));
    if (expected_sra != result)
    {
        puts("sra failed");
        return 1;
    }
    return 0;
}

int test_store_load()
{
    register int aw = -1000000, result;
    int b;
    register int *ptr = &b;
    __asm__ __volatile__("sw %0, 0(%1)" : : "r"(aw), "r"(ptr));
    __asm__ __volatile__("lw %0, 0(%1)" : "=r"(result) : "r"(ptr));
    if (aw != result)
    {
        puts("sw/lw failed");
        return 1;
    }

    register int ah = -1000, result_h;
    __asm__ __volatile__("sh %0, 0(%1)" : : "r"(ah), "r"(ptr));
    __asm__ __volatile__("lh %0, 0(%1)" : "=r"(result_h) : "r"(ptr));
    if (ah != result_h)
    {
        puts("sh/lh failed");
        return 1;
    }
    __asm__ __volatile__("sh %0, 2(%1)" : : "r"(ah), "r"(ptr));
    __asm__ __volatile__("lhu %0, 2(%1)" : "=r"(result_h) : "r"(ptr));
    if ((ah & 0xFFFF) != result_h)
    {
        puts("shu/lhu failed");
        return 1;
    }

    register int al = -100, result_b;
    __asm__ __volatile__("sb %0, 0(%1)" : : "r"(al), "r"(ptr));
    __asm__ __volatile__("lb %0, 0(%1)" : "=r"(result_b) : "r"(ptr));
    if (al != result_b)
    {
        puts("sb/lb failed");
        return 1;
    }

    __asm__ __volatile__("sb %0, 1(%1)" : : "r"(al), "r"(ptr));
    __asm__ __volatile__("lbu %0, 1(%1)" : "=r"(result_b) : "r"(ptr));
    if ((al & 0xFF) != result_b)
    {
        puts("sbu/lbu failed");
        return 1;
    }

    register int al2 = 100;
    __asm__ __volatile__("sb %0, 2(%1)" : : "r"(al2), "r"(ptr));
    __asm__ __volatile__("lb %0, 2(%1)" : "=r"(result_b) : "r"(ptr));
    if (al2 != result_b)
    {
        puts("sb/lb2 failed");
        return 1;
    }

    __asm__ __volatile__("sb %0, 3(%1)" : : "r"(al2), "r"(ptr));
    __asm__ __volatile__("lbu %0, 3(%1)" : "=r"(result_b) : "r"(ptr));
    if ((al2 & 0xFF) != result_b)
    {
        puts("sbu/lbu2 failed");
        return 1;
    }

    return 0;
}

void __attribute__((optimize("O0"))) test_branch(unsigned char data1, unsigned char data2) {
    if (data1 == data2) {
        puts("eq");
    }

    if (data1 != data2) {
        puts("neq");
    }
    
    if (data1 < data2) {
        puts("lt");
    }
    
    if (data1 >= data2) {
        puts("geq");
    }
}

int main()
{

    // puts("Hello RISC-V!");
	putchar(' ');

    test_branch(1, 1);
    // test_branch(2, 1);

    int res = test_lui() |
              test_alu_imm() |
              test_alu() |
        	  test_store_load();

    if (res == 0)
    {
        puts("All tests passed!");
    	// puts("Bye RISC-V!");
    }

    return res;
}
