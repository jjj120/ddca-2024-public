
# Snake Controller
The `snake_ctrl` module can be used to track and control the movement of the snake stored in a memory (e.g. inside the `snake_game_field`).
## Dependencies
* Graphics command package (`gfx_cmd_pkg`) 
* Mathematical Support Package (`math_pkg`) 


## Required Files
 * `snake_ctrl_pkg.vhd`
 * `snake_ctrl_top.qarlog`
 * `snake_ctrl_top.vho` (use **only for simulation**, not for synthesis!)
 * `snake_ctrl.vhd`

## Snake Controller Component Declaration

```vhdl
component snake_ctrl is
	port (
		clk : in std_logic;
		res_n : in std_logic;
		busy : in std_logic;
		rd : out std_logic;
		rd_pos : out vec2d_t;
		rd_data : in std_logic_vector(SNAKE_FIELD_DATA_WIDTH-1 downto 0);
		wr : out std_logic;
		wr_pos : out vec2d_t;
		wr_data : out std_logic_vector(SNAKE_FIELD_DATA_WIDTH-1 downto 0);
		player : in player_t;
		init : in std_logic;
		init_head_position  : in vec2d_t;
		init_head_direction : in direction_t;
		init_body_length : in std_logic_vector(3 downto 0);
		move_head : in std_logic;
		movement_direction : in direction_t;
		move_tail : in std_logic;
		done : out std_logic
	);
end component;
```

## Snake Controller Interface Description
The module features three operations:
- Initialization: This operation writes a snake into the provided memory.
The initialization is started by asserting the `init` signal (for exactly one clock cycle) while providing the initial position of the snake's head (`init_head_position`), the initial direction of the snake's head (`init_head_direction`) and the initial length of the snake's body (`init_body_length`).
Note that the head and tail do **not** count to the body length.
- Head movement: This operation, started by asserting `move_head` for exactly one clock cycle, moves the snake's head in the direction set via `movement_direction`.
The old head entry is placed by the correct snake body entry.
The following figure shows an example, where the head moves upward and is replaced by a body entry with the correct bend.
![Snake Head Movement](.mdata/move_head.png)
- Tail movement: This operation, started by asserting `move_tail` for exactly one clock cycle, advances the snake's tail by one entry in the correct direction.

The `player` input signal tells the `snake_ctrl` module for which snake it is supposed to perform the desired operation.

Note that each of the module's three operations is started by asserting the respective signal for exactly one cycle.
The required input parameters for the operation must be applied in the same clock cycle.
The module will then perform the respective operation and indicate a complete operation by asserting `done` for exactly one cycle.

The picture below shows an example timing diagram of the initialization of the snake.
Note that the initialization (as well as the other operations) can take a variable number of cycles.

A similar timing also applies for the movement operations of the snake's head and tail.

![Snake Controller Interface Timing](.mdata/snake_ctrl_timing.svg)

Note that the `snake_ctrl` module is supposed to be connected to a memory that contains the snake game's state.
The `busy` signal tells the `snake_ctrl` module whether the memory is currently being accessed by some other module (`busy` asserted), or if the `snake_ctrl` module can access it without causing interference (`busy` low).
The `snake_ctrl` module will **never** perform any kind of memory access while `busy` is asserted.

The `rd` signal is asserted when the `snake_ctrl` module wants to read the entry at the game field position `rd_pos` (set in the same clock cycle as `rd`).
The `snake_ctrl` module expects the result of this read operation at the next clock cycle at `rd_data`.

The `snake_ctrl` module asserts `wr` and sets `wr_pos` and `wr_data` for exactly one clock cycle when it wants to write `wr_data` to the entry at the game field position `wr_pos`.

## Useful Constants, Types and Functions

The [snake_ctrl_pkg](./src/snake_ctrl_pkg.vhd) provides some constants, types and functions that make code interfacing with the `snake_ctrl` more read- and maintainable.

The type `entry_t` is a convenience subtype of `std_logic_vector` that can be used for entries of the game field.
Such entries can be created using the `create_game_field_entry` function, which expects a game field entry type and, optionally, a rotation (for the snake bitmaps) and a player.
For the type of a game field entry, there is the `entry_type_t` type.
There are constants for the different types of game field entries (`BLANK_ENTRY`, `SNAKE_*` and `FOOD_*`).
The `get_entry_type`, `get_rotation` and `get_player` functions allow to extract the respective informatio of a game field entry.
The `get_bmp_offset` function is useful for drawing a game field, as it returns the offset in pixels in the bitmap containing the game characters for a game field entry.

The `player_t` enumeration type contains two values (`PLAYER1`, `PLAYER2`) and can be used for distinguishing between the two players of the snake game.
The `not` operator is defined for this type in the obvious way.
Furthermore, there are two convenience conversion functions, `player_to_int` and `player_to_sl`, which convert a player value into an integer, respectively `std_logic`, value (`PLAYER1` to `0` / `'0'` and `PLAYER2` to `1` / `'1'`).

The `direction_t` enumeration type is used for directional information.
It contains the four values `UP`, `DOWN`, `LEFT` and `RIGHT`.
The `not` operator is defined such that it yields the opposite direction.

The `vec2d_t` record type is a tuple of an `x` and `y` coordinate on the grid of a snake game field.
The `created_vec2d` function allows creating a value of this type given an x and y grid-coordinate as integers.
The `move_vec2d` function expects a `vec2d_t` and `direction_t` parameter and changes the `vec2d`'s coordinates by 1 in the given direction.
The `move_vec2d_and_wrap` does the same as `move_vec2d`, except that it also takes an `x_max` and `y_max` parameter and wraps the coordinates resulting from `move_vec2d` such that they are within 0 and the specified maximum values.
Note that a `NULL_VEC` constant is defined for the (0,0) coordinate pair.
