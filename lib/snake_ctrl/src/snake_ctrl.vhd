
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.snake_ctrl_pkg.all;
use work.gfx_cmd_pkg.all;

entity snake_ctrl is
	port (
		clk : in std_logic;
		res_n : in std_logic;
		busy : in std_logic;
		rd      : out std_logic;
		rd_pos  : out vec2d_t;
		rd_data : in std_logic_vector(SNAKE_FIELD_DATA_WIDTH-1 downto 0);
		wr      : out std_logic;
		wr_pos  : out vec2d_t;
		wr_data : out std_logic_vector(SNAKE_FIELD_DATA_WIDTH-1 downto 0);
		player : in player_t;
		init : in std_logic;
		init_head_position : in vec2d_t;
		init_head_direction : in direction_t;
		init_body_length : in std_logic_vector(3 downto 0);
		move_head : in std_logic;
		movement_direction : in direction_t;
		move_tail : in std_logic;
		done :  out std_logic
	); 
end entity;

architecture arch of snake_ctrl is

	type state_t is ( 
		IDLE,
		SET_DONE,
		-- snake initialization states
		PLACE_HEAD, PLACE_BODY, PLACE_TAIL,
		-- move head states
		REPLACE_OLD_HEAD, DRAW_NEW_HEAD,
		-- move tail states
		DELETE_OLD_TAIL, GET_OLD_TAIL_TYPE, WAIT_OLD_TAIL_TYPE,
		CALCULATE_NEW_TAIL_DIRECTION, DRAW_NEW_TAIL
	);
	
	type snake_position_array_t is array(integer range<>) of snake_position_t;

	signal state : state_t;
	signal cur_position : vec2d_t;
	signal cur_length : std_logic_vector(3 downto 0);
	signal cur_player : player_t;
	signal target_init_body_length : std_logic_vector(3 downto 0);
	signal target_movement_direction : direction_t;
	signal snake_pos : snake_position_array_t(1 downto 0);
begin

	sync : process(clk, res_n)
	begin
		if (res_n = '0') then
			state <= IDLE;
			cur_position <= NULL_VEC;
			cur_length <= (others=>'0');
			cur_player <= PLAYER1;
			target_init_body_length <= (others=>'0');
			target_movement_direction <= UP;
			snake_pos <= (others => NULL_SNAKE_POSITION);
			rd <= '0';
			rd_pos <= NULL_VEC;
			wr <= '0';
			wr_data <= (others => '0');
			wr_pos <= NULL_VEC;
			done <= '0';
		elsif (rising_edge(clk)) then
			rd <= '0';
			rd_pos <= NULL_VEC;
			wr <= '0';
			wr_data <= (others => '0');
			wr_pos <= NULL_VEC;
			done <= '0';
			
			case state is
				when IDLE => 
					cur_player <= player;
					if (init = '1') then
						state <= PLACE_HEAD;
						cur_length <= (others=>'0');
						cur_position <= init_head_position;
						target_init_body_length <= init_body_length;
						snake_pos(player_to_int(player)).head_direction <= init_head_direction;
						snake_pos(player_to_int(player)).tail_direction <= init_head_direction;
						snake_pos(player_to_int(player)).head_position <= init_head_position;
					elsif (move_head = '1') then
						state <= REPLACE_OLD_HEAD;
						target_movement_direction <= movement_direction;
					elsif (move_tail = '1') then
						state <= DELETE_OLD_TAIL;
					end if;
					
				------------------------------------------------
				--            SNAKE INITIALIZATION            --
				------------------------------------------------
				when PLACE_HEAD =>
					if not busy then
						wr <= '1';
						wr_pos <= cur_position;
						wr_data <= create_game_field_entry(
							entry_type => SNAKE_HEAD,
							rot => get_head_rotation_from_direction(snake_pos(player_to_int(cur_player)).head_direction),
							player => cur_player
						);
						cur_position <= move_vec2d(cur_position, not snake_pos(player_to_int(cur_player)).head_direction);
						state <= PLACE_BODY;
					end if;
				
				when PLACE_BODY =>
					if(unsigned(cur_length) = unsigned(target_init_body_length)) then
						state <= PLACE_TAIL;
					else
						if not busy then
							wr <= '1';
							wr_pos <= cur_position;
							if (snake_pos(player_to_int(cur_player)).head_direction = UP or snake_pos(player_to_int(cur_player)).head_direction = DOWN) then
								wr_data <= create_game_field_entry(
									entry_type => SNAKE_STRAIGHT,
									rot => ROT_R0,
									player => cur_player
								);
							else
								wr_data <= create_game_field_entry(
									entry_type => SNAKE_STRAIGHT,
									rot => ROT_R90,
									player => cur_player
								);
							end if;
							cur_length <= std_logic_vector(unsigned(cur_length) + 1);
							cur_position <= move_vec2d(cur_position, not snake_pos(player_to_int(cur_player)).head_direction);
							state <= PLACE_BODY;
						end if;
					end if;
				
				when PLACE_TAIL => 
					if not busy then
						wr <= '1';
						wr_pos <= cur_position;
						wr_data <= create_game_field_entry(
							entry_type => SNAKE_TAIL,
							rot => get_tail_rotation_from_direction(snake_pos(player_to_int(cur_player)).head_direction),
							player => cur_player
						);
						snake_pos(player_to_int(cur_player)).tail_position <= cur_position;
						state <= SET_DONE;
					end if;
				
				------------------------------------------------
				--              HEAD MOVEMENT                 --
				------------------------------------------------
				when REPLACE_OLD_HEAD =>
					if not busy then
						wr <= '1';
						wr_pos <= snake_pos(player_to_int(cur_player)).head_position;
						wr_data <= get_entry_for_head_replacement(
							old_dir => snake_pos(player_to_int(cur_player)).head_direction,
							new_dir => target_movement_direction,
							player => cur_player
						);
						--move head to new position, skip last row as it is used for the score text
						snake_pos(player_to_int(cur_player)).head_position <= move_vec2d_and_wrap(
							snake_pos(player_to_int(cur_player)).head_position,
							target_movement_direction,
							SNAKE_FIELD_WIDTH-1, SNAKE_FIELD_HEIGHT-2
						);
						snake_pos(player_to_int(cur_player)).head_direction <= target_movement_direction;
						state <= DRAW_NEW_HEAD;
					end if;
			
				when DRAW_NEW_HEAD =>
					if not busy then
						wr <= '1';
						wr_pos <= snake_pos(player_to_int(cur_player)).head_position;
						wr_data <= create_game_field_entry(
							entry_type => SNAKE_HEAD,
							rot => get_head_rotation_from_direction(snake_pos(player_to_int(cur_player)).head_direction),
							player => cur_player
						);
						state <= SET_DONE;
					end if;
				
				------------------------------------------------
				--              TAIL MOVEMENT                 --
				------------------------------------------------
				when DELETE_OLD_TAIL => 
					if not busy then
						wr <= '1';
						wr_pos <= snake_pos(player_to_int(cur_player)).tail_position;
						wr_data <= create_game_field_entry(
							entry_type => BLANK_ENTRY,
							rot => ROT_R0,
							player => cur_player
						);
						--calculate new tail position, again ignore the last row
						snake_pos(player_to_int(cur_player)).tail_position <= move_vec2d_and_wrap(
							snake_pos(player_to_int(cur_player)).tail_position, 
							snake_pos(player_to_int(cur_player)).tail_direction, 
							SNAKE_FIELD_WIDTH-1,
							SNAKE_FIELD_HEIGHT-2
						);
						state <= GET_OLD_TAIL_TYPE;
					end if;
					
				when GET_OLD_TAIL_TYPE =>
					if not busy then
						rd <= '1';
						rd_pos <= snake_pos(player_to_int(cur_player)).tail_position;
						state <= WAIT_OLD_TAIL_TYPE;
					end if;

				when WAIT_OLD_TAIL_TYPE =>
					state <= CALCULATE_NEW_TAIL_DIRECTION;
				
				when CALCULATE_NEW_TAIL_DIRECTION =>
					snake_pos(player_to_int(cur_player)).tail_direction <= get_new_tail_direction(snake_pos(player_to_int(cur_player)).tail_direction, rd_data);
					state <= DRAW_NEW_TAIL;
				
				when DRAW_NEW_TAIL => 
					if not busy then
						wr <= '1';
						wr_pos <= snake_pos(player_to_int(cur_player)).tail_position;
						wr_data <= create_game_field_entry(
							entry_type => SNAKE_TAIL,
							rot => get_tail_rotation_from_direction(snake_pos(player_to_int(cur_player)).tail_direction),
							player => cur_player
						);
						state <= SET_DONE;
					end if;

				------------------------------------------------
				--                    DONE                    --
				------------------------------------------------
				when SET_DONE => 
					done <= '1';
					state <= IDLE;
			
				when others =>
					null;
			
			end case;
		end if;
	end process;

end architecture;
