library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

use work.math_pkg.all;
use work.gfx_cmd_pkg.all;

package snake_ctrl_pkg is

	constant DISPLAY_WIDTH          : integer := 320; -- Display width in pixels (c.f. VGA Graphics Controller manual)
	constant DISPLAY_HEIGHT         : integer := 240; -- Display height in pixels (c.f. VGA Graphics Controller manual)
	constant SNAKE_CHAR_SIZE        : integer := 10; -- Width of the characters used to draw the snake (NOT the width of char of the font bitmap)
	constant SNAKE_FIELD_WIDTH      : integer := DISPLAY_WIDTH / SNAKE_CHAR_SIZE;
	constant SNAKE_FIELD_HEIGHT     : integer := DISPLAY_HEIGHT / SNAKE_CHAR_SIZE;
	constant SNAKE_FIELD_DATA_WIDTH : integer := 6; -- Player [1] & Rotation [2] & Entry Type [3] = Entry [6]
	
	-- DO NOT CHANGE THE SEQUENCE OF THIS TYPE
	-- Some function use the POS attribute and expect this sequence 
	type direction_t is (UP, DOWN, LEFT, RIGHT);
	type direction_array_t is array(integer range<>) of direction_t;
	-- Function to invert directions: not UP = DOWN, not LEFT = RIGHT
	function "NOT" (a : direction_t) return direction_t;

	-- Type for position on the game field
	constant VEC2D_X_WIDTH : integer := log2c(SNAKE_FIELD_WIDTH-1);
	constant VEC2D_Y_WIDTH : integer := log2c(SNAKE_FIELD_HEIGHT-1);
	type vec2d_t is 
	record
		x : std_logic_vector(VEC2D_X_WIDTH-1 downto 0);
		y : std_logic_vector(VEC2D_Y_WIDTH-1 downto 0);
	end record;
	constant NULL_VEC : vec2d_t := (x=>(others=>'0'), y=>(others=>'0'));

	function create_vec2d(x : integer; y : integer) return vec2d_t;
	function move_vec2d(vec: vec2d_t; dir : direction_t) return vec2d_t;
	function move_vec2d_and_wrap(vec: vec2d_t; dir : direction_t; x_max : integer; y_max :integer) return vec2d_t;

	type player_t is (PLAYER1, PLAYER2);
	function "NOT" (a : player_t) return player_t;
	-- PLAYER1 => 0, PLAYER2 => 1
	function player_to_int(player: player_t) return integer;
	-- PLAYER1 => '0', PLAYER2 => '1'
	function player_to_sl(player: player_t) return std_logic;

	type snake_position_t is
	record
		head_position  : vec2d_t;
		head_direction : direction_t;
		tail_position  : vec2d_t;
		tail_direction : direction_t;
	end record;
	
	constant NULL_SNAKE_POSITION : snake_position_t := (
		head_position  => NULL_VEC,
		head_direction => UP,
		tail_position  => NULL_VEC,
		tail_direction => UP
	);

	subtype entry_t is std_logic_vector(SNAKE_FIELD_DATA_WIDTH-1 downto 0);
	subtype entry_type_t is std_logic_vector(2 downto 0);
	-- IDs for the different snake characters
	constant BLANK_ENTRY    : entry_type_t := "000";
	constant SNAKE_HEAD     : entry_type_t := "001";
	constant SNAKE_TAIL     : entry_type_t := "010";
	constant SNAKE_STRAIGHT : entry_type_t := "011";
	constant SNAKE_BEND     : entry_type_t := "100";
	constant FOOD_REGULAR   : entry_type_t := "101";
	constant FOOD_SPECIAL   : entry_type_t := "110";

	-- Convenience constants for the rotated snake characters
	constant SNAKE_HEAD_UP    : std_logic_vector(4 downto 0) := ROT_R0 & SNAKE_HEAD;
	constant SNAKE_HEAD_DOWN  : std_logic_vector(4 downto 0) := ROT_R180 & SNAKE_HEAD;
	constant SNAKE_HEAD_LEFT  : std_logic_vector(4 downto 0) := ROT_R270 & SNAKE_HEAD;
	constant SNAKE_HEAD_RIGHT : std_logic_vector(4 downto 0) := ROT_R90 & SNAKE_HEAD;
	
	constant SNAKE_TAIL_UP    : std_logic_vector(4 downto 0) := ROT_R0 & SNAKE_TAIL;
	constant SNAKE_TAIL_DOWN  : std_logic_vector(4 downto 0) := ROT_R180 & SNAKE_TAIL;
	constant SNAKE_TAIL_LEFT  : std_logic_vector(4 downto 0) := ROT_R270 & SNAKE_TAIL;
	constant SNAKE_TAIL_RIGHT : std_logic_vector(4 downto 0) := ROT_R90 & SNAKE_TAIL;
	
	constant SNAKE_BODY_UP_LEFT    : std_logic_vector(4 downto 0) := ROT_R0 & SNAKE_BEND;
	constant SNAKE_BODY_UP_RIGHT   : std_logic_vector(4 downto 0) := ROT_R270 & SNAKE_BEND;
	constant SNAKE_BODY_DOWN_LEFT  : std_logic_vector(4 downto 0) := ROT_R90 & SNAKE_BEND;
	constant SNAKE_BODY_DOWN_RIGHT : std_logic_vector(4 downto 0) := ROT_R180 & SNAKE_BEND;
	
	constant SNAKE_BODY_HORIZONTAL : std_logic_vector(4 downto 0) := ROT_R90 & SNAKE_STRAIGHT;
	constant SNAKE_BODY_VERTICAL   : std_logic_vector(4 downto 0) := ROT_R0 & SNAKE_STRAIGHT;

	-- Functions for extracting fields from entries
	function get_entry_type(entry: entry_t) return entry_type_t;
	function get_rotation(entry: entry_t) return rot_t;
	function get_player(entry: entry_t) return player_t;

	function create_game_field_entry(entry_type: entry_type_t; rot: rot_t := ROT_R0; player: player_t := PLAYER1) return entry_t;
	function get_bmp_offset(entry: entry_t) return std_logic_vector;
	
	-- Functions return roation of head / tail character such that it has the right direction
	function get_head_rotation_from_direction(dir : direction_t) return rot_t;
	function get_tail_rotation_from_direction(dir : direction_t) return rot_t;

	function get_entry_for_head_replacement(old_dir : direction_t; new_dir : direction_t; player: player_t := PLAYER1) return entry_t;
	function get_new_tail_direction(old_tail_dir : direction_t; cur_tail_entry : entry_t) return direction_t;

	component snake_ctrl is
		port (
			clk : in std_logic;
			res_n : in std_logic;
			busy : in std_logic;
			rd : out std_logic;
			rd_pos : out vec2d_t;
			rd_data : in std_logic_vector(SNAKE_FIELD_DATA_WIDTH-1 downto 0);
			wr : out std_logic;
			wr_pos : out vec2d_t;
			wr_data : out std_logic_vector(SNAKE_FIELD_DATA_WIDTH-1 downto 0);
			player : in player_t;
			init : in std_logic;
			init_head_position  : in vec2d_t;
			init_head_direction : in direction_t;
			init_body_length : in std_logic_vector(3 downto 0);
			move_head : in std_logic;
			movement_direction : in direction_t;
			move_tail : in std_logic;
			done : out std_logic
		);
	end component;
end package;


package body snake_ctrl_pkg is

	function create_vec2d(x : integer; y : integer) return vec2d_t is
		constant vec : vec2d_t := (x=>std_logic_vector(to_unsigned(x, log2c(SNAKE_FIELD_WIDTH-1))), y=>std_logic_vector(to_unsigned(y, log2c(SNAKE_FIELD_HEIGHT-1))));
	begin
		return vec;
	end function;

	function move_vec2d(vec: vec2d_t; dir : direction_t) return vec2d_t is
		variable rvec : vec2d_t;
	begin
		rvec := vec;
		case dir is
			when UP =>
				rvec.y := std_logic_vector(unsigned(rvec.y) - 1);
			when DOWN =>
				rvec.y := std_logic_vector(unsigned(rvec.y) + 1);
			when LEFT =>
				rvec.x := std_logic_vector(unsigned(rvec.x) - 1);
			when RIGHT =>
				rvec.x := std_logic_vector(unsigned(rvec.x) + 1);
		end case;
		
		return rvec;
	end function;
	
	function move_vec2d_and_wrap(vec: vec2d_t; dir : direction_t; x_max : integer; y_max :integer) return vec2d_t is
		variable rvec : vec2d_t;
	begin
		rvec := vec;
		case dir is
			when UP =>
				if(unsigned(rvec.y) = 0) then
					rvec.y := std_logic_vector(to_unsigned(y_max,VEC2D_Y_WIDTH));
				else
					rvec.y := std_logic_vector(unsigned(rvec.y) - 1);
				end if;
			when DOWN =>
				if(unsigned(rvec.y) = y_max) then
					rvec.y := (others=>'0');
				else
					rvec.y := std_logic_vector(unsigned(rvec.y) + 1);
				end if;
			when LEFT =>
				if(unsigned(rvec.x) = 0) then
					rvec.x := std_logic_vector(to_unsigned(x_max,VEC2D_X_WIDTH));
				else
					rvec.x := std_logic_vector(unsigned(rvec.x) - 1);
				end if;
			when RIGHT =>
				if(unsigned(rvec.x) = x_max) then
					rvec.x := (others=>'0');
				else
					rvec.x := std_logic_vector(unsigned(rvec.x) + 1);
				end if;
		end case;
		
		return rvec;
	end function;
	
	function "NOT" (a : direction_t) return direction_t is
		constant mapping : direction_array_t(0 to 3) := (DOWN, UP, RIGHT, LEFT);
	begin
		return mapping(direction_t'pos(a));
	end function;

	function "NOT" (a : player_t) return player_t is
	begin
		if (a = PLAYER1) then
			return PLAYER2;
		else
			return PLAYER1;
		end if;
	end function;

	function player_to_sl(player: player_t) return std_logic is
	begin
		if (player = PLAYER1) then
			return '0';
		else
			return '1';
		end if;
	end function;

	function player_to_int(player: player_t) return integer is
	begin
		if (player = PLAYER2) then
			return 1;
		end if;
		return 0;
	end function;

	function get_entry_type(entry: entry_t) return entry_type_t is
	begin
		return entry(2 downto 0);
	end function;

	function get_bmp_offset(entry: entry_t) return std_logic_vector is
		variable entry_type : entry_type_t;
		constant OFFSET_WIDTH : integer := 10;
	begin
		entry_type := entry(2 downto 0);

		case entry_type is
			when SNAKE_HEAD     => return std_logic_vector(to_unsigned(0 * SNAKE_CHAR_SIZE, OFFSET_WIDTH));
			when SNAKE_TAIL     => return std_logic_vector(to_unsigned(1 * SNAKE_CHAR_SIZE, OFFSET_WIDTH));
			when SNAKE_STRAIGHT => return std_logic_vector(to_unsigned(2 * SNAKE_CHAR_SIZE, OFFSET_WIDTH));
			when SNAKE_BEND     => return std_logic_vector(to_unsigned(3 * SNAKE_CHAR_SIZE, OFFSET_WIDTH));
			when FOOD_REGULAR   => return std_logic_vector(to_unsigned(4 * SNAKE_CHAR_SIZE, OFFSET_WIDTH));
			when FOOD_SPECIAL   => return std_logic_vector(to_unsigned(5 * SNAKE_CHAR_SIZE, OFFSET_WIDTH));
			when others =>
				null;
				-- Useless return, but keeps compiler quiet
				return (0 to OFFSET_WIDTH-1 => '0');
		end case;
	end function;

	function get_rotation(entry: entry_t) return rot_t is
	begin
		return entry(4 downto 3);
	end function;

	function get_player(entry: entry_t) return player_t is
	begin
		case entry(5) is
			when '0' => return PLAYER1;
			when '1' => return PLAYER2;
			when others =>
				null;
				-- Useless return, but keeps compiler quiet
				return PLAYER1;
		end case;
	end function;

	function create_game_field_entry(entry_type: entry_type_t; rot: rot_t := ROT_R0; player: player_t := PLAYER1) return entry_t is
	begin
		return player_to_sl(player) & rot & entry_type;
	end function;

	function get_head_rotation_from_direction(dir : direction_t) return rot_t is
	begin
		case dir is
			when UP => return ROT_R0;
			when RIGHT => return ROT_R90;
			when DOWN => return ROT_R180;
			when LEFT => return ROT_R270;
		end case;
	end function;

	function get_tail_rotation_from_direction(dir : direction_t) return rot_t is
	begin
		case dir is
			when UP => return ROT_R180;
			when RIGHT => return ROT_R270;
			when DOWN => return ROT_R0;
			when LEFT => return ROT_R90;
		end case;
	end function;

	function get_entry_for_head_replacement(old_dir : direction_t; new_dir : direction_t; player: player_t := PLAYER1) return entry_t is
		variable entry: entry_t;
	begin
		entry(5) := player_to_sl(player);
		case old_dir is
			when UP =>
				case new_dir is
					when UP     => entry(4 downto 0) := SNAKE_BODY_VERTICAL;
					when LEFT   => entry(4 downto 0) := SNAKE_BODY_UP_LEFT;
					when RIGHT  => entry(4 downto 0) := SNAKE_BODY_UP_RIGHT;
					when others => entry(4 downto 0) := "00111"; --invalid
				end case;
			when DOWN =>
				case new_dir is
					when DOWN   => entry(4 downto 0) := SNAKE_BODY_VERTICAL;
					when LEFT   => entry(4 downto 0) := SNAKE_BODY_DOWN_LEFT;
					when RIGHT  => entry(4 downto 0) := SNAKE_BODY_DOWN_RIGHT;
					when others => entry(4 downto 0) := "00111"; --invalid
				end case;
			when LEFT =>
				case new_dir is
					when LEFT   => entry(4 downto 0) := SNAKE_BODY_HORIZONTAL;
					when UP     => entry(4 downto 0) := SNAKE_BODY_DOWN_RIGHT;
					when DOWN   => entry(4 downto 0) := SNAKE_BODY_UP_RIGHT;
					when others => entry(4 downto 0) := "00111"; --invalid
				end case;
			when RIGHT =>
				case new_dir is
					when RIGHT  => entry(4 downto 0) := SNAKE_BODY_HORIZONTAL;
					when UP     => entry(4 downto 0) := SNAKE_BODY_DOWN_LEFT;
					when DOWN   => entry(4 downto 0) := SNAKE_BODY_UP_LEFT;
					when others => entry(4 downto 0) := "00111"; --invalid
				end case;
		end case;

		return entry;
	end function;
	
	function get_new_tail_direction(old_tail_dir : direction_t; cur_tail_entry : entry_t) return direction_t is
		variable cur_tail_symbol : std_logic_vector(4 downto 0);
	begin
		cur_tail_symbol := get_rotation(cur_tail_entry) & get_entry_type(cur_tail_entry);
		case old_tail_dir is
			when DOWN =>
				case cur_tail_symbol is
					when SNAKE_BODY_VERTICAL => return DOWN;
					when SNAKE_BODY_DOWN_LEFT => return LEFT;
					when SNAKE_BODY_DOWN_RIGHT => return RIGHT;
					when others => return UP; --invalid
				end case;
			when UP =>
				case cur_tail_symbol is
					when SNAKE_BODY_VERTICAL => return UP;
					when SNAKE_BODY_UP_LEFT => return LEFT;
					when SNAKE_BODY_UP_RIGHT => return RIGHT;
					when others => return UP; --invalid
				end case;
			when LEFT =>
				case cur_tail_symbol is
					when SNAKE_BODY_HORIZONTAL => return LEFT;
					when SNAKE_BODY_UP_RIGHT => return DOWN;
					when SNAKE_BODY_DOWN_RIGHT => return UP;
					when others => return UP; --invalid
				end case;
			when RIGHT =>
				case cur_tail_symbol is
					when SNAKE_BODY_HORIZONTAL => return RIGHT;
					when SNAKE_BODY_UP_LEFT => return DOWN;
					when SNAKE_BODY_DOWN_LEFT => return UP;
					when others => return UP; --invalid
				end case;
		end case;
	end function;
end package body;

