-- Copyright (C) 2023  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 22.1std.2 Build 922 07/20/2023 SC Standard Edition"

-- DATE "04/10/2024 14:03:42"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim (VHDL) only
-- 

LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_ASDO_DATA1~	=>  Location: PIN_F4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_FLASH_nCE_nCSO~	=>  Location: PIN_E2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DCLK~	=>  Location: PIN_P3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DATA0~	=>  Location: PIN_N7,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~padout\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~padout\ : std_logic;
SIGNAL \~ALTERA_DATA0~~padout\ : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_DATA0~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	snake_ctrl_top IS
    PORT (
	clk : IN std_logic;
	res_n : IN std_logic;
	busy : IN std_logic;
	rd : OUT std_logic;
	\rd_pos.y\ : OUT std_logic_vector(4 DOWNTO 0);
	\rd_pos.x\ : OUT std_logic_vector(4 DOWNTO 0);
	rd_data : IN std_logic_vector(5 DOWNTO 0);
	wr : OUT std_logic;
	\wr_pos.y\ : OUT std_logic_vector(4 DOWNTO 0);
	\wr_pos.x\ : OUT std_logic_vector(4 DOWNTO 0);
	wr_data : OUT std_logic_vector(5 DOWNTO 0);
	player : IN std_logic;
	init : IN std_logic;
	\init_head_position.y\ : IN std_logic_vector(4 DOWNTO 0);
	\init_head_position.x\ : IN std_logic_vector(4 DOWNTO 0);
	\init_head_direction.UP\ : IN std_logic;
	\init_head_direction.DOWN\ : IN std_logic;
	\init_head_direction.LEFT\ : IN std_logic;
	\init_head_direction.RIGHT\ : IN std_logic;
	init_body_length : IN std_logic_vector(3 DOWNTO 0);
	move_head : IN std_logic;
	\movement_direction.UP\ : IN std_logic;
	\movement_direction.DOWN\ : IN std_logic;
	\movement_direction.LEFT\ : IN std_logic;
	\movement_direction.RIGHT\ : IN std_logic;
	move_tail : IN std_logic;
	done : OUT std_logic
	);
END snake_ctrl_top;

-- Design Ports Information
-- rd	=>  Location: PIN_D20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd_pos.y[0]	=>  Location: PIN_D21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd_pos.y[1]	=>  Location: PIN_G21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd_pos.y[2]	=>  Location: PIN_F18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd_pos.y[3]	=>  Location: PIN_D17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd_pos.y[4]	=>  Location: PIN_C17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd_pos.x[0]	=>  Location: PIN_D19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd_pos.x[1]	=>  Location: PIN_G17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd_pos.x[2]	=>  Location: PIN_C18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd_pos.x[3]	=>  Location: PIN_C21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd_pos.x[4]	=>  Location: PIN_D12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd_data[5]	=>  Location: PIN_H24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr	=>  Location: PIN_G16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_pos.y[0]	=>  Location: PIN_C22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_pos.y[1]	=>  Location: PIN_B19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_pos.y[2]	=>  Location: PIN_F15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_pos.y[3]	=>  Location: PIN_D24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_pos.y[4]	=>  Location: PIN_A21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_pos.x[0]	=>  Location: PIN_A19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_pos.x[1]	=>  Location: PIN_B22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_pos.x[2]	=>  Location: PIN_A17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_pos.x[3]	=>  Location: PIN_J16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_pos.x[4]	=>  Location: PIN_E24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_data[0]	=>  Location: PIN_C14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_data[1]	=>  Location: PIN_G22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_data[2]	=>  Location: PIN_C19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_data[3]	=>  Location: PIN_G19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_data[4]	=>  Location: PIN_H19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_data[5]	=>  Location: PIN_H15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- done	=>  Location: PIN_H17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- busy	=>  Location: PIN_G18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clk	=>  Location: PIN_J1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- res_n	=>  Location: PIN_Y2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- player	=>  Location: PIN_A18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- init	=>  Location: PIN_H21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- move_head	=>  Location: PIN_G20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- move_tail	=>  Location: PIN_D18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- init_body_length[1]	=>  Location: PIN_E18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- init_body_length[0]	=>  Location: PIN_E25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- init_body_length[3]	=>  Location: PIN_B18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- init_body_length[2]	=>  Location: PIN_B21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- init_head_position.y[0]	=>  Location: PIN_A22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- init_head_position.y[1]	=>  Location: PIN_F19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- init_head_position.y[2]	=>  Location: PIN_C20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- init_head_position.y[3]	=>  Location: PIN_E19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- init_head_position.y[4]	=>  Location: PIN_E17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- init_head_position.x[0]	=>  Location: PIN_C15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- init_head_position.x[1]	=>  Location: PIN_C13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- init_head_position.x[2]	=>  Location: PIN_C12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- init_head_position.x[3]	=>  Location: PIN_D14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- init_head_position.x[4]	=>  Location: PIN_A12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- init_head_direction.DOWN	=>  Location: PIN_E15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- movement_direction.DOWN	=>  Location: PIN_J17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- movement_direction.UP	=>  Location: PIN_D13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- init_head_direction.UP	=>  Location: PIN_J19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- movement_direction.LEFT	=>  Location: PIN_D27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- movement_direction.RIGHT	=>  Location: PIN_F17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- init_head_direction.RIGHT	=>  Location: PIN_D15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- init_head_direction.LEFT	=>  Location: PIN_D16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd_data[3]	=>  Location: PIN_C16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd_data[4]	=>  Location: PIN_J15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd_data[2]	=>  Location: PIN_B17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd_data[0]	=>  Location: PIN_G15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd_data[1]	=>  Location: PIN_H16,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF snake_ctrl_top IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_res_n : std_logic;
SIGNAL ww_busy : std_logic;
SIGNAL ww_rd : std_logic;
SIGNAL \ww_rd_pos.y\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \ww_rd_pos.x\ : std_logic_vector(4 DOWNTO 0);
SIGNAL ww_rd_data : std_logic_vector(5 DOWNTO 0);
SIGNAL ww_wr : std_logic;
SIGNAL \ww_wr_pos.y\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \ww_wr_pos.x\ : std_logic_vector(4 DOWNTO 0);
SIGNAL ww_wr_data : std_logic_vector(5 DOWNTO 0);
SIGNAL ww_player : std_logic;
SIGNAL ww_init : std_logic;
SIGNAL \ww_init_head_position.y\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \ww_init_head_position.x\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \ww_init_head_direction.UP\ : std_logic;
SIGNAL \ww_init_head_direction.DOWN\ : std_logic;
SIGNAL \ww_init_head_direction.LEFT\ : std_logic;
SIGNAL \ww_init_head_direction.RIGHT\ : std_logic;
SIGNAL ww_init_body_length : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_move_head : std_logic;
SIGNAL \ww_movement_direction.UP\ : std_logic;
SIGNAL \ww_movement_direction.DOWN\ : std_logic;
SIGNAL \ww_movement_direction.LEFT\ : std_logic;
SIGNAL \ww_movement_direction.RIGHT\ : std_logic;
SIGNAL ww_move_tail : std_logic;
SIGNAL ww_done : std_logic;
SIGNAL \res_n~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \clk~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \rd_data[5]~input_o\ : std_logic;
SIGNAL \rd~output_o\ : std_logic;
SIGNAL \rd_pos.y[0]~output_o\ : std_logic;
SIGNAL \rd_pos.y[1]~output_o\ : std_logic;
SIGNAL \rd_pos.y[2]~output_o\ : std_logic;
SIGNAL \rd_pos.y[3]~output_o\ : std_logic;
SIGNAL \rd_pos.y[4]~output_o\ : std_logic;
SIGNAL \rd_pos.x[0]~output_o\ : std_logic;
SIGNAL \rd_pos.x[1]~output_o\ : std_logic;
SIGNAL \rd_pos.x[2]~output_o\ : std_logic;
SIGNAL \rd_pos.x[3]~output_o\ : std_logic;
SIGNAL \rd_pos.x[4]~output_o\ : std_logic;
SIGNAL \wr~output_o\ : std_logic;
SIGNAL \wr_pos.y[0]~output_o\ : std_logic;
SIGNAL \wr_pos.y[1]~output_o\ : std_logic;
SIGNAL \wr_pos.y[2]~output_o\ : std_logic;
SIGNAL \wr_pos.y[3]~output_o\ : std_logic;
SIGNAL \wr_pos.y[4]~output_o\ : std_logic;
SIGNAL \wr_pos.x[0]~output_o\ : std_logic;
SIGNAL \wr_pos.x[1]~output_o\ : std_logic;
SIGNAL \wr_pos.x[2]~output_o\ : std_logic;
SIGNAL \wr_pos.x[3]~output_o\ : std_logic;
SIGNAL \wr_pos.x[4]~output_o\ : std_logic;
SIGNAL \wr_data[0]~output_o\ : std_logic;
SIGNAL \wr_data[1]~output_o\ : std_logic;
SIGNAL \wr_data[2]~output_o\ : std_logic;
SIGNAL \wr_data[3]~output_o\ : std_logic;
SIGNAL \wr_data[4]~output_o\ : std_logic;
SIGNAL \wr_data[5]~output_o\ : std_logic;
SIGNAL \done~output_o\ : std_logic;
SIGNAL \clk~input_o\ : std_logic;
SIGNAL \clk~inputclkctrl_outclk\ : std_logic;
SIGNAL \busy~input_o\ : std_logic;
SIGNAL \init~input_o\ : std_logic;
SIGNAL \move_head~input_o\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector45~0_combout\ : std_logic;
SIGNAL \res_n~input_o\ : std_logic;
SIGNAL \res_n~inputclkctrl_outclk\ : std_logic;
SIGNAL \snake_ctrl_inst|state.WAIT_OLD_TAIL_TYPE~q\ : std_logic;
SIGNAL \snake_ctrl_inst|state.CALCULATE_NEW_TAIL_DIRECTION~q\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector49~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|state.DRAW_NEW_TAIL~q\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_pos.y[3]~8_combout\ : std_logic;
SIGNAL \move_tail~input_o\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector40~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|cur_length[2]~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector40~5_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|state.PLACE_HEAD~q\ : std_logic;
SIGNAL \init_body_length[1]~input_o\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector53~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|cur_length[2]~3_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector52~0_combout\ : std_logic;
SIGNAL \init_body_length[0]~input_o\ : std_logic;
SIGNAL \snake_ctrl_inst|Equal0~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector131~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector41~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|state.PLACE_BODY~q\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector51~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add4~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector50~0_combout\ : std_logic;
SIGNAL \init_body_length[2]~input_o\ : std_logic;
SIGNAL \init_body_length[3]~input_o\ : std_logic;
SIGNAL \snake_ctrl_inst|target_init_body_length[3]~feeder_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Equal0~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector40~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector40~3_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector42~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|state.PLACE_TAIL~q\ : std_logic;
SIGNAL \snake_ctrl_inst|target_movement_direction~12_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector43~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\ : std_logic;
SIGNAL \snake_ctrl_inst|state.DRAW_NEW_HEAD~q\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector39~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector39~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|state.SET_DONE~q\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector38~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|state.IDLE~q\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector40~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector40~4_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector45~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector45~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\ : std_logic;
SIGNAL \snake_ctrl_inst|state.GET_OLD_TAIL_TYPE~q\ : std_logic;
SIGNAL \snake_ctrl_inst|rd~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd~q\ : std_logic;
SIGNAL \player~input_o\ : std_logic;
SIGNAL \snake_ctrl_inst|cur_player~q\ : std_logic;
SIGNAL \init_head_direction.LEFT~input_o\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[1].head_position.x[2]~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[1].tail_direction.LEFT~0_combout\ : std_logic;
SIGNAL \rd_data[0]~input_o\ : std_logic;
SIGNAL \rd_data[2]~input_o\ : std_logic;
SIGNAL \rd_data[1]~input_o\ : std_logic;
SIGNAL \rd_data[4]~input_o\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector35~2_combout\ : std_logic;
SIGNAL \rd_data[3]~input_o\ : std_logic;
SIGNAL \init_head_direction.DOWN~input_o\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[0].head_position.y[0]~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[0].tail_direction.DOWN~0_combout\ : std_logic;
SIGNAL \init_head_direction.RIGHT~input_o\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[0].tail_direction.RIGHT~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Mux4~0_combout\ : std_logic;
SIGNAL \init_head_direction.UP~input_o\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector116~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector34~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector34~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector34~4_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector34~3_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector34~6_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector34~5_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector34~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector34~7_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector116~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[0].tail_direction.UP~q\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector88~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector88~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[1].tail_direction.UP~q\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_data~4_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector37~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector37~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector116~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[0].tail_direction.RIGHT~q\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[1].tail_direction.RIGHT~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector88~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[1].tail_direction.RIGHT~q\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_data~6_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector35~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector35~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector35~3_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[0].tail_direction.DOWN~q\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[1].tail_direction.DOWN~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[1].tail_direction.DOWN~q\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_data~5_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector36~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector36~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector36~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[1].tail_direction.LEFT~q\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[0].tail_direction.LEFT~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[0].tail_direction.LEFT~q\ : std_logic;
SIGNAL \snake_ctrl_inst|tail_position~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector115~3_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[1].tail_position.y[4]~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~40_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add10~1\ : std_logic;
SIGNAL \snake_ctrl_inst|Add10~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector112~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add10~3\ : std_logic;
SIGNAL \snake_ctrl_inst|Add10~4_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add9~1\ : std_logic;
SIGNAL \snake_ctrl_inst|Add9~3\ : std_logic;
SIGNAL \snake_ctrl_inst|Add9~5\ : std_logic;
SIGNAL \snake_ctrl_inst|Add9~7\ : std_logic;
SIGNAL \snake_ctrl_inst|Add9~8_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add10~7\ : std_logic;
SIGNAL \snake_ctrl_inst|Add10~8_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector111~0_combout\ : std_logic;
SIGNAL \movement_direction.DOWN~input_o\ : std_logic;
SIGNAL \snake_ctrl_inst|target_movement_direction.DOWN~feeder_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|target_movement_direction.DOWN~q\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector133~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector103~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[0].head_position.y[0]~3_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[0].head_direction.DOWN~q\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector131~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector75~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[1].head_position.x[2]~3_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[1].head_direction.DOWN~q\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_data~0_combout\ : std_logic;
SIGNAL \movement_direction.RIGHT~input_o\ : std_logic;
SIGNAL \snake_ctrl_inst|target_movement_direction.RIGHT~q\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector105~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[0].head_direction.RIGHT~q\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector77~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[1].head_direction.RIGHT~q\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_data~2_combout\ : std_logic;
SIGNAL \movement_direction.LEFT~input_o\ : std_logic;
SIGNAL \snake_ctrl_inst|target_movement_direction.LEFT~q\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector104~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[0].head_direction.LEFT~q\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector76~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[1].head_direction.LEFT~q\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_data~3_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Mux0~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|cur_position.y[0]~5_combout\ : std_logic;
SIGNAL \init_head_position.y[0]~input_o\ : std_logic;
SIGNAL \snake_ctrl_inst|cur_position.y[1]~7_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|cur_position.y[1]~8_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|cur_position.y[1]~9_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|cur_position.y[0]~6\ : std_logic;
SIGNAL \snake_ctrl_inst|cur_position.y[1]~10_combout\ : std_logic;
SIGNAL \init_head_position.y[1]~input_o\ : std_logic;
SIGNAL \snake_ctrl_inst|cur_position.y[1]~11\ : std_logic;
SIGNAL \snake_ctrl_inst|cur_position.y[2]~12_combout\ : std_logic;
SIGNAL \init_head_position.y[2]~input_o\ : std_logic;
SIGNAL \snake_ctrl_inst|cur_position.y[2]~13\ : std_logic;
SIGNAL \snake_ctrl_inst|cur_position.y[3]~14_combout\ : std_logic;
SIGNAL \init_head_position.y[3]~input_o\ : std_logic;
SIGNAL \snake_ctrl_inst|cur_position.y[3]~15\ : std_logic;
SIGNAL \snake_ctrl_inst|cur_position.y[4]~16_combout\ : std_logic;
SIGNAL \init_head_position.y[4]~input_o\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector115~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector111~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector111~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[0].tail_position.y[4]~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~44_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector113~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add9~4_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector113~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector113~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~42_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add10~5\ : std_logic;
SIGNAL \snake_ctrl_inst|Add10~6_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector112~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add9~6_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector112~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector112~3_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~43_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Equal6~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector114~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add9~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector114~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector114~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~41_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Equal5~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector115~4_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add9~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector115~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add10~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector115~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector115~5_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~60_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~61_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~62_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~63_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~64_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~45_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add11~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add12~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add12~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|cur_position.x[0]~5_combout\ : std_logic;
SIGNAL \init_head_position.x[0]~input_o\ : std_logic;
SIGNAL \snake_ctrl_inst|cur_position.x[3]~7_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add12~3_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~65_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~46_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add12~1\ : std_logic;
SIGNAL \snake_ctrl_inst|Add12~4_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add11~1\ : std_logic;
SIGNAL \snake_ctrl_inst|Add11~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add12~6_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Mux2~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|cur_position.x[0]~6\ : std_logic;
SIGNAL \snake_ctrl_inst|cur_position.x[1]~8_combout\ : std_logic;
SIGNAL \init_head_position.x[1]~input_o\ : std_logic;
SIGNAL \snake_ctrl_inst|Add12~7_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~66_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|cur_position.x[1]~9\ : std_logic;
SIGNAL \snake_ctrl_inst|cur_position.x[2]~10_combout\ : std_logic;
SIGNAL \init_head_position.x[2]~input_o\ : std_logic;
SIGNAL \snake_ctrl_inst|Add12~10_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~47_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add12~5\ : std_logic;
SIGNAL \snake_ctrl_inst|Add12~8_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add11~3\ : std_logic;
SIGNAL \snake_ctrl_inst|Add11~4_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add12~11_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~67_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~48_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add11~5\ : std_logic;
SIGNAL \snake_ctrl_inst|Add11~6_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|cur_position.x[2]~11\ : std_logic;
SIGNAL \snake_ctrl_inst|cur_position.x[3]~12_combout\ : std_logic;
SIGNAL \init_head_position.x[3]~input_o\ : std_logic;
SIGNAL \snake_ctrl_inst|Add12~14_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add12~9\ : std_logic;
SIGNAL \snake_ctrl_inst|Add12~12_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add12~15_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~68_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~49_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add11~7\ : std_logic;
SIGNAL \snake_ctrl_inst|Add11~8_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add12~13\ : std_logic;
SIGNAL \snake_ctrl_inst|Add12~16_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|cur_position.x[3]~13\ : std_logic;
SIGNAL \snake_ctrl_inst|cur_position.x[4]~14_combout\ : std_logic;
SIGNAL \init_head_position.x[4]~input_o\ : std_logic;
SIGNAL \snake_ctrl_inst|Add12~18_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add12~19_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~69_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector120~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector120~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr~q\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~50_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_pos.y[3]~5_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_pos.y[0]~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector101~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector101~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector100~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector98~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector101~3_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add6~1\ : std_logic;
SIGNAL \snake_ctrl_inst|Add6~3\ : std_logic;
SIGNAL \snake_ctrl_inst|Add6~4_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add5~1\ : std_logic;
SIGNAL \snake_ctrl_inst|Add5~3\ : std_logic;
SIGNAL \snake_ctrl_inst|Add5~4_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector99~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector99~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector99~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_pos~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Equal1~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector101~4_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add5~5\ : std_logic;
SIGNAL \snake_ctrl_inst|Add5~6_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add6~5\ : std_logic;
SIGNAL \snake_ctrl_inst|Add6~6_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector98~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector98~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector98~3_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_pos~3_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add5~7\ : std_logic;
SIGNAL \snake_ctrl_inst|Add5~8_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector97~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add6~7\ : std_logic;
SIGNAL \snake_ctrl_inst|Add6~8_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector97~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector97~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_pos~4_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector101~6_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add5~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector100~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add6~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector100~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_pos~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Equal2~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add6~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector101~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add5~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector101~5_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_pos~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_pos.y[3]~6_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_pos.y[3]~7_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_pos.y[3]~9_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_pos.y[3]~10_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~51_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_pos.y[1]~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~52_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_pos.y[2]~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~53_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_pos.y[3]~3_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~54_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_pos.y[4]~4_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~55_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_pos.x[0]~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add8~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add7~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add8~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add8~3_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_pos~5_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~56_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_pos.x[1]~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add8~1\ : std_logic;
SIGNAL \snake_ctrl_inst|Add8~4_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add7~1\ : std_logic;
SIGNAL \snake_ctrl_inst|Add7~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add8~6_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add8~7_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_pos~6_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~57_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_pos.x[2]~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add7~3\ : std_logic;
SIGNAL \snake_ctrl_inst|Add7~4_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add8~5\ : std_logic;
SIGNAL \snake_ctrl_inst|Add8~8_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add8~10_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add8~11_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_pos~7_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~58_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_pos.x[3]~3_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add8~9\ : std_logic;
SIGNAL \snake_ctrl_inst|Add8~12_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add8~14_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add7~5\ : std_logic;
SIGNAL \snake_ctrl_inst|Add7~6_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add8~15_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_pos~8_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|rd_pos~59_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_pos.x[4]~4_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add8~18_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add7~7\ : std_logic;
SIGNAL \snake_ctrl_inst|Add7~8_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add8~13\ : std_logic;
SIGNAL \snake_ctrl_inst|Add8~16_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Add8~19_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_pos~9_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector13~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector120~2_combout\ : std_logic;
SIGNAL \movement_direction.UP~input_o\ : std_logic;
SIGNAL \snake_ctrl_inst|target_movement_direction.UP~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|target_movement_direction.UP~q\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector74~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[1].head_direction.UP~q\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector102~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[0].head_direction.UP~q\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_data~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector13~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector135~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector135~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector134~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector12~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector12~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector12~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|wr_data[2]~feeder_combout\ : std_logic;
SIGNAL \~GND~combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector133~3_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector133~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector11~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector11~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector11~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector133~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector133~4_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector132~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector132~0_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector132~2_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector132~3_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector132~4_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector131~3_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector131~1_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|Selector131~4_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|done~feeder_combout\ : std_logic;
SIGNAL \snake_ctrl_inst|done~q\ : std_logic;
SIGNAL \snake_ctrl_inst|snake_pos[0].head_position.x\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \snake_ctrl_inst|rd_pos.y\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \snake_ctrl_inst|wr_data\ : std_logic_vector(5 DOWNTO 0);
SIGNAL \snake_ctrl_inst|cur_position.y\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \snake_ctrl_inst|wr_pos.y\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \snake_ctrl_inst|cur_position.x\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \snake_ctrl_inst|wr_pos.x\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \snake_ctrl_inst|rd_pos.x\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \snake_ctrl_inst|snake_pos[1].tail_position.y\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \snake_ctrl_inst|snake_pos[0].tail_position.y\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \snake_ctrl_inst|snake_pos[1].tail_position.x\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \snake_ctrl_inst|snake_pos[0].tail_position.x\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \snake_ctrl_inst|cur_length\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \snake_ctrl_inst|target_init_body_length\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \snake_ctrl_inst|snake_pos[1].head_position.y\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \snake_ctrl_inst|snake_pos[0].head_position.y\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \snake_ctrl_inst|snake_pos[1].head_position.x\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \snake_ctrl_inst|ALT_INV_state.IDLE~q\ : std_logic;
SIGNAL \snake_ctrl_inst|ALT_INV_state.REPLACE_OLD_HEAD~q\ : std_logic;
SIGNAL \ALT_INV_busy~input_o\ : std_logic;

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

ww_clk <= clk;
ww_res_n <= res_n;
ww_busy <= busy;
rd <= ww_rd;
\rd_pos.y\ <= \ww_rd_pos.y\;
\rd_pos.x\ <= \ww_rd_pos.x\;
ww_rd_data <= rd_data;
wr <= ww_wr;
\wr_pos.y\ <= \ww_wr_pos.y\;
\wr_pos.x\ <= \ww_wr_pos.x\;
wr_data <= ww_wr_data;
ww_player <= player;
ww_init <= init;
\ww_init_head_position.y\ <= \init_head_position.y\;
\ww_init_head_position.x\ <= \init_head_position.x\;
\ww_init_head_direction.UP\ <= \init_head_direction.UP\;
\ww_init_head_direction.DOWN\ <= \init_head_direction.DOWN\;
\ww_init_head_direction.LEFT\ <= \init_head_direction.LEFT\;
\ww_init_head_direction.RIGHT\ <= \init_head_direction.RIGHT\;
ww_init_body_length <= init_body_length;
ww_move_head <= move_head;
\ww_movement_direction.UP\ <= \movement_direction.UP\;
\ww_movement_direction.DOWN\ <= \movement_direction.DOWN\;
\ww_movement_direction.LEFT\ <= \movement_direction.LEFT\;
\ww_movement_direction.RIGHT\ <= \movement_direction.RIGHT\;
ww_move_tail <= move_tail;
done <= ww_done;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\res_n~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \res_n~input_o\);

\clk~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \clk~input_o\);
\snake_ctrl_inst|ALT_INV_state.IDLE~q\ <= NOT \snake_ctrl_inst|state.IDLE~q\;
\snake_ctrl_inst|ALT_INV_state.REPLACE_OLD_HEAD~q\ <= NOT \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\;
\ALT_INV_busy~input_o\ <= NOT \busy~input_o\;
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: IOOBUF_X85_Y73_N16
\rd~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|rd~q\,
	devoe => ww_devoe,
	o => \rd~output_o\);

-- Location: IOOBUF_X96_Y73_N23
\rd_pos.y[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|rd_pos.y\(0),
	devoe => ww_devoe,
	o => \rd_pos.y[0]~output_o\);

-- Location: IOOBUF_X74_Y73_N23
\rd_pos.y[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|rd_pos.y\(1),
	devoe => ww_devoe,
	o => \rd_pos.y[1]~output_o\);

-- Location: IOOBUF_X87_Y73_N16
\rd_pos.y[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|rd_pos.y\(2),
	devoe => ww_devoe,
	o => \rd_pos.y[2]~output_o\);

-- Location: IOOBUF_X81_Y73_N9
\rd_pos.y[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|rd_pos.y\(3),
	devoe => ww_devoe,
	o => \rd_pos.y[3]~output_o\);

-- Location: IOOBUF_X81_Y73_N2
\rd_pos.y[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|rd_pos.y\(4),
	devoe => ww_devoe,
	o => \rd_pos.y[4]~output_o\);

-- Location: IOOBUF_X83_Y73_N16
\rd_pos.x[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|rd_pos.x\(0),
	devoe => ww_devoe,
	o => \rd_pos.x[0]~output_o\);

-- Location: IOOBUF_X83_Y73_N23
\rd_pos.x[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|rd_pos.x\(1),
	devoe => ww_devoe,
	o => \rd_pos.x[1]~output_o\);

-- Location: IOOBUF_X87_Y73_N23
\rd_pos.x[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|rd_pos.x\(2),
	devoe => ww_devoe,
	o => \rd_pos.x[2]~output_o\);

-- Location: IOOBUF_X91_Y73_N16
\rd_pos.x[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|rd_pos.x\(3),
	devoe => ww_devoe,
	o => \rd_pos.x[3]~output_o\);

-- Location: IOOBUF_X52_Y73_N23
\rd_pos.x[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|rd_pos.x\(4),
	devoe => ww_devoe,
	o => \rd_pos.x[4]~output_o\);

-- Location: IOOBUF_X67_Y73_N2
\wr~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|wr~q\,
	devoe => ww_devoe,
	o => \wr~output_o\);

-- Location: IOOBUF_X96_Y73_N16
\wr_pos.y[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|wr_pos.y\(0),
	devoe => ww_devoe,
	o => \wr_pos.y[0]~output_o\);

-- Location: IOOBUF_X81_Y73_N23
\wr_pos.y[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|wr_pos.y\(1),
	devoe => ww_devoe,
	o => \wr_pos.y[1]~output_o\);

-- Location: IOOBUF_X58_Y73_N2
\wr_pos.y[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|wr_pos.y\(2),
	devoe => ww_devoe,
	o => \wr_pos.y[2]~output_o\);

-- Location: IOOBUF_X98_Y73_N23
\wr_pos.y[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|wr_pos.y\(3),
	devoe => ww_devoe,
	o => \wr_pos.y[3]~output_o\);

-- Location: IOOBUF_X89_Y73_N23
\wr_pos.y[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|wr_pos.y\(4),
	devoe => ww_devoe,
	o => \wr_pos.y[4]~output_o\);

-- Location: IOOBUF_X81_Y73_N16
\wr_pos.x[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|wr_pos.x\(0),
	devoe => ww_devoe,
	o => \wr_pos.x[0]~output_o\);

-- Location: IOOBUF_X89_Y73_N16
\wr_pos.x[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|wr_pos.x\(1),
	devoe => ww_devoe,
	o => \wr_pos.x[1]~output_o\);

-- Location: IOOBUF_X60_Y73_N2
\wr_pos.x[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|wr_pos.x\(2),
	devoe => ww_devoe,
	o => \wr_pos.x[2]~output_o\);

-- Location: IOOBUF_X65_Y73_N16
\wr_pos.x[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|wr_pos.x\(3),
	devoe => ww_devoe,
	o => \wr_pos.x[3]~output_o\);

-- Location: IOOBUF_X85_Y73_N23
\wr_pos.x[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|wr_pos.x\(4),
	devoe => ww_devoe,
	o => \wr_pos.x[4]~output_o\);

-- Location: IOOBUF_X52_Y73_N2
\wr_data[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|wr_data\(0),
	devoe => ww_devoe,
	o => \wr_data[0]~output_o\);

-- Location: IOOBUF_X72_Y73_N23
\wr_data[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|wr_data\(1),
	devoe => ww_devoe,
	o => \wr_data[1]~output_o\);

-- Location: IOOBUF_X83_Y73_N9
\wr_data[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|wr_data\(2),
	devoe => ww_devoe,
	o => \wr_data[2]~output_o\);

-- Location: IOOBUF_X69_Y73_N16
\wr_data[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|wr_data\(3),
	devoe => ww_devoe,
	o => \wr_data[3]~output_o\);

-- Location: IOOBUF_X72_Y73_N2
\wr_data[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|wr_data\(4),
	devoe => ww_devoe,
	o => \wr_data[4]~output_o\);

-- Location: IOOBUF_X60_Y73_N16
\wr_data[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|wr_data\(5),
	devoe => ww_devoe,
	o => \wr_data[5]~output_o\);

-- Location: IOOBUF_X67_Y73_N9
\done~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snake_ctrl_inst|done~q\,
	devoe => ww_devoe,
	o => \done~output_o\);

-- Location: IOIBUF_X0_Y36_N8
\clk~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk,
	o => \clk~input_o\);

-- Location: CLKCTRL_G2
\clk~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clk~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clk~inputclkctrl_outclk\);

-- Location: IOIBUF_X69_Y73_N22
\busy~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_busy,
	o => \busy~input_o\);

-- Location: IOIBUF_X72_Y73_N15
\init~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_init,
	o => \init~input_o\);

-- Location: IOIBUF_X74_Y73_N15
\move_head~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_move_head,
	o => \move_head~input_o\);

-- Location: LCCOMB_X74_Y69_N6
\snake_ctrl_inst|Selector45~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector45~0_combout\ = (!\init~input_o\ & (!\snake_ctrl_inst|state.IDLE~q\ & !\move_head~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \init~input_o\,
	datab => \snake_ctrl_inst|state.IDLE~q\,
	datac => \move_head~input_o\,
	combout => \snake_ctrl_inst|Selector45~0_combout\);

-- Location: IOIBUF_X0_Y36_N15
\res_n~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_res_n,
	o => \res_n~input_o\);

-- Location: CLKCTRL_G4
\res_n~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \res_n~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \res_n~inputclkctrl_outclk\);

-- Location: FF_X72_Y69_N27
\snake_ctrl_inst|state.WAIT_OLD_TAIL_TYPE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|rd~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|state.WAIT_OLD_TAIL_TYPE~q\);

-- Location: FF_X72_Y69_N31
\snake_ctrl_inst|state.CALCULATE_NEW_TAIL_DIRECTION\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snake_ctrl_inst|state.WAIT_OLD_TAIL_TYPE~q\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|state.CALCULATE_NEW_TAIL_DIRECTION~q\);

-- Location: LCCOMB_X69_Y69_N30
\snake_ctrl_inst|Selector49~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector49~0_combout\ = (\snake_ctrl_inst|state.CALCULATE_NEW_TAIL_DIRECTION~q\) # ((\busy~input_o\ & \snake_ctrl_inst|state.DRAW_NEW_TAIL~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \busy~input_o\,
	datac => \snake_ctrl_inst|state.DRAW_NEW_TAIL~q\,
	datad => \snake_ctrl_inst|state.CALCULATE_NEW_TAIL_DIRECTION~q\,
	combout => \snake_ctrl_inst|Selector49~0_combout\);

-- Location: FF_X69_Y69_N31
\snake_ctrl_inst|state.DRAW_NEW_TAIL\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector49~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|state.DRAW_NEW_TAIL~q\);

-- Location: LCCOMB_X72_Y69_N30
\snake_ctrl_inst|wr_pos.y[3]~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_pos.y[3]~8_combout\ = (!\snake_ctrl_inst|state.WAIT_OLD_TAIL_TYPE~q\ & (!\snake_ctrl_inst|state.CALCULATE_NEW_TAIL_DIRECTION~q\ & !\snake_ctrl_inst|state.SET_DONE~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|state.WAIT_OLD_TAIL_TYPE~q\,
	datac => \snake_ctrl_inst|state.CALCULATE_NEW_TAIL_DIRECTION~q\,
	datad => \snake_ctrl_inst|state.SET_DONE~q\,
	combout => \snake_ctrl_inst|wr_pos.y[3]~8_combout\);

-- Location: IOIBUF_X85_Y73_N1
\move_tail~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_move_tail,
	o => \move_tail~input_o\);

-- Location: LCCOMB_X74_Y69_N14
\snake_ctrl_inst|Selector40~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector40~2_combout\ = (!\snake_ctrl_inst|state.IDLE~q\ & ((\init~input_o\) # ((\move_head~input_o\) # (\move_tail~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \init~input_o\,
	datab => \snake_ctrl_inst|state.IDLE~q\,
	datac => \move_head~input_o\,
	datad => \move_tail~input_o\,
	combout => \snake_ctrl_inst|Selector40~2_combout\);

-- Location: LCCOMB_X75_Y69_N12
\snake_ctrl_inst|cur_length[2]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|cur_length[2]~2_combout\ = (\init~input_o\ & !\snake_ctrl_inst|state.IDLE~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \init~input_o\,
	datad => \snake_ctrl_inst|state.IDLE~q\,
	combout => \snake_ctrl_inst|cur_length[2]~2_combout\);

-- Location: LCCOMB_X74_Y69_N20
\snake_ctrl_inst|Selector40~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector40~5_combout\ = (\snake_ctrl_inst|Selector40~4_combout\ & (((\snake_ctrl_inst|state.PLACE_HEAD~q\ & !\snake_ctrl_inst|Selector40~3_combout\)))) # (!\snake_ctrl_inst|Selector40~4_combout\ & 
-- ((\snake_ctrl_inst|cur_length[2]~2_combout\) # ((\snake_ctrl_inst|state.PLACE_HEAD~q\ & !\snake_ctrl_inst|Selector40~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010011110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector40~4_combout\,
	datab => \snake_ctrl_inst|cur_length[2]~2_combout\,
	datac => \snake_ctrl_inst|state.PLACE_HEAD~q\,
	datad => \snake_ctrl_inst|Selector40~3_combout\,
	combout => \snake_ctrl_inst|Selector40~5_combout\);

-- Location: FF_X74_Y69_N21
\snake_ctrl_inst|state.PLACE_HEAD\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector40~5_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|state.PLACE_HEAD~q\);

-- Location: IOIBUF_X87_Y73_N8
\init_body_length[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_init_body_length(1),
	o => \init_body_length[1]~input_o\);

-- Location: FF_X75_Y69_N13
\snake_ctrl_inst|target_init_body_length[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \init_body_length[1]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snake_ctrl_inst|cur_length[2]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|target_init_body_length\(1));

-- Location: LCCOMB_X75_Y69_N24
\snake_ctrl_inst|Selector53~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector53~0_combout\ = (\snake_ctrl_inst|state.PLACE_BODY~q\ & !\snake_ctrl_inst|cur_length\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|state.PLACE_BODY~q\,
	datac => \snake_ctrl_inst|cur_length\(0),
	combout => \snake_ctrl_inst|Selector53~0_combout\);

-- Location: LCCOMB_X75_Y69_N14
\snake_ctrl_inst|cur_length[2]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|cur_length[2]~3_combout\ = (\busy~input_o\ & (!\snake_ctrl_inst|state.IDLE~q\ & (\init~input_o\))) # (!\busy~input_o\ & ((\snake_ctrl_inst|Selector131~0_combout\) # ((!\snake_ctrl_inst|state.IDLE~q\ & \init~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \busy~input_o\,
	datab => \snake_ctrl_inst|state.IDLE~q\,
	datac => \init~input_o\,
	datad => \snake_ctrl_inst|Selector131~0_combout\,
	combout => \snake_ctrl_inst|cur_length[2]~3_combout\);

-- Location: FF_X75_Y69_N25
\snake_ctrl_inst|cur_length[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector53~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|cur_length[2]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|cur_length\(0));

-- Location: LCCOMB_X75_Y69_N18
\snake_ctrl_inst|Selector52~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector52~0_combout\ = (\snake_ctrl_inst|state.PLACE_BODY~q\ & (\snake_ctrl_inst|cur_length\(1) $ (\snake_ctrl_inst|cur_length\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|state.PLACE_BODY~q\,
	datac => \snake_ctrl_inst|cur_length\(1),
	datad => \snake_ctrl_inst|cur_length\(0),
	combout => \snake_ctrl_inst|Selector52~0_combout\);

-- Location: FF_X75_Y69_N19
\snake_ctrl_inst|cur_length[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector52~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|cur_length[2]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|cur_length\(1));

-- Location: IOIBUF_X83_Y73_N1
\init_body_length[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_init_body_length(0),
	o => \init_body_length[0]~input_o\);

-- Location: FF_X75_Y69_N7
\snake_ctrl_inst|target_init_body_length[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \init_body_length[0]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snake_ctrl_inst|cur_length[2]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|target_init_body_length\(0));

-- Location: LCCOMB_X75_Y69_N6
\snake_ctrl_inst|Equal0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Equal0~0_combout\ = (\snake_ctrl_inst|target_init_body_length\(1) & (\snake_ctrl_inst|cur_length\(1) & (\snake_ctrl_inst|target_init_body_length\(0) $ (!\snake_ctrl_inst|cur_length\(0))))) # (!\snake_ctrl_inst|target_init_body_length\(1) 
-- & (!\snake_ctrl_inst|cur_length\(1) & (\snake_ctrl_inst|target_init_body_length\(0) $ (!\snake_ctrl_inst|cur_length\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000000001001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|target_init_body_length\(1),
	datab => \snake_ctrl_inst|cur_length\(1),
	datac => \snake_ctrl_inst|target_init_body_length\(0),
	datad => \snake_ctrl_inst|cur_length\(0),
	combout => \snake_ctrl_inst|Equal0~0_combout\);

-- Location: LCCOMB_X75_Y69_N0
\snake_ctrl_inst|Selector131~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector131~0_combout\ = (\snake_ctrl_inst|state.PLACE_BODY~q\ & ((!\snake_ctrl_inst|Equal0~0_combout\) # (!\snake_ctrl_inst|Equal0~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Equal0~1_combout\,
	datac => \snake_ctrl_inst|state.PLACE_BODY~q\,
	datad => \snake_ctrl_inst|Equal0~0_combout\,
	combout => \snake_ctrl_inst|Selector131~0_combout\);

-- Location: LCCOMB_X72_Y69_N18
\snake_ctrl_inst|Selector41~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector41~0_combout\ = (\snake_ctrl_inst|Selector131~0_combout\) # ((!\busy~input_o\ & \snake_ctrl_inst|state.PLACE_HEAD~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \busy~input_o\,
	datac => \snake_ctrl_inst|state.PLACE_HEAD~q\,
	datad => \snake_ctrl_inst|Selector131~0_combout\,
	combout => \snake_ctrl_inst|Selector41~0_combout\);

-- Location: FF_X72_Y69_N19
\snake_ctrl_inst|state.PLACE_BODY\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector41~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|state.PLACE_BODY~q\);

-- Location: LCCOMB_X75_Y69_N8
\snake_ctrl_inst|Selector51~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector51~0_combout\ = (\snake_ctrl_inst|state.PLACE_BODY~q\ & (\snake_ctrl_inst|cur_length\(2) $ (((\snake_ctrl_inst|cur_length\(1) & \snake_ctrl_inst|cur_length\(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|state.PLACE_BODY~q\,
	datab => \snake_ctrl_inst|cur_length\(1),
	datac => \snake_ctrl_inst|cur_length\(2),
	datad => \snake_ctrl_inst|cur_length\(0),
	combout => \snake_ctrl_inst|Selector51~0_combout\);

-- Location: FF_X75_Y69_N9
\snake_ctrl_inst|cur_length[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector51~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|cur_length[2]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|cur_length\(2));

-- Location: LCCOMB_X75_Y69_N16
\snake_ctrl_inst|Add4~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add4~0_combout\ = \snake_ctrl_inst|cur_length\(3) $ (((\snake_ctrl_inst|cur_length\(1) & (\snake_ctrl_inst|cur_length\(2) & \snake_ctrl_inst|cur_length\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_length\(3),
	datab => \snake_ctrl_inst|cur_length\(1),
	datac => \snake_ctrl_inst|cur_length\(2),
	datad => \snake_ctrl_inst|cur_length\(0),
	combout => \snake_ctrl_inst|Add4~0_combout\);

-- Location: LCCOMB_X75_Y69_N10
\snake_ctrl_inst|Selector50~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector50~0_combout\ = (\snake_ctrl_inst|state.PLACE_BODY~q\ & \snake_ctrl_inst|Add4~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \snake_ctrl_inst|state.PLACE_BODY~q\,
	datad => \snake_ctrl_inst|Add4~0_combout\,
	combout => \snake_ctrl_inst|Selector50~0_combout\);

-- Location: FF_X75_Y69_N11
\snake_ctrl_inst|cur_length[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector50~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|cur_length[2]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|cur_length\(3));

-- Location: IOIBUF_X87_Y73_N1
\init_body_length[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_init_body_length(2),
	o => \init_body_length[2]~input_o\);

-- Location: FF_X75_Y69_N31
\snake_ctrl_inst|target_init_body_length[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \init_body_length[2]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snake_ctrl_inst|cur_length[2]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|target_init_body_length\(2));

-- Location: IOIBUF_X79_Y73_N8
\init_body_length[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_init_body_length(3),
	o => \init_body_length[3]~input_o\);

-- Location: LCCOMB_X75_Y69_N28
\snake_ctrl_inst|target_init_body_length[3]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|target_init_body_length[3]~feeder_combout\ = \init_body_length[3]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \init_body_length[3]~input_o\,
	combout => \snake_ctrl_inst|target_init_body_length[3]~feeder_combout\);

-- Location: FF_X75_Y69_N29
\snake_ctrl_inst|target_init_body_length[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|target_init_body_length[3]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|cur_length[2]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|target_init_body_length\(3));

-- Location: LCCOMB_X75_Y69_N30
\snake_ctrl_inst|Equal0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Equal0~1_combout\ = (\snake_ctrl_inst|cur_length\(3) & (\snake_ctrl_inst|target_init_body_length\(3) & (\snake_ctrl_inst|cur_length\(2) $ (!\snake_ctrl_inst|target_init_body_length\(2))))) # (!\snake_ctrl_inst|cur_length\(3) & 
-- (!\snake_ctrl_inst|target_init_body_length\(3) & (\snake_ctrl_inst|cur_length\(2) $ (!\snake_ctrl_inst|target_init_body_length\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001001000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_length\(3),
	datab => \snake_ctrl_inst|cur_length\(2),
	datac => \snake_ctrl_inst|target_init_body_length\(2),
	datad => \snake_ctrl_inst|target_init_body_length\(3),
	combout => \snake_ctrl_inst|Equal0~1_combout\);

-- Location: LCCOMB_X75_Y69_N22
\snake_ctrl_inst|Selector40~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector40~0_combout\ = (\snake_ctrl_inst|Equal0~1_combout\ & (\snake_ctrl_inst|state.PLACE_BODY~q\ & \snake_ctrl_inst|Equal0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Equal0~1_combout\,
	datac => \snake_ctrl_inst|state.PLACE_BODY~q\,
	datad => \snake_ctrl_inst|Equal0~0_combout\,
	combout => \snake_ctrl_inst|Selector40~0_combout\);

-- Location: LCCOMB_X74_Y69_N16
\snake_ctrl_inst|Selector40~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector40~3_combout\ = (\snake_ctrl_inst|Selector40~1_combout\) # (((\snake_ctrl_inst|Selector40~2_combout\) # (\snake_ctrl_inst|Selector40~0_combout\)) # (!\snake_ctrl_inst|wr_pos.y[3]~8_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector40~1_combout\,
	datab => \snake_ctrl_inst|wr_pos.y[3]~8_combout\,
	datac => \snake_ctrl_inst|Selector40~2_combout\,
	datad => \snake_ctrl_inst|Selector40~0_combout\,
	combout => \snake_ctrl_inst|Selector40~3_combout\);

-- Location: LCCOMB_X74_Y69_N2
\snake_ctrl_inst|Selector42~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector42~0_combout\ = (\snake_ctrl_inst|Selector40~4_combout\ & (!\snake_ctrl_inst|Selector40~3_combout\ & (\snake_ctrl_inst|state.PLACE_TAIL~q\))) # (!\snake_ctrl_inst|Selector40~4_combout\ & ((\snake_ctrl_inst|Selector40~0_combout\) # 
-- ((!\snake_ctrl_inst|Selector40~3_combout\ & \snake_ctrl_inst|state.PLACE_TAIL~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector40~4_combout\,
	datab => \snake_ctrl_inst|Selector40~3_combout\,
	datac => \snake_ctrl_inst|state.PLACE_TAIL~q\,
	datad => \snake_ctrl_inst|Selector40~0_combout\,
	combout => \snake_ctrl_inst|Selector42~0_combout\);

-- Location: FF_X74_Y69_N3
\snake_ctrl_inst|state.PLACE_TAIL\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector42~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|state.PLACE_TAIL~q\);

-- Location: LCCOMB_X74_Y69_N4
\snake_ctrl_inst|target_movement_direction~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|target_movement_direction~12_combout\ = (!\init~input_o\ & (!\snake_ctrl_inst|state.IDLE~q\ & \move_head~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \init~input_o\,
	datab => \snake_ctrl_inst|state.IDLE~q\,
	datac => \move_head~input_o\,
	combout => \snake_ctrl_inst|target_movement_direction~12_combout\);

-- Location: LCCOMB_X74_Y69_N22
\snake_ctrl_inst|Selector43~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector43~0_combout\ = (\snake_ctrl_inst|Selector40~4_combout\ & (((\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\ & !\snake_ctrl_inst|Selector40~3_combout\)))) # (!\snake_ctrl_inst|Selector40~4_combout\ & 
-- ((\snake_ctrl_inst|target_movement_direction~12_combout\) # ((\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\ & !\snake_ctrl_inst|Selector40~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010011110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector40~4_combout\,
	datab => \snake_ctrl_inst|target_movement_direction~12_combout\,
	datac => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	datad => \snake_ctrl_inst|Selector40~3_combout\,
	combout => \snake_ctrl_inst|Selector43~0_combout\);

-- Location: FF_X74_Y69_N23
\snake_ctrl_inst|state.REPLACE_OLD_HEAD\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector43~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\);

-- Location: FF_X72_Y69_N23
\snake_ctrl_inst|state.DRAW_NEW_HEAD\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \ALT_INV_busy~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|state.DRAW_NEW_HEAD~q\);

-- Location: LCCOMB_X72_Y69_N12
\snake_ctrl_inst|Selector39~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector39~0_combout\ = (!\snake_ctrl_inst|state.DRAW_NEW_TAIL~q\ & (!\snake_ctrl_inst|state.PLACE_TAIL~q\ & !\snake_ctrl_inst|state.DRAW_NEW_HEAD~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|state.DRAW_NEW_TAIL~q\,
	datab => \snake_ctrl_inst|state.PLACE_TAIL~q\,
	datac => \snake_ctrl_inst|state.DRAW_NEW_HEAD~q\,
	combout => \snake_ctrl_inst|Selector39~0_combout\);

-- Location: LCCOMB_X72_Y69_N24
\snake_ctrl_inst|Selector39~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector39~1_combout\ = (!\snake_ctrl_inst|Selector39~0_combout\ & ((\snake_ctrl_inst|state.SET_DONE~q\) # (!\busy~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \busy~input_o\,
	datac => \snake_ctrl_inst|state.SET_DONE~q\,
	datad => \snake_ctrl_inst|Selector39~0_combout\,
	combout => \snake_ctrl_inst|Selector39~1_combout\);

-- Location: FF_X72_Y69_N25
\snake_ctrl_inst|state.SET_DONE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector39~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|state.SET_DONE~q\);

-- Location: LCCOMB_X74_Y69_N28
\snake_ctrl_inst|Selector38~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector38~0_combout\ = (!\snake_ctrl_inst|state.SET_DONE~q\ & ((\move_tail~input_o\) # (!\snake_ctrl_inst|Selector45~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector45~0_combout\,
	datab => \snake_ctrl_inst|state.SET_DONE~q\,
	datad => \move_tail~input_o\,
	combout => \snake_ctrl_inst|Selector38~0_combout\);

-- Location: FF_X74_Y69_N29
\snake_ctrl_inst|state.IDLE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector38~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|state.IDLE~q\);

-- Location: LCCOMB_X73_Y69_N14
\snake_ctrl_inst|Selector40~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector40~1_combout\ = (\snake_ctrl_inst|state.IDLE~q\ & !\busy~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|state.IDLE~q\,
	datac => \busy~input_o\,
	combout => \snake_ctrl_inst|Selector40~1_combout\);

-- Location: LCCOMB_X74_Y69_N10
\snake_ctrl_inst|Selector40~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector40~4_combout\ = ((!\snake_ctrl_inst|Selector40~1_combout\ & (!\snake_ctrl_inst|Selector40~2_combout\ & !\snake_ctrl_inst|Selector40~0_combout\))) # (!\snake_ctrl_inst|wr_pos.y[3]~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector40~1_combout\,
	datab => \snake_ctrl_inst|wr_pos.y[3]~8_combout\,
	datac => \snake_ctrl_inst|Selector40~2_combout\,
	datad => \snake_ctrl_inst|Selector40~0_combout\,
	combout => \snake_ctrl_inst|Selector40~4_combout\);

-- Location: LCCOMB_X74_Y69_N24
\snake_ctrl_inst|Selector45~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector45~1_combout\ = (\snake_ctrl_inst|Selector45~0_combout\ & (!\snake_ctrl_inst|Selector40~0_combout\ & \move_tail~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector45~0_combout\,
	datab => \snake_ctrl_inst|Selector40~0_combout\,
	datad => \move_tail~input_o\,
	combout => \snake_ctrl_inst|Selector45~1_combout\);

-- Location: LCCOMB_X74_Y69_N0
\snake_ctrl_inst|Selector45~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector45~2_combout\ = (\snake_ctrl_inst|Selector40~4_combout\ & (!\snake_ctrl_inst|Selector40~3_combout\ & (\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\))) # (!\snake_ctrl_inst|Selector40~4_combout\ & 
-- ((\snake_ctrl_inst|Selector45~1_combout\) # ((!\snake_ctrl_inst|Selector40~3_combout\ & \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector40~4_combout\,
	datab => \snake_ctrl_inst|Selector40~3_combout\,
	datac => \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\,
	datad => \snake_ctrl_inst|Selector45~1_combout\,
	combout => \snake_ctrl_inst|Selector45~2_combout\);

-- Location: FF_X74_Y69_N1
\snake_ctrl_inst|state.DELETE_OLD_TAIL\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector45~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\);

-- Location: FF_X72_Y69_N1
\snake_ctrl_inst|state.GET_OLD_TAIL_TYPE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \ALT_INV_busy~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|state.GET_OLD_TAIL_TYPE~q\);

-- Location: LCCOMB_X72_Y69_N26
\snake_ctrl_inst|rd~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd~0_combout\ = (!\busy~input_o\ & \snake_ctrl_inst|state.GET_OLD_TAIL_TYPE~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \busy~input_o\,
	datad => \snake_ctrl_inst|state.GET_OLD_TAIL_TYPE~q\,
	combout => \snake_ctrl_inst|rd~0_combout\);

-- Location: FF_X75_Y66_N1
\snake_ctrl_inst|rd\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snake_ctrl_inst|rd~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|rd~q\);

-- Location: IOIBUF_X79_Y73_N1
\player~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_player,
	o => \player~input_o\);

-- Location: FF_X74_Y69_N19
\snake_ctrl_inst|cur_player\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \player~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snake_ctrl_inst|ALT_INV_state.IDLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|cur_player~q\);

-- Location: IOIBUF_X62_Y73_N22
\init_head_direction.LEFT~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_init_head_direction.LEFT\,
	o => \init_head_direction.LEFT~input_o\);

-- Location: LCCOMB_X74_Y69_N26
\snake_ctrl_inst|snake_pos[1].head_position.x[2]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|snake_pos[1].head_position.x[2]~2_combout\ = ((\snake_ctrl_inst|state.IDLE~q\) # (!\player~input_o\)) # (!\init~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \init~input_o\,
	datac => \player~input_o\,
	datad => \snake_ctrl_inst|state.IDLE~q\,
	combout => \snake_ctrl_inst|snake_pos[1].head_position.x[2]~2_combout\);

-- Location: LCCOMB_X70_Y69_N12
\snake_ctrl_inst|snake_pos[1].tail_direction.LEFT~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|snake_pos[1].tail_direction.LEFT~0_combout\ = (\snake_ctrl_inst|snake_pos[1].head_position.x[2]~2_combout\ & ((\snake_ctrl_inst|snake_pos[1].tail_direction.LEFT~q\))) # (!\snake_ctrl_inst|snake_pos[1].head_position.x[2]~2_combout\ & 
-- (\init_head_direction.LEFT~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \init_head_direction.LEFT~input_o\,
	datac => \snake_ctrl_inst|snake_pos[1].tail_direction.LEFT~q\,
	datad => \snake_ctrl_inst|snake_pos[1].head_position.x[2]~2_combout\,
	combout => \snake_ctrl_inst|snake_pos[1].tail_direction.LEFT~0_combout\);

-- Location: IOIBUF_X65_Y73_N8
\rd_data[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd_data(0),
	o => \rd_data[0]~input_o\);

-- Location: IOIBUF_X60_Y73_N8
\rd_data[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd_data(2),
	o => \rd_data[2]~input_o\);

-- Location: IOIBUF_X65_Y73_N22
\rd_data[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd_data(1),
	o => \rd_data[1]~input_o\);

-- Location: IOIBUF_X60_Y73_N22
\rd_data[4]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd_data(4),
	o => \rd_data[4]~input_o\);

-- Location: LCCOMB_X68_Y69_N14
\snake_ctrl_inst|Selector35~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector35~2_combout\ = (\rd_data[0]~input_o\ & (!\rd_data[2]~input_o\ & (\rd_data[1]~input_o\ & !\rd_data[4]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rd_data[0]~input_o\,
	datab => \rd_data[2]~input_o\,
	datac => \rd_data[1]~input_o\,
	datad => \rd_data[4]~input_o\,
	combout => \snake_ctrl_inst|Selector35~2_combout\);

-- Location: IOIBUF_X62_Y73_N15
\rd_data[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd_data(3),
	o => \rd_data[3]~input_o\);

-- Location: IOIBUF_X58_Y73_N8
\init_head_direction.DOWN~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_init_head_direction.DOWN\,
	o => \init_head_direction.DOWN~input_o\);

-- Location: LCCOMB_X74_Y69_N12
\snake_ctrl_inst|snake_pos[0].head_position.y[0]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|snake_pos[0].head_position.y[0]~2_combout\ = ((\player~input_o\) # (\snake_ctrl_inst|state.IDLE~q\)) # (!\init~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \init~input_o\,
	datac => \player~input_o\,
	datad => \snake_ctrl_inst|state.IDLE~q\,
	combout => \snake_ctrl_inst|snake_pos[0].head_position.y[0]~2_combout\);

-- Location: LCCOMB_X69_Y69_N0
\snake_ctrl_inst|snake_pos[0].tail_direction.DOWN~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|snake_pos[0].tail_direction.DOWN~0_combout\ = (\snake_ctrl_inst|snake_pos[0].head_position.y[0]~2_combout\ & ((\snake_ctrl_inst|snake_pos[0].tail_direction.DOWN~q\))) # (!\snake_ctrl_inst|snake_pos[0].head_position.y[0]~2_combout\ & 
-- (\init_head_direction.DOWN~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \init_head_direction.DOWN~input_o\,
	datac => \snake_ctrl_inst|snake_pos[0].tail_direction.DOWN~q\,
	datad => \snake_ctrl_inst|snake_pos[0].head_position.y[0]~2_combout\,
	combout => \snake_ctrl_inst|snake_pos[0].tail_direction.DOWN~0_combout\);

-- Location: IOIBUF_X58_Y73_N22
\init_head_direction.RIGHT~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_init_head_direction.RIGHT\,
	o => \init_head_direction.RIGHT~input_o\);

-- Location: LCCOMB_X69_Y69_N2
\snake_ctrl_inst|snake_pos[0].tail_direction.RIGHT~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|snake_pos[0].tail_direction.RIGHT~0_combout\ = (\snake_ctrl_inst|snake_pos[0].head_position.y[0]~2_combout\ & ((\snake_ctrl_inst|snake_pos[0].tail_direction.RIGHT~q\))) # (!\snake_ctrl_inst|snake_pos[0].head_position.y[0]~2_combout\ & 
-- (\init_head_direction.RIGHT~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \init_head_direction.RIGHT~input_o\,
	datac => \snake_ctrl_inst|snake_pos[0].tail_direction.RIGHT~q\,
	datad => \snake_ctrl_inst|snake_pos[0].head_position.y[0]~2_combout\,
	combout => \snake_ctrl_inst|snake_pos[0].tail_direction.RIGHT~0_combout\);

-- Location: LCCOMB_X68_Y69_N16
\snake_ctrl_inst|Mux4~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Mux4~0_combout\ = (!\rd_data[0]~input_o\ & (\rd_data[2]~input_o\ & (!\rd_data[1]~input_o\ & \rd_data[4]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rd_data[0]~input_o\,
	datab => \rd_data[2]~input_o\,
	datac => \rd_data[1]~input_o\,
	datad => \rd_data[4]~input_o\,
	combout => \snake_ctrl_inst|Mux4~0_combout\);

-- Location: IOIBUF_X72_Y73_N8
\init_head_direction.UP~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_init_head_direction.UP\,
	o => \init_head_direction.UP~input_o\);

-- Location: LCCOMB_X73_Y68_N12
\snake_ctrl_inst|Selector116~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector116~0_combout\ = (\snake_ctrl_inst|snake_pos[0].head_position.y[0]~2_combout\ & (!\snake_ctrl_inst|snake_pos[0].tail_direction.UP~q\)) # (!\snake_ctrl_inst|snake_pos[0].head_position.y[0]~2_combout\ & 
-- ((\init_head_direction.UP~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[0].tail_direction.UP~q\,
	datac => \init_head_direction.UP~input_o\,
	datad => \snake_ctrl_inst|snake_pos[0].head_position.y[0]~2_combout\,
	combout => \snake_ctrl_inst|Selector116~0_combout\);

-- Location: LCCOMB_X68_Y69_N30
\snake_ctrl_inst|Selector34~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector34~2_combout\ = (\rd_data[0]~input_o\ & (!\rd_data[2]~input_o\ & (\rd_data[1]~input_o\ & !\rd_data[4]~input_o\))) # (!\rd_data[0]~input_o\ & (\rd_data[2]~input_o\ & (!\rd_data[1]~input_o\ & \rd_data[4]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rd_data[0]~input_o\,
	datab => \rd_data[2]~input_o\,
	datac => \rd_data[1]~input_o\,
	datad => \rd_data[4]~input_o\,
	combout => \snake_ctrl_inst|Selector34~2_combout\);

-- Location: LCCOMB_X68_Y69_N20
\snake_ctrl_inst|Selector34~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector34~1_combout\ = (\rd_data[2]~input_o\ & (!\rd_data[1]~input_o\ & !\rd_data[0]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \rd_data[2]~input_o\,
	datac => \rd_data[1]~input_o\,
	datad => \rd_data[0]~input_o\,
	combout => \snake_ctrl_inst|Selector34~1_combout\);

-- Location: LCCOMB_X68_Y69_N26
\snake_ctrl_inst|Selector34~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector34~4_combout\ = (\rd_data[3]~input_o\) # ((!\snake_ctrl_inst|Selector34~2_combout\ & \snake_ctrl_inst|tail_position~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector34~2_combout\,
	datab => \snake_ctrl_inst|tail_position~0_combout\,
	datad => \rd_data[3]~input_o\,
	combout => \snake_ctrl_inst|Selector34~4_combout\);

-- Location: LCCOMB_X68_Y69_N24
\snake_ctrl_inst|Selector34~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector34~3_combout\ = (\snake_ctrl_inst|wr_data~4_combout\ & (((\snake_ctrl_inst|Selector34~2_combout\ & \rd_data[3]~input_o\)) # (!\snake_ctrl_inst|tail_position~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector34~2_combout\,
	datab => \snake_ctrl_inst|tail_position~0_combout\,
	datac => \snake_ctrl_inst|wr_data~4_combout\,
	datad => \rd_data[3]~input_o\,
	combout => \snake_ctrl_inst|Selector34~3_combout\);

-- Location: LCCOMB_X68_Y69_N6
\snake_ctrl_inst|Selector34~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector34~6_combout\ = (\snake_ctrl_inst|Selector34~2_combout\ & ((\snake_ctrl_inst|Selector34~4_combout\ & (\snake_ctrl_inst|Selector34~1_combout\)) # (!\snake_ctrl_inst|Selector34~4_combout\ & 
-- ((\snake_ctrl_inst|Selector34~3_combout\))))) # (!\snake_ctrl_inst|Selector34~2_combout\ & (\snake_ctrl_inst|Selector34~3_combout\ & ((\snake_ctrl_inst|Selector34~4_combout\) # (!\snake_ctrl_inst|Selector34~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector34~2_combout\,
	datab => \snake_ctrl_inst|Selector34~1_combout\,
	datac => \snake_ctrl_inst|Selector34~4_combout\,
	datad => \snake_ctrl_inst|Selector34~3_combout\,
	combout => \snake_ctrl_inst|Selector34~6_combout\);

-- Location: LCCOMB_X68_Y69_N4
\snake_ctrl_inst|Selector34~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector34~5_combout\ = (\snake_ctrl_inst|Selector34~2_combout\ & (!\snake_ctrl_inst|Selector34~1_combout\ & ((\snake_ctrl_inst|Selector34~3_combout\)))) # (!\snake_ctrl_inst|Selector34~2_combout\ & (\snake_ctrl_inst|Selector34~1_combout\ 
-- & ((\snake_ctrl_inst|Selector34~3_combout\) # (!\snake_ctrl_inst|Selector34~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector34~2_combout\,
	datab => \snake_ctrl_inst|Selector34~1_combout\,
	datac => \snake_ctrl_inst|Selector34~4_combout\,
	datad => \snake_ctrl_inst|Selector34~3_combout\,
	combout => \snake_ctrl_inst|Selector34~5_combout\);

-- Location: LCCOMB_X68_Y69_N10
\snake_ctrl_inst|Selector34~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector34~0_combout\ = (\snake_ctrl_inst|wr_data~5_combout\ & ((\rd_data[3]~input_o\) # (!\snake_ctrl_inst|Mux4~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rd_data[3]~input_o\,
	datab => \snake_ctrl_inst|Mux4~0_combout\,
	datad => \snake_ctrl_inst|wr_data~5_combout\,
	combout => \snake_ctrl_inst|Selector34~0_combout\);

-- Location: LCCOMB_X68_Y69_N8
\snake_ctrl_inst|Selector34~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector34~7_combout\ = (\snake_ctrl_inst|Selector34~6_combout\ & ((\snake_ctrl_inst|wr_data~6_combout\) # ((!\snake_ctrl_inst|Selector34~5_combout\ & \snake_ctrl_inst|Selector34~0_combout\)))) # (!\snake_ctrl_inst|Selector34~6_combout\ & 
-- (((\snake_ctrl_inst|Selector34~0_combout\) # (!\snake_ctrl_inst|Selector34~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111110001101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector34~6_combout\,
	datab => \snake_ctrl_inst|wr_data~6_combout\,
	datac => \snake_ctrl_inst|Selector34~5_combout\,
	datad => \snake_ctrl_inst|Selector34~0_combout\,
	combout => \snake_ctrl_inst|Selector34~7_combout\);

-- Location: LCCOMB_X73_Y68_N6
\snake_ctrl_inst|Selector116~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector116~1_combout\ = (\snake_ctrl_inst|state.CALCULATE_NEW_TAIL_DIRECTION~q\ & ((\snake_ctrl_inst|cur_player~q\ & (!\snake_ctrl_inst|Selector116~0_combout\)) # (!\snake_ctrl_inst|cur_player~q\ & 
-- ((!\snake_ctrl_inst|Selector34~7_combout\))))) # (!\snake_ctrl_inst|state.CALCULATE_NEW_TAIL_DIRECTION~q\ & (!\snake_ctrl_inst|Selector116~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100110101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector116~0_combout\,
	datab => \snake_ctrl_inst|Selector34~7_combout\,
	datac => \snake_ctrl_inst|state.CALCULATE_NEW_TAIL_DIRECTION~q\,
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|Selector116~1_combout\);

-- Location: FF_X73_Y68_N7
\snake_ctrl_inst|snake_pos[0].tail_direction.UP\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector116~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].tail_direction.UP~q\);

-- Location: LCCOMB_X73_Y68_N26
\snake_ctrl_inst|Selector88~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector88~0_combout\ = (\snake_ctrl_inst|snake_pos[1].head_position.x[2]~2_combout\ & (!\snake_ctrl_inst|snake_pos[1].tail_direction.UP~q\)) # (!\snake_ctrl_inst|snake_pos[1].head_position.x[2]~2_combout\ & 
-- ((\init_head_direction.UP~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|snake_pos[1].tail_direction.UP~q\,
	datac => \init_head_direction.UP~input_o\,
	datad => \snake_ctrl_inst|snake_pos[1].head_position.x[2]~2_combout\,
	combout => \snake_ctrl_inst|Selector88~0_combout\);

-- Location: LCCOMB_X73_Y68_N4
\snake_ctrl_inst|Selector88~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector88~1_combout\ = (\snake_ctrl_inst|state.CALCULATE_NEW_TAIL_DIRECTION~q\ & ((\snake_ctrl_inst|cur_player~q\ & (!\snake_ctrl_inst|Selector34~7_combout\)) # (!\snake_ctrl_inst|cur_player~q\ & 
-- ((!\snake_ctrl_inst|Selector88~0_combout\))))) # (!\snake_ctrl_inst|state.CALCULATE_NEW_TAIL_DIRECTION~q\ & (((!\snake_ctrl_inst|Selector88~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100011100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector34~7_combout\,
	datab => \snake_ctrl_inst|state.CALCULATE_NEW_TAIL_DIRECTION~q\,
	datac => \snake_ctrl_inst|Selector88~0_combout\,
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|Selector88~1_combout\);

-- Location: FF_X73_Y68_N5
\snake_ctrl_inst|snake_pos[1].tail_direction.UP\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector88~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].tail_direction.UP~q\);

-- Location: LCCOMB_X73_Y68_N0
\snake_ctrl_inst|wr_data~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_data~4_combout\ = (\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].tail_direction.UP~q\))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].tail_direction.UP~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[0].tail_direction.UP~q\,
	datac => \snake_ctrl_inst|snake_pos[1].tail_direction.UP~q\,
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|wr_data~4_combout\);

-- Location: LCCOMB_X68_Y69_N0
\snake_ctrl_inst|Selector37~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector37~0_combout\ = (\snake_ctrl_inst|Mux4~0_combout\ & ((\rd_data[3]~input_o\ & ((!\snake_ctrl_inst|wr_data~4_combout\))) # (!\rd_data[3]~input_o\ & (\snake_ctrl_inst|wr_data~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rd_data[3]~input_o\,
	datab => \snake_ctrl_inst|wr_data~5_combout\,
	datac => \snake_ctrl_inst|Mux4~0_combout\,
	datad => \snake_ctrl_inst|wr_data~4_combout\,
	combout => \snake_ctrl_inst|Selector37~0_combout\);

-- Location: LCCOMB_X69_Y69_N26
\snake_ctrl_inst|Selector37~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector37~1_combout\ = (\snake_ctrl_inst|Selector37~0_combout\) # ((\snake_ctrl_inst|wr_data~6_combout\ & (\rd_data[3]~input_o\ & \snake_ctrl_inst|Selector35~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector37~0_combout\,
	datab => \snake_ctrl_inst|wr_data~6_combout\,
	datac => \rd_data[3]~input_o\,
	datad => \snake_ctrl_inst|Selector35~2_combout\,
	combout => \snake_ctrl_inst|Selector37~1_combout\);

-- Location: LCCOMB_X69_Y69_N8
\snake_ctrl_inst|Selector116~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector116~2_combout\ = (!\snake_ctrl_inst|cur_player~q\ & \snake_ctrl_inst|state.CALCULATE_NEW_TAIL_DIRECTION~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \snake_ctrl_inst|cur_player~q\,
	datad => \snake_ctrl_inst|state.CALCULATE_NEW_TAIL_DIRECTION~q\,
	combout => \snake_ctrl_inst|Selector116~2_combout\);

-- Location: FF_X69_Y69_N3
\snake_ctrl_inst|snake_pos[0].tail_direction.RIGHT\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|snake_pos[0].tail_direction.RIGHT~0_combout\,
	asdata => \snake_ctrl_inst|Selector37~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \snake_ctrl_inst|Selector116~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].tail_direction.RIGHT~q\);

-- Location: LCCOMB_X70_Y69_N18
\snake_ctrl_inst|snake_pos[1].tail_direction.RIGHT~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|snake_pos[1].tail_direction.RIGHT~0_combout\ = (\snake_ctrl_inst|snake_pos[1].head_position.x[2]~2_combout\ & ((\snake_ctrl_inst|snake_pos[1].tail_direction.RIGHT~q\))) # (!\snake_ctrl_inst|snake_pos[1].head_position.x[2]~2_combout\ & 
-- (\init_head_direction.RIGHT~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \init_head_direction.RIGHT~input_o\,
	datac => \snake_ctrl_inst|snake_pos[1].tail_direction.RIGHT~q\,
	datad => \snake_ctrl_inst|snake_pos[1].head_position.x[2]~2_combout\,
	combout => \snake_ctrl_inst|snake_pos[1].tail_direction.RIGHT~0_combout\);

-- Location: LCCOMB_X70_Y69_N2
\snake_ctrl_inst|Selector88~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector88~2_combout\ = (\snake_ctrl_inst|cur_player~q\ & \snake_ctrl_inst|state.CALCULATE_NEW_TAIL_DIRECTION~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \snake_ctrl_inst|cur_player~q\,
	datad => \snake_ctrl_inst|state.CALCULATE_NEW_TAIL_DIRECTION~q\,
	combout => \snake_ctrl_inst|Selector88~2_combout\);

-- Location: FF_X70_Y69_N19
\snake_ctrl_inst|snake_pos[1].tail_direction.RIGHT\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|snake_pos[1].tail_direction.RIGHT~0_combout\,
	asdata => \snake_ctrl_inst|Selector37~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \snake_ctrl_inst|Selector88~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].tail_direction.RIGHT~q\);

-- Location: LCCOMB_X69_Y69_N28
\snake_ctrl_inst|wr_data~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_data~6_combout\ = (\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].tail_direction.RIGHT~q\))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].tail_direction.RIGHT~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|snake_pos[0].tail_direction.RIGHT~q\,
	datac => \snake_ctrl_inst|cur_player~q\,
	datad => \snake_ctrl_inst|snake_pos[1].tail_direction.RIGHT~q\,
	combout => \snake_ctrl_inst|wr_data~6_combout\);

-- Location: LCCOMB_X68_Y69_N18
\snake_ctrl_inst|Selector35~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector35~0_combout\ = (\rd_data[3]~input_o\ & (((\rd_data[4]~input_o\ & \snake_ctrl_inst|tail_position~0_combout\)))) # (!\rd_data[3]~input_o\ & (\snake_ctrl_inst|wr_data~6_combout\ & (!\rd_data[4]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rd_data[3]~input_o\,
	datab => \snake_ctrl_inst|wr_data~6_combout\,
	datac => \rd_data[4]~input_o\,
	datad => \snake_ctrl_inst|tail_position~0_combout\,
	combout => \snake_ctrl_inst|Selector35~0_combout\);

-- Location: LCCOMB_X68_Y69_N12
\snake_ctrl_inst|Selector35~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector35~1_combout\ = (!\rd_data[0]~input_o\ & (\rd_data[2]~input_o\ & (!\rd_data[1]~input_o\ & \snake_ctrl_inst|Selector35~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rd_data[0]~input_o\,
	datab => \rd_data[2]~input_o\,
	datac => \rd_data[1]~input_o\,
	datad => \snake_ctrl_inst|Selector35~0_combout\,
	combout => \snake_ctrl_inst|Selector35~1_combout\);

-- Location: LCCOMB_X69_Y69_N22
\snake_ctrl_inst|Selector35~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector35~3_combout\ = (\snake_ctrl_inst|Selector35~1_combout\) # ((\snake_ctrl_inst|wr_data~5_combout\ & (!\rd_data[3]~input_o\ & \snake_ctrl_inst|Selector35~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector35~1_combout\,
	datab => \snake_ctrl_inst|wr_data~5_combout\,
	datac => \rd_data[3]~input_o\,
	datad => \snake_ctrl_inst|Selector35~2_combout\,
	combout => \snake_ctrl_inst|Selector35~3_combout\);

-- Location: FF_X69_Y69_N1
\snake_ctrl_inst|snake_pos[0].tail_direction.DOWN\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|snake_pos[0].tail_direction.DOWN~0_combout\,
	asdata => \snake_ctrl_inst|Selector35~3_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \snake_ctrl_inst|Selector116~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].tail_direction.DOWN~q\);

-- Location: LCCOMB_X70_Y69_N24
\snake_ctrl_inst|snake_pos[1].tail_direction.DOWN~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|snake_pos[1].tail_direction.DOWN~0_combout\ = (\snake_ctrl_inst|snake_pos[1].head_position.x[2]~2_combout\ & ((\snake_ctrl_inst|snake_pos[1].tail_direction.DOWN~q\))) # (!\snake_ctrl_inst|snake_pos[1].head_position.x[2]~2_combout\ & 
-- (\init_head_direction.DOWN~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \init_head_direction.DOWN~input_o\,
	datac => \snake_ctrl_inst|snake_pos[1].tail_direction.DOWN~q\,
	datad => \snake_ctrl_inst|snake_pos[1].head_position.x[2]~2_combout\,
	combout => \snake_ctrl_inst|snake_pos[1].tail_direction.DOWN~0_combout\);

-- Location: FF_X70_Y69_N25
\snake_ctrl_inst|snake_pos[1].tail_direction.DOWN\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|snake_pos[1].tail_direction.DOWN~0_combout\,
	asdata => \snake_ctrl_inst|Selector35~3_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \snake_ctrl_inst|Selector88~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].tail_direction.DOWN~q\);

-- Location: LCCOMB_X69_Y69_N24
\snake_ctrl_inst|wr_data~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_data~5_combout\ = (\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].tail_direction.DOWN~q\))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].tail_direction.DOWN~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|snake_pos[0].tail_direction.DOWN~q\,
	datac => \snake_ctrl_inst|cur_player~q\,
	datad => \snake_ctrl_inst|snake_pos[1].tail_direction.DOWN~q\,
	combout => \snake_ctrl_inst|wr_data~5_combout\);

-- Location: LCCOMB_X68_Y69_N2
\snake_ctrl_inst|Selector36~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector36~0_combout\ = (!\rd_data[0]~input_o\ & (\rd_data[2]~input_o\ & (!\rd_data[1]~input_o\ & !\rd_data[4]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rd_data[0]~input_o\,
	datab => \rd_data[2]~input_o\,
	datac => \rd_data[1]~input_o\,
	datad => \rd_data[4]~input_o\,
	combout => \snake_ctrl_inst|Selector36~0_combout\);

-- Location: LCCOMB_X68_Y69_N28
\snake_ctrl_inst|Selector36~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector36~1_combout\ = (\snake_ctrl_inst|Selector36~0_combout\ & ((\rd_data[3]~input_o\ & (\snake_ctrl_inst|wr_data~5_combout\)) # (!\rd_data[3]~input_o\ & ((!\snake_ctrl_inst|wr_data~4_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000011010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rd_data[3]~input_o\,
	datab => \snake_ctrl_inst|wr_data~5_combout\,
	datac => \snake_ctrl_inst|Selector36~0_combout\,
	datad => \snake_ctrl_inst|wr_data~4_combout\,
	combout => \snake_ctrl_inst|Selector36~1_combout\);

-- Location: LCCOMB_X69_Y69_N4
\snake_ctrl_inst|Selector36~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector36~2_combout\ = (\snake_ctrl_inst|Selector36~1_combout\) # ((\snake_ctrl_inst|Selector35~2_combout\ & (\rd_data[3]~input_o\ & \snake_ctrl_inst|tail_position~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector35~2_combout\,
	datab => \rd_data[3]~input_o\,
	datac => \snake_ctrl_inst|Selector36~1_combout\,
	datad => \snake_ctrl_inst|tail_position~0_combout\,
	combout => \snake_ctrl_inst|Selector36~2_combout\);

-- Location: FF_X70_Y69_N13
\snake_ctrl_inst|snake_pos[1].tail_direction.LEFT\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|snake_pos[1].tail_direction.LEFT~0_combout\,
	asdata => \snake_ctrl_inst|Selector36~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \snake_ctrl_inst|Selector88~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].tail_direction.LEFT~q\);

-- Location: LCCOMB_X69_Y69_N20
\snake_ctrl_inst|snake_pos[0].tail_direction.LEFT~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|snake_pos[0].tail_direction.LEFT~0_combout\ = (\snake_ctrl_inst|snake_pos[0].head_position.y[0]~2_combout\ & ((\snake_ctrl_inst|snake_pos[0].tail_direction.LEFT~q\))) # (!\snake_ctrl_inst|snake_pos[0].head_position.y[0]~2_combout\ & 
-- (\init_head_direction.LEFT~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \init_head_direction.LEFT~input_o\,
	datac => \snake_ctrl_inst|snake_pos[0].tail_direction.LEFT~q\,
	datad => \snake_ctrl_inst|snake_pos[0].head_position.y[0]~2_combout\,
	combout => \snake_ctrl_inst|snake_pos[0].tail_direction.LEFT~0_combout\);

-- Location: FF_X69_Y69_N21
\snake_ctrl_inst|snake_pos[0].tail_direction.LEFT\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|snake_pos[0].tail_direction.LEFT~0_combout\,
	asdata => \snake_ctrl_inst|Selector36~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \snake_ctrl_inst|Selector116~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].tail_direction.LEFT~q\);

-- Location: LCCOMB_X69_Y69_N16
\snake_ctrl_inst|tail_position~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|tail_position~0_combout\ = (\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[1].tail_direction.LEFT~q\)) # (!\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[0].tail_direction.LEFT~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_player~q\,
	datac => \snake_ctrl_inst|snake_pos[1].tail_direction.LEFT~q\,
	datad => \snake_ctrl_inst|snake_pos[0].tail_direction.LEFT~q\,
	combout => \snake_ctrl_inst|tail_position~0_combout\);

-- Location: LCCOMB_X69_Y69_N12
\snake_ctrl_inst|Selector115~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector115~3_combout\ = (\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\ & (!\snake_ctrl_inst|tail_position~0_combout\ & (!\snake_ctrl_inst|wr_data~5_combout\ & !\snake_ctrl_inst|wr_data~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\,
	datab => \snake_ctrl_inst|tail_position~0_combout\,
	datac => \snake_ctrl_inst|wr_data~5_combout\,
	datad => \snake_ctrl_inst|wr_data~6_combout\,
	combout => \snake_ctrl_inst|Selector115~3_combout\);

-- Location: LCCOMB_X74_Y67_N16
\snake_ctrl_inst|snake_pos[1].tail_position.y[4]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|snake_pos[1].tail_position.y[4]~0_combout\ = (\snake_ctrl_inst|cur_player~q\ & (!\busy~input_o\ & ((\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\) # (\snake_ctrl_inst|state.PLACE_TAIL~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\,
	datab => \snake_ctrl_inst|cur_player~q\,
	datac => \busy~input_o\,
	datad => \snake_ctrl_inst|state.PLACE_TAIL~q\,
	combout => \snake_ctrl_inst|snake_pos[1].tail_position.y[4]~0_combout\);

-- Location: FF_X74_Y66_N1
\snake_ctrl_inst|snake_pos[1].tail_position.y[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snake_ctrl_inst|Selector115~5_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snake_ctrl_inst|snake_pos[1].tail_position.y[4]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].tail_position.y\(0));

-- Location: LCCOMB_X74_Y66_N0
\snake_ctrl_inst|rd_pos~40\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~40_combout\ = (\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].tail_position.y\(0)))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].tail_position.y\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[0].tail_position.y\(0),
	datab => \snake_ctrl_inst|cur_player~q\,
	datac => \snake_ctrl_inst|snake_pos[1].tail_position.y\(0),
	combout => \snake_ctrl_inst|rd_pos~40_combout\);

-- Location: LCCOMB_X73_Y65_N4
\snake_ctrl_inst|Add10~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add10~0_combout\ = \snake_ctrl_inst|rd_pos~40_combout\ $ (VCC)
-- \snake_ctrl_inst|Add10~1\ = CARRY(\snake_ctrl_inst|rd_pos~40_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|rd_pos~40_combout\,
	datad => VCC,
	combout => \snake_ctrl_inst|Add10~0_combout\,
	cout => \snake_ctrl_inst|Add10~1\);

-- Location: LCCOMB_X73_Y65_N6
\snake_ctrl_inst|Add10~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add10~2_combout\ = (\snake_ctrl_inst|rd_pos~41_combout\ & (!\snake_ctrl_inst|Add10~1\)) # (!\snake_ctrl_inst|rd_pos~41_combout\ & ((\snake_ctrl_inst|Add10~1\) # (GND)))
-- \snake_ctrl_inst|Add10~3\ = CARRY((!\snake_ctrl_inst|Add10~1\) # (!\snake_ctrl_inst|rd_pos~41_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|rd_pos~41_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|Add10~1\,
	combout => \snake_ctrl_inst|Add10~2_combout\,
	cout => \snake_ctrl_inst|Add10~3\);

-- Location: LCCOMB_X69_Y69_N18
\snake_ctrl_inst|Selector112~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector112~0_combout\ = (\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\ & (!\snake_ctrl_inst|tail_position~0_combout\ & (\snake_ctrl_inst|wr_data~5_combout\ & !\snake_ctrl_inst|wr_data~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\,
	datab => \snake_ctrl_inst|tail_position~0_combout\,
	datac => \snake_ctrl_inst|wr_data~5_combout\,
	datad => \snake_ctrl_inst|wr_data~6_combout\,
	combout => \snake_ctrl_inst|Selector112~0_combout\);

-- Location: LCCOMB_X73_Y65_N8
\snake_ctrl_inst|Add10~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add10~4_combout\ = (\snake_ctrl_inst|rd_pos~42_combout\ & (\snake_ctrl_inst|Add10~3\ $ (GND))) # (!\snake_ctrl_inst|rd_pos~42_combout\ & (!\snake_ctrl_inst|Add10~3\ & VCC))
-- \snake_ctrl_inst|Add10~5\ = CARRY((\snake_ctrl_inst|rd_pos~42_combout\ & !\snake_ctrl_inst|Add10~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|rd_pos~42_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|Add10~3\,
	combout => \snake_ctrl_inst|Add10~4_combout\,
	cout => \snake_ctrl_inst|Add10~5\);

-- Location: LCCOMB_X74_Y65_N14
\snake_ctrl_inst|Add9~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add9~0_combout\ = \snake_ctrl_inst|rd_pos~40_combout\ $ (VCC)
-- \snake_ctrl_inst|Add9~1\ = CARRY(\snake_ctrl_inst|rd_pos~40_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|rd_pos~40_combout\,
	datad => VCC,
	combout => \snake_ctrl_inst|Add9~0_combout\,
	cout => \snake_ctrl_inst|Add9~1\);

-- Location: LCCOMB_X74_Y65_N16
\snake_ctrl_inst|Add9~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add9~2_combout\ = (\snake_ctrl_inst|rd_pos~41_combout\ & (\snake_ctrl_inst|Add9~1\ & VCC)) # (!\snake_ctrl_inst|rd_pos~41_combout\ & (!\snake_ctrl_inst|Add9~1\))
-- \snake_ctrl_inst|Add9~3\ = CARRY((!\snake_ctrl_inst|rd_pos~41_combout\ & !\snake_ctrl_inst|Add9~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|rd_pos~41_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|Add9~1\,
	combout => \snake_ctrl_inst|Add9~2_combout\,
	cout => \snake_ctrl_inst|Add9~3\);

-- Location: LCCOMB_X74_Y65_N18
\snake_ctrl_inst|Add9~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add9~4_combout\ = (\snake_ctrl_inst|rd_pos~42_combout\ & ((GND) # (!\snake_ctrl_inst|Add9~3\))) # (!\snake_ctrl_inst|rd_pos~42_combout\ & (\snake_ctrl_inst|Add9~3\ $ (GND)))
-- \snake_ctrl_inst|Add9~5\ = CARRY((\snake_ctrl_inst|rd_pos~42_combout\) # (!\snake_ctrl_inst|Add9~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|rd_pos~42_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|Add9~3\,
	combout => \snake_ctrl_inst|Add9~4_combout\,
	cout => \snake_ctrl_inst|Add9~5\);

-- Location: LCCOMB_X74_Y65_N20
\snake_ctrl_inst|Add9~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add9~6_combout\ = (\snake_ctrl_inst|rd_pos~43_combout\ & (\snake_ctrl_inst|Add9~5\ & VCC)) # (!\snake_ctrl_inst|rd_pos~43_combout\ & (!\snake_ctrl_inst|Add9~5\))
-- \snake_ctrl_inst|Add9~7\ = CARRY((!\snake_ctrl_inst|rd_pos~43_combout\ & !\snake_ctrl_inst|Add9~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|rd_pos~43_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|Add9~5\,
	combout => \snake_ctrl_inst|Add9~6_combout\,
	cout => \snake_ctrl_inst|Add9~7\);

-- Location: LCCOMB_X74_Y65_N22
\snake_ctrl_inst|Add9~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add9~8_combout\ = \snake_ctrl_inst|rd_pos~44_combout\ $ (\snake_ctrl_inst|Add9~7\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|rd_pos~44_combout\,
	cin => \snake_ctrl_inst|Add9~7\,
	combout => \snake_ctrl_inst|Add9~8_combout\);

-- Location: LCCOMB_X73_Y65_N10
\snake_ctrl_inst|Add10~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add10~6_combout\ = (\snake_ctrl_inst|rd_pos~43_combout\ & (!\snake_ctrl_inst|Add10~5\)) # (!\snake_ctrl_inst|rd_pos~43_combout\ & ((\snake_ctrl_inst|Add10~5\) # (GND)))
-- \snake_ctrl_inst|Add10~7\ = CARRY((!\snake_ctrl_inst|Add10~5\) # (!\snake_ctrl_inst|rd_pos~43_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|rd_pos~43_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|Add10~5\,
	combout => \snake_ctrl_inst|Add10~6_combout\,
	cout => \snake_ctrl_inst|Add10~7\);

-- Location: LCCOMB_X73_Y65_N12
\snake_ctrl_inst|Add10~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add10~8_combout\ = \snake_ctrl_inst|Add10~7\ $ (!\snake_ctrl_inst|rd_pos~44_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \snake_ctrl_inst|rd_pos~44_combout\,
	cin => \snake_ctrl_inst|Add10~7\,
	combout => \snake_ctrl_inst|Add10~8_combout\);

-- Location: LCCOMB_X73_Y65_N22
\snake_ctrl_inst|Selector111~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector111~0_combout\ = (\snake_ctrl_inst|Add10~8_combout\ & (\snake_ctrl_inst|Selector112~0_combout\ & ((!\snake_ctrl_inst|rd_pos~44_combout\) # (!\snake_ctrl_inst|Equal6~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Add10~8_combout\,
	datab => \snake_ctrl_inst|Equal6~0_combout\,
	datac => \snake_ctrl_inst|Selector112~0_combout\,
	datad => \snake_ctrl_inst|rd_pos~44_combout\,
	combout => \snake_ctrl_inst|Selector111~0_combout\);

-- Location: IOIBUF_X69_Y73_N1
\movement_direction.DOWN~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_movement_direction.DOWN\,
	o => \movement_direction.DOWN~input_o\);

-- Location: LCCOMB_X70_Y68_N16
\snake_ctrl_inst|target_movement_direction.DOWN~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|target_movement_direction.DOWN~feeder_combout\ = \movement_direction.DOWN~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \movement_direction.DOWN~input_o\,
	combout => \snake_ctrl_inst|target_movement_direction.DOWN~feeder_combout\);

-- Location: FF_X70_Y68_N17
\snake_ctrl_inst|target_movement_direction.DOWN\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|target_movement_direction.DOWN~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|target_movement_direction~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|target_movement_direction.DOWN~q\);

-- Location: LCCOMB_X70_Y68_N30
\snake_ctrl_inst|Selector133~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector133~0_combout\ = (!\busy~input_o\ & \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \busy~input_o\,
	datad => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	combout => \snake_ctrl_inst|Selector133~0_combout\);

-- Location: LCCOMB_X69_Y65_N6
\snake_ctrl_inst|Selector103~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector103~0_combout\ = (\snake_ctrl_inst|Selector133~0_combout\ & ((\snake_ctrl_inst|cur_player~q\ & ((\init_head_direction.DOWN~input_o\))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|target_movement_direction.DOWN~q\)))) # 
-- (!\snake_ctrl_inst|Selector133~0_combout\ & (((\init_head_direction.DOWN~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|target_movement_direction.DOWN~q\,
	datab => \snake_ctrl_inst|Selector133~0_combout\,
	datac => \init_head_direction.DOWN~input_o\,
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|Selector103~0_combout\);

-- Location: LCCOMB_X70_Y68_N14
\snake_ctrl_inst|snake_pos[0].head_position.y[0]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|snake_pos[0].head_position.y[0]~3_combout\ = ((\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\ & (!\busy~input_o\ & !\snake_ctrl_inst|cur_player~q\))) # (!\snake_ctrl_inst|snake_pos[0].head_position.y[0]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101011101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[0].head_position.y[0]~2_combout\,
	datab => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	datac => \busy~input_o\,
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|snake_pos[0].head_position.y[0]~3_combout\);

-- Location: FF_X69_Y65_N7
\snake_ctrl_inst|snake_pos[0].head_direction.DOWN\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector103~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[0].head_position.y[0]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].head_direction.DOWN~q\);

-- Location: LCCOMB_X70_Y69_N8
\snake_ctrl_inst|Selector131~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector131~2_combout\ = (\snake_ctrl_inst|cur_player~q\ & !\busy~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_player~q\,
	datac => \busy~input_o\,
	combout => \snake_ctrl_inst|Selector131~2_combout\);

-- Location: LCCOMB_X69_Y65_N28
\snake_ctrl_inst|Selector75~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector75~0_combout\ = (\snake_ctrl_inst|Selector131~2_combout\ & ((\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\ & (\snake_ctrl_inst|target_movement_direction.DOWN~q\)) # (!\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\ & 
-- ((\init_head_direction.DOWN~input_o\))))) # (!\snake_ctrl_inst|Selector131~2_combout\ & (((\init_head_direction.DOWN~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|target_movement_direction.DOWN~q\,
	datab => \snake_ctrl_inst|Selector131~2_combout\,
	datac => \init_head_direction.DOWN~input_o\,
	datad => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	combout => \snake_ctrl_inst|Selector75~0_combout\);

-- Location: LCCOMB_X70_Y69_N28
\snake_ctrl_inst|snake_pos[1].head_position.x[2]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|snake_pos[1].head_position.x[2]~3_combout\ = ((\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\ & !\busy~input_o\))) # (!\snake_ctrl_inst|snake_pos[1].head_position.x[2]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_player~q\,
	datab => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	datac => \busy~input_o\,
	datad => \snake_ctrl_inst|snake_pos[1].head_position.x[2]~2_combout\,
	combout => \snake_ctrl_inst|snake_pos[1].head_position.x[2]~3_combout\);

-- Location: FF_X69_Y65_N29
\snake_ctrl_inst|snake_pos[1].head_direction.DOWN\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector75~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[1].head_position.x[2]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].head_direction.DOWN~q\);

-- Location: LCCOMB_X69_Y65_N8
\snake_ctrl_inst|wr_data~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_data~0_combout\ = (\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].head_direction.DOWN~q\))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].head_direction.DOWN~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[0].head_direction.DOWN~q\,
	datab => \snake_ctrl_inst|snake_pos[1].head_direction.DOWN~q\,
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|wr_data~0_combout\);

-- Location: IOIBUF_X67_Y73_N15
\movement_direction.RIGHT~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_movement_direction.RIGHT\,
	o => \movement_direction.RIGHT~input_o\);

-- Location: FF_X73_Y69_N7
\snake_ctrl_inst|target_movement_direction.RIGHT\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \movement_direction.RIGHT~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snake_ctrl_inst|target_movement_direction~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|target_movement_direction.RIGHT~q\);

-- Location: LCCOMB_X69_Y65_N12
\snake_ctrl_inst|Selector105~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector105~0_combout\ = (\snake_ctrl_inst|Selector133~0_combout\ & ((\snake_ctrl_inst|cur_player~q\ & ((\init_head_direction.RIGHT~input_o\))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|target_movement_direction.RIGHT~q\)))) 
-- # (!\snake_ctrl_inst|Selector133~0_combout\ & (((\init_head_direction.RIGHT~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|target_movement_direction.RIGHT~q\,
	datab => \snake_ctrl_inst|Selector133~0_combout\,
	datac => \init_head_direction.RIGHT~input_o\,
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|Selector105~0_combout\);

-- Location: FF_X69_Y65_N13
\snake_ctrl_inst|snake_pos[0].head_direction.RIGHT\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector105~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[0].head_position.y[0]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].head_direction.RIGHT~q\);

-- Location: LCCOMB_X69_Y65_N18
\snake_ctrl_inst|Selector77~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector77~0_combout\ = (\snake_ctrl_inst|Selector131~2_combout\ & ((\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\ & (\snake_ctrl_inst|target_movement_direction.RIGHT~q\)) # (!\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\ & 
-- ((\init_head_direction.RIGHT~input_o\))))) # (!\snake_ctrl_inst|Selector131~2_combout\ & (((\init_head_direction.RIGHT~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|target_movement_direction.RIGHT~q\,
	datab => \snake_ctrl_inst|Selector131~2_combout\,
	datac => \init_head_direction.RIGHT~input_o\,
	datad => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	combout => \snake_ctrl_inst|Selector77~0_combout\);

-- Location: FF_X69_Y65_N19
\snake_ctrl_inst|snake_pos[1].head_direction.RIGHT\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector77~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[1].head_position.x[2]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].head_direction.RIGHT~q\);

-- Location: LCCOMB_X69_Y65_N22
\snake_ctrl_inst|wr_data~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_data~2_combout\ = (\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].head_direction.RIGHT~q\))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].head_direction.RIGHT~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[0].head_direction.RIGHT~q\,
	datab => \snake_ctrl_inst|snake_pos[1].head_direction.RIGHT~q\,
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|wr_data~2_combout\);

-- Location: IOIBUF_X115_Y61_N22
\movement_direction.LEFT~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_movement_direction.LEFT\,
	o => \movement_direction.LEFT~input_o\);

-- Location: FF_X70_Y65_N19
\snake_ctrl_inst|target_movement_direction.LEFT\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \movement_direction.LEFT~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snake_ctrl_inst|target_movement_direction~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|target_movement_direction.LEFT~q\);

-- Location: LCCOMB_X70_Y68_N10
\snake_ctrl_inst|Selector104~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector104~0_combout\ = (\snake_ctrl_inst|Selector133~0_combout\ & ((\snake_ctrl_inst|cur_player~q\ & ((\init_head_direction.LEFT~input_o\))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|target_movement_direction.LEFT~q\)))) # 
-- (!\snake_ctrl_inst|Selector133~0_combout\ & (((\init_head_direction.LEFT~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector133~0_combout\,
	datab => \snake_ctrl_inst|target_movement_direction.LEFT~q\,
	datac => \init_head_direction.LEFT~input_o\,
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|Selector104~0_combout\);

-- Location: FF_X70_Y68_N11
\snake_ctrl_inst|snake_pos[0].head_direction.LEFT\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector104~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[0].head_position.y[0]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].head_direction.LEFT~q\);

-- Location: LCCOMB_X70_Y69_N30
\snake_ctrl_inst|Selector76~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector76~0_combout\ = (\snake_ctrl_inst|Selector131~2_combout\ & ((\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\ & (\snake_ctrl_inst|target_movement_direction.LEFT~q\)) # (!\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\ & 
-- ((\init_head_direction.LEFT~input_o\))))) # (!\snake_ctrl_inst|Selector131~2_combout\ & (((\init_head_direction.LEFT~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|target_movement_direction.LEFT~q\,
	datab => \snake_ctrl_inst|Selector131~2_combout\,
	datac => \init_head_direction.LEFT~input_o\,
	datad => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	combout => \snake_ctrl_inst|Selector76~0_combout\);

-- Location: FF_X70_Y69_N31
\snake_ctrl_inst|snake_pos[1].head_direction.LEFT\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector76~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[1].head_position.x[2]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].head_direction.LEFT~q\);

-- Location: LCCOMB_X70_Y68_N12
\snake_ctrl_inst|wr_data~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_data~3_combout\ = (\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].head_direction.LEFT~q\))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].head_direction.LEFT~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[0].head_direction.LEFT~q\,
	datac => \snake_ctrl_inst|snake_pos[1].head_direction.LEFT~q\,
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|wr_data~3_combout\);

-- Location: LCCOMB_X74_Y68_N2
\snake_ctrl_inst|Mux0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Mux0~0_combout\ = ((\snake_ctrl_inst|wr_data~2_combout\) # (\snake_ctrl_inst|wr_data~3_combout\)) # (!\snake_ctrl_inst|wr_data~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111011101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|wr_data~0_combout\,
	datab => \snake_ctrl_inst|wr_data~2_combout\,
	datad => \snake_ctrl_inst|wr_data~3_combout\,
	combout => \snake_ctrl_inst|Mux0~0_combout\);

-- Location: LCCOMB_X74_Y68_N10
\snake_ctrl_inst|cur_position.y[0]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|cur_position.y[0]~5_combout\ = \snake_ctrl_inst|cur_position.y\(0) $ (VCC)
-- \snake_ctrl_inst|cur_position.y[0]~6\ = CARRY(\snake_ctrl_inst|cur_position.y\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_position.y\(0),
	datad => VCC,
	combout => \snake_ctrl_inst|cur_position.y[0]~5_combout\,
	cout => \snake_ctrl_inst|cur_position.y[0]~6\);

-- Location: IOIBUF_X89_Y73_N8
\init_head_position.y[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_init_head_position.y\(0),
	o => \init_head_position.y[0]~input_o\);

-- Location: LCCOMB_X75_Y69_N26
\snake_ctrl_inst|cur_position.y[1]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|cur_position.y[1]~7_combout\ = (!\busy~input_o\ & ((\snake_ctrl_inst|state.PLACE_BODY~q\) # (\snake_ctrl_inst|state.PLACE_HEAD~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \busy~input_o\,
	datac => \snake_ctrl_inst|state.PLACE_BODY~q\,
	datad => \snake_ctrl_inst|state.PLACE_HEAD~q\,
	combout => \snake_ctrl_inst|cur_position.y[1]~7_combout\);

-- Location: LCCOMB_X75_Y69_N20
\snake_ctrl_inst|cur_position.y[1]~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|cur_position.y[1]~8_combout\ = (\snake_ctrl_inst|cur_position.y[1]~7_combout\ & (((\snake_ctrl_inst|state.PLACE_HEAD~q\) # (!\snake_ctrl_inst|Equal0~0_combout\)) # (!\snake_ctrl_inst|Equal0~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Equal0~1_combout\,
	datab => \snake_ctrl_inst|state.PLACE_HEAD~q\,
	datac => \snake_ctrl_inst|cur_position.y[1]~7_combout\,
	datad => \snake_ctrl_inst|Equal0~0_combout\,
	combout => \snake_ctrl_inst|cur_position.y[1]~8_combout\);

-- Location: LCCOMB_X74_Y68_N8
\snake_ctrl_inst|cur_position.y[1]~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|cur_position.y[1]~9_combout\ = (\snake_ctrl_inst|cur_length[2]~2_combout\) # ((!\snake_ctrl_inst|wr_data~3_combout\ & (!\snake_ctrl_inst|wr_data~2_combout\ & \snake_ctrl_inst|cur_position.y[1]~8_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|wr_data~3_combout\,
	datab => \snake_ctrl_inst|wr_data~2_combout\,
	datac => \snake_ctrl_inst|cur_length[2]~2_combout\,
	datad => \snake_ctrl_inst|cur_position.y[1]~8_combout\,
	combout => \snake_ctrl_inst|cur_position.y[1]~9_combout\);

-- Location: FF_X74_Y68_N11
\snake_ctrl_inst|cur_position.y[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|cur_position.y[0]~5_combout\,
	asdata => \init_head_position.y[0]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \snake_ctrl_inst|ALT_INV_state.IDLE~q\,
	ena => \snake_ctrl_inst|cur_position.y[1]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|cur_position.y\(0));

-- Location: LCCOMB_X74_Y68_N12
\snake_ctrl_inst|cur_position.y[1]~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|cur_position.y[1]~10_combout\ = (\snake_ctrl_inst|cur_position.y\(1) & ((\snake_ctrl_inst|Mux0~0_combout\ & (!\snake_ctrl_inst|cur_position.y[0]~6\)) # (!\snake_ctrl_inst|Mux0~0_combout\ & (\snake_ctrl_inst|cur_position.y[0]~6\ & VCC)))) 
-- # (!\snake_ctrl_inst|cur_position.y\(1) & ((\snake_ctrl_inst|Mux0~0_combout\ & ((\snake_ctrl_inst|cur_position.y[0]~6\) # (GND))) # (!\snake_ctrl_inst|Mux0~0_combout\ & (!\snake_ctrl_inst|cur_position.y[0]~6\))))
-- \snake_ctrl_inst|cur_position.y[1]~11\ = CARRY((\snake_ctrl_inst|cur_position.y\(1) & (\snake_ctrl_inst|Mux0~0_combout\ & !\snake_ctrl_inst|cur_position.y[0]~6\)) # (!\snake_ctrl_inst|cur_position.y\(1) & ((\snake_ctrl_inst|Mux0~0_combout\) # 
-- (!\snake_ctrl_inst|cur_position.y[0]~6\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100101001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_position.y\(1),
	datab => \snake_ctrl_inst|Mux0~0_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|cur_position.y[0]~6\,
	combout => \snake_ctrl_inst|cur_position.y[1]~10_combout\,
	cout => \snake_ctrl_inst|cur_position.y[1]~11\);

-- Location: IOIBUF_X94_Y73_N1
\init_head_position.y[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_init_head_position.y\(1),
	o => \init_head_position.y[1]~input_o\);

-- Location: FF_X74_Y68_N13
\snake_ctrl_inst|cur_position.y[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|cur_position.y[1]~10_combout\,
	asdata => \init_head_position.y[1]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \snake_ctrl_inst|ALT_INV_state.IDLE~q\,
	ena => \snake_ctrl_inst|cur_position.y[1]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|cur_position.y\(1));

-- Location: LCCOMB_X74_Y68_N14
\snake_ctrl_inst|cur_position.y[2]~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|cur_position.y[2]~12_combout\ = ((\snake_ctrl_inst|cur_position.y\(2) $ (\snake_ctrl_inst|Mux0~0_combout\ $ (\snake_ctrl_inst|cur_position.y[1]~11\)))) # (GND)
-- \snake_ctrl_inst|cur_position.y[2]~13\ = CARRY((\snake_ctrl_inst|cur_position.y\(2) & ((!\snake_ctrl_inst|cur_position.y[1]~11\) # (!\snake_ctrl_inst|Mux0~0_combout\))) # (!\snake_ctrl_inst|cur_position.y\(2) & (!\snake_ctrl_inst|Mux0~0_combout\ & 
-- !\snake_ctrl_inst|cur_position.y[1]~11\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_position.y\(2),
	datab => \snake_ctrl_inst|Mux0~0_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|cur_position.y[1]~11\,
	combout => \snake_ctrl_inst|cur_position.y[2]~12_combout\,
	cout => \snake_ctrl_inst|cur_position.y[2]~13\);

-- Location: IOIBUF_X85_Y73_N8
\init_head_position.y[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_init_head_position.y\(2),
	o => \init_head_position.y[2]~input_o\);

-- Location: FF_X74_Y68_N15
\snake_ctrl_inst|cur_position.y[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|cur_position.y[2]~12_combout\,
	asdata => \init_head_position.y[2]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \snake_ctrl_inst|ALT_INV_state.IDLE~q\,
	ena => \snake_ctrl_inst|cur_position.y[1]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|cur_position.y\(2));

-- Location: LCCOMB_X74_Y68_N16
\snake_ctrl_inst|cur_position.y[3]~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|cur_position.y[3]~14_combout\ = (\snake_ctrl_inst|cur_position.y\(3) & ((\snake_ctrl_inst|Mux0~0_combout\ & (!\snake_ctrl_inst|cur_position.y[2]~13\)) # (!\snake_ctrl_inst|Mux0~0_combout\ & (\snake_ctrl_inst|cur_position.y[2]~13\ & 
-- VCC)))) # (!\snake_ctrl_inst|cur_position.y\(3) & ((\snake_ctrl_inst|Mux0~0_combout\ & ((\snake_ctrl_inst|cur_position.y[2]~13\) # (GND))) # (!\snake_ctrl_inst|Mux0~0_combout\ & (!\snake_ctrl_inst|cur_position.y[2]~13\))))
-- \snake_ctrl_inst|cur_position.y[3]~15\ = CARRY((\snake_ctrl_inst|cur_position.y\(3) & (\snake_ctrl_inst|Mux0~0_combout\ & !\snake_ctrl_inst|cur_position.y[2]~13\)) # (!\snake_ctrl_inst|cur_position.y\(3) & ((\snake_ctrl_inst|Mux0~0_combout\) # 
-- (!\snake_ctrl_inst|cur_position.y[2]~13\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100101001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_position.y\(3),
	datab => \snake_ctrl_inst|Mux0~0_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|cur_position.y[2]~13\,
	combout => \snake_ctrl_inst|cur_position.y[3]~14_combout\,
	cout => \snake_ctrl_inst|cur_position.y[3]~15\);

-- Location: IOIBUF_X94_Y73_N8
\init_head_position.y[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_init_head_position.y\(3),
	o => \init_head_position.y[3]~input_o\);

-- Location: FF_X74_Y68_N17
\snake_ctrl_inst|cur_position.y[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|cur_position.y[3]~14_combout\,
	asdata => \init_head_position.y[3]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \snake_ctrl_inst|ALT_INV_state.IDLE~q\,
	ena => \snake_ctrl_inst|cur_position.y[1]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|cur_position.y\(3));

-- Location: LCCOMB_X74_Y68_N18
\snake_ctrl_inst|cur_position.y[4]~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|cur_position.y[4]~16_combout\ = \snake_ctrl_inst|Mux0~0_combout\ $ (\snake_ctrl_inst|cur_position.y[3]~15\ $ (\snake_ctrl_inst|cur_position.y\(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|Mux0~0_combout\,
	datad => \snake_ctrl_inst|cur_position.y\(4),
	cin => \snake_ctrl_inst|cur_position.y[3]~15\,
	combout => \snake_ctrl_inst|cur_position.y[4]~16_combout\);

-- Location: IOIBUF_X67_Y73_N22
\init_head_position.y[4]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_init_head_position.y\(4),
	o => \init_head_position.y[4]~input_o\);

-- Location: FF_X74_Y68_N19
\snake_ctrl_inst|cur_position.y[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|cur_position.y[4]~16_combout\,
	asdata => \init_head_position.y[4]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \snake_ctrl_inst|ALT_INV_state.IDLE~q\,
	ena => \snake_ctrl_inst|cur_position.y[1]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|cur_position.y\(4));

-- Location: LCCOMB_X74_Y66_N20
\snake_ctrl_inst|Selector115~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector115~1_combout\ = (\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\ & ((\snake_ctrl_inst|tail_position~0_combout\) # (\snake_ctrl_inst|wr_data~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|tail_position~0_combout\,
	datab => \snake_ctrl_inst|wr_data~6_combout\,
	datad => \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\,
	combout => \snake_ctrl_inst|Selector115~1_combout\);

-- Location: LCCOMB_X74_Y65_N30
\snake_ctrl_inst|Selector111~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector111~1_combout\ = (\snake_ctrl_inst|cur_position.y\(4) & (((\snake_ctrl_inst|Selector115~1_combout\ & \snake_ctrl_inst|rd_pos~44_combout\)) # (!\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\))) # (!\snake_ctrl_inst|cur_position.y\(4) & 
-- (\snake_ctrl_inst|Selector115~1_combout\ & (\snake_ctrl_inst|rd_pos~44_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_position.y\(4),
	datab => \snake_ctrl_inst|Selector115~1_combout\,
	datac => \snake_ctrl_inst|rd_pos~44_combout\,
	datad => \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\,
	combout => \snake_ctrl_inst|Selector111~1_combout\);

-- Location: LCCOMB_X74_Y65_N8
\snake_ctrl_inst|Selector111~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector111~2_combout\ = (\snake_ctrl_inst|Selector111~0_combout\) # ((\snake_ctrl_inst|Selector111~1_combout\) # ((\snake_ctrl_inst|Add9~8_combout\ & \snake_ctrl_inst|Selector115~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Add9~8_combout\,
	datab => \snake_ctrl_inst|Selector111~0_combout\,
	datac => \snake_ctrl_inst|Selector111~1_combout\,
	datad => \snake_ctrl_inst|Selector115~3_combout\,
	combout => \snake_ctrl_inst|Selector111~2_combout\);

-- Location: LCCOMB_X74_Y66_N26
\snake_ctrl_inst|snake_pos[0].tail_position.y[4]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|snake_pos[0].tail_position.y[4]~0_combout\ = (!\snake_ctrl_inst|cur_player~q\ & (!\busy~input_o\ & ((\snake_ctrl_inst|state.PLACE_TAIL~q\) # (\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000100010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_player~q\,
	datab => \busy~input_o\,
	datac => \snake_ctrl_inst|state.PLACE_TAIL~q\,
	datad => \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\,
	combout => \snake_ctrl_inst|snake_pos[0].tail_position.y[4]~0_combout\);

-- Location: FF_X74_Y65_N9
\snake_ctrl_inst|snake_pos[0].tail_position.y[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector111~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[0].tail_position.y[4]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].tail_position.y\(4));

-- Location: FF_X74_Y65_N19
\snake_ctrl_inst|snake_pos[1].tail_position.y[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snake_ctrl_inst|Selector111~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snake_ctrl_inst|snake_pos[1].tail_position.y[4]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].tail_position.y\(4));

-- Location: LCCOMB_X74_Y65_N26
\snake_ctrl_inst|rd_pos~44\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~44_combout\ = (\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].tail_position.y\(4)))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].tail_position.y\(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[0].tail_position.y\(4),
	datac => \snake_ctrl_inst|snake_pos[1].tail_position.y\(4),
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|rd_pos~44_combout\);

-- Location: LCCOMB_X73_Y65_N16
\snake_ctrl_inst|Selector113~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector113~0_combout\ = (\snake_ctrl_inst|Selector112~0_combout\ & (\snake_ctrl_inst|Add10~4_combout\ & ((!\snake_ctrl_inst|rd_pos~44_combout\) # (!\snake_ctrl_inst|Equal6~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector112~0_combout\,
	datab => \snake_ctrl_inst|Equal6~0_combout\,
	datac => \snake_ctrl_inst|Add10~4_combout\,
	datad => \snake_ctrl_inst|rd_pos~44_combout\,
	combout => \snake_ctrl_inst|Selector113~0_combout\);

-- Location: LCCOMB_X74_Y65_N28
\snake_ctrl_inst|Selector113~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector113~1_combout\ = (\snake_ctrl_inst|rd_pos~42_combout\ & ((\snake_ctrl_inst|Selector115~1_combout\) # ((\snake_ctrl_inst|cur_position.y\(2) & !\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\)))) # (!\snake_ctrl_inst|rd_pos~42_combout\ & 
-- (\snake_ctrl_inst|cur_position.y\(2) & ((!\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|rd_pos~42_combout\,
	datab => \snake_ctrl_inst|cur_position.y\(2),
	datac => \snake_ctrl_inst|Selector115~1_combout\,
	datad => \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\,
	combout => \snake_ctrl_inst|Selector113~1_combout\);

-- Location: LCCOMB_X74_Y65_N4
\snake_ctrl_inst|Selector113~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector113~2_combout\ = (\snake_ctrl_inst|Selector113~0_combout\) # ((\snake_ctrl_inst|Selector113~1_combout\) # ((\snake_ctrl_inst|Add9~4_combout\ & \snake_ctrl_inst|Selector115~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector113~0_combout\,
	datab => \snake_ctrl_inst|Add9~4_combout\,
	datac => \snake_ctrl_inst|Selector115~3_combout\,
	datad => \snake_ctrl_inst|Selector113~1_combout\,
	combout => \snake_ctrl_inst|Selector113~2_combout\);

-- Location: FF_X74_Y65_N5
\snake_ctrl_inst|snake_pos[0].tail_position.y[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector113~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[0].tail_position.y[4]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].tail_position.y\(2));

-- Location: FF_X74_Y65_N17
\snake_ctrl_inst|snake_pos[1].tail_position.y[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snake_ctrl_inst|Selector113~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snake_ctrl_inst|snake_pos[1].tail_position.y[4]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].tail_position.y\(2));

-- Location: LCCOMB_X74_Y65_N6
\snake_ctrl_inst|rd_pos~42\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~42_combout\ = (\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].tail_position.y\(2)))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].tail_position.y\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[0].tail_position.y\(2),
	datab => \snake_ctrl_inst|snake_pos[1].tail_position.y\(2),
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|rd_pos~42_combout\);

-- Location: LCCOMB_X73_Y65_N20
\snake_ctrl_inst|Selector112~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector112~2_combout\ = (\snake_ctrl_inst|rd_pos~43_combout\ & ((\snake_ctrl_inst|Selector115~1_combout\) # ((\snake_ctrl_inst|cur_position.y\(3) & !\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\)))) # (!\snake_ctrl_inst|rd_pos~43_combout\ & 
-- (\snake_ctrl_inst|cur_position.y\(3) & (!\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|rd_pos~43_combout\,
	datab => \snake_ctrl_inst|cur_position.y\(3),
	datac => \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\,
	datad => \snake_ctrl_inst|Selector115~1_combout\,
	combout => \snake_ctrl_inst|Selector112~2_combout\);

-- Location: LCCOMB_X73_Y65_N2
\snake_ctrl_inst|Selector112~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector112~1_combout\ = (\snake_ctrl_inst|Selector115~3_combout\ & (\snake_ctrl_inst|Add9~6_combout\ & ((\snake_ctrl_inst|rd_pos~44_combout\) # (!\snake_ctrl_inst|Equal5~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector115~3_combout\,
	datab => \snake_ctrl_inst|Equal5~0_combout\,
	datac => \snake_ctrl_inst|Add9~6_combout\,
	datad => \snake_ctrl_inst|rd_pos~44_combout\,
	combout => \snake_ctrl_inst|Selector112~1_combout\);

-- Location: LCCOMB_X73_Y65_N26
\snake_ctrl_inst|Selector112~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector112~3_combout\ = (\snake_ctrl_inst|Selector112~2_combout\) # ((\snake_ctrl_inst|Selector112~1_combout\) # ((\snake_ctrl_inst|Add10~6_combout\ & \snake_ctrl_inst|Selector112~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Add10~6_combout\,
	datab => \snake_ctrl_inst|Selector112~2_combout\,
	datac => \snake_ctrl_inst|Selector112~0_combout\,
	datad => \snake_ctrl_inst|Selector112~1_combout\,
	combout => \snake_ctrl_inst|Selector112~3_combout\);

-- Location: FF_X73_Y65_N1
\snake_ctrl_inst|snake_pos[1].tail_position.y[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snake_ctrl_inst|Selector112~3_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snake_ctrl_inst|snake_pos[1].tail_position.y[4]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].tail_position.y\(3));

-- Location: FF_X73_Y65_N27
\snake_ctrl_inst|snake_pos[0].tail_position.y[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector112~3_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[0].tail_position.y[4]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].tail_position.y\(3));

-- Location: LCCOMB_X73_Y65_N30
\snake_ctrl_inst|rd_pos~43\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~43_combout\ = (\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[1].tail_position.y\(3))) # (!\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[0].tail_position.y\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|snake_pos[1].tail_position.y\(3),
	datac => \snake_ctrl_inst|snake_pos[0].tail_position.y\(3),
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|rd_pos~43_combout\);

-- Location: LCCOMB_X73_Y65_N24
\snake_ctrl_inst|Equal6~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Equal6~0_combout\ = (\snake_ctrl_inst|rd_pos~41_combout\ & (!\snake_ctrl_inst|rd_pos~43_combout\ & (!\snake_ctrl_inst|rd_pos~40_combout\ & \snake_ctrl_inst|rd_pos~42_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|rd_pos~41_combout\,
	datab => \snake_ctrl_inst|rd_pos~43_combout\,
	datac => \snake_ctrl_inst|rd_pos~40_combout\,
	datad => \snake_ctrl_inst|rd_pos~42_combout\,
	combout => \snake_ctrl_inst|Equal6~0_combout\);

-- Location: LCCOMB_X73_Y65_N14
\snake_ctrl_inst|Selector114~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector114~0_combout\ = (\snake_ctrl_inst|Add10~2_combout\ & (\snake_ctrl_inst|Selector112~0_combout\ & ((!\snake_ctrl_inst|rd_pos~44_combout\) # (!\snake_ctrl_inst|Equal6~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Add10~2_combout\,
	datab => \snake_ctrl_inst|Equal6~0_combout\,
	datac => \snake_ctrl_inst|Selector112~0_combout\,
	datad => \snake_ctrl_inst|rd_pos~44_combout\,
	combout => \snake_ctrl_inst|Selector114~0_combout\);

-- Location: LCCOMB_X74_Y65_N10
\snake_ctrl_inst|Selector114~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector114~1_combout\ = (\snake_ctrl_inst|cur_position.y\(1) & (((\snake_ctrl_inst|rd_pos~41_combout\ & \snake_ctrl_inst|Selector115~1_combout\)) # (!\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\))) # (!\snake_ctrl_inst|cur_position.y\(1) & 
-- (\snake_ctrl_inst|rd_pos~41_combout\ & (\snake_ctrl_inst|Selector115~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_position.y\(1),
	datab => \snake_ctrl_inst|rd_pos~41_combout\,
	datac => \snake_ctrl_inst|Selector115~1_combout\,
	datad => \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\,
	combout => \snake_ctrl_inst|Selector114~1_combout\);

-- Location: LCCOMB_X74_Y65_N2
\snake_ctrl_inst|Selector114~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector114~2_combout\ = (\snake_ctrl_inst|Selector114~0_combout\) # ((\snake_ctrl_inst|Selector114~1_combout\) # ((\snake_ctrl_inst|Add9~2_combout\ & \snake_ctrl_inst|Selector115~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector114~0_combout\,
	datab => \snake_ctrl_inst|Add9~2_combout\,
	datac => \snake_ctrl_inst|Selector115~3_combout\,
	datad => \snake_ctrl_inst|Selector114~1_combout\,
	combout => \snake_ctrl_inst|Selector114~2_combout\);

-- Location: FF_X74_Y65_N3
\snake_ctrl_inst|snake_pos[0].tail_position.y[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector114~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[0].tail_position.y[4]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].tail_position.y\(1));

-- Location: FF_X74_Y65_N15
\snake_ctrl_inst|snake_pos[1].tail_position.y[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snake_ctrl_inst|Selector114~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snake_ctrl_inst|snake_pos[1].tail_position.y[4]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].tail_position.y\(1));

-- Location: LCCOMB_X74_Y69_N18
\snake_ctrl_inst|rd_pos~41\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~41_combout\ = (\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].tail_position.y\(1)))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].tail_position.y\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[0].tail_position.y\(1),
	datac => \snake_ctrl_inst|cur_player~q\,
	datad => \snake_ctrl_inst|snake_pos[1].tail_position.y\(1),
	combout => \snake_ctrl_inst|rd_pos~41_combout\);

-- Location: LCCOMB_X73_Y65_N28
\snake_ctrl_inst|Equal5~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Equal5~0_combout\ = (!\snake_ctrl_inst|rd_pos~41_combout\ & (!\snake_ctrl_inst|rd_pos~43_combout\ & (!\snake_ctrl_inst|rd_pos~40_combout\ & !\snake_ctrl_inst|rd_pos~42_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|rd_pos~41_combout\,
	datab => \snake_ctrl_inst|rd_pos~43_combout\,
	datac => \snake_ctrl_inst|rd_pos~40_combout\,
	datad => \snake_ctrl_inst|rd_pos~42_combout\,
	combout => \snake_ctrl_inst|Equal5~0_combout\);

-- Location: LCCOMB_X73_Y65_N0
\snake_ctrl_inst|Selector115~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector115~4_combout\ = (\snake_ctrl_inst|Selector115~3_combout\ & ((\snake_ctrl_inst|rd_pos~44_combout\) # (!\snake_ctrl_inst|Equal5~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector115~3_combout\,
	datab => \snake_ctrl_inst|Equal5~0_combout\,
	datad => \snake_ctrl_inst|rd_pos~44_combout\,
	combout => \snake_ctrl_inst|Selector115~4_combout\);

-- Location: LCCOMB_X74_Y66_N24
\snake_ctrl_inst|Selector115~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector115~2_combout\ = (\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\ & (\snake_ctrl_inst|Selector115~1_combout\ & (\snake_ctrl_inst|rd_pos~40_combout\))) # (!\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\ & 
-- ((\snake_ctrl_inst|cur_position.y\(0)) # ((\snake_ctrl_inst|Selector115~1_combout\ & \snake_ctrl_inst|rd_pos~40_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101010111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\,
	datab => \snake_ctrl_inst|Selector115~1_combout\,
	datac => \snake_ctrl_inst|rd_pos~40_combout\,
	datad => \snake_ctrl_inst|cur_position.y\(0),
	combout => \snake_ctrl_inst|Selector115~2_combout\);

-- Location: LCCOMB_X73_Y65_N18
\snake_ctrl_inst|Selector115~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector115~0_combout\ = (\snake_ctrl_inst|Selector112~0_combout\ & (\snake_ctrl_inst|Add10~0_combout\ & ((!\snake_ctrl_inst|rd_pos~44_combout\) # (!\snake_ctrl_inst|Equal6~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector112~0_combout\,
	datab => \snake_ctrl_inst|Equal6~0_combout\,
	datac => \snake_ctrl_inst|Add10~0_combout\,
	datad => \snake_ctrl_inst|rd_pos~44_combout\,
	combout => \snake_ctrl_inst|Selector115~0_combout\);

-- Location: LCCOMB_X74_Y65_N24
\snake_ctrl_inst|Selector115~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector115~5_combout\ = (\snake_ctrl_inst|Selector115~2_combout\) # ((\snake_ctrl_inst|Selector115~0_combout\) # ((\snake_ctrl_inst|Selector115~4_combout\ & \snake_ctrl_inst|Add9~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector115~4_combout\,
	datab => \snake_ctrl_inst|Add9~0_combout\,
	datac => \snake_ctrl_inst|Selector115~2_combout\,
	datad => \snake_ctrl_inst|Selector115~0_combout\,
	combout => \snake_ctrl_inst|Selector115~5_combout\);

-- Location: FF_X74_Y65_N25
\snake_ctrl_inst|snake_pos[0].tail_position.y[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector115~5_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[0].tail_position.y[4]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].tail_position.y\(0));

-- Location: LCCOMB_X74_Y66_N6
\snake_ctrl_inst|rd_pos~60\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~60_combout\ = (\snake_ctrl_inst|rd~0_combout\ & ((\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].tail_position.y\(0)))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].tail_position.y\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[0].tail_position.y\(0),
	datab => \snake_ctrl_inst|snake_pos[1].tail_position.y\(0),
	datac => \snake_ctrl_inst|cur_player~q\,
	datad => \snake_ctrl_inst|rd~0_combout\,
	combout => \snake_ctrl_inst|rd_pos~60_combout\);

-- Location: FF_X75_Y66_N3
\snake_ctrl_inst|rd_pos.y[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snake_ctrl_inst|rd_pos~60_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|rd_pos.y\(0));

-- Location: LCCOMB_X74_Y69_N8
\snake_ctrl_inst|rd_pos~61\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~61_combout\ = (\snake_ctrl_inst|rd~0_combout\ & ((\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].tail_position.y\(1)))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].tail_position.y\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[0].tail_position.y\(1),
	datab => \snake_ctrl_inst|cur_player~q\,
	datac => \snake_ctrl_inst|rd~0_combout\,
	datad => \snake_ctrl_inst|snake_pos[1].tail_position.y\(1),
	combout => \snake_ctrl_inst|rd_pos~61_combout\);

-- Location: FF_X74_Y69_N9
\snake_ctrl_inst|rd_pos.y[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|rd_pos~61_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|rd_pos.y\(1));

-- Location: LCCOMB_X75_Y65_N8
\snake_ctrl_inst|rd_pos~62\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~62_combout\ = (\snake_ctrl_inst|rd~0_combout\ & ((\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[1].tail_position.y\(2))) # (!\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[0].tail_position.y\(2))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[1].tail_position.y\(2),
	datab => \snake_ctrl_inst|snake_pos[0].tail_position.y\(2),
	datac => \snake_ctrl_inst|cur_player~q\,
	datad => \snake_ctrl_inst|rd~0_combout\,
	combout => \snake_ctrl_inst|rd_pos~62_combout\);

-- Location: FF_X75_Y65_N9
\snake_ctrl_inst|rd_pos.y[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|rd_pos~62_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|rd_pos.y\(2));

-- Location: LCCOMB_X75_Y66_N28
\snake_ctrl_inst|rd_pos~63\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~63_combout\ = (\snake_ctrl_inst|rd~0_combout\ & ((\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[1].tail_position.y\(3))) # (!\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[0].tail_position.y\(3))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[1].tail_position.y\(3),
	datab => \snake_ctrl_inst|snake_pos[0].tail_position.y\(3),
	datac => \snake_ctrl_inst|rd~0_combout\,
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|rd_pos~63_combout\);

-- Location: FF_X75_Y66_N29
\snake_ctrl_inst|rd_pos.y[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|rd_pos~63_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|rd_pos.y\(3));

-- Location: LCCOMB_X75_Y66_N6
\snake_ctrl_inst|rd_pos~64\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~64_combout\ = (\snake_ctrl_inst|rd~0_combout\ & ((\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].tail_position.y\(4)))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].tail_position.y\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_player~q\,
	datab => \snake_ctrl_inst|snake_pos[0].tail_position.y\(4),
	datac => \snake_ctrl_inst|rd~0_combout\,
	datad => \snake_ctrl_inst|snake_pos[1].tail_position.y\(4),
	combout => \snake_ctrl_inst|rd_pos~64_combout\);

-- Location: FF_X75_Y66_N7
\snake_ctrl_inst|rd_pos.y[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|rd_pos~64_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|rd_pos.y\(4));

-- Location: FF_X75_Y66_N5
\snake_ctrl_inst|snake_pos[1].tail_position.x[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snake_ctrl_inst|Add12~3_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snake_ctrl_inst|snake_pos[1].tail_position.y[4]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].tail_position.x\(0));

-- Location: LCCOMB_X75_Y66_N4
\snake_ctrl_inst|rd_pos~45\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~45_combout\ = (\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].tail_position.x\(0)))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].tail_position.x\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[0].tail_position.x\(0),
	datac => \snake_ctrl_inst|snake_pos[1].tail_position.x\(0),
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|rd_pos~45_combout\);

-- Location: LCCOMB_X75_Y66_N10
\snake_ctrl_inst|Add11~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add11~0_combout\ = (\snake_ctrl_inst|rd_pos~45_combout\ & (\snake_ctrl_inst|tail_position~0_combout\ $ (VCC))) # (!\snake_ctrl_inst|rd_pos~45_combout\ & (\snake_ctrl_inst|tail_position~0_combout\ & VCC))
-- \snake_ctrl_inst|Add11~1\ = CARRY((\snake_ctrl_inst|rd_pos~45_combout\ & \snake_ctrl_inst|tail_position~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|rd_pos~45_combout\,
	datab => \snake_ctrl_inst|tail_position~0_combout\,
	datad => VCC,
	combout => \snake_ctrl_inst|Add11~0_combout\,
	cout => \snake_ctrl_inst|Add11~1\);

-- Location: LCCOMB_X76_Y66_N10
\snake_ctrl_inst|Add12~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add12~0_combout\ = \snake_ctrl_inst|rd_pos~45_combout\ $ (VCC)
-- \snake_ctrl_inst|Add12~1\ = CARRY(\snake_ctrl_inst|rd_pos~45_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|rd_pos~45_combout\,
	datad => VCC,
	combout => \snake_ctrl_inst|Add12~0_combout\,
	cout => \snake_ctrl_inst|Add12~1\);

-- Location: LCCOMB_X76_Y66_N20
\snake_ctrl_inst|Add12~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add12~2_combout\ = (\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\ & ((\snake_ctrl_inst|wr_data~6_combout\ & ((\snake_ctrl_inst|Add12~0_combout\))) # (!\snake_ctrl_inst|wr_data~6_combout\ & (\snake_ctrl_inst|Add11~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Add11~0_combout\,
	datab => \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\,
	datac => \snake_ctrl_inst|wr_data~6_combout\,
	datad => \snake_ctrl_inst|Add12~0_combout\,
	combout => \snake_ctrl_inst|Add12~2_combout\);

-- Location: LCCOMB_X74_Y68_N22
\snake_ctrl_inst|cur_position.x[0]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|cur_position.x[0]~5_combout\ = \snake_ctrl_inst|cur_position.x\(0) $ (VCC)
-- \snake_ctrl_inst|cur_position.x[0]~6\ = CARRY(\snake_ctrl_inst|cur_position.x\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_position.x\(0),
	datad => VCC,
	combout => \snake_ctrl_inst|cur_position.x[0]~5_combout\,
	cout => \snake_ctrl_inst|cur_position.x[0]~6\);

-- Location: IOIBUF_X58_Y73_N15
\init_head_position.x[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_init_head_position.x\(0),
	o => \init_head_position.x[0]~input_o\);

-- Location: LCCOMB_X74_Y68_N0
\snake_ctrl_inst|cur_position.x[3]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|cur_position.x[3]~7_combout\ = (\snake_ctrl_inst|cur_length[2]~2_combout\) # ((\snake_ctrl_inst|cur_position.y[1]~8_combout\ & ((\snake_ctrl_inst|wr_data~3_combout\) # (\snake_ctrl_inst|wr_data~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|wr_data~3_combout\,
	datab => \snake_ctrl_inst|wr_data~2_combout\,
	datac => \snake_ctrl_inst|cur_length[2]~2_combout\,
	datad => \snake_ctrl_inst|cur_position.y[1]~8_combout\,
	combout => \snake_ctrl_inst|cur_position.x[3]~7_combout\);

-- Location: FF_X74_Y68_N23
\snake_ctrl_inst|cur_position.x[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|cur_position.x[0]~5_combout\,
	asdata => \init_head_position.x[0]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \snake_ctrl_inst|ALT_INV_state.IDLE~q\,
	ena => \snake_ctrl_inst|cur_position.x[3]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|cur_position.x\(0));

-- Location: LCCOMB_X74_Y66_N10
\snake_ctrl_inst|Add12~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add12~3_combout\ = (\snake_ctrl_inst|Add12~2_combout\) # ((!\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\ & \snake_ctrl_inst|cur_position.x\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110011011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\,
	datab => \snake_ctrl_inst|Add12~2_combout\,
	datac => \snake_ctrl_inst|cur_position.x\(0),
	combout => \snake_ctrl_inst|Add12~3_combout\);

-- Location: FF_X74_Y66_N11
\snake_ctrl_inst|snake_pos[0].tail_position.x[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Add12~3_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[0].tail_position.y[4]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].tail_position.x\(0));

-- Location: LCCOMB_X75_Y66_N8
\snake_ctrl_inst|rd_pos~65\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~65_combout\ = (\snake_ctrl_inst|rd~0_combout\ & ((\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].tail_position.x\(0)))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].tail_position.x\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[0].tail_position.x\(0),
	datab => \snake_ctrl_inst|snake_pos[1].tail_position.x\(0),
	datac => \snake_ctrl_inst|rd~0_combout\,
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|rd_pos~65_combout\);

-- Location: FF_X75_Y66_N9
\snake_ctrl_inst|rd_pos.x[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|rd_pos~65_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|rd_pos.x\(0));

-- Location: FF_X74_Y66_N21
\snake_ctrl_inst|snake_pos[1].tail_position.x[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snake_ctrl_inst|Add12~7_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snake_ctrl_inst|snake_pos[1].tail_position.y[4]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].tail_position.x\(1));

-- Location: LCCOMB_X75_Y66_N0
\snake_ctrl_inst|rd_pos~46\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~46_combout\ = (\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].tail_position.x\(1)))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].tail_position.x\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[0].tail_position.x\(1),
	datab => \snake_ctrl_inst|snake_pos[1].tail_position.x\(1),
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|rd_pos~46_combout\);

-- Location: LCCOMB_X76_Y66_N12
\snake_ctrl_inst|Add12~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add12~4_combout\ = (\snake_ctrl_inst|rd_pos~46_combout\ & (!\snake_ctrl_inst|Add12~1\)) # (!\snake_ctrl_inst|rd_pos~46_combout\ & ((\snake_ctrl_inst|Add12~1\) # (GND)))
-- \snake_ctrl_inst|Add12~5\ = CARRY((!\snake_ctrl_inst|Add12~1\) # (!\snake_ctrl_inst|rd_pos~46_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|rd_pos~46_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|Add12~1\,
	combout => \snake_ctrl_inst|Add12~4_combout\,
	cout => \snake_ctrl_inst|Add12~5\);

-- Location: LCCOMB_X75_Y66_N12
\snake_ctrl_inst|Add11~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add11~2_combout\ = (\snake_ctrl_inst|tail_position~0_combout\ & ((\snake_ctrl_inst|rd_pos~46_combout\ & (\snake_ctrl_inst|Add11~1\ & VCC)) # (!\snake_ctrl_inst|rd_pos~46_combout\ & (!\snake_ctrl_inst|Add11~1\)))) # 
-- (!\snake_ctrl_inst|tail_position~0_combout\ & ((\snake_ctrl_inst|rd_pos~46_combout\ & (!\snake_ctrl_inst|Add11~1\)) # (!\snake_ctrl_inst|rd_pos~46_combout\ & ((\snake_ctrl_inst|Add11~1\) # (GND)))))
-- \snake_ctrl_inst|Add11~3\ = CARRY((\snake_ctrl_inst|tail_position~0_combout\ & (!\snake_ctrl_inst|rd_pos~46_combout\ & !\snake_ctrl_inst|Add11~1\)) # (!\snake_ctrl_inst|tail_position~0_combout\ & ((!\snake_ctrl_inst|Add11~1\) # 
-- (!\snake_ctrl_inst|rd_pos~46_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|tail_position~0_combout\,
	datab => \snake_ctrl_inst|rd_pos~46_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|Add11~1\,
	combout => \snake_ctrl_inst|Add11~2_combout\,
	cout => \snake_ctrl_inst|Add11~3\);

-- Location: LCCOMB_X74_Y66_N28
\snake_ctrl_inst|Add12~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add12~6_combout\ = (\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\ & ((\snake_ctrl_inst|wr_data~6_combout\ & (\snake_ctrl_inst|Add12~4_combout\)) # (!\snake_ctrl_inst|wr_data~6_combout\ & ((\snake_ctrl_inst|Add11~2_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|wr_data~6_combout\,
	datab => \snake_ctrl_inst|Add12~4_combout\,
	datac => \snake_ctrl_inst|Add11~2_combout\,
	datad => \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\,
	combout => \snake_ctrl_inst|Add12~6_combout\);

-- Location: LCCOMB_X74_Y68_N4
\snake_ctrl_inst|Mux2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Mux2~0_combout\ = (!\snake_ctrl_inst|wr_data~2_combout\ & ((!\snake_ctrl_inst|wr_data~3_combout\) # (!\snake_ctrl_inst|wr_data~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000100110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|wr_data~0_combout\,
	datab => \snake_ctrl_inst|wr_data~2_combout\,
	datad => \snake_ctrl_inst|wr_data~3_combout\,
	combout => \snake_ctrl_inst|Mux2~0_combout\);

-- Location: LCCOMB_X74_Y68_N24
\snake_ctrl_inst|cur_position.x[1]~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|cur_position.x[1]~8_combout\ = (\snake_ctrl_inst|cur_position.x\(1) & ((\snake_ctrl_inst|Mux2~0_combout\ & (!\snake_ctrl_inst|cur_position.x[0]~6\)) # (!\snake_ctrl_inst|Mux2~0_combout\ & (\snake_ctrl_inst|cur_position.x[0]~6\ & VCC)))) # 
-- (!\snake_ctrl_inst|cur_position.x\(1) & ((\snake_ctrl_inst|Mux2~0_combout\ & ((\snake_ctrl_inst|cur_position.x[0]~6\) # (GND))) # (!\snake_ctrl_inst|Mux2~0_combout\ & (!\snake_ctrl_inst|cur_position.x[0]~6\))))
-- \snake_ctrl_inst|cur_position.x[1]~9\ = CARRY((\snake_ctrl_inst|cur_position.x\(1) & (\snake_ctrl_inst|Mux2~0_combout\ & !\snake_ctrl_inst|cur_position.x[0]~6\)) # (!\snake_ctrl_inst|cur_position.x\(1) & ((\snake_ctrl_inst|Mux2~0_combout\) # 
-- (!\snake_ctrl_inst|cur_position.x[0]~6\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100101001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_position.x\(1),
	datab => \snake_ctrl_inst|Mux2~0_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|cur_position.x[0]~6\,
	combout => \snake_ctrl_inst|cur_position.x[1]~8_combout\,
	cout => \snake_ctrl_inst|cur_position.x[1]~9\);

-- Location: IOIBUF_X54_Y73_N1
\init_head_position.x[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_init_head_position.x\(1),
	o => \init_head_position.x[1]~input_o\);

-- Location: FF_X74_Y68_N25
\snake_ctrl_inst|cur_position.x[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|cur_position.x[1]~8_combout\,
	asdata => \init_head_position.x[1]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \snake_ctrl_inst|ALT_INV_state.IDLE~q\,
	ena => \snake_ctrl_inst|cur_position.x[3]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|cur_position.x\(1));

-- Location: LCCOMB_X74_Y66_N14
\snake_ctrl_inst|Add12~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add12~7_combout\ = (\snake_ctrl_inst|Add12~6_combout\) # ((\snake_ctrl_inst|cur_position.x\(1) & !\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|Add12~6_combout\,
	datac => \snake_ctrl_inst|cur_position.x\(1),
	datad => \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\,
	combout => \snake_ctrl_inst|Add12~7_combout\);

-- Location: FF_X74_Y66_N15
\snake_ctrl_inst|snake_pos[0].tail_position.x[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Add12~7_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[0].tail_position.y[4]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].tail_position.x\(1));

-- Location: LCCOMB_X75_Y66_N20
\snake_ctrl_inst|rd_pos~66\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~66_combout\ = (\snake_ctrl_inst|rd~0_combout\ & ((\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].tail_position.x\(1)))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].tail_position.x\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[0].tail_position.x\(1),
	datab => \snake_ctrl_inst|rd~0_combout\,
	datac => \snake_ctrl_inst|snake_pos[1].tail_position.x\(1),
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|rd_pos~66_combout\);

-- Location: FF_X75_Y66_N21
\snake_ctrl_inst|rd_pos.x[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|rd_pos~66_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|rd_pos.x\(1));

-- Location: LCCOMB_X74_Y68_N26
\snake_ctrl_inst|cur_position.x[2]~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|cur_position.x[2]~10_combout\ = ((\snake_ctrl_inst|cur_position.x\(2) $ (\snake_ctrl_inst|Mux2~0_combout\ $ (\snake_ctrl_inst|cur_position.x[1]~9\)))) # (GND)
-- \snake_ctrl_inst|cur_position.x[2]~11\ = CARRY((\snake_ctrl_inst|cur_position.x\(2) & ((!\snake_ctrl_inst|cur_position.x[1]~9\) # (!\snake_ctrl_inst|Mux2~0_combout\))) # (!\snake_ctrl_inst|cur_position.x\(2) & (!\snake_ctrl_inst|Mux2~0_combout\ & 
-- !\snake_ctrl_inst|cur_position.x[1]~9\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_position.x\(2),
	datab => \snake_ctrl_inst|Mux2~0_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|cur_position.x[1]~9\,
	combout => \snake_ctrl_inst|cur_position.x[2]~10_combout\,
	cout => \snake_ctrl_inst|cur_position.x[2]~11\);

-- Location: IOIBUF_X52_Y73_N15
\init_head_position.x[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_init_head_position.x\(2),
	o => \init_head_position.x[2]~input_o\);

-- Location: FF_X74_Y68_N27
\snake_ctrl_inst|cur_position.x[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|cur_position.x[2]~10_combout\,
	asdata => \init_head_position.x[2]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \snake_ctrl_inst|ALT_INV_state.IDLE~q\,
	ena => \snake_ctrl_inst|cur_position.x[3]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|cur_position.x\(2));

-- Location: LCCOMB_X76_Y66_N22
\snake_ctrl_inst|Add12~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add12~10_combout\ = (\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\ & ((\snake_ctrl_inst|wr_data~6_combout\))) # (!\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\ & (\snake_ctrl_inst|cur_position.x\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_position.x\(2),
	datac => \snake_ctrl_inst|wr_data~6_combout\,
	datad => \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\,
	combout => \snake_ctrl_inst|Add12~10_combout\);

-- Location: FF_X76_Y66_N27
\snake_ctrl_inst|snake_pos[0].tail_position.x[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Add12~11_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[0].tail_position.y[4]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].tail_position.x\(2));

-- Location: LCCOMB_X76_Y66_N24
\snake_ctrl_inst|rd_pos~47\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~47_combout\ = (\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[1].tail_position.x\(2))) # (!\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[0].tail_position.x\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|cur_player~q\,
	datac => \snake_ctrl_inst|snake_pos[1].tail_position.x\(2),
	datad => \snake_ctrl_inst|snake_pos[0].tail_position.x\(2),
	combout => \snake_ctrl_inst|rd_pos~47_combout\);

-- Location: LCCOMB_X76_Y66_N14
\snake_ctrl_inst|Add12~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add12~8_combout\ = (\snake_ctrl_inst|rd_pos~47_combout\ & (\snake_ctrl_inst|Add12~5\ $ (GND))) # (!\snake_ctrl_inst|rd_pos~47_combout\ & (!\snake_ctrl_inst|Add12~5\ & VCC))
-- \snake_ctrl_inst|Add12~9\ = CARRY((\snake_ctrl_inst|rd_pos~47_combout\ & !\snake_ctrl_inst|Add12~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|rd_pos~47_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|Add12~5\,
	combout => \snake_ctrl_inst|Add12~8_combout\,
	cout => \snake_ctrl_inst|Add12~9\);

-- Location: LCCOMB_X75_Y66_N14
\snake_ctrl_inst|Add11~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add11~4_combout\ = ((\snake_ctrl_inst|tail_position~0_combout\ $ (\snake_ctrl_inst|rd_pos~47_combout\ $ (!\snake_ctrl_inst|Add11~3\)))) # (GND)
-- \snake_ctrl_inst|Add11~5\ = CARRY((\snake_ctrl_inst|tail_position~0_combout\ & ((\snake_ctrl_inst|rd_pos~47_combout\) # (!\snake_ctrl_inst|Add11~3\))) # (!\snake_ctrl_inst|tail_position~0_combout\ & (\snake_ctrl_inst|rd_pos~47_combout\ & 
-- !\snake_ctrl_inst|Add11~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|tail_position~0_combout\,
	datab => \snake_ctrl_inst|rd_pos~47_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|Add11~3\,
	combout => \snake_ctrl_inst|Add11~4_combout\,
	cout => \snake_ctrl_inst|Add11~5\);

-- Location: LCCOMB_X76_Y66_N26
\snake_ctrl_inst|Add12~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add12~11_combout\ = (\snake_ctrl_inst|Add12~10_combout\ & (((\snake_ctrl_inst|Add12~8_combout\)) # (!\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\))) # (!\snake_ctrl_inst|Add12~10_combout\ & (\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\ & 
-- ((\snake_ctrl_inst|Add11~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Add12~10_combout\,
	datab => \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\,
	datac => \snake_ctrl_inst|Add12~8_combout\,
	datad => \snake_ctrl_inst|Add11~4_combout\,
	combout => \snake_ctrl_inst|Add12~11_combout\);

-- Location: FF_X76_Y66_N25
\snake_ctrl_inst|snake_pos[1].tail_position.x[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snake_ctrl_inst|Add12~11_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snake_ctrl_inst|snake_pos[1].tail_position.y[4]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].tail_position.x\(2));

-- Location: LCCOMB_X75_Y66_N30
\snake_ctrl_inst|rd_pos~67\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~67_combout\ = (\snake_ctrl_inst|rd~0_combout\ & ((\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[1].tail_position.x\(2))) # (!\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[0].tail_position.x\(2))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_player~q\,
	datab => \snake_ctrl_inst|snake_pos[1].tail_position.x\(2),
	datac => \snake_ctrl_inst|rd~0_combout\,
	datad => \snake_ctrl_inst|snake_pos[0].tail_position.x\(2),
	combout => \snake_ctrl_inst|rd_pos~67_combout\);

-- Location: FF_X75_Y66_N31
\snake_ctrl_inst|rd_pos.x[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|rd_pos~67_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|rd_pos.x\(2));

-- Location: FF_X74_Y66_N3
\snake_ctrl_inst|snake_pos[0].tail_position.x[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Add12~15_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[0].tail_position.y[4]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].tail_position.x\(3));

-- Location: LCCOMB_X75_Y66_N2
\snake_ctrl_inst|rd_pos~48\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~48_combout\ = (\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[1].tail_position.x\(3))) # (!\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[0].tail_position.x\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[1].tail_position.x\(3),
	datab => \snake_ctrl_inst|cur_player~q\,
	datad => \snake_ctrl_inst|snake_pos[0].tail_position.x\(3),
	combout => \snake_ctrl_inst|rd_pos~48_combout\);

-- Location: LCCOMB_X75_Y66_N16
\snake_ctrl_inst|Add11~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add11~6_combout\ = (\snake_ctrl_inst|tail_position~0_combout\ & ((\snake_ctrl_inst|rd_pos~48_combout\ & (\snake_ctrl_inst|Add11~5\ & VCC)) # (!\snake_ctrl_inst|rd_pos~48_combout\ & (!\snake_ctrl_inst|Add11~5\)))) # 
-- (!\snake_ctrl_inst|tail_position~0_combout\ & ((\snake_ctrl_inst|rd_pos~48_combout\ & (!\snake_ctrl_inst|Add11~5\)) # (!\snake_ctrl_inst|rd_pos~48_combout\ & ((\snake_ctrl_inst|Add11~5\) # (GND)))))
-- \snake_ctrl_inst|Add11~7\ = CARRY((\snake_ctrl_inst|tail_position~0_combout\ & (!\snake_ctrl_inst|rd_pos~48_combout\ & !\snake_ctrl_inst|Add11~5\)) # (!\snake_ctrl_inst|tail_position~0_combout\ & ((!\snake_ctrl_inst|Add11~5\) # 
-- (!\snake_ctrl_inst|rd_pos~48_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|tail_position~0_combout\,
	datab => \snake_ctrl_inst|rd_pos~48_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|Add11~5\,
	combout => \snake_ctrl_inst|Add11~6_combout\,
	cout => \snake_ctrl_inst|Add11~7\);

-- Location: LCCOMB_X74_Y68_N28
\snake_ctrl_inst|cur_position.x[3]~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|cur_position.x[3]~12_combout\ = (\snake_ctrl_inst|cur_position.x\(3) & ((\snake_ctrl_inst|Mux2~0_combout\ & (!\snake_ctrl_inst|cur_position.x[2]~11\)) # (!\snake_ctrl_inst|Mux2~0_combout\ & (\snake_ctrl_inst|cur_position.x[2]~11\ & 
-- VCC)))) # (!\snake_ctrl_inst|cur_position.x\(3) & ((\snake_ctrl_inst|Mux2~0_combout\ & ((\snake_ctrl_inst|cur_position.x[2]~11\) # (GND))) # (!\snake_ctrl_inst|Mux2~0_combout\ & (!\snake_ctrl_inst|cur_position.x[2]~11\))))
-- \snake_ctrl_inst|cur_position.x[3]~13\ = CARRY((\snake_ctrl_inst|cur_position.x\(3) & (\snake_ctrl_inst|Mux2~0_combout\ & !\snake_ctrl_inst|cur_position.x[2]~11\)) # (!\snake_ctrl_inst|cur_position.x\(3) & ((\snake_ctrl_inst|Mux2~0_combout\) # 
-- (!\snake_ctrl_inst|cur_position.x[2]~11\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100101001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_position.x\(3),
	datab => \snake_ctrl_inst|Mux2~0_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|cur_position.x[2]~11\,
	combout => \snake_ctrl_inst|cur_position.x[3]~12_combout\,
	cout => \snake_ctrl_inst|cur_position.x[3]~13\);

-- Location: IOIBUF_X52_Y73_N8
\init_head_position.x[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_init_head_position.x\(3),
	o => \init_head_position.x[3]~input_o\);

-- Location: FF_X74_Y68_N29
\snake_ctrl_inst|cur_position.x[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|cur_position.x[3]~12_combout\,
	asdata => \init_head_position.x[3]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \snake_ctrl_inst|ALT_INV_state.IDLE~q\,
	ena => \snake_ctrl_inst|cur_position.x[3]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|cur_position.x\(3));

-- Location: LCCOMB_X74_Y66_N8
\snake_ctrl_inst|Add12~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add12~14_combout\ = (\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\ & (\snake_ctrl_inst|wr_data~6_combout\)) # (!\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\ & ((\snake_ctrl_inst|cur_position.x\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|wr_data~6_combout\,
	datab => \snake_ctrl_inst|cur_position.x\(3),
	datad => \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\,
	combout => \snake_ctrl_inst|Add12~14_combout\);

-- Location: LCCOMB_X76_Y66_N16
\snake_ctrl_inst|Add12~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add12~12_combout\ = (\snake_ctrl_inst|rd_pos~48_combout\ & (!\snake_ctrl_inst|Add12~9\)) # (!\snake_ctrl_inst|rd_pos~48_combout\ & ((\snake_ctrl_inst|Add12~9\) # (GND)))
-- \snake_ctrl_inst|Add12~13\ = CARRY((!\snake_ctrl_inst|Add12~9\) # (!\snake_ctrl_inst|rd_pos~48_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|rd_pos~48_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|Add12~9\,
	combout => \snake_ctrl_inst|Add12~12_combout\,
	cout => \snake_ctrl_inst|Add12~13\);

-- Location: LCCOMB_X74_Y66_N2
\snake_ctrl_inst|Add12~15\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add12~15_combout\ = (\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\ & ((\snake_ctrl_inst|Add12~14_combout\ & ((\snake_ctrl_inst|Add12~12_combout\))) # (!\snake_ctrl_inst|Add12~14_combout\ & (\snake_ctrl_inst|Add11~6_combout\)))) # 
-- (!\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\ & (((\snake_ctrl_inst|Add12~14_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100001011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\,
	datab => \snake_ctrl_inst|Add11~6_combout\,
	datac => \snake_ctrl_inst|Add12~14_combout\,
	datad => \snake_ctrl_inst|Add12~12_combout\,
	combout => \snake_ctrl_inst|Add12~15_combout\);

-- Location: FF_X74_Y66_N9
\snake_ctrl_inst|snake_pos[1].tail_position.x[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snake_ctrl_inst|Add12~15_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snake_ctrl_inst|snake_pos[1].tail_position.y[4]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].tail_position.x\(3));

-- Location: LCCOMB_X75_Y66_N24
\snake_ctrl_inst|rd_pos~68\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~68_combout\ = (\snake_ctrl_inst|rd~0_combout\ & ((\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[1].tail_position.x\(3))) # (!\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[0].tail_position.x\(3))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[1].tail_position.x\(3),
	datab => \snake_ctrl_inst|cur_player~q\,
	datac => \snake_ctrl_inst|rd~0_combout\,
	datad => \snake_ctrl_inst|snake_pos[0].tail_position.x\(3),
	combout => \snake_ctrl_inst|rd_pos~68_combout\);

-- Location: FF_X75_Y66_N25
\snake_ctrl_inst|rd_pos.x[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|rd_pos~68_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|rd_pos.x\(3));

-- Location: FF_X76_Y66_N29
\snake_ctrl_inst|snake_pos[1].tail_position.x[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snake_ctrl_inst|Add12~19_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snake_ctrl_inst|snake_pos[1].tail_position.y[4]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].tail_position.x\(4));

-- Location: LCCOMB_X76_Y66_N28
\snake_ctrl_inst|rd_pos~49\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~49_combout\ = (\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].tail_position.x\(4)))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].tail_position.x\(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[0].tail_position.x\(4),
	datab => \snake_ctrl_inst|cur_player~q\,
	datac => \snake_ctrl_inst|snake_pos[1].tail_position.x\(4),
	combout => \snake_ctrl_inst|rd_pos~49_combout\);

-- Location: LCCOMB_X75_Y66_N18
\snake_ctrl_inst|Add11~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add11~8_combout\ = \snake_ctrl_inst|tail_position~0_combout\ $ (\snake_ctrl_inst|Add11~7\ $ (!\snake_ctrl_inst|rd_pos~49_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010100101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|tail_position~0_combout\,
	datad => \snake_ctrl_inst|rd_pos~49_combout\,
	cin => \snake_ctrl_inst|Add11~7\,
	combout => \snake_ctrl_inst|Add11~8_combout\);

-- Location: LCCOMB_X76_Y66_N18
\snake_ctrl_inst|Add12~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add12~16_combout\ = \snake_ctrl_inst|Add12~13\ $ (!\snake_ctrl_inst|rd_pos~49_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \snake_ctrl_inst|rd_pos~49_combout\,
	cin => \snake_ctrl_inst|Add12~13\,
	combout => \snake_ctrl_inst|Add12~16_combout\);

-- Location: LCCOMB_X74_Y68_N30
\snake_ctrl_inst|cur_position.x[4]~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|cur_position.x[4]~14_combout\ = \snake_ctrl_inst|cur_position.x\(4) $ (\snake_ctrl_inst|Mux2~0_combout\ $ (\snake_ctrl_inst|cur_position.x[3]~13\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_position.x\(4),
	datab => \snake_ctrl_inst|Mux2~0_combout\,
	cin => \snake_ctrl_inst|cur_position.x[3]~13\,
	combout => \snake_ctrl_inst|cur_position.x[4]~14_combout\);

-- Location: IOIBUF_X47_Y73_N1
\init_head_position.x[4]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_init_head_position.x\(4),
	o => \init_head_position.x[4]~input_o\);

-- Location: FF_X74_Y68_N31
\snake_ctrl_inst|cur_position.x[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|cur_position.x[4]~14_combout\,
	asdata => \init_head_position.x[4]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \snake_ctrl_inst|ALT_INV_state.IDLE~q\,
	ena => \snake_ctrl_inst|cur_position.x[3]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|cur_position.x\(4));

-- Location: LCCOMB_X76_Y66_N0
\snake_ctrl_inst|Add12~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add12~18_combout\ = (\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\ & ((\snake_ctrl_inst|wr_data~6_combout\))) # (!\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\ & (\snake_ctrl_inst|cur_position.x\(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_position.x\(4),
	datac => \snake_ctrl_inst|wr_data~6_combout\,
	datad => \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\,
	combout => \snake_ctrl_inst|Add12~18_combout\);

-- Location: LCCOMB_X76_Y66_N30
\snake_ctrl_inst|Add12~19\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add12~19_combout\ = (\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\ & ((\snake_ctrl_inst|Add12~18_combout\ & ((\snake_ctrl_inst|Add12~16_combout\))) # (!\snake_ctrl_inst|Add12~18_combout\ & (\snake_ctrl_inst|Add11~8_combout\)))) # 
-- (!\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\ & (((\snake_ctrl_inst|Add12~18_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\,
	datab => \snake_ctrl_inst|Add11~8_combout\,
	datac => \snake_ctrl_inst|Add12~16_combout\,
	datad => \snake_ctrl_inst|Add12~18_combout\,
	combout => \snake_ctrl_inst|Add12~19_combout\);

-- Location: FF_X76_Y66_N31
\snake_ctrl_inst|snake_pos[0].tail_position.x[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Add12~19_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[0].tail_position.y[4]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].tail_position.x\(4));

-- Location: LCCOMB_X75_Y66_N26
\snake_ctrl_inst|rd_pos~69\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~69_combout\ = (\snake_ctrl_inst|rd~0_combout\ & ((\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].tail_position.x\(4)))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].tail_position.x\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[0].tail_position.x\(4),
	datab => \snake_ctrl_inst|cur_player~q\,
	datac => \snake_ctrl_inst|rd~0_combout\,
	datad => \snake_ctrl_inst|snake_pos[1].tail_position.x\(4),
	combout => \snake_ctrl_inst|rd_pos~69_combout\);

-- Location: FF_X75_Y66_N27
\snake_ctrl_inst|rd_pos.x[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|rd_pos~69_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|rd_pos.x\(4));

-- Location: LCCOMB_X72_Y69_N16
\snake_ctrl_inst|Selector120~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector120~0_combout\ = (\snake_ctrl_inst|state.PLACE_HEAD~q\) # ((\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\) # ((\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\) # (!\snake_ctrl_inst|Selector39~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|state.PLACE_HEAD~q\,
	datab => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	datac => \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\,
	datad => \snake_ctrl_inst|Selector39~0_combout\,
	combout => \snake_ctrl_inst|Selector120~0_combout\);

-- Location: LCCOMB_X72_Y69_N2
\snake_ctrl_inst|Selector120~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector120~1_combout\ = (!\busy~input_o\ & ((\snake_ctrl_inst|Selector120~0_combout\) # (\snake_ctrl_inst|Selector131~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \busy~input_o\,
	datab => \snake_ctrl_inst|Selector120~0_combout\,
	datad => \snake_ctrl_inst|Selector131~0_combout\,
	combout => \snake_ctrl_inst|Selector120~1_combout\);

-- Location: FF_X72_Y69_N3
\snake_ctrl_inst|wr\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector120~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|wr~q\);

-- Location: LCCOMB_X74_Y66_N4
\snake_ctrl_inst|rd_pos~50\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~50_combout\ = (!\busy~input_o\ & ((\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].tail_position.y\(0)))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].tail_position.y\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_player~q\,
	datab => \busy~input_o\,
	datac => \snake_ctrl_inst|snake_pos[0].tail_position.y\(0),
	datad => \snake_ctrl_inst|snake_pos[1].tail_position.y\(0),
	combout => \snake_ctrl_inst|rd_pos~50_combout\);

-- Location: LCCOMB_X72_Y69_N28
\snake_ctrl_inst|wr_pos.y[3]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_pos.y[3]~5_combout\ = (!\snake_ctrl_inst|state.PLACE_HEAD~q\ & (!\snake_ctrl_inst|state.PLACE_TAIL~q\ & !\snake_ctrl_inst|Selector131~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|state.PLACE_HEAD~q\,
	datac => \snake_ctrl_inst|state.PLACE_TAIL~q\,
	datad => \snake_ctrl_inst|Selector131~0_combout\,
	combout => \snake_ctrl_inst|wr_pos.y[3]~5_combout\);

-- Location: LCCOMB_X73_Y66_N0
\snake_ctrl_inst|wr_pos.y[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_pos.y[0]~0_combout\ = (\snake_ctrl_inst|wr_pos.y[3]~5_combout\ & (\snake_ctrl_inst|rd_pos~50_combout\)) # (!\snake_ctrl_inst|wr_pos.y[3]~5_combout\ & ((\snake_ctrl_inst|cur_position.y\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|rd_pos~50_combout\,
	datab => \snake_ctrl_inst|wr_pos.y[3]~5_combout\,
	datad => \snake_ctrl_inst|cur_position.y\(0),
	combout => \snake_ctrl_inst|wr_pos.y[0]~0_combout\);

-- Location: LCCOMB_X70_Y67_N16
\snake_ctrl_inst|Selector101~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector101~1_combout\ = (\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\ & ((\snake_ctrl_inst|target_movement_direction.RIGHT~q\) # (\snake_ctrl_inst|target_movement_direction.LEFT~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|target_movement_direction.RIGHT~q\,
	datac => \snake_ctrl_inst|target_movement_direction.LEFT~q\,
	datad => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	combout => \snake_ctrl_inst|Selector101~1_combout\);

-- Location: LCCOMB_X70_Y67_N10
\snake_ctrl_inst|Selector101~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector101~2_combout\ = (\snake_ctrl_inst|wr_pos~0_combout\ & ((\snake_ctrl_inst|Selector101~1_combout\) # ((\init_head_position.y[0]~input_o\ & !\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\)))) # (!\snake_ctrl_inst|wr_pos~0_combout\ & 
-- (((\init_head_position.y[0]~input_o\ & !\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|wr_pos~0_combout\,
	datab => \snake_ctrl_inst|Selector101~1_combout\,
	datac => \init_head_position.y[0]~input_o\,
	datad => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	combout => \snake_ctrl_inst|Selector101~2_combout\);

-- Location: LCCOMB_X74_Y68_N20
\snake_ctrl_inst|Selector100~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector100~0_combout\ = (!\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\ & \init_head_position.y[1]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	datac => \init_head_position.y[1]~input_o\,
	combout => \snake_ctrl_inst|Selector100~0_combout\);

-- Location: LCCOMB_X69_Y65_N0
\snake_ctrl_inst|Selector98~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector98~0_combout\ = (\snake_ctrl_inst|target_movement_direction.DOWN~q\ & (!\snake_ctrl_inst|target_movement_direction.RIGHT~q\ & (!\snake_ctrl_inst|target_movement_direction.LEFT~q\ & \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|target_movement_direction.DOWN~q\,
	datab => \snake_ctrl_inst|target_movement_direction.RIGHT~q\,
	datac => \snake_ctrl_inst|target_movement_direction.LEFT~q\,
	datad => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	combout => \snake_ctrl_inst|Selector98~0_combout\);

-- Location: LCCOMB_X70_Y68_N26
\snake_ctrl_inst|Selector101~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector101~3_combout\ = (!\snake_ctrl_inst|target_movement_direction.DOWN~q\ & (!\snake_ctrl_inst|target_movement_direction.LEFT~q\ & (!\snake_ctrl_inst|target_movement_direction.RIGHT~q\ & \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|target_movement_direction.DOWN~q\,
	datab => \snake_ctrl_inst|target_movement_direction.LEFT~q\,
	datac => \snake_ctrl_inst|target_movement_direction.RIGHT~q\,
	datad => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	combout => \snake_ctrl_inst|Selector101~3_combout\);

-- Location: LCCOMB_X69_Y67_N18
\snake_ctrl_inst|Add6~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add6~0_combout\ = \snake_ctrl_inst|wr_pos~0_combout\ $ (VCC)
-- \snake_ctrl_inst|Add6~1\ = CARRY(\snake_ctrl_inst|wr_pos~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|wr_pos~0_combout\,
	datad => VCC,
	combout => \snake_ctrl_inst|Add6~0_combout\,
	cout => \snake_ctrl_inst|Add6~1\);

-- Location: LCCOMB_X69_Y67_N20
\snake_ctrl_inst|Add6~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add6~2_combout\ = (\snake_ctrl_inst|wr_pos~1_combout\ & (!\snake_ctrl_inst|Add6~1\)) # (!\snake_ctrl_inst|wr_pos~1_combout\ & ((\snake_ctrl_inst|Add6~1\) # (GND)))
-- \snake_ctrl_inst|Add6~3\ = CARRY((!\snake_ctrl_inst|Add6~1\) # (!\snake_ctrl_inst|wr_pos~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|wr_pos~1_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|Add6~1\,
	combout => \snake_ctrl_inst|Add6~2_combout\,
	cout => \snake_ctrl_inst|Add6~3\);

-- Location: LCCOMB_X69_Y67_N22
\snake_ctrl_inst|Add6~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add6~4_combout\ = (\snake_ctrl_inst|wr_pos~2_combout\ & (\snake_ctrl_inst|Add6~3\ $ (GND))) # (!\snake_ctrl_inst|wr_pos~2_combout\ & (!\snake_ctrl_inst|Add6~3\ & VCC))
-- \snake_ctrl_inst|Add6~5\ = CARRY((\snake_ctrl_inst|wr_pos~2_combout\ & !\snake_ctrl_inst|Add6~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|wr_pos~2_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|Add6~3\,
	combout => \snake_ctrl_inst|Add6~4_combout\,
	cout => \snake_ctrl_inst|Add6~5\);

-- Location: LCCOMB_X70_Y67_N22
\snake_ctrl_inst|Add5~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add5~0_combout\ = \snake_ctrl_inst|wr_pos~0_combout\ $ (VCC)
-- \snake_ctrl_inst|Add5~1\ = CARRY(\snake_ctrl_inst|wr_pos~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|wr_pos~0_combout\,
	datad => VCC,
	combout => \snake_ctrl_inst|Add5~0_combout\,
	cout => \snake_ctrl_inst|Add5~1\);

-- Location: LCCOMB_X70_Y67_N24
\snake_ctrl_inst|Add5~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add5~2_combout\ = (\snake_ctrl_inst|wr_pos~1_combout\ & (\snake_ctrl_inst|Add5~1\ & VCC)) # (!\snake_ctrl_inst|wr_pos~1_combout\ & (!\snake_ctrl_inst|Add5~1\))
-- \snake_ctrl_inst|Add5~3\ = CARRY((!\snake_ctrl_inst|wr_pos~1_combout\ & !\snake_ctrl_inst|Add5~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|wr_pos~1_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|Add5~1\,
	combout => \snake_ctrl_inst|Add5~2_combout\,
	cout => \snake_ctrl_inst|Add5~3\);

-- Location: LCCOMB_X70_Y67_N26
\snake_ctrl_inst|Add5~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add5~4_combout\ = (\snake_ctrl_inst|wr_pos~2_combout\ & ((GND) # (!\snake_ctrl_inst|Add5~3\))) # (!\snake_ctrl_inst|wr_pos~2_combout\ & (\snake_ctrl_inst|Add5~3\ $ (GND)))
-- \snake_ctrl_inst|Add5~5\ = CARRY((\snake_ctrl_inst|wr_pos~2_combout\) # (!\snake_ctrl_inst|Add5~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|wr_pos~2_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|Add5~3\,
	combout => \snake_ctrl_inst|Add5~4_combout\,
	cout => \snake_ctrl_inst|Add5~5\);

-- Location: LCCOMB_X69_Y67_N4
\snake_ctrl_inst|Selector99~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector99~1_combout\ = (\snake_ctrl_inst|Selector101~3_combout\ & ((\snake_ctrl_inst|Add5~4_combout\) # ((\snake_ctrl_inst|Selector101~1_combout\ & \snake_ctrl_inst|wr_pos~2_combout\)))) # (!\snake_ctrl_inst|Selector101~3_combout\ & 
-- (\snake_ctrl_inst|Selector101~1_combout\ & ((\snake_ctrl_inst|wr_pos~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector101~3_combout\,
	datab => \snake_ctrl_inst|Selector101~1_combout\,
	datac => \snake_ctrl_inst|Add5~4_combout\,
	datad => \snake_ctrl_inst|wr_pos~2_combout\,
	combout => \snake_ctrl_inst|Selector99~1_combout\);

-- Location: LCCOMB_X74_Y68_N6
\snake_ctrl_inst|Selector99~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector99~0_combout\ = (!\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\ & \init_head_position.y[2]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	datac => \init_head_position.y[2]~input_o\,
	combout => \snake_ctrl_inst|Selector99~0_combout\);

-- Location: LCCOMB_X69_Y67_N30
\snake_ctrl_inst|Selector99~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector99~2_combout\ = (\snake_ctrl_inst|Selector99~1_combout\) # ((\snake_ctrl_inst|Selector99~0_combout\) # ((\snake_ctrl_inst|Add6~4_combout\ & \snake_ctrl_inst|Selector101~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Add6~4_combout\,
	datab => \snake_ctrl_inst|Selector101~6_combout\,
	datac => \snake_ctrl_inst|Selector99~1_combout\,
	datad => \snake_ctrl_inst|Selector99~0_combout\,
	combout => \snake_ctrl_inst|Selector99~2_combout\);

-- Location: FF_X69_Y67_N31
\snake_ctrl_inst|snake_pos[0].head_position.y[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector99~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[0].head_position.y[0]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].head_position.y\(2));

-- Location: FF_X69_Y67_N29
\snake_ctrl_inst|snake_pos[1].head_position.y[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snake_ctrl_inst|Selector99~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snake_ctrl_inst|snake_pos[1].head_position.x[2]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].head_position.y\(2));

-- Location: LCCOMB_X69_Y67_N28
\snake_ctrl_inst|wr_pos~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_pos~2_combout\ = (\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].head_position.y\(2)))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].head_position.y\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[0].head_position.y\(2),
	datac => \snake_ctrl_inst|snake_pos[1].head_position.y\(2),
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|wr_pos~2_combout\);

-- Location: LCCOMB_X70_Y67_N4
\snake_ctrl_inst|Equal1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Equal1~0_combout\ = (!\snake_ctrl_inst|wr_pos~3_combout\ & (!\snake_ctrl_inst|wr_pos~2_combout\ & (!\snake_ctrl_inst|wr_pos~1_combout\ & !\snake_ctrl_inst|wr_pos~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|wr_pos~3_combout\,
	datab => \snake_ctrl_inst|wr_pos~2_combout\,
	datac => \snake_ctrl_inst|wr_pos~1_combout\,
	datad => \snake_ctrl_inst|wr_pos~0_combout\,
	combout => \snake_ctrl_inst|Equal1~0_combout\);

-- Location: LCCOMB_X70_Y67_N6
\snake_ctrl_inst|Selector101~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector101~4_combout\ = (\snake_ctrl_inst|Selector101~3_combout\ & ((\snake_ctrl_inst|wr_pos~4_combout\) # (!\snake_ctrl_inst|Equal1~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000010110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|wr_pos~4_combout\,
	datab => \snake_ctrl_inst|Equal1~0_combout\,
	datac => \snake_ctrl_inst|Selector101~3_combout\,
	combout => \snake_ctrl_inst|Selector101~4_combout\);

-- Location: LCCOMB_X70_Y67_N28
\snake_ctrl_inst|Add5~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add5~6_combout\ = (\snake_ctrl_inst|wr_pos~3_combout\ & (\snake_ctrl_inst|Add5~5\ & VCC)) # (!\snake_ctrl_inst|wr_pos~3_combout\ & (!\snake_ctrl_inst|Add5~5\))
-- \snake_ctrl_inst|Add5~7\ = CARRY((!\snake_ctrl_inst|wr_pos~3_combout\ & !\snake_ctrl_inst|Add5~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|wr_pos~3_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|Add5~5\,
	combout => \snake_ctrl_inst|Add5~6_combout\,
	cout => \snake_ctrl_inst|Add5~7\);

-- Location: LCCOMB_X69_Y67_N24
\snake_ctrl_inst|Add6~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add6~6_combout\ = (\snake_ctrl_inst|wr_pos~3_combout\ & (!\snake_ctrl_inst|Add6~5\)) # (!\snake_ctrl_inst|wr_pos~3_combout\ & ((\snake_ctrl_inst|Add6~5\) # (GND)))
-- \snake_ctrl_inst|Add6~7\ = CARRY((!\snake_ctrl_inst|Add6~5\) # (!\snake_ctrl_inst|wr_pos~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|wr_pos~3_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|Add6~5\,
	combout => \snake_ctrl_inst|Add6~6_combout\,
	cout => \snake_ctrl_inst|Add6~7\);

-- Location: LCCOMB_X69_Y67_N6
\snake_ctrl_inst|Selector98~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector98~2_combout\ = (\snake_ctrl_inst|Add6~6_combout\ & ((\snake_ctrl_inst|Selector98~0_combout\) # ((\snake_ctrl_inst|wr_pos~3_combout\ & \snake_ctrl_inst|Selector101~1_combout\)))) # (!\snake_ctrl_inst|Add6~6_combout\ & 
-- (\snake_ctrl_inst|wr_pos~3_combout\ & ((\snake_ctrl_inst|Selector101~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Add6~6_combout\,
	datab => \snake_ctrl_inst|wr_pos~3_combout\,
	datac => \snake_ctrl_inst|Selector98~0_combout\,
	datad => \snake_ctrl_inst|Selector101~1_combout\,
	combout => \snake_ctrl_inst|Selector98~2_combout\);

-- Location: LCCOMB_X70_Y67_N2
\snake_ctrl_inst|Selector98~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector98~1_combout\ = (!\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\ & \init_head_position.y[3]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	datad => \init_head_position.y[3]~input_o\,
	combout => \snake_ctrl_inst|Selector98~1_combout\);

-- Location: LCCOMB_X70_Y67_N14
\snake_ctrl_inst|Selector98~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector98~3_combout\ = (\snake_ctrl_inst|Selector98~2_combout\) # ((\snake_ctrl_inst|Selector98~1_combout\) # ((\snake_ctrl_inst|Selector101~4_combout\ & \snake_ctrl_inst|Add5~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector101~4_combout\,
	datab => \snake_ctrl_inst|Add5~6_combout\,
	datac => \snake_ctrl_inst|Selector98~2_combout\,
	datad => \snake_ctrl_inst|Selector98~1_combout\,
	combout => \snake_ctrl_inst|Selector98~3_combout\);

-- Location: FF_X70_Y67_N13
\snake_ctrl_inst|snake_pos[1].head_position.y[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snake_ctrl_inst|Selector98~3_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snake_ctrl_inst|snake_pos[1].head_position.x[2]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].head_position.y\(3));

-- Location: FF_X70_Y67_N15
\snake_ctrl_inst|snake_pos[0].head_position.y[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector98~3_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[0].head_position.y[0]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].head_position.y\(3));

-- Location: LCCOMB_X70_Y67_N12
\snake_ctrl_inst|wr_pos~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_pos~3_combout\ = (\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[1].head_position.y\(3))) # (!\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[0].head_position.y\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_player~q\,
	datac => \snake_ctrl_inst|snake_pos[1].head_position.y\(3),
	datad => \snake_ctrl_inst|snake_pos[0].head_position.y\(3),
	combout => \snake_ctrl_inst|wr_pos~3_combout\);

-- Location: LCCOMB_X70_Y67_N30
\snake_ctrl_inst|Add5~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add5~8_combout\ = \snake_ctrl_inst|Add5~7\ $ (\snake_ctrl_inst|wr_pos~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \snake_ctrl_inst|wr_pos~4_combout\,
	cin => \snake_ctrl_inst|Add5~7\,
	combout => \snake_ctrl_inst|Add5~8_combout\);

-- Location: LCCOMB_X70_Y67_N20
\snake_ctrl_inst|Selector97~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector97~1_combout\ = (\snake_ctrl_inst|wr_pos~4_combout\ & ((\snake_ctrl_inst|Selector101~1_combout\) # ((\snake_ctrl_inst|Selector101~3_combout\ & \snake_ctrl_inst|Add5~8_combout\)))) # (!\snake_ctrl_inst|wr_pos~4_combout\ & 
-- (((\snake_ctrl_inst|Selector101~3_combout\ & \snake_ctrl_inst|Add5~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|wr_pos~4_combout\,
	datab => \snake_ctrl_inst|Selector101~1_combout\,
	datac => \snake_ctrl_inst|Selector101~3_combout\,
	datad => \snake_ctrl_inst|Add5~8_combout\,
	combout => \snake_ctrl_inst|Selector97~1_combout\);

-- Location: LCCOMB_X69_Y67_N26
\snake_ctrl_inst|Add6~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add6~8_combout\ = \snake_ctrl_inst|Add6~7\ $ (!\snake_ctrl_inst|wr_pos~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \snake_ctrl_inst|wr_pos~4_combout\,
	cin => \snake_ctrl_inst|Add6~7\,
	combout => \snake_ctrl_inst|Add6~8_combout\);

-- Location: LCCOMB_X70_Y68_N20
\snake_ctrl_inst|Selector97~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector97~0_combout\ = (\init_head_position.y[4]~input_o\ & !\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \init_head_position.y[4]~input_o\,
	datad => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	combout => \snake_ctrl_inst|Selector97~0_combout\);

-- Location: LCCOMB_X69_Y67_N10
\snake_ctrl_inst|Selector97~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector97~2_combout\ = (\snake_ctrl_inst|Selector97~1_combout\) # ((\snake_ctrl_inst|Selector97~0_combout\) # ((\snake_ctrl_inst|Selector101~6_combout\ & \snake_ctrl_inst|Add6~8_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector97~1_combout\,
	datab => \snake_ctrl_inst|Selector101~6_combout\,
	datac => \snake_ctrl_inst|Add6~8_combout\,
	datad => \snake_ctrl_inst|Selector97~0_combout\,
	combout => \snake_ctrl_inst|Selector97~2_combout\);

-- Location: FF_X69_Y67_N11
\snake_ctrl_inst|snake_pos[0].head_position.y[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector97~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[0].head_position.y[0]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].head_position.y\(4));

-- Location: FF_X69_Y67_N9
\snake_ctrl_inst|snake_pos[1].head_position.y[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snake_ctrl_inst|Selector97~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snake_ctrl_inst|snake_pos[1].head_position.x[2]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].head_position.y\(4));

-- Location: LCCOMB_X69_Y67_N8
\snake_ctrl_inst|wr_pos~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_pos~4_combout\ = (\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].head_position.y\(4)))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].head_position.y\(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[0].head_position.y\(4),
	datac => \snake_ctrl_inst|snake_pos[1].head_position.y\(4),
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|wr_pos~4_combout\);

-- Location: LCCOMB_X69_Y67_N0
\snake_ctrl_inst|Selector101~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector101~6_combout\ = (\snake_ctrl_inst|Selector98~0_combout\ & ((!\snake_ctrl_inst|wr_pos~4_combout\) # (!\snake_ctrl_inst|Equal2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Equal2~0_combout\,
	datac => \snake_ctrl_inst|Selector98~0_combout\,
	datad => \snake_ctrl_inst|wr_pos~4_combout\,
	combout => \snake_ctrl_inst|Selector101~6_combout\);

-- Location: LCCOMB_X70_Y67_N8
\snake_ctrl_inst|Selector100~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector100~1_combout\ = (\snake_ctrl_inst|Selector101~3_combout\ & ((\snake_ctrl_inst|Add5~2_combout\) # ((\snake_ctrl_inst|wr_pos~1_combout\ & \snake_ctrl_inst|Selector101~1_combout\)))) # (!\snake_ctrl_inst|Selector101~3_combout\ & 
-- (((\snake_ctrl_inst|wr_pos~1_combout\ & \snake_ctrl_inst|Selector101~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector101~3_combout\,
	datab => \snake_ctrl_inst|Add5~2_combout\,
	datac => \snake_ctrl_inst|wr_pos~1_combout\,
	datad => \snake_ctrl_inst|Selector101~1_combout\,
	combout => \snake_ctrl_inst|Selector100~1_combout\);

-- Location: LCCOMB_X69_Y67_N2
\snake_ctrl_inst|Selector100~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector100~2_combout\ = (\snake_ctrl_inst|Selector100~0_combout\) # ((\snake_ctrl_inst|Selector100~1_combout\) # ((\snake_ctrl_inst|Selector101~6_combout\ & \snake_ctrl_inst|Add6~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector100~0_combout\,
	datab => \snake_ctrl_inst|Selector101~6_combout\,
	datac => \snake_ctrl_inst|Selector100~1_combout\,
	datad => \snake_ctrl_inst|Add6~2_combout\,
	combout => \snake_ctrl_inst|Selector100~2_combout\);

-- Location: FF_X69_Y67_N3
\snake_ctrl_inst|snake_pos[0].head_position.y[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector100~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[0].head_position.y[0]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].head_position.y\(1));

-- Location: FF_X69_Y67_N17
\snake_ctrl_inst|snake_pos[1].head_position.y[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snake_ctrl_inst|Selector100~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snake_ctrl_inst|snake_pos[1].head_position.x[2]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].head_position.y\(1));

-- Location: LCCOMB_X69_Y67_N16
\snake_ctrl_inst|wr_pos~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_pos~1_combout\ = (\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].head_position.y\(1)))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].head_position.y\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|snake_pos[0].head_position.y\(1),
	datac => \snake_ctrl_inst|snake_pos[1].head_position.y\(1),
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|wr_pos~1_combout\);

-- Location: LCCOMB_X69_Y67_N12
\snake_ctrl_inst|Equal2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Equal2~0_combout\ = (\snake_ctrl_inst|wr_pos~1_combout\ & (!\snake_ctrl_inst|wr_pos~0_combout\ & (!\snake_ctrl_inst|wr_pos~3_combout\ & \snake_ctrl_inst|wr_pos~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|wr_pos~1_combout\,
	datab => \snake_ctrl_inst|wr_pos~0_combout\,
	datac => \snake_ctrl_inst|wr_pos~3_combout\,
	datad => \snake_ctrl_inst|wr_pos~2_combout\,
	combout => \snake_ctrl_inst|Equal2~0_combout\);

-- Location: LCCOMB_X69_Y67_N14
\snake_ctrl_inst|Selector101~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector101~0_combout\ = (\snake_ctrl_inst|Add6~0_combout\ & (\snake_ctrl_inst|Selector98~0_combout\ & ((!\snake_ctrl_inst|wr_pos~4_combout\) # (!\snake_ctrl_inst|Equal2~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Equal2~0_combout\,
	datab => \snake_ctrl_inst|Add6~0_combout\,
	datac => \snake_ctrl_inst|Selector98~0_combout\,
	datad => \snake_ctrl_inst|wr_pos~4_combout\,
	combout => \snake_ctrl_inst|Selector101~0_combout\);

-- Location: LCCOMB_X70_Y67_N18
\snake_ctrl_inst|Selector101~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector101~5_combout\ = (\snake_ctrl_inst|Selector101~2_combout\) # ((\snake_ctrl_inst|Selector101~0_combout\) # ((\snake_ctrl_inst|Add5~0_combout\ & \snake_ctrl_inst|Selector101~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector101~2_combout\,
	datab => \snake_ctrl_inst|Selector101~0_combout\,
	datac => \snake_ctrl_inst|Add5~0_combout\,
	datad => \snake_ctrl_inst|Selector101~4_combout\,
	combout => \snake_ctrl_inst|Selector101~5_combout\);

-- Location: FF_X70_Y67_N19
\snake_ctrl_inst|snake_pos[0].head_position.y[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector101~5_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[0].head_position.y[0]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].head_position.y\(0));

-- Location: FF_X70_Y67_N1
\snake_ctrl_inst|snake_pos[1].head_position.y[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snake_ctrl_inst|Selector101~5_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snake_ctrl_inst|snake_pos[1].head_position.x[2]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].head_position.y\(0));

-- Location: LCCOMB_X70_Y67_N0
\snake_ctrl_inst|wr_pos~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_pos~0_combout\ = (\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].head_position.y\(0)))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].head_position.y\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_player~q\,
	datab => \snake_ctrl_inst|snake_pos[0].head_position.y\(0),
	datac => \snake_ctrl_inst|snake_pos[1].head_position.y\(0),
	combout => \snake_ctrl_inst|wr_pos~0_combout\);

-- Location: LCCOMB_X72_Y69_N22
\snake_ctrl_inst|wr_pos.y[3]~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_pos.y[3]~6_combout\ = (\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\) # (\snake_ctrl_inst|state.DRAW_NEW_HEAD~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	datac => \snake_ctrl_inst|state.DRAW_NEW_HEAD~q\,
	combout => \snake_ctrl_inst|wr_pos.y[3]~6_combout\);

-- Location: LCCOMB_X72_Y69_N0
\snake_ctrl_inst|wr_pos.y[3]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_pos.y[3]~7_combout\ = ((\snake_ctrl_inst|state.GET_OLD_TAIL_TYPE~q\) # ((\snake_ctrl_inst|wr_pos.y[3]~6_combout\ & \busy~input_o\))) # (!\snake_ctrl_inst|state.IDLE~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101111110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|wr_pos.y[3]~6_combout\,
	datab => \snake_ctrl_inst|state.IDLE~q\,
	datac => \snake_ctrl_inst|state.GET_OLD_TAIL_TYPE~q\,
	datad => \busy~input_o\,
	combout => \snake_ctrl_inst|wr_pos.y[3]~7_combout\);

-- Location: LCCOMB_X75_Y69_N2
\snake_ctrl_inst|wr_pos.y[3]~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_pos.y[3]~9_combout\ = (\snake_ctrl_inst|wr_pos.y[3]~8_combout\ & (((!\snake_ctrl_inst|Equal0~0_combout\) # (!\snake_ctrl_inst|Equal0~1_combout\)) # (!\snake_ctrl_inst|state.PLACE_BODY~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|wr_pos.y[3]~8_combout\,
	datab => \snake_ctrl_inst|state.PLACE_BODY~q\,
	datac => \snake_ctrl_inst|Equal0~1_combout\,
	datad => \snake_ctrl_inst|Equal0~0_combout\,
	combout => \snake_ctrl_inst|wr_pos.y[3]~9_combout\);

-- Location: LCCOMB_X73_Y66_N12
\snake_ctrl_inst|wr_pos.y[3]~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_pos.y[3]~10_combout\ = (\snake_ctrl_inst|wr_pos.y[3]~7_combout\) # (((!\snake_ctrl_inst|wr_pos.y[3]~5_combout\ & \busy~input_o\)) # (!\snake_ctrl_inst|wr_pos.y[3]~9_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011111110101111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|wr_pos.y[3]~7_combout\,
	datab => \snake_ctrl_inst|wr_pos.y[3]~5_combout\,
	datac => \snake_ctrl_inst|wr_pos.y[3]~9_combout\,
	datad => \busy~input_o\,
	combout => \snake_ctrl_inst|wr_pos.y[3]~10_combout\);

-- Location: FF_X73_Y66_N1
\snake_ctrl_inst|wr_pos.y[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|wr_pos.y[0]~0_combout\,
	asdata => \snake_ctrl_inst|wr_pos~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snake_ctrl_inst|wr_pos.y[3]~10_combout\,
	sload => \snake_ctrl_inst|wr_pos.y[3]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|wr_pos.y\(0));

-- Location: LCCOMB_X74_Y65_N12
\snake_ctrl_inst|rd_pos~51\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~51_combout\ = (!\busy~input_o\ & ((\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[1].tail_position.y\(1))) # (!\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[0].tail_position.y\(1))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[1].tail_position.y\(1),
	datab => \snake_ctrl_inst|snake_pos[0].tail_position.y\(1),
	datac => \busy~input_o\,
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|rd_pos~51_combout\);

-- Location: LCCOMB_X73_Y66_N26
\snake_ctrl_inst|wr_pos.y[1]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_pos.y[1]~1_combout\ = (\snake_ctrl_inst|wr_pos.y[3]~5_combout\ & (\snake_ctrl_inst|rd_pos~51_combout\)) # (!\snake_ctrl_inst|wr_pos.y[3]~5_combout\ & ((\snake_ctrl_inst|cur_position.y\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|rd_pos~51_combout\,
	datab => \snake_ctrl_inst|wr_pos.y[3]~5_combout\,
	datad => \snake_ctrl_inst|cur_position.y\(1),
	combout => \snake_ctrl_inst|wr_pos.y[1]~1_combout\);

-- Location: FF_X73_Y66_N27
\snake_ctrl_inst|wr_pos.y[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|wr_pos.y[1]~1_combout\,
	asdata => \snake_ctrl_inst|wr_pos~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snake_ctrl_inst|wr_pos.y[3]~10_combout\,
	sload => \snake_ctrl_inst|wr_pos.y[3]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|wr_pos.y\(1));

-- Location: LCCOMB_X74_Y65_N0
\snake_ctrl_inst|rd_pos~52\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~52_combout\ = (!\busy~input_o\ & ((\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].tail_position.y\(2)))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].tail_position.y\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[0].tail_position.y\(2),
	datab => \snake_ctrl_inst|snake_pos[1].tail_position.y\(2),
	datac => \busy~input_o\,
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|rd_pos~52_combout\);

-- Location: LCCOMB_X73_Y66_N20
\snake_ctrl_inst|wr_pos.y[2]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_pos.y[2]~2_combout\ = (\snake_ctrl_inst|wr_pos.y[3]~5_combout\ & (\snake_ctrl_inst|rd_pos~52_combout\)) # (!\snake_ctrl_inst|wr_pos.y[3]~5_combout\ & ((\snake_ctrl_inst|cur_position.y\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|rd_pos~52_combout\,
	datab => \snake_ctrl_inst|wr_pos.y[3]~5_combout\,
	datad => \snake_ctrl_inst|cur_position.y\(2),
	combout => \snake_ctrl_inst|wr_pos.y[2]~2_combout\);

-- Location: FF_X73_Y66_N21
\snake_ctrl_inst|wr_pos.y[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|wr_pos.y[2]~2_combout\,
	asdata => \snake_ctrl_inst|wr_pos~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snake_ctrl_inst|wr_pos.y[3]~10_combout\,
	sload => \snake_ctrl_inst|wr_pos.y[3]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|wr_pos.y\(2));

-- Location: LCCOMB_X74_Y66_N22
\snake_ctrl_inst|rd_pos~53\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~53_combout\ = (!\busy~input_o\ & ((\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].tail_position.y\(3)))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].tail_position.y\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[0].tail_position.y\(3),
	datab => \busy~input_o\,
	datac => \snake_ctrl_inst|cur_player~q\,
	datad => \snake_ctrl_inst|snake_pos[1].tail_position.y\(3),
	combout => \snake_ctrl_inst|rd_pos~53_combout\);

-- Location: LCCOMB_X73_Y66_N14
\snake_ctrl_inst|wr_pos.y[3]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_pos.y[3]~3_combout\ = (\snake_ctrl_inst|wr_pos.y[3]~5_combout\ & ((\snake_ctrl_inst|rd_pos~53_combout\))) # (!\snake_ctrl_inst|wr_pos.y[3]~5_combout\ & (\snake_ctrl_inst|cur_position.y\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_position.y\(3),
	datab => \snake_ctrl_inst|wr_pos.y[3]~5_combout\,
	datad => \snake_ctrl_inst|rd_pos~53_combout\,
	combout => \snake_ctrl_inst|wr_pos.y[3]~3_combout\);

-- Location: FF_X73_Y66_N15
\snake_ctrl_inst|wr_pos.y[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|wr_pos.y[3]~3_combout\,
	asdata => \snake_ctrl_inst|wr_pos~3_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snake_ctrl_inst|wr_pos.y[3]~10_combout\,
	sload => \snake_ctrl_inst|wr_pos.y[3]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|wr_pos.y\(3));

-- Location: LCCOMB_X74_Y66_N16
\snake_ctrl_inst|rd_pos~54\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~54_combout\ = (!\busy~input_o\ & ((\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].tail_position.y\(4)))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].tail_position.y\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_player~q\,
	datab => \snake_ctrl_inst|snake_pos[0].tail_position.y\(4),
	datac => \snake_ctrl_inst|snake_pos[1].tail_position.y\(4),
	datad => \busy~input_o\,
	combout => \snake_ctrl_inst|rd_pos~54_combout\);

-- Location: LCCOMB_X73_Y66_N8
\snake_ctrl_inst|wr_pos.y[4]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_pos.y[4]~4_combout\ = (\snake_ctrl_inst|wr_pos.y[3]~5_combout\ & ((\snake_ctrl_inst|rd_pos~54_combout\))) # (!\snake_ctrl_inst|wr_pos.y[3]~5_combout\ & (\snake_ctrl_inst|cur_position.y\(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_position.y\(4),
	datab => \snake_ctrl_inst|wr_pos.y[3]~5_combout\,
	datad => \snake_ctrl_inst|rd_pos~54_combout\,
	combout => \snake_ctrl_inst|wr_pos.y[4]~4_combout\);

-- Location: FF_X73_Y66_N9
\snake_ctrl_inst|wr_pos.y[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|wr_pos.y[4]~4_combout\,
	asdata => \snake_ctrl_inst|wr_pos~4_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snake_ctrl_inst|wr_pos.y[3]~10_combout\,
	sload => \snake_ctrl_inst|wr_pos.y[3]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|wr_pos.y\(4));

-- Location: LCCOMB_X74_Y66_N18
\snake_ctrl_inst|rd_pos~55\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~55_combout\ = (!\busy~input_o\ & ((\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].tail_position.x\(0)))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].tail_position.x\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[0].tail_position.x\(0),
	datab => \busy~input_o\,
	datac => \snake_ctrl_inst|cur_player~q\,
	datad => \snake_ctrl_inst|snake_pos[1].tail_position.x\(0),
	combout => \snake_ctrl_inst|rd_pos~55_combout\);

-- Location: LCCOMB_X73_Y66_N18
\snake_ctrl_inst|wr_pos.x[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_pos.x[0]~0_combout\ = (\snake_ctrl_inst|wr_pos.y[3]~5_combout\ & ((\snake_ctrl_inst|rd_pos~55_combout\))) # (!\snake_ctrl_inst|wr_pos.y[3]~5_combout\ & (\snake_ctrl_inst|cur_position.x\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_position.x\(0),
	datab => \snake_ctrl_inst|wr_pos.y[3]~5_combout\,
	datad => \snake_ctrl_inst|rd_pos~55_combout\,
	combout => \snake_ctrl_inst|wr_pos.x[0]~0_combout\);

-- Location: LCCOMB_X70_Y65_N0
\snake_ctrl_inst|Add8~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add8~0_combout\ = \snake_ctrl_inst|wr_pos~5_combout\ $ (VCC)
-- \snake_ctrl_inst|Add8~1\ = CARRY(\snake_ctrl_inst|wr_pos~5_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|wr_pos~5_combout\,
	datad => VCC,
	combout => \snake_ctrl_inst|Add8~0_combout\,
	cout => \snake_ctrl_inst|Add8~1\);

-- Location: LCCOMB_X70_Y65_N16
\snake_ctrl_inst|Add7~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add7~0_combout\ = (\snake_ctrl_inst|wr_pos~5_combout\ & (\snake_ctrl_inst|target_movement_direction.LEFT~q\ $ (VCC))) # (!\snake_ctrl_inst|wr_pos~5_combout\ & (\snake_ctrl_inst|target_movement_direction.LEFT~q\ & VCC))
-- \snake_ctrl_inst|Add7~1\ = CARRY((\snake_ctrl_inst|wr_pos~5_combout\ & \snake_ctrl_inst|target_movement_direction.LEFT~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|wr_pos~5_combout\,
	datab => \snake_ctrl_inst|target_movement_direction.LEFT~q\,
	datad => VCC,
	combout => \snake_ctrl_inst|Add7~0_combout\,
	cout => \snake_ctrl_inst|Add7~1\);

-- Location: LCCOMB_X70_Y65_N28
\snake_ctrl_inst|Add8~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add8~2_combout\ = (\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\ & ((\snake_ctrl_inst|target_movement_direction.RIGHT~q\ & (\snake_ctrl_inst|Add8~0_combout\)) # (!\snake_ctrl_inst|target_movement_direction.RIGHT~q\ & 
-- ((\snake_ctrl_inst|Add7~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Add8~0_combout\,
	datab => \snake_ctrl_inst|Add7~0_combout\,
	datac => \snake_ctrl_inst|target_movement_direction.RIGHT~q\,
	datad => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	combout => \snake_ctrl_inst|Add8~2_combout\);

-- Location: LCCOMB_X69_Y65_N24
\snake_ctrl_inst|Add8~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add8~3_combout\ = (\snake_ctrl_inst|Add8~2_combout\) # ((\init_head_position.x[0]~input_o\ & !\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|Add8~2_combout\,
	datac => \init_head_position.x[0]~input_o\,
	datad => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	combout => \snake_ctrl_inst|Add8~3_combout\);

-- Location: FF_X69_Y65_N25
\snake_ctrl_inst|snake_pos[0].head_position.x[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Add8~3_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[0].head_position.y[0]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].head_position.x\(0));

-- Location: FF_X70_Y65_N27
\snake_ctrl_inst|snake_pos[1].head_position.x[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snake_ctrl_inst|Add8~3_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snake_ctrl_inst|snake_pos[1].head_position.x[2]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].head_position.x\(0));

-- Location: LCCOMB_X70_Y65_N26
\snake_ctrl_inst|wr_pos~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_pos~5_combout\ = (\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].head_position.x\(0)))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].head_position.x\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_player~q\,
	datab => \snake_ctrl_inst|snake_pos[0].head_position.x\(0),
	datac => \snake_ctrl_inst|snake_pos[1].head_position.x\(0),
	combout => \snake_ctrl_inst|wr_pos~5_combout\);

-- Location: FF_X73_Y66_N19
\snake_ctrl_inst|wr_pos.x[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|wr_pos.x[0]~0_combout\,
	asdata => \snake_ctrl_inst|wr_pos~5_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snake_ctrl_inst|wr_pos.y[3]~10_combout\,
	sload => \snake_ctrl_inst|wr_pos.y[3]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|wr_pos.x\(0));

-- Location: LCCOMB_X74_Y66_N12
\snake_ctrl_inst|rd_pos~56\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~56_combout\ = (!\busy~input_o\ & ((\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[1].tail_position.x\(1))) # (!\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[0].tail_position.x\(1))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[1].tail_position.x\(1),
	datab => \busy~input_o\,
	datac => \snake_ctrl_inst|cur_player~q\,
	datad => \snake_ctrl_inst|snake_pos[0].tail_position.x\(1),
	combout => \snake_ctrl_inst|rd_pos~56_combout\);

-- Location: LCCOMB_X73_Y66_N4
\snake_ctrl_inst|wr_pos.x[1]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_pos.x[1]~1_combout\ = (\snake_ctrl_inst|wr_pos.y[3]~5_combout\ & (\snake_ctrl_inst|rd_pos~56_combout\)) # (!\snake_ctrl_inst|wr_pos.y[3]~5_combout\ & ((\snake_ctrl_inst|cur_position.x\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|wr_pos.y[3]~5_combout\,
	datab => \snake_ctrl_inst|rd_pos~56_combout\,
	datad => \snake_ctrl_inst|cur_position.x\(1),
	combout => \snake_ctrl_inst|wr_pos.x[1]~1_combout\);

-- Location: LCCOMB_X70_Y65_N2
\snake_ctrl_inst|Add8~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add8~4_combout\ = (\snake_ctrl_inst|wr_pos~6_combout\ & (!\snake_ctrl_inst|Add8~1\)) # (!\snake_ctrl_inst|wr_pos~6_combout\ & ((\snake_ctrl_inst|Add8~1\) # (GND)))
-- \snake_ctrl_inst|Add8~5\ = CARRY((!\snake_ctrl_inst|Add8~1\) # (!\snake_ctrl_inst|wr_pos~6_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|wr_pos~6_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|Add8~1\,
	combout => \snake_ctrl_inst|Add8~4_combout\,
	cout => \snake_ctrl_inst|Add8~5\);

-- Location: LCCOMB_X70_Y65_N18
\snake_ctrl_inst|Add7~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add7~2_combout\ = (\snake_ctrl_inst|wr_pos~6_combout\ & ((\snake_ctrl_inst|target_movement_direction.LEFT~q\ & (\snake_ctrl_inst|Add7~1\ & VCC)) # (!\snake_ctrl_inst|target_movement_direction.LEFT~q\ & (!\snake_ctrl_inst|Add7~1\)))) # 
-- (!\snake_ctrl_inst|wr_pos~6_combout\ & ((\snake_ctrl_inst|target_movement_direction.LEFT~q\ & (!\snake_ctrl_inst|Add7~1\)) # (!\snake_ctrl_inst|target_movement_direction.LEFT~q\ & ((\snake_ctrl_inst|Add7~1\) # (GND)))))
-- \snake_ctrl_inst|Add7~3\ = CARRY((\snake_ctrl_inst|wr_pos~6_combout\ & (!\snake_ctrl_inst|target_movement_direction.LEFT~q\ & !\snake_ctrl_inst|Add7~1\)) # (!\snake_ctrl_inst|wr_pos~6_combout\ & ((!\snake_ctrl_inst|Add7~1\) # 
-- (!\snake_ctrl_inst|target_movement_direction.LEFT~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|wr_pos~6_combout\,
	datab => \snake_ctrl_inst|target_movement_direction.LEFT~q\,
	datad => VCC,
	cin => \snake_ctrl_inst|Add7~1\,
	combout => \snake_ctrl_inst|Add7~2_combout\,
	cout => \snake_ctrl_inst|Add7~3\);

-- Location: LCCOMB_X69_Y65_N26
\snake_ctrl_inst|Add8~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add8~6_combout\ = (\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\ & ((\snake_ctrl_inst|target_movement_direction.RIGHT~q\ & (\snake_ctrl_inst|Add8~4_combout\)) # (!\snake_ctrl_inst|target_movement_direction.RIGHT~q\ & 
-- ((\snake_ctrl_inst|Add7~2_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Add8~4_combout\,
	datab => \snake_ctrl_inst|Add7~2_combout\,
	datac => \snake_ctrl_inst|target_movement_direction.RIGHT~q\,
	datad => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	combout => \snake_ctrl_inst|Add8~6_combout\);

-- Location: LCCOMB_X69_Y65_N20
\snake_ctrl_inst|Add8~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add8~7_combout\ = (\snake_ctrl_inst|Add8~6_combout\) # ((\init_head_position.x[1]~input_o\ & !\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \init_head_position.x[1]~input_o\,
	datac => \snake_ctrl_inst|Add8~6_combout\,
	datad => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	combout => \snake_ctrl_inst|Add8~7_combout\);

-- Location: FF_X69_Y65_N21
\snake_ctrl_inst|snake_pos[0].head_position.x[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Add8~7_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[0].head_position.y[0]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].head_position.x\(1));

-- Location: FF_X69_Y65_N3
\snake_ctrl_inst|snake_pos[1].head_position.x[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snake_ctrl_inst|Add8~7_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snake_ctrl_inst|snake_pos[1].head_position.x[2]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].head_position.x\(1));

-- Location: LCCOMB_X69_Y65_N2
\snake_ctrl_inst|wr_pos~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_pos~6_combout\ = (\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].head_position.x\(1)))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].head_position.x\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|snake_pos[0].head_position.x\(1),
	datac => \snake_ctrl_inst|snake_pos[1].head_position.x\(1),
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|wr_pos~6_combout\);

-- Location: FF_X73_Y66_N5
\snake_ctrl_inst|wr_pos.x[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|wr_pos.x[1]~1_combout\,
	asdata => \snake_ctrl_inst|wr_pos~6_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snake_ctrl_inst|wr_pos.y[3]~10_combout\,
	sload => \snake_ctrl_inst|wr_pos.y[3]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|wr_pos.x\(1));

-- Location: LCCOMB_X75_Y66_N22
\snake_ctrl_inst|rd_pos~57\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~57_combout\ = (!\busy~input_o\ & ((\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[1].tail_position.x\(2))) # (!\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[0].tail_position.x\(2))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110100001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_player~q\,
	datab => \snake_ctrl_inst|snake_pos[1].tail_position.x\(2),
	datac => \busy~input_o\,
	datad => \snake_ctrl_inst|snake_pos[0].tail_position.x\(2),
	combout => \snake_ctrl_inst|rd_pos~57_combout\);

-- Location: LCCOMB_X73_Y66_N22
\snake_ctrl_inst|wr_pos.x[2]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_pos.x[2]~2_combout\ = (\snake_ctrl_inst|wr_pos.y[3]~5_combout\ & (\snake_ctrl_inst|rd_pos~57_combout\)) # (!\snake_ctrl_inst|wr_pos.y[3]~5_combout\ & ((\snake_ctrl_inst|cur_position.x\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|rd_pos~57_combout\,
	datab => \snake_ctrl_inst|cur_position.x\(2),
	datad => \snake_ctrl_inst|wr_pos.y[3]~5_combout\,
	combout => \snake_ctrl_inst|wr_pos.x[2]~2_combout\);

-- Location: LCCOMB_X70_Y65_N20
\snake_ctrl_inst|Add7~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add7~4_combout\ = ((\snake_ctrl_inst|wr_pos~7_combout\ $ (\snake_ctrl_inst|target_movement_direction.LEFT~q\ $ (!\snake_ctrl_inst|Add7~3\)))) # (GND)
-- \snake_ctrl_inst|Add7~5\ = CARRY((\snake_ctrl_inst|wr_pos~7_combout\ & ((\snake_ctrl_inst|target_movement_direction.LEFT~q\) # (!\snake_ctrl_inst|Add7~3\))) # (!\snake_ctrl_inst|wr_pos~7_combout\ & (\snake_ctrl_inst|target_movement_direction.LEFT~q\ & 
-- !\snake_ctrl_inst|Add7~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|wr_pos~7_combout\,
	datab => \snake_ctrl_inst|target_movement_direction.LEFT~q\,
	datad => VCC,
	cin => \snake_ctrl_inst|Add7~3\,
	combout => \snake_ctrl_inst|Add7~4_combout\,
	cout => \snake_ctrl_inst|Add7~5\);

-- Location: LCCOMB_X70_Y65_N4
\snake_ctrl_inst|Add8~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add8~8_combout\ = (\snake_ctrl_inst|wr_pos~7_combout\ & (\snake_ctrl_inst|Add8~5\ $ (GND))) # (!\snake_ctrl_inst|wr_pos~7_combout\ & (!\snake_ctrl_inst|Add8~5\ & VCC))
-- \snake_ctrl_inst|Add8~9\ = CARRY((\snake_ctrl_inst|wr_pos~7_combout\ & !\snake_ctrl_inst|Add8~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|wr_pos~7_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|Add8~5\,
	combout => \snake_ctrl_inst|Add8~8_combout\,
	cout => \snake_ctrl_inst|Add8~9\);

-- Location: LCCOMB_X70_Y65_N30
\snake_ctrl_inst|Add8~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add8~10_combout\ = (\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\ & ((\snake_ctrl_inst|target_movement_direction.RIGHT~q\ & ((\snake_ctrl_inst|Add8~8_combout\))) # (!\snake_ctrl_inst|target_movement_direction.RIGHT~q\ & 
-- (\snake_ctrl_inst|Add7~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Add7~4_combout\,
	datab => \snake_ctrl_inst|Add8~8_combout\,
	datac => \snake_ctrl_inst|target_movement_direction.RIGHT~q\,
	datad => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	combout => \snake_ctrl_inst|Add8~10_combout\);

-- Location: LCCOMB_X69_Y65_N14
\snake_ctrl_inst|Add8~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add8~11_combout\ = (\snake_ctrl_inst|Add8~10_combout\) # ((\init_head_position.x[2]~input_o\ & !\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \init_head_position.x[2]~input_o\,
	datac => \snake_ctrl_inst|Add8~10_combout\,
	datad => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	combout => \snake_ctrl_inst|Add8~11_combout\);

-- Location: FF_X70_Y65_N13
\snake_ctrl_inst|snake_pos[1].head_position.x[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snake_ctrl_inst|Add8~11_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snake_ctrl_inst|snake_pos[1].head_position.x[2]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].head_position.x\(2));

-- Location: FF_X69_Y65_N15
\snake_ctrl_inst|snake_pos[0].head_position.x[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Add8~11_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[0].head_position.y[0]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].head_position.x\(2));

-- Location: LCCOMB_X70_Y65_N12
\snake_ctrl_inst|wr_pos~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_pos~7_combout\ = (\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[1].head_position.x\(2))) # (!\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[0].head_position.x\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_player~q\,
	datac => \snake_ctrl_inst|snake_pos[1].head_position.x\(2),
	datad => \snake_ctrl_inst|snake_pos[0].head_position.x\(2),
	combout => \snake_ctrl_inst|wr_pos~7_combout\);

-- Location: FF_X73_Y66_N23
\snake_ctrl_inst|wr_pos.x[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|wr_pos.x[2]~2_combout\,
	asdata => \snake_ctrl_inst|wr_pos~7_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snake_ctrl_inst|wr_pos.y[3]~10_combout\,
	sload => \snake_ctrl_inst|wr_pos.y[3]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|wr_pos.x\(2));

-- Location: LCCOMB_X74_Y66_N30
\snake_ctrl_inst|rd_pos~58\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~58_combout\ = (!\busy~input_o\ & ((\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[1].tail_position.x\(3))) # (!\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[0].tail_position.x\(3))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[1].tail_position.x\(3),
	datab => \snake_ctrl_inst|snake_pos[0].tail_position.x\(3),
	datac => \snake_ctrl_inst|cur_player~q\,
	datad => \busy~input_o\,
	combout => \snake_ctrl_inst|rd_pos~58_combout\);

-- Location: LCCOMB_X73_Y66_N24
\snake_ctrl_inst|wr_pos.x[3]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_pos.x[3]~3_combout\ = (\snake_ctrl_inst|wr_pos.y[3]~5_combout\ & (\snake_ctrl_inst|rd_pos~58_combout\)) # (!\snake_ctrl_inst|wr_pos.y[3]~5_combout\ & ((\snake_ctrl_inst|cur_position.x\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|rd_pos~58_combout\,
	datab => \snake_ctrl_inst|wr_pos.y[3]~5_combout\,
	datad => \snake_ctrl_inst|cur_position.x\(3),
	combout => \snake_ctrl_inst|wr_pos.x[3]~3_combout\);

-- Location: LCCOMB_X70_Y65_N6
\snake_ctrl_inst|Add8~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add8~12_combout\ = (\snake_ctrl_inst|wr_pos~8_combout\ & (!\snake_ctrl_inst|Add8~9\)) # (!\snake_ctrl_inst|wr_pos~8_combout\ & ((\snake_ctrl_inst|Add8~9\) # (GND)))
-- \snake_ctrl_inst|Add8~13\ = CARRY((!\snake_ctrl_inst|Add8~9\) # (!\snake_ctrl_inst|wr_pos~8_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|wr_pos~8_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|Add8~9\,
	combout => \snake_ctrl_inst|Add8~12_combout\,
	cout => \snake_ctrl_inst|Add8~13\);

-- Location: LCCOMB_X69_Y65_N4
\snake_ctrl_inst|Add8~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add8~14_combout\ = (\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\ & ((!\snake_ctrl_inst|target_movement_direction.RIGHT~q\))) # (!\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\ & (\init_head_position.x[3]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \init_head_position.x[3]~input_o\,
	datac => \snake_ctrl_inst|target_movement_direction.RIGHT~q\,
	datad => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	combout => \snake_ctrl_inst|Add8~14_combout\);

-- Location: LCCOMB_X70_Y65_N22
\snake_ctrl_inst|Add7~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add7~6_combout\ = (\snake_ctrl_inst|target_movement_direction.LEFT~q\ & ((\snake_ctrl_inst|wr_pos~8_combout\ & (\snake_ctrl_inst|Add7~5\ & VCC)) # (!\snake_ctrl_inst|wr_pos~8_combout\ & (!\snake_ctrl_inst|Add7~5\)))) # 
-- (!\snake_ctrl_inst|target_movement_direction.LEFT~q\ & ((\snake_ctrl_inst|wr_pos~8_combout\ & (!\snake_ctrl_inst|Add7~5\)) # (!\snake_ctrl_inst|wr_pos~8_combout\ & ((\snake_ctrl_inst|Add7~5\) # (GND)))))
-- \snake_ctrl_inst|Add7~7\ = CARRY((\snake_ctrl_inst|target_movement_direction.LEFT~q\ & (!\snake_ctrl_inst|wr_pos~8_combout\ & !\snake_ctrl_inst|Add7~5\)) # (!\snake_ctrl_inst|target_movement_direction.LEFT~q\ & ((!\snake_ctrl_inst|Add7~5\) # 
-- (!\snake_ctrl_inst|wr_pos~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|target_movement_direction.LEFT~q\,
	datab => \snake_ctrl_inst|wr_pos~8_combout\,
	datad => VCC,
	cin => \snake_ctrl_inst|Add7~5\,
	combout => \snake_ctrl_inst|Add7~6_combout\,
	cout => \snake_ctrl_inst|Add7~7\);

-- Location: LCCOMB_X69_Y65_N16
\snake_ctrl_inst|Add8~15\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add8~15_combout\ = (\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\ & ((\snake_ctrl_inst|Add8~14_combout\ & ((\snake_ctrl_inst|Add7~6_combout\))) # (!\snake_ctrl_inst|Add8~14_combout\ & (\snake_ctrl_inst|Add8~12_combout\)))) # 
-- (!\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\ & (((\snake_ctrl_inst|Add8~14_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100000111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Add8~12_combout\,
	datab => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	datac => \snake_ctrl_inst|Add8~14_combout\,
	datad => \snake_ctrl_inst|Add7~6_combout\,
	combout => \snake_ctrl_inst|Add8~15_combout\);

-- Location: FF_X69_Y65_N17
\snake_ctrl_inst|snake_pos[0].head_position.x[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Add8~15_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[0].head_position.y[0]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].head_position.x\(3));

-- Location: FF_X70_Y65_N15
\snake_ctrl_inst|snake_pos[1].head_position.x[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snake_ctrl_inst|Add8~15_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snake_ctrl_inst|snake_pos[1].head_position.x[2]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].head_position.x\(3));

-- Location: LCCOMB_X70_Y65_N14
\snake_ctrl_inst|wr_pos~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_pos~8_combout\ = (\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].head_position.x\(3)))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].head_position.x\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_player~q\,
	datab => \snake_ctrl_inst|snake_pos[0].head_position.x\(3),
	datac => \snake_ctrl_inst|snake_pos[1].head_position.x\(3),
	combout => \snake_ctrl_inst|wr_pos~8_combout\);

-- Location: FF_X73_Y66_N25
\snake_ctrl_inst|wr_pos.x[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|wr_pos.x[3]~3_combout\,
	asdata => \snake_ctrl_inst|wr_pos~8_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snake_ctrl_inst|wr_pos.y[3]~10_combout\,
	sload => \snake_ctrl_inst|wr_pos.y[3]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|wr_pos.x\(3));

-- Location: LCCOMB_X76_Y66_N8
\snake_ctrl_inst|rd_pos~59\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|rd_pos~59_combout\ = (!\busy~input_o\ & ((\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].tail_position.x\(4)))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[0].tail_position.x\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[0].tail_position.x\(4),
	datab => \snake_ctrl_inst|cur_player~q\,
	datac => \busy~input_o\,
	datad => \snake_ctrl_inst|snake_pos[1].tail_position.x\(4),
	combout => \snake_ctrl_inst|rd_pos~59_combout\);

-- Location: LCCOMB_X73_Y66_N10
\snake_ctrl_inst|wr_pos.x[4]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_pos.x[4]~4_combout\ = (\snake_ctrl_inst|wr_pos.y[3]~5_combout\ & ((\snake_ctrl_inst|rd_pos~59_combout\))) # (!\snake_ctrl_inst|wr_pos.y[3]~5_combout\ & (\snake_ctrl_inst|cur_position.x\(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_position.x\(4),
	datab => \snake_ctrl_inst|wr_pos.y[3]~5_combout\,
	datad => \snake_ctrl_inst|rd_pos~59_combout\,
	combout => \snake_ctrl_inst|wr_pos.x[4]~4_combout\);

-- Location: LCCOMB_X69_Y65_N30
\snake_ctrl_inst|Add8~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add8~18_combout\ = (\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\ & ((!\snake_ctrl_inst|target_movement_direction.RIGHT~q\))) # (!\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\ & (\init_head_position.x[4]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \init_head_position.x[4]~input_o\,
	datac => \snake_ctrl_inst|target_movement_direction.RIGHT~q\,
	datad => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	combout => \snake_ctrl_inst|Add8~18_combout\);

-- Location: LCCOMB_X70_Y65_N24
\snake_ctrl_inst|Add7~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add7~8_combout\ = \snake_ctrl_inst|target_movement_direction.LEFT~q\ $ (\snake_ctrl_inst|Add7~7\ $ (!\snake_ctrl_inst|wr_pos~9_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|target_movement_direction.LEFT~q\,
	datad => \snake_ctrl_inst|wr_pos~9_combout\,
	cin => \snake_ctrl_inst|Add7~7\,
	combout => \snake_ctrl_inst|Add7~8_combout\);

-- Location: LCCOMB_X70_Y65_N8
\snake_ctrl_inst|Add8~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add8~16_combout\ = \snake_ctrl_inst|Add8~13\ $ (!\snake_ctrl_inst|wr_pos~9_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \snake_ctrl_inst|wr_pos~9_combout\,
	cin => \snake_ctrl_inst|Add8~13\,
	combout => \snake_ctrl_inst|Add8~16_combout\);

-- Location: LCCOMB_X69_Y65_N10
\snake_ctrl_inst|Add8~19\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Add8~19_combout\ = (\snake_ctrl_inst|Add8~18_combout\ & ((\snake_ctrl_inst|Add7~8_combout\) # ((!\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\)))) # (!\snake_ctrl_inst|Add8~18_combout\ & (((\snake_ctrl_inst|Add8~16_combout\ & 
-- \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Add8~18_combout\,
	datab => \snake_ctrl_inst|Add7~8_combout\,
	datac => \snake_ctrl_inst|Add8~16_combout\,
	datad => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	combout => \snake_ctrl_inst|Add8~19_combout\);

-- Location: FF_X70_Y65_N11
\snake_ctrl_inst|snake_pos[1].head_position.x[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snake_ctrl_inst|Add8~19_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snake_ctrl_inst|snake_pos[1].head_position.x[2]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].head_position.x\(4));

-- Location: FF_X69_Y65_N11
\snake_ctrl_inst|snake_pos[0].head_position.x[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Add8~19_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[0].head_position.y[0]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].head_position.x\(4));

-- Location: LCCOMB_X70_Y65_N10
\snake_ctrl_inst|wr_pos~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_pos~9_combout\ = (\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[1].head_position.x\(4))) # (!\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[0].head_position.x\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|cur_player~q\,
	datac => \snake_ctrl_inst|snake_pos[1].head_position.x\(4),
	datad => \snake_ctrl_inst|snake_pos[0].head_position.x\(4),
	combout => \snake_ctrl_inst|wr_pos~9_combout\);

-- Location: FF_X73_Y66_N11
\snake_ctrl_inst|wr_pos.x[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|wr_pos.x[4]~4_combout\,
	asdata => \snake_ctrl_inst|wr_pos~9_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snake_ctrl_inst|wr_pos.y[3]~10_combout\,
	sload => \snake_ctrl_inst|wr_pos.y[3]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|wr_pos.x\(4));

-- Location: LCCOMB_X73_Y69_N24
\snake_ctrl_inst|Selector13~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector13~1_combout\ = (\snake_ctrl_inst|target_movement_direction.RIGHT~q\ & ((\snake_ctrl_inst|wr_data~2_combout\) # ((\snake_ctrl_inst|wr_data~3_combout\)))) # (!\snake_ctrl_inst|target_movement_direction.RIGHT~q\ & 
-- (\snake_ctrl_inst|target_movement_direction.LEFT~q\ & ((\snake_ctrl_inst|wr_data~2_combout\) # (\snake_ctrl_inst|wr_data~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|target_movement_direction.RIGHT~q\,
	datab => \snake_ctrl_inst|wr_data~2_combout\,
	datac => \snake_ctrl_inst|wr_data~3_combout\,
	datad => \snake_ctrl_inst|target_movement_direction.LEFT~q\,
	combout => \snake_ctrl_inst|Selector13~1_combout\);

-- Location: LCCOMB_X75_Y69_N4
\snake_ctrl_inst|Selector120~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector120~2_combout\ = (!\busy~input_o\ & (\snake_ctrl_inst|state.PLACE_BODY~q\ & ((!\snake_ctrl_inst|Equal0~0_combout\) # (!\snake_ctrl_inst|Equal0~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Equal0~1_combout\,
	datab => \busy~input_o\,
	datac => \snake_ctrl_inst|state.PLACE_BODY~q\,
	datad => \snake_ctrl_inst|Equal0~0_combout\,
	combout => \snake_ctrl_inst|Selector120~2_combout\);

-- Location: IOIBUF_X54_Y73_N8
\movement_direction.UP~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_movement_direction.UP\,
	o => \movement_direction.UP~input_o\);

-- Location: LCCOMB_X72_Y68_N0
\snake_ctrl_inst|target_movement_direction.UP~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|target_movement_direction.UP~0_combout\ = !\movement_direction.UP~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \movement_direction.UP~input_o\,
	combout => \snake_ctrl_inst|target_movement_direction.UP~0_combout\);

-- Location: FF_X72_Y68_N1
\snake_ctrl_inst|target_movement_direction.UP\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|target_movement_direction.UP~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|target_movement_direction~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|target_movement_direction.UP~q\);

-- Location: LCCOMB_X72_Y68_N2
\snake_ctrl_inst|Selector74~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector74~0_combout\ = (\snake_ctrl_inst|Selector131~2_combout\ & ((\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\ & (\snake_ctrl_inst|target_movement_direction.UP~q\)) # (!\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\ & 
-- ((!\init_head_direction.UP~input_o\))))) # (!\snake_ctrl_inst|Selector131~2_combout\ & (((!\init_head_direction.UP~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector131~2_combout\,
	datab => \snake_ctrl_inst|target_movement_direction.UP~q\,
	datac => \init_head_direction.UP~input_o\,
	datad => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	combout => \snake_ctrl_inst|Selector74~0_combout\);

-- Location: FF_X72_Y68_N3
\snake_ctrl_inst|snake_pos[1].head_direction.UP\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector74~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[1].head_position.x[2]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[1].head_direction.UP~q\);

-- Location: LCCOMB_X73_Y68_N24
\snake_ctrl_inst|Selector102~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector102~0_combout\ = (\snake_ctrl_inst|Selector133~0_combout\ & ((\snake_ctrl_inst|cur_player~q\ & ((!\init_head_direction.UP~input_o\))) # (!\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|target_movement_direction.UP~q\)))) # 
-- (!\snake_ctrl_inst|Selector133~0_combout\ & (((!\init_head_direction.UP~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111110001011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|target_movement_direction.UP~q\,
	datab => \snake_ctrl_inst|Selector133~0_combout\,
	datac => \init_head_direction.UP~input_o\,
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|Selector102~0_combout\);

-- Location: FF_X73_Y68_N25
\snake_ctrl_inst|snake_pos[0].head_direction.UP\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector102~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snake_ctrl_inst|snake_pos[0].head_position.y[0]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|snake_pos[0].head_direction.UP~q\);

-- Location: LCCOMB_X73_Y68_N10
\snake_ctrl_inst|wr_data~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_data~1_combout\ = (\snake_ctrl_inst|cur_player~q\ & (\snake_ctrl_inst|snake_pos[1].head_direction.UP~q\)) # (!\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[0].head_direction.UP~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[1].head_direction.UP~q\,
	datab => \snake_ctrl_inst|snake_pos[0].head_direction.UP~q\,
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|wr_data~1_combout\);

-- Location: LCCOMB_X73_Y69_N12
\snake_ctrl_inst|Selector13~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector13~0_combout\ = (\snake_ctrl_inst|target_movement_direction.DOWN~q\ & (((\snake_ctrl_inst|wr_data~0_combout\) # (!\snake_ctrl_inst|wr_data~1_combout\)))) # (!\snake_ctrl_inst|target_movement_direction.DOWN~q\ & 
-- (!\snake_ctrl_inst|target_movement_direction.UP~q\ & ((\snake_ctrl_inst|wr_data~0_combout\) # (!\snake_ctrl_inst|wr_data~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000010111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|target_movement_direction.DOWN~q\,
	datab => \snake_ctrl_inst|target_movement_direction.UP~q\,
	datac => \snake_ctrl_inst|wr_data~0_combout\,
	datad => \snake_ctrl_inst|wr_data~1_combout\,
	combout => \snake_ctrl_inst|Selector13~0_combout\);

-- Location: LCCOMB_X73_Y69_N18
\snake_ctrl_inst|Selector135~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector135~0_combout\ = (!\snake_ctrl_inst|Selector120~2_combout\ & (((!\snake_ctrl_inst|Selector13~1_combout\ & !\snake_ctrl_inst|Selector13~0_combout\)) # (!\snake_ctrl_inst|Selector133~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010100000111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector133~0_combout\,
	datab => \snake_ctrl_inst|Selector13~1_combout\,
	datac => \snake_ctrl_inst|Selector120~2_combout\,
	datad => \snake_ctrl_inst|Selector13~0_combout\,
	combout => \snake_ctrl_inst|Selector135~0_combout\);

-- Location: LCCOMB_X73_Y69_N8
\snake_ctrl_inst|Selector135~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector135~1_combout\ = ((!\busy~input_o\ & ((\snake_ctrl_inst|state.DRAW_NEW_HEAD~q\) # (\snake_ctrl_inst|state.PLACE_HEAD~q\)))) # (!\snake_ctrl_inst|Selector135~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111100111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|state.DRAW_NEW_HEAD~q\,
	datab => \snake_ctrl_inst|Selector135~0_combout\,
	datac => \busy~input_o\,
	datad => \snake_ctrl_inst|state.PLACE_HEAD~q\,
	combout => \snake_ctrl_inst|Selector135~1_combout\);

-- Location: FF_X73_Y69_N9
\snake_ctrl_inst|wr_data[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector135~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|wr_data\(0));

-- Location: LCCOMB_X72_Y69_N8
\snake_ctrl_inst|Selector134~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector134~0_combout\ = ((!\busy~input_o\ & ((\snake_ctrl_inst|state.PLACE_TAIL~q\) # (\snake_ctrl_inst|state.DRAW_NEW_TAIL~q\)))) # (!\snake_ctrl_inst|Selector135~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011101110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \busy~input_o\,
	datab => \snake_ctrl_inst|Selector135~0_combout\,
	datac => \snake_ctrl_inst|state.PLACE_TAIL~q\,
	datad => \snake_ctrl_inst|state.DRAW_NEW_TAIL~q\,
	combout => \snake_ctrl_inst|Selector134~0_combout\);

-- Location: FF_X72_Y69_N9
\snake_ctrl_inst|wr_data[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector134~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|wr_data\(1));

-- Location: LCCOMB_X70_Y68_N0
\snake_ctrl_inst|Selector12~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector12~1_combout\ = (!\snake_ctrl_inst|target_movement_direction.LEFT~q\ & ((\snake_ctrl_inst|cur_player~q\ & ((\snake_ctrl_inst|snake_pos[1].head_direction.LEFT~q\))) # (!\snake_ctrl_inst|cur_player~q\ & 
-- (\snake_ctrl_inst|snake_pos[0].head_direction.LEFT~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|snake_pos[0].head_direction.LEFT~q\,
	datab => \snake_ctrl_inst|target_movement_direction.LEFT~q\,
	datac => \snake_ctrl_inst|snake_pos[1].head_direction.LEFT~q\,
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|Selector12~1_combout\);

-- Location: LCCOMB_X73_Y69_N28
\snake_ctrl_inst|Selector12~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector12~0_combout\ = (\snake_ctrl_inst|target_movement_direction.DOWN~q\ & (\snake_ctrl_inst|wr_data~2_combout\ & ((!\snake_ctrl_inst|target_movement_direction.RIGHT~q\)))) # (!\snake_ctrl_inst|target_movement_direction.DOWN~q\ & 
-- ((\snake_ctrl_inst|wr_data~0_combout\) # ((\snake_ctrl_inst|wr_data~2_combout\ & !\snake_ctrl_inst|target_movement_direction.RIGHT~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000011011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|target_movement_direction.DOWN~q\,
	datab => \snake_ctrl_inst|wr_data~2_combout\,
	datac => \snake_ctrl_inst|wr_data~0_combout\,
	datad => \snake_ctrl_inst|target_movement_direction.RIGHT~q\,
	combout => \snake_ctrl_inst|Selector12~0_combout\);

-- Location: LCCOMB_X73_Y69_N30
\snake_ctrl_inst|Selector12~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector12~2_combout\ = (\snake_ctrl_inst|Selector12~1_combout\) # ((\snake_ctrl_inst|Selector12~0_combout\) # ((!\snake_ctrl_inst|wr_data~1_combout\ & \snake_ctrl_inst|target_movement_direction.UP~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|wr_data~1_combout\,
	datab => \snake_ctrl_inst|target_movement_direction.UP~q\,
	datac => \snake_ctrl_inst|Selector12~1_combout\,
	datad => \snake_ctrl_inst|Selector12~0_combout\,
	combout => \snake_ctrl_inst|Selector12~2_combout\);

-- Location: LCCOMB_X76_Y69_N24
\snake_ctrl_inst|wr_data[2]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|wr_data[2]~feeder_combout\ = \snake_ctrl_inst|Selector12~2_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \snake_ctrl_inst|Selector12~2_combout\,
	combout => \snake_ctrl_inst|wr_data[2]~feeder_combout\);

-- Location: LCCOMB_X76_Y69_N26
\~GND\ : cycloneive_lcell_comb
-- Equation(s):
-- \~GND~combout\ = GND

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	combout => \~GND~combout\);

-- Location: FF_X76_Y69_N25
\snake_ctrl_inst|wr_data[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|wr_data[2]~feeder_combout\,
	asdata => \~GND~combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snake_ctrl_inst|ALT_INV_state.REPLACE_OLD_HEAD~q\,
	sload => \busy~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|wr_data\(2));

-- Location: LCCOMB_X73_Y69_N22
\snake_ctrl_inst|Selector133~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector133~3_combout\ = (!\snake_ctrl_inst|wr_data~0_combout\ & (!\busy~input_o\ & \snake_ctrl_inst|wr_data~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|wr_data~0_combout\,
	datac => \busy~input_o\,
	datad => \snake_ctrl_inst|wr_data~1_combout\,
	combout => \snake_ctrl_inst|Selector133~3_combout\);

-- Location: LCCOMB_X69_Y69_N10
\snake_ctrl_inst|Selector133~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector133~1_combout\ = (!\busy~input_o\ & (!\snake_ctrl_inst|wr_data~5_combout\ & (\snake_ctrl_inst|state.DRAW_NEW_TAIL~q\ & \snake_ctrl_inst|wr_data~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \busy~input_o\,
	datab => \snake_ctrl_inst|wr_data~5_combout\,
	datac => \snake_ctrl_inst|state.DRAW_NEW_TAIL~q\,
	datad => \snake_ctrl_inst|wr_data~4_combout\,
	combout => \snake_ctrl_inst|Selector133~1_combout\);

-- Location: LCCOMB_X73_Y69_N26
\snake_ctrl_inst|Selector11~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector11~2_combout\ = (\snake_ctrl_inst|wr_data~1_combout\ & (\snake_ctrl_inst|target_movement_direction.LEFT~q\ & (\snake_ctrl_inst|wr_data~0_combout\))) # (!\snake_ctrl_inst|wr_data~1_combout\ & 
-- ((\snake_ctrl_inst|target_movement_direction.RIGHT~q\) # ((\snake_ctrl_inst|target_movement_direction.LEFT~q\ & \snake_ctrl_inst|wr_data~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101010111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|wr_data~1_combout\,
	datab => \snake_ctrl_inst|target_movement_direction.LEFT~q\,
	datac => \snake_ctrl_inst|wr_data~0_combout\,
	datad => \snake_ctrl_inst|target_movement_direction.RIGHT~q\,
	combout => \snake_ctrl_inst|Selector11~2_combout\);

-- Location: LCCOMB_X73_Y69_N6
\snake_ctrl_inst|Selector11~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector11~0_combout\ = (\snake_ctrl_inst|wr_data~2_combout\ & ((\snake_ctrl_inst|target_movement_direction.RIGHT~q\) # (!\snake_ctrl_inst|target_movement_direction.UP~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \snake_ctrl_inst|wr_data~2_combout\,
	datac => \snake_ctrl_inst|target_movement_direction.RIGHT~q\,
	datad => \snake_ctrl_inst|target_movement_direction.UP~q\,
	combout => \snake_ctrl_inst|Selector11~0_combout\);

-- Location: LCCOMB_X73_Y69_N0
\snake_ctrl_inst|Selector11~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector11~1_combout\ = (\snake_ctrl_inst|Selector11~0_combout\) # ((\snake_ctrl_inst|wr_data~3_combout\ & ((\snake_ctrl_inst|target_movement_direction.DOWN~q\) # (\snake_ctrl_inst|target_movement_direction.LEFT~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|target_movement_direction.DOWN~q\,
	datab => \snake_ctrl_inst|Selector11~0_combout\,
	datac => \snake_ctrl_inst|wr_data~3_combout\,
	datad => \snake_ctrl_inst|target_movement_direction.LEFT~q\,
	combout => \snake_ctrl_inst|Selector11~1_combout\);

-- Location: LCCOMB_X73_Y69_N4
\snake_ctrl_inst|Selector133~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector133~2_combout\ = (\snake_ctrl_inst|Selector133~1_combout\) # ((\snake_ctrl_inst|Selector133~0_combout\ & ((\snake_ctrl_inst|Selector11~2_combout\) # (\snake_ctrl_inst|Selector11~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector133~0_combout\,
	datab => \snake_ctrl_inst|Selector133~1_combout\,
	datac => \snake_ctrl_inst|Selector11~2_combout\,
	datad => \snake_ctrl_inst|Selector11~1_combout\,
	combout => \snake_ctrl_inst|Selector133~2_combout\);

-- Location: LCCOMB_X72_Y69_N6
\snake_ctrl_inst|Selector133~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector133~4_combout\ = (\snake_ctrl_inst|Selector133~2_combout\) # ((\snake_ctrl_inst|Selector133~3_combout\ & ((\snake_ctrl_inst|state.DRAW_NEW_HEAD~q\) # (!\snake_ctrl_inst|wr_pos.y[3]~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|wr_pos.y[3]~5_combout\,
	datab => \snake_ctrl_inst|Selector133~3_combout\,
	datac => \snake_ctrl_inst|state.DRAW_NEW_HEAD~q\,
	datad => \snake_ctrl_inst|Selector133~2_combout\,
	combout => \snake_ctrl_inst|Selector133~4_combout\);

-- Location: FF_X72_Y69_N7
\snake_ctrl_inst|wr_data[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector133~4_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|wr_data\(3));

-- Location: LCCOMB_X73_Y69_N16
\snake_ctrl_inst|Selector132~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector132~1_combout\ = (\snake_ctrl_inst|target_movement_direction.RIGHT~q\ & (((\snake_ctrl_inst|wr_data~0_combout\)) # (!\snake_ctrl_inst|wr_data~1_combout\))) # (!\snake_ctrl_inst|target_movement_direction.RIGHT~q\ & 
-- (((\snake_ctrl_inst|Selector12~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|wr_data~1_combout\,
	datab => \snake_ctrl_inst|wr_data~0_combout\,
	datac => \snake_ctrl_inst|Selector12~1_combout\,
	datad => \snake_ctrl_inst|target_movement_direction.RIGHT~q\,
	combout => \snake_ctrl_inst|Selector132~1_combout\);

-- Location: LCCOMB_X69_Y69_N14
\snake_ctrl_inst|Selector132~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector132~0_combout\ = (!\busy~input_o\ & (\snake_ctrl_inst|state.DRAW_NEW_TAIL~q\ & ((\snake_ctrl_inst|wr_data~6_combout\) # (!\snake_ctrl_inst|wr_data~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \busy~input_o\,
	datab => \snake_ctrl_inst|wr_data~6_combout\,
	datac => \snake_ctrl_inst|state.DRAW_NEW_TAIL~q\,
	datad => \snake_ctrl_inst|wr_data~4_combout\,
	combout => \snake_ctrl_inst|Selector132~0_combout\);

-- Location: LCCOMB_X73_Y69_N10
\snake_ctrl_inst|Selector132~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector132~2_combout\ = (\snake_ctrl_inst|wr_data~2_combout\ & (\snake_ctrl_inst|state.PLACE_TAIL~q\)) # (!\snake_ctrl_inst|wr_data~2_combout\ & (((\snake_ctrl_inst|state.DRAW_NEW_HEAD~q\) # (\snake_ctrl_inst|state.PLACE_HEAD~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|state.PLACE_TAIL~q\,
	datab => \snake_ctrl_inst|wr_data~2_combout\,
	datac => \snake_ctrl_inst|state.DRAW_NEW_HEAD~q\,
	datad => \snake_ctrl_inst|state.PLACE_HEAD~q\,
	combout => \snake_ctrl_inst|Selector132~2_combout\);

-- Location: LCCOMB_X73_Y69_N20
\snake_ctrl_inst|Selector132~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector132~3_combout\ = (!\busy~input_o\ & ((\snake_ctrl_inst|wr_data~1_combout\ & (\snake_ctrl_inst|Selector132~2_combout\)) # (!\snake_ctrl_inst|wr_data~1_combout\ & ((\snake_ctrl_inst|state.PLACE_TAIL~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector132~2_combout\,
	datab => \busy~input_o\,
	datac => \snake_ctrl_inst|state.PLACE_TAIL~q\,
	datad => \snake_ctrl_inst|wr_data~1_combout\,
	combout => \snake_ctrl_inst|Selector132~3_combout\);

-- Location: LCCOMB_X73_Y69_N2
\snake_ctrl_inst|Selector132~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector132~4_combout\ = (\snake_ctrl_inst|Selector132~0_combout\) # ((\snake_ctrl_inst|Selector132~3_combout\) # ((\snake_ctrl_inst|Selector133~0_combout\ & \snake_ctrl_inst|Selector132~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|Selector133~0_combout\,
	datab => \snake_ctrl_inst|Selector132~1_combout\,
	datac => \snake_ctrl_inst|Selector132~0_combout\,
	datad => \snake_ctrl_inst|Selector132~3_combout\,
	combout => \snake_ctrl_inst|Selector132~4_combout\);

-- Location: FF_X73_Y69_N3
\snake_ctrl_inst|wr_data[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector132~4_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|wr_data\(4));

-- Location: LCCOMB_X72_Y69_N20
\snake_ctrl_inst|Selector131~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector131~3_combout\ = (\snake_ctrl_inst|Selector131~2_combout\ & ((\snake_ctrl_inst|state.PLACE_HEAD~q\) # ((\snake_ctrl_inst|state.PLACE_TAIL~q\) # (\snake_ctrl_inst|Selector131~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|state.PLACE_HEAD~q\,
	datab => \snake_ctrl_inst|state.PLACE_TAIL~q\,
	datac => \snake_ctrl_inst|Selector131~2_combout\,
	datad => \snake_ctrl_inst|Selector131~0_combout\,
	combout => \snake_ctrl_inst|Selector131~3_combout\);

-- Location: LCCOMB_X72_Y69_N4
\snake_ctrl_inst|Selector131~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector131~1_combout\ = (\snake_ctrl_inst|state.DRAW_NEW_TAIL~q\) # ((\snake_ctrl_inst|state.DELETE_OLD_TAIL~q\) # ((\snake_ctrl_inst|state.DRAW_NEW_HEAD~q\) # (\snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snake_ctrl_inst|state.DRAW_NEW_TAIL~q\,
	datab => \snake_ctrl_inst|state.DELETE_OLD_TAIL~q\,
	datac => \snake_ctrl_inst|state.DRAW_NEW_HEAD~q\,
	datad => \snake_ctrl_inst|state.REPLACE_OLD_HEAD~q\,
	combout => \snake_ctrl_inst|Selector131~1_combout\);

-- Location: LCCOMB_X72_Y69_N14
\snake_ctrl_inst|Selector131~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|Selector131~4_combout\ = (\snake_ctrl_inst|Selector131~3_combout\) # ((!\busy~input_o\ & (\snake_ctrl_inst|Selector131~1_combout\ & \snake_ctrl_inst|cur_player~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \busy~input_o\,
	datab => \snake_ctrl_inst|Selector131~3_combout\,
	datac => \snake_ctrl_inst|Selector131~1_combout\,
	datad => \snake_ctrl_inst|cur_player~q\,
	combout => \snake_ctrl_inst|Selector131~4_combout\);

-- Location: FF_X72_Y69_N15
\snake_ctrl_inst|wr_data[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|Selector131~4_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|wr_data\(5));

-- Location: LCCOMB_X72_Y69_N10
\snake_ctrl_inst|done~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \snake_ctrl_inst|done~feeder_combout\ = \snake_ctrl_inst|state.SET_DONE~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \snake_ctrl_inst|state.SET_DONE~q\,
	combout => \snake_ctrl_inst|done~feeder_combout\);

-- Location: FF_X72_Y69_N11
\snake_ctrl_inst|done\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snake_ctrl_inst|done~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snake_ctrl_inst|done~q\);

-- Location: IOIBUF_X115_Y65_N22
\rd_data[5]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd_data(5),
	o => \rd_data[5]~input_o\);

ww_rd <= \rd~output_o\;

\ww_rd_pos.y\(0) <= \rd_pos.y[0]~output_o\;

\ww_rd_pos.y\(1) <= \rd_pos.y[1]~output_o\;

\ww_rd_pos.y\(2) <= \rd_pos.y[2]~output_o\;

\ww_rd_pos.y\(3) <= \rd_pos.y[3]~output_o\;

\ww_rd_pos.y\(4) <= \rd_pos.y[4]~output_o\;

\ww_rd_pos.x\(0) <= \rd_pos.x[0]~output_o\;

\ww_rd_pos.x\(1) <= \rd_pos.x[1]~output_o\;

\ww_rd_pos.x\(2) <= \rd_pos.x[2]~output_o\;

\ww_rd_pos.x\(3) <= \rd_pos.x[3]~output_o\;

\ww_rd_pos.x\(4) <= \rd_pos.x[4]~output_o\;

ww_wr <= \wr~output_o\;

\ww_wr_pos.y\(0) <= \wr_pos.y[0]~output_o\;

\ww_wr_pos.y\(1) <= \wr_pos.y[1]~output_o\;

\ww_wr_pos.y\(2) <= \wr_pos.y[2]~output_o\;

\ww_wr_pos.y\(3) <= \wr_pos.y[3]~output_o\;

\ww_wr_pos.y\(4) <= \wr_pos.y[4]~output_o\;

\ww_wr_pos.x\(0) <= \wr_pos.x[0]~output_o\;

\ww_wr_pos.x\(1) <= \wr_pos.x[1]~output_o\;

\ww_wr_pos.x\(2) <= \wr_pos.x[2]~output_o\;

\ww_wr_pos.x\(3) <= \wr_pos.x[3]~output_o\;

\ww_wr_pos.x\(4) <= \wr_pos.x[4]~output_o\;

ww_wr_data(0) <= \wr_data[0]~output_o\;

ww_wr_data(1) <= \wr_data[1]~output_o\;

ww_wr_data(2) <= \wr_data[2]~output_o\;

ww_wr_data(3) <= \wr_data[3]~output_o\;

ww_wr_data(4) <= \wr_data[4]~output_o\;

ww_wr_data(5) <= \wr_data[5]~output_o\;

ww_done <= \done~output_o\;
END structure;


