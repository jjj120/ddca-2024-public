library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.snake_ctrl_pkg.all;

entity snake_ctrl_tb is
end entity;

architecture tb of snake_ctrl_tb is
	signal clk, res_n : std_logic := '0';
	signal stop_clk : boolean := false;
	constant CLK_PERIOD : time := 10 ns;

	signal player : player_t;
	signal busy : std_logic;
	signal rd, wr : std_logic;
	signal rd_pos, wr_pos : vec2d_t;
	signal rd_data, wr_data : std_logic_vector(SNAKE_FIELD_DATA_WIDTH-1 downto 0);
	signal init : std_logic;
	signal init_head_position : vec2d_t;
	signal init_head_direction : direction_t;
	signal init_body_length : std_logic_vector(3 downto 0);
	signal move_head : std_logic;
	signal movement_direction : direction_t;
	signal move_tail : std_logic;
	signal done : std_logic;
begin
	uut : snake_ctrl
	port map (
		clk   => clk,
		res_n => res_n,

		busy                => busy,
		rd                  => rd,
		rd_pos              => rd_pos,
		rd_data             => rd_data,
		wr                  => wr,
		wr_pos              => wr_pos,
		wr_data             => wr_data,

		player              => player,
		init                => init,
		init_head_position  => init_head_position,
		init_head_direction => init_head_direction,
		init_body_length    => init_body_length,
		move_head           => move_head,
		movement_direction  => movement_direction,
		move_tail           => move_tail,
		done                => done
	); 

	stim : process
	begin
		res_n <= '0';

		rd_data <= (others => '0');
		player <= PLAYER1;
		busy <= '0';

		init <= '0';
		init_head_position <= NULL_VEC;
		init_head_direction <= DOWN;
		init_body_length <= (others => '0');

		move_head <= '0';
		movement_direction <= DOWN;
		move_tail <= '0';
		wait for 10 * CLK_PERIOD;
		wait until rising_edge(clk);
		res_n <= '1';
		init <= '1';
		init_head_position <= create_vec2d(100, 100);
		init_head_direction <= RIGHT;
		init_body_length <= std_logic_vector(to_unsigned(5, 4));
		wait until rising_edge(clk);
		init <= '0';
		init_head_position <= NULL_VEC;
		init_head_direction <= DOWN;
		init_body_length <= (others => '0');
		wait until rising_edge(clk) and done = '1';
		move_head <= '1';
		movement_direction <= UP;
		wait until rising_edge(clk);
		move_head <= '0';
		wait until rising_edge(clk) and done = '1';
		move_tail <= '1';
		wait until rising_edge(clk);
		move_tail <= '0';
		wait until rising_edge(clk) and done = '1';

		wait for 200 * CLK_PERIOD;
		stop_clk <= True;
		report "Testbench done";
		wait;
	end process;

	clk_gen : process
	begin
		while not stop_clk loop
			clk <= '1';
			wait for CLK_PERIOD / 2;
			clk <= '0';
			wait for CLK_PERIOD / 2;
		end loop;
		report "Clock stop";
		wait;
	end process;

end architecture;
