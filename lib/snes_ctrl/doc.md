
# SNES Controller

This module provides an interface controller for the [SNES (Super Nintendo Entertainment System)](https://en.wikipedia.org/wiki/Super_Nintendo_Entertainment_System) controller.

## Dependencies
* None

## Required Files
 * `snes_ctrl_top.qarlog`
 * `snes_ctrl_top.qxp` (use **only for synthesis** (i.e., in quartus), not for simulation!)
 * `precompiled_snes_ctrl.vhd`
 * `snes_ctrl_pkg.vhd`
 * `snes_ctrl_top.vho` (use **only for simulation**, not for synthesis!)

## SNES Controller Component Declaration

```vhdl
component precompiled_snes_ctrl is
	port (
		clk : in std_logic;
		res_n : in std_logic;

		-- interface to SNES controller
		snes_clk   : out std_logic;
		snes_latch : out std_logic;
		snes_data  : in std_logic;

		-- button outputs
		ctrl_state : out snes_ctrl_state_t
	);
end component;
```

## SNES Controller Interface Description

The `precompiled_snes_ctrl` implements the serial SNES controller protocol on the external signal wires `snes_clk`, `snes_data` and `snes_latch`.
To satisfy the timing constraints of this protocl it expects that the clock frequency on the `clk` input is 50 MHz.
The `res_n` input is an active-low reset input.

The internal interface to the `precompiled_snes_ctrl` is fairly simple.
The core polls the controller state approximately every 20ms and updates its output `ctrl_state` accordingly.
This output is a record type with the following structure:

```vhdl
type snes_ctrl_state_t is record
	btn_up     : std_logic;
	btn_down   : std_logic;
	btn_left   : std_logic;
	btn_right  : std_logic;
	btn_a      : std_logic;
	btn_b      : std_logic;
	btn_x      : std_logic;
	btn_y      : std_logic;
	btn_l      : std_logic;
	btn_r      : std_logic;
	btn_start  : std_logic;
	btn_select : std_logic;
end record;
```

Each of the 12 SNES controller buttons corresponds to one signal in the record.
Hence, if the X button is pressed, `btn_x` will be set to `'1'`.
