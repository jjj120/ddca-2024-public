
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.snes_ctrl_pkg.all;

entity precompiled_snes_ctrl is
	port (
		clk : in std_logic;
		res_n : in std_logic;
		
		-- interface to SNES controller
		snes_clk   : out std_logic;
		snes_latch : out std_logic;
		snes_data  : in std_logic;
		
		-- button outputs
		ctrl_state : out snes_ctrl_state_t
	);
end entity;

architecture rtl of precompiled_snes_ctrl is
	signal ctrl_state_int : std_logic_vector(11 downto 0);
	component snes_ctrl_top is
		port (
			clk : in std_logic;
			res_n : in std_logic;
			snes_clk : out std_logic;
			snes_latch : out std_logic;
			snes_data : in std_logic;
			ctrl_state : out std_logic_vector(11 downto 0)
		);
	end component;
begin
	snes_ctrl_top_inst : snes_ctrl_top
	port map (
		clk        => clk,
		res_n      => res_n,
		snes_clk   => snes_clk,
		snes_latch => snes_latch,
		snes_data  => snes_data,
		ctrl_state => ctrl_state_int
	);
	ctrl_state <= to_snes_ctrl_state(ctrl_state_int);
end architecture;

