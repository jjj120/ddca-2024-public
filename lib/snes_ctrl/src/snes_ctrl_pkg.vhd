library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package snes_ctrl_pkg is

	type snes_ctrl_state_t is record
		btn_up     : std_logic;
		btn_down   : std_logic;
		btn_left   : std_logic;
		btn_right  : std_logic;
		btn_a      : std_logic;
		btn_b      : std_logic;
		btn_x      : std_logic;
		btn_y      : std_logic;
		btn_l      : std_logic;
		btn_r      : std_logic;
		btn_start  : std_logic;
		btn_select : std_logic;
	end record;

	constant SNES_CTRL_STATE_RESET_VALUE : snes_ctrl_state_t := (others=>'0');

	component snes_ctrl is
		generic (
			CLK_FREQ : integer := 50000000;
			CLK_OUT_FREQ : integer := 1000000;
			REFRESH_TIMEOUT : integer := 1000
		);
		port (
			clk : in std_logic;
			res_n : in std_logic;
			snes_clk : out std_logic;
			snes_latch : out std_logic;
			snes_data : in std_logic;
			ctrl_state : out snes_ctrl_state_t
		);
	end component;
	
	component precompiled_snes_ctrl is
		port (
			clk : in std_logic;
			res_n : in std_logic;
			
			-- interface to SNES controller
			snes_clk   : out std_logic;
			snes_latch : out std_logic;
			snes_data  : in std_logic;
			
			-- button outputs
			ctrl_state : out snes_ctrl_state_t
		);
	end component;
	
	function to_slv (s : snes_ctrl_state_t) return std_logic_vector;
	function to_snes_ctrl_state(slv : std_logic_vector(11 downto 0)) return snes_ctrl_state_t;
	
end package;

package body snes_ctrl_pkg is
	function to_slv(s : snes_ctrl_state_t) return std_logic_vector is
		variable ret : std_logic_vector(11 downto 0);
	begin
		ret(0)  := s.btn_b;
		ret(1)  := s.btn_y;
		ret(2)  := s.btn_select;
		ret(3)  := s.btn_start;
		ret(4)  := s.btn_up;
		ret(5)  := s.btn_down;
		ret(6)  := s.btn_left;
		ret(7)  := s.btn_right;
		ret(8)  := s.btn_a;
		ret(9)  := s.btn_x;
		ret(10) := s.btn_l;
		ret(11) := s.btn_r;
		return ret;
	end function;

	function to_snes_ctrl_state(slv : std_logic_vector(11 downto 0)) return snes_ctrl_state_t is
		variable ret : snes_ctrl_state_t;
	begin
		ret.btn_b      := slv(0);
		ret.btn_y      := slv(1);
		ret.btn_select := slv(2);
		ret.btn_start  := slv(3);
		ret.btn_up     := slv(4);
		ret.btn_down   := slv(5);
		ret.btn_left   := slv(6);
		ret.btn_right  := slv(7);
		ret.btn_a      := slv(8);
		ret.btn_x      := slv(9);
		ret.btn_l      := slv(10);
		ret.btn_r      := slv(11);
		return ret;
	end function;
end package body;
