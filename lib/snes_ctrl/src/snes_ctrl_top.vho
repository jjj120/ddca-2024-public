-- Copyright (C) 2023  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 22.1std.2 Build 922 07/20/2023 SC Standard Edition"

-- DATE "04/10/2024 14:04:11"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim (VHDL) only
-- 

LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_ASDO_DATA1~	=>  Location: PIN_F4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_FLASH_nCE_nCSO~	=>  Location: PIN_E2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DCLK~	=>  Location: PIN_P3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DATA0~	=>  Location: PIN_N7,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~padout\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~padout\ : std_logic;
SIGNAL \~ALTERA_DATA0~~padout\ : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_DATA0~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	snes_ctrl_top IS
    PORT (
	clk : IN std_logic;
	res_n : IN std_logic;
	snes_clk : OUT std_logic;
	snes_latch : OUT std_logic;
	snes_data : IN std_logic;
	ctrl_state : OUT std_logic_vector(11 DOWNTO 0)
	);
END snes_ctrl_top;

-- Design Ports Information
-- snes_clk	=>  Location: PIN_B17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- snes_latch	=>  Location: PIN_H17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[0]	=>  Location: PIN_D16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[1]	=>  Location: PIN_J15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[2]	=>  Location: PIN_J16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[3]	=>  Location: PIN_A17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[4]	=>  Location: PIN_C16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[5]	=>  Location: PIN_H16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[6]	=>  Location: PIN_J17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[7]	=>  Location: PIN_G19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[8]	=>  Location: PIN_G15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[9]	=>  Location: PIN_E17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[10]	=>  Location: PIN_G18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ctrl_state[11]	=>  Location: PIN_G16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clk	=>  Location: PIN_J1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- res_n	=>  Location: PIN_Y2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- snes_data	=>  Location: PIN_F17,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF snes_ctrl_top IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_res_n : std_logic;
SIGNAL ww_snes_clk : std_logic;
SIGNAL ww_snes_latch : std_logic;
SIGNAL ww_snes_data : std_logic;
SIGNAL ww_ctrl_state : std_logic_vector(11 DOWNTO 0);
SIGNAL \res_n~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \clk~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \snes_clk~output_o\ : std_logic;
SIGNAL \snes_latch~output_o\ : std_logic;
SIGNAL \ctrl_state[0]~output_o\ : std_logic;
SIGNAL \ctrl_state[1]~output_o\ : std_logic;
SIGNAL \ctrl_state[2]~output_o\ : std_logic;
SIGNAL \ctrl_state[3]~output_o\ : std_logic;
SIGNAL \ctrl_state[4]~output_o\ : std_logic;
SIGNAL \ctrl_state[5]~output_o\ : std_logic;
SIGNAL \ctrl_state[6]~output_o\ : std_logic;
SIGNAL \ctrl_state[7]~output_o\ : std_logic;
SIGNAL \ctrl_state[8]~output_o\ : std_logic;
SIGNAL \ctrl_state[9]~output_o\ : std_logic;
SIGNAL \ctrl_state[10]~output_o\ : std_logic;
SIGNAL \ctrl_state[11]~output_o\ : std_logic;
SIGNAL \clk~input_o\ : std_logic;
SIGNAL \clk~inputclkctrl_outclk\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[0]~16_combout\ : std_logic;
SIGNAL \res_n~input_o\ : std_logic;
SIGNAL \res_n~inputclkctrl_outclk\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[1]~19\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[2]~20_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[2]~21\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[3]~22_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[3]~23\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[4]~26_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[4]~27\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[5]~28_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[5]~29\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[6]~30_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[6]~31\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[7]~32_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[7]~33\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[8]~34_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[8]~35\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[9]~36_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[9]~37\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[10]~38_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[10]~39\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[11]~40_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[11]~41\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[12]~42_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[12]~43\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[13]~44_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[13]~45\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[14]~46_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[14]~47\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[15]~48_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|Equal0~2_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|Equal0~0_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|Equal1~0_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|Equal0~1_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|Equal1~1_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|Equal1~2_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|Equal0~4_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|Equal0~3_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|Selector4~1_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|Selector1~0_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|state.LATCH~q\ : std_logic;
SIGNAL \snes_ctrl_inst|Equal1~3_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|state.LATCH_WAIT~0_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|state.LATCH_WAIT~q\ : std_logic;
SIGNAL \snes_ctrl_inst|Selector4~2_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|Selector4~3_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|Selector3~0_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|state.CLK_HIGH~q\ : std_logic;
SIGNAL \snes_ctrl_inst|bit_cnt[0]~6_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|bit_cnt[1]~5_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|bit_cnt[3]~7_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|bit_cnt[2]~4_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|bit_cnt[3]~2_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|bit_cnt[3]~3_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|Selector0~0_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|Selector0~1_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|state.WAIT_TIMEOUT~q\ : std_logic;
SIGNAL \snes_ctrl_inst|Selector5~0_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|Selector5~1_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|Selector5~2_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|state.CLK_LOW~q\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[8]~24_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[8]~25_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[0]~17\ : std_logic;
SIGNAL \snes_ctrl_inst|clk_cnt[1]~18_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|Selector4~0_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|Selector4~4_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|state.SAMPLE~q\ : std_logic;
SIGNAL \snes_ctrl_inst|snes_clk~0_combout\ : std_logic;
SIGNAL \snes_data~input_o\ : std_logic;
SIGNAL \snes_ctrl_inst|shift_reg[0]~0_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|shift_reg[1]~feeder_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|shift_reg[2]~feeder_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|shift_reg[3]~feeder_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|shift_reg[4]~feeder_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|shift_reg[5]~feeder_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|shift_reg[7]~feeder_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|shift_reg[8]~feeder_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|shift_reg[9]~feeder_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|shift_reg[13]~feeder_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|shift_reg[14]~feeder_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|shift_reg[15]~feeder_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|ctrl_state.btn_b~feeder_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|ctrl_state.btn_b~q\ : std_logic;
SIGNAL \snes_ctrl_inst|ctrl_state.btn_y~feeder_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|ctrl_state.btn_y~q\ : std_logic;
SIGNAL \snes_ctrl_inst|ctrl_state.btn_select~feeder_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|ctrl_state.btn_select~q\ : std_logic;
SIGNAL \snes_ctrl_inst|ctrl_state.btn_start~feeder_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|ctrl_state.btn_start~q\ : std_logic;
SIGNAL \snes_ctrl_inst|ctrl_state.btn_up~q\ : std_logic;
SIGNAL \snes_ctrl_inst|ctrl_state.btn_down~q\ : std_logic;
SIGNAL \snes_ctrl_inst|ctrl_state.btn_left~q\ : std_logic;
SIGNAL \snes_ctrl_inst|ctrl_state.btn_right~feeder_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|ctrl_state.btn_right~q\ : std_logic;
SIGNAL \snes_ctrl_inst|ctrl_state.btn_a~feeder_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|ctrl_state.btn_a~q\ : std_logic;
SIGNAL \snes_ctrl_inst|ctrl_state.btn_x~feeder_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|ctrl_state.btn_x~q\ : std_logic;
SIGNAL \snes_ctrl_inst|ctrl_state.btn_l~q\ : std_logic;
SIGNAL \snes_ctrl_inst|ctrl_state.btn_r~feeder_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|ctrl_state.btn_r~q\ : std_logic;
SIGNAL \snes_ctrl_inst|shift_reg\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \snes_ctrl_inst|clk_cnt\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \snes_ctrl_inst|bit_cnt\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \snes_ctrl_inst|ALT_INV_state.WAIT_TIMEOUT~q\ : std_logic;
SIGNAL \snes_ctrl_inst|ALT_INV_snes_clk~0_combout\ : std_logic;
SIGNAL \snes_ctrl_inst|ALT_INV_state.SAMPLE~q\ : std_logic;

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

ww_clk <= clk;
ww_res_n <= res_n;
snes_clk <= ww_snes_clk;
snes_latch <= ww_snes_latch;
ww_snes_data <= snes_data;
ctrl_state <= ww_ctrl_state;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\res_n~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \res_n~input_o\);

\clk~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \clk~input_o\);
\snes_ctrl_inst|ALT_INV_state.WAIT_TIMEOUT~q\ <= NOT \snes_ctrl_inst|state.WAIT_TIMEOUT~q\;
\snes_ctrl_inst|ALT_INV_snes_clk~0_combout\ <= NOT \snes_ctrl_inst|snes_clk~0_combout\;
\snes_ctrl_inst|ALT_INV_state.SAMPLE~q\ <= NOT \snes_ctrl_inst|state.SAMPLE~q\;
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: IOOBUF_X60_Y73_N9
\snes_clk~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snes_ctrl_inst|ALT_INV_snes_clk~0_combout\,
	devoe => ww_devoe,
	o => \snes_clk~output_o\);

-- Location: IOOBUF_X67_Y73_N9
\snes_latch~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snes_ctrl_inst|state.LATCH~q\,
	devoe => ww_devoe,
	o => \snes_latch~output_o\);

-- Location: IOOBUF_X62_Y73_N23
\ctrl_state[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snes_ctrl_inst|ctrl_state.btn_b~q\,
	devoe => ww_devoe,
	o => \ctrl_state[0]~output_o\);

-- Location: IOOBUF_X60_Y73_N23
\ctrl_state[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snes_ctrl_inst|ctrl_state.btn_y~q\,
	devoe => ww_devoe,
	o => \ctrl_state[1]~output_o\);

-- Location: IOOBUF_X65_Y73_N16
\ctrl_state[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snes_ctrl_inst|ctrl_state.btn_select~q\,
	devoe => ww_devoe,
	o => \ctrl_state[2]~output_o\);

-- Location: IOOBUF_X60_Y73_N2
\ctrl_state[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snes_ctrl_inst|ctrl_state.btn_start~q\,
	devoe => ww_devoe,
	o => \ctrl_state[3]~output_o\);

-- Location: IOOBUF_X62_Y73_N16
\ctrl_state[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snes_ctrl_inst|ctrl_state.btn_up~q\,
	devoe => ww_devoe,
	o => \ctrl_state[4]~output_o\);

-- Location: IOOBUF_X65_Y73_N23
\ctrl_state[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snes_ctrl_inst|ctrl_state.btn_down~q\,
	devoe => ww_devoe,
	o => \ctrl_state[5]~output_o\);

-- Location: IOOBUF_X69_Y73_N2
\ctrl_state[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snes_ctrl_inst|ctrl_state.btn_left~q\,
	devoe => ww_devoe,
	o => \ctrl_state[6]~output_o\);

-- Location: IOOBUF_X69_Y73_N16
\ctrl_state[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snes_ctrl_inst|ctrl_state.btn_right~q\,
	devoe => ww_devoe,
	o => \ctrl_state[7]~output_o\);

-- Location: IOOBUF_X65_Y73_N9
\ctrl_state[8]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snes_ctrl_inst|ctrl_state.btn_a~q\,
	devoe => ww_devoe,
	o => \ctrl_state[8]~output_o\);

-- Location: IOOBUF_X67_Y73_N23
\ctrl_state[9]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snes_ctrl_inst|ctrl_state.btn_x~q\,
	devoe => ww_devoe,
	o => \ctrl_state[9]~output_o\);

-- Location: IOOBUF_X69_Y73_N23
\ctrl_state[10]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snes_ctrl_inst|ctrl_state.btn_l~q\,
	devoe => ww_devoe,
	o => \ctrl_state[10]~output_o\);

-- Location: IOOBUF_X67_Y73_N2
\ctrl_state[11]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \snes_ctrl_inst|ctrl_state.btn_r~q\,
	devoe => ww_devoe,
	o => \ctrl_state[11]~output_o\);

-- Location: IOIBUF_X0_Y36_N8
\clk~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk,
	o => \clk~input_o\);

-- Location: CLKCTRL_G2
\clk~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clk~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clk~inputclkctrl_outclk\);

-- Location: LCCOMB_X69_Y71_N0
\snes_ctrl_inst|clk_cnt[0]~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|clk_cnt[0]~16_combout\ = \snes_ctrl_inst|clk_cnt\(0) $ (VCC)
-- \snes_ctrl_inst|clk_cnt[0]~17\ = CARRY(\snes_ctrl_inst|clk_cnt\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \snes_ctrl_inst|clk_cnt\(0),
	datad => VCC,
	combout => \snes_ctrl_inst|clk_cnt[0]~16_combout\,
	cout => \snes_ctrl_inst|clk_cnt[0]~17\);

-- Location: IOIBUF_X0_Y36_N15
\res_n~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_res_n,
	o => \res_n~input_o\);

-- Location: CLKCTRL_G4
\res_n~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \res_n~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \res_n~inputclkctrl_outclk\);

-- Location: LCCOMB_X69_Y71_N2
\snes_ctrl_inst|clk_cnt[1]~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|clk_cnt[1]~18_combout\ = (\snes_ctrl_inst|clk_cnt\(1) & (!\snes_ctrl_inst|clk_cnt[0]~17\)) # (!\snes_ctrl_inst|clk_cnt\(1) & ((\snes_ctrl_inst|clk_cnt[0]~17\) # (GND)))
-- \snes_ctrl_inst|clk_cnt[1]~19\ = CARRY((!\snes_ctrl_inst|clk_cnt[0]~17\) # (!\snes_ctrl_inst|clk_cnt\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \snes_ctrl_inst|clk_cnt\(1),
	datad => VCC,
	cin => \snes_ctrl_inst|clk_cnt[0]~17\,
	combout => \snes_ctrl_inst|clk_cnt[1]~18_combout\,
	cout => \snes_ctrl_inst|clk_cnt[1]~19\);

-- Location: LCCOMB_X69_Y71_N4
\snes_ctrl_inst|clk_cnt[2]~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|clk_cnt[2]~20_combout\ = (\snes_ctrl_inst|clk_cnt\(2) & (\snes_ctrl_inst|clk_cnt[1]~19\ $ (GND))) # (!\snes_ctrl_inst|clk_cnt\(2) & (!\snes_ctrl_inst|clk_cnt[1]~19\ & VCC))
-- \snes_ctrl_inst|clk_cnt[2]~21\ = CARRY((\snes_ctrl_inst|clk_cnt\(2) & !\snes_ctrl_inst|clk_cnt[1]~19\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \snes_ctrl_inst|clk_cnt\(2),
	datad => VCC,
	cin => \snes_ctrl_inst|clk_cnt[1]~19\,
	combout => \snes_ctrl_inst|clk_cnt[2]~20_combout\,
	cout => \snes_ctrl_inst|clk_cnt[2]~21\);

-- Location: FF_X69_Y71_N5
\snes_ctrl_inst|clk_cnt[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|clk_cnt[2]~20_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snes_ctrl_inst|clk_cnt[8]~25_combout\,
	ena => \snes_ctrl_inst|ALT_INV_state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|clk_cnt\(2));

-- Location: LCCOMB_X69_Y71_N6
\snes_ctrl_inst|clk_cnt[3]~22\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|clk_cnt[3]~22_combout\ = (\snes_ctrl_inst|clk_cnt\(3) & (!\snes_ctrl_inst|clk_cnt[2]~21\)) # (!\snes_ctrl_inst|clk_cnt\(3) & ((\snes_ctrl_inst|clk_cnt[2]~21\) # (GND)))
-- \snes_ctrl_inst|clk_cnt[3]~23\ = CARRY((!\snes_ctrl_inst|clk_cnt[2]~21\) # (!\snes_ctrl_inst|clk_cnt\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|clk_cnt\(3),
	datad => VCC,
	cin => \snes_ctrl_inst|clk_cnt[2]~21\,
	combout => \snes_ctrl_inst|clk_cnt[3]~22_combout\,
	cout => \snes_ctrl_inst|clk_cnt[3]~23\);

-- Location: FF_X69_Y71_N7
\snes_ctrl_inst|clk_cnt[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|clk_cnt[3]~22_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snes_ctrl_inst|clk_cnt[8]~25_combout\,
	ena => \snes_ctrl_inst|ALT_INV_state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|clk_cnt\(3));

-- Location: LCCOMB_X69_Y71_N8
\snes_ctrl_inst|clk_cnt[4]~26\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|clk_cnt[4]~26_combout\ = (\snes_ctrl_inst|clk_cnt\(4) & (\snes_ctrl_inst|clk_cnt[3]~23\ $ (GND))) # (!\snes_ctrl_inst|clk_cnt\(4) & (!\snes_ctrl_inst|clk_cnt[3]~23\ & VCC))
-- \snes_ctrl_inst|clk_cnt[4]~27\ = CARRY((\snes_ctrl_inst|clk_cnt\(4) & !\snes_ctrl_inst|clk_cnt[3]~23\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \snes_ctrl_inst|clk_cnt\(4),
	datad => VCC,
	cin => \snes_ctrl_inst|clk_cnt[3]~23\,
	combout => \snes_ctrl_inst|clk_cnt[4]~26_combout\,
	cout => \snes_ctrl_inst|clk_cnt[4]~27\);

-- Location: FF_X69_Y71_N9
\snes_ctrl_inst|clk_cnt[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|clk_cnt[4]~26_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snes_ctrl_inst|clk_cnt[8]~25_combout\,
	ena => \snes_ctrl_inst|ALT_INV_state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|clk_cnt\(4));

-- Location: LCCOMB_X69_Y71_N10
\snes_ctrl_inst|clk_cnt[5]~28\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|clk_cnt[5]~28_combout\ = (\snes_ctrl_inst|clk_cnt\(5) & (!\snes_ctrl_inst|clk_cnt[4]~27\)) # (!\snes_ctrl_inst|clk_cnt\(5) & ((\snes_ctrl_inst|clk_cnt[4]~27\) # (GND)))
-- \snes_ctrl_inst|clk_cnt[5]~29\ = CARRY((!\snes_ctrl_inst|clk_cnt[4]~27\) # (!\snes_ctrl_inst|clk_cnt\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|clk_cnt\(5),
	datad => VCC,
	cin => \snes_ctrl_inst|clk_cnt[4]~27\,
	combout => \snes_ctrl_inst|clk_cnt[5]~28_combout\,
	cout => \snes_ctrl_inst|clk_cnt[5]~29\);

-- Location: FF_X69_Y71_N11
\snes_ctrl_inst|clk_cnt[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|clk_cnt[5]~28_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snes_ctrl_inst|clk_cnt[8]~25_combout\,
	ena => \snes_ctrl_inst|ALT_INV_state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|clk_cnt\(5));

-- Location: LCCOMB_X69_Y71_N12
\snes_ctrl_inst|clk_cnt[6]~30\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|clk_cnt[6]~30_combout\ = (\snes_ctrl_inst|clk_cnt\(6) & (\snes_ctrl_inst|clk_cnt[5]~29\ $ (GND))) # (!\snes_ctrl_inst|clk_cnt\(6) & (!\snes_ctrl_inst|clk_cnt[5]~29\ & VCC))
-- \snes_ctrl_inst|clk_cnt[6]~31\ = CARRY((\snes_ctrl_inst|clk_cnt\(6) & !\snes_ctrl_inst|clk_cnt[5]~29\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|clk_cnt\(6),
	datad => VCC,
	cin => \snes_ctrl_inst|clk_cnt[5]~29\,
	combout => \snes_ctrl_inst|clk_cnt[6]~30_combout\,
	cout => \snes_ctrl_inst|clk_cnt[6]~31\);

-- Location: FF_X69_Y71_N13
\snes_ctrl_inst|clk_cnt[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|clk_cnt[6]~30_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snes_ctrl_inst|clk_cnt[8]~25_combout\,
	ena => \snes_ctrl_inst|ALT_INV_state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|clk_cnt\(6));

-- Location: LCCOMB_X69_Y71_N14
\snes_ctrl_inst|clk_cnt[7]~32\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|clk_cnt[7]~32_combout\ = (\snes_ctrl_inst|clk_cnt\(7) & (!\snes_ctrl_inst|clk_cnt[6]~31\)) # (!\snes_ctrl_inst|clk_cnt\(7) & ((\snes_ctrl_inst|clk_cnt[6]~31\) # (GND)))
-- \snes_ctrl_inst|clk_cnt[7]~33\ = CARRY((!\snes_ctrl_inst|clk_cnt[6]~31\) # (!\snes_ctrl_inst|clk_cnt\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \snes_ctrl_inst|clk_cnt\(7),
	datad => VCC,
	cin => \snes_ctrl_inst|clk_cnt[6]~31\,
	combout => \snes_ctrl_inst|clk_cnt[7]~32_combout\,
	cout => \snes_ctrl_inst|clk_cnt[7]~33\);

-- Location: FF_X69_Y71_N15
\snes_ctrl_inst|clk_cnt[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|clk_cnt[7]~32_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snes_ctrl_inst|clk_cnt[8]~25_combout\,
	ena => \snes_ctrl_inst|ALT_INV_state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|clk_cnt\(7));

-- Location: LCCOMB_X69_Y71_N16
\snes_ctrl_inst|clk_cnt[8]~34\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|clk_cnt[8]~34_combout\ = (\snes_ctrl_inst|clk_cnt\(8) & (\snes_ctrl_inst|clk_cnt[7]~33\ $ (GND))) # (!\snes_ctrl_inst|clk_cnt\(8) & (!\snes_ctrl_inst|clk_cnt[7]~33\ & VCC))
-- \snes_ctrl_inst|clk_cnt[8]~35\ = CARRY((\snes_ctrl_inst|clk_cnt\(8) & !\snes_ctrl_inst|clk_cnt[7]~33\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \snes_ctrl_inst|clk_cnt\(8),
	datad => VCC,
	cin => \snes_ctrl_inst|clk_cnt[7]~33\,
	combout => \snes_ctrl_inst|clk_cnt[8]~34_combout\,
	cout => \snes_ctrl_inst|clk_cnt[8]~35\);

-- Location: FF_X69_Y71_N17
\snes_ctrl_inst|clk_cnt[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|clk_cnt[8]~34_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snes_ctrl_inst|clk_cnt[8]~25_combout\,
	ena => \snes_ctrl_inst|ALT_INV_state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|clk_cnt\(8));

-- Location: LCCOMB_X69_Y71_N18
\snes_ctrl_inst|clk_cnt[9]~36\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|clk_cnt[9]~36_combout\ = (\snes_ctrl_inst|clk_cnt\(9) & (!\snes_ctrl_inst|clk_cnt[8]~35\)) # (!\snes_ctrl_inst|clk_cnt\(9) & ((\snes_ctrl_inst|clk_cnt[8]~35\) # (GND)))
-- \snes_ctrl_inst|clk_cnt[9]~37\ = CARRY((!\snes_ctrl_inst|clk_cnt[8]~35\) # (!\snes_ctrl_inst|clk_cnt\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \snes_ctrl_inst|clk_cnt\(9),
	datad => VCC,
	cin => \snes_ctrl_inst|clk_cnt[8]~35\,
	combout => \snes_ctrl_inst|clk_cnt[9]~36_combout\,
	cout => \snes_ctrl_inst|clk_cnt[9]~37\);

-- Location: FF_X69_Y71_N19
\snes_ctrl_inst|clk_cnt[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|clk_cnt[9]~36_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snes_ctrl_inst|clk_cnt[8]~25_combout\,
	ena => \snes_ctrl_inst|ALT_INV_state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|clk_cnt\(9));

-- Location: LCCOMB_X69_Y71_N20
\snes_ctrl_inst|clk_cnt[10]~38\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|clk_cnt[10]~38_combout\ = (\snes_ctrl_inst|clk_cnt\(10) & (\snes_ctrl_inst|clk_cnt[9]~37\ $ (GND))) # (!\snes_ctrl_inst|clk_cnt\(10) & (!\snes_ctrl_inst|clk_cnt[9]~37\ & VCC))
-- \snes_ctrl_inst|clk_cnt[10]~39\ = CARRY((\snes_ctrl_inst|clk_cnt\(10) & !\snes_ctrl_inst|clk_cnt[9]~37\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \snes_ctrl_inst|clk_cnt\(10),
	datad => VCC,
	cin => \snes_ctrl_inst|clk_cnt[9]~37\,
	combout => \snes_ctrl_inst|clk_cnt[10]~38_combout\,
	cout => \snes_ctrl_inst|clk_cnt[10]~39\);

-- Location: FF_X69_Y71_N21
\snes_ctrl_inst|clk_cnt[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|clk_cnt[10]~38_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snes_ctrl_inst|clk_cnt[8]~25_combout\,
	ena => \snes_ctrl_inst|ALT_INV_state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|clk_cnt\(10));

-- Location: LCCOMB_X69_Y71_N22
\snes_ctrl_inst|clk_cnt[11]~40\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|clk_cnt[11]~40_combout\ = (\snes_ctrl_inst|clk_cnt\(11) & (!\snes_ctrl_inst|clk_cnt[10]~39\)) # (!\snes_ctrl_inst|clk_cnt\(11) & ((\snes_ctrl_inst|clk_cnt[10]~39\) # (GND)))
-- \snes_ctrl_inst|clk_cnt[11]~41\ = CARRY((!\snes_ctrl_inst|clk_cnt[10]~39\) # (!\snes_ctrl_inst|clk_cnt\(11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|clk_cnt\(11),
	datad => VCC,
	cin => \snes_ctrl_inst|clk_cnt[10]~39\,
	combout => \snes_ctrl_inst|clk_cnt[11]~40_combout\,
	cout => \snes_ctrl_inst|clk_cnt[11]~41\);

-- Location: FF_X69_Y71_N23
\snes_ctrl_inst|clk_cnt[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|clk_cnt[11]~40_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snes_ctrl_inst|clk_cnt[8]~25_combout\,
	ena => \snes_ctrl_inst|ALT_INV_state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|clk_cnt\(11));

-- Location: LCCOMB_X69_Y71_N24
\snes_ctrl_inst|clk_cnt[12]~42\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|clk_cnt[12]~42_combout\ = (\snes_ctrl_inst|clk_cnt\(12) & (\snes_ctrl_inst|clk_cnt[11]~41\ $ (GND))) # (!\snes_ctrl_inst|clk_cnt\(12) & (!\snes_ctrl_inst|clk_cnt[11]~41\ & VCC))
-- \snes_ctrl_inst|clk_cnt[12]~43\ = CARRY((\snes_ctrl_inst|clk_cnt\(12) & !\snes_ctrl_inst|clk_cnt[11]~41\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \snes_ctrl_inst|clk_cnt\(12),
	datad => VCC,
	cin => \snes_ctrl_inst|clk_cnt[11]~41\,
	combout => \snes_ctrl_inst|clk_cnt[12]~42_combout\,
	cout => \snes_ctrl_inst|clk_cnt[12]~43\);

-- Location: FF_X69_Y71_N25
\snes_ctrl_inst|clk_cnt[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|clk_cnt[12]~42_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snes_ctrl_inst|clk_cnt[8]~25_combout\,
	ena => \snes_ctrl_inst|ALT_INV_state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|clk_cnt\(12));

-- Location: LCCOMB_X69_Y71_N26
\snes_ctrl_inst|clk_cnt[13]~44\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|clk_cnt[13]~44_combout\ = (\snes_ctrl_inst|clk_cnt\(13) & (!\snes_ctrl_inst|clk_cnt[12]~43\)) # (!\snes_ctrl_inst|clk_cnt\(13) & ((\snes_ctrl_inst|clk_cnt[12]~43\) # (GND)))
-- \snes_ctrl_inst|clk_cnt[13]~45\ = CARRY((!\snes_ctrl_inst|clk_cnt[12]~43\) # (!\snes_ctrl_inst|clk_cnt\(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|clk_cnt\(13),
	datad => VCC,
	cin => \snes_ctrl_inst|clk_cnt[12]~43\,
	combout => \snes_ctrl_inst|clk_cnt[13]~44_combout\,
	cout => \snes_ctrl_inst|clk_cnt[13]~45\);

-- Location: FF_X69_Y71_N27
\snes_ctrl_inst|clk_cnt[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|clk_cnt[13]~44_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snes_ctrl_inst|clk_cnt[8]~25_combout\,
	ena => \snes_ctrl_inst|ALT_INV_state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|clk_cnt\(13));

-- Location: LCCOMB_X69_Y71_N28
\snes_ctrl_inst|clk_cnt[14]~46\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|clk_cnt[14]~46_combout\ = (\snes_ctrl_inst|clk_cnt\(14) & (\snes_ctrl_inst|clk_cnt[13]~45\ $ (GND))) # (!\snes_ctrl_inst|clk_cnt\(14) & (!\snes_ctrl_inst|clk_cnt[13]~45\ & VCC))
-- \snes_ctrl_inst|clk_cnt[14]~47\ = CARRY((\snes_ctrl_inst|clk_cnt\(14) & !\snes_ctrl_inst|clk_cnt[13]~45\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \snes_ctrl_inst|clk_cnt\(14),
	datad => VCC,
	cin => \snes_ctrl_inst|clk_cnt[13]~45\,
	combout => \snes_ctrl_inst|clk_cnt[14]~46_combout\,
	cout => \snes_ctrl_inst|clk_cnt[14]~47\);

-- Location: FF_X69_Y71_N29
\snes_ctrl_inst|clk_cnt[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|clk_cnt[14]~46_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snes_ctrl_inst|clk_cnt[8]~25_combout\,
	ena => \snes_ctrl_inst|ALT_INV_state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|clk_cnt\(14));

-- Location: LCCOMB_X69_Y71_N30
\snes_ctrl_inst|clk_cnt[15]~48\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|clk_cnt[15]~48_combout\ = \snes_ctrl_inst|clk_cnt\(15) $ (\snes_ctrl_inst|clk_cnt[14]~47\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|clk_cnt\(15),
	cin => \snes_ctrl_inst|clk_cnt[14]~47\,
	combout => \snes_ctrl_inst|clk_cnt[15]~48_combout\);

-- Location: FF_X69_Y71_N31
\snes_ctrl_inst|clk_cnt[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|clk_cnt[15]~48_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snes_ctrl_inst|clk_cnt[8]~25_combout\,
	ena => \snes_ctrl_inst|ALT_INV_state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|clk_cnt\(15));

-- Location: LCCOMB_X69_Y70_N4
\snes_ctrl_inst|Equal0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|Equal0~2_combout\ = (!\snes_ctrl_inst|clk_cnt\(14) & (!\snes_ctrl_inst|clk_cnt\(15) & !\snes_ctrl_inst|clk_cnt\(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \snes_ctrl_inst|clk_cnt\(14),
	datac => \snes_ctrl_inst|clk_cnt\(15),
	datad => \snes_ctrl_inst|clk_cnt\(13),
	combout => \snes_ctrl_inst|Equal0~2_combout\);

-- Location: LCCOMB_X69_Y70_N24
\snes_ctrl_inst|Equal0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|Equal0~0_combout\ = (\snes_ctrl_inst|clk_cnt\(5) & (\snes_ctrl_inst|clk_cnt\(3) & (\snes_ctrl_inst|clk_cnt\(6) & !\snes_ctrl_inst|clk_cnt\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|clk_cnt\(5),
	datab => \snes_ctrl_inst|clk_cnt\(3),
	datac => \snes_ctrl_inst|clk_cnt\(6),
	datad => \snes_ctrl_inst|clk_cnt\(2),
	combout => \snes_ctrl_inst|Equal0~0_combout\);

-- Location: LCCOMB_X68_Y71_N10
\snes_ctrl_inst|Equal1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|Equal1~0_combout\ = (!\snes_ctrl_inst|clk_cnt\(8) & (\snes_ctrl_inst|clk_cnt\(4) & !\snes_ctrl_inst|clk_cnt\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \snes_ctrl_inst|clk_cnt\(8),
	datac => \snes_ctrl_inst|clk_cnt\(4),
	datad => \snes_ctrl_inst|clk_cnt\(9),
	combout => \snes_ctrl_inst|Equal1~0_combout\);

-- Location: LCCOMB_X69_Y70_N18
\snes_ctrl_inst|Equal0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|Equal0~1_combout\ = (!\snes_ctrl_inst|clk_cnt\(12) & (\snes_ctrl_inst|clk_cnt\(7) & (!\snes_ctrl_inst|clk_cnt\(11) & !\snes_ctrl_inst|clk_cnt\(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|clk_cnt\(12),
	datab => \snes_ctrl_inst|clk_cnt\(7),
	datac => \snes_ctrl_inst|clk_cnt\(11),
	datad => \snes_ctrl_inst|clk_cnt\(10),
	combout => \snes_ctrl_inst|Equal0~1_combout\);

-- Location: LCCOMB_X68_Y70_N0
\snes_ctrl_inst|Equal1~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|Equal1~1_combout\ = (\snes_ctrl_inst|Equal0~2_combout\ & (\snes_ctrl_inst|Equal0~0_combout\ & (\snes_ctrl_inst|Equal1~0_combout\ & \snes_ctrl_inst|Equal0~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|Equal0~2_combout\,
	datab => \snes_ctrl_inst|Equal0~0_combout\,
	datac => \snes_ctrl_inst|Equal1~0_combout\,
	datad => \snes_ctrl_inst|Equal0~1_combout\,
	combout => \snes_ctrl_inst|Equal1~1_combout\);

-- Location: LCCOMB_X68_Y71_N20
\snes_ctrl_inst|Equal1~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|Equal1~2_combout\ = (\snes_ctrl_inst|Equal1~1_combout\ & (!\snes_ctrl_inst|clk_cnt\(0) & \snes_ctrl_inst|clk_cnt\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \snes_ctrl_inst|Equal1~1_combout\,
	datac => \snes_ctrl_inst|clk_cnt\(0),
	datad => \snes_ctrl_inst|clk_cnt\(1),
	combout => \snes_ctrl_inst|Equal1~2_combout\);

-- Location: LCCOMB_X68_Y71_N30
\snes_ctrl_inst|Equal0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|Equal0~4_combout\ = (!\snes_ctrl_inst|clk_cnt\(1) & (!\snes_ctrl_inst|clk_cnt\(4) & (!\snes_ctrl_inst|clk_cnt\(0) & \snes_ctrl_inst|clk_cnt\(8))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|clk_cnt\(1),
	datab => \snes_ctrl_inst|clk_cnt\(4),
	datac => \snes_ctrl_inst|clk_cnt\(0),
	datad => \snes_ctrl_inst|clk_cnt\(8),
	combout => \snes_ctrl_inst|Equal0~4_combout\);

-- Location: LCCOMB_X68_Y70_N2
\snes_ctrl_inst|Equal0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|Equal0~3_combout\ = (\snes_ctrl_inst|Equal0~2_combout\ & (\snes_ctrl_inst|Equal0~0_combout\ & \snes_ctrl_inst|Equal0~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|Equal0~2_combout\,
	datab => \snes_ctrl_inst|Equal0~0_combout\,
	datad => \snes_ctrl_inst|Equal0~1_combout\,
	combout => \snes_ctrl_inst|Equal0~3_combout\);

-- Location: LCCOMB_X68_Y71_N18
\snes_ctrl_inst|Selector4~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|Selector4~1_combout\ = (\snes_ctrl_inst|Equal0~4_combout\ & (\snes_ctrl_inst|Equal0~3_combout\ & (!\snes_ctrl_inst|state.WAIT_TIMEOUT~q\ & \snes_ctrl_inst|clk_cnt\(9))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|Equal0~4_combout\,
	datab => \snes_ctrl_inst|Equal0~3_combout\,
	datac => \snes_ctrl_inst|state.WAIT_TIMEOUT~q\,
	datad => \snes_ctrl_inst|clk_cnt\(9),
	combout => \snes_ctrl_inst|Selector4~1_combout\);

-- Location: LCCOMB_X67_Y71_N26
\snes_ctrl_inst|Selector1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|Selector1~0_combout\ = (!\snes_ctrl_inst|state.SAMPLE~q\ & ((\snes_ctrl_inst|Selector4~3_combout\ & ((\snes_ctrl_inst|Selector4~1_combout\))) # (!\snes_ctrl_inst|Selector4~3_combout\ & (\snes_ctrl_inst|state.LATCH~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|Selector4~3_combout\,
	datab => \snes_ctrl_inst|state.SAMPLE~q\,
	datac => \snes_ctrl_inst|state.LATCH~q\,
	datad => \snes_ctrl_inst|Selector4~1_combout\,
	combout => \snes_ctrl_inst|Selector1~0_combout\);

-- Location: FF_X67_Y71_N27
\snes_ctrl_inst|state.LATCH\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|Selector1~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|state.LATCH~q\);

-- Location: LCCOMB_X68_Y71_N4
\snes_ctrl_inst|Equal1~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|Equal1~3_combout\ = (!\snes_ctrl_inst|clk_cnt\(0) & \snes_ctrl_inst|clk_cnt\(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \snes_ctrl_inst|clk_cnt\(0),
	datad => \snes_ctrl_inst|clk_cnt\(1),
	combout => \snes_ctrl_inst|Equal1~3_combout\);

-- Location: LCCOMB_X67_Y71_N12
\snes_ctrl_inst|state.LATCH_WAIT~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|state.LATCH_WAIT~0_combout\ = (\snes_ctrl_inst|Equal1~1_combout\ & ((\snes_ctrl_inst|Equal1~3_combout\ & (\snes_ctrl_inst|state.LATCH~q\)) # (!\snes_ctrl_inst|Equal1~3_combout\ & ((\snes_ctrl_inst|state.LATCH_WAIT~q\))))) # 
-- (!\snes_ctrl_inst|Equal1~1_combout\ & (((\snes_ctrl_inst|state.LATCH_WAIT~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|state.LATCH~q\,
	datab => \snes_ctrl_inst|Equal1~1_combout\,
	datac => \snes_ctrl_inst|state.LATCH_WAIT~q\,
	datad => \snes_ctrl_inst|Equal1~3_combout\,
	combout => \snes_ctrl_inst|state.LATCH_WAIT~0_combout\);

-- Location: FF_X67_Y71_N13
\snes_ctrl_inst|state.LATCH_WAIT\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|state.LATCH_WAIT~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|state.LATCH_WAIT~q\);

-- Location: LCCOMB_X67_Y71_N0
\snes_ctrl_inst|Selector4~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|Selector4~2_combout\ = (\snes_ctrl_inst|state.LATCH_WAIT~q\) # ((\snes_ctrl_inst|state.LATCH~q\) # (\snes_ctrl_inst|state.CLK_HIGH~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|state.LATCH_WAIT~q\,
	datac => \snes_ctrl_inst|state.LATCH~q\,
	datad => \snes_ctrl_inst|state.CLK_HIGH~q\,
	combout => \snes_ctrl_inst|Selector4~2_combout\);

-- Location: LCCOMB_X67_Y71_N10
\snes_ctrl_inst|Selector4~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|Selector4~3_combout\ = (\snes_ctrl_inst|Selector4~0_combout\) # ((\snes_ctrl_inst|Selector4~1_combout\) # ((\snes_ctrl_inst|Selector4~2_combout\ & \snes_ctrl_inst|Equal1~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|Selector4~0_combout\,
	datab => \snes_ctrl_inst|Selector4~2_combout\,
	datac => \snes_ctrl_inst|Equal1~2_combout\,
	datad => \snes_ctrl_inst|Selector4~1_combout\,
	combout => \snes_ctrl_inst|Selector4~3_combout\);

-- Location: LCCOMB_X67_Y71_N6
\snes_ctrl_inst|Selector3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|Selector3~0_combout\ = (\snes_ctrl_inst|state.SAMPLE~q\) # ((!\snes_ctrl_inst|Selector4~3_combout\ & \snes_ctrl_inst|state.CLK_HIGH~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110011011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|Selector4~3_combout\,
	datab => \snes_ctrl_inst|state.SAMPLE~q\,
	datac => \snes_ctrl_inst|state.CLK_HIGH~q\,
	combout => \snes_ctrl_inst|Selector3~0_combout\);

-- Location: FF_X67_Y71_N7
\snes_ctrl_inst|state.CLK_HIGH\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|Selector3~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|state.CLK_HIGH~q\);

-- Location: LCCOMB_X67_Y71_N24
\snes_ctrl_inst|bit_cnt[0]~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|bit_cnt[0]~6_combout\ = \snes_ctrl_inst|bit_cnt\(0) $ (((\snes_ctrl_inst|state.CLK_HIGH~q\ & (\snes_ctrl_inst|Equal1~1_combout\ & \snes_ctrl_inst|Equal1~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|state.CLK_HIGH~q\,
	datab => \snes_ctrl_inst|Equal1~1_combout\,
	datac => \snes_ctrl_inst|bit_cnt\(0),
	datad => \snes_ctrl_inst|Equal1~3_combout\,
	combout => \snes_ctrl_inst|bit_cnt[0]~6_combout\);

-- Location: FF_X67_Y71_N25
\snes_ctrl_inst|bit_cnt[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|bit_cnt[0]~6_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|bit_cnt\(0));

-- Location: LCCOMB_X67_Y71_N22
\snes_ctrl_inst|bit_cnt[1]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|bit_cnt[1]~5_combout\ = \snes_ctrl_inst|bit_cnt\(1) $ (((\snes_ctrl_inst|Equal1~2_combout\ & (\snes_ctrl_inst|bit_cnt\(0) & \snes_ctrl_inst|state.CLK_HIGH~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|Equal1~2_combout\,
	datab => \snes_ctrl_inst|bit_cnt\(0),
	datac => \snes_ctrl_inst|bit_cnt\(1),
	datad => \snes_ctrl_inst|state.CLK_HIGH~q\,
	combout => \snes_ctrl_inst|bit_cnt[1]~5_combout\);

-- Location: FF_X67_Y71_N23
\snes_ctrl_inst|bit_cnt[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|bit_cnt[1]~5_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|bit_cnt\(1));

-- Location: LCCOMB_X68_Y71_N12
\snes_ctrl_inst|bit_cnt[3]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|bit_cnt[3]~7_combout\ = (\snes_ctrl_inst|clk_cnt\(1) & (\snes_ctrl_inst|state.CLK_HIGH~q\ & (!\snes_ctrl_inst|clk_cnt\(0) & \snes_ctrl_inst|Equal1~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|clk_cnt\(1),
	datab => \snes_ctrl_inst|state.CLK_HIGH~q\,
	datac => \snes_ctrl_inst|clk_cnt\(0),
	datad => \snes_ctrl_inst|Equal1~1_combout\,
	combout => \snes_ctrl_inst|bit_cnt[3]~7_combout\);

-- Location: LCCOMB_X67_Y71_N20
\snes_ctrl_inst|bit_cnt[2]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|bit_cnt[2]~4_combout\ = \snes_ctrl_inst|bit_cnt\(2) $ (((\snes_ctrl_inst|bit_cnt\(1) & (\snes_ctrl_inst|bit_cnt[3]~7_combout\ & \snes_ctrl_inst|bit_cnt\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|bit_cnt\(1),
	datab => \snes_ctrl_inst|bit_cnt[3]~7_combout\,
	datac => \snes_ctrl_inst|bit_cnt\(2),
	datad => \snes_ctrl_inst|bit_cnt\(0),
	combout => \snes_ctrl_inst|bit_cnt[2]~4_combout\);

-- Location: FF_X67_Y71_N21
\snes_ctrl_inst|bit_cnt[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|bit_cnt[2]~4_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|bit_cnt\(2));

-- Location: LCCOMB_X67_Y71_N2
\snes_ctrl_inst|bit_cnt[3]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|bit_cnt[3]~2_combout\ = (\snes_ctrl_inst|bit_cnt\(2) & (\snes_ctrl_inst|bit_cnt\(1) & \snes_ctrl_inst|bit_cnt\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \snes_ctrl_inst|bit_cnt\(2),
	datac => \snes_ctrl_inst|bit_cnt\(1),
	datad => \snes_ctrl_inst|bit_cnt\(0),
	combout => \snes_ctrl_inst|bit_cnt[3]~2_combout\);

-- Location: LCCOMB_X68_Y71_N0
\snes_ctrl_inst|bit_cnt[3]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|bit_cnt[3]~3_combout\ = \snes_ctrl_inst|bit_cnt\(3) $ (((\snes_ctrl_inst|bit_cnt[3]~2_combout\ & (\snes_ctrl_inst|state.CLK_HIGH~q\ & \snes_ctrl_inst|Equal1~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|bit_cnt[3]~2_combout\,
	datab => \snes_ctrl_inst|state.CLK_HIGH~q\,
	datac => \snes_ctrl_inst|bit_cnt\(3),
	datad => \snes_ctrl_inst|Equal1~2_combout\,
	combout => \snes_ctrl_inst|bit_cnt[3]~3_combout\);

-- Location: FF_X68_Y71_N1
\snes_ctrl_inst|bit_cnt[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|bit_cnt[3]~3_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|bit_cnt\(3));

-- Location: LCCOMB_X68_Y71_N26
\snes_ctrl_inst|Selector0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|Selector0~0_combout\ = (!\snes_ctrl_inst|state.WAIT_TIMEOUT~q\ & (((!\snes_ctrl_inst|clk_cnt\(9)) # (!\snes_ctrl_inst|Equal0~3_combout\)) # (!\snes_ctrl_inst|Equal0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|Equal0~4_combout\,
	datab => \snes_ctrl_inst|Equal0~3_combout\,
	datac => \snes_ctrl_inst|state.WAIT_TIMEOUT~q\,
	datad => \snes_ctrl_inst|clk_cnt\(9),
	combout => \snes_ctrl_inst|Selector0~0_combout\);

-- Location: LCCOMB_X68_Y71_N8
\snes_ctrl_inst|Selector0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|Selector0~1_combout\ = (!\snes_ctrl_inst|Selector0~0_combout\ & (((!\snes_ctrl_inst|bit_cnt[3]~7_combout\) # (!\snes_ctrl_inst|bit_cnt\(3))) # (!\snes_ctrl_inst|bit_cnt[3]~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|bit_cnt[3]~2_combout\,
	datab => \snes_ctrl_inst|bit_cnt\(3),
	datac => \snes_ctrl_inst|Selector0~0_combout\,
	datad => \snes_ctrl_inst|bit_cnt[3]~7_combout\,
	combout => \snes_ctrl_inst|Selector0~1_combout\);

-- Location: FF_X68_Y71_N9
\snes_ctrl_inst|state.WAIT_TIMEOUT\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|Selector0~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|state.WAIT_TIMEOUT~q\);

-- Location: LCCOMB_X68_Y71_N6
\snes_ctrl_inst|Selector5~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|Selector5~0_combout\ = (\snes_ctrl_inst|state.CLK_LOW~q\ & ((\snes_ctrl_inst|clk_cnt\(1)) # ((!\snes_ctrl_inst|clk_cnt\(0)) # (!\snes_ctrl_inst|Equal1~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|clk_cnt\(1),
	datab => \snes_ctrl_inst|Equal1~1_combout\,
	datac => \snes_ctrl_inst|clk_cnt\(0),
	datad => \snes_ctrl_inst|state.CLK_LOW~q\,
	combout => \snes_ctrl_inst|Selector5~0_combout\);

-- Location: LCCOMB_X68_Y71_N2
\snes_ctrl_inst|Selector5~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|Selector5~1_combout\ = (\snes_ctrl_inst|state.LATCH_WAIT~q\) # ((\snes_ctrl_inst|state.CLK_HIGH~q\ & ((!\snes_ctrl_inst|bit_cnt\(3)) # (!\snes_ctrl_inst|bit_cnt[3]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|bit_cnt[3]~2_combout\,
	datab => \snes_ctrl_inst|bit_cnt\(3),
	datac => \snes_ctrl_inst|state.CLK_HIGH~q\,
	datad => \snes_ctrl_inst|state.LATCH_WAIT~q\,
	combout => \snes_ctrl_inst|Selector5~1_combout\);

-- Location: LCCOMB_X68_Y71_N24
\snes_ctrl_inst|Selector5~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|Selector5~2_combout\ = (\snes_ctrl_inst|Selector5~0_combout\) # ((\snes_ctrl_inst|Selector5~1_combout\ & \snes_ctrl_inst|Equal1~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|Selector5~0_combout\,
	datab => \snes_ctrl_inst|Selector5~1_combout\,
	datad => \snes_ctrl_inst|Equal1~2_combout\,
	combout => \snes_ctrl_inst|Selector5~2_combout\);

-- Location: FF_X68_Y71_N25
\snes_ctrl_inst|state.CLK_LOW\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|Selector5~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|state.CLK_LOW~q\);

-- Location: LCCOMB_X68_Y71_N22
\snes_ctrl_inst|clk_cnt[8]~24\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|clk_cnt[8]~24_combout\ = (\snes_ctrl_inst|state.WAIT_TIMEOUT~q\ & (\snes_ctrl_inst|Equal1~1_combout\ & (\snes_ctrl_inst|Equal1~3_combout\ & !\snes_ctrl_inst|state.CLK_LOW~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|state.WAIT_TIMEOUT~q\,
	datab => \snes_ctrl_inst|Equal1~1_combout\,
	datac => \snes_ctrl_inst|Equal1~3_combout\,
	datad => \snes_ctrl_inst|state.CLK_LOW~q\,
	combout => \snes_ctrl_inst|clk_cnt[8]~24_combout\);

-- Location: LCCOMB_X68_Y71_N16
\snes_ctrl_inst|clk_cnt[8]~25\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|clk_cnt[8]~25_combout\ = (\snes_ctrl_inst|Selector4~1_combout\) # ((\snes_ctrl_inst|clk_cnt[8]~24_combout\) # ((\snes_ctrl_inst|state.WAIT_TIMEOUT~q\ & \snes_ctrl_inst|Selector4~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|state.WAIT_TIMEOUT~q\,
	datab => \snes_ctrl_inst|Selector4~1_combout\,
	datac => \snes_ctrl_inst|clk_cnt[8]~24_combout\,
	datad => \snes_ctrl_inst|Selector4~0_combout\,
	combout => \snes_ctrl_inst|clk_cnt[8]~25_combout\);

-- Location: FF_X69_Y71_N1
\snes_ctrl_inst|clk_cnt[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|clk_cnt[0]~16_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snes_ctrl_inst|clk_cnt[8]~25_combout\,
	ena => \snes_ctrl_inst|ALT_INV_state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|clk_cnt\(0));

-- Location: FF_X69_Y71_N3
\snes_ctrl_inst|clk_cnt[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|clk_cnt[1]~18_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sclr => \snes_ctrl_inst|clk_cnt[8]~25_combout\,
	ena => \snes_ctrl_inst|ALT_INV_state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|clk_cnt\(1));

-- Location: LCCOMB_X68_Y71_N28
\snes_ctrl_inst|Selector4~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|Selector4~0_combout\ = (!\snes_ctrl_inst|clk_cnt\(1) & (\snes_ctrl_inst|Equal1~1_combout\ & (\snes_ctrl_inst|clk_cnt\(0) & \snes_ctrl_inst|state.CLK_LOW~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|clk_cnt\(1),
	datab => \snes_ctrl_inst|Equal1~1_combout\,
	datac => \snes_ctrl_inst|clk_cnt\(0),
	datad => \snes_ctrl_inst|state.CLK_LOW~q\,
	combout => \snes_ctrl_inst|Selector4~0_combout\);

-- Location: LCCOMB_X67_Y71_N8
\snes_ctrl_inst|Selector4~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|Selector4~4_combout\ = (\snes_ctrl_inst|Selector4~0_combout\ & (!\snes_ctrl_inst|state.SAMPLE~q\ & \snes_ctrl_inst|Selector4~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|Selector4~0_combout\,
	datac => \snes_ctrl_inst|state.SAMPLE~q\,
	datad => \snes_ctrl_inst|Selector4~3_combout\,
	combout => \snes_ctrl_inst|Selector4~4_combout\);

-- Location: FF_X67_Y71_N9
\snes_ctrl_inst|state.SAMPLE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|Selector4~4_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|state.SAMPLE~q\);

-- Location: LCCOMB_X65_Y71_N24
\snes_ctrl_inst|snes_clk~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|snes_clk~0_combout\ = (\snes_ctrl_inst|state.SAMPLE~q\) # (\snes_ctrl_inst|state.CLK_LOW~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \snes_ctrl_inst|state.SAMPLE~q\,
	datad => \snes_ctrl_inst|state.CLK_LOW~q\,
	combout => \snes_ctrl_inst|snes_clk~0_combout\);

-- Location: IOIBUF_X67_Y73_N15
\snes_data~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_snes_data,
	o => \snes_data~input_o\);

-- Location: LCCOMB_X67_Y71_N4
\snes_ctrl_inst|shift_reg[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|shift_reg[0]~0_combout\ = !\snes_data~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \snes_data~input_o\,
	combout => \snes_ctrl_inst|shift_reg[0]~0_combout\);

-- Location: FF_X67_Y71_N5
\snes_ctrl_inst|shift_reg[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|shift_reg[0]~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snes_ctrl_inst|state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|shift_reg\(0));

-- Location: LCCOMB_X66_Y71_N6
\snes_ctrl_inst|shift_reg[1]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|shift_reg[1]~feeder_combout\ = \snes_ctrl_inst|shift_reg\(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \snes_ctrl_inst|shift_reg\(0),
	combout => \snes_ctrl_inst|shift_reg[1]~feeder_combout\);

-- Location: FF_X66_Y71_N7
\snes_ctrl_inst|shift_reg[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|shift_reg[1]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snes_ctrl_inst|state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|shift_reg\(1));

-- Location: LCCOMB_X66_Y71_N20
\snes_ctrl_inst|shift_reg[2]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|shift_reg[2]~feeder_combout\ = \snes_ctrl_inst|shift_reg\(1)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \snes_ctrl_inst|shift_reg\(1),
	combout => \snes_ctrl_inst|shift_reg[2]~feeder_combout\);

-- Location: FF_X66_Y71_N21
\snes_ctrl_inst|shift_reg[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|shift_reg[2]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snes_ctrl_inst|state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|shift_reg\(2));

-- Location: LCCOMB_X66_Y71_N18
\snes_ctrl_inst|shift_reg[3]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|shift_reg[3]~feeder_combout\ = \snes_ctrl_inst|shift_reg\(2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \snes_ctrl_inst|shift_reg\(2),
	combout => \snes_ctrl_inst|shift_reg[3]~feeder_combout\);

-- Location: FF_X66_Y71_N19
\snes_ctrl_inst|shift_reg[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|shift_reg[3]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snes_ctrl_inst|state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|shift_reg\(3));

-- Location: LCCOMB_X66_Y71_N16
\snes_ctrl_inst|shift_reg[4]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|shift_reg[4]~feeder_combout\ = \snes_ctrl_inst|shift_reg\(3)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \snes_ctrl_inst|shift_reg\(3),
	combout => \snes_ctrl_inst|shift_reg[4]~feeder_combout\);

-- Location: FF_X66_Y71_N17
\snes_ctrl_inst|shift_reg[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|shift_reg[4]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snes_ctrl_inst|state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|shift_reg\(4));

-- Location: LCCOMB_X66_Y71_N22
\snes_ctrl_inst|shift_reg[5]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|shift_reg[5]~feeder_combout\ = \snes_ctrl_inst|shift_reg\(4)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \snes_ctrl_inst|shift_reg\(4),
	combout => \snes_ctrl_inst|shift_reg[5]~feeder_combout\);

-- Location: FF_X66_Y71_N23
\snes_ctrl_inst|shift_reg[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|shift_reg[5]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snes_ctrl_inst|state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|shift_reg\(5));

-- Location: FF_X66_Y71_N29
\snes_ctrl_inst|shift_reg[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snes_ctrl_inst|shift_reg\(5),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snes_ctrl_inst|state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|shift_reg\(6));

-- Location: LCCOMB_X66_Y71_N10
\snes_ctrl_inst|shift_reg[7]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|shift_reg[7]~feeder_combout\ = \snes_ctrl_inst|shift_reg\(6)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \snes_ctrl_inst|shift_reg\(6),
	combout => \snes_ctrl_inst|shift_reg[7]~feeder_combout\);

-- Location: FF_X66_Y71_N11
\snes_ctrl_inst|shift_reg[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|shift_reg[7]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snes_ctrl_inst|state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|shift_reg\(7));

-- Location: LCCOMB_X66_Y71_N0
\snes_ctrl_inst|shift_reg[8]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|shift_reg[8]~feeder_combout\ = \snes_ctrl_inst|shift_reg\(7)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \snes_ctrl_inst|shift_reg\(7),
	combout => \snes_ctrl_inst|shift_reg[8]~feeder_combout\);

-- Location: FF_X66_Y71_N1
\snes_ctrl_inst|shift_reg[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|shift_reg[8]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snes_ctrl_inst|state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|shift_reg\(8));

-- Location: LCCOMB_X66_Y71_N30
\snes_ctrl_inst|shift_reg[9]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|shift_reg[9]~feeder_combout\ = \snes_ctrl_inst|shift_reg\(8)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \snes_ctrl_inst|shift_reg\(8),
	combout => \snes_ctrl_inst|shift_reg[9]~feeder_combout\);

-- Location: FF_X66_Y71_N31
\snes_ctrl_inst|shift_reg[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|shift_reg[9]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snes_ctrl_inst|state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|shift_reg\(9));

-- Location: FF_X66_Y71_N13
\snes_ctrl_inst|shift_reg[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snes_ctrl_inst|shift_reg\(9),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snes_ctrl_inst|state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|shift_reg\(10));

-- Location: FF_X65_Y71_N23
\snes_ctrl_inst|shift_reg[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snes_ctrl_inst|shift_reg\(10),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snes_ctrl_inst|state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|shift_reg\(11));

-- Location: FF_X65_Y71_N29
\snes_ctrl_inst|shift_reg[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snes_ctrl_inst|shift_reg\(11),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snes_ctrl_inst|state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|shift_reg\(12));

-- Location: LCCOMB_X65_Y71_N10
\snes_ctrl_inst|shift_reg[13]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|shift_reg[13]~feeder_combout\ = \snes_ctrl_inst|shift_reg\(12)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \snes_ctrl_inst|shift_reg\(12),
	combout => \snes_ctrl_inst|shift_reg[13]~feeder_combout\);

-- Location: FF_X65_Y71_N11
\snes_ctrl_inst|shift_reg[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|shift_reg[13]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snes_ctrl_inst|state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|shift_reg\(13));

-- Location: LCCOMB_X65_Y71_N16
\snes_ctrl_inst|shift_reg[14]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|shift_reg[14]~feeder_combout\ = \snes_ctrl_inst|shift_reg\(13)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \snes_ctrl_inst|shift_reg\(13),
	combout => \snes_ctrl_inst|shift_reg[14]~feeder_combout\);

-- Location: FF_X65_Y71_N17
\snes_ctrl_inst|shift_reg[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|shift_reg[14]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snes_ctrl_inst|state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|shift_reg\(14));

-- Location: LCCOMB_X65_Y71_N6
\snes_ctrl_inst|shift_reg[15]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|shift_reg[15]~feeder_combout\ = \snes_ctrl_inst|shift_reg\(14)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \snes_ctrl_inst|shift_reg\(14),
	combout => \snes_ctrl_inst|shift_reg[15]~feeder_combout\);

-- Location: FF_X65_Y71_N7
\snes_ctrl_inst|shift_reg[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|shift_reg[15]~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snes_ctrl_inst|state.SAMPLE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|shift_reg\(15));

-- Location: LCCOMB_X65_Y71_N26
\snes_ctrl_inst|ctrl_state.btn_b~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|ctrl_state.btn_b~feeder_combout\ = \snes_ctrl_inst|shift_reg\(15)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \snes_ctrl_inst|shift_reg\(15),
	combout => \snes_ctrl_inst|ctrl_state.btn_b~feeder_combout\);

-- Location: FF_X65_Y71_N27
\snes_ctrl_inst|ctrl_state.btn_b\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|ctrl_state.btn_b~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snes_ctrl_inst|ALT_INV_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|ctrl_state.btn_b~q\);

-- Location: LCCOMB_X65_Y71_N20
\snes_ctrl_inst|ctrl_state.btn_y~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|ctrl_state.btn_y~feeder_combout\ = \snes_ctrl_inst|shift_reg\(14)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \snes_ctrl_inst|shift_reg\(14),
	combout => \snes_ctrl_inst|ctrl_state.btn_y~feeder_combout\);

-- Location: FF_X65_Y71_N21
\snes_ctrl_inst|ctrl_state.btn_y\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|ctrl_state.btn_y~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snes_ctrl_inst|ALT_INV_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|ctrl_state.btn_y~q\);

-- Location: LCCOMB_X65_Y71_N14
\snes_ctrl_inst|ctrl_state.btn_select~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|ctrl_state.btn_select~feeder_combout\ = \snes_ctrl_inst|shift_reg\(13)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \snes_ctrl_inst|shift_reg\(13),
	combout => \snes_ctrl_inst|ctrl_state.btn_select~feeder_combout\);

-- Location: FF_X65_Y71_N15
\snes_ctrl_inst|ctrl_state.btn_select\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|ctrl_state.btn_select~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snes_ctrl_inst|ALT_INV_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|ctrl_state.btn_select~q\);

-- Location: LCCOMB_X65_Y71_N8
\snes_ctrl_inst|ctrl_state.btn_start~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|ctrl_state.btn_start~feeder_combout\ = \snes_ctrl_inst|shift_reg\(12)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \snes_ctrl_inst|shift_reg\(12),
	combout => \snes_ctrl_inst|ctrl_state.btn_start~feeder_combout\);

-- Location: FF_X65_Y71_N9
\snes_ctrl_inst|ctrl_state.btn_start\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|ctrl_state.btn_start~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snes_ctrl_inst|ALT_INV_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|ctrl_state.btn_start~q\);

-- Location: FF_X65_Y71_N19
\snes_ctrl_inst|ctrl_state.btn_up\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snes_ctrl_inst|shift_reg\(11),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snes_ctrl_inst|ALT_INV_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|ctrl_state.btn_up~q\);

-- Location: FF_X65_Y71_N13
\snes_ctrl_inst|ctrl_state.btn_down\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snes_ctrl_inst|shift_reg\(10),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snes_ctrl_inst|ALT_INV_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|ctrl_state.btn_down~q\);

-- Location: FF_X66_Y71_N25
\snes_ctrl_inst|ctrl_state.btn_left\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snes_ctrl_inst|shift_reg\(9),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snes_ctrl_inst|ALT_INV_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|ctrl_state.btn_left~q\);

-- Location: LCCOMB_X66_Y71_N2
\snes_ctrl_inst|ctrl_state.btn_right~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|ctrl_state.btn_right~feeder_combout\ = \snes_ctrl_inst|shift_reg\(8)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \snes_ctrl_inst|shift_reg\(8),
	combout => \snes_ctrl_inst|ctrl_state.btn_right~feeder_combout\);

-- Location: FF_X66_Y71_N3
\snes_ctrl_inst|ctrl_state.btn_right\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|ctrl_state.btn_right~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snes_ctrl_inst|ALT_INV_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|ctrl_state.btn_right~q\);

-- Location: LCCOMB_X66_Y71_N4
\snes_ctrl_inst|ctrl_state.btn_a~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|ctrl_state.btn_a~feeder_combout\ = \snes_ctrl_inst|shift_reg\(7)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \snes_ctrl_inst|shift_reg\(7),
	combout => \snes_ctrl_inst|ctrl_state.btn_a~feeder_combout\);

-- Location: FF_X66_Y71_N5
\snes_ctrl_inst|ctrl_state.btn_a\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|ctrl_state.btn_a~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snes_ctrl_inst|ALT_INV_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|ctrl_state.btn_a~q\);

-- Location: LCCOMB_X66_Y71_N14
\snes_ctrl_inst|ctrl_state.btn_x~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|ctrl_state.btn_x~feeder_combout\ = \snes_ctrl_inst|shift_reg\(6)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \snes_ctrl_inst|shift_reg\(6),
	combout => \snes_ctrl_inst|ctrl_state.btn_x~feeder_combout\);

-- Location: FF_X66_Y71_N15
\snes_ctrl_inst|ctrl_state.btn_x\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|ctrl_state.btn_x~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snes_ctrl_inst|ALT_INV_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|ctrl_state.btn_x~q\);

-- Location: FF_X66_Y71_N9
\snes_ctrl_inst|ctrl_state.btn_l\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \snes_ctrl_inst|shift_reg\(5),
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \snes_ctrl_inst|ALT_INV_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|ctrl_state.btn_l~q\);

-- Location: LCCOMB_X66_Y71_N26
\snes_ctrl_inst|ctrl_state.btn_r~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \snes_ctrl_inst|ctrl_state.btn_r~feeder_combout\ = \snes_ctrl_inst|shift_reg\(4)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \snes_ctrl_inst|shift_reg\(4),
	combout => \snes_ctrl_inst|ctrl_state.btn_r~feeder_combout\);

-- Location: FF_X66_Y71_N27
\snes_ctrl_inst|ctrl_state.btn_r\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \snes_ctrl_inst|ctrl_state.btn_r~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \snes_ctrl_inst|ALT_INV_state.WAIT_TIMEOUT~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \snes_ctrl_inst|ctrl_state.btn_r~q\);

ww_snes_clk <= \snes_clk~output_o\;

ww_snes_latch <= \snes_latch~output_o\;

ww_ctrl_state(0) <= \ctrl_state[0]~output_o\;

ww_ctrl_state(1) <= \ctrl_state[1]~output_o\;

ww_ctrl_state(2) <= \ctrl_state[2]~output_o\;

ww_ctrl_state(3) <= \ctrl_state[3]~output_o\;

ww_ctrl_state(4) <= \ctrl_state[4]~output_o\;

ww_ctrl_state(5) <= \ctrl_state[5]~output_o\;

ww_ctrl_state(6) <= \ctrl_state[6]~output_o\;

ww_ctrl_state(7) <= \ctrl_state[7]~output_o\;

ww_ctrl_state(8) <= \ctrl_state[8]~output_o\;

ww_ctrl_state(9) <= \ctrl_state[9]~output_o\;

ww_ctrl_state(10) <= \ctrl_state[10]~output_o\;

ww_ctrl_state(11) <= \ctrl_state[11]~output_o\;
END structure;


