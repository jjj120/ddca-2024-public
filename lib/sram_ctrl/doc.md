
# SRAM Controller

This module provides an interface controller to the SRAM chip (IS61WV102416BLL) on the DE2-115 board.
This chip is a 2 MB, word-addressable, asynchronous, single-port, static RAM, which means
 * there are 2^20 memory locations (i.e., addresses), where each stores 16 bits of data
 * its interface does not include a clock signal
 * reading and writing simultaneously is not possible
The controller abstracts away these hardware details and provides three separate, byte-addressable, (more or less) independent interfaces to this memory (two read ports and one write port).

## Dependencies
* None

## Required Files
 * `sram_ctrl_a21d16b8_top.qarlog`
 * `sram_ctrl_pkg.vhd`
 * `precompiled_sram_ctrl_a21d16b8.vhd`
 * `sram_ctrl_a21d16b8_top.vho` (use **only for simulation**, not for synthesis!)
 * `sram_ctrl_a21d16b8_top.qxp` (use **only for synthesis** (i.e., in quartus), not for simulation!)

## Precompiled SRAM Controller  Component Declaration

```vhdl
component precompiled_sram_ctrl_a21d16b8 is
	port (
		clk : in std_logic;
		res_n : in std_logic;

		-- write port (buffered)
		wr_addr : in std_logic_vector(20 downto 0);
		wr_data : in std_logic_vector(15 downto 0);
		wr : in std_logic;
		wr_access_mode : in sram_access_mode_t;
		wr_empty : out std_logic;
		wr_full : out std_logic;
		wr_half_full : out std_logic;

		-- read port 1 (high priority)
		rd1_addr : in std_logic_vector(20 downto 0);
		rd1 : in std_logic;
		rd1_access_mode : in sram_access_mode_t;
		rd1_busy : out std_logic;
		rd1_data : out std_logic_vector(15 downto 0);
		rd1_valid : out std_logic;

		-- read port 2 (low priority)
		rd2_addr : in std_logic_vector(20 downto 0);
		rd2 : in std_logic;
		rd2_access_mode : in sram_access_mode_t;
		rd2_busy : out std_logic;
		rd2_data : out std_logic_vector(15 downto 0);
		rd2_valid : out std_logic;

		-- external interface to SRAM
		sram_dq : inout std_logic_vector(15 downto 0);
		sram_addr : out std_logic_vector(19 downto 0);
		sram_ub_n : out std_logic;
		sram_lb_n : out std_logic;
		sram_we_n : out std_logic;
		sram_ce_n : out std_logic;
		sram_oe_n : out std_logic
	);
end component;
```

## Precompiled SRAM Controller  Interface Description


As mentioned before the controller features two read ports and one write port, that can be operated independent from each other.

The `access_mode_t` type is an enum with the values `BYTE` and `WORD`, for 8 bit and 16 bit memory operations, respectively.
`WORD` access is only allowed for even addresses (i.e., unaligned access is not supported, the behavior in this case is undefined)!

### Write Port

The (single) write port consists of the `wr*` signals.
These signals essentially connect to the write port of an internal FIFO, where the write operations are buffered.
The signals `wr_empty` and `wr[_half]_full` are the status flags of this FIFO.
For a `BYTE` access the upper 8-bits of `wr_data` (i.e., `wr_data(15 downto 8)`) are ignored.

The actual write operations to the SRAM are scheduled by controller if there are no pending read operations.
In these case it asserts the outputs `rd1_busy` and `rd2_busy` to prevent an incoming read operation to interrupt writing to the SRAM.

### Read Ports

The two read ports are operated with the `rd1*` and `rd2*` signals.
The timing diagram below shows an example for how these ports are interfaced.

![SRAM Controller read timing](.mdata/read_timing.svg)

Whenever, the `rdX_busy` is low, a read operation may be performed by asserting `rdX` for one clock cycle.
In this cycle the desired address and access mode must be applied to `rdX_addr` and `rdX_access_mode`, respectively.
If `rdX` is low the values at `rdX_addr` and `rdX_access_mode` don't matter.

After some time (2 clock cycles in the figure), which depends on the priority of the read port and the activity on the other read port, the controller provides the result of the read operation at the output `rdX_data`.
To indicate that the data is valid the signal `rdX_valid` is used.
This means that for each read operation (initiated by asserting `rdX` for one cycle), `rdX_valid` is asserted for exactly one clock cycle and the associated data is provided at `rdX_data`.
Whenever `rdX_valid` is low, `rdX_data` is set to the all-zero vector.
For a `BYTE` access the top byte in `rdX_data` (i.e., `rd_data(15 downto 0)`) is zero.


The `rdX_busy` signals are derived **only** from the internal state of the controller and **not** combinationally from any of its inputs!
This means that it can happen, that both `rdX_busy` signals are low and read operations are performed on both read ports in the same clock cycle (i.e., `rd1=rd2=1`).
In such a case the read operation on the `rd2` port is buffered internally, and only executed when there are no more pending read operations on `rd1`.
To prevent new read operations on `rd2` during this time, `rd2_busy` is asserted.
The timing diagram below illustrates this behavior.
The labels A, B, C, X, Y indicate the individual read operations and their associated responses.

![SRAM Controller read timing for simultaneous read operations](.mdata/simultaneous_read.svg)

Consequently, the read ports have different priority.
While read operation on `rd1` are always executed immediately, `rd2` read operations can be stalled for an arbitrary amount of time.
The read access time at the `rd1` port (i.e., the time between assertion of `rd1` and `rd1_valid`) is always exactly 2 cycles.
For the `rd2` port this value can also be guaranteed as a lower bound, as read operations might be delayed for an arbitrary amount of time by continuous read operation on the `rd1` port.

