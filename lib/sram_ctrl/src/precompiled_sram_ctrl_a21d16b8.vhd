
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.math_pkg.all;
use work.mem_pkg.all;
use work.sram_ctrl_pkg.all;


entity precompiled_sram_ctrl_a21d16b8 is
	port (
		clk : in  std_logic;
		res_n : in std_logic;

		-- write port (buffered)
		wr_addr : in std_logic_vector(21-1 downto 0);
		wr_data : in std_logic_vector(16-1 downto 0);
		wr      : in std_logic;
		wr_access_mode : in sram_access_mode_t;
		wr_empty : out std_logic;
		wr_full : out std_logic;
		wr_half_full : out std_logic;
	
		-- read port 1 (high priority)
		rd1_addr  : in std_logic_vector(21-1 downto 0);
		rd1       : in std_logic;
		rd1_access_mode : in sram_access_mode_t;
		rd1_busy  : out std_logic;
		rd1_data  : out std_logic_vector(16-1 downto 0);
		rd1_valid : out std_logic;
		
		-- read port 2 (low priority)
		rd2_addr  : in std_logic_vector(21-1 downto 0);
		rd2       : in std_logic;
		rd2_access_mode : in sram_access_mode_t;
		rd2_busy  : out std_logic;
		rd2_data  : out std_logic_vector(16-1 downto 0);
		rd2_valid : out std_logic;
		
		-- external interface to SRAM
		sram_dq : inout std_logic_vector(16-1 downto 0);
		sram_addr : out std_logic_vector(21-2 downto 0);
		sram_ub_n : out std_logic;
		sram_lb_n : out std_logic;
		sram_we_n : out std_logic;
		sram_ce_n : out std_logic;
		sram_oe_n : out std_logic
	);
end entity;


architecture arch of precompiled_sram_ctrl_a21d16b8 is
	component sram_ctrl_a21d16b8_top is
		port (
			clk : in  std_logic;
			res_n : in std_logic;

			-- write port (buffered)
			wr_addr : in std_logic_vector(21-1 downto 0);
			wr_data : in std_logic_vector(16-1 downto 0);
			wr      : in std_logic;
			wr_access_mode : in std_logic;
			wr_empty : out std_logic;
			wr_full : out std_logic;
			wr_half_full : out std_logic;
		
			-- read port 1 (high priority)
			rd1_addr  : in std_logic_vector(21-1 downto 0);
			rd1       : in std_logic;
			rd1_access_mode : in std_logic;
			rd1_busy  : out std_logic;
			rd1_data  : out std_logic_vector(16-1 downto 0);
			rd1_valid : out std_logic;
			
			-- read port 2 (low priority)
			rd2_addr  : in std_logic_vector(21-1 downto 0);
			rd2       : in std_logic;
			rd2_access_mode : in std_logic;
			rd2_busy  : out std_logic;
			rd2_data  : out std_logic_vector(16-1 downto 0);
			rd2_valid : out std_logic;
			
			-- external interface to SRAM
			sram_dq : inout std_logic_vector(16-1 downto 0);
			sram_addr : out std_logic_vector(21-2 downto 0);
			sram_ub_n : out std_logic;
			sram_lb_n : out std_logic;
			sram_we_n : out std_logic;
			sram_ce_n : out std_logic;
			sram_oe_n : out std_logic
		);
	end component;
begin
	sram_ctrl_a21d16b8_inst: sram_ctrl_a21d16b8_top
	port map (
		clk => clk,
		res_n => res_n,

		-- write port (buffered)
		wr_addr => wr_addr,
		wr_data => wr_data,
		wr      => wr,
		wr_access_mode => to_std_logic(wr_access_mode),
		wr_empty => wr_empty,
		wr_full => wr_full,
		wr_half_full => wr_half_full,
	
		-- read port 1 (high priority)
		rd1_addr  => rd1_addr,
		rd1       => rd1,
		rd1_access_mode => to_std_logic(rd1_access_mode),
		rd1_busy  => rd1_busy,
		rd1_data  => rd1_data,
		rd1_valid => rd1_valid,
		
		-- read port 2 (low priority)
		rd2_addr  => rd2_addr,
		rd2       => rd2,
		rd2_access_mode => to_std_logic(rd2_access_mode),
		rd2_busy  => rd2_busy,
		rd2_data  => rd2_data,
		rd2_valid => rd2_valid,
		
		-- external interface to SRAM
		sram_dq => sram_dq,
		sram_addr => sram_addr,
		sram_ub_n => sram_ub_n,
		sram_lb_n => sram_lb_n,
		sram_we_n => sram_we_n,
		sram_ce_n => sram_ce_n,
		sram_oe_n => sram_oe_n
	);
end architecture;

