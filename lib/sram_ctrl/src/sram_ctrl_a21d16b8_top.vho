-- Copyright (C) 2023  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 22.1std.2 Build 922 07/20/2023 SC Standard Edition"

-- DATE "04/12/2024 10:12:46"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim (VHDL) only
-- 

LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_ASDO_DATA1~	=>  Location: PIN_F4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_FLASH_nCE_nCSO~	=>  Location: PIN_E2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DCLK~	=>  Location: PIN_P3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DATA0~	=>  Location: PIN_N7,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~padout\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~padout\ : std_logic;
SIGNAL \~ALTERA_DATA0~~padout\ : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_DATA0~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	sram_ctrl_a21d16b8_top IS
    PORT (
	clk : IN std_logic;
	res_n : IN std_logic;
	wr_addr : IN std_logic_vector(20 DOWNTO 0);
	wr_data : IN std_logic_vector(15 DOWNTO 0);
	wr : IN std_logic;
	wr_access_mode : IN std_logic;
	wr_empty : OUT std_logic;
	wr_full : OUT std_logic;
	wr_half_full : OUT std_logic;
	rd1_addr : IN std_logic_vector(20 DOWNTO 0);
	rd1 : IN std_logic;
	rd1_access_mode : IN std_logic;
	rd1_busy : OUT std_logic;
	rd1_data : OUT std_logic_vector(15 DOWNTO 0);
	rd1_valid : OUT std_logic;
	rd2_addr : IN std_logic_vector(20 DOWNTO 0);
	rd2 : IN std_logic;
	rd2_access_mode : IN std_logic;
	rd2_busy : OUT std_logic;
	rd2_data : OUT std_logic_vector(15 DOWNTO 0);
	rd2_valid : OUT std_logic;
	sram_dq : INOUT std_logic_vector(15 DOWNTO 0);
	sram_addr : OUT std_logic_vector(19 DOWNTO 0);
	sram_ub_n : OUT std_logic;
	sram_lb_n : OUT std_logic;
	sram_we_n : OUT std_logic;
	sram_ce_n : OUT std_logic;
	sram_oe_n : OUT std_logic
	);
END sram_ctrl_a21d16b8_top;

-- Design Ports Information
-- wr_empty	=>  Location: PIN_G23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_full	=>  Location: PIN_H23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_half_full	=>  Location: PIN_L23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_busy	=>  Location: PIN_D19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_data[0]	=>  Location: PIN_AD12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_data[1]	=>  Location: PIN_A8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_data[2]	=>  Location: PIN_J10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_data[3]	=>  Location: PIN_G10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_data[4]	=>  Location: PIN_A12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_data[5]	=>  Location: PIN_B8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_data[6]	=>  Location: PIN_H10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_data[7]	=>  Location: PIN_C8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_data[8]	=>  Location: PIN_E12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_data[9]	=>  Location: PIN_H12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_data[10]	=>  Location: PIN_B6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_data[11]	=>  Location: PIN_D8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_data[12]	=>  Location: PIN_A6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_data[13]	=>  Location: PIN_G11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_data[14]	=>  Location: PIN_C7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_data[15]	=>  Location: PIN_G12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_valid	=>  Location: PIN_H22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_busy	=>  Location: PIN_G19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_data[0]	=>  Location: PIN_F10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_data[1]	=>  Location: PIN_B11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_data[2]	=>  Location: PIN_A11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_data[3]	=>  Location: PIN_J13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_data[4]	=>  Location: PIN_F12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_data[5]	=>  Location: PIN_G14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_data[6]	=>  Location: PIN_F14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_data[7]	=>  Location: PIN_J12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_data[8]	=>  Location: PIN_C9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_data[9]	=>  Location: PIN_F11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_data[10]	=>  Location: PIN_D11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_data[11]	=>  Location: PIN_A7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_data[12]	=>  Location: PIN_D9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_data[13]	=>  Location: PIN_B7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_data[14]	=>  Location: PIN_E11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_data[15]	=>  Location: PIN_C11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_valid	=>  Location: PIN_H17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_addr[0]	=>  Location: PIN_A19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_addr[1]	=>  Location: PIN_C22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_addr[2]	=>  Location: PIN_F21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_addr[3]	=>  Location: PIN_E22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_addr[4]	=>  Location: PIN_G24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_addr[5]	=>  Location: PIN_F17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_addr[6]	=>  Location: PIN_H16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_addr[7]	=>  Location: PIN_J17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_addr[8]	=>  Location: PIN_G25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_addr[9]	=>  Location: PIN_J19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_addr[10]	=>  Location: PIN_F22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_addr[11]	=>  Location: PIN_F24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_addr[12]	=>  Location: PIN_G26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_addr[13]	=>  Location: PIN_F3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_addr[14]	=>  Location: PIN_J16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_addr[15]	=>  Location: PIN_J15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_addr[16]	=>  Location: PIN_B17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_addr[17]	=>  Location: PIN_B25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_addr[18]	=>  Location: PIN_D21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_addr[19]	=>  Location: PIN_H21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_ub_n	=>  Location: PIN_G17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_lb_n	=>  Location: PIN_A25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_we_n	=>  Location: PIN_H24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_ce_n	=>  Location: PIN_F19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_oe_n	=>  Location: PIN_C23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_dq[0]	=>  Location: PIN_F15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_dq[1]	=>  Location: PIN_H14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_dq[2]	=>  Location: PIN_J14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_dq[3]	=>  Location: PIN_D12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_dq[4]	=>  Location: PIN_D13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_dq[5]	=>  Location: PIN_C12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_dq[6]	=>  Location: PIN_E14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_dq[7]	=>  Location: PIN_D14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_dq[8]	=>  Location: PIN_C10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_dq[9]	=>  Location: PIN_D15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_dq[10]	=>  Location: PIN_B10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_dq[11]	=>  Location: PIN_C15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_dq[12]	=>  Location: PIN_G13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_dq[13]	=>  Location: PIN_H13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_dq[14]	=>  Location: PIN_A10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sram_dq[15]	=>  Location: PIN_D10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clk	=>  Location: PIN_J1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- res_n	=>  Location: PIN_Y2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr	=>  Location: PIN_Y1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1	=>  Location: PIN_C13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2	=>  Location: PIN_G15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_addr[1]	=>  Location: PIN_A26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_addr[1]	=>  Location: PIN_C26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_addr[2]	=>  Location: PIN_B26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_addr[2]	=>  Location: PIN_D20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_addr[3]	=>  Location: PIN_D22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_addr[3]	=>  Location: PIN_K28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_addr[4]	=>  Location: PIN_D25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_addr[4]	=>  Location: PIN_B18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_addr[5]	=>  Location: PIN_D17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_addr[5]	=>  Location: PIN_E21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_addr[6]	=>  Location: PIN_E15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_addr[6]	=>  Location: PIN_D16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_addr[7]	=>  Location: PIN_J24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_addr[7]	=>  Location: PIN_L24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_addr[8]	=>  Location: PIN_C14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_addr[8]	=>  Location: PIN_AH21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_addr[9]	=>  Location: PIN_F25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_addr[9]	=>  Location: PIN_D24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_addr[10]	=>  Location: PIN_AD17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_addr[10]	=>  Location: PIN_J23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_addr[11]	=>  Location: PIN_A23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_addr[11]	=>  Location: PIN_J22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_addr[12]	=>  Location: PIN_K26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_addr[12]	=>  Location: PIN_M27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_addr[13]	=>  Location: PIN_L22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_addr[13]	=>  Location: PIN_K25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_addr[14]	=>  Location: PIN_C20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_addr[14]	=>  Location: PIN_C25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_addr[15]	=>  Location: PIN_M26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_addr[15]	=>  Location: PIN_G18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_addr[16]	=>  Location: PIN_E10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_addr[16]	=>  Location: PIN_E19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_addr[17]	=>  Location: PIN_J25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_addr[17]	=>  Location: PIN_K27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_addr[18]	=>  Location: PIN_A18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_addr[18]	=>  Location: PIN_D23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_addr[19]	=>  Location: PIN_D26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_addr[19]	=>  Location: PIN_L25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_addr[20]	=>  Location: PIN_R21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_addr[20]	=>  Location: PIN_B23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_addr[0]	=>  Location: PIN_H15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd1_access_mode	=>  Location: PIN_C16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_addr[0]	=>  Location: PIN_E17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rd2_access_mode	=>  Location: PIN_G16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_addr[1]	=>  Location: PIN_C24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_addr[2]	=>  Location: PIN_A17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_addr[3]	=>  Location: PIN_G21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_addr[4]	=>  Location: PIN_A21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_addr[5]	=>  Location: PIN_B22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_addr[6]	=>  Location: PIN_F26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_addr[7]	=>  Location: PIN_K21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_addr[8]	=>  Location: PIN_C27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_addr[9]	=>  Location: PIN_H19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_addr[10]	=>  Location: PIN_G22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_addr[11]	=>  Location: PIN_D27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_addr[12]	=>  Location: PIN_F18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_addr[13]	=>  Location: PIN_E26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_addr[14]	=>  Location: PIN_E18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_addr[15]	=>  Location: PIN_J26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_addr[16]	=>  Location: PIN_G20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_addr[17]	=>  Location: PIN_B19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_addr[18]	=>  Location: PIN_E27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_addr[19]	=>  Location: PIN_G27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_addr[20]	=>  Location: PIN_M21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_addr[0]	=>  Location: PIN_C21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_access_mode	=>  Location: PIN_H26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_data[0]	=>  Location: PIN_F28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_data[1]	=>  Location: PIN_F27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_data[2]	=>  Location: PIN_A22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_data[3]	=>  Location: PIN_B21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_data[4]	=>  Location: PIN_H25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_data[5]	=>  Location: PIN_E24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_data[6]	=>  Location: PIN_C17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_data[7]	=>  Location: PIN_D28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_data[8]	=>  Location: PIN_C19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_data[9]	=>  Location: PIN_K22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_data[10]	=>  Location: PIN_G28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_data[11]	=>  Location: PIN_E25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_data[12]	=>  Location: PIN_L21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_data[13]	=>  Location: PIN_D18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_data[14]	=>  Location: PIN_C18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wr_data[15]	=>  Location: PIN_E28,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF sram_ctrl_a21d16b8_top IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_res_n : std_logic;
SIGNAL ww_wr_addr : std_logic_vector(20 DOWNTO 0);
SIGNAL ww_wr_data : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_wr : std_logic;
SIGNAL ww_wr_access_mode : std_logic;
SIGNAL ww_wr_empty : std_logic;
SIGNAL ww_wr_full : std_logic;
SIGNAL ww_wr_half_full : std_logic;
SIGNAL ww_rd1_addr : std_logic_vector(20 DOWNTO 0);
SIGNAL ww_rd1 : std_logic;
SIGNAL ww_rd1_access_mode : std_logic;
SIGNAL ww_rd1_busy : std_logic;
SIGNAL ww_rd1_data : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_rd1_valid : std_logic;
SIGNAL ww_rd2_addr : std_logic_vector(20 DOWNTO 0);
SIGNAL ww_rd2 : std_logic;
SIGNAL ww_rd2_access_mode : std_logic;
SIGNAL ww_rd2_busy : std_logic;
SIGNAL ww_rd2_data : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_rd2_valid : std_logic;
SIGNAL ww_sram_addr : std_logic_vector(19 DOWNTO 0);
SIGNAL ww_sram_ub_n : std_logic;
SIGNAL ww_sram_lb_n : std_logic;
SIGNAL ww_sram_we_n : std_logic;
SIGNAL ww_sram_ce_n : std_logic;
SIGNAL ww_sram_oe_n : std_logic;
SIGNAL \res_n~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \clk~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \sram_dq[0]~output_o\ : std_logic;
SIGNAL \sram_dq[1]~output_o\ : std_logic;
SIGNAL \sram_dq[2]~output_o\ : std_logic;
SIGNAL \sram_dq[3]~output_o\ : std_logic;
SIGNAL \sram_dq[4]~output_o\ : std_logic;
SIGNAL \sram_dq[5]~output_o\ : std_logic;
SIGNAL \sram_dq[6]~output_o\ : std_logic;
SIGNAL \sram_dq[7]~output_o\ : std_logic;
SIGNAL \sram_dq[8]~output_o\ : std_logic;
SIGNAL \sram_dq[9]~output_o\ : std_logic;
SIGNAL \sram_dq[10]~output_o\ : std_logic;
SIGNAL \sram_dq[11]~output_o\ : std_logic;
SIGNAL \sram_dq[12]~output_o\ : std_logic;
SIGNAL \sram_dq[13]~output_o\ : std_logic;
SIGNAL \sram_dq[14]~output_o\ : std_logic;
SIGNAL \sram_dq[15]~output_o\ : std_logic;
SIGNAL \wr_empty~output_o\ : std_logic;
SIGNAL \wr_full~output_o\ : std_logic;
SIGNAL \wr_half_full~output_o\ : std_logic;
SIGNAL \rd1_busy~output_o\ : std_logic;
SIGNAL \rd1_data[0]~output_o\ : std_logic;
SIGNAL \rd1_data[1]~output_o\ : std_logic;
SIGNAL \rd1_data[2]~output_o\ : std_logic;
SIGNAL \rd1_data[3]~output_o\ : std_logic;
SIGNAL \rd1_data[4]~output_o\ : std_logic;
SIGNAL \rd1_data[5]~output_o\ : std_logic;
SIGNAL \rd1_data[6]~output_o\ : std_logic;
SIGNAL \rd1_data[7]~output_o\ : std_logic;
SIGNAL \rd1_data[8]~output_o\ : std_logic;
SIGNAL \rd1_data[9]~output_o\ : std_logic;
SIGNAL \rd1_data[10]~output_o\ : std_logic;
SIGNAL \rd1_data[11]~output_o\ : std_logic;
SIGNAL \rd1_data[12]~output_o\ : std_logic;
SIGNAL \rd1_data[13]~output_o\ : std_logic;
SIGNAL \rd1_data[14]~output_o\ : std_logic;
SIGNAL \rd1_data[15]~output_o\ : std_logic;
SIGNAL \rd1_valid~output_o\ : std_logic;
SIGNAL \rd2_busy~output_o\ : std_logic;
SIGNAL \rd2_data[0]~output_o\ : std_logic;
SIGNAL \rd2_data[1]~output_o\ : std_logic;
SIGNAL \rd2_data[2]~output_o\ : std_logic;
SIGNAL \rd2_data[3]~output_o\ : std_logic;
SIGNAL \rd2_data[4]~output_o\ : std_logic;
SIGNAL \rd2_data[5]~output_o\ : std_logic;
SIGNAL \rd2_data[6]~output_o\ : std_logic;
SIGNAL \rd2_data[7]~output_o\ : std_logic;
SIGNAL \rd2_data[8]~output_o\ : std_logic;
SIGNAL \rd2_data[9]~output_o\ : std_logic;
SIGNAL \rd2_data[10]~output_o\ : std_logic;
SIGNAL \rd2_data[11]~output_o\ : std_logic;
SIGNAL \rd2_data[12]~output_o\ : std_logic;
SIGNAL \rd2_data[13]~output_o\ : std_logic;
SIGNAL \rd2_data[14]~output_o\ : std_logic;
SIGNAL \rd2_data[15]~output_o\ : std_logic;
SIGNAL \rd2_valid~output_o\ : std_logic;
SIGNAL \sram_addr[0]~output_o\ : std_logic;
SIGNAL \sram_addr[1]~output_o\ : std_logic;
SIGNAL \sram_addr[2]~output_o\ : std_logic;
SIGNAL \sram_addr[3]~output_o\ : std_logic;
SIGNAL \sram_addr[4]~output_o\ : std_logic;
SIGNAL \sram_addr[5]~output_o\ : std_logic;
SIGNAL \sram_addr[6]~output_o\ : std_logic;
SIGNAL \sram_addr[7]~output_o\ : std_logic;
SIGNAL \sram_addr[8]~output_o\ : std_logic;
SIGNAL \sram_addr[9]~output_o\ : std_logic;
SIGNAL \sram_addr[10]~output_o\ : std_logic;
SIGNAL \sram_addr[11]~output_o\ : std_logic;
SIGNAL \sram_addr[12]~output_o\ : std_logic;
SIGNAL \sram_addr[13]~output_o\ : std_logic;
SIGNAL \sram_addr[14]~output_o\ : std_logic;
SIGNAL \sram_addr[15]~output_o\ : std_logic;
SIGNAL \sram_addr[16]~output_o\ : std_logic;
SIGNAL \sram_addr[17]~output_o\ : std_logic;
SIGNAL \sram_addr[18]~output_o\ : std_logic;
SIGNAL \sram_addr[19]~output_o\ : std_logic;
SIGNAL \sram_ub_n~output_o\ : std_logic;
SIGNAL \sram_lb_n~output_o\ : std_logic;
SIGNAL \sram_we_n~output_o\ : std_logic;
SIGNAL \sram_ce_n~output_o\ : std_logic;
SIGNAL \sram_oe_n~output_o\ : std_logic;
SIGNAL \clk~input_o\ : std_logic;
SIGNAL \clk~inputclkctrl_outclk\ : std_logic;
SIGNAL \wr_access_mode~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address[0]~0_combout\ : std_logic;
SIGNAL \res_n~input_o\ : std_logic;
SIGNAL \res_n~inputclkctrl_outclk\ : std_logic;
SIGNAL \wr~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address[0]~0_combout\ : std_logic;
SIGNAL \rd1~input_o\ : std_logic;
SIGNAL \rd2~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_pending~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_pending~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add2~1_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add2~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add3~1_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add3~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty_next~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty_next~1_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty_next~2_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|Selector2~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|state.IDLE~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|state_nxt.WRITE1~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|state.WRITE1~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|state.WRITE2~feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|state.WRITE2~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|full_next~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|full_next~1_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|full_next~2_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|full~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|wr_int~combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~193_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~117_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~79_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~155_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~453_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~454_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~269feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~269_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~231_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~451_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~307_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~345_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~452_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~455_combout\ : std_logic;
SIGNAL \wr_data[0]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~253feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~253_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~215_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~464_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~291_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~329_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~465_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~177_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~101_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~63_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~139_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~466_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~467_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~468_combout\ : std_logic;
SIGNAL \wr_addr[0]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~232feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~232_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~194_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~446_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~270_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~308_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~447_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~156_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~80_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~42_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~118_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~448_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~449_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~450_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[0]~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|Selector2~1_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_drive_data~q\ : std_logic;
SIGNAL \wr_data[1]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~254_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~216_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~469_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~292_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~330_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~470_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~64_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~140_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~471_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~102_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~178_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~472_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~473_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[1]~1_combout\ : std_logic;
SIGNAL \wr_data[2]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~255_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~217_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~474_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~293feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~293_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~331_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~475_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~179feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~179_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~103_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~65_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~141_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~476_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~477_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~478_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[2]~2_combout\ : std_logic;
SIGNAL \wr_data[3]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~180_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~66_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~142_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~481_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~104_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~482_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~256_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~218_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~479_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~294_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~332_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~480_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~483_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[3]~3_combout\ : std_logic;
SIGNAL \wr_data[4]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~295_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~257_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~219_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~484_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~333_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~485_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~181_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~105_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~67_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~143_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~486_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~487_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~488_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[4]~4_combout\ : std_logic;
SIGNAL \wr_data[5]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~258_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~220_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~489_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~296feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~296_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~334_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~490_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~182_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~68_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~144_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~491_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~106_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~492_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~493_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[5]~5_combout\ : std_logic;
SIGNAL \wr_data[6]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~259_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~221_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~494_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~297_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~335_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~495_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~183feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~183_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~107_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~69_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~145_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~496_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~497_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~498_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[6]~6_combout\ : std_logic;
SIGNAL \wr_data[7]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~260_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~222_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~499_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~298_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~336_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~500_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~184feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~184_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~70_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~146_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~501_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~108_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~502_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~503_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[7]~7_combout\ : std_logic;
SIGNAL \wr_data[8]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~299feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~299_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~261_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~223_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~504_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~337_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~505_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~185feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~185_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~71_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~147_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~506_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~109_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~507_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~508_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[8]~8_combout\ : std_logic;
SIGNAL \wr_data[9]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~300_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~262_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~224_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~509_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~338_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~510_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~186_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~72_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~148_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~511_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~110_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~512_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~513_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[9]~9_combout\ : std_logic;
SIGNAL \wr_data[10]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~187_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~111_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~73_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~149feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~149_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~516_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~517_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~301_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~339_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~263_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~225_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~514_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~515_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~518_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[10]~10_combout\ : std_logic;
SIGNAL \wr_data[11]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~264_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~226_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~519_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~302feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~302_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~340_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~520_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~74_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~150_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~521_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~112_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~188_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~522_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~523_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[11]~11_combout\ : std_logic;
SIGNAL \wr_data[12]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~265_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~227_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~524_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~341_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~303_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~525_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~189_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~113_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~75_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~151_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~526_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~527_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~528_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[12]~12_combout\ : std_logic;
SIGNAL \wr_data[13]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~304feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~304_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~266_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~228_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~529_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~342_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~530_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~190_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~76_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~152_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~531_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~114_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~532_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~533_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[13]~13_combout\ : std_logic;
SIGNAL \wr_data[14]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~305_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~343_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~267_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~229_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~534_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~535_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~77_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~153feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~153_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~536_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~191feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~191_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~115_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~537_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~538_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[14]~14_combout\ : std_logic;
SIGNAL \wr_data[15]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~268_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~230_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~539_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~306_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~344_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~540_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~192feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~192_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~116_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~78_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~154_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~541_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~542_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~543_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[15]~15_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~1\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~2_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~3\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~4_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[2]~12_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~5\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~6_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[3]~11_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~7\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~8_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[4]~13_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~9\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~10_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[5]~14_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~11\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~12_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[6]~15_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~13\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~14_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[7]~16_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~15\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~16_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[8]~17_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~17\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~18_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[9]~10_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~19\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~20_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[10]~8_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~21\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~22_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[11]~9_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~23\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~24_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[12]~6_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~25\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~26_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[13]~7_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~27\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~28_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[14]~5_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~29\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~30_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[15]~2_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~31\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~32_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[16]~3_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~33\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~34_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[17]~4_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~35\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~36_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[18]~20_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~37\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~38_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[19]~21_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~39\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~40_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[20]~22_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~41\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~42_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[21]~23_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~43\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~44_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[22]~24_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~45\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~46_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[23]~25_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~47\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~48_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[24]~18_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~9_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~15_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~49\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~50_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[25]~19_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~10_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~4_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~5_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~6_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~7_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~8_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~51\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~52_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[26]~30_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~53\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~54_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[27]~29_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~55\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~56_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[28]~27_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~57\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~58_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[29]~28_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~59\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~60_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[30]~26_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~11_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~12_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~13_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~61\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~62_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[31]~1_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~14_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|half_full~q\ : std_logic;
SIGNAL \rd1_addr[0]~input_o\ : std_logic;
SIGNAL \rd1_access_mode~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd1_output_mode~10_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd1_output_mode.LOW_BYTE~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd1_output_mode~9_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd1_output_mode.WORD~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd1_data~2_combout\ : std_logic;
SIGNAL \sram_dq[8]~input_o\ : std_logic;
SIGNAL \sram_dq[0]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd1_output_mode~8_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd1_output_mode.HIGH_BYTE~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|Selector11~0_combout\ : std_logic;
SIGNAL \sram_dq[9]~input_o\ : std_logic;
SIGNAL \sram_dq[1]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|Selector10~0_combout\ : std_logic;
SIGNAL \sram_dq[10]~input_o\ : std_logic;
SIGNAL \sram_dq[2]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|Selector9~0_combout\ : std_logic;
SIGNAL \sram_dq[11]~input_o\ : std_logic;
SIGNAL \sram_dq[3]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|Selector8~0_combout\ : std_logic;
SIGNAL \sram_dq[4]~input_o\ : std_logic;
SIGNAL \sram_dq[12]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|Selector7~0_combout\ : std_logic;
SIGNAL \sram_dq[5]~input_o\ : std_logic;
SIGNAL \sram_dq[13]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|Selector6~0_combout\ : std_logic;
SIGNAL \sram_dq[14]~input_o\ : std_logic;
SIGNAL \sram_dq[6]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|Selector5~0_combout\ : std_logic;
SIGNAL \sram_dq[15]~input_o\ : std_logic;
SIGNAL \sram_dq[7]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|Selector4~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd1_data~3_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd1_data~4_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd1_data~5_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd1_data~6_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd1_data~7_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd1_data~8_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd1_data~9_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd1_data~10_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[11]~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd1_output_mode.NONE~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd1_valid~feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd1_valid~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_busy~0_combout\ : std_logic;
SIGNAL \rd2_addr[0]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]~1_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_output_mode~8_combout\ : std_logic;
SIGNAL \rd2_access_mode~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_output_mode~9_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_access_mode_buffer~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_output_mode~10_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_output_mode~11_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_output_mode.HIGH_BYTE~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_output_mode~13_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_output_mode.LOW_BYTE~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_output_buffer~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_output_mode~12_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_output_mode.WORD~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_data~2_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|Selector19~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|Selector18~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|Selector17~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|Selector16~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|Selector15~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|Selector14~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|Selector13~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|Selector12~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_data~3_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_data~4_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_data~5_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_data~6_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_data~7_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_data~8_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_data~9_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_data~10_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_output_mode~14_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_output_mode.NONE~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_valid~feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_valid~q\ : std_logic;
SIGNAL \rd1_addr[1]~input_o\ : std_logic;
SIGNAL \rd2_addr[1]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[0]~40_combout\ : std_logic;
SIGNAL \wr_addr[1]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~157feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~157_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~81feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~81_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~43_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~348_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~119_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~349_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~233feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~233_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~195_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~346_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~271feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~271_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~309_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~347_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~350_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[0]~41_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[0]~80_combout\ : std_logic;
SIGNAL \wr_addr[2]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~234feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~234_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~196_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~351_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~272_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~310_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~352_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~120_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~44_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~353_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~158_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~82_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~354_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~355_combout\ : std_logic;
SIGNAL \rd2_addr[2]~input_o\ : std_logic;
SIGNAL \rd1_addr[2]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[1]~42_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[1]~43_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[1]~81_combout\ : std_logic;
SIGNAL \wr_addr[3]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~159_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~121_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~45_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~358_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~83_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~359_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~235feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~235_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~197_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~356_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~273_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~311_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~357_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~360_combout\ : std_logic;
SIGNAL \rd2_addr[3]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[2]~44_combout\ : std_logic;
SIGNAL \rd1_addr[3]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[2]~45_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[2]~82_combout\ : std_logic;
SIGNAL \wr_addr[4]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~274_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~236feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~236_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~198_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~361_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~312_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~362_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~122_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~46_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~363_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~84_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~160_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~364_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~365_combout\ : std_logic;
SIGNAL \rd2_addr[4]~input_o\ : std_logic;
SIGNAL \rd1_addr[4]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[3]~46_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[3]~47_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[3]~83_combout\ : std_logic;
SIGNAL \wr_addr[5]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~123feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~123_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~47_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~368_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~161feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~161_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~85_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~369_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~237_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~199_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~366_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~275feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~275_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~313_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~367_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~370_combout\ : std_logic;
SIGNAL \rd1_addr[5]~input_o\ : std_logic;
SIGNAL \rd2_addr[5]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[4]~48_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[4]~49_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[4]~84_combout\ : std_logic;
SIGNAL \rd2_addr[6]~input_o\ : std_logic;
SIGNAL \rd1_addr[6]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[5]~50_combout\ : std_logic;
SIGNAL \wr_addr[6]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~162_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~124_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~48_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~373_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~86_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~374_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~238feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~238_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~200_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~371_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~276feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~276_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~314_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~372_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~375_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[5]~51_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[5]~85_combout\ : std_logic;
SIGNAL \rd2_addr[7]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[6]~52_combout\ : std_logic;
SIGNAL \rd1_addr[7]~input_o\ : std_logic;
SIGNAL \wr_addr[7]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~125_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~49_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~378_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~163_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~87_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~379_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~239_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~201_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~376_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~277_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~315_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~377_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~380_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[6]~53_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[6]~86_combout\ : std_logic;
SIGNAL \rd1_addr[8]~input_o\ : std_logic;
SIGNAL \rd2_addr[8]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[7]~54_combout\ : std_logic;
SIGNAL \wr_addr[8]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~202_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~240feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~240_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~381_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~278_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~316_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~382_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~164_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~126_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~50_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~383_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~88_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~384_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~385_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[7]~55_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[7]~87_combout\ : std_logic;
SIGNAL \wr_addr[9]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~203_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~241_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~386_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~317_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~279feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~279_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~387_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~51_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~127feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~127_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~388_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~89_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~165feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~165_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~389_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~390_combout\ : std_logic;
SIGNAL \rd2_addr[9]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[8]~56_combout\ : std_logic;
SIGNAL \rd1_addr[9]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[8]~57_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[8]~88_combout\ : std_logic;
SIGNAL \wr_addr[10]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~166_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~128_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~52_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~393_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~90_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~394_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~204_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~242_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~391_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~280_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~318_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~392_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~395_combout\ : std_logic;
SIGNAL \rd2_addr[10]~input_o\ : std_logic;
SIGNAL \rd1_addr[10]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[9]~58_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[9]~59_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[9]~89_combout\ : std_logic;
SIGNAL \rd2_addr[11]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[10]~60_combout\ : std_logic;
SIGNAL \wr_addr[11]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~167_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~129_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~53_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~398_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~91_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~399_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~281_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~205_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~243feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~243_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~396_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~319_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~397_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~400_combout\ : std_logic;
SIGNAL \rd1_addr[11]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[10]~61_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[10]~90_combout\ : std_logic;
SIGNAL \wr_addr[12]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~168feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~168_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~92_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~130feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~130_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~54_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~403_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~404_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~282_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~320_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~206_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~244feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~244_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~401_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~402_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~405_combout\ : std_logic;
SIGNAL \rd1_addr[12]~input_o\ : std_logic;
SIGNAL \rd2_addr[12]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[11]~62_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[11]~63_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[11]~91_combout\ : std_logic;
SIGNAL \rd1_addr[13]~input_o\ : std_logic;
SIGNAL \rd2_addr[13]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[12]~64_combout\ : std_logic;
SIGNAL \wr_addr[13]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~131feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~131_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~55_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~408_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~169feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~169_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~93_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~409_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~245_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~207_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~406_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~283_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~321_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~407_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~410_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[12]~65_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[12]~92_combout\ : std_logic;
SIGNAL \wr_addr[14]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~208_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~246_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~411_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~284_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~322_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~412_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~132feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~132_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~56_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~413_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~170feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~170_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~94_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~414_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~415_combout\ : std_logic;
SIGNAL \rd1_addr[14]~input_o\ : std_logic;
SIGNAL \rd2_addr[14]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[13]~66_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[13]~67_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[13]~93_combout\ : std_logic;
SIGNAL \rd2_addr[15]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[14]~68_combout\ : std_logic;
SIGNAL \rd1_addr[15]~input_o\ : std_logic;
SIGNAL \wr_addr[15]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~209_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~247feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~247_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~416_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~285_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~323_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~417_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~171feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~171_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~133_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~57_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~418_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~95_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~419_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~420_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[14]~69_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[14]~94_combout\ : std_logic;
SIGNAL \wr_addr[16]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~172_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~134_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~58_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~423_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~96_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~424_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~210_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~248feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~248_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~421_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~324_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~286_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~422_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~425_combout\ : std_logic;
SIGNAL \rd2_addr[16]~input_o\ : std_logic;
SIGNAL \rd1_addr[16]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[15]~70_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[15]~71_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[15]~95_combout\ : std_logic;
SIGNAL \wr_addr[17]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~173_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~97_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~59_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~135_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~428_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~429_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~249_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~211_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~426_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~287_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~325_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~427_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~430_combout\ : std_logic;
SIGNAL \rd1_addr[17]~input_o\ : std_logic;
SIGNAL \rd2_addr[17]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[16]~72_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[16]~73_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[16]~96_combout\ : std_logic;
SIGNAL \rd1_addr[18]~input_o\ : std_logic;
SIGNAL \rd2_addr[18]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[17]~74_combout\ : std_logic;
SIGNAL \wr_addr[18]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~288_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~250feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~250_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~212_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~431_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~326_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~432_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~174_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~98_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~60_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~136_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~433_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~434_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~435_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[17]~75_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[17]~97_combout\ : std_logic;
SIGNAL \rd2_addr[19]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[18]~76_combout\ : std_logic;
SIGNAL \wr_addr[19]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~251feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~251_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~213_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~436_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~289_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~327_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~437_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~175_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~99_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~137_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~61_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~438_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~439_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~440_combout\ : std_logic;
SIGNAL \rd1_addr[19]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[18]~77_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[18]~98_combout\ : std_logic;
SIGNAL \rd1_addr[20]~input_o\ : std_logic;
SIGNAL \rd2_addr[20]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[19]~78_combout\ : std_logic;
SIGNAL \wr_addr[20]~input_o\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~252_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~214_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~441_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~290_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~328_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~442_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~176_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~100_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~138_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~62_q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~443_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~444_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~445_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[19]~79_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr_nxt[19]~99_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|Selector1~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|Selector1~1_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_ub_n~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|Selector0~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_lb_n~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_we_n~feeder_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_we_n~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_ce_n_nxt~0_combout\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_ce_n~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_oe_n~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|sram_data_out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\ : std_logic_vector(37 DOWNTO 0);
SIGNAL \sram_ctrl_a21d16b8_inst|rd1_data\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\ : std_logic_vector(20 DOWNTO 0);
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \sram_ctrl_a21d16b8_inst|sram_addr\ : std_logic_vector(19 DOWNTO 0);
SIGNAL \sram_ctrl_a21d16b8_inst|rd2_data\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \sram_ctrl_a21d16b8_inst|ALT_INV_sram_oe_n~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|ALT_INV_sram_ce_n~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|ALT_INV_sram_we_n~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|ALT_INV_sram_lb_n~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|ALT_INV_sram_ub_n~q\ : std_logic;
SIGNAL \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|ALT_INV_rd_valid~q\ : std_logic;

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

ww_clk <= clk;
ww_res_n <= res_n;
ww_wr_addr <= wr_addr;
ww_wr_data <= wr_data;
ww_wr <= wr;
ww_wr_access_mode <= wr_access_mode;
wr_empty <= ww_wr_empty;
wr_full <= ww_wr_full;
wr_half_full <= ww_wr_half_full;
ww_rd1_addr <= rd1_addr;
ww_rd1 <= rd1;
ww_rd1_access_mode <= rd1_access_mode;
rd1_busy <= ww_rd1_busy;
rd1_data <= ww_rd1_data;
rd1_valid <= ww_rd1_valid;
ww_rd2_addr <= rd2_addr;
ww_rd2 <= rd2;
ww_rd2_access_mode <= rd2_access_mode;
rd2_busy <= ww_rd2_busy;
rd2_data <= ww_rd2_data;
rd2_valid <= ww_rd2_valid;
sram_addr <= ww_sram_addr;
sram_ub_n <= ww_sram_ub_n;
sram_lb_n <= ww_sram_lb_n;
sram_we_n <= ww_sram_we_n;
sram_ce_n <= ww_sram_ce_n;
sram_oe_n <= ww_sram_oe_n;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\res_n~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \res_n~input_o\);

\clk~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \clk~input_o\);
\sram_ctrl_a21d16b8_inst|ALT_INV_sram_oe_n~q\ <= NOT \sram_ctrl_a21d16b8_inst|sram_oe_n~q\;
\sram_ctrl_a21d16b8_inst|ALT_INV_sram_ce_n~q\ <= NOT \sram_ctrl_a21d16b8_inst|sram_ce_n~q\;
\sram_ctrl_a21d16b8_inst|ALT_INV_sram_we_n~q\ <= NOT \sram_ctrl_a21d16b8_inst|sram_we_n~q\;
\sram_ctrl_a21d16b8_inst|ALT_INV_sram_lb_n~q\ <= NOT \sram_ctrl_a21d16b8_inst|sram_lb_n~q\;
\sram_ctrl_a21d16b8_inst|ALT_INV_sram_ub_n~q\ <= NOT \sram_ctrl_a21d16b8_inst|sram_ub_n~q\;
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|ALT_INV_rd_valid~q\ <= NOT \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\;
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: IOOBUF_X58_Y73_N2
\sram_dq[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_data_out\(0),
	oe => \sram_ctrl_a21d16b8_inst|sram_drive_data~q\,
	devoe => ww_devoe,
	o => \sram_dq[0]~output_o\);

-- Location: IOOBUF_X49_Y73_N16
\sram_dq[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_data_out\(1),
	oe => \sram_ctrl_a21d16b8_inst|sram_drive_data~q\,
	devoe => ww_devoe,
	o => \sram_dq[1]~output_o\);

-- Location: IOOBUF_X49_Y73_N23
\sram_dq[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_data_out\(2),
	oe => \sram_ctrl_a21d16b8_inst|sram_drive_data~q\,
	devoe => ww_devoe,
	o => \sram_dq[2]~output_o\);

-- Location: IOOBUF_X52_Y73_N23
\sram_dq[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_data_out\(3),
	oe => \sram_ctrl_a21d16b8_inst|sram_drive_data~q\,
	devoe => ww_devoe,
	o => \sram_dq[3]~output_o\);

-- Location: IOOBUF_X54_Y73_N9
\sram_dq[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_data_out\(4),
	oe => \sram_ctrl_a21d16b8_inst|sram_drive_data~q\,
	devoe => ww_devoe,
	o => \sram_dq[4]~output_o\);

-- Location: IOOBUF_X52_Y73_N16
\sram_dq[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_data_out\(5),
	oe => \sram_ctrl_a21d16b8_inst|sram_drive_data~q\,
	devoe => ww_devoe,
	o => \sram_dq[5]~output_o\);

-- Location: IOOBUF_X45_Y73_N9
\sram_dq[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_data_out\(6),
	oe => \sram_ctrl_a21d16b8_inst|sram_drive_data~q\,
	devoe => ww_devoe,
	o => \sram_dq[6]~output_o\);

-- Location: IOOBUF_X52_Y73_N9
\sram_dq[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_data_out\(7),
	oe => \sram_ctrl_a21d16b8_inst|sram_drive_data~q\,
	devoe => ww_devoe,
	o => \sram_dq[7]~output_o\);

-- Location: IOOBUF_X35_Y73_N16
\sram_dq[8]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_data_out\(8),
	oe => \sram_ctrl_a21d16b8_inst|sram_drive_data~q\,
	devoe => ww_devoe,
	o => \sram_dq[8]~output_o\);

-- Location: IOOBUF_X58_Y73_N23
\sram_dq[9]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_data_out\(9),
	oe => \sram_ctrl_a21d16b8_inst|sram_drive_data~q\,
	devoe => ww_devoe,
	o => \sram_dq[9]~output_o\);

-- Location: IOOBUF_X38_Y73_N9
\sram_dq[10]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_data_out\(10),
	oe => \sram_ctrl_a21d16b8_inst|sram_drive_data~q\,
	devoe => ww_devoe,
	o => \sram_dq[10]~output_o\);

-- Location: IOOBUF_X58_Y73_N16
\sram_dq[11]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_data_out\(11),
	oe => \sram_ctrl_a21d16b8_inst|sram_drive_data~q\,
	devoe => ww_devoe,
	o => \sram_dq[11]~output_o\);

-- Location: IOOBUF_X38_Y73_N16
\sram_dq[12]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_data_out\(12),
	oe => \sram_ctrl_a21d16b8_inst|sram_drive_data~q\,
	devoe => ww_devoe,
	o => \sram_dq[12]~output_o\);

-- Location: IOOBUF_X38_Y73_N23
\sram_dq[13]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_data_out\(13),
	oe => \sram_ctrl_a21d16b8_inst|sram_drive_data~q\,
	devoe => ww_devoe,
	o => \sram_dq[13]~output_o\);

-- Location: IOOBUF_X38_Y73_N2
\sram_dq[14]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_data_out\(14),
	oe => \sram_ctrl_a21d16b8_inst|sram_drive_data~q\,
	devoe => ww_devoe,
	o => \sram_dq[14]~output_o\);

-- Location: IOOBUF_X35_Y73_N23
\sram_dq[15]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_data_out\(15),
	oe => \sram_ctrl_a21d16b8_inst|sram_drive_data~q\,
	devoe => ww_devoe,
	o => \sram_dq[15]~output_o\);

-- Location: IOOBUF_X115_Y69_N16
\wr_empty~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|ALT_INV_rd_valid~q\,
	devoe => ww_devoe,
	o => \wr_empty~output_o\);

-- Location: IOOBUF_X115_Y65_N16
\wr_full~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|full~q\,
	devoe => ww_devoe,
	o => \wr_full~output_o\);

-- Location: IOOBUF_X115_Y49_N9
\wr_half_full~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|half_full~q\,
	devoe => ww_devoe,
	o => \wr_half_full~output_o\);

-- Location: IOOBUF_X83_Y73_N16
\rd1_busy~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	devoe => ww_devoe,
	o => \rd1_busy~output_o\);

-- Location: IOOBUF_X47_Y0_N2
\rd1_data[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd1_data\(0),
	devoe => ww_devoe,
	o => \rd1_data[0]~output_o\);

-- Location: IOOBUF_X18_Y73_N23
\rd1_data[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd1_data\(1),
	devoe => ww_devoe,
	o => \rd1_data[1]~output_o\);

-- Location: IOOBUF_X20_Y73_N23
\rd1_data[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd1_data\(2),
	devoe => ww_devoe,
	o => \rd1_data[2]~output_o\);

-- Location: IOOBUF_X20_Y73_N9
\rd1_data[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd1_data\(3),
	devoe => ww_devoe,
	o => \rd1_data[3]~output_o\);

-- Location: IOOBUF_X47_Y73_N2
\rd1_data[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd1_data\(4),
	devoe => ww_devoe,
	o => \rd1_data[4]~output_o\);

-- Location: IOOBUF_X16_Y73_N2
\rd1_data[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd1_data\(5),
	devoe => ww_devoe,
	o => \rd1_data[5]~output_o\);

-- Location: IOOBUF_X20_Y73_N16
\rd1_data[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd1_data\(6),
	devoe => ww_devoe,
	o => \rd1_data[6]~output_o\);

-- Location: IOOBUF_X16_Y73_N9
\rd1_data[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd1_data\(7),
	devoe => ww_devoe,
	o => \rd1_data[7]~output_o\);

-- Location: IOOBUF_X33_Y73_N2
\rd1_data[8]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd1_data\(8),
	devoe => ww_devoe,
	o => \rd1_data[8]~output_o\);

-- Location: IOOBUF_X25_Y73_N23
\rd1_data[9]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd1_data\(9),
	devoe => ww_devoe,
	o => \rd1_data[9]~output_o\);

-- Location: IOOBUF_X27_Y73_N23
\rd1_data[10]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd1_data\(10),
	devoe => ww_devoe,
	o => \rd1_data[10]~output_o\);

-- Location: IOOBUF_X16_Y73_N16
\rd1_data[11]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd1_data\(11),
	devoe => ww_devoe,
	o => \rd1_data[11]~output_o\);

-- Location: IOOBUF_X27_Y73_N16
\rd1_data[12]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd1_data\(12),
	devoe => ww_devoe,
	o => \rd1_data[12]~output_o\);

-- Location: IOOBUF_X25_Y73_N16
\rd1_data[13]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd1_data\(13),
	devoe => ww_devoe,
	o => \rd1_data[13]~output_o\);

-- Location: IOOBUF_X16_Y73_N23
\rd1_data[14]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd1_data\(14),
	devoe => ww_devoe,
	o => \rd1_data[14]~output_o\);

-- Location: IOOBUF_X27_Y73_N9
\rd1_data[15]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd1_data\(15),
	devoe => ww_devoe,
	o => \rd1_data[15]~output_o\);

-- Location: IOOBUF_X115_Y69_N2
\rd1_valid~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd1_valid~q\,
	devoe => ww_devoe,
	o => \rd1_valid~output_o\);

-- Location: IOOBUF_X69_Y73_N16
\rd2_busy~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd2_busy~0_combout\,
	devoe => ww_devoe,
	o => \rd2_busy~output_o\);

-- Location: IOOBUF_X20_Y73_N2
\rd2_data[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd2_data\(0),
	devoe => ww_devoe,
	o => \rd2_data[0]~output_o\);

-- Location: IOOBUF_X42_Y73_N9
\rd2_data[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd2_data\(1),
	devoe => ww_devoe,
	o => \rd2_data[1]~output_o\);

-- Location: IOOBUF_X42_Y73_N2
\rd2_data[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd2_data\(2),
	devoe => ww_devoe,
	o => \rd2_data[2]~output_o\);

-- Location: IOOBUF_X40_Y73_N2
\rd2_data[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd2_data\(3),
	devoe => ww_devoe,
	o => \rd2_data[3]~output_o\);

-- Location: IOOBUF_X33_Y73_N9
\rd2_data[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd2_data\(4),
	devoe => ww_devoe,
	o => \rd2_data[4]~output_o\);

-- Location: IOOBUF_X47_Y73_N16
\rd2_data[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd2_data\(5),
	devoe => ww_devoe,
	o => \rd2_data[5]~output_o\);

-- Location: IOOBUF_X45_Y73_N2
\rd2_data[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd2_data\(6),
	devoe => ww_devoe,
	o => \rd2_data[6]~output_o\);

-- Location: IOOBUF_X40_Y73_N9
\rd2_data[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd2_data\(7),
	devoe => ww_devoe,
	o => \rd2_data[7]~output_o\);

-- Location: IOOBUF_X23_Y73_N16
\rd2_data[8]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd2_data\(8),
	devoe => ww_devoe,
	o => \rd2_data[8]~output_o\);

-- Location: IOOBUF_X31_Y73_N9
\rd2_data[9]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd2_data\(9),
	devoe => ww_devoe,
	o => \rd2_data[9]~output_o\);

-- Location: IOOBUF_X23_Y73_N9
\rd2_data[10]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd2_data\(10),
	devoe => ww_devoe,
	o => \rd2_data[10]~output_o\);

-- Location: IOOBUF_X29_Y73_N2
\rd2_data[11]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd2_data\(11),
	devoe => ww_devoe,
	o => \rd2_data[11]~output_o\);

-- Location: IOOBUF_X23_Y73_N23
\rd2_data[12]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd2_data\(12),
	devoe => ww_devoe,
	o => \rd2_data[12]~output_o\);

-- Location: IOOBUF_X29_Y73_N9
\rd2_data[13]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd2_data\(13),
	devoe => ww_devoe,
	o => \rd2_data[13]~output_o\);

-- Location: IOOBUF_X31_Y73_N2
\rd2_data[14]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd2_data\(14),
	devoe => ww_devoe,
	o => \rd2_data[14]~output_o\);

-- Location: IOOBUF_X23_Y73_N2
\rd2_data[15]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd2_data\(15),
	devoe => ww_devoe,
	o => \rd2_data[15]~output_o\);

-- Location: IOOBUF_X67_Y73_N9
\rd2_valid~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|rd2_valid~q\,
	devoe => ww_devoe,
	o => \rd2_valid~output_o\);

-- Location: IOOBUF_X81_Y73_N16
\sram_addr[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_addr\(0),
	devoe => ww_devoe,
	o => \sram_addr[0]~output_o\);

-- Location: IOOBUF_X96_Y73_N16
\sram_addr[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_addr\(1),
	devoe => ww_devoe,
	o => \sram_addr[1]~output_o\);

-- Location: IOOBUF_X107_Y73_N16
\sram_addr[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_addr\(2),
	devoe => ww_devoe,
	o => \sram_addr[2]~output_o\);

-- Location: IOOBUF_X111_Y73_N9
\sram_addr[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_addr\(3),
	devoe => ww_devoe,
	o => \sram_addr[3]~output_o\);

-- Location: IOOBUF_X115_Y69_N23
\sram_addr[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_addr\(4),
	devoe => ww_devoe,
	o => \sram_addr[4]~output_o\);

-- Location: IOOBUF_X67_Y73_N16
\sram_addr[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_addr\(5),
	devoe => ww_devoe,
	o => \sram_addr[5]~output_o\);

-- Location: IOOBUF_X65_Y73_N23
\sram_addr[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_addr\(6),
	devoe => ww_devoe,
	o => \sram_addr[6]~output_o\);

-- Location: IOOBUF_X69_Y73_N2
\sram_addr[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_addr\(7),
	devoe => ww_devoe,
	o => \sram_addr[7]~output_o\);

-- Location: IOOBUF_X115_Y66_N16
\sram_addr[8]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_addr\(8),
	devoe => ww_devoe,
	o => \sram_addr[8]~output_o\);

-- Location: IOOBUF_X72_Y73_N9
\sram_addr[9]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_addr\(9),
	devoe => ww_devoe,
	o => \sram_addr[9]~output_o\);

-- Location: IOOBUF_X107_Y73_N23
\sram_addr[10]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_addr\(10),
	devoe => ww_devoe,
	o => \sram_addr[10]~output_o\);

-- Location: IOOBUF_X115_Y68_N16
\sram_addr[11]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_addr\(11),
	devoe => ww_devoe,
	o => \sram_addr[11]~output_o\);

-- Location: IOOBUF_X115_Y66_N23
\sram_addr[12]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_addr\(12),
	devoe => ww_devoe,
	o => \sram_addr[12]~output_o\);

-- Location: IOOBUF_X0_Y66_N23
\sram_addr[13]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_addr\(13),
	devoe => ww_devoe,
	o => \sram_addr[13]~output_o\);

-- Location: IOOBUF_X65_Y73_N16
\sram_addr[14]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_addr\(14),
	devoe => ww_devoe,
	o => \sram_addr[14]~output_o\);

-- Location: IOOBUF_X60_Y73_N23
\sram_addr[15]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_addr\(15),
	devoe => ww_devoe,
	o => \sram_addr[15]~output_o\);

-- Location: IOOBUF_X60_Y73_N9
\sram_addr[16]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_addr\(16),
	devoe => ww_devoe,
	o => \sram_addr[16]~output_o\);

-- Location: IOOBUF_X107_Y73_N2
\sram_addr[17]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_addr\(17),
	devoe => ww_devoe,
	o => \sram_addr[17]~output_o\);

-- Location: IOOBUF_X96_Y73_N23
\sram_addr[18]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_addr\(18),
	devoe => ww_devoe,
	o => \sram_addr[18]~output_o\);

-- Location: IOOBUF_X72_Y73_N16
\sram_addr[19]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|sram_addr\(19),
	devoe => ww_devoe,
	o => \sram_addr[19]~output_o\);

-- Location: IOOBUF_X83_Y73_N23
\sram_ub_n~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|ALT_INV_sram_ub_n~q\,
	devoe => ww_devoe,
	o => \sram_ub_n~output_o\);

-- Location: IOOBUF_X109_Y73_N9
\sram_lb_n~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|ALT_INV_sram_lb_n~q\,
	devoe => ww_devoe,
	o => \sram_lb_n~output_o\);

-- Location: IOOBUF_X115_Y65_N23
\sram_we_n~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|ALT_INV_sram_we_n~q\,
	devoe => ww_devoe,
	o => \sram_we_n~output_o\);

-- Location: IOOBUF_X94_Y73_N2
\sram_ce_n~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|ALT_INV_sram_ce_n~q\,
	devoe => ww_devoe,
	o => \sram_ce_n~output_o\);

-- Location: IOOBUF_X100_Y73_N23
\sram_oe_n~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \sram_ctrl_a21d16b8_inst|ALT_INV_sram_oe_n~q\,
	devoe => ww_devoe,
	o => \sram_oe_n~output_o\);

-- Location: IOIBUF_X0_Y36_N8
\clk~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk,
	o => \clk~input_o\);

-- Location: CLKCTRL_G2
\clk~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clk~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clk~inputclkctrl_outclk\);

-- Location: IOIBUF_X115_Y58_N22
\wr_access_mode~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_access_mode,
	o => \wr_access_mode~input_o\);

-- Location: LCCOMB_X76_Y62_N10
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address[0]~0_combout\ = !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address[0]~0_combout\);

-- Location: IOIBUF_X0_Y36_N15
\res_n~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_res_n,
	o => \res_n~input_o\);

-- Location: CLKCTRL_G4
\res_n~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \res_n~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \res_n~inputclkctrl_outclk\);

-- Location: IOIBUF_X0_Y36_N22
\wr~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr,
	o => \wr~input_o\);

-- Location: LCCOMB_X76_Y63_N24
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address[0]~0_combout\ = !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address[0]~0_combout\);

-- Location: IOIBUF_X54_Y73_N1
\rd1~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd1,
	o => \rd1~input_o\);

-- Location: IOIBUF_X65_Y73_N8
\rd2~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd2,
	o => \rd2~input_o\);

-- Location: LCCOMB_X68_Y69_N24
\sram_ctrl_a21d16b8_inst|rd2_pending~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd2_pending~0_combout\ = (\rd1~input_o\ & ((\sram_ctrl_a21d16b8_inst|rd2_pending~q\) # ((\rd2~input_o\ & !\sram_ctrl_a21d16b8_inst|state.IDLE~q\)))) # (!\rd1~input_o\ & (\sram_ctrl_a21d16b8_inst|rd2_pending~q\ & ((\rd2~input_o\) # 
-- (\sram_ctrl_a21d16b8_inst|state.IDLE~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rd1~input_o\,
	datab => \rd2~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|rd2_pending~q\,
	datad => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	combout => \sram_ctrl_a21d16b8_inst|rd2_pending~0_combout\);

-- Location: FF_X68_Y69_N25
\sram_ctrl_a21d16b8_inst|rd2_pending\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd2_pending~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_pending~q\);

-- Location: LCCOMB_X68_Y69_N6
\sram_ctrl_a21d16b8_inst|sram_addr[6]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\ = (!\rd1~input_o\ & (!\rd2~input_o\ & !\sram_ctrl_a21d16b8_inst|rd2_pending~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \rd1~input_o\,
	datac => \rd2~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd2_pending~q\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\);

-- Location: FF_X76_Y62_N7
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add2~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2));

-- Location: LCCOMB_X80_Y63_N0
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add2~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add2~1_combout\ = \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) $ (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add2~1_combout\);

-- Location: FF_X80_Y63_N1
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add2~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1));

-- Location: LCCOMB_X76_Y62_N6
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add2~0_combout\ = \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) $ (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add2~0_combout\);

-- Location: LCCOMB_X76_Y62_N16
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add3~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add3~1_combout\ = \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1) $ (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add3~1_combout\);

-- Location: FF_X76_Y62_N17
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add3~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|wr_int~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1));

-- Location: LCCOMB_X76_Y62_N24
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add3~0_combout\ = \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(2) $ (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0) & 
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(2),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add3~0_combout\);

-- Location: FF_X76_Y62_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add3~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|wr_int~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(2));

-- Location: LCCOMB_X76_Y62_N2
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty_next~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty_next~0_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1) $ (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1) $ (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100001000011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty_next~0_combout\);

-- Location: LCCOMB_X76_Y62_N4
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty_next~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty_next~1_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty_next~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add2~0_combout\ $ (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add2~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(2),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty_next~0_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty_next~1_combout\);

-- Location: LCCOMB_X77_Y65_N26
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty_next~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty_next~2_combout\ = (\wr~input_o\) # (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty~q\ $ (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty_next~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \wr~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty~q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty_next~1_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty_next~2_combout\);

-- Location: FF_X77_Y65_N27
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty_next~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty~q\);

-- Location: LCCOMB_X77_Y65_N0
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~0_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty~q\) # ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\ & !\sram_ctrl_a21d16b8_inst|state.WRITE2~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty~q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	datad => \sram_ctrl_a21d16b8_inst|state.WRITE2~q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~0_combout\);

-- Location: FF_X77_Y65_N1
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\);

-- Location: LCCOMB_X77_Y65_N4
\sram_ctrl_a21d16b8_inst|Selector2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|Selector2~0_combout\ = (!\sram_ctrl_a21d16b8_inst|state.WRITE2~q\ & ((\sram_ctrl_a21d16b8_inst|state.IDLE~q\) # ((\sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|state.WRITE2~q\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	combout => \sram_ctrl_a21d16b8_inst|Selector2~0_combout\);

-- Location: FF_X77_Y65_N5
\sram_ctrl_a21d16b8_inst|state.IDLE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|Selector2~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|state.IDLE~q\);

-- Location: LCCOMB_X77_Y65_N10
\sram_ctrl_a21d16b8_inst|state_nxt.WRITE1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|state_nxt.WRITE1~0_combout\ = (!\sram_ctrl_a21d16b8_inst|state.IDLE~q\ & (\sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datac => \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	combout => \sram_ctrl_a21d16b8_inst|state_nxt.WRITE1~0_combout\);

-- Location: FF_X77_Y65_N11
\sram_ctrl_a21d16b8_inst|state.WRITE1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|state_nxt.WRITE1~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|state.WRITE1~q\);

-- Location: LCCOMB_X77_Y65_N20
\sram_ctrl_a21d16b8_inst|state.WRITE2~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|state.WRITE2~feeder_combout\ = \sram_ctrl_a21d16b8_inst|state.WRITE1~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \sram_ctrl_a21d16b8_inst|state.WRITE1~q\,
	combout => \sram_ctrl_a21d16b8_inst|state.WRITE2~feeder_combout\);

-- Location: FF_X77_Y65_N21
\sram_ctrl_a21d16b8_inst|state.WRITE2\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|state.WRITE2~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|state.WRITE2~q\);

-- Location: LCCOMB_X77_Y65_N24
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty~q\ & ((\sram_ctrl_a21d16b8_inst|state.WRITE2~q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|state.WRITE2~q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty~q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\);

-- Location: FF_X76_Y63_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address[0]~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0));

-- Location: LCCOMB_X76_Y62_N12
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|full_next~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|full_next~0_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1) $ (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1) $ (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001100001000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|full_next~0_combout\);

-- Location: LCCOMB_X77_Y65_N22
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|full_next~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|full_next~1_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|full_next~0_combout\ & (\wr~input_o\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) $ 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add3~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|full_next~0_combout\,
	datab => \wr~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add3~0_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|full_next~1_combout\);

-- Location: LCCOMB_X77_Y65_N18
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|full_next~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|full_next~2_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|full_next~1_combout\) # ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|full~q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|full_next~1_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|full~q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|full_next~2_combout\);

-- Location: FF_X77_Y65_N19
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|full\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|full_next~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|full~q\);

-- Location: LCCOMB_X77_Y65_N12
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|wr_int\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|wr_int~combout\ = (\wr~input_o\ & !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|full~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \wr~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|full~q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|wr_int~combout\);

-- Location: FF_X76_Y62_N11
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address[0]~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|wr_int~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0));

-- Location: LCCOMB_X76_Y62_N20
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|wr_int~combout\ & !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|wr_int~combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(2),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\);

-- Location: FF_X79_Y61_N27
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~193\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_access_mode~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~193_q\);

-- Location: LCCOMB_X76_Y62_N8
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|wr_int~combout\ & !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|wr_int~combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(2),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\);

-- Location: FF_X79_Y61_N17
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~117\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_access_mode~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~117_q\);

-- Location: LCCOMB_X76_Y62_N26
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\ = (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|wr_int~combout\ & !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|wr_int~combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(2),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\);

-- Location: FF_X80_Y61_N15
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~79\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_access_mode~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~79_q\);

-- Location: LCCOMB_X76_Y62_N30
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\ = (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|wr_int~combout\ & !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|wr_int~combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(2),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\);

-- Location: FF_X80_Y61_N21
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~155\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_access_mode~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~155_q\);

-- Location: LCCOMB_X80_Y61_N14
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~453\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~453_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~155_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~79_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~79_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~155_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~453_combout\);

-- Location: LCCOMB_X79_Y61_N16
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~454\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~454_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~453_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~193_q\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~453_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~117_q\))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~453_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~193_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~117_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~453_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~454_combout\);

-- Location: LCCOMB_X75_Y60_N12
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~269feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~269feeder_combout\ = \wr_access_mode~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_access_mode~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~269feeder_combout\);

-- Location: LCCOMB_X76_Y62_N0
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|wr_int~combout\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|wr_int~combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(2),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\);

-- Location: FF_X75_Y60_N13
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~269\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~269feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~269_q\);

-- Location: LCCOMB_X76_Y62_N18
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\ = (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|wr_int~combout\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|wr_int~combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(2),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\);

-- Location: FF_X75_Y60_N31
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~231\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_access_mode~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~231_q\);

-- Location: LCCOMB_X75_Y60_N30
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~451\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~451_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~269_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~231_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~269_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~231_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~451_combout\);

-- Location: LCCOMB_X76_Y62_N14
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\ = (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|wr_int~combout\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|wr_int~combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(2),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\);

-- Location: FF_X76_Y60_N21
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~307\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_access_mode~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~307_q\);

-- Location: LCCOMB_X76_Y62_N28
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|wr_int~combout\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(1),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|wr_int~combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|write_address\(2),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\);

-- Location: FF_X76_Y60_N31
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~345\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_access_mode~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~345_q\);

-- Location: LCCOMB_X76_Y60_N30
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~452\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~452_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~451_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~345_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~451_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~307_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~451_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~307_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~345_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~452_combout\);

-- Location: LCCOMB_X76_Y63_N8
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~455\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~455_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~452_combout\))) 
-- # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~454_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~454_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~452_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~455_combout\);

-- Location: FF_X76_Y63_N9
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[37]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~455_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37));

-- Location: IOIBUF_X115_Y56_N22
\wr_data[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_data(0),
	o => \wr_data[0]~input_o\);

-- Location: LCCOMB_X75_Y60_N24
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~253feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~253feeder_combout\ = \wr_data[0]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_data[0]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~253feeder_combout\);

-- Location: FF_X75_Y60_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~253\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~253feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~253_q\);

-- Location: FF_X75_Y60_N3
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~215\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[0]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~215_q\);

-- Location: LCCOMB_X75_Y60_N2
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~464\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~464_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~253_q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~215_q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~253_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~215_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~464_combout\);

-- Location: FF_X76_Y60_N1
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~291\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[0]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~291_q\);

-- Location: FF_X76_Y60_N3
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~329\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[0]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~329_q\);

-- Location: LCCOMB_X76_Y60_N2
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~465\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~465_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~464_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~329_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~464_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~291_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~464_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~291_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~329_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~465_combout\);

-- Location: FF_X79_Y61_N23
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~177\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[0]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~177_q\);

-- Location: FF_X79_Y61_N21
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~101\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[0]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~101_q\);

-- Location: FF_X80_Y61_N3
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~63\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[0]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~63_q\);

-- Location: FF_X80_Y61_N1
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~139\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[0]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~139_q\);

-- Location: LCCOMB_X80_Y61_N2
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~466\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~466_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~139_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~63_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~63_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~139_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~466_combout\);

-- Location: LCCOMB_X79_Y61_N20
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~467\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~467_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~466_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~177_q\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~466_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~101_q\))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~466_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~177_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~101_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~466_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~467_combout\);

-- Location: LCCOMB_X76_Y62_N22
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~468\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~468_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~465_combout\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~467_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~465_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~467_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~468_combout\);

-- Location: FF_X76_Y62_N23
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~468_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(21));

-- Location: IOIBUF_X91_Y73_N15
\wr_addr[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_addr(0),
	o => \wr_addr[0]~input_o\);

-- Location: LCCOMB_X75_Y60_N8
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~232feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~232feeder_combout\ = \wr_addr[0]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[0]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~232feeder_combout\);

-- Location: FF_X75_Y60_N9
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~232\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~232feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~232_q\);

-- Location: FF_X75_Y60_N19
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~194\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[0]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~194_q\);

-- Location: LCCOMB_X75_Y60_N18
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~446\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~446_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~232_q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~194_q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~232_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~194_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~446_combout\);

-- Location: FF_X76_Y60_N9
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~270\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[0]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~270_q\);

-- Location: FF_X76_Y60_N19
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~308\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[0]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~308_q\);

-- Location: LCCOMB_X76_Y60_N18
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~447\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~447_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~446_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~308_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~446_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~270_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~446_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~270_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~308_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~447_combout\);

-- Location: FF_X79_Y61_N15
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~156\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[0]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~156_q\);

-- Location: FF_X79_Y61_N5
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~80\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[0]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~80_q\);

-- Location: FF_X80_Y61_N27
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~42\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[0]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~42_q\);

-- Location: FF_X80_Y61_N17
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~118\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[0]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~118_q\);

-- Location: LCCOMB_X80_Y61_N26
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~448\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~448_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~118_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~42_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~42_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~118_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~448_combout\);

-- Location: LCCOMB_X79_Y61_N4
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~449\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~449_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~448_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~156_q\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~448_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~80_q\))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~448_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~156_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~80_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~448_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~449_combout\);

-- Location: LCCOMB_X76_Y63_N22
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~450\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~450_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~447_combout\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~449_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~447_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~449_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~450_combout\);

-- Location: FF_X76_Y63_N23
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~450_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0));

-- Location: LCCOMB_X76_Y66_N20
\sram_ctrl_a21d16b8_inst|sram_data_out_nxt[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[0]~0_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(21) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(21),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0),
	combout => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[0]~0_combout\);

-- Location: FF_X76_Y66_N21
\sram_ctrl_a21d16b8_inst|sram_data_out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[0]~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_data_out\(0));

-- Location: LCCOMB_X66_Y69_N8
\sram_ctrl_a21d16b8_inst|Selector2~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|Selector2~1_combout\ = (\sram_ctrl_a21d16b8_inst|state.IDLE~q\) # ((\sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	combout => \sram_ctrl_a21d16b8_inst|Selector2~1_combout\);

-- Location: FF_X66_Y69_N9
\sram_ctrl_a21d16b8_inst|sram_drive_data\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|Selector2~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_drive_data~q\);

-- Location: IOIBUF_X115_Y56_N15
\wr_data[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_data(1),
	o => \wr_data[1]~input_o\);

-- Location: FF_X75_Y60_N5
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~254\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[1]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~254_q\);

-- Location: FF_X75_Y60_N15
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~216\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[1]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~216_q\);

-- Location: LCCOMB_X75_Y60_N14
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~469\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~469_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~254_q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~216_q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~254_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~216_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~469_combout\);

-- Location: FF_X76_Y60_N29
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~292\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[1]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~292_q\);

-- Location: FF_X76_Y60_N7
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~330\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[1]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~330_q\);

-- Location: LCCOMB_X76_Y60_N6
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~470\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~470_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~469_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~330_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~469_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~292_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~469_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~292_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~330_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~470_combout\);

-- Location: FF_X80_Y61_N31
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~64\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[1]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~64_q\);

-- Location: FF_X80_Y61_N29
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~140\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[1]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~140_q\);

-- Location: LCCOMB_X80_Y61_N30
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~471\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~471_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~140_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~64_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~64_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~140_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~471_combout\);

-- Location: FF_X79_Y61_N9
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~102\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[1]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~102_q\);

-- Location: FF_X79_Y61_N3
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~178\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[1]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~178_q\);

-- Location: LCCOMB_X79_Y61_N8
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~472\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~472_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~471_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~178_q\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~471_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~102_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~471_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~102_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~178_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~472_combout\);

-- Location: LCCOMB_X76_Y63_N12
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~473\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~473_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~470_combout\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~472_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~470_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~472_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~473_combout\);

-- Location: FF_X76_Y63_N13
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~473_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(22));

-- Location: LCCOMB_X76_Y66_N22
\sram_ctrl_a21d16b8_inst|sram_data_out_nxt[1]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[1]~1_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(22) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(22),
	combout => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[1]~1_combout\);

-- Location: FF_X76_Y66_N23
\sram_ctrl_a21d16b8_inst|sram_data_out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[1]~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_data_out\(1));

-- Location: IOIBUF_X89_Y73_N8
\wr_data[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_data(2),
	o => \wr_data[2]~input_o\);

-- Location: FF_X77_Y64_N1
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~255\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[2]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~255_q\);

-- Location: FF_X77_Y64_N3
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~217\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[2]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~217_q\);

-- Location: LCCOMB_X77_Y64_N2
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~474\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~474_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~255_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~217_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~255_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~217_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~474_combout\);

-- Location: LCCOMB_X77_Y63_N24
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~293feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~293feeder_combout\ = \wr_data[2]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_data[2]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~293feeder_combout\);

-- Location: FF_X77_Y63_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~293\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~293feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~293_q\);

-- Location: FF_X77_Y63_N19
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~331\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[2]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~331_q\);

-- Location: LCCOMB_X77_Y63_N18
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~475\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~475_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~474_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~331_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~474_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~293_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~474_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~293_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~331_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~475_combout\);

-- Location: LCCOMB_X79_Y61_N30
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~179feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~179feeder_combout\ = \wr_data[2]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_data[2]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~179feeder_combout\);

-- Location: FF_X79_Y61_N31
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~179\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~179feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~179_q\);

-- Location: FF_X79_Y61_N29
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~103\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[2]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~103_q\);

-- Location: FF_X81_Y63_N11
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~65\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[2]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~65_q\);

-- Location: FF_X81_Y63_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~141\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[2]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~141_q\);

-- Location: LCCOMB_X81_Y63_N10
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~476\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~476_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~141_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~65_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~65_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~141_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~476_combout\);

-- Location: LCCOMB_X79_Y61_N28
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~477\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~477_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~476_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~179_q\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~476_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~103_q\))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~476_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~179_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~103_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~476_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~477_combout\);

-- Location: LCCOMB_X76_Y63_N14
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~478\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~478_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~475_combout\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~477_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~475_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~477_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~478_combout\);

-- Location: FF_X76_Y63_N15
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~478_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(23));

-- Location: LCCOMB_X76_Y66_N24
\sram_ctrl_a21d16b8_inst|sram_data_out_nxt[2]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[2]~2_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(23) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(23),
	combout => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[2]~2_combout\);

-- Location: FF_X76_Y66_N25
\sram_ctrl_a21d16b8_inst|sram_data_out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[2]~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_data_out\(2));

-- Location: IOIBUF_X87_Y73_N1
\wr_data[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_data(3),
	o => \wr_data[3]~input_o\);

-- Location: FF_X75_Y62_N27
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~180\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[3]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~180_q\);

-- Location: FF_X81_Y63_N31
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~66\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[3]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~66_q\);

-- Location: FF_X81_Y63_N13
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~142\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[3]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~142_q\);

-- Location: LCCOMB_X81_Y63_N30
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~481\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~481_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~142_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~66_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~66_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~142_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~481_combout\);

-- Location: FF_X75_Y62_N17
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~104\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[3]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~104_q\);

-- Location: LCCOMB_X75_Y62_N16
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~482\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~482_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~481_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~180_q\) # ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~481_combout\ 
-- & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~104_q\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~180_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~481_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~104_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~482_combout\);

-- Location: FF_X77_Y64_N13
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~256\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[3]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~256_q\);

-- Location: FF_X77_Y64_N15
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~218\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[3]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~218_q\);

-- Location: LCCOMB_X77_Y64_N14
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~479\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~479_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~256_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~218_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~256_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~218_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~479_combout\);

-- Location: FF_X77_Y63_N5
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~294\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[3]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~294_q\);

-- Location: FF_X77_Y63_N7
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~332\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[3]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~332_q\);

-- Location: LCCOMB_X77_Y63_N6
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~480\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~480_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~479_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~332_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~479_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~294_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~479_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~294_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~332_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~480_combout\);

-- Location: LCCOMB_X76_Y63_N16
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~483\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~483_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~480_combout\))) 
-- # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~482_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~482_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~480_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~483_combout\);

-- Location: FF_X76_Y63_N17
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~483_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(24));

-- Location: LCCOMB_X76_Y66_N10
\sram_ctrl_a21d16b8_inst|sram_data_out_nxt[3]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[3]~3_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(24) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(24),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0),
	combout => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[3]~3_combout\);

-- Location: FF_X76_Y66_N11
\sram_ctrl_a21d16b8_inst|sram_data_out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[3]~3_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_data_out\(3));

-- Location: IOIBUF_X115_Y58_N15
\wr_data[4]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_data(4),
	o => \wr_data[4]~input_o\);

-- Location: FF_X77_Y63_N17
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~295\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[4]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~295_q\);

-- Location: FF_X77_Y64_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~257\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[4]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~257_q\);

-- Location: FF_X77_Y64_N27
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~219\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[4]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~219_q\);

-- Location: LCCOMB_X77_Y64_N26
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~484\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~484_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~257_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~219_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~257_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~219_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~484_combout\);

-- Location: FF_X77_Y63_N3
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~333\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[4]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~333_q\);

-- Location: LCCOMB_X77_Y63_N2
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~485\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~485_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~484_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~333_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~484_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~295_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~295_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~484_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~333_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~485_combout\);

-- Location: FF_X79_Y63_N27
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~181\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[4]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~181_q\);

-- Location: FF_X79_Y63_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~105\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[4]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~105_q\);

-- Location: FF_X81_Y63_N3
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~67\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[4]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~67_q\);

-- Location: FF_X81_Y63_N9
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~143\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[4]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~143_q\);

-- Location: LCCOMB_X81_Y63_N2
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~486\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~486_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~143_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~67_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~67_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~143_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~486_combout\);

-- Location: LCCOMB_X79_Y63_N24
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~487\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~487_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~486_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~181_q\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~486_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~105_q\))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~486_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~181_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~105_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~486_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~487_combout\);

-- Location: LCCOMB_X76_Y66_N12
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~488\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~488_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~485_combout\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~487_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~485_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~487_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~488_combout\);

-- Location: FF_X76_Y66_N13
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~488_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(25));

-- Location: LCCOMB_X76_Y66_N28
\sram_ctrl_a21d16b8_inst|sram_data_out_nxt[4]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[4]~4_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(25) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(25),
	combout => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[4]~4_combout\);

-- Location: FF_X76_Y66_N29
\sram_ctrl_a21d16b8_inst|sram_data_out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[4]~4_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_data_out\(4));

-- Location: IOIBUF_X85_Y73_N22
\wr_data[5]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_data(5),
	o => \wr_data[5]~input_o\);

-- Location: FF_X77_Y64_N29
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~258\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[5]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~258_q\);

-- Location: FF_X77_Y64_N31
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~220\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[5]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~220_q\);

-- Location: LCCOMB_X77_Y64_N30
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~489\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~489_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~258_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~220_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~258_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~220_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~489_combout\);

-- Location: LCCOMB_X77_Y63_N20
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~296feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~296feeder_combout\ = \wr_data[5]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_data[5]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~296feeder_combout\);

-- Location: FF_X77_Y63_N21
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~296\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~296feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~296_q\);

-- Location: FF_X77_Y63_N23
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~334\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[5]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~334_q\);

-- Location: LCCOMB_X77_Y63_N22
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~490\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~490_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~489_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~334_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~489_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~296_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~489_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~296_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~334_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~490_combout\);

-- Location: FF_X79_Y63_N13
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~182\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[5]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~182_q\);

-- Location: FF_X81_Y63_N23
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~68\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[5]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~68_q\);

-- Location: FF_X81_Y63_N29
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~144\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[5]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~144_q\);

-- Location: LCCOMB_X81_Y63_N22
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~491\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~491_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~144_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~68_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~68_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~144_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~491_combout\);

-- Location: FF_X80_Y63_N9
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~106\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[5]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~106_q\);

-- Location: LCCOMB_X80_Y63_N8
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~492\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~492_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~491_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~182_q\) # ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~491_combout\ 
-- & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~106_q\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~182_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~491_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~106_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~492_combout\);

-- Location: LCCOMB_X76_Y63_N10
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~493\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~493_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~490_combout\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~492_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~490_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~492_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~493_combout\);

-- Location: FF_X76_Y63_N11
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~493_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(26));

-- Location: LCCOMB_X76_Y66_N30
\sram_ctrl_a21d16b8_inst|sram_data_out_nxt[5]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[5]~5_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(26) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(26),
	combout => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[5]~5_combout\);

-- Location: FF_X76_Y66_N31
\sram_ctrl_a21d16b8_inst|sram_data_out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[5]~5_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_data_out\(5));

-- Location: IOIBUF_X81_Y73_N1
\wr_data[6]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_data(6),
	o => \wr_data[6]~input_o\);

-- Location: FF_X77_Y64_N9
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~259\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[6]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~259_q\);

-- Location: FF_X77_Y64_N19
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~221\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[6]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~221_q\);

-- Location: LCCOMB_X77_Y64_N18
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~494\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~494_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~259_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~221_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~259_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~221_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~494_combout\);

-- Location: FF_X77_Y63_N1
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~297\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[6]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~297_q\);

-- Location: FF_X77_Y63_N11
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~335\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[6]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~335_q\);

-- Location: LCCOMB_X77_Y63_N10
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~495\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~495_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~494_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~335_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~494_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~297_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~494_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~297_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~335_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~495_combout\);

-- Location: LCCOMB_X79_Y60_N16
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~183feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~183feeder_combout\ = \wr_data[6]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_data[6]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~183feeder_combout\);

-- Location: FF_X79_Y60_N17
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~183\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~183feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~183_q\);

-- Location: FF_X80_Y63_N19
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~107\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[6]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~107_q\);

-- Location: FF_X81_Y63_N19
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~69\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[6]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~69_q\);

-- Location: FF_X81_Y63_N17
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~145\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[6]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~145_q\);

-- Location: LCCOMB_X81_Y63_N18
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~496\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~496_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~145_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~69_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~69_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~145_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~496_combout\);

-- Location: LCCOMB_X80_Y63_N18
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~497\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~497_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~496_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~183_q\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~496_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~107_q\))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~496_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~183_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~107_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~496_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~497_combout\);

-- Location: LCCOMB_X76_Y65_N24
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~498\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~498_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~495_combout\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~497_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~495_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~497_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~498_combout\);

-- Location: FF_X76_Y65_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~498_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(27));

-- Location: LCCOMB_X76_Y65_N0
\sram_ctrl_a21d16b8_inst|sram_data_out_nxt[6]~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[6]~6_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(27) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(27),
	combout => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[6]~6_combout\);

-- Location: FF_X76_Y65_N1
\sram_ctrl_a21d16b8_inst|sram_data_out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[6]~6_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_data_out\(6));

-- Location: IOIBUF_X115_Y60_N15
\wr_data[7]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_data(7),
	o => \wr_data[7]~input_o\);

-- Location: FF_X77_Y64_N5
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~260\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[7]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~260_q\);

-- Location: FF_X77_Y64_N23
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~222\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[7]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~222_q\);

-- Location: LCCOMB_X77_Y64_N22
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~499\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~499_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~260_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~222_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~260_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~222_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~499_combout\);

-- Location: FF_X77_Y63_N29
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~298\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[7]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~298_q\);

-- Location: FF_X77_Y63_N31
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~336\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[7]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~336_q\);

-- Location: LCCOMB_X77_Y63_N30
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~500\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~500_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~499_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~336_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~499_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~298_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~499_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~298_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~336_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~500_combout\);

-- Location: LCCOMB_X79_Y60_N2
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~184feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~184feeder_combout\ = \wr_data[7]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_data[7]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~184feeder_combout\);

-- Location: FF_X79_Y60_N3
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~184\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~184feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~184_q\);

-- Location: FF_X81_Y63_N15
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~70\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[7]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~70_q\);

-- Location: FF_X81_Y63_N5
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~146\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[7]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~146_q\);

-- Location: LCCOMB_X81_Y63_N14
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~501\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~501_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~146_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~70_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~70_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~146_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~501_combout\);

-- Location: FF_X80_Y63_N13
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~108\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[7]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~108_q\);

-- Location: LCCOMB_X80_Y63_N12
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~502\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~502_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~501_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~184_q\) # ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~501_combout\ 
-- & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~108_q\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~184_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~501_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~108_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~502_combout\);

-- Location: LCCOMB_X80_Y63_N20
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~503\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~503_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~500_combout\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~502_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~500_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~502_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~503_combout\);

-- Location: FF_X80_Y63_N21
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~503_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(28));

-- Location: LCCOMB_X76_Y65_N18
\sram_ctrl_a21d16b8_inst|sram_data_out_nxt[7]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[7]~7_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(28) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(28),
	combout => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[7]~7_combout\);

-- Location: FF_X76_Y65_N19
\sram_ctrl_a21d16b8_inst|sram_data_out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[7]~7_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_data_out\(7));

-- Location: IOIBUF_X83_Y73_N8
\wr_data[8]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_data(8),
	o => \wr_data[8]~input_o\);

-- Location: LCCOMB_X77_Y63_N8
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~299feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~299feeder_combout\ = \wr_data[8]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_data[8]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~299feeder_combout\);

-- Location: FF_X77_Y63_N9
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~299\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~299feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~299_q\);

-- Location: FF_X77_Y64_N17
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~261\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[8]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~261_q\);

-- Location: FF_X77_Y64_N11
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~223\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[8]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~223_q\);

-- Location: LCCOMB_X77_Y64_N10
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~504\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~504_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~261_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~223_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~261_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~223_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~504_combout\);

-- Location: FF_X77_Y63_N27
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~337\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[8]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~337_q\);

-- Location: LCCOMB_X77_Y63_N26
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~505\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~505_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~504_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~337_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~504_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~299_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~299_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~504_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~337_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~505_combout\);

-- Location: LCCOMB_X79_Y63_N14
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~185feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~185feeder_combout\ = \wr_data[8]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_data[8]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~185feeder_combout\);

-- Location: FF_X79_Y63_N15
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~185\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~185feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~185_q\);

-- Location: FF_X81_Y63_N27
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~71\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[8]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~71_q\);

-- Location: FF_X81_Y63_N1
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~147\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[8]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~147_q\);

-- Location: LCCOMB_X81_Y63_N26
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~506\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~506_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~147_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~71_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~71_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~147_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~506_combout\);

-- Location: FF_X80_Y63_N7
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~109\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[8]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~109_q\);

-- Location: LCCOMB_X80_Y63_N6
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~507\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~507_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~506_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~185_q\) # ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~506_combout\ 
-- & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~109_q\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~185_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~506_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~109_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~507_combout\);

-- Location: LCCOMB_X76_Y63_N20
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~508\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~508_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~505_combout\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~507_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~505_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~507_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~508_combout\);

-- Location: FF_X76_Y63_N21
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~508_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(29));

-- Location: LCCOMB_X76_Y66_N16
\sram_ctrl_a21d16b8_inst|sram_data_out_nxt[8]~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[8]~8_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(29))))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(21) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(21),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(29),
	combout => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[8]~8_combout\);

-- Location: FF_X76_Y66_N17
\sram_ctrl_a21d16b8_inst|sram_data_out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[8]~8_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_data_out\(8));

-- Location: IOIBUF_X115_Y64_N8
\wr_data[9]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_data(9),
	o => \wr_data[9]~input_o\);

-- Location: FF_X77_Y63_N13
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~300\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[9]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~300_q\);

-- Location: FF_X77_Y64_N21
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~262\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[9]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~262_q\);

-- Location: FF_X77_Y64_N7
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~224\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[9]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~224_q\);

-- Location: LCCOMB_X77_Y64_N6
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~509\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~509_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~262_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~224_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~262_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~224_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~509_combout\);

-- Location: FF_X77_Y63_N15
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~338\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[9]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~338_q\);

-- Location: LCCOMB_X77_Y63_N14
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~510\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~510_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~509_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~338_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~509_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~300_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~300_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~509_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~338_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~510_combout\);

-- Location: FF_X75_Y62_N31
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~186\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[9]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~186_q\);

-- Location: FF_X81_Y63_N7
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~72\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[9]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~72_q\);

-- Location: FF_X81_Y63_N21
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~148\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[9]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~148_q\);

-- Location: LCCOMB_X81_Y63_N6
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~511\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~511_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~148_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~72_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~72_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~148_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~511_combout\);

-- Location: FF_X75_Y62_N29
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~110\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[9]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~110_q\);

-- Location: LCCOMB_X75_Y62_N28
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~512\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~512_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~511_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~186_q\) # ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~511_combout\ 
-- & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~110_q\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~186_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~511_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~110_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~512_combout\);

-- Location: LCCOMB_X76_Y63_N30
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~513\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~513_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~510_combout\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~512_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~510_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~512_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~513_combout\);

-- Location: FF_X76_Y63_N31
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~513_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(30));

-- Location: LCCOMB_X76_Y66_N18
\sram_ctrl_a21d16b8_inst|sram_data_out_nxt[9]~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[9]~9_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(30))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0) & 
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(22)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(30),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(22),
	combout => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[9]~9_combout\);

-- Location: FF_X76_Y66_N19
\sram_ctrl_a21d16b8_inst|sram_data_out[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[9]~9_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_data_out\(9));

-- Location: IOIBUF_X115_Y52_N8
\wr_data[10]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_data(10),
	o => \wr_data[10]~input_o\);

-- Location: FF_X75_Y62_N19
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~187\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[10]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~187_q\);

-- Location: FF_X75_Y62_N1
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~111\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[10]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~111_q\);

-- Location: FF_X79_Y62_N19
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~73\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[10]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~73_q\);

-- Location: LCCOMB_X79_Y62_N24
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~149feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~149feeder_combout\ = \wr_data[10]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_data[10]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~149feeder_combout\);

-- Location: FF_X79_Y62_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~149\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~149feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~149_q\);

-- Location: LCCOMB_X79_Y62_N18
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~516\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~516_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~149_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~73_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~73_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~149_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~516_combout\);

-- Location: LCCOMB_X75_Y62_N0
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~517\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~517_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~516_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~187_q\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~516_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~111_q\))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~516_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~187_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~111_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~516_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~517_combout\);

-- Location: FF_X76_Y59_N1
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~301\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[10]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~301_q\);

-- Location: FF_X76_Y59_N11
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~339\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[10]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~339_q\);

-- Location: FF_X77_Y59_N9
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~263\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[10]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~263_q\);

-- Location: FF_X77_Y59_N19
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~225\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[10]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~225_q\);

-- Location: LCCOMB_X77_Y59_N18
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~514\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~514_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~263_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~225_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~263_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~225_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~514_combout\);

-- Location: LCCOMB_X76_Y59_N10
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~515\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~515_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~514_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~339_q\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~514_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~301_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~514_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~301_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~339_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~514_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~515_combout\);

-- Location: LCCOMB_X76_Y66_N6
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~518\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~518_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~515_combout\))) 
-- # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~517_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~517_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~515_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~518_combout\);

-- Location: FF_X76_Y66_N7
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~518_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(31));

-- Location: LCCOMB_X76_Y66_N4
\sram_ctrl_a21d16b8_inst|sram_data_out_nxt[10]~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[10]~10_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(31))))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(23) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(23),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(31),
	combout => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[10]~10_combout\);

-- Location: FF_X76_Y66_N5
\sram_ctrl_a21d16b8_inst|sram_data_out[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[10]~10_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_data_out\(10));

-- Location: IOIBUF_X83_Y73_N1
\wr_data[11]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_data(11),
	o => \wr_data[11]~input_o\);

-- Location: FF_X77_Y59_N5
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~264\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[11]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~264_q\);

-- Location: FF_X77_Y59_N23
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~226\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[11]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~226_q\);

-- Location: LCCOMB_X77_Y59_N22
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~519\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~519_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~264_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~226_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~264_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~226_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~519_combout\);

-- Location: LCCOMB_X76_Y59_N20
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~302feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~302feeder_combout\ = \wr_data[11]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_data[11]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~302feeder_combout\);

-- Location: FF_X76_Y59_N21
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~302\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~302feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~302_q\);

-- Location: FF_X76_Y59_N23
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~340\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[11]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~340_q\);

-- Location: LCCOMB_X76_Y59_N22
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~520\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~520_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~519_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~340_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~519_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~302_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~519_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~302_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~340_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~520_combout\);

-- Location: FF_X79_Y62_N15
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~74\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[11]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~74_q\);

-- Location: FF_X79_Y62_N13
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~150\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[11]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~150_q\);

-- Location: LCCOMB_X79_Y62_N14
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~521\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~521_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~150_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~74_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~74_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~150_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~521_combout\);

-- Location: FF_X79_Y63_N9
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~112\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[11]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~112_q\);

-- Location: FF_X79_Y63_N3
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~188\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[11]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~188_q\);

-- Location: LCCOMB_X79_Y63_N8
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~522\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~522_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~521_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~188_q\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~521_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~112_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~521_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~112_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~188_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~522_combout\);

-- Location: LCCOMB_X80_Y63_N22
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~523\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~523_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~520_combout\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~522_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~520_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~522_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~523_combout\);

-- Location: FF_X80_Y63_N23
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[32]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~523_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(32));

-- Location: LCCOMB_X76_Y66_N14
\sram_ctrl_a21d16b8_inst|sram_data_out_nxt[11]~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[11]~11_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(32))))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(24) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(24),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(32),
	combout => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[11]~11_combout\);

-- Location: FF_X76_Y66_N15
\sram_ctrl_a21d16b8_inst|sram_data_out[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[11]~11_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_data_out\(11));

-- Location: IOIBUF_X115_Y62_N8
\wr_data[12]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_data(12),
	o => \wr_data[12]~input_o\);

-- Location: FF_X77_Y59_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~265\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[12]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~265_q\);

-- Location: FF_X77_Y59_N11
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~227\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[12]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~227_q\);

-- Location: LCCOMB_X77_Y59_N10
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~524\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~524_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~265_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~227_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~265_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~227_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~524_combout\);

-- Location: FF_X76_Y59_N3
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~341\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[12]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~341_q\);

-- Location: FF_X76_Y59_N17
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~303\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[12]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~303_q\);

-- Location: LCCOMB_X76_Y59_N2
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~525\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~525_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~524_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~341_q\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~524_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~303_q\))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~524_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~524_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~341_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~303_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~525_combout\);

-- Location: FF_X79_Y63_N31
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~189\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[12]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~189_q\);

-- Location: FF_X79_Y63_N21
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~113\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[12]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~113_q\);

-- Location: FF_X79_Y62_N3
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~75\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[12]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~75_q\);

-- Location: FF_X79_Y62_N17
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~151\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[12]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~151_q\);

-- Location: LCCOMB_X79_Y62_N2
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~526\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~526_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~151_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~75_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~75_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~151_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~526_combout\);

-- Location: LCCOMB_X79_Y63_N20
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~527\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~527_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~526_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~189_q\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~526_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~113_q\))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~526_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~189_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~113_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~526_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~527_combout\);

-- Location: LCCOMB_X76_Y63_N0
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~528\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~528_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~525_combout\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~527_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~525_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~527_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~528_combout\);

-- Location: FF_X76_Y63_N1
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[33]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~528_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(33));

-- Location: LCCOMB_X76_Y66_N0
\sram_ctrl_a21d16b8_inst|sram_data_out_nxt[12]~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[12]~12_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(33))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0) & 
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(25)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(33),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(25),
	combout => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[12]~12_combout\);

-- Location: FF_X76_Y66_N1
\sram_ctrl_a21d16b8_inst|sram_data_out[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[12]~12_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_data_out\(12));

-- Location: IOIBUF_X85_Y73_N1
\wr_data[13]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_data(13),
	o => \wr_data[13]~input_o\);

-- Location: LCCOMB_X76_Y59_N12
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~304feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~304feeder_combout\ = \wr_data[13]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_data[13]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~304feeder_combout\);

-- Location: FF_X76_Y59_N13
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~304\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~304feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~304_q\);

-- Location: FF_X77_Y59_N21
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~266\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[13]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~266_q\);

-- Location: FF_X77_Y59_N31
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~228\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[13]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~228_q\);

-- Location: LCCOMB_X77_Y59_N30
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~529\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~529_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~266_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~228_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~266_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~228_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~529_combout\);

-- Location: FF_X76_Y59_N7
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~342\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[13]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~342_q\);

-- Location: LCCOMB_X76_Y59_N6
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~530\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~530_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~529_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~342_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~529_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~304_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~304_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~529_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~342_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~530_combout\);

-- Location: FF_X79_Y60_N7
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~190\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[13]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~190_q\);

-- Location: FF_X79_Y62_N23
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~76\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[13]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~76_q\);

-- Location: FF_X79_Y62_N21
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~152\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[13]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~152_q\);

-- Location: LCCOMB_X79_Y62_N22
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~531\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~531_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~152_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~76_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~76_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~152_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~531_combout\);

-- Location: FF_X79_Y60_N21
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~114\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[13]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~114_q\);

-- Location: LCCOMB_X79_Y60_N20
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~532\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~532_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~531_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~190_q\) # ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~531_combout\ 
-- & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~114_q\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~190_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~531_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~114_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~532_combout\);

-- Location: LCCOMB_X76_Y63_N18
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~533\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~533_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~530_combout\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~532_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~530_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~532_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~533_combout\);

-- Location: FF_X76_Y63_N19
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[34]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~533_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(34));

-- Location: LCCOMB_X76_Y66_N2
\sram_ctrl_a21d16b8_inst|sram_data_out_nxt[13]~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[13]~13_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(34))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0) & 
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(26)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(34),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(26),
	combout => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[13]~13_combout\);

-- Location: FF_X76_Y66_N3
\sram_ctrl_a21d16b8_inst|sram_data_out[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[13]~13_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_data_out\(13));

-- Location: IOIBUF_X87_Y73_N22
\wr_data[14]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_data(14),
	o => \wr_data[14]~input_o\);

-- Location: FF_X76_Y59_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~305\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[14]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~305_q\);

-- Location: FF_X76_Y59_N27
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~343\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[14]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~343_q\);

-- Location: FF_X77_Y59_N17
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~267\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[14]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~267_q\);

-- Location: FF_X77_Y59_N27
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~229\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[14]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~229_q\);

-- Location: LCCOMB_X77_Y59_N26
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~534\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~534_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~267_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~229_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~267_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~229_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~534_combout\);

-- Location: LCCOMB_X76_Y59_N26
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~535\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~535_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~534_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~343_q\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~534_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~305_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~534_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~305_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~343_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~534_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~535_combout\);

-- Location: FF_X79_Y62_N11
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~77\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[14]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~77_q\);

-- Location: LCCOMB_X79_Y62_N0
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~153feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~153feeder_combout\ = \wr_data[14]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_data[14]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~153feeder_combout\);

-- Location: FF_X79_Y62_N1
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~153\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~153feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~153_q\);

-- Location: LCCOMB_X79_Y62_N10
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~536\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~536_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~153_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~77_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~77_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~153_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~536_combout\);

-- Location: LCCOMB_X79_Y60_N24
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~191feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~191feeder_combout\ = \wr_data[14]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_data[14]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~191feeder_combout\);

-- Location: FF_X79_Y60_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~191\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~191feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~191_q\);

-- Location: FF_X80_Y63_N17
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~115\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[14]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~115_q\);

-- Location: LCCOMB_X80_Y63_N16
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~537\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~537_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~536_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~191_q\) # ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~536_combout\ 
-- & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~115_q\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~536_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~191_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~115_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~537_combout\);

-- Location: LCCOMB_X76_Y63_N28
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~538\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~538_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~535_combout\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~537_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~535_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~537_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~538_combout\);

-- Location: FF_X76_Y63_N29
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[35]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~538_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(35));

-- Location: LCCOMB_X76_Y65_N20
\sram_ctrl_a21d16b8_inst|sram_data_out_nxt[14]~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[14]~14_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(35))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0) & 
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(27)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(35),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(27),
	combout => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[14]~14_combout\);

-- Location: FF_X76_Y65_N21
\sram_ctrl_a21d16b8_inst|sram_data_out[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[14]~14_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_data_out\(14));

-- Location: IOIBUF_X115_Y57_N22
\wr_data[15]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_data(15),
	o => \wr_data[15]~input_o\);

-- Location: FF_X77_Y59_N29
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~268\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[15]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~268_q\);

-- Location: FF_X77_Y59_N7
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~230\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[15]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~230_q\);

-- Location: LCCOMB_X77_Y59_N6
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~539\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~539_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~268_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~230_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~268_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~230_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~539_combout\);

-- Location: FF_X76_Y59_N29
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~306\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[15]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~306_q\);

-- Location: FF_X76_Y59_N15
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~344\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[15]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~344_q\);

-- Location: LCCOMB_X76_Y59_N14
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~540\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~540_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~539_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~344_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~539_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~306_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~539_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~306_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~344_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~540_combout\);

-- Location: LCCOMB_X79_Y60_N12
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~192feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~192feeder_combout\ = \wr_data[15]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_data[15]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~192feeder_combout\);

-- Location: FF_X79_Y60_N13
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~192\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~192feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~192_q\);

-- Location: FF_X79_Y60_N11
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~116\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[15]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~116_q\);

-- Location: FF_X79_Y62_N7
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~78\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[15]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~78_q\);

-- Location: FF_X79_Y62_N29
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~154\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_data[15]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~154_q\);

-- Location: LCCOMB_X79_Y62_N6
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~541\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~541_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~154_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~78_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~78_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~154_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~541_combout\);

-- Location: LCCOMB_X79_Y60_N10
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~542\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~542_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~541_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~192_q\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~541_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~116_q\))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~541_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~192_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~116_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~541_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~542_combout\);

-- Location: LCCOMB_X76_Y65_N10
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~543\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~543_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~540_combout\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~542_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~540_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~542_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~543_combout\);

-- Location: FF_X76_Y65_N11
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[36]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~543_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(36));

-- Location: LCCOMB_X76_Y65_N14
\sram_ctrl_a21d16b8_inst|sram_data_out_nxt[15]~15\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[15]~15_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(36))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0) & 
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(28)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(36),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(28),
	combout => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[15]~15_combout\);

-- Location: FF_X76_Y65_N15
\sram_ctrl_a21d16b8_inst|sram_data_out[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_data_out_nxt[15]~15_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_data_out\(15));

-- Location: LCCOMB_X77_Y65_N2
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ = (\wr~input_o\ & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\ & !\sram_ctrl_a21d16b8_inst|state.WRITE2~q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	datab => \sram_ctrl_a21d16b8_inst|state.WRITE2~q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty~q\,
	datad => \wr~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\);

-- Location: FF_X86_Y63_N11
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[24]~18_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(24));

-- Location: LCCOMB_X77_Y65_N28
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ = \wr~input_o\ $ (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty~q\ & ((\sram_ctrl_a21d16b8_inst|state.WRITE2~q\) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010111111010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	datab => \sram_ctrl_a21d16b8_inst|state.WRITE2~q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|empty~q\,
	datad => \wr~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\);

-- Location: LCCOMB_X85_Y64_N0
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~0_combout\ = \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(0) $ (VCC)
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~1\ = CARRY(\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(0),
	datad => VCC,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~0_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~1\);

-- Location: FF_X85_Y64_N1
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(0));

-- Location: LCCOMB_X85_Y64_N2
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~2_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(1) & 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~1\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~1\) # (GND))))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(1) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~1\ & VCC)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(1) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~1\))))
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~3\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~1\) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(1)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(1) & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100100101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(1),
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~1\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~2_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~3\);

-- Location: FF_X85_Y64_N3
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(1));

-- Location: LCCOMB_X85_Y64_N4
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~4_combout\ = ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ $ (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(2) $ 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~3\)))) # (GND)
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~5\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(2) & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~3\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(2)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~3\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(2),
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~3\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~4_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~5\);

-- Location: LCCOMB_X83_Y64_N16
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[2]~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[2]~12_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~4_combout\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(2),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~4_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[2]~12_combout\);

-- Location: FF_X83_Y64_N17
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[2]~12_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(2));

-- Location: LCCOMB_X85_Y64_N6
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~6_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(3) & 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~5\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(3) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~5\) # (GND))))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(3) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~5\ & VCC)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(3) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~5\))))
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~7\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~5\) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(3)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(3) & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~5\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100100101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(3),
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~5\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~6_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~7\);

-- Location: LCCOMB_X84_Y64_N6
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[3]~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[3]~11_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~6_combout\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(3),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~6_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[3]~11_combout\);

-- Location: FF_X84_Y64_N7
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[3]~11_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(3));

-- Location: LCCOMB_X85_Y64_N8
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~8_combout\ = ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ $ (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(4) $ 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~7\)))) # (GND)
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~9\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(4) & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~7\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(4)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~7\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(4),
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~7\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~8_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~9\);

-- Location: LCCOMB_X84_Y64_N20
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[4]~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[4]~13_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~8_combout\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~8_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(4),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[4]~13_combout\);

-- Location: FF_X84_Y64_N21
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[4]~13_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(4));

-- Location: LCCOMB_X85_Y64_N10
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~10_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(5) & 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~9\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(5) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~9\) # (GND))))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(5) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~9\ & VCC)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(5) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~9\))))
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~11\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~9\) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(5)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(5) & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~9\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100100101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(5),
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~9\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~10_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~11\);

-- Location: LCCOMB_X84_Y64_N18
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[5]~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[5]~14_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~10_combout\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~10_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(5),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[5]~14_combout\);

-- Location: FF_X84_Y64_N19
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[5]~14_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(5));

-- Location: LCCOMB_X85_Y64_N12
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~12_combout\ = ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(6) $ (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ $ 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~11\)))) # (GND)
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~13\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(6) & ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~11\) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(6) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~11\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(6),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~11\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~12_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~13\);

-- Location: LCCOMB_X84_Y64_N0
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[6]~15\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[6]~15_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~12_combout\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~12_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(6),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[6]~15_combout\);

-- Location: FF_X84_Y64_N1
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[6]~15_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(6));

-- Location: LCCOMB_X85_Y64_N14
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~14_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(7) & 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~13\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(7) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~13\) # (GND))))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(7) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~13\ & VCC)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(7) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~13\))))
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~15\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~13\) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(7)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(7) & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~13\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100100101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(7),
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~13\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~14_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~15\);

-- Location: LCCOMB_X84_Y64_N22
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[7]~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[7]~16_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~14_combout\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(7))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~14_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(7),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[7]~16_combout\);

-- Location: FF_X84_Y64_N23
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[7]~16_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(7));

-- Location: LCCOMB_X85_Y64_N16
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~16_combout\ = ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ $ (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(8) $ 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~15\)))) # (GND)
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~17\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(8) & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~15\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(8)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~15\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(8),
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~15\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~16_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~17\);

-- Location: LCCOMB_X84_Y64_N28
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[8]~17\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[8]~17_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~16_combout\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(8)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(8),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~16_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[8]~17_combout\);

-- Location: FF_X84_Y64_N29
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[8]~17_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(8));

-- Location: LCCOMB_X85_Y64_N18
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~18_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(9) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~17\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~17\ & VCC)))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(9) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~17\) # (GND))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~17\))))
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~19\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(9) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~17\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(9) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~17\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100101001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(9),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~17\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~18_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~19\);

-- Location: LCCOMB_X84_Y64_N10
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[9]~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[9]~10_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~18_combout\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(9),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~18_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[9]~10_combout\);

-- Location: FF_X84_Y64_N11
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[9]~10_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(9));

-- Location: LCCOMB_X85_Y64_N20
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~20_combout\ = ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(10) $ (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ $ 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~19\)))) # (GND)
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~21\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(10) & ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~19\) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(10) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~19\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(10),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~19\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~20_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~21\);

-- Location: LCCOMB_X84_Y64_N24
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[10]~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[10]~8_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~20_combout\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~20_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(10),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[10]~8_combout\);

-- Location: FF_X84_Y64_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[10]~8_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(10));

-- Location: LCCOMB_X85_Y64_N22
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~22\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~22_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(11) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~21\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~21\ & VCC)))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(11) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~21\) # (GND))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~21\))))
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~23\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(11) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~21\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(11) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~21\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100101001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(11),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~21\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~22_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~23\);

-- Location: LCCOMB_X84_Y64_N14
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[11]~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[11]~9_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~22_combout\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(11),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~22_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[11]~9_combout\);

-- Location: FF_X84_Y64_N15
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[11]~9_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(11));

-- Location: LCCOMB_X85_Y64_N24
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~24\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~24_combout\ = ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(12) $ (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ $ 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~23\)))) # (GND)
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~25\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(12) & ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~23\) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(12) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~23\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(12),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~23\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~24_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~25\);

-- Location: LCCOMB_X84_Y64_N12
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[12]~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[12]~6_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~24_combout\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(12)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(12),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~24_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[12]~6_combout\);

-- Location: FF_X84_Y64_N13
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[12]~6_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(12));

-- Location: LCCOMB_X85_Y64_N26
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~26\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~26_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(13) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~25\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~25\ & VCC)))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(13) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~25\) # (GND))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~25\))))
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~27\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(13) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~25\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(13) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~25\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100101001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(13),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~25\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~26_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~27\);

-- Location: LCCOMB_X84_Y64_N2
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[13]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[13]~7_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~26_combout\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(13),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~26_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[13]~7_combout\);

-- Location: FF_X84_Y64_N3
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[13]~7_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(13));

-- Location: LCCOMB_X85_Y64_N28
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~28\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~28_combout\ = ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(14) $ (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ $ 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~27\)))) # (GND)
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~29\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(14) & ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~27\) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(14) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~27\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(14),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~27\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~28_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~29\);

-- Location: LCCOMB_X84_Y64_N8
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[14]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[14]~5_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~28_combout\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(14)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(14),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~28_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[14]~5_combout\);

-- Location: FF_X84_Y64_N9
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[14]~5_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(14));

-- Location: LCCOMB_X85_Y64_N30
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~30\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~30_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(15) & 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~29\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(15) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~29\) # (GND))))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(15) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~29\ & VCC)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(15) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~29\))))
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~31\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~29\) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(15)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(15) & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~29\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100100101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(15),
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~29\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~30_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~31\);

-- Location: LCCOMB_X84_Y63_N18
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[15]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[15]~2_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~30_combout\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(15))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~30_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(15),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[15]~2_combout\);

-- Location: FF_X84_Y63_N19
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[15]~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(15));

-- Location: LCCOMB_X85_Y63_N0
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~32\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~32_combout\ = ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(16) $ (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ $ 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~31\)))) # (GND)
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~33\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(16) & ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~31\) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(16) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~31\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(16),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~31\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~32_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~33\);

-- Location: LCCOMB_X84_Y63_N8
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[16]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[16]~3_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~32_combout\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(16))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~32_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(16),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[16]~3_combout\);

-- Location: FF_X84_Y63_N9
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[16]~3_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(16));

-- Location: LCCOMB_X85_Y63_N2
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~34\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~34_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(17) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~33\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~33\ & VCC)))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(17) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~33\) # (GND))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~33\))))
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~35\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(17) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~33\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(17) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~33\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100101001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(17),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~33\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~34_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~35\);

-- Location: LCCOMB_X84_Y63_N14
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[17]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[17]~4_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~34_combout\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(17)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(17),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~34_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[17]~4_combout\);

-- Location: FF_X84_Y63_N15
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[17]~4_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(17));

-- Location: LCCOMB_X85_Y63_N4
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~36\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~36_combout\ = ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(18) $ (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ $ 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~35\)))) # (GND)
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~37\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(18) & ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~35\) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(18) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~35\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(18),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~35\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~36_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~37\);

-- Location: LCCOMB_X86_Y63_N14
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[18]~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[18]~20_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~36_combout\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(18))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~36_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(18),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[18]~20_combout\);

-- Location: FF_X86_Y63_N15
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[18]~20_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(18));

-- Location: LCCOMB_X85_Y63_N6
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~38\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~38_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(19) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~37\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~37\ & VCC)))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(19) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~37\) # (GND))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~37\))))
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~39\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(19) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~37\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(19) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~37\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100101001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(19),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~37\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~38_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~39\);

-- Location: LCCOMB_X86_Y63_N4
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[19]~21\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[19]~21_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~38_combout\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(19))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~38_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(19),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[19]~21_combout\);

-- Location: FF_X86_Y63_N5
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[19]~21_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(19));

-- Location: LCCOMB_X85_Y63_N8
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~40\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~40_combout\ = ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(20) $ (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ $ 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~39\)))) # (GND)
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~41\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(20) & ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~39\) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(20) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~39\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(20),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~39\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~40_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~41\);

-- Location: LCCOMB_X86_Y63_N18
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[20]~22\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[20]~22_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~40_combout\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(20)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(20),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~40_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[20]~22_combout\);

-- Location: FF_X86_Y63_N19
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[20]~22_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(20));

-- Location: LCCOMB_X85_Y63_N10
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~42\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~42_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(21) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~41\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~41\ & VCC)))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(21) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~41\) # (GND))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~41\))))
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~43\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(21) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~41\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(21) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~41\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100101001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(21),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~41\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~42_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~43\);

-- Location: LCCOMB_X86_Y63_N8
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[21]~23\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[21]~23_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~42_combout\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(21)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(21),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~42_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[21]~23_combout\);

-- Location: FF_X86_Y63_N9
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[21]~23_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(21));

-- Location: LCCOMB_X85_Y63_N12
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~44\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~44_combout\ = ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(22) $ (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ $ 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~43\)))) # (GND)
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~45\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(22) & ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~43\) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(22) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~43\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(22),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~43\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~44_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~45\);

-- Location: LCCOMB_X86_Y63_N30
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[22]~24\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[22]~24_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~44_combout\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(22)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(22),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~44_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[22]~24_combout\);

-- Location: FF_X86_Y63_N31
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[22]~24_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(22));

-- Location: LCCOMB_X85_Y63_N14
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~46\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~46_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(23) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~45\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~45\ & VCC)))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(23) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~45\) # (GND))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~45\))))
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~47\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(23) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~45\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(23) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~45\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100101001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(23),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~45\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~46_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~47\);

-- Location: LCCOMB_X86_Y63_N20
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[23]~25\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[23]~25_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~46_combout\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(23)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(23),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~46_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[23]~25_combout\);

-- Location: FF_X86_Y63_N21
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[23]~25_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(23));

-- Location: LCCOMB_X85_Y63_N16
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~48\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~48_combout\ = ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ $ (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(24) $ 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~47\)))) # (GND)
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~49\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(24) & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~47\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(24)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~47\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(24),
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~47\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~48_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~49\);

-- Location: LCCOMB_X86_Y63_N10
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[24]~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[24]~18_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~48_combout\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(24))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~48_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(24),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[24]~18_combout\);

-- Location: LCCOMB_X86_Y63_N16
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~9_combout\ = (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[19]~21_combout\ & 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[21]~23_combout\ & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[18]~20_combout\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[20]~22_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[19]~21_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[21]~23_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[18]~20_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[20]~22_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~9_combout\);

-- Location: LCCOMB_X86_Y63_N28
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~15\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~15_combout\ = (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[23]~25_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~44_combout\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(22))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000100000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~44_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[23]~25_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(22),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~15_combout\);

-- Location: FF_X86_Y63_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[25]~19_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(25));

-- Location: LCCOMB_X85_Y63_N18
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~50\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~50_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(25) & 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~49\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(25) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~49\) # (GND))))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(25) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~49\ & VCC)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(25) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~49\))))
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~51\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~49\) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(25)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(25) & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~49\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100100101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(25),
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~49\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~50_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~51\);

-- Location: LCCOMB_X86_Y63_N24
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[25]~19\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[25]~19_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~50_combout\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(25)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(25),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~50_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[25]~19_combout\);

-- Location: LCCOMB_X86_Y63_N26
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~10_combout\ = (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[24]~18_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~9_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~15_combout\ & !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[25]~19_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[24]~18_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~9_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~15_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[25]~19_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~10_combout\);

-- Location: LCCOMB_X84_Y64_N16
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~4_combout\ = (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[2]~12_combout\ & 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[4]~13_combout\ & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[3]~11_combout\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[5]~14_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[2]~12_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[4]~13_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[3]~11_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[5]~14_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~4_combout\);

-- Location: LCCOMB_X84_Y64_N26
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~5_combout\ = (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[6]~15_combout\ & 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[8]~17_combout\ & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[7]~16_combout\ & 
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[6]~15_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[8]~17_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[7]~16_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~4_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~5_combout\);

-- Location: LCCOMB_X84_Y64_N4
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~6_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~5_combout\ & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[11]~9_combout\ & 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[9]~10_combout\ & !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[10]~8_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~5_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[11]~9_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[9]~10_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[10]~8_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~6_combout\);

-- Location: LCCOMB_X84_Y64_N30
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~7_combout\ = (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[12]~6_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~6_combout\ & 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[14]~5_combout\ & !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[13]~7_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[12]~6_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~6_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[14]~5_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[13]~7_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~7_combout\);

-- Location: LCCOMB_X84_Y63_N20
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~8_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~7_combout\ & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[16]~3_combout\ & 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[17]~4_combout\ & !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[15]~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~7_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[16]~3_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[17]~4_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[15]~2_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~8_combout\);

-- Location: FF_X84_Y63_N31
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[29]~28_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(29));

-- Location: LCCOMB_X85_Y63_N20
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~52\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~52_combout\ = ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ $ (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(26) $ 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~51\)))) # (GND)
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~53\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(26) & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~51\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(26)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~51\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(26),
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~51\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~52_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~53\);

-- Location: LCCOMB_X84_Y63_N28
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[26]~30\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[26]~30_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~52_combout\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(26))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~52_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(26),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[26]~30_combout\);

-- Location: FF_X84_Y63_N29
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[26]~30_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(26));

-- Location: LCCOMB_X85_Y63_N22
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~54\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~54_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(27) & 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~53\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(27) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~53\) # (GND))))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(27) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~53\ & VCC)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(27) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~53\))))
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~55\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~53\) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(27)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(27) & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~53\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100100101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(27),
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~53\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~54_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~55\);

-- Location: LCCOMB_X84_Y63_N2
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[27]~29\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[27]~29_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~54_combout\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(27)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(27),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~54_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[27]~29_combout\);

-- Location: FF_X84_Y63_N3
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[27]~29_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(27));

-- Location: LCCOMB_X85_Y63_N24
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~56\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~56_combout\ = ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ $ (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(28) $ 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~55\)))) # (GND)
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~57\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(28) & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~55\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(28)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~55\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(28),
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~55\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~56_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~57\);

-- Location: LCCOMB_X84_Y63_N16
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[28]~27\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[28]~27_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~56_combout\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(28)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(28),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~56_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[28]~27_combout\);

-- Location: FF_X84_Y63_N17
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[28]~27_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(28));

-- Location: LCCOMB_X85_Y63_N26
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~58\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~58_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(29) & 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~57\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(29) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~57\) # (GND))))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(29) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~57\ & VCC)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(29) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~57\))))
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~59\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~57\) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(29)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(29) & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~57\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100100101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(29),
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~57\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~58_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~59\);

-- Location: LCCOMB_X84_Y63_N30
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[29]~28\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[29]~28_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~58_combout\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(29)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(29),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~58_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[29]~28_combout\);

-- Location: FF_X84_Y63_N5
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[30]~26_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(30));

-- Location: LCCOMB_X85_Y63_N28
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~60\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~60_combout\ = ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ $ (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(30) $ 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~59\)))) # (GND)
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~61\ = CARRY((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(30) & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~59\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(30)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~59\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(30),
	datad => VCC,
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~59\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~60_combout\,
	cout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~61\);

-- Location: LCCOMB_X84_Y63_N4
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[30]~26\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[30]~26_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~60_combout\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(30)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(30),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~60_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[30]~26_combout\);

-- Location: LCCOMB_X84_Y63_N22
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~11_combout\ = (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(27) & !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(26))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(27),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(26),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~11_combout\);

-- Location: LCCOMB_X84_Y63_N0
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~12_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & (((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~52_combout\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~54_combout\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~11_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001000101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~11_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~52_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~54_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~12_combout\);

-- Location: LCCOMB_X84_Y63_N26
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~13_combout\ = (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[29]~28_combout\ & 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[28]~27_combout\ & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[30]~26_combout\ & 
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~12_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[29]~28_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[28]~27_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[30]~26_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~12_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~13_combout\);

-- Location: FF_X84_Y63_N11
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[31]~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(31));

-- Location: LCCOMB_X85_Y63_N30
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~62\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~62_combout\ = \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\ $ (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~61\ $ 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(31)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010100101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|exec~0_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(31),
	cin => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~61\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~62_combout\);

-- Location: LCCOMB_X84_Y63_N10
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[31]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[31]~1_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~62_combout\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(31))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|Add0~62_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff\(31),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[0]~0_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[31]~1_combout\);

-- Location: LCCOMB_X84_Y63_N24
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~14_combout\ = (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[31]~1_combout\ & 
-- (((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~13_combout\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~8_combout\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~10_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~10_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~8_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~13_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|pointer_diff_next[31]~1_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~14_combout\);

-- Location: FF_X84_Y63_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|half_full\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|LessThan0~14_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|half_full~q\);

-- Location: IOIBUF_X60_Y73_N15
\rd1_addr[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd1_addr(0),
	o => \rd1_addr[0]~input_o\);

-- Location: IOIBUF_X62_Y73_N15
\rd1_access_mode~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd1_access_mode,
	o => \rd1_access_mode~input_o\);

-- Location: LCCOMB_X66_Y69_N24
\sram_ctrl_a21d16b8_inst|rd1_output_mode~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd1_output_mode~10_combout\ = (!\rd1_addr[0]~input_o\ & (\rd1~input_o\ & (!\sram_ctrl_a21d16b8_inst|state.IDLE~q\ & !\rd1_access_mode~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rd1_addr[0]~input_o\,
	datab => \rd1~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \rd1_access_mode~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|rd1_output_mode~10_combout\);

-- Location: FF_X66_Y69_N25
\sram_ctrl_a21d16b8_inst|rd1_output_mode.LOW_BYTE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd1_output_mode~10_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd1_output_mode.LOW_BYTE~q\);

-- Location: LCCOMB_X66_Y69_N22
\sram_ctrl_a21d16b8_inst|rd1_output_mode~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd1_output_mode~9_combout\ = (\rd1~input_o\ & (!\sram_ctrl_a21d16b8_inst|state.IDLE~q\ & \rd1_access_mode~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \rd1~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \rd1_access_mode~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|rd1_output_mode~9_combout\);

-- Location: FF_X66_Y69_N23
\sram_ctrl_a21d16b8_inst|rd1_output_mode.WORD\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd1_output_mode~9_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd1_output_mode.WORD~q\);

-- Location: LCCOMB_X66_Y69_N10
\sram_ctrl_a21d16b8_inst|rd1_data~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd1_data~2_combout\ = (\sram_ctrl_a21d16b8_inst|rd1_output_mode.LOW_BYTE~q\) # (\sram_ctrl_a21d16b8_inst|rd1_output_mode.WORD~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|rd1_output_mode.LOW_BYTE~q\,
	datac => \sram_ctrl_a21d16b8_inst|rd1_output_mode.WORD~q\,
	combout => \sram_ctrl_a21d16b8_inst|rd1_data~2_combout\);

-- Location: IOIBUF_X35_Y73_N15
\sram_dq[8]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => sram_dq(8),
	o => \sram_dq[8]~input_o\);

-- Location: IOIBUF_X58_Y73_N1
\sram_dq[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => sram_dq(0),
	o => \sram_dq[0]~input_o\);

-- Location: LCCOMB_X66_Y69_N20
\sram_ctrl_a21d16b8_inst|rd1_output_mode~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd1_output_mode~8_combout\ = (\rd1_addr[0]~input_o\ & (\rd1~input_o\ & (!\sram_ctrl_a21d16b8_inst|state.IDLE~q\ & !\rd1_access_mode~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rd1_addr[0]~input_o\,
	datab => \rd1~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \rd1_access_mode~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|rd1_output_mode~8_combout\);

-- Location: FF_X66_Y69_N21
\sram_ctrl_a21d16b8_inst|rd1_output_mode.HIGH_BYTE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd1_output_mode~8_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd1_output_mode.HIGH_BYTE~q\);

-- Location: LCCOMB_X48_Y69_N8
\sram_ctrl_a21d16b8_inst|Selector11~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|Selector11~0_combout\ = (\sram_ctrl_a21d16b8_inst|rd1_data~2_combout\ & ((\sram_dq[0]~input_o\) # ((\sram_dq[8]~input_o\ & \sram_ctrl_a21d16b8_inst|rd1_output_mode.HIGH_BYTE~q\)))) # (!\sram_ctrl_a21d16b8_inst|rd1_data~2_combout\ 
-- & (\sram_dq[8]~input_o\ & ((\sram_ctrl_a21d16b8_inst|rd1_output_mode.HIGH_BYTE~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|rd1_data~2_combout\,
	datab => \sram_dq[8]~input_o\,
	datac => \sram_dq[0]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd1_output_mode.HIGH_BYTE~q\,
	combout => \sram_ctrl_a21d16b8_inst|Selector11~0_combout\);

-- Location: FF_X48_Y69_N9
\sram_ctrl_a21d16b8_inst|rd1_data[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|Selector11~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd1_data\(0));

-- Location: IOIBUF_X58_Y73_N22
\sram_dq[9]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => sram_dq(9),
	o => \sram_dq[9]~input_o\);

-- Location: IOIBUF_X49_Y73_N15
\sram_dq[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => sram_dq(1),
	o => \sram_dq[1]~input_o\);

-- Location: LCCOMB_X48_Y69_N18
\sram_ctrl_a21d16b8_inst|Selector10~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|Selector10~0_combout\ = (\sram_ctrl_a21d16b8_inst|rd1_data~2_combout\ & ((\sram_dq[1]~input_o\) # ((\sram_dq[9]~input_o\ & \sram_ctrl_a21d16b8_inst|rd1_output_mode.HIGH_BYTE~q\)))) # (!\sram_ctrl_a21d16b8_inst|rd1_data~2_combout\ 
-- & (\sram_dq[9]~input_o\ & ((\sram_ctrl_a21d16b8_inst|rd1_output_mode.HIGH_BYTE~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|rd1_data~2_combout\,
	datab => \sram_dq[9]~input_o\,
	datac => \sram_dq[1]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd1_output_mode.HIGH_BYTE~q\,
	combout => \sram_ctrl_a21d16b8_inst|Selector10~0_combout\);

-- Location: FF_X48_Y69_N19
\sram_ctrl_a21d16b8_inst|rd1_data[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|Selector10~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd1_data\(1));

-- Location: IOIBUF_X38_Y73_N8
\sram_dq[10]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => sram_dq(10),
	o => \sram_dq[10]~input_o\);

-- Location: IOIBUF_X49_Y73_N22
\sram_dq[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => sram_dq(2),
	o => \sram_dq[2]~input_o\);

-- Location: LCCOMB_X48_Y69_N28
\sram_ctrl_a21d16b8_inst|Selector9~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|Selector9~0_combout\ = (\sram_dq[10]~input_o\ & ((\sram_ctrl_a21d16b8_inst|rd1_output_mode.HIGH_BYTE~q\) # ((\sram_dq[2]~input_o\ & \sram_ctrl_a21d16b8_inst|rd1_data~2_combout\)))) # (!\sram_dq[10]~input_o\ & 
-- (((\sram_dq[2]~input_o\ & \sram_ctrl_a21d16b8_inst|rd1_data~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_dq[10]~input_o\,
	datab => \sram_ctrl_a21d16b8_inst|rd1_output_mode.HIGH_BYTE~q\,
	datac => \sram_dq[2]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd1_data~2_combout\,
	combout => \sram_ctrl_a21d16b8_inst|Selector9~0_combout\);

-- Location: FF_X48_Y69_N29
\sram_ctrl_a21d16b8_inst|rd1_data[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|Selector9~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd1_data\(2));

-- Location: IOIBUF_X58_Y73_N15
\sram_dq[11]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => sram_dq(11),
	o => \sram_dq[11]~input_o\);

-- Location: IOIBUF_X52_Y73_N22
\sram_dq[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => sram_dq(3),
	o => \sram_dq[3]~input_o\);

-- Location: LCCOMB_X48_Y69_N14
\sram_ctrl_a21d16b8_inst|Selector8~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|Selector8~0_combout\ = (\sram_ctrl_a21d16b8_inst|rd1_data~2_combout\ & ((\sram_dq[3]~input_o\) # ((\sram_dq[11]~input_o\ & \sram_ctrl_a21d16b8_inst|rd1_output_mode.HIGH_BYTE~q\)))) # (!\sram_ctrl_a21d16b8_inst|rd1_data~2_combout\ 
-- & (\sram_dq[11]~input_o\ & ((\sram_ctrl_a21d16b8_inst|rd1_output_mode.HIGH_BYTE~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|rd1_data~2_combout\,
	datab => \sram_dq[11]~input_o\,
	datac => \sram_dq[3]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd1_output_mode.HIGH_BYTE~q\,
	combout => \sram_ctrl_a21d16b8_inst|Selector8~0_combout\);

-- Location: FF_X48_Y69_N15
\sram_ctrl_a21d16b8_inst|rd1_data[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|Selector8~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd1_data\(3));

-- Location: IOIBUF_X54_Y73_N8
\sram_dq[4]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => sram_dq(4),
	o => \sram_dq[4]~input_o\);

-- Location: IOIBUF_X38_Y73_N15
\sram_dq[12]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => sram_dq(12),
	o => \sram_dq[12]~input_o\);

-- Location: LCCOMB_X48_Y69_N0
\sram_ctrl_a21d16b8_inst|Selector7~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|Selector7~0_combout\ = (\sram_ctrl_a21d16b8_inst|rd1_data~2_combout\ & ((\sram_dq[4]~input_o\) # ((\sram_dq[12]~input_o\ & \sram_ctrl_a21d16b8_inst|rd1_output_mode.HIGH_BYTE~q\)))) # (!\sram_ctrl_a21d16b8_inst|rd1_data~2_combout\ 
-- & (((\sram_dq[12]~input_o\ & \sram_ctrl_a21d16b8_inst|rd1_output_mode.HIGH_BYTE~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|rd1_data~2_combout\,
	datab => \sram_dq[4]~input_o\,
	datac => \sram_dq[12]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd1_output_mode.HIGH_BYTE~q\,
	combout => \sram_ctrl_a21d16b8_inst|Selector7~0_combout\);

-- Location: FF_X48_Y69_N1
\sram_ctrl_a21d16b8_inst|rd1_data[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|Selector7~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd1_data\(4));

-- Location: IOIBUF_X52_Y73_N15
\sram_dq[5]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => sram_dq(5),
	o => \sram_dq[5]~input_o\);

-- Location: IOIBUF_X38_Y73_N22
\sram_dq[13]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => sram_dq(13),
	o => \sram_dq[13]~input_o\);

-- Location: LCCOMB_X48_Y69_N26
\sram_ctrl_a21d16b8_inst|Selector6~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|Selector6~0_combout\ = (\sram_dq[5]~input_o\ & ((\sram_ctrl_a21d16b8_inst|rd1_data~2_combout\) # ((\sram_ctrl_a21d16b8_inst|rd1_output_mode.HIGH_BYTE~q\ & \sram_dq[13]~input_o\)))) # (!\sram_dq[5]~input_o\ & 
-- (\sram_ctrl_a21d16b8_inst|rd1_output_mode.HIGH_BYTE~q\ & (\sram_dq[13]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_dq[5]~input_o\,
	datab => \sram_ctrl_a21d16b8_inst|rd1_output_mode.HIGH_BYTE~q\,
	datac => \sram_dq[13]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd1_data~2_combout\,
	combout => \sram_ctrl_a21d16b8_inst|Selector6~0_combout\);

-- Location: FF_X48_Y69_N27
\sram_ctrl_a21d16b8_inst|rd1_data[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|Selector6~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd1_data\(5));

-- Location: IOIBUF_X38_Y73_N1
\sram_dq[14]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => sram_dq(14),
	o => \sram_dq[14]~input_o\);

-- Location: IOIBUF_X45_Y73_N8
\sram_dq[6]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => sram_dq(6),
	o => \sram_dq[6]~input_o\);

-- Location: LCCOMB_X48_Y69_N20
\sram_ctrl_a21d16b8_inst|Selector5~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|Selector5~0_combout\ = (\sram_dq[14]~input_o\ & ((\sram_ctrl_a21d16b8_inst|rd1_output_mode.HIGH_BYTE~q\) # ((\sram_dq[6]~input_o\ & \sram_ctrl_a21d16b8_inst|rd1_data~2_combout\)))) # (!\sram_dq[14]~input_o\ & 
-- (((\sram_dq[6]~input_o\ & \sram_ctrl_a21d16b8_inst|rd1_data~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_dq[14]~input_o\,
	datab => \sram_ctrl_a21d16b8_inst|rd1_output_mode.HIGH_BYTE~q\,
	datac => \sram_dq[6]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd1_data~2_combout\,
	combout => \sram_ctrl_a21d16b8_inst|Selector5~0_combout\);

-- Location: FF_X48_Y69_N21
\sram_ctrl_a21d16b8_inst|rd1_data[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|Selector5~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd1_data\(6));

-- Location: IOIBUF_X35_Y73_N22
\sram_dq[15]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => sram_dq(15),
	o => \sram_dq[15]~input_o\);

-- Location: IOIBUF_X52_Y73_N8
\sram_dq[7]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => sram_dq(7),
	o => \sram_dq[7]~input_o\);

-- Location: LCCOMB_X48_Y69_N6
\sram_ctrl_a21d16b8_inst|Selector4~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|Selector4~0_combout\ = (\sram_ctrl_a21d16b8_inst|rd1_data~2_combout\ & ((\sram_dq[7]~input_o\) # ((\sram_dq[15]~input_o\ & \sram_ctrl_a21d16b8_inst|rd1_output_mode.HIGH_BYTE~q\)))) # (!\sram_ctrl_a21d16b8_inst|rd1_data~2_combout\ 
-- & (\sram_dq[15]~input_o\ & ((\sram_ctrl_a21d16b8_inst|rd1_output_mode.HIGH_BYTE~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|rd1_data~2_combout\,
	datab => \sram_dq[15]~input_o\,
	datac => \sram_dq[7]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd1_output_mode.HIGH_BYTE~q\,
	combout => \sram_ctrl_a21d16b8_inst|Selector4~0_combout\);

-- Location: FF_X48_Y69_N7
\sram_ctrl_a21d16b8_inst|rd1_data[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|Selector4~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd1_data\(7));

-- Location: LCCOMB_X32_Y69_N8
\sram_ctrl_a21d16b8_inst|rd1_data~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd1_data~3_combout\ = (\sram_dq[8]~input_o\ & \sram_ctrl_a21d16b8_inst|rd1_output_mode.WORD~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_dq[8]~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|rd1_output_mode.WORD~q\,
	combout => \sram_ctrl_a21d16b8_inst|rd1_data~3_combout\);

-- Location: FF_X32_Y69_N9
\sram_ctrl_a21d16b8_inst|rd1_data[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd1_data~3_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd1_data\(8));

-- Location: LCCOMB_X32_Y69_N2
\sram_ctrl_a21d16b8_inst|rd1_data~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd1_data~4_combout\ = (\sram_dq[9]~input_o\ & \sram_ctrl_a21d16b8_inst|rd1_output_mode.WORD~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_dq[9]~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|rd1_output_mode.WORD~q\,
	combout => \sram_ctrl_a21d16b8_inst|rd1_data~4_combout\);

-- Location: FF_X32_Y69_N3
\sram_ctrl_a21d16b8_inst|rd1_data[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd1_data~4_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd1_data\(9));

-- Location: LCCOMB_X32_Y69_N12
\sram_ctrl_a21d16b8_inst|rd1_data~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd1_data~5_combout\ = (\sram_ctrl_a21d16b8_inst|rd1_output_mode.WORD~q\ & \sram_dq[10]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|rd1_output_mode.WORD~q\,
	datac => \sram_dq[10]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|rd1_data~5_combout\);

-- Location: FF_X32_Y69_N13
\sram_ctrl_a21d16b8_inst|rd1_data[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd1_data~5_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd1_data\(10));

-- Location: LCCOMB_X32_Y69_N14
\sram_ctrl_a21d16b8_inst|rd1_data~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd1_data~6_combout\ = (\sram_dq[11]~input_o\ & \sram_ctrl_a21d16b8_inst|rd1_output_mode.WORD~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_dq[11]~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|rd1_output_mode.WORD~q\,
	combout => \sram_ctrl_a21d16b8_inst|rd1_data~6_combout\);

-- Location: FF_X32_Y69_N15
\sram_ctrl_a21d16b8_inst|rd1_data[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd1_data~6_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd1_data\(11));

-- Location: LCCOMB_X32_Y69_N24
\sram_ctrl_a21d16b8_inst|rd1_data~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd1_data~7_combout\ = (\sram_ctrl_a21d16b8_inst|rd1_output_mode.WORD~q\ & \sram_dq[12]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|rd1_output_mode.WORD~q\,
	datac => \sram_dq[12]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|rd1_data~7_combout\);

-- Location: FF_X32_Y69_N25
\sram_ctrl_a21d16b8_inst|rd1_data[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd1_data~7_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd1_data\(12));

-- Location: LCCOMB_X32_Y69_N10
\sram_ctrl_a21d16b8_inst|rd1_data~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd1_data~8_combout\ = (\sram_ctrl_a21d16b8_inst|rd1_output_mode.WORD~q\ & \sram_dq[13]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|rd1_output_mode.WORD~q\,
	datac => \sram_dq[13]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|rd1_data~8_combout\);

-- Location: FF_X32_Y69_N11
\sram_ctrl_a21d16b8_inst|rd1_data[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd1_data~8_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd1_data\(13));

-- Location: LCCOMB_X32_Y69_N20
\sram_ctrl_a21d16b8_inst|rd1_data~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd1_data~9_combout\ = (\sram_dq[14]~input_o\ & \sram_ctrl_a21d16b8_inst|rd1_output_mode.WORD~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_dq[14]~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|rd1_output_mode.WORD~q\,
	combout => \sram_ctrl_a21d16b8_inst|rd1_data~9_combout\);

-- Location: FF_X32_Y69_N21
\sram_ctrl_a21d16b8_inst|rd1_data[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd1_data~9_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd1_data\(14));

-- Location: LCCOMB_X32_Y69_N6
\sram_ctrl_a21d16b8_inst|rd1_data~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd1_data~10_combout\ = (\sram_dq[15]~input_o\ & \sram_ctrl_a21d16b8_inst|rd1_output_mode.WORD~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_dq[15]~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|rd1_output_mode.WORD~q\,
	combout => \sram_ctrl_a21d16b8_inst|rd1_data~10_combout\);

-- Location: FF_X32_Y69_N7
\sram_ctrl_a21d16b8_inst|rd1_data[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd1_data~10_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd1_data\(15));

-- Location: LCCOMB_X66_Y69_N12
\sram_ctrl_a21d16b8_inst|rd2_addr_buffer[11]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[11]~0_combout\ = (\rd1~input_o\ & !\sram_ctrl_a21d16b8_inst|state.IDLE~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \rd1~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	combout => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[11]~0_combout\);

-- Location: FF_X66_Y69_N13
\sram_ctrl_a21d16b8_inst|rd1_output_mode.NONE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[11]~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd1_output_mode.NONE~q\);

-- Location: LCCOMB_X66_Y69_N16
\sram_ctrl_a21d16b8_inst|rd1_valid~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd1_valid~feeder_combout\ = \sram_ctrl_a21d16b8_inst|rd1_output_mode.NONE~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \sram_ctrl_a21d16b8_inst|rd1_output_mode.NONE~q\,
	combout => \sram_ctrl_a21d16b8_inst|rd1_valid~feeder_combout\);

-- Location: FF_X66_Y69_N17
\sram_ctrl_a21d16b8_inst|rd1_valid\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd1_valid~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd1_valid~q\);

-- Location: LCCOMB_X68_Y69_N2
\sram_ctrl_a21d16b8_inst|rd2_busy~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd2_busy~0_combout\ = (\sram_ctrl_a21d16b8_inst|rd2_pending~q\) # (\sram_ctrl_a21d16b8_inst|state.IDLE~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|rd2_pending~q\,
	datad => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	combout => \sram_ctrl_a21d16b8_inst|rd2_busy~0_combout\);

-- Location: IOIBUF_X67_Y73_N22
\rd2_addr[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd2_addr(0),
	o => \rd2_addr[0]~input_o\);

-- Location: LCCOMB_X68_Y69_N30
\sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]~1_combout\ = (\rd1~input_o\ & (\rd2~input_o\ & !\sram_ctrl_a21d16b8_inst|state.IDLE~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \rd1~input_o\,
	datac => \rd2~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	combout => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]~1_combout\);

-- Location: FF_X68_Y69_N13
\sram_ctrl_a21d16b8_inst|rd2_addr_buffer[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \rd2_addr[0]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(0));

-- Location: LCCOMB_X68_Y69_N14
\sram_ctrl_a21d16b8_inst|rd2_output_mode~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd2_output_mode~8_combout\ = (!\sram_ctrl_a21d16b8_inst|state.IDLE~q\ & (\rd2~input_o\ & (!\rd1~input_o\ & !\sram_ctrl_a21d16b8_inst|rd2_pending~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datab => \rd2~input_o\,
	datac => \rd1~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd2_pending~q\,
	combout => \sram_ctrl_a21d16b8_inst|rd2_output_mode~8_combout\);

-- Location: IOIBUF_X67_Y73_N1
\rd2_access_mode~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd2_access_mode,
	o => \rd2_access_mode~input_o\);

-- Location: LCCOMB_X68_Y69_N8
\sram_ctrl_a21d16b8_inst|rd2_output_mode~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd2_output_mode~9_combout\ = (\sram_ctrl_a21d16b8_inst|rd2_output_mode~8_combout\ & !\rd2_access_mode~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|rd2_output_mode~8_combout\,
	datac => \rd2_access_mode~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|rd2_output_mode~9_combout\);

-- Location: FF_X68_Y69_N19
\sram_ctrl_a21d16b8_inst|rd2_access_mode_buffer\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \rd2_access_mode~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_access_mode_buffer~q\);

-- Location: LCCOMB_X68_Y69_N18
\sram_ctrl_a21d16b8_inst|rd2_output_mode~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd2_output_mode~10_combout\ = (!\rd1~input_o\ & (!\sram_ctrl_a21d16b8_inst|rd2_access_mode_buffer~q\ & \sram_ctrl_a21d16b8_inst|rd2_pending~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \rd1~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|rd2_access_mode_buffer~q\,
	datad => \sram_ctrl_a21d16b8_inst|rd2_pending~q\,
	combout => \sram_ctrl_a21d16b8_inst|rd2_output_mode~10_combout\);

-- Location: LCCOMB_X68_Y69_N0
\sram_ctrl_a21d16b8_inst|rd2_output_mode~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd2_output_mode~11_combout\ = (\sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(0) & ((\sram_ctrl_a21d16b8_inst|rd2_output_mode~10_combout\) # ((\sram_ctrl_a21d16b8_inst|rd2_output_mode~9_combout\ & \rd2_addr[0]~input_o\)))) # 
-- (!\sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(0) & (\sram_ctrl_a21d16b8_inst|rd2_output_mode~9_combout\ & (\rd2_addr[0]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(0),
	datab => \sram_ctrl_a21d16b8_inst|rd2_output_mode~9_combout\,
	datac => \rd2_addr[0]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd2_output_mode~10_combout\,
	combout => \sram_ctrl_a21d16b8_inst|rd2_output_mode~11_combout\);

-- Location: FF_X68_Y69_N1
\sram_ctrl_a21d16b8_inst|rd2_output_mode.HIGH_BYTE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd2_output_mode~11_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_output_mode.HIGH_BYTE~q\);

-- Location: LCCOMB_X68_Y69_N28
\sram_ctrl_a21d16b8_inst|rd2_output_mode~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd2_output_mode~13_combout\ = (\sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(0) & (\sram_ctrl_a21d16b8_inst|rd2_output_mode~9_combout\ & (!\rd2_addr[0]~input_o\))) # (!\sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(0) & 
-- ((\sram_ctrl_a21d16b8_inst|rd2_output_mode~10_combout\) # ((\sram_ctrl_a21d16b8_inst|rd2_output_mode~9_combout\ & !\rd2_addr[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101110100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(0),
	datab => \sram_ctrl_a21d16b8_inst|rd2_output_mode~9_combout\,
	datac => \rd2_addr[0]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd2_output_mode~10_combout\,
	combout => \sram_ctrl_a21d16b8_inst|rd2_output_mode~13_combout\);

-- Location: FF_X68_Y69_N29
\sram_ctrl_a21d16b8_inst|rd2_output_mode.LOW_BYTE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd2_output_mode~13_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_output_mode.LOW_BYTE~q\);

-- Location: LCCOMB_X68_Y69_N20
\sram_ctrl_a21d16b8_inst|rd2_output_buffer~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd2_output_buffer~0_combout\ = (!\rd1~input_o\ & \sram_ctrl_a21d16b8_inst|rd2_pending~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \rd1~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd2_pending~q\,
	combout => \sram_ctrl_a21d16b8_inst|rd2_output_buffer~0_combout\);

-- Location: LCCOMB_X68_Y69_N10
\sram_ctrl_a21d16b8_inst|rd2_output_mode~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd2_output_mode~12_combout\ = (\rd2_access_mode~input_o\ & ((\sram_ctrl_a21d16b8_inst|rd2_output_mode~8_combout\) # ((\sram_ctrl_a21d16b8_inst|rd2_access_mode_buffer~q\ & \sram_ctrl_a21d16b8_inst|rd2_output_buffer~0_combout\)))) # 
-- (!\rd2_access_mode~input_o\ & (\sram_ctrl_a21d16b8_inst|rd2_access_mode_buffer~q\ & ((\sram_ctrl_a21d16b8_inst|rd2_output_buffer~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rd2_access_mode~input_o\,
	datab => \sram_ctrl_a21d16b8_inst|rd2_access_mode_buffer~q\,
	datac => \sram_ctrl_a21d16b8_inst|rd2_output_mode~8_combout\,
	datad => \sram_ctrl_a21d16b8_inst|rd2_output_buffer~0_combout\,
	combout => \sram_ctrl_a21d16b8_inst|rd2_output_mode~12_combout\);

-- Location: FF_X68_Y69_N11
\sram_ctrl_a21d16b8_inst|rd2_output_mode.WORD\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd2_output_mode~12_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_output_mode.WORD~q\);

-- Location: LCCOMB_X68_Y69_N22
\sram_ctrl_a21d16b8_inst|rd2_data~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd2_data~2_combout\ = (\sram_ctrl_a21d16b8_inst|rd2_output_mode.LOW_BYTE~q\) # (\sram_ctrl_a21d16b8_inst|rd2_output_mode.WORD~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|rd2_output_mode.LOW_BYTE~q\,
	datad => \sram_ctrl_a21d16b8_inst|rd2_output_mode.WORD~q\,
	combout => \sram_ctrl_a21d16b8_inst|rd2_data~2_combout\);

-- Location: LCCOMB_X48_Y69_N24
\sram_ctrl_a21d16b8_inst|Selector19~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|Selector19~0_combout\ = (\sram_ctrl_a21d16b8_inst|rd2_output_mode.HIGH_BYTE~q\ & ((\sram_dq[8]~input_o\) # ((\sram_dq[0]~input_o\ & \sram_ctrl_a21d16b8_inst|rd2_data~2_combout\)))) # 
-- (!\sram_ctrl_a21d16b8_inst|rd2_output_mode.HIGH_BYTE~q\ & (((\sram_dq[0]~input_o\ & \sram_ctrl_a21d16b8_inst|rd2_data~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|rd2_output_mode.HIGH_BYTE~q\,
	datab => \sram_dq[8]~input_o\,
	datac => \sram_dq[0]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd2_data~2_combout\,
	combout => \sram_ctrl_a21d16b8_inst|Selector19~0_combout\);

-- Location: FF_X48_Y69_N25
\sram_ctrl_a21d16b8_inst|rd2_data[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|Selector19~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_data\(0));

-- Location: LCCOMB_X48_Y69_N2
\sram_ctrl_a21d16b8_inst|Selector18~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|Selector18~0_combout\ = (\sram_ctrl_a21d16b8_inst|rd2_output_mode.HIGH_BYTE~q\ & ((\sram_dq[9]~input_o\) # ((\sram_dq[1]~input_o\ & \sram_ctrl_a21d16b8_inst|rd2_data~2_combout\)))) # 
-- (!\sram_ctrl_a21d16b8_inst|rd2_output_mode.HIGH_BYTE~q\ & (((\sram_dq[1]~input_o\ & \sram_ctrl_a21d16b8_inst|rd2_data~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|rd2_output_mode.HIGH_BYTE~q\,
	datab => \sram_dq[9]~input_o\,
	datac => \sram_dq[1]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd2_data~2_combout\,
	combout => \sram_ctrl_a21d16b8_inst|Selector18~0_combout\);

-- Location: FF_X48_Y69_N3
\sram_ctrl_a21d16b8_inst|rd2_data[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|Selector18~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_data\(1));

-- Location: LCCOMB_X48_Y69_N12
\sram_ctrl_a21d16b8_inst|Selector17~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|Selector17~0_combout\ = (\sram_dq[10]~input_o\ & ((\sram_ctrl_a21d16b8_inst|rd2_output_mode.HIGH_BYTE~q\) # ((\sram_ctrl_a21d16b8_inst|rd2_data~2_combout\ & \sram_dq[2]~input_o\)))) # (!\sram_dq[10]~input_o\ & 
-- (\sram_ctrl_a21d16b8_inst|rd2_data~2_combout\ & (\sram_dq[2]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_dq[10]~input_o\,
	datab => \sram_ctrl_a21d16b8_inst|rd2_data~2_combout\,
	datac => \sram_dq[2]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd2_output_mode.HIGH_BYTE~q\,
	combout => \sram_ctrl_a21d16b8_inst|Selector17~0_combout\);

-- Location: FF_X48_Y69_N13
\sram_ctrl_a21d16b8_inst|rd2_data[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|Selector17~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_data\(2));

-- Location: LCCOMB_X48_Y69_N30
\sram_ctrl_a21d16b8_inst|Selector16~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|Selector16~0_combout\ = (\sram_ctrl_a21d16b8_inst|rd2_output_mode.HIGH_BYTE~q\ & ((\sram_dq[11]~input_o\) # ((\sram_dq[3]~input_o\ & \sram_ctrl_a21d16b8_inst|rd2_data~2_combout\)))) # 
-- (!\sram_ctrl_a21d16b8_inst|rd2_output_mode.HIGH_BYTE~q\ & (((\sram_dq[3]~input_o\ & \sram_ctrl_a21d16b8_inst|rd2_data~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|rd2_output_mode.HIGH_BYTE~q\,
	datab => \sram_dq[11]~input_o\,
	datac => \sram_dq[3]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd2_data~2_combout\,
	combout => \sram_ctrl_a21d16b8_inst|Selector16~0_combout\);

-- Location: FF_X48_Y69_N31
\sram_ctrl_a21d16b8_inst|rd2_data[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|Selector16~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_data\(3));

-- Location: LCCOMB_X48_Y69_N16
\sram_ctrl_a21d16b8_inst|Selector15~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|Selector15~0_combout\ = (\sram_ctrl_a21d16b8_inst|rd2_output_mode.HIGH_BYTE~q\ & ((\sram_dq[12]~input_o\) # ((\sram_dq[4]~input_o\ & \sram_ctrl_a21d16b8_inst|rd2_data~2_combout\)))) # 
-- (!\sram_ctrl_a21d16b8_inst|rd2_output_mode.HIGH_BYTE~q\ & (\sram_dq[4]~input_o\ & ((\sram_ctrl_a21d16b8_inst|rd2_data~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|rd2_output_mode.HIGH_BYTE~q\,
	datab => \sram_dq[4]~input_o\,
	datac => \sram_dq[12]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd2_data~2_combout\,
	combout => \sram_ctrl_a21d16b8_inst|Selector15~0_combout\);

-- Location: FF_X48_Y69_N17
\sram_ctrl_a21d16b8_inst|rd2_data[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|Selector15~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_data\(4));

-- Location: LCCOMB_X48_Y69_N10
\sram_ctrl_a21d16b8_inst|Selector14~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|Selector14~0_combout\ = (\sram_dq[5]~input_o\ & ((\sram_ctrl_a21d16b8_inst|rd2_data~2_combout\) # ((\sram_dq[13]~input_o\ & \sram_ctrl_a21d16b8_inst|rd2_output_mode.HIGH_BYTE~q\)))) # (!\sram_dq[5]~input_o\ & 
-- (((\sram_dq[13]~input_o\ & \sram_ctrl_a21d16b8_inst|rd2_output_mode.HIGH_BYTE~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_dq[5]~input_o\,
	datab => \sram_ctrl_a21d16b8_inst|rd2_data~2_combout\,
	datac => \sram_dq[13]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd2_output_mode.HIGH_BYTE~q\,
	combout => \sram_ctrl_a21d16b8_inst|Selector14~0_combout\);

-- Location: FF_X48_Y69_N11
\sram_ctrl_a21d16b8_inst|rd2_data[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|Selector14~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_data\(5));

-- Location: LCCOMB_X48_Y69_N4
\sram_ctrl_a21d16b8_inst|Selector13~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|Selector13~0_combout\ = (\sram_dq[14]~input_o\ & ((\sram_ctrl_a21d16b8_inst|rd2_output_mode.HIGH_BYTE~q\) # ((\sram_ctrl_a21d16b8_inst|rd2_data~2_combout\ & \sram_dq[6]~input_o\)))) # (!\sram_dq[14]~input_o\ & 
-- (\sram_ctrl_a21d16b8_inst|rd2_data~2_combout\ & (\sram_dq[6]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_dq[14]~input_o\,
	datab => \sram_ctrl_a21d16b8_inst|rd2_data~2_combout\,
	datac => \sram_dq[6]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd2_output_mode.HIGH_BYTE~q\,
	combout => \sram_ctrl_a21d16b8_inst|Selector13~0_combout\);

-- Location: FF_X48_Y69_N5
\sram_ctrl_a21d16b8_inst|rd2_data[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|Selector13~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_data\(6));

-- Location: LCCOMB_X48_Y69_N22
\sram_ctrl_a21d16b8_inst|Selector12~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|Selector12~0_combout\ = (\sram_ctrl_a21d16b8_inst|rd2_output_mode.HIGH_BYTE~q\ & ((\sram_dq[15]~input_o\) # ((\sram_dq[7]~input_o\ & \sram_ctrl_a21d16b8_inst|rd2_data~2_combout\)))) # 
-- (!\sram_ctrl_a21d16b8_inst|rd2_output_mode.HIGH_BYTE~q\ & (((\sram_dq[7]~input_o\ & \sram_ctrl_a21d16b8_inst|rd2_data~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|rd2_output_mode.HIGH_BYTE~q\,
	datab => \sram_dq[15]~input_o\,
	datac => \sram_dq[7]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd2_data~2_combout\,
	combout => \sram_ctrl_a21d16b8_inst|Selector12~0_combout\);

-- Location: FF_X48_Y69_N23
\sram_ctrl_a21d16b8_inst|rd2_data[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|Selector12~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_data\(7));

-- Location: LCCOMB_X32_Y69_N0
\sram_ctrl_a21d16b8_inst|rd2_data~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd2_data~3_combout\ = (\sram_dq[8]~input_o\ & \sram_ctrl_a21d16b8_inst|rd2_output_mode.WORD~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_dq[8]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd2_output_mode.WORD~q\,
	combout => \sram_ctrl_a21d16b8_inst|rd2_data~3_combout\);

-- Location: FF_X32_Y69_N1
\sram_ctrl_a21d16b8_inst|rd2_data[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd2_data~3_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_data\(8));

-- Location: LCCOMB_X32_Y69_N18
\sram_ctrl_a21d16b8_inst|rd2_data~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd2_data~4_combout\ = (\sram_dq[9]~input_o\ & \sram_ctrl_a21d16b8_inst|rd2_output_mode.WORD~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_dq[9]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd2_output_mode.WORD~q\,
	combout => \sram_ctrl_a21d16b8_inst|rd2_data~4_combout\);

-- Location: FF_X32_Y69_N19
\sram_ctrl_a21d16b8_inst|rd2_data[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd2_data~4_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_data\(9));

-- Location: LCCOMB_X32_Y69_N28
\sram_ctrl_a21d16b8_inst|rd2_data~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd2_data~5_combout\ = (\sram_dq[10]~input_o\ & \sram_ctrl_a21d16b8_inst|rd2_output_mode.WORD~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \sram_dq[10]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd2_output_mode.WORD~q\,
	combout => \sram_ctrl_a21d16b8_inst|rd2_data~5_combout\);

-- Location: FF_X32_Y69_N29
\sram_ctrl_a21d16b8_inst|rd2_data[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd2_data~5_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_data\(10));

-- Location: LCCOMB_X32_Y69_N30
\sram_ctrl_a21d16b8_inst|rd2_data~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd2_data~6_combout\ = (\sram_dq[11]~input_o\ & \sram_ctrl_a21d16b8_inst|rd2_output_mode.WORD~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_dq[11]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd2_output_mode.WORD~q\,
	combout => \sram_ctrl_a21d16b8_inst|rd2_data~6_combout\);

-- Location: FF_X32_Y69_N31
\sram_ctrl_a21d16b8_inst|rd2_data[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd2_data~6_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_data\(11));

-- Location: LCCOMB_X32_Y69_N16
\sram_ctrl_a21d16b8_inst|rd2_data~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd2_data~7_combout\ = (\sram_dq[12]~input_o\ & \sram_ctrl_a21d16b8_inst|rd2_output_mode.WORD~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \sram_dq[12]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd2_output_mode.WORD~q\,
	combout => \sram_ctrl_a21d16b8_inst|rd2_data~7_combout\);

-- Location: FF_X32_Y69_N17
\sram_ctrl_a21d16b8_inst|rd2_data[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd2_data~7_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_data\(12));

-- Location: LCCOMB_X32_Y69_N26
\sram_ctrl_a21d16b8_inst|rd2_data~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd2_data~8_combout\ = (\sram_dq[13]~input_o\ & \sram_ctrl_a21d16b8_inst|rd2_output_mode.WORD~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \sram_dq[13]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd2_output_mode.WORD~q\,
	combout => \sram_ctrl_a21d16b8_inst|rd2_data~8_combout\);

-- Location: FF_X32_Y69_N27
\sram_ctrl_a21d16b8_inst|rd2_data[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd2_data~8_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_data\(13));

-- Location: LCCOMB_X32_Y69_N4
\sram_ctrl_a21d16b8_inst|rd2_data~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd2_data~9_combout\ = (\sram_dq[14]~input_o\ & \sram_ctrl_a21d16b8_inst|rd2_output_mode.WORD~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_dq[14]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd2_output_mode.WORD~q\,
	combout => \sram_ctrl_a21d16b8_inst|rd2_data~9_combout\);

-- Location: FF_X32_Y69_N5
\sram_ctrl_a21d16b8_inst|rd2_data[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd2_data~9_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_data\(14));

-- Location: LCCOMB_X32_Y69_N22
\sram_ctrl_a21d16b8_inst|rd2_data~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd2_data~10_combout\ = (\sram_dq[15]~input_o\ & \sram_ctrl_a21d16b8_inst|rd2_output_mode.WORD~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_dq[15]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd2_output_mode.WORD~q\,
	combout => \sram_ctrl_a21d16b8_inst|rd2_data~10_combout\);

-- Location: FF_X32_Y69_N23
\sram_ctrl_a21d16b8_inst|rd2_data[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd2_data~10_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_data\(15));

-- Location: LCCOMB_X68_Y69_N16
\sram_ctrl_a21d16b8_inst|rd2_output_mode~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd2_output_mode~14_combout\ = (!\rd1~input_o\ & ((\sram_ctrl_a21d16b8_inst|rd2_pending~q\) # ((!\sram_ctrl_a21d16b8_inst|state.IDLE~q\ & \rd2~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datab => \rd1~input_o\,
	datac => \rd2~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd2_pending~q\,
	combout => \sram_ctrl_a21d16b8_inst|rd2_output_mode~14_combout\);

-- Location: FF_X68_Y69_N17
\sram_ctrl_a21d16b8_inst|rd2_output_mode.NONE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd2_output_mode~14_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_output_mode.NONE~q\);

-- Location: LCCOMB_X68_Y69_N4
\sram_ctrl_a21d16b8_inst|rd2_valid~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|rd2_valid~feeder_combout\ = \sram_ctrl_a21d16b8_inst|rd2_output_mode.NONE~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \sram_ctrl_a21d16b8_inst|rd2_output_mode.NONE~q\,
	combout => \sram_ctrl_a21d16b8_inst|rd2_valid~feeder_combout\);

-- Location: FF_X68_Y69_N5
\sram_ctrl_a21d16b8_inst|rd2_valid\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|rd2_valid~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_valid~q\);

-- Location: IOIBUF_X109_Y73_N1
\rd1_addr[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd1_addr(1),
	o => \rd1_addr[1]~input_o\);

-- Location: IOIBUF_X113_Y73_N1
\rd2_addr[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd2_addr(1),
	o => \rd2_addr[1]~input_o\);

-- Location: LCCOMB_X66_Y69_N14
\sram_ctrl_a21d16b8_inst|sram_addr[6]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ = (\rd1~input_o\) # ((\sram_ctrl_a21d16b8_inst|state.IDLE~q\) # ((\sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rd1~input_o\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\);

-- Location: FF_X74_Y66_N25
\sram_ctrl_a21d16b8_inst|rd2_addr_buffer[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \rd2_addr[1]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(1));

-- Location: LCCOMB_X66_Y69_N18
\sram_ctrl_a21d16b8_inst|sram_addr[6]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ = (\sram_ctrl_a21d16b8_inst|Selector2~1_combout\) # ((\rd2~input_o\ & !\rd1~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \rd2~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|Selector2~1_combout\,
	datad => \rd1~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\);

-- Location: LCCOMB_X74_Y66_N24
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[0]~40\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[0]~40_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & (((\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\)))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & (\rd2_addr[1]~input_o\)) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & ((\sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(1))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rd2_addr[1]~input_o\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	datac => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(1),
	datad => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[0]~40_combout\);

-- Location: IOIBUF_X98_Y73_N15
\wr_addr[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_addr(1),
	o => \wr_addr[1]~input_o\);

-- Location: LCCOMB_X79_Y63_N18
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~157feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~157feeder_combout\ = \wr_addr[1]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[1]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~157feeder_combout\);

-- Location: FF_X79_Y63_N19
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~157\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~157feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~157_q\);

-- Location: LCCOMB_X79_Y63_N16
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~81feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~81feeder_combout\ = \wr_addr[1]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[1]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~81feeder_combout\);

-- Location: FF_X79_Y63_N17
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~81\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~81feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~81_q\);

-- Location: FF_X75_Y63_N3
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~43\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[1]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~43_q\);

-- Location: LCCOMB_X75_Y63_N2
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~348\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~348_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~81_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~43_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~81_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~43_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~348_combout\);

-- Location: FF_X75_Y63_N1
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~119\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[1]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~119_q\);

-- Location: LCCOMB_X75_Y63_N0
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~349\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~349_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~348_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~157_q\) # ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~348_combout\ 
-- & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~119_q\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~157_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~348_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~119_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~349_combout\);

-- Location: LCCOMB_X74_Y60_N8
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~233feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~233feeder_combout\ = \wr_addr[1]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[1]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~233feeder_combout\);

-- Location: FF_X74_Y60_N9
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~233\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~233feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~233_q\);

-- Location: FF_X74_Y60_N19
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~195\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[1]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~195_q\);

-- Location: LCCOMB_X74_Y60_N18
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~346\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~346_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~233_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~195_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~233_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~195_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~346_combout\);

-- Location: LCCOMB_X74_Y64_N8
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~271feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~271feeder_combout\ = \wr_addr[1]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[1]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~271feeder_combout\);

-- Location: FF_X74_Y64_N9
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~271\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~271feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~271_q\);

-- Location: FF_X74_Y64_N27
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~309\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[1]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~309_q\);

-- Location: LCCOMB_X74_Y64_N26
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~347\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~347_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~346_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~309_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~346_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~271_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~346_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~271_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~309_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~347_combout\);

-- Location: LCCOMB_X74_Y66_N2
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~350\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~350_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~347_combout\))) 
-- # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~349_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~349_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~347_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~350_combout\);

-- Location: FF_X74_Y66_N3
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~350_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(1));

-- Location: LCCOMB_X74_Y66_N4
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[0]~41\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[0]~41_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[0]~40_combout\ & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(1)) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\)))) 
-- # (!\sram_ctrl_a21d16b8_inst|sram_addr_nxt[0]~40_combout\ & (\rd1_addr[1]~input_o\ & (\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110000101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rd1_addr[1]~input_o\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[0]~40_combout\,
	datac => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(1),
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[0]~41_combout\);

-- Location: LCCOMB_X73_Y66_N24
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[0]~80\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[0]~80_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[0]~41_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\) # ((\sram_ctrl_a21d16b8_inst|state.IDLE~q\) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[0]~41_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[0]~80_combout\);

-- Location: FF_X73_Y66_N25
\sram_ctrl_a21d16b8_inst|sram_addr[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[0]~80_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_addr\(0));

-- Location: IOIBUF_X60_Y73_N1
\wr_addr[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_addr(2),
	o => \wr_addr[2]~input_o\);

-- Location: LCCOMB_X74_Y60_N4
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~234feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~234feeder_combout\ = \wr_addr[2]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[2]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~234feeder_combout\);

-- Location: FF_X74_Y60_N5
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~234\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~234feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~234_q\);

-- Location: FF_X74_Y60_N23
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~196\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[2]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~196_q\);

-- Location: LCCOMB_X74_Y60_N22
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~351\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~351_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~234_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~196_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~234_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~196_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~351_combout\);

-- Location: FF_X74_Y64_N21
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~272\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[2]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~272_q\);

-- Location: FF_X74_Y64_N7
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~310\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[2]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~310_q\);

-- Location: LCCOMB_X74_Y64_N6
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~352\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~352_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~351_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~310_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~351_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~272_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~351_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~272_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~310_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~352_combout\);

-- Location: FF_X75_Y63_N5
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~120\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[2]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~120_q\);

-- Location: FF_X75_Y63_N31
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~44\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[2]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~44_q\);

-- Location: LCCOMB_X75_Y63_N30
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~353\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~353_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~120_q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~44_q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~120_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~44_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~353_combout\);

-- Location: FF_X75_Y62_N11
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~158\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[2]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~158_q\);

-- Location: FF_X75_Y62_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~82\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[2]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~82_q\);

-- Location: LCCOMB_X75_Y62_N24
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~354\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~354_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~353_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~158_q\) # ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~353_combout\ 
-- & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~82_q\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~353_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~158_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~82_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~354_combout\);

-- Location: LCCOMB_X74_Y66_N16
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~355\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~355_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~352_combout\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~354_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~352_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~354_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~355_combout\);

-- Location: FF_X74_Y66_N17
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~355_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(2));

-- Location: IOIBUF_X113_Y73_N8
\rd2_addr[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd2_addr(2),
	o => \rd2_addr[2]~input_o\);

-- Location: FF_X74_Y66_N7
\sram_ctrl_a21d16b8_inst|rd2_addr_buffer[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \rd2_addr[2]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(2));

-- Location: IOIBUF_X85_Y73_N15
\rd1_addr[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd1_addr(2),
	o => \rd1_addr[2]~input_o\);

-- Location: LCCOMB_X74_Y66_N6
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[1]~42\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[1]~42_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & (\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\)) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & ((\rd1_addr[2]~input_o\))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & (\sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	datac => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(2),
	datad => \rd1_addr[2]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[1]~42_combout\);

-- Location: LCCOMB_X74_Y66_N18
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[1]~43\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[1]~43_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & ((\sram_ctrl_a21d16b8_inst|sram_addr_nxt[1]~42_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(2))) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr_nxt[1]~42_combout\ & ((\rd2_addr[2]~input_o\))))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & (((\sram_ctrl_a21d16b8_inst|sram_addr_nxt[1]~42_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(2),
	datac => \rd2_addr[2]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[1]~42_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[1]~43_combout\);

-- Location: LCCOMB_X73_Y66_N18
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[1]~81\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[1]~81_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[1]~43_combout\ & (((\sram_ctrl_a21d16b8_inst|state.IDLE~q\) # (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[1]~43_combout\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[1]~81_combout\);

-- Location: FF_X73_Y66_N19
\sram_ctrl_a21d16b8_inst|sram_addr[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[1]~81_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_addr\(1));

-- Location: IOIBUF_X74_Y73_N22
\wr_addr[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_addr(3),
	o => \wr_addr[3]~input_o\);

-- Location: FF_X75_Y62_N7
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~159\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[3]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~159_q\);

-- Location: FF_X75_Y63_N17
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~121\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[3]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~121_q\);

-- Location: FF_X75_Y63_N19
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~45\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[3]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~45_q\);

-- Location: LCCOMB_X75_Y63_N18
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~358\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~358_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~121_q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~45_q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~121_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~45_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~358_combout\);

-- Location: FF_X75_Y62_N21
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~83\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[3]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~83_q\);

-- Location: LCCOMB_X75_Y62_N20
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~359\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~359_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~358_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~159_q\) # ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~358_combout\ 
-- & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~83_q\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~159_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~358_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~83_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~359_combout\);

-- Location: LCCOMB_X74_Y60_N16
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~235feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~235feeder_combout\ = \wr_addr[3]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[3]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~235feeder_combout\);

-- Location: FF_X74_Y60_N17
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~235\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~235feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~235_q\);

-- Location: FF_X74_Y60_N27
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~197\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[3]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~197_q\);

-- Location: LCCOMB_X74_Y60_N26
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~356\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~356_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~235_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~197_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~235_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~197_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~356_combout\);

-- Location: FF_X74_Y64_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~273\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[3]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~273_q\);

-- Location: FF_X74_Y64_N19
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~311\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[3]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~311_q\);

-- Location: LCCOMB_X74_Y64_N18
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~357\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~357_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~356_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~311_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~356_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~273_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~356_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~273_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~311_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~357_combout\);

-- Location: LCCOMB_X74_Y66_N30
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~360\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~360_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~357_combout\))) 
-- # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~359_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~359_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~357_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~360_combout\);

-- Location: FF_X74_Y66_N31
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~360_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(3));

-- Location: IOIBUF_X115_Y49_N1
\rd2_addr[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd2_addr(3),
	o => \rd2_addr[3]~input_o\);

-- Location: FF_X74_Y66_N29
\sram_ctrl_a21d16b8_inst|rd2_addr_buffer[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \rd2_addr[3]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(3));

-- Location: LCCOMB_X74_Y66_N28
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[2]~44\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[2]~44_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & (((\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\)))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & (\rd2_addr[3]~input_o\)) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & ((\sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(3))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rd2_addr[3]~input_o\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	datac => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(3),
	datad => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[2]~44_combout\);

-- Location: IOIBUF_X111_Y73_N1
\rd1_addr[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd1_addr(3),
	o => \rd1_addr[3]~input_o\);

-- Location: LCCOMB_X74_Y66_N8
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[2]~45\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[2]~45_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[2]~44_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(3)) # ((!\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\)))) 
-- # (!\sram_ctrl_a21d16b8_inst|sram_addr_nxt[2]~44_combout\ & (((\rd1_addr[3]~input_o\ & \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(3),
	datab => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[2]~44_combout\,
	datac => \rd1_addr[3]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[2]~45_combout\);

-- Location: LCCOMB_X73_Y66_N12
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[2]~82\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[2]~82_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[2]~45_combout\ & (((\sram_ctrl_a21d16b8_inst|state.IDLE~q\) # (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[2]~45_combout\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[2]~82_combout\);

-- Location: FF_X73_Y66_N13
\sram_ctrl_a21d16b8_inst|sram_addr[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[2]~82_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_addr\(2));

-- Location: IOIBUF_X89_Y73_N22
\wr_addr[4]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_addr(4),
	o => \wr_addr[4]~input_o\);

-- Location: FF_X74_Y64_N13
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~274\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[4]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~274_q\);

-- Location: LCCOMB_X74_Y60_N12
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~236feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~236feeder_combout\ = \wr_addr[4]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[4]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~236feeder_combout\);

-- Location: FF_X74_Y60_N13
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~236\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~236feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~236_q\);

-- Location: FF_X74_Y60_N15
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~198\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[4]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~198_q\);

-- Location: LCCOMB_X74_Y60_N14
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~361\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~361_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~236_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~198_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~236_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~198_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~361_combout\);

-- Location: FF_X74_Y64_N23
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~312\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[4]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~312_q\);

-- Location: LCCOMB_X74_Y64_N22
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~362\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~362_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~361_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~312_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~361_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~274_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~274_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~361_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~312_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~362_combout\);

-- Location: FF_X75_Y63_N21
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~122\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[4]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~122_q\);

-- Location: FF_X75_Y63_N7
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~46\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[4]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~46_q\);

-- Location: LCCOMB_X75_Y63_N6
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~363\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~363_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~122_q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~46_q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~122_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~46_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~363_combout\);

-- Location: FF_X79_Y63_N5
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~84\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[4]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~84_q\);

-- Location: FF_X79_Y63_N7
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~160\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[4]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~160_q\);

-- Location: LCCOMB_X79_Y63_N4
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~364\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~364_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~363_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~160_q\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~363_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~84_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~363_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~84_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~160_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~364_combout\);

-- Location: LCCOMB_X74_Y66_N20
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~365\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~365_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~362_combout\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~364_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110010101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~362_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~364_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~365_combout\);

-- Location: FF_X74_Y66_N21
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~365_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(4));

-- Location: IOIBUF_X105_Y73_N1
\rd2_addr[4]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd2_addr(4),
	o => \rd2_addr[4]~input_o\);

-- Location: FF_X74_Y66_N11
\sram_ctrl_a21d16b8_inst|rd2_addr_buffer[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \rd2_addr[4]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(4));

-- Location: IOIBUF_X79_Y73_N8
\rd1_addr[4]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd1_addr(4),
	o => \rd1_addr[4]~input_o\);

-- Location: LCCOMB_X74_Y66_N10
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[3]~46\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[3]~46_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & (\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\)) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & ((\rd1_addr[4]~input_o\))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & (\sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	datac => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(4),
	datad => \rd1_addr[4]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[3]~46_combout\);

-- Location: LCCOMB_X74_Y66_N14
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[3]~47\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[3]~47_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & ((\sram_ctrl_a21d16b8_inst|sram_addr_nxt[3]~46_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(4))) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr_nxt[3]~46_combout\ & ((\rd2_addr[4]~input_o\))))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & (((\sram_ctrl_a21d16b8_inst|sram_addr_nxt[3]~46_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(4),
	datac => \rd2_addr[4]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[3]~46_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[3]~47_combout\);

-- Location: LCCOMB_X73_Y66_N6
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[3]~83\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[3]~83_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[3]~47_combout\ & (((\sram_ctrl_a21d16b8_inst|state.IDLE~q\) # (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[3]~47_combout\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[3]~83_combout\);

-- Location: FF_X73_Y66_N7
\sram_ctrl_a21d16b8_inst|sram_addr[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[3]~83_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_addr\(3));

-- Location: IOIBUF_X89_Y73_N15
\wr_addr[5]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_addr(5),
	o => \wr_addr[5]~input_o\);

-- Location: LCCOMB_X75_Y63_N8
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~123feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~123feeder_combout\ = \wr_addr[5]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[5]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~123feeder_combout\);

-- Location: FF_X75_Y63_N9
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~123\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~123feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~123_q\);

-- Location: FF_X75_Y63_N11
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~47\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[5]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~47_q\);

-- Location: LCCOMB_X75_Y63_N10
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~368\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~368_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~123_q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~47_q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~123_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~47_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~368_combout\);

-- Location: LCCOMB_X76_Y63_N26
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~161feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~161feeder_combout\ = \wr_addr[5]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[5]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~161feeder_combout\);

-- Location: FF_X76_Y63_N27
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~161\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~161feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~161_q\);

-- Location: FF_X80_Y63_N3
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~85\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[5]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~85_q\);

-- Location: LCCOMB_X80_Y63_N2
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~369\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~369_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~368_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~161_q\) # ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~368_combout\ 
-- & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~85_q\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~368_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~161_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~85_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~369_combout\);

-- Location: FF_X74_Y60_N1
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~237\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[5]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~237_q\);

-- Location: FF_X74_Y60_N3
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~199\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[5]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~199_q\);

-- Location: LCCOMB_X74_Y60_N2
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~366\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~366_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~237_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~199_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~237_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~199_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~366_combout\);

-- Location: LCCOMB_X74_Y64_N0
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~275feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~275feeder_combout\ = \wr_addr[5]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[5]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~275feeder_combout\);

-- Location: FF_X74_Y64_N1
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~275\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~275feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~275_q\);

-- Location: FF_X74_Y64_N11
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~313\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[5]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~313_q\);

-- Location: LCCOMB_X74_Y64_N10
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~367\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~367_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~366_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~313_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~366_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~275_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~366_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~275_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~313_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~367_combout\);

-- Location: LCCOMB_X74_Y66_N26
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~370\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~370_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~367_combout\))) 
-- # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~369_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~369_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~367_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~370_combout\);

-- Location: FF_X74_Y66_N27
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~370_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(5));

-- Location: IOIBUF_X81_Y73_N8
\rd1_addr[5]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd1_addr(5),
	o => \rd1_addr[5]~input_o\);

-- Location: IOIBUF_X107_Y73_N8
\rd2_addr[5]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd2_addr(5),
	o => \rd2_addr[5]~input_o\);

-- Location: FF_X74_Y66_N1
\sram_ctrl_a21d16b8_inst|rd2_addr_buffer[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \rd2_addr[5]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(5));

-- Location: LCCOMB_X74_Y66_N0
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[4]~48\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[4]~48_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & ((\rd2_addr[5]~input_o\) # ((\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\)))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(5) & !\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	datab => \rd2_addr[5]~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(5),
	datad => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[4]~48_combout\);

-- Location: LCCOMB_X74_Y66_N12
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[4]~49\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[4]~49_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & ((\sram_ctrl_a21d16b8_inst|sram_addr_nxt[4]~48_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(5))) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr_nxt[4]~48_combout\ & ((\rd1_addr[5]~input_o\))))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & (((\sram_ctrl_a21d16b8_inst|sram_addr_nxt[4]~48_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(5),
	datab => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	datac => \rd1_addr[5]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[4]~48_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[4]~49_combout\);

-- Location: LCCOMB_X73_Y66_N16
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[4]~84\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[4]~84_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[4]~49_combout\ & (((\sram_ctrl_a21d16b8_inst|state.IDLE~q\) # (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[4]~49_combout\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[4]~84_combout\);

-- Location: FF_X73_Y66_N17
\sram_ctrl_a21d16b8_inst|sram_addr[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[4]~84_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_addr\(4));

-- Location: IOIBUF_X58_Y73_N8
\rd2_addr[6]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd2_addr(6),
	o => \rd2_addr[6]~input_o\);

-- Location: IOIBUF_X62_Y73_N22
\rd1_addr[6]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd1_addr(6),
	o => \rd1_addr[6]~input_o\);

-- Location: FF_X66_Y69_N29
\sram_ctrl_a21d16b8_inst|rd2_addr_buffer[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \rd2_addr[6]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(6));

-- Location: LCCOMB_X66_Y69_N28
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[5]~50\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[5]~50_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & ((\rd1_addr[6]~input_o\) # ((\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\)))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(6) & !\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rd1_addr[6]~input_o\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	datac => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(6),
	datad => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[5]~50_combout\);

-- Location: IOIBUF_X115_Y59_N15
\wr_addr[6]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_addr(6),
	o => \wr_addr[6]~input_o\);

-- Location: FF_X79_Y63_N11
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~162\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[6]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~162_q\);

-- Location: FF_X75_Y63_N29
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~124\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[6]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~124_q\);

-- Location: FF_X75_Y63_N23
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~48\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[6]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~48_q\);

-- Location: LCCOMB_X75_Y63_N22
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~373\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~373_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~124_q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~48_q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~124_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~48_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~373_combout\);

-- Location: FF_X79_Y63_N1
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~86\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[6]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~86_q\);

-- Location: LCCOMB_X79_Y63_N0
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~374\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~374_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~373_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~162_q\) # ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~373_combout\ 
-- & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~86_q\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~162_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~373_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~86_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~374_combout\);

-- Location: LCCOMB_X74_Y60_N20
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~238feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~238feeder_combout\ = \wr_addr[6]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[6]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~238feeder_combout\);

-- Location: FF_X74_Y60_N21
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~238\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~238feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~238_q\);

-- Location: FF_X74_Y60_N31
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~200\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[6]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~200_q\);

-- Location: LCCOMB_X74_Y60_N30
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~371\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~371_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~238_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~200_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~238_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~200_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~371_combout\);

-- Location: LCCOMB_X74_Y64_N28
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~276feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~276feeder_combout\ = \wr_addr[6]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[6]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~276feeder_combout\);

-- Location: FF_X74_Y64_N29
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~276\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~276feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~276_q\);

-- Location: FF_X74_Y64_N31
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~314\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[6]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~314_q\);

-- Location: LCCOMB_X74_Y64_N30
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~372\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~372_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~371_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~314_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~371_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~276_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~371_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~276_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~314_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~372_combout\);

-- Location: LCCOMB_X76_Y63_N2
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~375\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~375_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~372_combout\))) 
-- # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~374_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~374_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~372_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~375_combout\);

-- Location: FF_X76_Y63_N3
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~375_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(6));

-- Location: LCCOMB_X66_Y69_N30
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[5]~51\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[5]~51_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[5]~50_combout\ & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(6)) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\)))) 
-- # (!\sram_ctrl_a21d16b8_inst|sram_addr_nxt[5]~50_combout\ & (\rd2_addr[6]~input_o\ & ((\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rd2_addr[6]~input_o\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[5]~50_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(6),
	datad => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[5]~51_combout\);

-- Location: LCCOMB_X66_Y69_N2
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[5]~85\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[5]~85_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[5]~51_combout\ & (((\sram_ctrl_a21d16b8_inst|state.IDLE~q\) # (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[5]~51_combout\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[5]~85_combout\);

-- Location: FF_X66_Y69_N3
\sram_ctrl_a21d16b8_inst|sram_addr[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[5]~85_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_addr\(5));

-- Location: IOIBUF_X115_Y48_N1
\rd2_addr[7]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd2_addr(7),
	o => \rd2_addr[7]~input_o\);

-- Location: FF_X74_Y63_N17
\sram_ctrl_a21d16b8_inst|rd2_addr_buffer[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \rd2_addr[7]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(7));

-- Location: LCCOMB_X74_Y63_N16
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[6]~52\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[6]~52_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & (((\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\)))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & (\rd2_addr[7]~input_o\)) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & ((\sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(7))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	datab => \rd2_addr[7]~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(7),
	datad => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[6]~52_combout\);

-- Location: IOIBUF_X115_Y63_N8
\rd1_addr[7]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd1_addr(7),
	o => \rd1_addr[7]~input_o\);

-- Location: IOIBUF_X115_Y64_N1
\wr_addr[7]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_addr(7),
	o => \wr_addr[7]~input_o\);

-- Location: FF_X75_Y63_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~125\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[7]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~125_q\);

-- Location: FF_X75_Y63_N27
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~49\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[7]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~49_q\);

-- Location: LCCOMB_X75_Y63_N26
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~378\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~378_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~125_q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~49_q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~125_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~49_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~378_combout\);

-- Location: FF_X75_Y62_N3
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~163\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[7]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~163_q\);

-- Location: FF_X75_Y62_N9
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~87\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[7]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~87_q\);

-- Location: LCCOMB_X75_Y62_N8
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~379\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~379_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~378_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~163_q\) # ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~378_combout\ 
-- & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~87_q\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~378_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~163_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~87_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~379_combout\);

-- Location: FF_X74_Y60_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~239\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[7]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~239_q\);

-- Location: FF_X74_Y60_N11
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~201\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[7]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~201_q\);

-- Location: LCCOMB_X74_Y60_N10
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~376\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~376_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~239_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~201_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~239_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~201_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~376_combout\);

-- Location: FF_X74_Y64_N17
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~277\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[7]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~277_q\);

-- Location: FF_X74_Y64_N3
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~315\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[7]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~315_q\);

-- Location: LCCOMB_X74_Y64_N2
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~377\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~377_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~376_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~315_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~376_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~277_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~376_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~277_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~315_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~377_combout\);

-- Location: LCCOMB_X74_Y63_N2
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~380\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~380_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~377_combout\))) 
-- # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~379_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~379_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~377_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~380_combout\);

-- Location: FF_X74_Y63_N3
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~380_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(7));

-- Location: LCCOMB_X74_Y63_N12
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[6]~53\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[6]~53_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & ((\sram_ctrl_a21d16b8_inst|sram_addr_nxt[6]~52_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(7)))) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr_nxt[6]~52_combout\ & (\rd1_addr[7]~input_o\)))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[6]~52_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[6]~52_combout\,
	datac => \rd1_addr[7]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(7),
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[6]~53_combout\);

-- Location: LCCOMB_X66_Y69_N4
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[6]~86\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[6]~86_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[6]~53_combout\ & (((\sram_ctrl_a21d16b8_inst|state.IDLE~q\) # (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[6]~53_combout\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[6]~86_combout\);

-- Location: FF_X66_Y69_N5
\sram_ctrl_a21d16b8_inst|sram_addr[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[6]~86_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_addr\(6));

-- Location: IOIBUF_X74_Y0_N1
\rd1_addr[8]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd1_addr(8),
	o => \rd1_addr[8]~input_o\);

-- Location: IOIBUF_X52_Y73_N1
\rd2_addr[8]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd2_addr(8),
	o => \rd2_addr[8]~input_o\);

-- Location: FF_X74_Y63_N15
\sram_ctrl_a21d16b8_inst|rd2_addr_buffer[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \rd2_addr[8]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(8));

-- Location: LCCOMB_X74_Y63_N14
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[7]~54\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[7]~54_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & ((\rd1_addr[8]~input_o\) # ((\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\)))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(8) & !\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	datab => \rd1_addr[8]~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(8),
	datad => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[7]~54_combout\);

-- Location: IOIBUF_X115_Y61_N15
\wr_addr[8]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_addr(8),
	o => \wr_addr[8]~input_o\);

-- Location: FF_X72_Y63_N11
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~202\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[8]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~202_q\);

-- Location: LCCOMB_X72_Y63_N0
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~240feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~240feeder_combout\ = \wr_addr[8]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[8]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~240feeder_combout\);

-- Location: FF_X72_Y63_N1
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~240\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~240feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~240_q\);

-- Location: LCCOMB_X72_Y63_N10
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~381\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~381_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~240_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~202_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~202_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~240_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~381_combout\);

-- Location: FF_X73_Y63_N17
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~278\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[8]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~278_q\);

-- Location: FF_X73_Y63_N11
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~316\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[8]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~316_q\);

-- Location: LCCOMB_X73_Y63_N10
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~382\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~382_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~381_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~316_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~381_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~278_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~381_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~278_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~316_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~382_combout\);

-- Location: FF_X74_Y61_N11
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~164\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[8]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~164_q\);

-- Location: FF_X75_Y61_N17
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~126\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[8]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~126_q\);

-- Location: FF_X75_Y61_N19
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~50\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[8]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~50_q\);

-- Location: LCCOMB_X75_Y61_N18
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~383\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~383_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~126_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~50_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~126_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~50_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~383_combout\);

-- Location: FF_X74_Y61_N9
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~88\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[8]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~88_q\);

-- Location: LCCOMB_X74_Y61_N8
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~384\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~384_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~383_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~164_q\) # ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~383_combout\ 
-- & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~88_q\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~164_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~383_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~88_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~384_combout\);

-- Location: LCCOMB_X74_Y63_N24
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~385\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~385_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~382_combout\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~384_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~382_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~384_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~385_combout\);

-- Location: FF_X74_Y63_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~385_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(8));

-- Location: LCCOMB_X74_Y63_N18
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[7]~55\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[7]~55_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & ((\sram_ctrl_a21d16b8_inst|sram_addr_nxt[7]~54_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(8)))) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr_nxt[7]~54_combout\ & (\rd2_addr[8]~input_o\)))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[7]~54_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[7]~54_combout\,
	datac => \rd2_addr[8]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(8),
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[7]~55_combout\);

-- Location: LCCOMB_X73_Y66_N10
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[7]~87\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[7]~87_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[7]~55_combout\ & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\) # (\sram_ctrl_a21d16b8_inst|state.IDLE~q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[7]~55_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[7]~87_combout\);

-- Location: FF_X73_Y66_N11
\sram_ctrl_a21d16b8_inst|sram_addr[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[7]~87_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_addr\(7));

-- Location: IOIBUF_X72_Y73_N1
\wr_addr[9]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_addr(9),
	o => \wr_addr[9]~input_o\);

-- Location: FF_X72_Y63_N23
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~203\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[9]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~203_q\);

-- Location: FF_X72_Y63_N13
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~241\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[9]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~241_q\);

-- Location: LCCOMB_X72_Y63_N22
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~386\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~386_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~241_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~203_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~203_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~241_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~386_combout\);

-- Location: FF_X73_Y63_N15
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~317\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[9]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~317_q\);

-- Location: LCCOMB_X73_Y63_N28
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~279feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~279feeder_combout\ = \wr_addr[9]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[9]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~279feeder_combout\);

-- Location: FF_X73_Y63_N29
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~279\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~279feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~279_q\);

-- Location: LCCOMB_X73_Y63_N14
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~387\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~387_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~386_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~317_q\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~386_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~279_q\))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~386_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~386_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~317_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~279_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~387_combout\);

-- Location: FF_X75_Y61_N31
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~51\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[9]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~51_q\);

-- Location: LCCOMB_X75_Y61_N12
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~127feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~127feeder_combout\ = \wr_addr[9]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[9]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~127feeder_combout\);

-- Location: FF_X75_Y61_N13
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~127\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~127feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~127_q\);

-- Location: LCCOMB_X75_Y61_N30
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~388\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~388_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~127_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~51_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~51_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~127_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~388_combout\);

-- Location: FF_X74_Y61_N29
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~89\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[9]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~89_q\);

-- Location: LCCOMB_X74_Y61_N6
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~165feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~165feeder_combout\ = \wr_addr[9]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[9]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~165feeder_combout\);

-- Location: FF_X74_Y61_N7
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~165\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~165feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~165_q\);

-- Location: LCCOMB_X74_Y61_N28
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~389\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~389_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~388_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~165_q\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~388_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~89_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~388_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~89_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~165_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~389_combout\);

-- Location: LCCOMB_X74_Y63_N30
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~390\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~390_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~387_combout\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~389_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~387_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~389_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~390_combout\);

-- Location: FF_X74_Y63_N31
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~390_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(9));

-- Location: IOIBUF_X98_Y73_N22
\rd2_addr[9]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd2_addr(9),
	o => \rd2_addr[9]~input_o\);

-- Location: FF_X74_Y63_N29
\sram_ctrl_a21d16b8_inst|rd2_addr_buffer[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \rd2_addr[9]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(9));

-- Location: LCCOMB_X74_Y63_N28
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[8]~56\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[8]~56_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & (((\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\)))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & (\rd2_addr[9]~input_o\)) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & ((\sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(9))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	datab => \rd2_addr[9]~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(9),
	datad => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[8]~56_combout\);

-- Location: IOIBUF_X115_Y68_N22
\rd1_addr[9]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd1_addr(9),
	o => \rd1_addr[9]~input_o\);

-- Location: LCCOMB_X74_Y63_N8
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[8]~57\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[8]~57_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[8]~56_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(9)) # ((!\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\)))) 
-- # (!\sram_ctrl_a21d16b8_inst|sram_addr_nxt[8]~56_combout\ & (((\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & \rd1_addr[9]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(9),
	datab => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[8]~56_combout\,
	datac => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	datad => \rd1_addr[9]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[8]~57_combout\);

-- Location: LCCOMB_X73_Y66_N4
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[8]~88\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[8]~88_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[8]~57_combout\ & (((\sram_ctrl_a21d16b8_inst|state.IDLE~q\) # (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[8]~57_combout\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[8]~88_combout\);

-- Location: FF_X73_Y66_N5
\sram_ctrl_a21d16b8_inst|sram_addr[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[8]~88_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_addr\(8));

-- Location: IOIBUF_X72_Y73_N22
\wr_addr[10]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_addr(10),
	o => \wr_addr[10]~input_o\);

-- Location: FF_X74_Y61_N3
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~166\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[10]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~166_q\);

-- Location: FF_X75_Y61_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~128\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[10]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~128_q\);

-- Location: FF_X75_Y61_N27
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~52\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[10]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~52_q\);

-- Location: LCCOMB_X75_Y61_N26
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~393\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~393_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~128_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~52_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~128_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~52_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~393_combout\);

-- Location: FF_X74_Y61_N1
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~90\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[10]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~90_q\);

-- Location: LCCOMB_X74_Y61_N0
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~394\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~394_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~393_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~166_q\) # ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~393_combout\ 
-- & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~90_q\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~166_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~393_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~90_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~394_combout\);

-- Location: FF_X72_Y63_N3
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~204\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[10]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~204_q\);

-- Location: FF_X72_Y63_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~242\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[10]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~242_q\);

-- Location: LCCOMB_X72_Y63_N2
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~391\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~391_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~242_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~204_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~204_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~242_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~391_combout\);

-- Location: FF_X73_Y63_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~280\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[10]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~280_q\);

-- Location: FF_X73_Y63_N19
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~318\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[10]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~318_q\);

-- Location: LCCOMB_X73_Y63_N18
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~392\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~392_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~391_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~318_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~391_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~280_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~391_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~280_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~318_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~392_combout\);

-- Location: LCCOMB_X74_Y62_N0
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~395\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~395_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~392_combout\))) 
-- # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~394_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~394_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~392_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~395_combout\);

-- Location: FF_X74_Y62_N1
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~395_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(10));

-- Location: IOIBUF_X74_Y0_N15
\rd2_addr[10]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd2_addr(10),
	o => \rd2_addr[10]~input_o\);

-- Location: IOIBUF_X115_Y63_N1
\rd1_addr[10]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd1_addr(10),
	o => \rd1_addr[10]~input_o\);

-- Location: FF_X74_Y63_N11
\sram_ctrl_a21d16b8_inst|rd2_addr_buffer[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \rd2_addr[10]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(10));

-- Location: LCCOMB_X74_Y63_N10
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[9]~58\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[9]~58_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & ((\rd1_addr[10]~input_o\) # ((\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\)))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(10) & !\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	datab => \rd1_addr[10]~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(10),
	datad => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[9]~58_combout\);

-- Location: LCCOMB_X74_Y63_N4
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[9]~59\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[9]~59_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & ((\sram_ctrl_a21d16b8_inst|sram_addr_nxt[9]~58_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(10))) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr_nxt[9]~58_combout\ & ((\rd2_addr[10]~input_o\))))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & (((\sram_ctrl_a21d16b8_inst|sram_addr_nxt[9]~58_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(10),
	datac => \rd2_addr[10]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[9]~58_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[9]~59_combout\);

-- Location: LCCOMB_X73_Y66_N14
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[9]~89\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[9]~89_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[9]~59_combout\ & (((\sram_ctrl_a21d16b8_inst|state.IDLE~q\) # (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[9]~59_combout\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[9]~89_combout\);

-- Location: FF_X73_Y66_N15
\sram_ctrl_a21d16b8_inst|sram_addr[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[9]~89_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_addr\(9));

-- Location: IOIBUF_X115_Y67_N15
\rd2_addr[11]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd2_addr(11),
	o => \rd2_addr[11]~input_o\);

-- Location: FF_X74_Y63_N23
\sram_ctrl_a21d16b8_inst|rd2_addr_buffer[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \rd2_addr[11]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(11));

-- Location: LCCOMB_X74_Y63_N22
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[10]~60\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[10]~60_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & (((\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\)))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & (\rd2_addr[11]~input_o\)) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & ((\sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(11))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rd2_addr[11]~input_o\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	datac => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(11),
	datad => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[10]~60_combout\);

-- Location: IOIBUF_X115_Y61_N22
\wr_addr[11]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_addr(11),
	o => \wr_addr[11]~input_o\);

-- Location: FF_X74_Y61_N15
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~167\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[11]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~167_q\);

-- Location: FF_X75_Y61_N21
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~129\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[11]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~129_q\);

-- Location: FF_X75_Y61_N23
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~53\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[11]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~53_q\);

-- Location: LCCOMB_X75_Y61_N22
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~398\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~398_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~129_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~53_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~129_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~53_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~398_combout\);

-- Location: FF_X74_Y61_N5
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~91\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[11]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~91_q\);

-- Location: LCCOMB_X74_Y61_N4
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~399\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~399_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~398_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~167_q\) # ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~398_combout\ 
-- & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~91_q\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~167_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~398_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~91_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~399_combout\);

-- Location: FF_X73_Y63_N13
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~281\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[11]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~281_q\);

-- Location: FF_X72_Y63_N7
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~205\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[11]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~205_q\);

-- Location: LCCOMB_X72_Y63_N4
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~243feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~243feeder_combout\ = \wr_addr[11]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[11]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~243feeder_combout\);

-- Location: FF_X72_Y63_N5
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~243\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~243feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~243_q\);

-- Location: LCCOMB_X72_Y63_N6
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~396\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~396_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~243_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~205_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~205_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~243_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~396_combout\);

-- Location: FF_X73_Y63_N7
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~319\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[11]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~319_q\);

-- Location: LCCOMB_X73_Y63_N6
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~397\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~397_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~396_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~319_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~396_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~281_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~281_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~396_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~319_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~397_combout\);

-- Location: LCCOMB_X74_Y63_N0
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~400\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~400_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~397_combout\))) 
-- # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~399_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~399_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~397_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~400_combout\);

-- Location: FF_X74_Y63_N1
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~400_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(11));

-- Location: IOIBUF_X102_Y73_N1
\rd1_addr[11]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd1_addr(11),
	o => \rd1_addr[11]~input_o\);

-- Location: LCCOMB_X74_Y63_N26
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[10]~61\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[10]~61_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[10]~60_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(11)) # 
-- ((!\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\)))) # (!\sram_ctrl_a21d16b8_inst|sram_addr_nxt[10]~60_combout\ & (((\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & \rd1_addr[11]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101010001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[10]~60_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(11),
	datac => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	datad => \rd1_addr[11]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[10]~61_combout\);

-- Location: LCCOMB_X73_Y66_N0
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[10]~90\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[10]~90_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[10]~61_combout\ & (((\sram_ctrl_a21d16b8_inst|state.IDLE~q\) # (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[10]~61_combout\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[10]~90_combout\);

-- Location: FF_X73_Y66_N1
\sram_ctrl_a21d16b8_inst|sram_addr[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[10]~90_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_addr\(10));

-- Location: IOIBUF_X87_Y73_N15
\wr_addr[12]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_addr(12),
	o => \wr_addr[12]~input_o\);

-- Location: LCCOMB_X79_Y63_N22
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~168feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~168feeder_combout\ = \wr_addr[12]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[12]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~168feeder_combout\);

-- Location: FF_X79_Y63_N23
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~168\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~168feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~168_q\);

-- Location: FF_X79_Y63_N29
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~92\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[12]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~92_q\);

-- Location: LCCOMB_X75_Y63_N12
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~130feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~130feeder_combout\ = \wr_addr[12]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[12]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~130feeder_combout\);

-- Location: FF_X75_Y63_N13
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~130\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~130feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~130_q\);

-- Location: FF_X75_Y63_N15
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~54\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[12]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~54_q\);

-- Location: LCCOMB_X75_Y63_N14
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~403\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~403_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~130_q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~54_q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~130_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~54_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~403_combout\);

-- Location: LCCOMB_X79_Y63_N28
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~404\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~404_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~403_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~168_q\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~403_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~92_q\))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~403_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~168_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~92_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~403_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~404_combout\);

-- Location: FF_X73_Y63_N9
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~282\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[12]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~282_q\);

-- Location: FF_X73_Y63_N27
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~320\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[12]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~320_q\);

-- Location: FF_X72_Y63_N19
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~206\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[12]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~206_q\);

-- Location: LCCOMB_X72_Y63_N8
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~244feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~244feeder_combout\ = \wr_addr[12]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[12]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~244feeder_combout\);

-- Location: FF_X72_Y63_N9
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~244\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~244feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~244_q\);

-- Location: LCCOMB_X72_Y63_N18
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~401\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~401_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~244_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~206_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~206_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~244_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~401_combout\);

-- Location: LCCOMB_X73_Y63_N26
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~402\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~402_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~401_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~320_q\))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~401_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~282_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~401_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~282_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~320_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~401_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~402_combout\);

-- Location: LCCOMB_X76_Y63_N4
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~405\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~405_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~402_combout\))) 
-- # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~404_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~404_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~402_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~405_combout\);

-- Location: FF_X76_Y63_N5
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~405_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(12));

-- Location: IOIBUF_X115_Y46_N8
\rd1_addr[12]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd1_addr(12),
	o => \rd1_addr[12]~input_o\);

-- Location: IOIBUF_X115_Y55_N22
\rd2_addr[12]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd2_addr(12),
	o => \rd2_addr[12]~input_o\);

-- Location: FF_X74_Y63_N21
\sram_ctrl_a21d16b8_inst|rd2_addr_buffer[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \rd2_addr[12]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(12));

-- Location: LCCOMB_X74_Y63_N20
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[11]~62\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[11]~62_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & ((\rd1_addr[12]~input_o\) # ((\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\)))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(12) & !\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	datab => \rd1_addr[12]~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(12),
	datad => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[11]~62_combout\);

-- Location: LCCOMB_X74_Y63_N6
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[11]~63\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[11]~63_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[11]~62_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(12)) # 
-- ((!\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\)))) # (!\sram_ctrl_a21d16b8_inst|sram_addr_nxt[11]~62_combout\ & (((\rd2_addr[12]~input_o\ & \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(12),
	datab => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[11]~62_combout\,
	datac => \rd2_addr[12]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[11]~63_combout\);

-- Location: LCCOMB_X73_Y66_N2
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[11]~91\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[11]~91_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[11]~63_combout\ & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\) # (\sram_ctrl_a21d16b8_inst|state.IDLE~q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[11]~63_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[11]~91_combout\);

-- Location: FF_X73_Y66_N3
\sram_ctrl_a21d16b8_inst|sram_addr[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[11]~91_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_addr\(11));

-- Location: IOIBUF_X115_Y62_N15
\rd1_addr[13]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd1_addr(13),
	o => \rd1_addr[13]~input_o\);

-- Location: IOIBUF_X115_Y55_N15
\rd2_addr[13]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd2_addr(13),
	o => \rd2_addr[13]~input_o\);

-- Location: FF_X74_Y62_N27
\sram_ctrl_a21d16b8_inst|rd2_addr_buffer[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \rd2_addr[13]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(13));

-- Location: LCCOMB_X74_Y62_N26
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[12]~64\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[12]~64_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & ((\rd2_addr[13]~input_o\) # ((\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\)))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(13) & !\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rd2_addr[13]~input_o\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	datac => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(13),
	datad => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[12]~64_combout\);

-- Location: IOIBUF_X115_Y59_N22
\wr_addr[13]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_addr(13),
	o => \wr_addr[13]~input_o\);

-- Location: LCCOMB_X75_Y61_N0
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~131feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~131feeder_combout\ = \wr_addr[13]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[13]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~131feeder_combout\);

-- Location: FF_X75_Y61_N1
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~131\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~131feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~131_q\);

-- Location: FF_X75_Y61_N3
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~55\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[13]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~55_q\);

-- Location: LCCOMB_X75_Y61_N2
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~408\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~408_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~131_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~55_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~131_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~55_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~408_combout\);

-- Location: LCCOMB_X75_Y62_N14
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~169feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~169feeder_combout\ = \wr_addr[13]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[13]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~169feeder_combout\);

-- Location: FF_X75_Y62_N15
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~169\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~169feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~169_q\);

-- Location: FF_X75_Y62_N5
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~93\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[13]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~93_q\);

-- Location: LCCOMB_X75_Y62_N4
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~409\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~409_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~408_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~169_q\) # ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~408_combout\ 
-- & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~93_q\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~408_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~169_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~93_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~409_combout\);

-- Location: FF_X74_Y60_N29
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~245\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[13]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~245_q\);

-- Location: FF_X74_Y60_N7
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~207\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[13]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~207_q\);

-- Location: LCCOMB_X74_Y60_N6
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~406\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~406_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~245_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~207_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~245_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~207_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~406_combout\);

-- Location: FF_X74_Y64_N5
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~283\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[13]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~283_q\);

-- Location: FF_X74_Y64_N15
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~321\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[13]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~321_q\);

-- Location: LCCOMB_X74_Y64_N14
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~407\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~407_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~406_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~321_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~406_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~283_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~406_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~283_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~321_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~407_combout\);

-- Location: LCCOMB_X74_Y62_N12
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~410\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~410_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~407_combout\))) 
-- # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~409_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~409_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~407_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~410_combout\);

-- Location: FF_X74_Y62_N13
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~410_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(13));

-- Location: LCCOMB_X74_Y62_N14
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[12]~65\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[12]~65_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & ((\sram_ctrl_a21d16b8_inst|sram_addr_nxt[12]~64_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(13)))) 
-- # (!\sram_ctrl_a21d16b8_inst|sram_addr_nxt[12]~64_combout\ & (\rd1_addr[13]~input_o\)))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & (((\sram_ctrl_a21d16b8_inst|sram_addr_nxt[12]~64_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100001011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	datab => \rd1_addr[13]~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[12]~64_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(13),
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[12]~65_combout\);

-- Location: LCCOMB_X73_Y66_N20
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[12]~92\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[12]~92_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[12]~65_combout\ & (((\sram_ctrl_a21d16b8_inst|state.IDLE~q\) # (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[12]~65_combout\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[12]~92_combout\);

-- Location: FF_X73_Y66_N21
\sram_ctrl_a21d16b8_inst|sram_addr[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[12]~92_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_addr\(12));

-- Location: IOIBUF_X87_Y73_N8
\wr_addr[14]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_addr(14),
	o => \wr_addr[14]~input_o\);

-- Location: FF_X72_Y63_N15
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~208\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[14]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~208_q\);

-- Location: FF_X72_Y63_N29
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~246\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[14]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~246_q\);

-- Location: LCCOMB_X72_Y63_N14
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~411\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~411_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~246_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~208_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~208_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~246_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~411_combout\);

-- Location: FF_X73_Y63_N5
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~284\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[14]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~284_q\);

-- Location: FF_X73_Y63_N23
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~322\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[14]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~322_q\);

-- Location: LCCOMB_X73_Y63_N22
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~412\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~412_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~411_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~322_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~411_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~284_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~411_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~284_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~322_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~412_combout\);

-- Location: LCCOMB_X75_Y61_N28
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~132feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~132feeder_combout\ = \wr_addr[14]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[14]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~132feeder_combout\);

-- Location: FF_X75_Y61_N29
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~132\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~132feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~132_q\);

-- Location: FF_X75_Y61_N7
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~56\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[14]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~56_q\);

-- Location: LCCOMB_X75_Y61_N6
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~413\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~413_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~132_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~56_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~132_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~56_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~413_combout\);

-- Location: LCCOMB_X74_Y61_N18
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~170feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~170feeder_combout\ = \wr_addr[14]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[14]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~170feeder_combout\);

-- Location: FF_X74_Y61_N19
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~170\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~170feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~170_q\);

-- Location: FF_X74_Y61_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~94\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[14]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~94_q\);

-- Location: LCCOMB_X74_Y61_N24
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~414\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~414_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~413_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~170_q\) # ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~413_combout\ 
-- & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~94_q\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~413_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~170_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~94_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~414_combout\);

-- Location: LCCOMB_X74_Y65_N26
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~415\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~415_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~412_combout\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~414_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~412_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~414_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~415_combout\);

-- Location: FF_X74_Y65_N27
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~415_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(14));

-- Location: IOIBUF_X105_Y73_N8
\rd1_addr[14]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd1_addr(14),
	o => \rd1_addr[14]~input_o\);

-- Location: IOIBUF_X85_Y73_N8
\rd2_addr[14]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd2_addr(14),
	o => \rd2_addr[14]~input_o\);

-- Location: FF_X74_Y65_N17
\sram_ctrl_a21d16b8_inst|rd2_addr_buffer[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \rd2_addr[14]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(14));

-- Location: LCCOMB_X74_Y65_N16
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[13]~66\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[13]~66_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & (((\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\)))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & (\rd1_addr[14]~input_o\)) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & ((\sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(14))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	datab => \rd1_addr[14]~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(14),
	datad => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[13]~66_combout\);

-- Location: LCCOMB_X74_Y65_N12
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[13]~67\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[13]~67_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[13]~66_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(14)) # 
-- ((!\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\)))) # (!\sram_ctrl_a21d16b8_inst|sram_addr_nxt[13]~66_combout\ & (((\rd2_addr[14]~input_o\ & \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(14),
	datab => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[13]~66_combout\,
	datac => \rd2_addr[14]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[13]~67_combout\);

-- Location: LCCOMB_X73_Y66_N22
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[13]~93\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[13]~93_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[13]~67_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\) # ((\sram_ctrl_a21d16b8_inst|state.IDLE~q\) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[13]~67_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[13]~93_combout\);

-- Location: FF_X73_Y66_N23
\sram_ctrl_a21d16b8_inst|sram_addr[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[13]~93_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_addr\(13));

-- Location: IOIBUF_X69_Y73_N22
\rd2_addr[15]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd2_addr(15),
	o => \rd2_addr[15]~input_o\);

-- Location: FF_X74_Y62_N25
\sram_ctrl_a21d16b8_inst|rd2_addr_buffer[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \rd2_addr[15]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(15));

-- Location: LCCOMB_X74_Y62_N24
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[14]~68\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[14]~68_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & ((\rd2_addr[15]~input_o\) # ((\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\)))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(15) & !\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	datab => \rd2_addr[15]~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(15),
	datad => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[14]~68_combout\);

-- Location: IOIBUF_X115_Y46_N1
\rd1_addr[15]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd1_addr(15),
	o => \rd1_addr[15]~input_o\);

-- Location: IOIBUF_X115_Y51_N8
\wr_addr[15]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_addr(15),
	o => \wr_addr[15]~input_o\);

-- Location: FF_X72_Y63_N27
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~209\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[15]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~209_q\);

-- Location: LCCOMB_X72_Y63_N16
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~247feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~247feeder_combout\ = \wr_addr[15]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[15]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~247feeder_combout\);

-- Location: FF_X72_Y63_N17
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~247\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~247feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~247_q\);

-- Location: LCCOMB_X72_Y63_N26
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~416\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~416_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~247_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~209_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~209_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~247_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~416_combout\);

-- Location: FF_X73_Y63_N1
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~285\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[15]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~285_q\);

-- Location: FF_X73_Y63_N3
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~323\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[15]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~323_q\);

-- Location: LCCOMB_X73_Y63_N2
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~417\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~417_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~416_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~323_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~416_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~285_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~416_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~285_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~323_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~417_combout\);

-- Location: LCCOMB_X74_Y61_N22
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~171feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~171feeder_combout\ = \wr_addr[15]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[15]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~171feeder_combout\);

-- Location: FF_X74_Y61_N23
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~171\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~171feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~171_q\);

-- Location: FF_X75_Y61_N9
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~133\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[15]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~133_q\);

-- Location: FF_X75_Y61_N11
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~57\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[15]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~57_q\);

-- Location: LCCOMB_X75_Y61_N10
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~418\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~418_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~133_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~57_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~133_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~57_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~418_combout\);

-- Location: FF_X74_Y61_N21
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~95\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[15]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~95_q\);

-- Location: LCCOMB_X74_Y61_N20
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~419\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~419_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~418_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~171_q\) # ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~418_combout\ 
-- & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~95_q\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~171_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~418_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~95_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~419_combout\);

-- Location: LCCOMB_X74_Y62_N18
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~420\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~420_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~417_combout\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~419_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~417_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~419_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~420_combout\);

-- Location: FF_X74_Y62_N19
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~420_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(15));

-- Location: LCCOMB_X74_Y62_N4
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[14]~69\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[14]~69_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & ((\sram_ctrl_a21d16b8_inst|sram_addr_nxt[14]~68_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(15)))) 
-- # (!\sram_ctrl_a21d16b8_inst|sram_addr_nxt[14]~68_combout\ & (\rd1_addr[15]~input_o\)))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[14]~68_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[14]~68_combout\,
	datac => \rd1_addr[15]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(15),
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[14]~69_combout\);

-- Location: LCCOMB_X66_Y69_N6
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[14]~94\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[14]~94_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[14]~69_combout\ & (((\sram_ctrl_a21d16b8_inst|state.IDLE~q\) # (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[14]~69_combout\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[14]~94_combout\);

-- Location: FF_X66_Y69_N7
\sram_ctrl_a21d16b8_inst|sram_addr[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[14]~94_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_addr\(14));

-- Location: IOIBUF_X74_Y73_N15
\wr_addr[16]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_addr(16),
	o => \wr_addr[16]~input_o\);

-- Location: FF_X74_Y61_N27
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~172\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[16]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~172_q\);

-- Location: FF_X75_Y61_N5
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~134\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[16]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~134_q\);

-- Location: FF_X75_Y61_N15
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~58\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[16]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~58_q\);

-- Location: LCCOMB_X75_Y61_N14
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~423\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~423_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~134_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~58_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~134_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~58_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~423_combout\);

-- Location: FF_X74_Y61_N17
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~96\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[16]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~96_q\);

-- Location: LCCOMB_X74_Y61_N16
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~424\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~424_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~423_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~172_q\) # ((!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~423_combout\ 
-- & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~96_q\ & \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~172_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~423_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~96_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~424_combout\);

-- Location: FF_X72_Y63_N31
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~210\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[16]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~210_q\);

-- Location: LCCOMB_X72_Y63_N20
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~248feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~248feeder_combout\ = \wr_addr[16]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[16]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~248feeder_combout\);

-- Location: FF_X72_Y63_N21
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~248\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~248feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~248_q\);

-- Location: LCCOMB_X72_Y63_N30
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~421\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~421_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~248_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~210_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~210_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~248_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~421_combout\);

-- Location: FF_X73_Y63_N31
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~324\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[16]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~324_q\);

-- Location: FF_X73_Y63_N21
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~286\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[16]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~286_q\);

-- Location: LCCOMB_X73_Y63_N30
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~422\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~422_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~421_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~324_q\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~421_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~286_q\))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~421_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~421_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~324_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~286_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~422_combout\);

-- Location: LCCOMB_X74_Y65_N24
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~425\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~425_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~422_combout\))) 
-- # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~424_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~424_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~422_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~425_combout\);

-- Location: FF_X74_Y65_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~425_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(16));

-- Location: IOIBUF_X18_Y73_N15
\rd2_addr[16]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd2_addr(16),
	o => \rd2_addr[16]~input_o\);

-- Location: IOIBUF_X94_Y73_N8
\rd1_addr[16]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd1_addr(16),
	o => \rd1_addr[16]~input_o\);

-- Location: FF_X74_Y65_N7
\sram_ctrl_a21d16b8_inst|rd2_addr_buffer[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \rd2_addr[16]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(16));

-- Location: LCCOMB_X74_Y65_N6
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[15]~70\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[15]~70_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & (((\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\)))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & (\rd1_addr[16]~input_o\)) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & ((\sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(16))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	datab => \rd1_addr[16]~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(16),
	datad => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[15]~70_combout\);

-- Location: LCCOMB_X74_Y65_N10
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[15]~71\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[15]~71_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & ((\sram_ctrl_a21d16b8_inst|sram_addr_nxt[15]~70_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(16))) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr_nxt[15]~70_combout\ & ((\rd2_addr[16]~input_o\))))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & (((\sram_ctrl_a21d16b8_inst|sram_addr_nxt[15]~70_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(16),
	datac => \rd2_addr[16]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[15]~70_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[15]~71_combout\);

-- Location: LCCOMB_X66_Y69_N0
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[15]~95\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[15]~95_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[15]~71_combout\ & (((\sram_ctrl_a21d16b8_inst|state.IDLE~q\) # (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[15]~71_combout\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[15]~95_combout\);

-- Location: FF_X66_Y69_N1
\sram_ctrl_a21d16b8_inst|sram_addr[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[15]~95_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_addr\(15));

-- Location: IOIBUF_X81_Y73_N22
\wr_addr[17]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_addr(17),
	o => \wr_addr[17]~input_o\);

-- Location: FF_X74_Y61_N31
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~173\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[17]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~173_q\);

-- Location: FF_X74_Y61_N13
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~97\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[17]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~97_q\);

-- Location: FF_X80_Y61_N11
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~59\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[17]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~59_q\);

-- Location: FF_X80_Y61_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~135\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[17]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~135_q\);

-- Location: LCCOMB_X80_Y61_N10
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~428\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~428_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~135_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~59_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~59_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~135_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~428_combout\);

-- Location: LCCOMB_X74_Y61_N12
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~429\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~429_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~428_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~173_q\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~428_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~97_q\))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~428_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~173_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~97_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~428_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~429_combout\);

-- Location: FF_X75_Y60_N17
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~249\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[17]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~249_q\);

-- Location: FF_X75_Y60_N11
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~211\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[17]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~211_q\);

-- Location: LCCOMB_X75_Y60_N10
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~426\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~426_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~249_q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~211_q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~249_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~211_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~426_combout\);

-- Location: FF_X76_Y60_N17
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~287\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[17]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~287_q\);

-- Location: FF_X76_Y60_N27
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~325\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[17]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~325_q\);

-- Location: LCCOMB_X76_Y60_N26
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~427\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~427_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~426_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~325_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~426_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~287_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~426_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~287_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~325_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~427_combout\);

-- Location: LCCOMB_X74_Y62_N8
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~430\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~430_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~427_combout\))) 
-- # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~429_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~429_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~427_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~430_combout\);

-- Location: FF_X74_Y62_N9
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~430_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(17));

-- Location: IOIBUF_X115_Y51_N1
\rd1_addr[17]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd1_addr(17),
	o => \rd1_addr[17]~input_o\);

-- Location: IOIBUF_X115_Y50_N8
\rd2_addr[17]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd2_addr(17),
	o => \rd2_addr[17]~input_o\);

-- Location: FF_X74_Y62_N7
\sram_ctrl_a21d16b8_inst|rd2_addr_buffer[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \rd2_addr[17]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(17));

-- Location: LCCOMB_X74_Y62_N6
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[16]~72\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[16]~72_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & ((\rd2_addr[17]~input_o\) # ((\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\)))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(17) & !\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	datab => \rd2_addr[17]~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(17),
	datad => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[16]~72_combout\);

-- Location: LCCOMB_X74_Y62_N2
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[16]~73\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[16]~73_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & ((\sram_ctrl_a21d16b8_inst|sram_addr_nxt[16]~72_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(17))) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr_nxt[16]~72_combout\ & ((\rd1_addr[17]~input_o\))))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & (((\sram_ctrl_a21d16b8_inst|sram_addr_nxt[16]~72_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(17),
	datac => \rd1_addr[17]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[16]~72_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[16]~73_combout\);

-- Location: LCCOMB_X66_Y69_N26
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[16]~96\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[16]~96_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[16]~73_combout\ & (((\sram_ctrl_a21d16b8_inst|state.IDLE~q\) # (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[16]~73_combout\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[16]~96_combout\);

-- Location: FF_X66_Y69_N27
\sram_ctrl_a21d16b8_inst|sram_addr[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[16]~96_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_addr\(16));

-- Location: IOIBUF_X100_Y73_N15
\rd1_addr[18]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd1_addr(18),
	o => \rd1_addr[18]~input_o\);

-- Location: IOIBUF_X79_Y73_N1
\rd2_addr[18]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd2_addr(18),
	o => \rd2_addr[18]~input_o\);

-- Location: FF_X74_Y65_N29
\sram_ctrl_a21d16b8_inst|rd2_addr_buffer[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \rd2_addr[18]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(18));

-- Location: LCCOMB_X74_Y65_N28
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[17]~74\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[17]~74_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & (((\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\)))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & (\rd1_addr[18]~input_o\)) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & ((\sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(18))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	datab => \rd1_addr[18]~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(18),
	datad => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[17]~74_combout\);

-- Location: IOIBUF_X115_Y57_N15
\wr_addr[18]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_addr(18),
	o => \wr_addr[18]~input_o\);

-- Location: FF_X76_Y60_N13
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~288\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[18]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~288_q\);

-- Location: LCCOMB_X75_Y60_N20
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~250feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~250feeder_combout\ = \wr_addr[18]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[18]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~250feeder_combout\);

-- Location: FF_X75_Y60_N21
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~250\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~250feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~250_q\);

-- Location: FF_X75_Y60_N23
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~212\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[18]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~212_q\);

-- Location: LCCOMB_X75_Y60_N22
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~431\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~431_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~250_q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~212_q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~250_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~212_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~431_combout\);

-- Location: FF_X76_Y60_N15
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~326\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[18]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~326_q\);

-- Location: LCCOMB_X76_Y60_N14
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~432\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~432_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~431_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~326_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~431_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~288_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~288_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~431_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~326_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~432_combout\);

-- Location: FF_X79_Y61_N19
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~174\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[18]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~174_q\);

-- Location: FF_X79_Y61_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~98\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[18]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~98_q\);

-- Location: FF_X80_Y61_N23
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~60\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[18]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~60_q\);

-- Location: FF_X80_Y61_N13
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~136\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[18]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~136_q\);

-- Location: LCCOMB_X80_Y61_N22
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~433\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~433_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~136_q\)))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~60_q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~60_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~136_q\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~433_combout\);

-- Location: LCCOMB_X79_Y61_N24
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~434\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~434_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~433_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~174_q\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~433_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~98_q\))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~433_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~174_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~98_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~433_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~434_combout\);

-- Location: LCCOMB_X77_Y65_N30
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~435\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~435_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~432_combout\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~434_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~432_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~434_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~435_combout\);

-- Location: FF_X77_Y65_N31
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~435_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(18));

-- Location: LCCOMB_X77_Y65_N16
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[17]~75\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[17]~75_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[17]~74_combout\ & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(18))) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\))) # (!\sram_ctrl_a21d16b8_inst|sram_addr_nxt[17]~74_combout\ & (\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & ((\rd2_addr[18]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[17]~74_combout\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(18),
	datad => \rd2_addr[18]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[17]~75_combout\);

-- Location: LCCOMB_X77_Y65_N6
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[17]~97\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[17]~97_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[17]~75_combout\ & (((\sram_ctrl_a21d16b8_inst|state.IDLE~q\) # (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[17]~75_combout\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[17]~97_combout\);

-- Location: FF_X77_Y65_N7
\sram_ctrl_a21d16b8_inst|sram_addr[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[17]~97_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_addr\(17));

-- Location: IOIBUF_X115_Y54_N15
\rd2_addr[19]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd2_addr(19),
	o => \rd2_addr[19]~input_o\);

-- Location: FF_X74_Y62_N29
\sram_ctrl_a21d16b8_inst|rd2_addr_buffer[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \rd2_addr[19]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(19));

-- Location: LCCOMB_X74_Y62_N28
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[18]~76\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[18]~76_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & ((\rd2_addr[19]~input_o\) # ((\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\)))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(19) & !\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rd2_addr[19]~input_o\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	datac => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(19),
	datad => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[18]~76_combout\);

-- Location: IOIBUF_X115_Y52_N1
\wr_addr[19]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_addr(19),
	o => \wr_addr[19]~input_o\);

-- Location: LCCOMB_X75_Y60_N0
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~251feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~251feeder_combout\ = \wr_addr[19]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \wr_addr[19]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~251feeder_combout\);

-- Location: FF_X75_Y60_N1
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~251\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~251feeder_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~251_q\);

-- Location: FF_X75_Y60_N27
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~213\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[19]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~213_q\);

-- Location: LCCOMB_X75_Y60_N26
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~436\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~436_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~251_q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~213_q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~251_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~213_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~436_combout\);

-- Location: FF_X76_Y60_N25
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~289\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[19]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~289_q\);

-- Location: FF_X76_Y60_N11
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~327\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[19]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~327_q\);

-- Location: LCCOMB_X76_Y60_N10
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~437\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~437_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~436_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~327_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~436_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~289_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~436_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~289_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~327_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~437_combout\);

-- Location: FF_X79_Y61_N7
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~175\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[19]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~175_q\);

-- Location: FF_X79_Y61_N13
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~99\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[19]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~99_q\);

-- Location: FF_X80_Y61_N9
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~137\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[19]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~137_q\);

-- Location: FF_X80_Y61_N19
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~61\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[19]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~61_q\);

-- Location: LCCOMB_X80_Y61_N18
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~438\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~438_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~137_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~61_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~137_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~61_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~438_combout\);

-- Location: LCCOMB_X79_Y61_N12
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~439\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~439_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~438_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~175_q\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~438_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~99_q\))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~438_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~175_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~99_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~438_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~439_combout\);

-- Location: LCCOMB_X74_Y62_N30
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~440\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~440_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~437_combout\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~439_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~437_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~439_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~440_combout\);

-- Location: FF_X74_Y62_N31
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~440_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(19));

-- Location: IOIBUF_X115_Y62_N22
\rd1_addr[19]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd1_addr(19),
	o => \rd1_addr[19]~input_o\);

-- Location: LCCOMB_X74_Y62_N16
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[18]~77\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[18]~77_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & ((\sram_ctrl_a21d16b8_inst|sram_addr_nxt[18]~76_combout\ & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(19))) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr_nxt[18]~76_combout\ & ((\rd1_addr[19]~input_o\))))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[18]~76_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[18]~76_combout\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(19),
	datad => \rd1_addr[19]~input_o\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[18]~77_combout\);

-- Location: LCCOMB_X73_Y66_N8
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[18]~98\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[18]~98_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[18]~77_combout\ & (((\sram_ctrl_a21d16b8_inst|state.IDLE~q\) # (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[18]~77_combout\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[18]~98_combout\);

-- Location: FF_X73_Y66_N9
\sram_ctrl_a21d16b8_inst|sram_addr[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[18]~98_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_addr\(18));

-- Location: IOIBUF_X102_Y73_N8
\rd1_addr[20]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd1_addr(20),
	o => \rd1_addr[20]~input_o\);

-- Location: IOIBUF_X115_Y36_N8
\rd2_addr[20]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rd2_addr(20),
	o => \rd2_addr[20]~input_o\);

-- Location: FF_X74_Y62_N11
\sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \rd2_addr[20]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer[20]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(20));

-- Location: LCCOMB_X74_Y62_N10
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[19]~78\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[19]~78_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & ((\rd1_addr[20]~input_o\) # ((\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\)))) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(20) & !\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~1_combout\,
	datab => \rd1_addr[20]~input_o\,
	datac => \sram_ctrl_a21d16b8_inst|rd2_addr_buffer\(20),
	datad => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[19]~78_combout\);

-- Location: IOIBUF_X115_Y53_N15
\wr_addr[20]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wr_addr(20),
	o => \wr_addr[20]~input_o\);

-- Location: FF_X75_Y60_N29
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~252\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[20]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~457_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~252_q\);

-- Location: FF_X75_Y60_N7
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~214\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[20]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~458_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~214_q\);

-- Location: LCCOMB_X75_Y60_N6
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~441\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~441_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~252_q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~214_q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~252_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~214_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~441_combout\);

-- Location: FF_X76_Y60_N5
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~290\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[20]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~456_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~290_q\);

-- Location: FF_X76_Y60_N23
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~328\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[20]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~459_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~328_q\);

-- Location: LCCOMB_X76_Y60_N22
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~442\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~442_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~441_combout\ & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~328_q\) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~441_combout\ 
-- & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~290_q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~441_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~290_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~328_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~442_combout\);

-- Location: FF_X79_Y61_N11
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~176\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[20]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~463_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~176_q\);

-- Location: FF_X79_Y61_N1
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~100\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[20]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~461_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~100_q\);

-- Location: FF_X80_Y61_N5
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~138\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[20]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~460_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~138_q\);

-- Location: FF_X80_Y61_N7
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~62\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \wr_addr[20]~input_o\,
	sload => VCC,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~462_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~62_q\);

-- Location: LCCOMB_X80_Y61_N6
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~443\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~443_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~138_q\) # 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1) & (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~62_q\ & 
-- !\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(1),
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~138_q\,
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~62_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~443_combout\);

-- Location: LCCOMB_X79_Y61_N0
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~444\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~444_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~443_combout\ & 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~176_q\)) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~443_combout\ & 
-- ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~100_q\))))) # (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0) & 
-- (((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~443_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~176_q\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~100_q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~443_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~444_combout\);

-- Location: LCCOMB_X74_Y62_N20
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~445\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~445_combout\ = (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~442_combout\)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2) & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~444_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|read_address\(2),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~442_combout\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~444_combout\,
	combout => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~445_combout\);

-- Location: FF_X74_Y62_N21
\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|ram~445_combout\,
	ena => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(20));

-- Location: LCCOMB_X74_Y62_N22
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[19]~79\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[19]~79_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[19]~78_combout\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(20)) # 
-- ((!\sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\)))) # (!\sram_ctrl_a21d16b8_inst|sram_addr_nxt[19]~78_combout\ & (((\rd2_addr[20]~input_o\ & \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[19]~78_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(20),
	datac => \rd2_addr[20]~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|sram_addr[6]~2_combout\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[19]~79_combout\);

-- Location: LCCOMB_X73_Y66_N26
\sram_ctrl_a21d16b8_inst|sram_addr_nxt[19]~99\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_addr_nxt[19]~99_combout\ = (\sram_ctrl_a21d16b8_inst|sram_addr_nxt[19]~79_combout\ & (((\sram_ctrl_a21d16b8_inst|state.IDLE~q\) # (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\)) # 
-- (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[19]~79_combout\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	combout => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[19]~99_combout\);

-- Location: FF_X73_Y66_N27
\sram_ctrl_a21d16b8_inst|sram_addr[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_addr_nxt[19]~99_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_addr\(19));

-- Location: LCCOMB_X68_Y69_N26
\sram_ctrl_a21d16b8_inst|Selector1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|Selector1~0_combout\ = (\sram_ctrl_a21d16b8_inst|state.IDLE~q\) # ((!\rd1~input_o\ & (!\rd2~input_o\ & !\sram_ctrl_a21d16b8_inst|rd2_pending~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010101011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datab => \rd1~input_o\,
	datac => \rd2~input_o\,
	datad => \sram_ctrl_a21d16b8_inst|rd2_pending~q\,
	combout => \sram_ctrl_a21d16b8_inst|Selector1~0_combout\);

-- Location: LCCOMB_X76_Y66_N8
\sram_ctrl_a21d16b8_inst|Selector1~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|Selector1~1_combout\ = ((\sram_ctrl_a21d16b8_inst|state.WRITE1~q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0)) # 
-- (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37))))) # (!\sram_ctrl_a21d16b8_inst|Selector1~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|Selector1~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37),
	datad => \sram_ctrl_a21d16b8_inst|state.WRITE1~q\,
	combout => \sram_ctrl_a21d16b8_inst|Selector1~1_combout\);

-- Location: FF_X76_Y66_N9
\sram_ctrl_a21d16b8_inst|sram_ub_n\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|Selector1~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_ub_n~q\);

-- Location: LCCOMB_X76_Y66_N26
\sram_ctrl_a21d16b8_inst|Selector0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|Selector0~0_combout\ = ((\sram_ctrl_a21d16b8_inst|state.WRITE1~q\ & ((\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37)) # 
-- (!\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0))))) # (!\sram_ctrl_a21d16b8_inst|Selector1~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111011101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|Selector1~0_combout\,
	datab => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(0),
	datac => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|fifo_inst|memory_inst|rd1_data\(37),
	datad => \sram_ctrl_a21d16b8_inst|state.WRITE1~q\,
	combout => \sram_ctrl_a21d16b8_inst|Selector0~0_combout\);

-- Location: FF_X76_Y66_N27
\sram_ctrl_a21d16b8_inst|sram_lb_n\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|Selector0~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_lb_n~q\);

-- Location: LCCOMB_X77_Y65_N8
\sram_ctrl_a21d16b8_inst|sram_we_n~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_we_n~feeder_combout\ = \sram_ctrl_a21d16b8_inst|state.WRITE1~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \sram_ctrl_a21d16b8_inst|state.WRITE1~q\,
	combout => \sram_ctrl_a21d16b8_inst|sram_we_n~feeder_combout\);

-- Location: FF_X77_Y65_N9
\sram_ctrl_a21d16b8_inst|sram_we_n\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_we_n~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_we_n~q\);

-- Location: LCCOMB_X73_Y66_N30
\sram_ctrl_a21d16b8_inst|sram_ce_n_nxt~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \sram_ctrl_a21d16b8_inst|sram_ce_n_nxt~0_combout\ = ((\sram_ctrl_a21d16b8_inst|state.IDLE~q\) # (\sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\)) # (!\sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sram_ctrl_a21d16b8_inst|sram_addr[6]~0_combout\,
	datac => \sram_ctrl_a21d16b8_inst|state.IDLE~q\,
	datad => \sram_ctrl_a21d16b8_inst|wr_fifo_block:wr_fifo|rd_valid~q\,
	combout => \sram_ctrl_a21d16b8_inst|sram_ce_n_nxt~0_combout\);

-- Location: FF_X73_Y66_N29
\sram_ctrl_a21d16b8_inst|sram_ce_n\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \sram_ctrl_a21d16b8_inst|sram_ce_n_nxt~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_ce_n~q\);

-- Location: FF_X73_Y66_N31
\sram_ctrl_a21d16b8_inst|sram_oe_n\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sram_ctrl_a21d16b8_inst|sram_ce_n_nxt~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sram_ctrl_a21d16b8_inst|sram_oe_n~q\);

ww_wr_empty <= \wr_empty~output_o\;

ww_wr_full <= \wr_full~output_o\;

ww_wr_half_full <= \wr_half_full~output_o\;

ww_rd1_busy <= \rd1_busy~output_o\;

ww_rd1_data(0) <= \rd1_data[0]~output_o\;

ww_rd1_data(1) <= \rd1_data[1]~output_o\;

ww_rd1_data(2) <= \rd1_data[2]~output_o\;

ww_rd1_data(3) <= \rd1_data[3]~output_o\;

ww_rd1_data(4) <= \rd1_data[4]~output_o\;

ww_rd1_data(5) <= \rd1_data[5]~output_o\;

ww_rd1_data(6) <= \rd1_data[6]~output_o\;

ww_rd1_data(7) <= \rd1_data[7]~output_o\;

ww_rd1_data(8) <= \rd1_data[8]~output_o\;

ww_rd1_data(9) <= \rd1_data[9]~output_o\;

ww_rd1_data(10) <= \rd1_data[10]~output_o\;

ww_rd1_data(11) <= \rd1_data[11]~output_o\;

ww_rd1_data(12) <= \rd1_data[12]~output_o\;

ww_rd1_data(13) <= \rd1_data[13]~output_o\;

ww_rd1_data(14) <= \rd1_data[14]~output_o\;

ww_rd1_data(15) <= \rd1_data[15]~output_o\;

ww_rd1_valid <= \rd1_valid~output_o\;

ww_rd2_busy <= \rd2_busy~output_o\;

ww_rd2_data(0) <= \rd2_data[0]~output_o\;

ww_rd2_data(1) <= \rd2_data[1]~output_o\;

ww_rd2_data(2) <= \rd2_data[2]~output_o\;

ww_rd2_data(3) <= \rd2_data[3]~output_o\;

ww_rd2_data(4) <= \rd2_data[4]~output_o\;

ww_rd2_data(5) <= \rd2_data[5]~output_o\;

ww_rd2_data(6) <= \rd2_data[6]~output_o\;

ww_rd2_data(7) <= \rd2_data[7]~output_o\;

ww_rd2_data(8) <= \rd2_data[8]~output_o\;

ww_rd2_data(9) <= \rd2_data[9]~output_o\;

ww_rd2_data(10) <= \rd2_data[10]~output_o\;

ww_rd2_data(11) <= \rd2_data[11]~output_o\;

ww_rd2_data(12) <= \rd2_data[12]~output_o\;

ww_rd2_data(13) <= \rd2_data[13]~output_o\;

ww_rd2_data(14) <= \rd2_data[14]~output_o\;

ww_rd2_data(15) <= \rd2_data[15]~output_o\;

ww_rd2_valid <= \rd2_valid~output_o\;

ww_sram_addr(0) <= \sram_addr[0]~output_o\;

ww_sram_addr(1) <= \sram_addr[1]~output_o\;

ww_sram_addr(2) <= \sram_addr[2]~output_o\;

ww_sram_addr(3) <= \sram_addr[3]~output_o\;

ww_sram_addr(4) <= \sram_addr[4]~output_o\;

ww_sram_addr(5) <= \sram_addr[5]~output_o\;

ww_sram_addr(6) <= \sram_addr[6]~output_o\;

ww_sram_addr(7) <= \sram_addr[7]~output_o\;

ww_sram_addr(8) <= \sram_addr[8]~output_o\;

ww_sram_addr(9) <= \sram_addr[9]~output_o\;

ww_sram_addr(10) <= \sram_addr[10]~output_o\;

ww_sram_addr(11) <= \sram_addr[11]~output_o\;

ww_sram_addr(12) <= \sram_addr[12]~output_o\;

ww_sram_addr(13) <= \sram_addr[13]~output_o\;

ww_sram_addr(14) <= \sram_addr[14]~output_o\;

ww_sram_addr(15) <= \sram_addr[15]~output_o\;

ww_sram_addr(16) <= \sram_addr[16]~output_o\;

ww_sram_addr(17) <= \sram_addr[17]~output_o\;

ww_sram_addr(18) <= \sram_addr[18]~output_o\;

ww_sram_addr(19) <= \sram_addr[19]~output_o\;

ww_sram_ub_n <= \sram_ub_n~output_o\;

ww_sram_lb_n <= \sram_lb_n~output_o\;

ww_sram_we_n <= \sram_we_n~output_o\;

ww_sram_ce_n <= \sram_ce_n~output_o\;

ww_sram_oe_n <= \sram_oe_n~output_o\;

sram_dq(0) <= \sram_dq[0]~output_o\;

sram_dq(1) <= \sram_dq[1]~output_o\;

sram_dq(2) <= \sram_dq[2]~output_o\;

sram_dq(3) <= \sram_dq[3]~output_o\;

sram_dq(4) <= \sram_dq[4]~output_o\;

sram_dq(5) <= \sram_dq[5]~output_o\;

sram_dq(6) <= \sram_dq[6]~output_o\;

sram_dq(7) <= \sram_dq[7]~output_o\;

sram_dq(8) <= \sram_dq[8]~output_o\;

sram_dq(9) <= \sram_dq[9]~output_o\;

sram_dq(10) <= \sram_dq[10]~output_o\;

sram_dq(11) <= \sram_dq[11]~output_o\;

sram_dq(12) <= \sram_dq[12]~output_o\;

sram_dq(13) <= \sram_dq[13]~output_o\;

sram_dq(14) <= \sram_dq[14]~output_o\;

sram_dq(15) <= \sram_dq[15]~output_o\;
END structure;


