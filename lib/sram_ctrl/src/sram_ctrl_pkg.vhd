library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package sram_ctrl_pkg is

	type sram_access_mode_t is (BYTE, WORD);
	
	function to_std_logic(x : sram_access_mode_t) return std_logic;
	function to_sram_access_mode_t(x : std_logic) return sram_access_mode_t;

	component precompiled_sram_ctrl_a21d16b8 is
		port (
			clk : in std_logic;
			res_n : in std_logic;
			
			-- write port (buffered)
			wr_addr : in std_logic_vector(20 downto 0);
			wr_data : in std_logic_vector(15 downto 0);
			wr : in std_logic;
			wr_access_mode : in sram_access_mode_t;
			wr_empty : out std_logic;
			wr_full : out std_logic;
			wr_half_full : out std_logic;
			
			-- read port 1 (high priority)
			rd1_addr : in std_logic_vector(20 downto 0);
			rd1 : in std_logic;
			rd1_access_mode : in sram_access_mode_t;
			rd1_busy : out std_logic;
			rd1_data : out std_logic_vector(15 downto 0);
			rd1_valid : out std_logic;
			
			-- read port 2 (low priority)
			rd2_addr : in std_logic_vector(20 downto 0);
			rd2 : in std_logic;
			rd2_access_mode : in sram_access_mode_t;
			rd2_busy : out std_logic;
			rd2_data : out std_logic_vector(15 downto 0);
			rd2_valid : out std_logic;
			
			-- external interface to SRAM
			sram_dq : inout std_logic_vector(15 downto 0);
			sram_addr : out std_logic_vector(19 downto 0);
			sram_ub_n : out std_logic;
			sram_lb_n : out std_logic;
			sram_we_n : out std_logic;
			sram_ce_n : out std_logic;
			sram_oe_n : out std_logic
		);
	end component;

	component sram_ctrl is
		generic (
			ADDR_WIDTH : integer := 21;
			DATA_WIDTH : integer := 16;
			WR_BUF_SIZE : integer := 8
		);
		port (
			clk : in std_logic;
			res_n : in std_logic;
			
			-- write port (buffered)
			wr_addr : in std_logic_vector(ADDR_WIDTH-1 downto 0);
			wr_data : in std_logic_vector(DATA_WIDTH-1 downto 0);
			wr : in std_logic;
			wr_access_mode : in sram_access_mode_t;
			wr_empty : out std_logic;
			wr_full : out std_logic;
			wr_half_full : out std_logic;
			
			-- read port 1 (high priority)
			rd1_addr : in std_logic_vector(ADDR_WIDTH-1 downto 0);
			rd1 : in std_logic;
			rd1_access_mode : in sram_access_mode_t;
			rd1_busy : out std_logic;
			rd1_data : out std_logic_vector(DATA_WIDTH-1 downto 0);
			rd1_valid : out std_logic;
			
			-- read port 2 (low priority)
			rd2_addr : in std_logic_vector(ADDR_WIDTH-1 downto 0);
			rd2 : in std_logic;
			rd2_access_mode : in sram_access_mode_t;
			rd2_busy : out std_logic;
			rd2_data : out std_logic_vector(DATA_WIDTH-1 downto 0);
			rd2_valid : out std_logic;
			
			-- external interface to SRAM
			sram_dq : inout std_logic_vector(DATA_WIDTH-1 downto 0);
			sram_addr : out std_logic_vector(ADDR_WIDTH-2 downto 0);
			sram_ub_n : out std_logic;
			sram_lb_n : out std_logic;
			sram_we_n : out std_logic;
			sram_ce_n : out std_logic;
			sram_oe_n : out std_logic
		);
	end component;
end package;


package body sram_ctrl_pkg is

	function to_std_logic(x : sram_access_mode_t) return std_logic is
	begin
		if (x = WORD) then
			return '1';
		end if;
		return '0';
	end function;

	function to_sram_access_mode_t(x : std_logic) return sram_access_mode_t is
	begin
		if (x = '1') then
			return WORD;
		end if;
		return BYTE;
	end function;

end package body;

