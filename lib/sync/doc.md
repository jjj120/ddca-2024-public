
# Synchronizer
The synchronizer component is used to connect external signals (e.g., from push buttons or serial ports) to a
design. As these input devices generate signals which not synchronous to internal FPGA clocks, using them
without proper synchronization can lead to upsets and hence malfunction of a design.

## Dependencies
* None

## Required Files
 * `sync.vhd`
 * `sync_pkg.vhd`

## Synchronizer Component Declaration

```vhdl
component sync is
	generic(
		SYNC_STAGES : integer range 2 to integer'high; -- number of synchronizer stages (i.e., flip flops)
		RESET_VALUE : std_logic -- reset value of the output signal
	);
	port (
		clk       : in std_logic;
		res_n     : in std_logic;
		data_in   : in std_logic;
		data_out  : out std_logic
	);
end component;
```

## Synchronizer Interface Description
The synchronizer has no special interface protocol. 
The input signal is sampled with the clock signal `clk`.
Therefore an output signal generated which is aligned to the `clk` and has a delay of `n` clock cycles, where `n` is the number of synchronizer stages (i.e., `SYNC_STAGES`).
Spikes or glitches not overlapping a rising clock edge (see example trace below) will not show up at the synchronizer output.

![Synchronizer timing](.mdata/sync_timing.svg)

## Internal Structure
The synchronizer internally consists of a D flip-flop chain.
The figure below shows an example of a three stage synchronizer (`SYNC_STAGES`=3).

![Synchronizer circuit](.mdata/internal_structure.svg)
