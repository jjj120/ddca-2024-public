
# UART Data Streamer
The `uart_data_streamer` module implements an 8N1 UART interface.
Received UART bytes are collected and put into a FIFO for consumers.

Moreover, the module also consumes data which is split into bytes and sent out via the UART `tx` port.


## Dependencies
* Math Package 
* Memory Package 


## Required Files
 * `uart_data_streamer.vhd`
 * `uart_data_streamer_pkg.vhd`

## UART Data Streamer Component Declaration

```vhdl
component uart_data_streamer is
	generic (
		CLK_FREQ                 : integer := 50_000_000; --the system clock frequency
		BAUD_RATE                : integer := 9600;
		SYNC_STAGES              : integer := 2; --the amount of sync stages for the receiver
		RX_FIFO_DEPTH            : integer := 16; --the fifo-depth for the receiver
		TX_FIFO_DEPTH            : integer := 16; --the fifo-depth for the transmitter
		OUTPUT_STREAM_DATA_BYTES : positive; -- In bytes
		INPUT_STREAM_DATA_BYTES  : positive; -- In bytes
		BYTE_ORDER               : BYTE_ORDER := BIG_ENDIAN
	);
	port (
		clk : in std_logic := '0';
		res_n : in std_logic := '1';

		-- UART 
		rx : in std_logic := '0';
		tx : out std_logic := '1';

		halt : in std_logic := '0';
		rx_full : out std_logic := '0';

		-- output stream
		os_valid : out std_logic := '0';
		os_data : out std_logic_vector(OUTPUT_STREAM_DATA_BYTES*8-1 downto 0) := (others => '0');
		os_ready : in std_logic := '0';

		-- input stream
		is_valid : in std_logic := '0';
		is_data  : in std_logic_vector(INPUT_STREAM_DATA_BYTES*8-1 downto 0):= (others => '0');
		is_ready : out std_logic := '0'
	);
end component;
```

## UART Data Streamer Interface Description
The structural overview of the `uart_data_streamer` is shown in the figure below:

![Structure](.mdata/structure.svg)

The `rx` input and `tx` output are typical 8N1 (8-bit, no parity, 1 stop bit) UART ports.

The defualt [byte ordering](https://en.wikipedia.org/wiki/Endianness) is big-endian.
This can be changed via the `BYTE_ORDERING` generic.

The `RX Collector` is a state machine which consecutively collects bytes from `rx` UART transactions. After receiving `OUTPUT_STREAM_DATA_BYTES` (`k` in the figure) many bytes (default: lower byte first) it writes the collected data vector into the `RX FIFO`. The `RX FIFO` provides at most `RX_FIFO_DEPTH` many data words to a consumer (see below).

The `TX FIFO` stores and receives `INPUT_STREAM_DATA_BYTES` times 8 wide bit vectors in typical [fwft-FIFO](../../mem/src/fifo_1c1r1w_fwft.vhd) fashion. Whenever this FIFO is not empty, the `TX Splitter` state machine reads a data words and splits them into bytes which are then sent out via the `tx` UART port (default: lower byte first).

## Output Stream

The output stream interface is primarily wired to an [fwft-FIFO](../../mem/src/fifo_1c1r1w_fwft.vhd). The `os_ready` input signal is used to signal the `uart_data_streamer` that a connected consumer is ready to consume data. `os_valid` is asserted to signal valid consumable data on the `os_data` output.

You can see the timing behaviour of the output stream interface in the figure below.

![Output Stream Timings](.mdata/os_timing.svg)

Since the output stream consumer most likely consumes data far quicker than the UART interface receives data, the `halt` signal can be used to stall the output stream.
This way the `RX_FIFO` can for sure store multiple received data words without them begin read immediately and might be useful for debugging the consumer.
This signal is simply negated ANDed with the `os_valid` signal.
The `rx_full` signal is simply wired to the `RX_FIFO`'s `full` signal.

## Input Stream

The input stream interface works very similar. The `is_ready` signal is used to signal a producer that the `uart_data_streamer` is ready to receive new data. This is the case whenever the `TX_FIFO` it not full. The `uart_data_streamer` stores data from the `is_data` input in its TX_FIFO when the `uart_data_streamer` is ready and the `is_valid` input is asserted.
