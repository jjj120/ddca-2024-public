library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package uart_data_streamer_pkg is
	type BYTE_ORDER is (
		LITTLE_ENDIAN,
		BIG_ENDIAN
	);

	component uart_data_streamer is
		generic (
			CLK_FREQ                 : integer := 50_000_000; --the system clock frequency
			BAUD_RATE                : integer := 9600;
			SYNC_STAGES              : integer := 2; --the amount of sync stages for the receiver
			RX_FIFO_DEPTH            : integer := 16; --the fifo-depth for the receiver
			TX_FIFO_DEPTH            : integer := 16; --the fifo-depth for the transmitter
			OUTPUT_STREAM_DATA_BYTES : positive; -- In bytes
			INPUT_STREAM_DATA_BYTES  : positive; -- In bytes
			BYTE_ORDER               : BYTE_ORDER := BIG_ENDIAN
		);
		port (
			clk : in std_logic := '0';
			res_n : in std_logic := '1';

			-- UART 
			rx : in std_logic := '0';
			tx : out std_logic := '1';

			halt : in std_logic := '0';
			rx_full : out std_logic := '0';

			-- output stream
			os_valid : out std_logic := '0';
			os_data : out std_logic_vector(OUTPUT_STREAM_DATA_BYTES*8-1 downto 0) := (others => '0');
			os_ready : in std_logic := '0';

			-- input stream
			is_valid : in std_logic := '0';
			is_data  : in std_logic_vector(INPUT_STREAM_DATA_BYTES*8-1 downto 0):= (others => '0');
			is_ready : out std_logic := '0'
		);
	end component;
end package;

