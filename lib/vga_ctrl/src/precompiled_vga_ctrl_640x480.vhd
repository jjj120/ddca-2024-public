library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity precompiled_vga_ctrl_640x480 is
	port (
		clk     : in  std_logic;
		res_n   : in  std_logic;

		frame_start : out std_logic;
		pix_data_r  : in std_logic_vector(7 downto 0); -- red pixel data
		pix_data_g  : in std_logic_vector(7 downto 0); -- green pixel data
		pix_data_b  : in std_logic_vector(7 downto 0); -- blue pixel data
		pix_ack     : out std_logic; -- read next pixel value from FIFO 
		
		-- connection to VGA connector/DAC
		vga_hsync       : out std_logic;
		vga_vsync       : out std_logic;
		vga_dac_clk     : out std_logic;
		vga_dac_blank_n : out std_logic;
		vga_dac_sync_n  : out std_logic;
		vga_dac_r       : out std_logic_vector(7 downto 0);
		vga_dac_g       : out std_logic_vector(7 downto 0);
		vga_dac_b       : out std_logic_vector(7 downto 0)
	);
end entity;

architecture arch of precompiled_vga_ctrl_640x480 is
	component vga_ctrl_640x480_top is
		port (
			clk     : in  std_logic;
			res_n   : in  std_logic;
	
			frame_start : out std_logic;
			pix_data_r  : in std_logic_vector(7 downto 0); -- red pixel data
			pix_data_g  : in std_logic_vector(7 downto 0); -- green pixel data
			pix_data_b  : in std_logic_vector(7 downto 0); -- blue pixel data
			pix_ack     : out std_logic; -- read next pixel value from FIFO 
			
			-- connection to VGA connector/DAC
			vga_hsync       : out std_logic;
			vga_vsync       : out std_logic;
			vga_dac_clk     : out std_logic;
			vga_dac_blank_n : out std_logic;
			vga_dac_sync_n  : out std_logic;
			vga_dac_r       : out std_logic_vector(7 downto 0);
			vga_dac_g       : out std_logic_vector(7 downto 0);
			vga_dac_b       : out std_logic_vector(7 downto 0)
		);
	end component;
begin
	vga_ctrl_inst : vga_ctrl_640x480_top
	port map (
		clk             => clk,
		res_n           => res_n,
		frame_start     => frame_start,
		pix_data_r      => pix_data_r,
		pix_data_g      => pix_data_g,
		pix_data_b      => pix_data_b,
		pix_ack         => pix_ack,
		vga_hsync       => vga_hsync,
		vga_vsync       => vga_vsync,
		vga_dac_clk     => vga_dac_clk,
		vga_dac_blank_n => vga_dac_blank_n,
		vga_dac_sync_n  => vga_dac_sync_n,
		vga_dac_r       => vga_dac_r,
		vga_dac_g       => vga_dac_g,
		vga_dac_b       => vga_dac_b
	);
end architecture;

