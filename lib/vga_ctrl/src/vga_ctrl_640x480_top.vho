-- Copyright (C) 2023  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 22.1std.2 Build 922 07/20/2023 SC Standard Edition"

-- DATE "04/11/2024 16:54:37"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim (VHDL) only
-- 

LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_ASDO_DATA1~	=>  Location: PIN_F4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_FLASH_nCE_nCSO~	=>  Location: PIN_E2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DCLK~	=>  Location: PIN_P3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DATA0~	=>  Location: PIN_N7,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~padout\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~padout\ : std_logic;
SIGNAL \~ALTERA_DATA0~~padout\ : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_DATA0~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	vga_ctrl_640x480_top IS
    PORT (
	clk : IN std_logic;
	res_n : IN std_logic;
	frame_start : OUT std_logic;
	pix_data_r : IN std_logic_vector(7 DOWNTO 0);
	pix_data_g : IN std_logic_vector(7 DOWNTO 0);
	pix_data_b : IN std_logic_vector(7 DOWNTO 0);
	pix_ack : OUT std_logic;
	vga_hsync : OUT std_logic;
	vga_vsync : OUT std_logic;
	vga_dac_clk : OUT std_logic;
	vga_dac_blank_n : OUT std_logic;
	vga_dac_sync_n : OUT std_logic;
	vga_dac_r : OUT std_logic_vector(7 DOWNTO 0);
	vga_dac_g : OUT std_logic_vector(7 DOWNTO 0);
	vga_dac_b : OUT std_logic_vector(7 DOWNTO 0)
	);
END vga_ctrl_640x480_top;

-- Design Ports Information
-- frame_start	=>  Location: PIN_C20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pix_ack	=>  Location: PIN_E25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_hsync	=>  Location: PIN_D20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_vsync	=>  Location: PIN_E24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_clk	=>  Location: PIN_R7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_blank_n	=>  Location: PIN_C19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_sync_n	=>  Location: PIN_K27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_r[0]	=>  Location: PIN_AE17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_r[1]	=>  Location: PIN_C16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_r[2]	=>  Location: PIN_AH18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_r[3]	=>  Location: PIN_H16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_r[4]	=>  Location: PIN_C15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_r[5]	=>  Location: PIN_AF18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_r[6]	=>  Location: PIN_F17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_r[7]	=>  Location: PIN_H15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_g[0]	=>  Location: PIN_D16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_g[1]	=>  Location: PIN_J15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_g[2]	=>  Location: PIN_D15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_g[3]	=>  Location: PIN_G19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_g[4]	=>  Location: PIN_AG18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_g[5]	=>  Location: PIN_J16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_g[6]	=>  Location: PIN_AB15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_g[7]	=>  Location: PIN_B17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_b[0]	=>  Location: PIN_F15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_b[1]	=>  Location: PIN_G15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_b[2]	=>  Location: PIN_AF17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_b[3]	=>  Location: PIN_E15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_b[4]	=>  Location: PIN_A17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_b[5]	=>  Location: PIN_AG19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_b[6]	=>  Location: PIN_AE18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- vga_dac_b[7]	=>  Location: PIN_AH19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clk	=>  Location: PIN_J1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pix_data_r[0]	=>  Location: PIN_J28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pix_data_r[1]	=>  Location: PIN_J27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pix_data_r[2]	=>  Location: PIN_AH21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pix_data_r[3]	=>  Location: PIN_E17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pix_data_r[4]	=>  Location: PIN_G16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pix_data_r[5]	=>  Location: PIN_AA15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pix_data_r[6]	=>  Location: PIN_A18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pix_data_r[7]	=>  Location: PIN_C17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pix_data_g[0]	=>  Location: PIN_G20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pix_data_g[1]	=>  Location: PIN_H21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pix_data_g[2]	=>  Location: PIN_H19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pix_data_g[3]	=>  Location: PIN_B18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pix_data_g[4]	=>  Location: PIN_AC17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pix_data_g[5]	=>  Location: PIN_H17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pix_data_g[6]	=>  Location: PIN_AG21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pix_data_g[7]	=>  Location: PIN_J19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pix_data_b[0]	=>  Location: PIN_G21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pix_data_b[1]	=>  Location: PIN_G22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pix_data_b[2]	=>  Location: PIN_AG22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pix_data_b[3]	=>  Location: PIN_G18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pix_data_b[4]	=>  Location: PIN_J17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pix_data_b[5]	=>  Location: PIN_AE16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pix_data_b[6]	=>  Location: PIN_AD17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pix_data_b[7]	=>  Location: PIN_AH22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- res_n	=>  Location: PIN_Y2,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF vga_ctrl_640x480_top IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_res_n : std_logic;
SIGNAL ww_frame_start : std_logic;
SIGNAL ww_pix_data_r : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_pix_data_g : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_pix_data_b : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_pix_ack : std_logic;
SIGNAL ww_vga_hsync : std_logic;
SIGNAL ww_vga_vsync : std_logic;
SIGNAL ww_vga_dac_clk : std_logic;
SIGNAL ww_vga_dac_blank_n : std_logic;
SIGNAL ww_vga_dac_sync_n : std_logic;
SIGNAL ww_vga_dac_r : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_vga_dac_g : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_vga_dac_b : std_logic_vector(7 DOWNTO 0);
SIGNAL \res_n~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \clk~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \frame_start~output_o\ : std_logic;
SIGNAL \pix_ack~output_o\ : std_logic;
SIGNAL \vga_hsync~output_o\ : std_logic;
SIGNAL \vga_vsync~output_o\ : std_logic;
SIGNAL \vga_dac_clk~output_o\ : std_logic;
SIGNAL \vga_dac_blank_n~output_o\ : std_logic;
SIGNAL \vga_dac_sync_n~output_o\ : std_logic;
SIGNAL \vga_dac_r[0]~output_o\ : std_logic;
SIGNAL \vga_dac_r[1]~output_o\ : std_logic;
SIGNAL \vga_dac_r[2]~output_o\ : std_logic;
SIGNAL \vga_dac_r[3]~output_o\ : std_logic;
SIGNAL \vga_dac_r[4]~output_o\ : std_logic;
SIGNAL \vga_dac_r[5]~output_o\ : std_logic;
SIGNAL \vga_dac_r[6]~output_o\ : std_logic;
SIGNAL \vga_dac_r[7]~output_o\ : std_logic;
SIGNAL \vga_dac_g[0]~output_o\ : std_logic;
SIGNAL \vga_dac_g[1]~output_o\ : std_logic;
SIGNAL \vga_dac_g[2]~output_o\ : std_logic;
SIGNAL \vga_dac_g[3]~output_o\ : std_logic;
SIGNAL \vga_dac_g[4]~output_o\ : std_logic;
SIGNAL \vga_dac_g[5]~output_o\ : std_logic;
SIGNAL \vga_dac_g[6]~output_o\ : std_logic;
SIGNAL \vga_dac_g[7]~output_o\ : std_logic;
SIGNAL \vga_dac_b[0]~output_o\ : std_logic;
SIGNAL \vga_dac_b[1]~output_o\ : std_logic;
SIGNAL \vga_dac_b[2]~output_o\ : std_logic;
SIGNAL \vga_dac_b[3]~output_o\ : std_logic;
SIGNAL \vga_dac_b[4]~output_o\ : std_logic;
SIGNAL \vga_dac_b[5]~output_o\ : std_logic;
SIGNAL \vga_dac_b[6]~output_o\ : std_logic;
SIGNAL \vga_dac_b[7]~output_o\ : std_logic;
SIGNAL \clk~input_o\ : std_logic;
SIGNAL \clk~inputclkctrl_outclk\ : std_logic;
SIGNAL \vga_ctrl_inst|Add0~0_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|hline_cnt~2_combout\ : std_logic;
SIGNAL \res_n~input_o\ : std_logic;
SIGNAL \res_n~inputclkctrl_outclk\ : std_logic;
SIGNAL \vga_ctrl_inst|Add1~0_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|Add1~1\ : std_logic;
SIGNAL \vga_ctrl_inst|Add1~2_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|Add1~3\ : std_logic;
SIGNAL \vga_ctrl_inst|Add1~4_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|Add1~5\ : std_logic;
SIGNAL \vga_ctrl_inst|Add1~6_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|Add1~7\ : std_logic;
SIGNAL \vga_ctrl_inst|Add1~8_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|Add1~9\ : std_logic;
SIGNAL \vga_ctrl_inst|Add1~10_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|line_clk_cnt~2_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|Add1~11\ : std_logic;
SIGNAL \vga_ctrl_inst|Add1~12_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|Add1~13\ : std_logic;
SIGNAL \vga_ctrl_inst|Add1~14_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|Add1~15\ : std_logic;
SIGNAL \vga_ctrl_inst|Add1~16_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|line_clk_cnt~0_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|Add1~17\ : std_logic;
SIGNAL \vga_ctrl_inst|Add1~18_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|line_clk_cnt~1_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|Equal0~1_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|Equal0~0_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|Equal0~2_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|Add0~1\ : std_logic;
SIGNAL \vga_ctrl_inst|Add0~2_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|Add0~3\ : std_logic;
SIGNAL \vga_ctrl_inst|Add0~5\ : std_logic;
SIGNAL \vga_ctrl_inst|Add0~6_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|hline_cnt~1_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|Add0~7\ : std_logic;
SIGNAL \vga_ctrl_inst|Add0~8_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|Add0~9\ : std_logic;
SIGNAL \vga_ctrl_inst|Add0~10_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|hline_cnt[5]~7_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|Add0~11\ : std_logic;
SIGNAL \vga_ctrl_inst|Add0~12_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|hline_cnt[6]~6_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|Add0~13\ : std_logic;
SIGNAL \vga_ctrl_inst|Add0~14_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|hline_cnt[7]~5_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|Add0~15\ : std_logic;
SIGNAL \vga_ctrl_inst|Add0~16_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|hline_cnt[8]~4_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|Add0~17\ : std_logic;
SIGNAL \vga_ctrl_inst|Add0~18_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|hline_cnt~0_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|Equal2~0_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|Equal2~1_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|Equal2~2_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|Add0~4_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|hline_cnt~3_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|frame_start~0_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|frame_start~1_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|frame_start~q\ : std_logic;
SIGNAL \vga_ctrl_inst|snyc~0_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|pix_ack~0_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|pix_ack~1_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_hsync_int~0_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_hsync_int~1_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_hsync_int~q\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_hsync~q\ : std_logic;
SIGNAL \vga_ctrl_inst|snyc~1_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|snyc~2_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_vsync_int~0_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_vsync_int~q\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_vsync~feeder_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_vsync~q\ : std_logic;
SIGNAL \pix_data_r[0]~input_o\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_dac_r[0]~0_combout\ : std_logic;
SIGNAL \pix_data_r[1]~input_o\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_dac_r[1]~1_combout\ : std_logic;
SIGNAL \pix_data_r[2]~input_o\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_dac_r[2]~2_combout\ : std_logic;
SIGNAL \pix_data_r[3]~input_o\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_dac_r[3]~3_combout\ : std_logic;
SIGNAL \pix_data_r[4]~input_o\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_dac_r[4]~4_combout\ : std_logic;
SIGNAL \pix_data_r[5]~input_o\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_dac_r[5]~5_combout\ : std_logic;
SIGNAL \pix_data_r[6]~input_o\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_dac_r[6]~6_combout\ : std_logic;
SIGNAL \pix_data_r[7]~input_o\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_dac_r[7]~7_combout\ : std_logic;
SIGNAL \pix_data_g[0]~input_o\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_dac_g[0]~0_combout\ : std_logic;
SIGNAL \pix_data_g[1]~input_o\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_dac_g[1]~1_combout\ : std_logic;
SIGNAL \pix_data_g[2]~input_o\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_dac_g[2]~2_combout\ : std_logic;
SIGNAL \pix_data_g[3]~input_o\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_dac_g[3]~3_combout\ : std_logic;
SIGNAL \pix_data_g[4]~input_o\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_dac_g[4]~4_combout\ : std_logic;
SIGNAL \pix_data_g[5]~input_o\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_dac_g[5]~5_combout\ : std_logic;
SIGNAL \pix_data_g[6]~input_o\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_dac_g[6]~6_combout\ : std_logic;
SIGNAL \pix_data_g[7]~input_o\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_dac_g[7]~7_combout\ : std_logic;
SIGNAL \pix_data_b[0]~input_o\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_dac_b[0]~0_combout\ : std_logic;
SIGNAL \pix_data_b[1]~input_o\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_dac_b[1]~1_combout\ : std_logic;
SIGNAL \pix_data_b[2]~input_o\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_dac_b[2]~2_combout\ : std_logic;
SIGNAL \pix_data_b[3]~input_o\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_dac_b[3]~3_combout\ : std_logic;
SIGNAL \pix_data_b[4]~input_o\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_dac_b[4]~4_combout\ : std_logic;
SIGNAL \pix_data_b[5]~input_o\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_dac_b[5]~5_combout\ : std_logic;
SIGNAL \pix_data_b[6]~input_o\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_dac_b[6]~6_combout\ : std_logic;
SIGNAL \pix_data_b[7]~input_o\ : std_logic;
SIGNAL \vga_ctrl_inst|vga_dac_b[7]~7_combout\ : std_logic;
SIGNAL \vga_ctrl_inst|hline_cnt\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \vga_ctrl_inst|line_clk_cnt\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \vga_ctrl_inst|ALT_INV_vga_vsync~q\ : std_logic;
SIGNAL \vga_ctrl_inst|ALT_INV_vga_hsync~q\ : std_logic;

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

ww_clk <= clk;
ww_res_n <= res_n;
frame_start <= ww_frame_start;
ww_pix_data_r <= pix_data_r;
ww_pix_data_g <= pix_data_g;
ww_pix_data_b <= pix_data_b;
pix_ack <= ww_pix_ack;
vga_hsync <= ww_vga_hsync;
vga_vsync <= ww_vga_vsync;
vga_dac_clk <= ww_vga_dac_clk;
vga_dac_blank_n <= ww_vga_dac_blank_n;
vga_dac_sync_n <= ww_vga_dac_sync_n;
vga_dac_r <= ww_vga_dac_r;
vga_dac_g <= ww_vga_dac_g;
vga_dac_b <= ww_vga_dac_b;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\res_n~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \res_n~input_o\);

\clk~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \clk~input_o\);
\vga_ctrl_inst|ALT_INV_vga_vsync~q\ <= NOT \vga_ctrl_inst|vga_vsync~q\;
\vga_ctrl_inst|ALT_INV_vga_hsync~q\ <= NOT \vga_ctrl_inst|vga_hsync~q\;
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: IOOBUF_X85_Y73_N9
\frame_start~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|frame_start~q\,
	devoe => ww_devoe,
	o => \frame_start~output_o\);

-- Location: IOOBUF_X83_Y73_N2
\pix_ack~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|pix_ack~1_combout\,
	devoe => ww_devoe,
	o => \pix_ack~output_o\);

-- Location: IOOBUF_X85_Y73_N16
\vga_hsync~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|ALT_INV_vga_hsync~q\,
	devoe => ww_devoe,
	o => \vga_hsync~output_o\);

-- Location: IOOBUF_X85_Y73_N23
\vga_vsync~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|ALT_INV_vga_vsync~q\,
	devoe => ww_devoe,
	o => \vga_vsync~output_o\);

-- Location: IOOBUF_X0_Y35_N16
\vga_dac_clk~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \clk~input_o\,
	devoe => ww_devoe,
	o => \vga_dac_clk~output_o\);

-- Location: IOOBUF_X83_Y73_N9
\vga_dac_blank_n~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|pix_ack~1_combout\,
	devoe => ww_devoe,
	o => \vga_dac_blank_n~output_o\);

-- Location: IOOBUF_X115_Y50_N9
\vga_dac_sync_n~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \vga_dac_sync_n~output_o\);

-- Location: IOOBUF_X67_Y0_N9
\vga_dac_r[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|vga_dac_r[0]~0_combout\,
	devoe => ww_devoe,
	o => \vga_dac_r[0]~output_o\);

-- Location: IOOBUF_X62_Y73_N16
\vga_dac_r[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|vga_dac_r[1]~1_combout\,
	devoe => ww_devoe,
	o => \vga_dac_r[1]~output_o\);

-- Location: IOOBUF_X69_Y0_N2
\vga_dac_r[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|vga_dac_r[2]~2_combout\,
	devoe => ww_devoe,
	o => \vga_dac_r[2]~output_o\);

-- Location: IOOBUF_X65_Y73_N23
\vga_dac_r[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|vga_dac_r[3]~3_combout\,
	devoe => ww_devoe,
	o => \vga_dac_r[3]~output_o\);

-- Location: IOOBUF_X58_Y73_N16
\vga_dac_r[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|vga_dac_r[4]~4_combout\,
	devoe => ww_devoe,
	o => \vga_dac_r[4]~output_o\);

-- Location: IOOBUF_X79_Y0_N16
\vga_dac_r[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|vga_dac_r[5]~5_combout\,
	devoe => ww_devoe,
	o => \vga_dac_r[5]~output_o\);

-- Location: IOOBUF_X67_Y73_N16
\vga_dac_r[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|vga_dac_r[6]~6_combout\,
	devoe => ww_devoe,
	o => \vga_dac_r[6]~output_o\);

-- Location: IOOBUF_X60_Y73_N16
\vga_dac_r[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|vga_dac_r[7]~7_combout\,
	devoe => ww_devoe,
	o => \vga_dac_r[7]~output_o\);

-- Location: IOOBUF_X62_Y73_N23
\vga_dac_g[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|vga_dac_g[0]~0_combout\,
	devoe => ww_devoe,
	o => \vga_dac_g[0]~output_o\);

-- Location: IOOBUF_X60_Y73_N23
\vga_dac_g[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|vga_dac_g[1]~1_combout\,
	devoe => ww_devoe,
	o => \vga_dac_g[1]~output_o\);

-- Location: IOOBUF_X58_Y73_N23
\vga_dac_g[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|vga_dac_g[2]~2_combout\,
	devoe => ww_devoe,
	o => \vga_dac_g[2]~output_o\);

-- Location: IOOBUF_X69_Y73_N16
\vga_dac_g[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|vga_dac_g[3]~3_combout\,
	devoe => ww_devoe,
	o => \vga_dac_g[3]~output_o\);

-- Location: IOOBUF_X69_Y0_N9
\vga_dac_g[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|vga_dac_g[4]~4_combout\,
	devoe => ww_devoe,
	o => \vga_dac_g[4]~output_o\);

-- Location: IOOBUF_X65_Y73_N16
\vga_dac_g[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|vga_dac_g[5]~5_combout\,
	devoe => ww_devoe,
	o => \vga_dac_g[5]~output_o\);

-- Location: IOOBUF_X67_Y0_N16
\vga_dac_g[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|vga_dac_g[6]~6_combout\,
	devoe => ww_devoe,
	o => \vga_dac_g[6]~output_o\);

-- Location: IOOBUF_X60_Y73_N9
\vga_dac_g[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|vga_dac_g[7]~7_combout\,
	devoe => ww_devoe,
	o => \vga_dac_g[7]~output_o\);

-- Location: IOOBUF_X58_Y73_N2
\vga_dac_b[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|vga_dac_b[0]~0_combout\,
	devoe => ww_devoe,
	o => \vga_dac_b[0]~output_o\);

-- Location: IOOBUF_X65_Y73_N9
\vga_dac_b[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|vga_dac_b[1]~1_combout\,
	devoe => ww_devoe,
	o => \vga_dac_b[1]~output_o\);

-- Location: IOOBUF_X67_Y0_N2
\vga_dac_b[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|vga_dac_b[2]~2_combout\,
	devoe => ww_devoe,
	o => \vga_dac_b[2]~output_o\);

-- Location: IOOBUF_X58_Y73_N9
\vga_dac_b[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|vga_dac_b[3]~3_combout\,
	devoe => ww_devoe,
	o => \vga_dac_b[3]~output_o\);

-- Location: IOOBUF_X60_Y73_N2
\vga_dac_b[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|vga_dac_b[4]~4_combout\,
	devoe => ww_devoe,
	o => \vga_dac_b[4]~output_o\);

-- Location: IOOBUF_X72_Y0_N9
\vga_dac_b[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|vga_dac_b[5]~5_combout\,
	devoe => ww_devoe,
	o => \vga_dac_b[5]~output_o\);

-- Location: IOOBUF_X79_Y0_N23
\vga_dac_b[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|vga_dac_b[6]~6_combout\,
	devoe => ww_devoe,
	o => \vga_dac_b[6]~output_o\);

-- Location: IOOBUF_X72_Y0_N2
\vga_dac_b[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_ctrl_inst|vga_dac_b[7]~7_combout\,
	devoe => ww_devoe,
	o => \vga_dac_b[7]~output_o\);

-- Location: IOIBUF_X0_Y36_N8
\clk~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk,
	o => \clk~input_o\);

-- Location: CLKCTRL_G2
\clk~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clk~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clk~inputclkctrl_outclk\);

-- Location: LCCOMB_X86_Y62_N6
\vga_ctrl_inst|Add0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|Add0~0_combout\ = \vga_ctrl_inst|hline_cnt\(0) $ (GND)
-- \vga_ctrl_inst|Add0~1\ = CARRY(!\vga_ctrl_inst|hline_cnt\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \vga_ctrl_inst|hline_cnt\(0),
	datad => VCC,
	combout => \vga_ctrl_inst|Add0~0_combout\,
	cout => \vga_ctrl_inst|Add0~1\);

-- Location: LCCOMB_X86_Y62_N4
\vga_ctrl_inst|hline_cnt~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|hline_cnt~2_combout\ = (\vga_ctrl_inst|Equal2~2_combout\) # (!\vga_ctrl_inst|Add0~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|Equal2~2_combout\,
	datad => \vga_ctrl_inst|Add0~0_combout\,
	combout => \vga_ctrl_inst|hline_cnt~2_combout\);

-- Location: IOIBUF_X0_Y36_N15
\res_n~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_res_n,
	o => \res_n~input_o\);

-- Location: CLKCTRL_G4
\res_n~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \res_n~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \res_n~inputclkctrl_outclk\);

-- Location: LCCOMB_X84_Y62_N10
\vga_ctrl_inst|Add1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|Add1~0_combout\ = \vga_ctrl_inst|line_clk_cnt\(0) $ (VCC)
-- \vga_ctrl_inst|Add1~1\ = CARRY(\vga_ctrl_inst|line_clk_cnt\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|line_clk_cnt\(0),
	datad => VCC,
	combout => \vga_ctrl_inst|Add1~0_combout\,
	cout => \vga_ctrl_inst|Add1~1\);

-- Location: FF_X84_Y62_N11
\vga_ctrl_inst|line_clk_cnt[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \vga_ctrl_inst|Add1~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_ctrl_inst|line_clk_cnt\(0));

-- Location: LCCOMB_X84_Y62_N12
\vga_ctrl_inst|Add1~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|Add1~2_combout\ = (\vga_ctrl_inst|line_clk_cnt\(1) & (!\vga_ctrl_inst|Add1~1\)) # (!\vga_ctrl_inst|line_clk_cnt\(1) & ((\vga_ctrl_inst|Add1~1\) # (GND)))
-- \vga_ctrl_inst|Add1~3\ = CARRY((!\vga_ctrl_inst|Add1~1\) # (!\vga_ctrl_inst|line_clk_cnt\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|line_clk_cnt\(1),
	datad => VCC,
	cin => \vga_ctrl_inst|Add1~1\,
	combout => \vga_ctrl_inst|Add1~2_combout\,
	cout => \vga_ctrl_inst|Add1~3\);

-- Location: FF_X84_Y62_N13
\vga_ctrl_inst|line_clk_cnt[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \vga_ctrl_inst|Add1~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_ctrl_inst|line_clk_cnt\(1));

-- Location: LCCOMB_X84_Y62_N14
\vga_ctrl_inst|Add1~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|Add1~4_combout\ = (\vga_ctrl_inst|line_clk_cnt\(2) & (\vga_ctrl_inst|Add1~3\ $ (GND))) # (!\vga_ctrl_inst|line_clk_cnt\(2) & (!\vga_ctrl_inst|Add1~3\ & VCC))
-- \vga_ctrl_inst|Add1~5\ = CARRY((\vga_ctrl_inst|line_clk_cnt\(2) & !\vga_ctrl_inst|Add1~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \vga_ctrl_inst|line_clk_cnt\(2),
	datad => VCC,
	cin => \vga_ctrl_inst|Add1~3\,
	combout => \vga_ctrl_inst|Add1~4_combout\,
	cout => \vga_ctrl_inst|Add1~5\);

-- Location: FF_X84_Y62_N15
\vga_ctrl_inst|line_clk_cnt[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \vga_ctrl_inst|Add1~4_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_ctrl_inst|line_clk_cnt\(2));

-- Location: LCCOMB_X84_Y62_N16
\vga_ctrl_inst|Add1~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|Add1~6_combout\ = (\vga_ctrl_inst|line_clk_cnt\(3) & (!\vga_ctrl_inst|Add1~5\)) # (!\vga_ctrl_inst|line_clk_cnt\(3) & ((\vga_ctrl_inst|Add1~5\) # (GND)))
-- \vga_ctrl_inst|Add1~7\ = CARRY((!\vga_ctrl_inst|Add1~5\) # (!\vga_ctrl_inst|line_clk_cnt\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \vga_ctrl_inst|line_clk_cnt\(3),
	datad => VCC,
	cin => \vga_ctrl_inst|Add1~5\,
	combout => \vga_ctrl_inst|Add1~6_combout\,
	cout => \vga_ctrl_inst|Add1~7\);

-- Location: FF_X84_Y62_N17
\vga_ctrl_inst|line_clk_cnt[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \vga_ctrl_inst|Add1~6_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_ctrl_inst|line_clk_cnt\(3));

-- Location: LCCOMB_X84_Y62_N18
\vga_ctrl_inst|Add1~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|Add1~8_combout\ = (\vga_ctrl_inst|line_clk_cnt\(4) & (\vga_ctrl_inst|Add1~7\ $ (GND))) # (!\vga_ctrl_inst|line_clk_cnt\(4) & (!\vga_ctrl_inst|Add1~7\ & VCC))
-- \vga_ctrl_inst|Add1~9\ = CARRY((\vga_ctrl_inst|line_clk_cnt\(4) & !\vga_ctrl_inst|Add1~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \vga_ctrl_inst|line_clk_cnt\(4),
	datad => VCC,
	cin => \vga_ctrl_inst|Add1~7\,
	combout => \vga_ctrl_inst|Add1~8_combout\,
	cout => \vga_ctrl_inst|Add1~9\);

-- Location: FF_X84_Y62_N19
\vga_ctrl_inst|line_clk_cnt[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \vga_ctrl_inst|Add1~8_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_ctrl_inst|line_clk_cnt\(4));

-- Location: LCCOMB_X84_Y62_N20
\vga_ctrl_inst|Add1~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|Add1~10_combout\ = (\vga_ctrl_inst|line_clk_cnt\(5) & (!\vga_ctrl_inst|Add1~9\)) # (!\vga_ctrl_inst|line_clk_cnt\(5) & ((\vga_ctrl_inst|Add1~9\) # (GND)))
-- \vga_ctrl_inst|Add1~11\ = CARRY((!\vga_ctrl_inst|Add1~9\) # (!\vga_ctrl_inst|line_clk_cnt\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \vga_ctrl_inst|line_clk_cnt\(5),
	datad => VCC,
	cin => \vga_ctrl_inst|Add1~9\,
	combout => \vga_ctrl_inst|Add1~10_combout\,
	cout => \vga_ctrl_inst|Add1~11\);

-- Location: LCCOMB_X84_Y62_N4
\vga_ctrl_inst|line_clk_cnt~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|line_clk_cnt~2_combout\ = (\vga_ctrl_inst|Add1~10_combout\ & !\vga_ctrl_inst|Equal0~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \vga_ctrl_inst|Add1~10_combout\,
	datad => \vga_ctrl_inst|Equal0~2_combout\,
	combout => \vga_ctrl_inst|line_clk_cnt~2_combout\);

-- Location: FF_X84_Y62_N5
\vga_ctrl_inst|line_clk_cnt[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \vga_ctrl_inst|line_clk_cnt~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_ctrl_inst|line_clk_cnt\(5));

-- Location: LCCOMB_X84_Y62_N22
\vga_ctrl_inst|Add1~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|Add1~12_combout\ = (\vga_ctrl_inst|line_clk_cnt\(6) & (\vga_ctrl_inst|Add1~11\ $ (GND))) # (!\vga_ctrl_inst|line_clk_cnt\(6) & (!\vga_ctrl_inst|Add1~11\ & VCC))
-- \vga_ctrl_inst|Add1~13\ = CARRY((\vga_ctrl_inst|line_clk_cnt\(6) & !\vga_ctrl_inst|Add1~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|line_clk_cnt\(6),
	datad => VCC,
	cin => \vga_ctrl_inst|Add1~11\,
	combout => \vga_ctrl_inst|Add1~12_combout\,
	cout => \vga_ctrl_inst|Add1~13\);

-- Location: FF_X84_Y62_N23
\vga_ctrl_inst|line_clk_cnt[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \vga_ctrl_inst|Add1~12_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_ctrl_inst|line_clk_cnt\(6));

-- Location: LCCOMB_X84_Y62_N24
\vga_ctrl_inst|Add1~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|Add1~14_combout\ = (\vga_ctrl_inst|line_clk_cnt\(7) & (!\vga_ctrl_inst|Add1~13\)) # (!\vga_ctrl_inst|line_clk_cnt\(7) & ((\vga_ctrl_inst|Add1~13\) # (GND)))
-- \vga_ctrl_inst|Add1~15\ = CARRY((!\vga_ctrl_inst|Add1~13\) # (!\vga_ctrl_inst|line_clk_cnt\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \vga_ctrl_inst|line_clk_cnt\(7),
	datad => VCC,
	cin => \vga_ctrl_inst|Add1~13\,
	combout => \vga_ctrl_inst|Add1~14_combout\,
	cout => \vga_ctrl_inst|Add1~15\);

-- Location: FF_X84_Y62_N25
\vga_ctrl_inst|line_clk_cnt[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \vga_ctrl_inst|Add1~14_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_ctrl_inst|line_clk_cnt\(7));

-- Location: LCCOMB_X84_Y62_N26
\vga_ctrl_inst|Add1~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|Add1~16_combout\ = (\vga_ctrl_inst|line_clk_cnt\(8) & (\vga_ctrl_inst|Add1~15\ $ (GND))) # (!\vga_ctrl_inst|line_clk_cnt\(8) & (!\vga_ctrl_inst|Add1~15\ & VCC))
-- \vga_ctrl_inst|Add1~17\ = CARRY((\vga_ctrl_inst|line_clk_cnt\(8) & !\vga_ctrl_inst|Add1~15\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \vga_ctrl_inst|line_clk_cnt\(8),
	datad => VCC,
	cin => \vga_ctrl_inst|Add1~15\,
	combout => \vga_ctrl_inst|Add1~16_combout\,
	cout => \vga_ctrl_inst|Add1~17\);

-- Location: LCCOMB_X85_Y62_N16
\vga_ctrl_inst|line_clk_cnt~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|line_clk_cnt~0_combout\ = (!\vga_ctrl_inst|Equal0~2_combout\ & \vga_ctrl_inst|Add1~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|Equal0~2_combout\,
	datac => \vga_ctrl_inst|Add1~16_combout\,
	combout => \vga_ctrl_inst|line_clk_cnt~0_combout\);

-- Location: FF_X85_Y62_N17
\vga_ctrl_inst|line_clk_cnt[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \vga_ctrl_inst|line_clk_cnt~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_ctrl_inst|line_clk_cnt\(8));

-- Location: LCCOMB_X84_Y62_N28
\vga_ctrl_inst|Add1~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|Add1~18_combout\ = \vga_ctrl_inst|line_clk_cnt\(9) $ (\vga_ctrl_inst|Add1~17\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \vga_ctrl_inst|line_clk_cnt\(9),
	cin => \vga_ctrl_inst|Add1~17\,
	combout => \vga_ctrl_inst|Add1~18_combout\);

-- Location: LCCOMB_X84_Y62_N8
\vga_ctrl_inst|line_clk_cnt~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|line_clk_cnt~1_combout\ = (\vga_ctrl_inst|Add1~18_combout\ & !\vga_ctrl_inst|Equal0~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \vga_ctrl_inst|Add1~18_combout\,
	datad => \vga_ctrl_inst|Equal0~2_combout\,
	combout => \vga_ctrl_inst|line_clk_cnt~1_combout\);

-- Location: FF_X84_Y62_N9
\vga_ctrl_inst|line_clk_cnt[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \vga_ctrl_inst|line_clk_cnt~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_ctrl_inst|line_clk_cnt\(9));

-- Location: LCCOMB_X84_Y62_N6
\vga_ctrl_inst|Equal0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|Equal0~1_combout\ = (!\vga_ctrl_inst|line_clk_cnt\(6) & (!\vga_ctrl_inst|line_clk_cnt\(5) & (\vga_ctrl_inst|line_clk_cnt\(9) & \vga_ctrl_inst|line_clk_cnt\(8))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|line_clk_cnt\(6),
	datab => \vga_ctrl_inst|line_clk_cnt\(5),
	datac => \vga_ctrl_inst|line_clk_cnt\(9),
	datad => \vga_ctrl_inst|line_clk_cnt\(8),
	combout => \vga_ctrl_inst|Equal0~1_combout\);

-- Location: LCCOMB_X84_Y62_N2
\vga_ctrl_inst|Equal0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|Equal0~0_combout\ = (\vga_ctrl_inst|line_clk_cnt\(1) & (\vga_ctrl_inst|line_clk_cnt\(3) & (\vga_ctrl_inst|line_clk_cnt\(2) & \vga_ctrl_inst|line_clk_cnt\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|line_clk_cnt\(1),
	datab => \vga_ctrl_inst|line_clk_cnt\(3),
	datac => \vga_ctrl_inst|line_clk_cnt\(2),
	datad => \vga_ctrl_inst|line_clk_cnt\(0),
	combout => \vga_ctrl_inst|Equal0~0_combout\);

-- Location: LCCOMB_X84_Y62_N0
\vga_ctrl_inst|Equal0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|Equal0~2_combout\ = (\vga_ctrl_inst|Equal0~1_combout\ & (\vga_ctrl_inst|Equal0~0_combout\ & (\vga_ctrl_inst|line_clk_cnt\(4) & !\vga_ctrl_inst|line_clk_cnt\(7))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|Equal0~1_combout\,
	datab => \vga_ctrl_inst|Equal0~0_combout\,
	datac => \vga_ctrl_inst|line_clk_cnt\(4),
	datad => \vga_ctrl_inst|line_clk_cnt\(7),
	combout => \vga_ctrl_inst|Equal0~2_combout\);

-- Location: FF_X86_Y62_N5
\vga_ctrl_inst|hline_cnt[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \vga_ctrl_inst|hline_cnt~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \vga_ctrl_inst|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_ctrl_inst|hline_cnt\(0));

-- Location: LCCOMB_X86_Y62_N8
\vga_ctrl_inst|Add0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|Add0~2_combout\ = (\vga_ctrl_inst|hline_cnt\(1) & (!\vga_ctrl_inst|Add0~1\)) # (!\vga_ctrl_inst|hline_cnt\(1) & ((\vga_ctrl_inst|Add0~1\) # (GND)))
-- \vga_ctrl_inst|Add0~3\ = CARRY((!\vga_ctrl_inst|Add0~1\) # (!\vga_ctrl_inst|hline_cnt\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \vga_ctrl_inst|hline_cnt\(1),
	datad => VCC,
	cin => \vga_ctrl_inst|Add0~1\,
	combout => \vga_ctrl_inst|Add0~2_combout\,
	cout => \vga_ctrl_inst|Add0~3\);

-- Location: FF_X86_Y62_N9
\vga_ctrl_inst|hline_cnt[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \vga_ctrl_inst|Add0~2_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \vga_ctrl_inst|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_ctrl_inst|hline_cnt\(1));

-- Location: LCCOMB_X86_Y62_N10
\vga_ctrl_inst|Add0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|Add0~4_combout\ = (\vga_ctrl_inst|hline_cnt\(2) & (\vga_ctrl_inst|Add0~3\ $ (GND))) # (!\vga_ctrl_inst|hline_cnt\(2) & (!\vga_ctrl_inst|Add0~3\ & VCC))
-- \vga_ctrl_inst|Add0~5\ = CARRY((\vga_ctrl_inst|hline_cnt\(2) & !\vga_ctrl_inst|Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|hline_cnt\(2),
	datad => VCC,
	cin => \vga_ctrl_inst|Add0~3\,
	combout => \vga_ctrl_inst|Add0~4_combout\,
	cout => \vga_ctrl_inst|Add0~5\);

-- Location: LCCOMB_X86_Y62_N12
\vga_ctrl_inst|Add0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|Add0~6_combout\ = (\vga_ctrl_inst|hline_cnt\(3) & (!\vga_ctrl_inst|Add0~5\)) # (!\vga_ctrl_inst|hline_cnt\(3) & ((\vga_ctrl_inst|Add0~5\) # (GND)))
-- \vga_ctrl_inst|Add0~7\ = CARRY((!\vga_ctrl_inst|Add0~5\) # (!\vga_ctrl_inst|hline_cnt\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|hline_cnt\(3),
	datad => VCC,
	cin => \vga_ctrl_inst|Add0~5\,
	combout => \vga_ctrl_inst|Add0~6_combout\,
	cout => \vga_ctrl_inst|Add0~7\);

-- Location: LCCOMB_X86_Y62_N26
\vga_ctrl_inst|hline_cnt~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|hline_cnt~1_combout\ = (!\vga_ctrl_inst|Equal2~2_combout\ & \vga_ctrl_inst|Add0~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|Equal2~2_combout\,
	datac => \vga_ctrl_inst|Add0~6_combout\,
	combout => \vga_ctrl_inst|hline_cnt~1_combout\);

-- Location: FF_X86_Y62_N27
\vga_ctrl_inst|hline_cnt[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \vga_ctrl_inst|hline_cnt~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \vga_ctrl_inst|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_ctrl_inst|hline_cnt\(3));

-- Location: LCCOMB_X86_Y62_N14
\vga_ctrl_inst|Add0~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|Add0~8_combout\ = (\vga_ctrl_inst|hline_cnt\(4) & (\vga_ctrl_inst|Add0~7\ $ (GND))) # (!\vga_ctrl_inst|hline_cnt\(4) & (!\vga_ctrl_inst|Add0~7\ & VCC))
-- \vga_ctrl_inst|Add0~9\ = CARRY((\vga_ctrl_inst|hline_cnt\(4) & !\vga_ctrl_inst|Add0~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \vga_ctrl_inst|hline_cnt\(4),
	datad => VCC,
	cin => \vga_ctrl_inst|Add0~7\,
	combout => \vga_ctrl_inst|Add0~8_combout\,
	cout => \vga_ctrl_inst|Add0~9\);

-- Location: FF_X86_Y62_N15
\vga_ctrl_inst|hline_cnt[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \vga_ctrl_inst|Add0~8_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \vga_ctrl_inst|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_ctrl_inst|hline_cnt\(4));

-- Location: LCCOMB_X86_Y62_N16
\vga_ctrl_inst|Add0~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|Add0~10_combout\ = (\vga_ctrl_inst|hline_cnt\(5) & ((\vga_ctrl_inst|Add0~9\) # (GND))) # (!\vga_ctrl_inst|hline_cnt\(5) & (!\vga_ctrl_inst|Add0~9\))
-- \vga_ctrl_inst|Add0~11\ = CARRY((\vga_ctrl_inst|hline_cnt\(5)) # (!\vga_ctrl_inst|Add0~9\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001111001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \vga_ctrl_inst|hline_cnt\(5),
	datad => VCC,
	cin => \vga_ctrl_inst|Add0~9\,
	combout => \vga_ctrl_inst|Add0~10_combout\,
	cout => \vga_ctrl_inst|Add0~11\);

-- Location: LCCOMB_X86_Y62_N28
\vga_ctrl_inst|hline_cnt[5]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|hline_cnt[5]~7_combout\ = !\vga_ctrl_inst|Add0~10_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \vga_ctrl_inst|Add0~10_combout\,
	combout => \vga_ctrl_inst|hline_cnt[5]~7_combout\);

-- Location: FF_X86_Y62_N29
\vga_ctrl_inst|hline_cnt[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \vga_ctrl_inst|hline_cnt[5]~7_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \vga_ctrl_inst|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_ctrl_inst|hline_cnt\(5));

-- Location: LCCOMB_X86_Y62_N18
\vga_ctrl_inst|Add0~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|Add0~12_combout\ = (\vga_ctrl_inst|hline_cnt\(6) & (!\vga_ctrl_inst|Add0~11\ & VCC)) # (!\vga_ctrl_inst|hline_cnt\(6) & (\vga_ctrl_inst|Add0~11\ $ (GND)))
-- \vga_ctrl_inst|Add0~13\ = CARRY((!\vga_ctrl_inst|hline_cnt\(6) & !\vga_ctrl_inst|Add0~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \vga_ctrl_inst|hline_cnt\(6),
	datad => VCC,
	cin => \vga_ctrl_inst|Add0~11\,
	combout => \vga_ctrl_inst|Add0~12_combout\,
	cout => \vga_ctrl_inst|Add0~13\);

-- Location: LCCOMB_X86_Y62_N2
\vga_ctrl_inst|hline_cnt[6]~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|hline_cnt[6]~6_combout\ = !\vga_ctrl_inst|Add0~12_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \vga_ctrl_inst|Add0~12_combout\,
	combout => \vga_ctrl_inst|hline_cnt[6]~6_combout\);

-- Location: FF_X86_Y62_N3
\vga_ctrl_inst|hline_cnt[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \vga_ctrl_inst|hline_cnt[6]~6_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \vga_ctrl_inst|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_ctrl_inst|hline_cnt\(6));

-- Location: LCCOMB_X86_Y62_N20
\vga_ctrl_inst|Add0~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|Add0~14_combout\ = (\vga_ctrl_inst|hline_cnt\(7) & ((\vga_ctrl_inst|Add0~13\) # (GND))) # (!\vga_ctrl_inst|hline_cnt\(7) & (!\vga_ctrl_inst|Add0~13\))
-- \vga_ctrl_inst|Add0~15\ = CARRY((\vga_ctrl_inst|hline_cnt\(7)) # (!\vga_ctrl_inst|Add0~13\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001111001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \vga_ctrl_inst|hline_cnt\(7),
	datad => VCC,
	cin => \vga_ctrl_inst|Add0~13\,
	combout => \vga_ctrl_inst|Add0~14_combout\,
	cout => \vga_ctrl_inst|Add0~15\);

-- Location: LCCOMB_X85_Y62_N6
\vga_ctrl_inst|hline_cnt[7]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|hline_cnt[7]~5_combout\ = !\vga_ctrl_inst|Add0~14_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \vga_ctrl_inst|Add0~14_combout\,
	combout => \vga_ctrl_inst|hline_cnt[7]~5_combout\);

-- Location: FF_X85_Y62_N7
\vga_ctrl_inst|hline_cnt[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \vga_ctrl_inst|hline_cnt[7]~5_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \vga_ctrl_inst|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_ctrl_inst|hline_cnt\(7));

-- Location: LCCOMB_X86_Y62_N22
\vga_ctrl_inst|Add0~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|Add0~16_combout\ = (\vga_ctrl_inst|hline_cnt\(8) & (!\vga_ctrl_inst|Add0~15\ & VCC)) # (!\vga_ctrl_inst|hline_cnt\(8) & (\vga_ctrl_inst|Add0~15\ $ (GND)))
-- \vga_ctrl_inst|Add0~17\ = CARRY((!\vga_ctrl_inst|hline_cnt\(8) & !\vga_ctrl_inst|Add0~15\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101000000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|hline_cnt\(8),
	datad => VCC,
	cin => \vga_ctrl_inst|Add0~15\,
	combout => \vga_ctrl_inst|Add0~16_combout\,
	cout => \vga_ctrl_inst|Add0~17\);

-- Location: LCCOMB_X85_Y62_N12
\vga_ctrl_inst|hline_cnt[8]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|hline_cnt[8]~4_combout\ = !\vga_ctrl_inst|Add0~16_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \vga_ctrl_inst|Add0~16_combout\,
	combout => \vga_ctrl_inst|hline_cnt[8]~4_combout\);

-- Location: FF_X85_Y62_N13
\vga_ctrl_inst|hline_cnt[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \vga_ctrl_inst|hline_cnt[8]~4_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \vga_ctrl_inst|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_ctrl_inst|hline_cnt\(8));

-- Location: LCCOMB_X86_Y62_N24
\vga_ctrl_inst|Add0~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|Add0~18_combout\ = \vga_ctrl_inst|Add0~17\ $ (\vga_ctrl_inst|hline_cnt\(9))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \vga_ctrl_inst|hline_cnt\(9),
	cin => \vga_ctrl_inst|Add0~17\,
	combout => \vga_ctrl_inst|Add0~18_combout\);

-- Location: LCCOMB_X86_Y62_N0
\vga_ctrl_inst|hline_cnt~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|hline_cnt~0_combout\ = (!\vga_ctrl_inst|Equal2~2_combout\ & \vga_ctrl_inst|Add0~18_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|Equal2~2_combout\,
	datad => \vga_ctrl_inst|Add0~18_combout\,
	combout => \vga_ctrl_inst|hline_cnt~0_combout\);

-- Location: FF_X86_Y62_N1
\vga_ctrl_inst|hline_cnt[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \vga_ctrl_inst|hline_cnt~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \vga_ctrl_inst|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_ctrl_inst|hline_cnt\(9));

-- Location: LCCOMB_X85_Y62_N0
\vga_ctrl_inst|Equal2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|Equal2~0_combout\ = (\vga_ctrl_inst|hline_cnt\(8) & (\vga_ctrl_inst|hline_cnt\(9) & (\vga_ctrl_inst|hline_cnt\(3) & !\vga_ctrl_inst|hline_cnt\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|hline_cnt\(8),
	datab => \vga_ctrl_inst|hline_cnt\(9),
	datac => \vga_ctrl_inst|hline_cnt\(3),
	datad => \vga_ctrl_inst|hline_cnt\(4),
	combout => \vga_ctrl_inst|Equal2~0_combout\);

-- Location: LCCOMB_X85_Y62_N2
\vga_ctrl_inst|Equal2~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|Equal2~1_combout\ = (\vga_ctrl_inst|hline_cnt\(6) & (\vga_ctrl_inst|Equal2~0_combout\ & (\vga_ctrl_inst|hline_cnt\(7) & \vga_ctrl_inst|hline_cnt\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|hline_cnt\(6),
	datab => \vga_ctrl_inst|Equal2~0_combout\,
	datac => \vga_ctrl_inst|hline_cnt\(7),
	datad => \vga_ctrl_inst|hline_cnt\(5),
	combout => \vga_ctrl_inst|Equal2~1_combout\);

-- Location: LCCOMB_X85_Y62_N14
\vga_ctrl_inst|Equal2~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|Equal2~2_combout\ = (\vga_ctrl_inst|hline_cnt\(2) & (\vga_ctrl_inst|Equal2~1_combout\ & (!\vga_ctrl_inst|hline_cnt\(1) & \vga_ctrl_inst|hline_cnt\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|hline_cnt\(2),
	datab => \vga_ctrl_inst|Equal2~1_combout\,
	datac => \vga_ctrl_inst|hline_cnt\(1),
	datad => \vga_ctrl_inst|hline_cnt\(0),
	combout => \vga_ctrl_inst|Equal2~2_combout\);

-- Location: LCCOMB_X86_Y62_N30
\vga_ctrl_inst|hline_cnt~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|hline_cnt~3_combout\ = (!\vga_ctrl_inst|Equal2~2_combout\ & \vga_ctrl_inst|Add0~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|Equal2~2_combout\,
	datac => \vga_ctrl_inst|Add0~4_combout\,
	combout => \vga_ctrl_inst|hline_cnt~3_combout\);

-- Location: FF_X86_Y62_N31
\vga_ctrl_inst|hline_cnt[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \vga_ctrl_inst|hline_cnt~3_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \vga_ctrl_inst|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_ctrl_inst|hline_cnt\(2));

-- Location: LCCOMB_X85_Y62_N4
\vga_ctrl_inst|frame_start~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|frame_start~0_combout\ = (!\vga_ctrl_inst|hline_cnt\(2) & (\vga_ctrl_inst|hline_cnt\(1) & !\vga_ctrl_inst|hline_cnt\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|hline_cnt\(2),
	datac => \vga_ctrl_inst|hline_cnt\(1),
	datad => \vga_ctrl_inst|hline_cnt\(0),
	combout => \vga_ctrl_inst|frame_start~0_combout\);

-- Location: LCCOMB_X83_Y62_N24
\vga_ctrl_inst|frame_start~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|frame_start~1_combout\ = (\vga_ctrl_inst|frame_start~0_combout\ & (\vga_ctrl_inst|Equal2~1_combout\ & \vga_ctrl_inst|Equal0~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|frame_start~0_combout\,
	datab => \vga_ctrl_inst|Equal2~1_combout\,
	datac => \vga_ctrl_inst|Equal0~2_combout\,
	combout => \vga_ctrl_inst|frame_start~1_combout\);

-- Location: FF_X83_Y62_N25
\vga_ctrl_inst|frame_start\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \vga_ctrl_inst|frame_start~1_combout\,
	ena => \res_n~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_ctrl_inst|frame_start~q\);

-- Location: LCCOMB_X85_Y62_N24
\vga_ctrl_inst|snyc~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|snyc~0_combout\ = (!\vga_ctrl_inst|hline_cnt\(7) & (!\vga_ctrl_inst|hline_cnt\(5) & (!\vga_ctrl_inst|hline_cnt\(8) & !\vga_ctrl_inst|hline_cnt\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|hline_cnt\(7),
	datab => \vga_ctrl_inst|hline_cnt\(5),
	datac => \vga_ctrl_inst|hline_cnt\(8),
	datad => \vga_ctrl_inst|hline_cnt\(6),
	combout => \vga_ctrl_inst|snyc~0_combout\);

-- Location: LCCOMB_X85_Y62_N26
\vga_ctrl_inst|pix_ack~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|pix_ack~0_combout\ = (!\vga_ctrl_inst|hline_cnt\(9) & (((!\vga_ctrl_inst|line_clk_cnt\(7) & !\vga_ctrl_inst|line_clk_cnt\(8))) # (!\vga_ctrl_inst|line_clk_cnt\(9))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010100000111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|line_clk_cnt\(9),
	datab => \vga_ctrl_inst|line_clk_cnt\(7),
	datac => \vga_ctrl_inst|hline_cnt\(9),
	datad => \vga_ctrl_inst|line_clk_cnt\(8),
	combout => \vga_ctrl_inst|pix_ack~0_combout\);

-- Location: LCCOMB_X85_Y62_N10
\vga_ctrl_inst|pix_ack~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|pix_ack~1_combout\ = (!\vga_ctrl_inst|snyc~0_combout\ & \vga_ctrl_inst|pix_ack~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \vga_ctrl_inst|snyc~0_combout\,
	datac => \vga_ctrl_inst|pix_ack~0_combout\,
	combout => \vga_ctrl_inst|pix_ack~1_combout\);

-- Location: LCCOMB_X84_Y62_N30
\vga_ctrl_inst|vga_hsync_int~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_hsync_int~0_combout\ = (\vga_ctrl_inst|line_clk_cnt\(6) & (\vga_ctrl_inst|line_clk_cnt\(5) & ((\vga_ctrl_inst|line_clk_cnt\(4)) # (\vga_ctrl_inst|Equal0~0_combout\)))) # (!\vga_ctrl_inst|line_clk_cnt\(6) & 
-- (!\vga_ctrl_inst|line_clk_cnt\(4) & (!\vga_ctrl_inst|line_clk_cnt\(5) & !\vga_ctrl_inst|Equal0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|line_clk_cnt\(6),
	datab => \vga_ctrl_inst|line_clk_cnt\(4),
	datac => \vga_ctrl_inst|line_clk_cnt\(5),
	datad => \vga_ctrl_inst|Equal0~0_combout\,
	combout => \vga_ctrl_inst|vga_hsync_int~0_combout\);

-- Location: LCCOMB_X85_Y62_N8
\vga_ctrl_inst|vga_hsync_int~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_hsync_int~1_combout\ = (\vga_ctrl_inst|line_clk_cnt\(9) & (!\vga_ctrl_inst|vga_hsync_int~0_combout\ & (!\vga_ctrl_inst|line_clk_cnt\(8) & \vga_ctrl_inst|line_clk_cnt\(7))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|line_clk_cnt\(9),
	datab => \vga_ctrl_inst|vga_hsync_int~0_combout\,
	datac => \vga_ctrl_inst|line_clk_cnt\(8),
	datad => \vga_ctrl_inst|line_clk_cnt\(7),
	combout => \vga_ctrl_inst|vga_hsync_int~1_combout\);

-- Location: FF_X85_Y62_N9
\vga_ctrl_inst|vga_hsync_int\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \vga_ctrl_inst|vga_hsync_int~1_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_ctrl_inst|vga_hsync_int~q\);

-- Location: FF_X85_Y62_N21
\vga_ctrl_inst|vga_hsync\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \vga_ctrl_inst|vga_hsync_int~q\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_ctrl_inst|vga_hsync~q\);

-- Location: LCCOMB_X85_Y62_N28
\vga_ctrl_inst|snyc~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|snyc~1_combout\ = (\vga_ctrl_inst|hline_cnt\(2)) # ((\vga_ctrl_inst|hline_cnt\(4)) # ((!\vga_ctrl_inst|hline_cnt\(3)) # (!\vga_ctrl_inst|hline_cnt\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|hline_cnt\(2),
	datab => \vga_ctrl_inst|hline_cnt\(4),
	datac => \vga_ctrl_inst|hline_cnt\(1),
	datad => \vga_ctrl_inst|hline_cnt\(3),
	combout => \vga_ctrl_inst|snyc~1_combout\);

-- Location: LCCOMB_X85_Y62_N30
\vga_ctrl_inst|snyc~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|snyc~2_combout\ = (\vga_ctrl_inst|snyc~1_combout\) # ((\vga_ctrl_inst|hline_cnt\(9)) # (!\vga_ctrl_inst|snyc~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \vga_ctrl_inst|snyc~1_combout\,
	datac => \vga_ctrl_inst|hline_cnt\(9),
	datad => \vga_ctrl_inst|snyc~0_combout\,
	combout => \vga_ctrl_inst|snyc~2_combout\);

-- Location: LCCOMB_X85_Y62_N18
\vga_ctrl_inst|vga_vsync_int~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_vsync_int~0_combout\ = (\vga_ctrl_inst|Equal2~2_combout\ & (((\vga_ctrl_inst|vga_vsync_int~q\)))) # (!\vga_ctrl_inst|Equal2~2_combout\ & ((\vga_ctrl_inst|Equal0~2_combout\ & (!\vga_ctrl_inst|snyc~2_combout\)) # 
-- (!\vga_ctrl_inst|Equal0~2_combout\ & ((\vga_ctrl_inst|vga_vsync_int~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|snyc~2_combout\,
	datab => \vga_ctrl_inst|Equal2~2_combout\,
	datac => \vga_ctrl_inst|vga_vsync_int~q\,
	datad => \vga_ctrl_inst|Equal0~2_combout\,
	combout => \vga_ctrl_inst|vga_vsync_int~0_combout\);

-- Location: FF_X85_Y62_N19
\vga_ctrl_inst|vga_vsync_int\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \vga_ctrl_inst|vga_vsync_int~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_ctrl_inst|vga_vsync_int~q\);

-- Location: LCCOMB_X85_Y62_N22
\vga_ctrl_inst|vga_vsync~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_vsync~feeder_combout\ = \vga_ctrl_inst|vga_vsync_int~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \vga_ctrl_inst|vga_vsync_int~q\,
	combout => \vga_ctrl_inst|vga_vsync~feeder_combout\);

-- Location: FF_X85_Y62_N23
\vga_ctrl_inst|vga_vsync\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \vga_ctrl_inst|vga_vsync~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_ctrl_inst|vga_vsync~q\);

-- Location: IOIBUF_X115_Y37_N8
\pix_data_r[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pix_data_r(0),
	o => \pix_data_r[0]~input_o\);

-- Location: LCCOMB_X74_Y1_N8
\vga_ctrl_inst|vga_dac_r[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_dac_r[0]~0_combout\ = (!\vga_ctrl_inst|snyc~0_combout\ & (\pix_data_r[0]~input_o\ & \vga_ctrl_inst|pix_ack~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|snyc~0_combout\,
	datac => \pix_data_r[0]~input_o\,
	datad => \vga_ctrl_inst|pix_ack~0_combout\,
	combout => \vga_ctrl_inst|vga_dac_r[0]~0_combout\);

-- Location: IOIBUF_X115_Y37_N1
\pix_data_r[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pix_data_r(1),
	o => \pix_data_r[1]~input_o\);

-- Location: LCCOMB_X66_Y72_N16
\vga_ctrl_inst|vga_dac_r[1]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_dac_r[1]~1_combout\ = (\vga_ctrl_inst|pix_ack~0_combout\ & (!\vga_ctrl_inst|snyc~0_combout\ & \pix_data_r[1]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|pix_ack~0_combout\,
	datac => \vga_ctrl_inst|snyc~0_combout\,
	datad => \pix_data_r[1]~input_o\,
	combout => \vga_ctrl_inst|vga_dac_r[1]~1_combout\);

-- Location: IOIBUF_X74_Y0_N1
\pix_data_r[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pix_data_r(2),
	o => \pix_data_r[2]~input_o\);

-- Location: LCCOMB_X74_Y1_N18
\vga_ctrl_inst|vga_dac_r[2]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_dac_r[2]~2_combout\ = (\pix_data_r[2]~input_o\ & (!\vga_ctrl_inst|snyc~0_combout\ & \vga_ctrl_inst|pix_ack~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \pix_data_r[2]~input_o\,
	datac => \vga_ctrl_inst|snyc~0_combout\,
	datad => \vga_ctrl_inst|pix_ack~0_combout\,
	combout => \vga_ctrl_inst|vga_dac_r[2]~2_combout\);

-- Location: IOIBUF_X67_Y73_N22
\pix_data_r[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pix_data_r(3),
	o => \pix_data_r[3]~input_o\);

-- Location: LCCOMB_X66_Y72_N18
\vga_ctrl_inst|vga_dac_r[3]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_dac_r[3]~3_combout\ = (\pix_data_r[3]~input_o\ & (!\vga_ctrl_inst|snyc~0_combout\ & \vga_ctrl_inst|pix_ack~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \pix_data_r[3]~input_o\,
	datac => \vga_ctrl_inst|snyc~0_combout\,
	datad => \vga_ctrl_inst|pix_ack~0_combout\,
	combout => \vga_ctrl_inst|vga_dac_r[3]~3_combout\);

-- Location: IOIBUF_X67_Y73_N1
\pix_data_r[4]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pix_data_r(4),
	o => \pix_data_r[4]~input_o\);

-- Location: LCCOMB_X66_Y72_N4
\vga_ctrl_inst|vga_dac_r[4]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_dac_r[4]~4_combout\ = (\pix_data_r[4]~input_o\ & (!\vga_ctrl_inst|snyc~0_combout\ & \vga_ctrl_inst|pix_ack~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \pix_data_r[4]~input_o\,
	datac => \vga_ctrl_inst|snyc~0_combout\,
	datad => \vga_ctrl_inst|pix_ack~0_combout\,
	combout => \vga_ctrl_inst|vga_dac_r[4]~4_combout\);

-- Location: IOIBUF_X67_Y0_N22
\pix_data_r[5]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pix_data_r(5),
	o => \pix_data_r[5]~input_o\);

-- Location: LCCOMB_X74_Y1_N28
\vga_ctrl_inst|vga_dac_r[5]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_dac_r[5]~5_combout\ = (\pix_data_r[5]~input_o\ & (!\vga_ctrl_inst|snyc~0_combout\ & \vga_ctrl_inst|pix_ack~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pix_data_r[5]~input_o\,
	datac => \vga_ctrl_inst|snyc~0_combout\,
	datad => \vga_ctrl_inst|pix_ack~0_combout\,
	combout => \vga_ctrl_inst|vga_dac_r[5]~5_combout\);

-- Location: IOIBUF_X79_Y73_N1
\pix_data_r[6]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pix_data_r(6),
	o => \pix_data_r[6]~input_o\);

-- Location: LCCOMB_X66_Y72_N6
\vga_ctrl_inst|vga_dac_r[6]~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_dac_r[6]~6_combout\ = (\pix_data_r[6]~input_o\ & (!\vga_ctrl_inst|snyc~0_combout\ & \vga_ctrl_inst|pix_ack~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pix_data_r[6]~input_o\,
	datac => \vga_ctrl_inst|snyc~0_combout\,
	datad => \vga_ctrl_inst|pix_ack~0_combout\,
	combout => \vga_ctrl_inst|vga_dac_r[6]~6_combout\);

-- Location: IOIBUF_X81_Y73_N1
\pix_data_r[7]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pix_data_r(7),
	o => \pix_data_r[7]~input_o\);

-- Location: LCCOMB_X66_Y72_N8
\vga_ctrl_inst|vga_dac_r[7]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_dac_r[7]~7_combout\ = (\pix_data_r[7]~input_o\ & (!\vga_ctrl_inst|snyc~0_combout\ & \vga_ctrl_inst|pix_ack~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \pix_data_r[7]~input_o\,
	datac => \vga_ctrl_inst|snyc~0_combout\,
	datad => \vga_ctrl_inst|pix_ack~0_combout\,
	combout => \vga_ctrl_inst|vga_dac_r[7]~7_combout\);

-- Location: IOIBUF_X74_Y73_N15
\pix_data_g[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pix_data_g(0),
	o => \pix_data_g[0]~input_o\);

-- Location: LCCOMB_X66_Y72_N26
\vga_ctrl_inst|vga_dac_g[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_dac_g[0]~0_combout\ = (\pix_data_g[0]~input_o\ & (!\vga_ctrl_inst|snyc~0_combout\ & \vga_ctrl_inst|pix_ack~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \pix_data_g[0]~input_o\,
	datac => \vga_ctrl_inst|snyc~0_combout\,
	datad => \vga_ctrl_inst|pix_ack~0_combout\,
	combout => \vga_ctrl_inst|vga_dac_g[0]~0_combout\);

-- Location: IOIBUF_X72_Y73_N15
\pix_data_g[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pix_data_g(1),
	o => \pix_data_g[1]~input_o\);

-- Location: LCCOMB_X66_Y72_N12
\vga_ctrl_inst|vga_dac_g[1]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_dac_g[1]~1_combout\ = (\pix_data_g[1]~input_o\ & (!\vga_ctrl_inst|snyc~0_combout\ & \vga_ctrl_inst|pix_ack~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pix_data_g[1]~input_o\,
	datac => \vga_ctrl_inst|snyc~0_combout\,
	datad => \vga_ctrl_inst|pix_ack~0_combout\,
	combout => \vga_ctrl_inst|vga_dac_g[1]~1_combout\);

-- Location: IOIBUF_X72_Y73_N1
\pix_data_g[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pix_data_g(2),
	o => \pix_data_g[2]~input_o\);

-- Location: LCCOMB_X66_Y72_N14
\vga_ctrl_inst|vga_dac_g[2]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_dac_g[2]~2_combout\ = (\pix_data_g[2]~input_o\ & (!\vga_ctrl_inst|snyc~0_combout\ & \vga_ctrl_inst|pix_ack~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pix_data_g[2]~input_o\,
	datac => \vga_ctrl_inst|snyc~0_combout\,
	datad => \vga_ctrl_inst|pix_ack~0_combout\,
	combout => \vga_ctrl_inst|vga_dac_g[2]~2_combout\);

-- Location: IOIBUF_X79_Y73_N8
\pix_data_g[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pix_data_g(3),
	o => \pix_data_g[3]~input_o\);

-- Location: LCCOMB_X66_Y72_N24
\vga_ctrl_inst|vga_dac_g[3]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_dac_g[3]~3_combout\ = (\pix_data_g[3]~input_o\ & (!\vga_ctrl_inst|snyc~0_combout\ & \vga_ctrl_inst|pix_ack~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \pix_data_g[3]~input_o\,
	datac => \vga_ctrl_inst|snyc~0_combout\,
	datad => \vga_ctrl_inst|pix_ack~0_combout\,
	combout => \vga_ctrl_inst|vga_dac_g[3]~3_combout\);

-- Location: IOIBUF_X74_Y0_N22
\pix_data_g[4]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pix_data_g(4),
	o => \pix_data_g[4]~input_o\);

-- Location: LCCOMB_X74_Y1_N22
\vga_ctrl_inst|vga_dac_g[4]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_dac_g[4]~4_combout\ = (\pix_data_g[4]~input_o\ & (!\vga_ctrl_inst|snyc~0_combout\ & \vga_ctrl_inst|pix_ack~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \pix_data_g[4]~input_o\,
	datac => \vga_ctrl_inst|snyc~0_combout\,
	datad => \vga_ctrl_inst|pix_ack~0_combout\,
	combout => \vga_ctrl_inst|vga_dac_g[4]~4_combout\);

-- Location: IOIBUF_X67_Y73_N8
\pix_data_g[5]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pix_data_g(5),
	o => \pix_data_g[5]~input_o\);

-- Location: LCCOMB_X66_Y72_N10
\vga_ctrl_inst|vga_dac_g[5]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_dac_g[5]~5_combout\ = (\pix_data_g[5]~input_o\ & (!\vga_ctrl_inst|snyc~0_combout\ & \vga_ctrl_inst|pix_ack~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pix_data_g[5]~input_o\,
	datac => \vga_ctrl_inst|snyc~0_combout\,
	datad => \vga_ctrl_inst|pix_ack~0_combout\,
	combout => \vga_ctrl_inst|vga_dac_g[5]~5_combout\);

-- Location: IOIBUF_X74_Y0_N8
\pix_data_g[6]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pix_data_g(6),
	o => \pix_data_g[6]~input_o\);

-- Location: LCCOMB_X74_Y1_N0
\vga_ctrl_inst|vga_dac_g[6]~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_dac_g[6]~6_combout\ = (\pix_data_g[6]~input_o\ & (!\vga_ctrl_inst|snyc~0_combout\ & \vga_ctrl_inst|pix_ack~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pix_data_g[6]~input_o\,
	datac => \vga_ctrl_inst|snyc~0_combout\,
	datad => \vga_ctrl_inst|pix_ack~0_combout\,
	combout => \vga_ctrl_inst|vga_dac_g[6]~6_combout\);

-- Location: IOIBUF_X72_Y73_N8
\pix_data_g[7]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pix_data_g(7),
	o => \pix_data_g[7]~input_o\);

-- Location: LCCOMB_X66_Y72_N28
\vga_ctrl_inst|vga_dac_g[7]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_dac_g[7]~7_combout\ = (\pix_data_g[7]~input_o\ & (!\vga_ctrl_inst|snyc~0_combout\ & \vga_ctrl_inst|pix_ack~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \pix_data_g[7]~input_o\,
	datac => \vga_ctrl_inst|snyc~0_combout\,
	datad => \vga_ctrl_inst|pix_ack~0_combout\,
	combout => \vga_ctrl_inst|vga_dac_g[7]~7_combout\);

-- Location: IOIBUF_X74_Y73_N22
\pix_data_b[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pix_data_b(0),
	o => \pix_data_b[0]~input_o\);

-- Location: LCCOMB_X66_Y72_N22
\vga_ctrl_inst|vga_dac_b[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_dac_b[0]~0_combout\ = (\pix_data_b[0]~input_o\ & (!\vga_ctrl_inst|snyc~0_combout\ & \vga_ctrl_inst|pix_ack~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \pix_data_b[0]~input_o\,
	datac => \vga_ctrl_inst|snyc~0_combout\,
	datad => \vga_ctrl_inst|pix_ack~0_combout\,
	combout => \vga_ctrl_inst|vga_dac_b[0]~0_combout\);

-- Location: IOIBUF_X72_Y73_N22
\pix_data_b[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pix_data_b(1),
	o => \pix_data_b[1]~input_o\);

-- Location: LCCOMB_X66_Y72_N0
\vga_ctrl_inst|vga_dac_b[1]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_dac_b[1]~1_combout\ = (\pix_data_b[1]~input_o\ & (!\vga_ctrl_inst|snyc~0_combout\ & \vga_ctrl_inst|pix_ack~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pix_data_b[1]~input_o\,
	datac => \vga_ctrl_inst|snyc~0_combout\,
	datad => \vga_ctrl_inst|pix_ack~0_combout\,
	combout => \vga_ctrl_inst|vga_dac_b[1]~1_combout\);

-- Location: IOIBUF_X79_Y0_N8
\pix_data_b[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pix_data_b(2),
	o => \pix_data_b[2]~input_o\);

-- Location: LCCOMB_X74_Y1_N10
\vga_ctrl_inst|vga_dac_b[2]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_dac_b[2]~2_combout\ = (\pix_data_b[2]~input_o\ & (!\vga_ctrl_inst|snyc~0_combout\ & \vga_ctrl_inst|pix_ack~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \pix_data_b[2]~input_o\,
	datac => \vga_ctrl_inst|snyc~0_combout\,
	datad => \vga_ctrl_inst|pix_ack~0_combout\,
	combout => \vga_ctrl_inst|vga_dac_b[2]~2_combout\);

-- Location: IOIBUF_X69_Y73_N22
\pix_data_b[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pix_data_b(3),
	o => \pix_data_b[3]~input_o\);

-- Location: LCCOMB_X66_Y72_N2
\vga_ctrl_inst|vga_dac_b[3]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_dac_b[3]~3_combout\ = (\pix_data_b[3]~input_o\ & (!\vga_ctrl_inst|snyc~0_combout\ & \vga_ctrl_inst|pix_ack~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pix_data_b[3]~input_o\,
	datac => \vga_ctrl_inst|snyc~0_combout\,
	datad => \vga_ctrl_inst|pix_ack~0_combout\,
	combout => \vga_ctrl_inst|vga_dac_b[3]~3_combout\);

-- Location: IOIBUF_X69_Y73_N1
\pix_data_b[4]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pix_data_b(4),
	o => \pix_data_b[4]~input_o\);

-- Location: LCCOMB_X66_Y72_N20
\vga_ctrl_inst|vga_dac_b[4]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_dac_b[4]~4_combout\ = (\pix_data_b[4]~input_o\ & (!\vga_ctrl_inst|snyc~0_combout\ & \vga_ctrl_inst|pix_ack~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pix_data_b[4]~input_o\,
	datac => \vga_ctrl_inst|snyc~0_combout\,
	datad => \vga_ctrl_inst|pix_ack~0_combout\,
	combout => \vga_ctrl_inst|vga_dac_b[4]~4_combout\);

-- Location: IOIBUF_X65_Y0_N22
\pix_data_b[5]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pix_data_b(5),
	o => \pix_data_b[5]~input_o\);

-- Location: LCCOMB_X74_Y1_N4
\vga_ctrl_inst|vga_dac_b[5]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_dac_b[5]~5_combout\ = (\pix_data_b[5]~input_o\ & (!\vga_ctrl_inst|snyc~0_combout\ & \vga_ctrl_inst|pix_ack~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \pix_data_b[5]~input_o\,
	datac => \vga_ctrl_inst|snyc~0_combout\,
	datad => \vga_ctrl_inst|pix_ack~0_combout\,
	combout => \vga_ctrl_inst|vga_dac_b[5]~5_combout\);

-- Location: IOIBUF_X74_Y0_N15
\pix_data_b[6]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pix_data_b(6),
	o => \pix_data_b[6]~input_o\);

-- Location: LCCOMB_X74_Y1_N30
\vga_ctrl_inst|vga_dac_b[6]~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_dac_b[6]~6_combout\ = (!\vga_ctrl_inst|snyc~0_combout\ & (\pix_data_b[6]~input_o\ & \vga_ctrl_inst|pix_ack~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vga_ctrl_inst|snyc~0_combout\,
	datac => \pix_data_b[6]~input_o\,
	datad => \vga_ctrl_inst|pix_ack~0_combout\,
	combout => \vga_ctrl_inst|vga_dac_b[6]~6_combout\);

-- Location: IOIBUF_X79_Y0_N1
\pix_data_b[7]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pix_data_b(7),
	o => \pix_data_b[7]~input_o\);

-- Location: LCCOMB_X74_Y1_N24
\vga_ctrl_inst|vga_dac_b[7]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \vga_ctrl_inst|vga_dac_b[7]~7_combout\ = (\pix_data_b[7]~input_o\ & (!\vga_ctrl_inst|snyc~0_combout\ & \vga_ctrl_inst|pix_ack~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \pix_data_b[7]~input_o\,
	datac => \vga_ctrl_inst|snyc~0_combout\,
	datad => \vga_ctrl_inst|pix_ack~0_combout\,
	combout => \vga_ctrl_inst|vga_dac_b[7]~7_combout\);

ww_frame_start <= \frame_start~output_o\;

ww_pix_ack <= \pix_ack~output_o\;

ww_vga_hsync <= \vga_hsync~output_o\;

ww_vga_vsync <= \vga_vsync~output_o\;

ww_vga_dac_clk <= \vga_dac_clk~output_o\;

ww_vga_dac_blank_n <= \vga_dac_blank_n~output_o\;

ww_vga_dac_sync_n <= \vga_dac_sync_n~output_o\;

ww_vga_dac_r(0) <= \vga_dac_r[0]~output_o\;

ww_vga_dac_r(1) <= \vga_dac_r[1]~output_o\;

ww_vga_dac_r(2) <= \vga_dac_r[2]~output_o\;

ww_vga_dac_r(3) <= \vga_dac_r[3]~output_o\;

ww_vga_dac_r(4) <= \vga_dac_r[4]~output_o\;

ww_vga_dac_r(5) <= \vga_dac_r[5]~output_o\;

ww_vga_dac_r(6) <= \vga_dac_r[6]~output_o\;

ww_vga_dac_r(7) <= \vga_dac_r[7]~output_o\;

ww_vga_dac_g(0) <= \vga_dac_g[0]~output_o\;

ww_vga_dac_g(1) <= \vga_dac_g[1]~output_o\;

ww_vga_dac_g(2) <= \vga_dac_g[2]~output_o\;

ww_vga_dac_g(3) <= \vga_dac_g[3]~output_o\;

ww_vga_dac_g(4) <= \vga_dac_g[4]~output_o\;

ww_vga_dac_g(5) <= \vga_dac_g[5]~output_o\;

ww_vga_dac_g(6) <= \vga_dac_g[6]~output_o\;

ww_vga_dac_g(7) <= \vga_dac_g[7]~output_o\;

ww_vga_dac_b(0) <= \vga_dac_b[0]~output_o\;

ww_vga_dac_b(1) <= \vga_dac_b[1]~output_o\;

ww_vga_dac_b(2) <= \vga_dac_b[2]~output_o\;

ww_vga_dac_b(3) <= \vga_dac_b[3]~output_o\;

ww_vga_dac_b(4) <= \vga_dac_b[4]~output_o\;

ww_vga_dac_b(5) <= \vga_dac_b[5]~output_o\;

ww_vga_dac_b(6) <= \vga_dac_b[6]~output_o\;

ww_vga_dac_b(7) <= \vga_dac_b[7]~output_o\;
END structure;


