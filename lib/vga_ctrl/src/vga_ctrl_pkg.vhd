library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package vga_ctrl_pkg is
	component vga_ctrl is
		generic (
			H_FRONT_PORCH  : integer; -- in clk cycles
			H_BACK_PORCH   : integer; -- in clk cycles
			H_SYNC_PULSE   : integer; -- in clk cycles
			H_VISIBLE_AREA : integer; -- the horizontal resolution
			V_FRONT_PORCH  : integer; -- in lines
			V_BACK_PORCH   : integer; -- in lines
			V_SYNC_PULSE   : integer; -- in lines
			V_VISIBLE_AREA : integer  -- the vertical resolution
		);
		port (
			clk     : in  std_logic;
			res_n   : in  std_logic;
	
			frame_start : out std_logic;
			pix_data_r  : in std_logic_vector(7 downto 0); -- red pixel data
			pix_data_g  : in std_logic_vector(7 downto 0); -- green pixel data
			pix_data_b  : in std_logic_vector(7 downto 0); -- blue pixel data
			pix_ack     : out std_logic; -- read next pixel value from FIFO 
			
			-- connection to VGA connector/DAC
			vga_hsync       : out std_logic;
			vga_vsync       : out std_logic;
			vga_dac_clk     : out std_logic;
			vga_dac_blank_n : out std_logic;
			vga_dac_sync_n  : out std_logic;
			vga_dac_r       : out std_logic_vector(7 downto 0);
			vga_dac_g       : out std_logic_vector(7 downto 0);
			vga_dac_b       : out std_logic_vector(7 downto 0)
		);
	end component;

	component precompiled_vga_ctrl_640x480 is
		port (
			clk     : in  std_logic;
			res_n   : in  std_logic;
	
			frame_start : out std_logic;
			pix_data_r  : in std_logic_vector(7 downto 0); -- red pixel data
			pix_data_g  : in std_logic_vector(7 downto 0); -- green pixel data
			pix_data_b  : in std_logic_vector(7 downto 0); -- blue pixel data
			pix_ack     : out std_logic; -- read next pixel value from FIFO 
			
			-- connection to VGA connector/DAC
			vga_hsync       : out std_logic;
			vga_vsync       : out std_logic;
			vga_dac_clk     : out std_logic;
			vga_dac_blank_n : out std_logic;
			vga_dac_sync_n  : out std_logic;
			vga_dac_r       : out std_logic_vector(7 downto 0);
			vga_dac_g       : out std_logic_vector(7 downto 0);
			vga_dac_b       : out std_logic_vector(7 downto 0)
		);
	end component;
	
	component tpg is
		generic (
			WIDTH : integer := 640;
			HEIGHT : integer := 480
		);
		port (
			clk : in std_logic;
			res_n : in std_logic;
			frame_start : in std_logic;
			pix_ack : in std_logic;
			pix_data_r : out std_logic_vector(7 downto 0);
			pix_data_g : out std_logic_vector(7 downto 0);
			pix_data_b : out std_logic_vector(7 downto 0)
		);
	end component;
end package;

