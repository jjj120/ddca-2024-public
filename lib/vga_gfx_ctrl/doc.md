
# VGA Graphics Controller
The `vga_gfx_ctrl` performs graphical operations on bitmaps using a command-based interface.
These operations include simple geometric operations (e.g., setting individual pixels, drawing lines, etc.) as well as the ability to copy and transform (i.e., rotating) a section of one bitmap onto another one, an operation also referred to as [bit blitting](https://en.wikipedia.org/wiki/Bit_blit).
Bitmaps are stored in external SRAM, referred to as Video RAM (VRAM) throughout this document.
The `vga_gfx_ctrl` interfaces with the ADV7123 video DAC to produce an RGB analog component video signal that can be output through a VGA connector.
It supports one fixed output resolution of 320x240 pixels and a color depth of 8 bit (RGB332).

## Internal Structure
The below figure shows an overview of the internal structure of the core:
![VGA Graphics Controller Overview](.mdata/core_overview.svg)

The Rasterizer provides the internal interface to the core, i.e., the signals other IP cores have to connect to, in order to use the `vga_gfx_ctrl`.
It executes the actual graphics commands by reading and writing pixel data from and to bitmaps in VRAM.
VRAM is byte-addressed, which means that every address corresponds to the 8-bit color value of a single pixel in the RGB332 format.
Bitmaps in VRAM are identified by Bitmap Descriptors (BDs), which are stored internally in the Rasterizer.
A BD contains 3 values:
- base address (unsigned 21 bit)
- width (unsigned 15 bit)
- height (unsigned 15 bit)

BDs are used by the Rasterizer to calculate the effective addresses of pixels in VRAM.
Given the pixel coordinates `x` and `y` for some pixel, its effective address in VRAM is given by:

$`[\text{effective address}] = \text{[base address]} + y *\text{[width]} + x`$

The coordinate $`(x,y)=(0,0)`$ points to the pixel of the upper left corner of the bitmap, while $`(x,y)=(\text{[width]}-1,\text{[height]}-1)`$ points to the lower right corner.
Note that bitmaps must always start at even addresses in VRAM (i.e., the LSB of the base address must be zero).


To generate the actual video signal the VGA Controller creates the appropriate signals for the video DAC (`vga_dac_*`) and the synchronization signals (`vga_hsync` and `vga_vsync`).
The actual pixel data required by the VGA Controller either comes from the Test Pattern Generator (TPG) or the Frame Reader.
The `frame_start` signal is used to synchronize these modules.
Although the supported resolution of the `vga_gfx_ctrl` is only 320x240 pixels, the VGA Controller outputs a resolution of 640x480.
This is done for the sake of compatibility, since not all monitors support resolutions lower than 640x480.

Hence, the Frame Reader and the TPG have to supply image data with a higher resolution.
Since the TPG only outputs a static image, this is not really an issue.
However, the Frame Reader must perform an upscaling operation on the data read from VRAM.
The Frame Reader uses the `base_addr_req` signal to synchronize the switching of the frame buffer with the Rasterizer, which is necessary in order to support double buffering.

## Dependencies
* Since the VGA Graphics Controller is provided as a precompiled module, there are no external dependencies. 


## Required Files
 * `vga_gfx_ctrl_pkg.vhd`
 * `vga_gfx_ctrl.vhd`
 * `gfx_cmd_pkg.vhd`
 * `rasterizer_a21d16_top.qxp` (use **only for synthesis** (i.e., in quartus), not for simulation!)
 * `frame_reader.vhd`
 * `rasterizer_a21d16_top.vho` (use **only for simulation**, not for synthesis!)
 * `precompiled_rasterizer_a21d16.vhd`

## VGA Graphics Controller Component Declaration

```vhdl
component vga_gfx_ctrl is
	port (
		clk : in std_logic;
		res_n : in std_logic;
		display_clk : in std_logic;
		display_res_n : in std_logic;
		gfx_cmd : in std_logic_vector(15 downto 0);
		gfx_cmd_wr : in std_logic;
		gfx_cmd_full : out std_logic;
		gfx_rd_data : out std_logic_vector(15 downto 0);
		gfx_rd_valid : out std_logic;
		gfx_frame_sync : out std_logic;
		sram_dq : inout std_logic_vector(15 downto 0);
		sram_addr : out std_logic_vector(19 downto 0);
		sram_ub_n : out std_logic;
		sram_lb_n : out std_logic;
		sram_we_n : out std_logic;
		sram_ce_n : out std_logic;
		sram_oe_n : out std_logic;
		vga_hsync : out std_logic;
		vga_vsync : out std_logic;
		vga_dac_clk : out std_logic;
		vga_dac_blank_n : out std_logic;
		vga_dac_sync_n : out std_logic;
		vga_dac_r : out std_logic_vector(7 downto 0);
		vga_dac_g : out std_logic_vector(7 downto 0);
		vga_dac_b : out std_logic_vector(7 downto 0)
	);
end component;
```

## VGA Graphics Controller Interface Description
Graphics commands are fed into the core using the signals `gfx_cmd_wr` and `gfx_cmd`.
Internally those signals directly feed a FIFO, referred to as the Command FIFO, whose full flag is in turn output at `gfx_cmd_full`.
Hence, this port (i.e., the signals `gfx_cmd_wr`, `gfx_cmd` and `gfx_cmd_full`) behaves exactly like the write port of a FIFO.

The `gfx_frame_sync` signal is only activated for a single clock cycle in response to the execution of a `DISPLAY_BMP` command and allows the synchronization of an interfacing IP core to the start of a new frame output by the VGA Controller.

The signals `gfx_rd_data` and `gfx_rd_valid` are used by commands that read data from VRAM.
Whenever the core writes new data to `gfx_rd_data`, `gfx_rd_valid` is asserted for exactly one clock cycle.
The data on `gfx_rd_data` remains valid until `gfx_rd_valid` goes high again.

## Supported Commands

### NOP
**Format**:

![NOP Command](.mdata/nop.svg)

**Description**: Do nothing.


### MOVE_GP
**Format**:

![MOVE_GP Command](.mdata/move_gp.svg)

**Description**: 
Sets the `gp` to (`x`,`y`).
If the `relative` bit is set, `x` and `y` will instead be added to the current `gp` (i.e., `gp.x` += `x`, `gp.y` += `y`).
The operands `x` and `y` are signed 16-bit values.



### INC_GP
**Format**:

![INC_GP Command](.mdata/inc_gp.svg)

**Description**: 
Adds the signed 10 bit integer in `incvalue` to either `gp.y` (`dir`=1) or `gp.x` (`dir`=0).
For that purpose `incvalue` is sign-extended.



### CLEAR
**Format**:

![CLEAR Command](.mdata/clear.svg)

**Description**: 
Sets every pixel in active bitmap to the color specified by `cs`.
Does not change the `gp`.



### SET_PIXEL
**Format**:

![SET_PIXEL Command](.mdata/set_pixel.svg)

**Description**: 
Sets the pixel in the active bitmap the `gp` currently points at to the color specified by `cs`.
If the `gp` is outside of the bounds of the active bitmap no pixel is set.
After that `gp.x` (`gp.y`) is incremented by one if `mx` (`my`) is set.



### DRAW_HLINE
**Format**:

![DRAW_HLINE Command](.mdata/draw_hline.svg)

**Description**: 
Draws a horizontal line between the `gp` and the destination coordinate at (`gp.x` + `dx`, `gp.y`) using the color specified by `cs`.
The operand `dx` is a signed 16-bit value.
After the line has been drawn `gp.x` is set to the destination *x* coordinate of the line if `mx` is set.
If `my` is set `gp.y` is incremented by one.



### DRAW_VLINE
**Format**:

![DRAW_VLINE Command](.mdata/draw_vline.svg)

**Description**: 
Draws a vertical line between the `gp` and the destination coordinate at (`gp.x`, `gp.y` + `dy`) using the color specified by `cs`.
The operand `dy` is a signed 16-bit value.
After the line has been drawn `gp.y` is set to the destination *y* coordinate of the line if `my` is set.
If `mx` is set `gp.x` is incremented by one.



### DRAW_TRIANGLE
**Format**:

![DRAW_TRIANGLE Command](.mdata/draw_triangle.svg)

**Description**: 
Draws a triangle defined by the points (`gp.x`,`gp.y`), (`x1`, `y1`) and (`x2`, `y2`) (points must be given in counter-clockwise order) and fills it with the color specified by `cs`.
The operands `x1`, `y1`, `x2`, `y2` are signed 16 bit values.
After the triangle has been drawn `gp.x` (`gp.y`) is set to `x1` / `x2` (`y1` / `y2`) if `xsel` (`ysel`) is set to `01` / `11`.



### GET_PIXEL
**Format**:

![GET_PIXEL Command](.mdata/get_pixel.svg)

**Description**: 
Reads the color of the pixel in the active bitmap the `gp` currently points to and outputs it using `gfx_rd_data`/`gfx_rd_valid`.
Since pixels are 8 bits wide only the lower 8 bits (i.e., 7 downto 0) of `gfx_rd_data` are used.
The upper bits are set to zero.
If the `gp` is outside of the bound of the active bitmap all bits in `gfx_rd_data` are set.
After that `gp.x` (`gp.y`) is incremented by one if `mx` (`my`) is set.



### VRAM_READ
**Format**:

![VRAM_READ Command](.mdata/vram_read.svg)

**Description**: 
Performs a read operation on the VRAM address `addrhi` & `addrlo` and outputs the result using `gfx_rd_data`/`gfx_rd_valid`.
If `m` is 0, a byte access is performed.
In this case the upper byte (15 downto 8) in `gfx_rd_data` is set to zero and the read byte is placed in the lower byte (7 downto 0).
If `m` is 1, a word access is performed.
For a word access the LSB of the address must be zero.



### VRAM_WRITE
**Format**:

![VRAM_WRITE Command](.mdata/vram_write.svg)

**Description**: 
Writes a single byte (`m`=0) or word (`m`=1) to VRAM.
If a byte access is performed only the lower 8 bits of the `data` operand are used, the upper 8 bits are ignored.
For a word access the LSB of the address must be zero.



### VRAM_WRITE_SEQ
**Format**:

![VRAM_WRITE_SEQ Command](.mdata/vram_write_seq.svg)

**Description**: 
Writes a sequence of bytes (`m`=0) or words (`m`=1) to VRAM starting at the address `addrhi` & `addrlo`.
In byte mode (`m`=0) only the lower 8 bits of each of the operands `data[0]` to `data[n-1]` are used.
The last address written is (`addrhi` & `addrlo`) + `n` - 1.
In word mode (`m`=1) the last address written is (`addrhi` & `addrlo`) + 2*(`n` - 1).



### VRAM_WRITE_INIT
**Format**:

![VRAM_WRITE_INIT Command](.mdata/vram_write_init.svg)

**Description**: 
Initializes a range of memory addresses starting at `addrhi & addrlo` to `data`.
In byte mode (`m`=0) only the lower 8 bits of the operand `data` are used.
The last address written is (`addrhi & addrlo`) + `n` - 1.
In word mode (`m`=1) the last address written is (`addrhi & addrlo`) + 2*(`n` - 1).



### SET_COLOR
**Format**:

![SET_COLOR Command](.mdata/set_color.svg)

**Description**: 
Sets the primary (secondary) color to `color` if `cs` is 0 (1).
For the actual color value in `color` the RGB332 format is used.



### SET_BB_EFFECT
**Format**:

![SET_BB_EFFECT Command](.mdata/set_bb_effect.svg)

**Description**: 
Sets the current BB Effect (i.e., the registers `maskop` and `mask`) used by all subsequent `BB_*` commands.
See the entry for `BB_CLIP` for the purpose of these registers.



### DEFINE_BMP
**Format**:

![DEFINE_BMP Command](.mdata/define_bmp.svg)

**Description**: 
Writes a Bitmap Descriptor to `bmpidx`.
Bitmaps always start at even addresses.
This means that the LSB of the low address is assumend to be zero.



### ACTIVATE_BMP
**Format**:

![ACTIVATE_BMP Command](.mdata/activate_bmp.svg)

**Description**: 
	Sets the Active Bitmap Descriptor by copying `bdt[bmpidx]` to `abd`.
	All subsequent drawing commands will use this bitmap as their target.
	Changing `bdt[bmpidx]` with a subsequent `DEFINE_BMP` command does not affect the `abd`.
	


### DISPLAY_BMP
**Format**:

![DISPLAY_BMP Command](.mdata/display_bmp.svg)

**Description**: 
Sets the Current Output Address for the Frame Reader to `bdt[bmpidx].base`.
If the frame synchronization flag `fs` is 1, the command blocks the execution of the following graphics commands until the Frame Reader starts to fetch a new frame.
This feature makes it possible to implement double buffering.
If `fs`=1 the `gfx_frame_sync` signal is asserted for exactly one clock cycle to indicate that the command has been executed.
The dimension of the bitmap referenced by `bmpidx` must be 320x240 pixels, `bdt[bmpidx].width` and `bdt[bmpidx].height` are ignored.



### BB_FULL
**Format**:

![BB_FULL Command](.mdata/bb_full.svg)

**Description**: 
This command is equivalent to calling `BB_CLIP` with the operands (0, 0, `bdt[bmpidx].width`, `bdt[bmpidx].height`).



### BB_CHAR
**Format**:

![BB_CHAR Command](.mdata/bb_char.svg)

**Description**: 
This command is equivalent to calling `BB_CLIP` with the operands (`xoffset`, 0, `charwidth`, `bdt[bmpidx].height`).



### BB_CLIP
**Format**:

![BB_CLIP Command](.mdata/bb_clip.svg)

**Description**: 
Performs a bit blit operation by copying (and transforming) the bitmap section defined by `x`, `y`, `width` and `height` of the source bitmap identified by `bmpidx` to the Active Bitmap to the position of the `gp`.
If the rectangle defined by unsigned 15-bit operands `x`, `y`, `width` and `height` lies (even only partially) outside of the bound of the source bitmap the behavior of the command is undefined.
If the bounds of the drawn image section are outside of the bounds of the destination bitmaps (i.e., the Active Bitmap) clipping is performed.
The `rot` field is used to control the rotation of the copied image section and can take the following values:

- 00: no rotation
- 01: 90° clockwise rotation
- 10: 180° clockwise rotation
- 11: 270° clockwise rotation

After the execution of the command `gp.x` (`gp.y`) is incremented by *dx* (*dy*) if `mx` (`my`) is 1:

```math
	dx = 
	\begin{cases}
		\text{\texttt{width}} & \text{if \texttt{rot}=00 or \texttt{rot}=10}\\
		\text{\texttt{height}} & \text{otherwise}
	\end{cases},\;\;\;\;\;
	dy = 
	\begin{cases}
		\text{\texttt{height}} & \text{if \texttt{rot}=00 or \texttt{rot}=10}\\
		\text{\texttt{width}} & \text{otherwise}
	\end{cases}
```

If the alpha mode (`am`) flag is 1, pixels in the source image that match the secondary color are not copied to the active bitmap.
The behavior of this command is changed by the registers `mask` and `maskop`.
Depending on the value of `maskop` the pixels read from the source bitmap are transformed by performing a bitwise Boolean operation with the `mask` register before writing them to the Active Bitmap.
Let *c* be the pixel color read for some pixel of the source bitmap, then *c'* will be written to the Active Bitmap:

```math
	c' =
	\begin{cases}
		c & \text{if \texttt{maskop}\ =00}\\
		c \text{ and \texttt{mask}\ } & \text{if \texttt{maskop}\ =01}\\
		c \text{ or  \texttt{mask}\ } & \text{if \texttt{maskop}\ =10}\\
		c \text{ xor \texttt{mask}\ } & \text{if \texttt{maskop}\ =11}\\
	\end{cases}
```

When the alpha mode is active (i.e., if `am` is 1) the same transformation is also applied to the secondary color.
This is done such that the "transparency" of pixels is preserved.
However, the actual register where the secondary color is stored is not changed.



## Useful Constants and Types


The [gfx_cmd_pkg](./src/gfx_cmd_pkg.vhd) includes some useful constants and types to make code interfacing with the `vga_gfx_ctrl` more read- and maintainable.

Among others, the package contains constants for all supported commands' opcodes (`OPCODE_*`).
Furthermore, as the `vga_gfx_ctrl` instructions are rather heterogenuous in terms of their fields, the `gfx_cmd_pkg` defines some constants that hold the values of the various fields and their width (`INDEX_*`, `WIDTH_*`).

Furthermore, you will find subtypes and constants for the fields (e.g. the `rot_t` type for rotations and the `ROT_R*` constants for its possible values)


## Utility Functions


In addition to constants and types, the [gfx_cmd_pkg](./src/gfx_cmd_pkg.vhd) also containts some useful utility functions for working with graphics commands.

For the fields of such commands, there exist `get_*` functions that extract and return the respective field out of an instruction.

The `create_gfx_instr` allows it to create a desired graphics instruction by passing it the **required** fields as parameters (parameters of unused fields are not required to be set).

The `get_operands_count` function returns the amount of operands required by the specified instruction as integer.

