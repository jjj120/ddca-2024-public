
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.sram_ctrl_pkg.all;
use work.gfx_cmd_pkg.all;


entity precompiled_rasterizer_a21d16 is
	port (
		clk     : in  std_logic;
		res_n   : in  std_logic;

		-- write interface to VRAM
		vram_wr_addr        : out std_logic_vector(20 downto 0);
		vram_wr_data        : out std_logic_vector(15 downto 0);
		vram_wr_busy        : in  std_logic;
		vram_wr_emtpy       : in std_logic;
		vram_wr             : out std_logic;
		vram_wr_access_mode : out sram_access_mode_t;
		
		-- read interface to VRAM
		vram_rd_addr        : out std_logic_vector(20 downto 0);
		vram_rd_data        : in  std_logic_vector(15 downto 0);
		vram_rd_busy        : in  std_logic;
		vram_rd             : out std_logic;
		vram_rd_valid       : in std_logic;
		vram_rd_access_mode : out sram_access_mode_t;
		
		--auxiliary signals
		fr_base_addr        : out std_logic_vector(20 downto 0);
		fr_base_addr_req    : in std_logic;
		tpg_active          : out std_logic;
		
		--command interface
		gfx_cmd        : in std_logic_vector(GFX_CMD_WIDTH-1 downto 0);
		gfx_cmd_wr     : in std_logic;
		gfx_cmd_full   : out std_logic;
		gfx_rd_data    : out std_logic_vector(15 downto 0);
		gfx_rd_valid   : out std_logic;
		gfx_frame_sync : out std_logic
	);
end entity;

architecture arch of precompiled_rasterizer_a21d16 is
	component rasterizer_a21d16_top is
		port (
			clk     : in  std_logic;
			res_n   : in  std_logic;
	
			-- write interface to VRAM
			vram_wr_addr        : out std_logic_vector(21-1 downto 0);
			vram_wr_data        : out std_logic_vector(16-1 downto 0);
			vram_wr_busy        : in  std_logic;
			vram_wr_emtpy       : in std_logic;
			vram_wr             : out std_logic;
			vram_wr_access_mode : out std_logic;
			
			-- read interface to VRAM
			vram_rd_addr        : out std_logic_vector(21-1 downto 0);
			vram_rd_data        : in  std_logic_vector(16-1 downto 0);
			vram_rd_busy        : in  std_logic;
			vram_rd             : out std_logic;
			vram_rd_valid       : in std_logic;
			vram_rd_access_mode : out std_logic;
			
			--auxiliary signals
			fr_base_addr      : out std_logic_vector(21-1 downto 0);
			fr_base_addr_req  : in std_logic;
			tpg_active        : out std_logic;
			
			--command interface
			gfx_cmd        : in std_logic_vector(GFX_CMD_WIDTH-1 downto 0);
			gfx_cmd_wr     : in std_logic;
			gfx_cmd_full   : out std_logic;
			gfx_rd_data    : out std_logic_vector(15 downto 0);
			gfx_rd_valid   : out std_logic;
			gfx_frame_sync : out std_logic
		);
	end component;

	signal vram_wr_access_mode_unc, vram_rd_access_mode_unc : std_logic;
begin

	rasterizer_a21d16_inst : rasterizer_a21d16_top
	port map (
		clk                 => clk,
		res_n               => res_n,
		vram_wr_addr        => vram_wr_addr,
		vram_wr_data        => vram_wr_data,
		vram_wr_busy        => vram_wr_busy,
		vram_wr_emtpy       => vram_wr_emtpy,
		vram_wr             => vram_wr,
		vram_wr_access_mode => vram_wr_access_mode_unc,
		vram_rd_addr        => vram_rd_addr,
		vram_rd_data        => vram_rd_data,
		vram_rd_busy        => vram_rd_busy,
		vram_rd_valid       => vram_rd_valid,
		vram_rd             => vram_rd,
		vram_rd_access_mode => vram_rd_access_mode_unc,
		fr_base_addr        => fr_base_addr,
		fr_base_addr_req    => fr_base_addr_req,
		tpg_active          => tpg_active,
		gfx_cmd             => gfx_cmd,
		gfx_cmd_wr          => gfx_cmd_wr,
		gfx_cmd_full        => gfx_cmd_full,
		gfx_rd_data         => gfx_rd_data,
		gfx_rd_valid        => gfx_rd_valid,
		gfx_frame_sync      => gfx_frame_sync
	);
	vram_rd_access_mode <= to_sram_access_mode_t(vram_rd_access_mode_unc);
	vram_wr_access_mode <= to_sram_access_mode_t(vram_wr_access_mode_unc);
end architecture;




