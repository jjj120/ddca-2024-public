library ieee;
use ieee.std_logic_1164.all;

use work.sram_ctrl_pkg.all;
use work.vga_ctrl_pkg.all;
use work.mem_pkg.all;

entity vga_gfx_ctrl is
	port (
		clk   : in std_logic;
		res_n : in std_logic;
		display_clk   : in std_logic;
		display_res_n : in std_logic;
	
		-- command interface
		gfx_cmd       : in std_logic_vector(15 downto 0);
		gfx_cmd_wr    : in std_logic;
		gfx_cmd_full  : out std_logic;
		gfx_rd_data     : out std_logic_vector(15 downto 0);
		gfx_rd_valid    : out std_logic;
		gfx_frame_sync  : out std_logic;
		
		-- external interface to SRAM
		sram_dq : inout std_logic_vector(15 downto 0);
		sram_addr : out std_logic_vector(19 downto 0);
		sram_ub_n : out std_logic;
		sram_lb_n : out std_logic;
		sram_we_n : out std_logic;
		sram_ce_n : out std_logic;
		sram_oe_n : out std_logic;
		
		-- connection to VGA connector/DAC
		vga_hsync       : out std_logic;
		vga_vsync       : out std_logic;
		vga_dac_clk     : out std_logic;
		vga_dac_blank_n : out std_logic;
		vga_dac_sync_n  : out std_logic;
		vga_dac_r       : out std_logic_vector(7 downto 0);
		vga_dac_g       : out std_logic_vector(7 downto 0);
		vga_dac_b       : out std_logic_vector(7 downto 0)
	);
end entity;

architecture arch of vga_gfx_ctrl is
	constant SRAM_ADDR_WIDTH : integer := 21;
	constant SRAM_DATA_WIDTH : integer := 16;

	signal vram_wr_addr : std_logic_vector(SRAM_ADDR_WIDTH-1 downto 0);
	signal vram_wr_data : std_logic_vector(SRAM_DATA_WIDTH-1 downto 0);
	signal vram_wr      : std_logic;
	signal vram_wr_access_mode : sram_access_mode_t;
	signal vram_wr_busy : std_logic;
	
	signal vram_rd_addr  : std_logic_vector(SRAM_ADDR_WIDTH-1 downto 0);
	signal vram_rd_data  : std_logic_vector(SRAM_DATA_WIDTH-1 downto 0);
	signal vram_rd       : std_logic;
	signal vram_rd_valid : std_logic;
	signal vram_rd_access_mode : sram_access_mode_t;
	signal vram_rd_busy : std_logic;
	signal vram_wr_emtpy : std_logic;
	
	signal fr_base_addr : std_logic_vector(SRAM_ADDR_WIDTH-1 downto 0);
	signal fr_base_addr_req : std_logic;
	
	signal fr_vram_rd_addr   : std_logic_vector(SRAM_ADDR_WIDTH-1 downto 0);
	signal fr_vram_rd        : std_logic;
	signal fr_vram_rd_access_mode : sram_access_mode_t;
	signal fr_vram_rd_busy  : std_logic;
	signal fr_vram_rd_data  : std_logic_vector(SRAM_DATA_WIDTH-1 downto 0);
	signal fr_vram_rd_valid : std_logic;
	
	signal pix_ack : std_logic;
	signal pix_data_r : std_logic_vector(7 downto 0);
	signal pix_data_g : std_logic_vector(7 downto 0);
	signal pix_data_b : std_logic_vector(7 downto 0);
	signal fr_pix_data_r : std_logic_vector(7 downto 0);
	signal fr_pix_data_g : std_logic_vector(7 downto 0);
	signal fr_pix_data_b : std_logic_vector(7 downto 0);
	signal tpg_pix_data_r : std_logic_vector(7 downto 0);
	signal tpg_pix_data_g : std_logic_vector(7 downto 0);
	signal tpg_pix_data_b : std_logic_vector(7 downto 0);
	
	signal instr_fifo_rd : std_logic;
	signal instr_fifo_empty : std_logic;
	signal instr_fifo_data : std_logic_vector(15 downto 0);
	
	signal tpg_active : std_logic;
	signal frame_start : std_logic;
begin
	
	rasterizer_inst : entity work.precompiled_rasterizer_a21d16
	--generic map (
	--	VRAM_ADDR_WIDTH  => SRAM_ADDR_WIDTH
	--	VRAM_DATA_WIDTH => SRAM_DATA_WIDTH
	--)
	port map (
		clk                    => clk,
		res_n                  => res_n,
		vram_wr_addr           => vram_wr_addr,
		vram_wr_data           => vram_wr_data,
		vram_wr_busy           => vram_wr_busy,
		vram_wr_emtpy          => vram_wr_emtpy,
		vram_wr                => vram_wr,
		vram_wr_access_mode    => vram_wr_access_mode,
		vram_rd_addr           => vram_rd_addr,
		vram_rd_data           => vram_rd_data,
		vram_rd_busy           => vram_rd_busy,
		vram_rd_valid          => vram_rd_valid,
		vram_rd                => vram_rd,
		vram_rd_access_mode    => vram_rd_access_mode,
		fr_base_addr           => fr_base_addr,
		fr_base_addr_req       => fr_base_addr_req,
		tpg_active             => tpg_active,
		gfx_cmd                => gfx_cmd,
		gfx_cmd_wr             => gfx_cmd_wr,
		gfx_cmd_full           => gfx_cmd_full,
		gfx_rd_data            => gfx_rd_data,
		gfx_rd_valid           => gfx_rd_valid,
		gfx_frame_sync         => gfx_frame_sync
	);

	sram_ctrl_inst : entity work.precompiled_sram_ctrl_a21d16b8
	--generic map (
	--	ADDR_WIDTH => SRAM_ADDR_WIDTH,
	--	DATA_WIDTH => SRAM_DATA_WIDTH
	--)
	port map (
		clk       => clk,
		res_n     => res_n,
		-- write port (rasterizer)
		wr_addr   => vram_wr_addr,
		wr_data   => vram_wr_data,
		wr        => vram_wr,
		wr_access_mode => vram_wr_access_mode,
		wr_full   => vram_wr_busy,
		wr_empty  => vram_wr_emtpy,
		wr_half_full => open,
		-- read port 1 (frame_reader)
		rd1_addr        => fr_vram_rd_addr,
		rd1             => fr_vram_rd,
		rd1_access_mode => fr_vram_rd_access_mode,
		rd1_busy        => fr_vram_rd_busy,
		rd1_data        => fr_vram_rd_data,
		rd1_valid       => fr_vram_rd_valid,
		-- read port 2 (rasterizer)
		rd2_addr        => vram_rd_addr,
		rd2             => vram_rd,
		rd2_access_mode => vram_rd_access_mode,
		rd2_busy        => vram_rd_busy,
		rd2_data        => vram_rd_data,
		rd2_valid       => vram_rd_valid,
		-- external signals
		sram_dq   => sram_dq,
		sram_addr => sram_addr,
		sram_ub_n => sram_ub_n,
		sram_lb_n => sram_lb_n,
		sram_we_n => sram_we_n ,
		sram_ce_n => sram_ce_n,
		sram_oe_n => sram_oe_n
	);

	frame_reader_inst : entity work.frame_reader
	generic map (
		VRAM_ADDR_WIDTH => SRAM_ADDR_WIDTH,
		VRAM_DATA_WIDTH => SRAM_DATA_WIDTH
	)
	port map (
		clk            => clk,
		res_n          => res_n,
		-- display clock
		display_clk    => display_clk,
		display_res_n  => display_res_n,
		-- interface to Rasterizer
		base_addr_req  => fr_base_addr_req,
		base_addr      => fr_base_addr,
		-- interface to SRAM
		vram_rd_addr   => fr_vram_rd_addr,
		vram_rd        => fr_vram_rd,
		vram_rd_access_mode => fr_vram_rd_access_mode,
		vram_rd_busy   => fr_vram_rd_busy,
		vram_rd_data   => fr_vram_rd_data,
		vram_rd_valid  => fr_vram_rd_valid,
		-- interface to VGA Controller
		frame_start    => frame_start,
		pix_ack        => pix_ack,
		pix_data_r     => fr_pix_data_r,
		pix_data_g     => fr_pix_data_g,
		pix_data_b     => fr_pix_data_b
	);

	tpg_inst : tpg
	port map (
		clk         => display_clk,
		res_n       => display_res_n,
		frame_start => frame_start,
		pix_ack     => pix_ack,
		pix_data_r  => tpg_pix_data_r,
		pix_data_g  => tpg_pix_data_g,
		pix_data_b  => tpg_pix_data_b
	);

	image_source_mux : process(all)
	begin
		pix_data_r <= fr_pix_data_r;
		pix_data_g <= fr_pix_data_g;
		pix_data_b <= fr_pix_data_b;
		
		if (tpg_active = '1') then
			pix_data_r <= tpg_pix_data_r;
			pix_data_g <= tpg_pix_data_g;
			pix_data_b <= tpg_pix_data_b;
		end if;
	end process;
	
	vga_ctrl_inst : entity work.precompiled_vga_ctrl_640x480
	port map (
		clk             => display_clk,
		res_n           => display_res_n,
		frame_start     => frame_start,
		pix_data_r      => pix_data_r,
		pix_data_g      => pix_data_g,
		pix_data_b      => pix_data_b,
		pix_ack         => pix_ack,
		vga_hsync       => vga_hsync,
		vga_vsync       => vga_vsync,
		vga_dac_clk     => vga_dac_clk,
		vga_dac_blank_n => vga_dac_blank_n,
		vga_dac_sync_n  => vga_dac_sync_n,
		vga_dac_r       => vga_dac_r,
		vga_dac_g       => vga_dac_g,
		vga_dac_b       => vga_dac_b
	);

end architecture;




