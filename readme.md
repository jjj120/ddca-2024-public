<div align="center">

![title](.mdata/title.svg)
<p>
Florian Huemer, Sebastian Wiedemann, Dylan Baumann
</p>
</div>


[[_TOC_]]


Welcome to the DDCA assignment repository.

# Course Overview

This flow chart gives a quick overview of the course.
For a detailed description of the individual steps, please consult the introduction slides in TUWEL.

![DDCA Course Flowchart](.mdata/course_flowchart.svg)


# Collaboration Policy

Please keep in mind that the main purpose of the regular **tasks** (i.e., non-Project-Tasks) in this course is to give you the opportunity to **practice for the exams**.
Hence, presenting a solution to a tutor that you did not come up with yourself and that you might not fully understand has only very limited value to you.
It is in your best interest to try to solve the tasks yourself.
Of course, it is allowed (and even encouraged) to talk to your colleagues, discuss ideas, and to seek assistance from AI while working on solutions. 
However, keep in mind that during the exam, you have no internet access and you will have to rely solely on your own knowledge and understanding.

For **Project Tasks** the work you submit **must be your own original work**!
A thourough exercise interview will test your understanding of the submitted work.
Additionally, we will use a plagiarism-checker to cross-check all submissions.
Cheating, of any kind, will lead to a negative grade on the course (irrespective of points you might have achieved already).
This also applies to the exams.
See the [Plagiarism Policy](https://www.tuwien.at/mwbw/im/ao/lehre/abschlussarbeiten/plagiarismus) of TU Wien for more details.

# Structure of this Repository

The root directory of this repository contains four sub-folders.

* `lib`: Here we store library modules and packages that will be used throughout the course. Don't make any changes here.
* `util`: This folder contains Makefile templates, used to build and simulate your code. Don't make any changes here.
* `top`: This folder is used to store top-level design related files. In particular it contains the project files for the FPGA design software, that you have to create and maintain throughout the course.
* `tasks`: The actual tasks are located here. Your own code (i.e., your solutions) also goes here. 

# First Steps

## Clone the Repository
Be sure to use `git` to actually clone this repository.
**Do not** just download it as an archive, because we will push updates during the semester and you will have a hard time tracking the changes otherwise.

## Retrieve your TILab Account
To retrieve your TILab account go to [password.tilab.tuwien.ac.at](https://password.tilab.tuwien.ac.at) and login with your TISS credentials.
Should you forget your TILab password, you can also reset it using this site.

## Download the VM
If you want to work on your own computer, we recommend downloading our (Virtual Box) VM image from the TILab server.
To do that you can either go to the TILab and copy the image to an USB stick or use the following command.

```bash
scp USERNAME@ssh.tilab.tuwien.ac.at:/opt/eda/vm/ECS-EDA-Tools-VM.tar.xz .
```

`USERNAME` is your TILab username you got in the previous step.

To extract the archive use the following command (under Windows you can use e.g., [7-zip](https://7-zip.org/).):

```bash
tar -xf ECS-EDA-Tools-VM.tar.xz
```

The root/user password for the VM is `ecseda`. 
You can change it using the `passwd` command.

On a Linux host system the VM can also be used with libvirt-based tools, like [GNOME Boxes](https://apps.gnome.org/Boxes/) or [virt-manager](https://virt-manager.org/).
For older versions of GNOME Boxes it might be necessary to first convert the virtual disk image (`ECS-EDA-Tools.vdi`) to a different format:

```bash
qemu-img convert -f vdi -O qcow2 ECS-EDA-Tools.vdi ECS-EDA-Tools.qcow2
```

## Complete Level 0
Level 0 contains the [Design Flow Tutorial](tasks/level0/designflow.md).
Work through it to get to know the tools we are using in this course and setup your Quartus project.


## Work on Tasks 

Now you should have the required knowledge to work on the Level 1 tasks.
A good place to start is the [`basic`](tasks/level1/basic/task.md) task.


# Tasks Overview

## Level 1

You need at least **6 Skill Points** to be allowed to sign-up for the Level 1 Exam!

| Task | Points | Keywords (Topics) |
|-|-|----|
| [adder4](tasks/level1/adder4/task.md) | 1 | structural modeling, combinational |
| [alu](tasks/level1/alu/task.md) | 2 | combinational, arithmetic operations, generics, random testing |
| [basic](tasks/level1/basic/task.md) | 1 | combinational |
| [blockram](tasks/level1/blockram/task.md) | 1 | memory |
| [generic_adder](tasks/level1/generic_adder/task.md) | 2 | structural modeling, generics |
| [gray_code](tasks/level1/gray_code/task.md) | 2 | combinational, generics |
| [lfsr](tasks/level1/lfsr/task.md) | 2 | registers, generics |
| [mergesort](tasks/level1/mergesort/task.md) | 2 | combinational, structural modeling, generics, random testing |
| [pwm](tasks/level1/pwm/task.md) | 2 | registers |
| [running_light](tasks/level1/running_light/task.md) | 3 | FSM |
| [simplecalc](tasks/level1/simplecalc/task.md) | 2 | registers, arithmetic operations |
| [ssdctrl](tasks/level1/ssdctrl/task.md) | 3 | FSM, arithmetic operations |

## Level 2

You need at least **3 Skill Points** to be allowed to sign-up for the Level 2 Exam!

| Task | Points | Keywords (Topics) |
|-|-|----|
| [snes_ctrl](tasks/level2/snes_ctrl/task.md) | 1 | FSM, External Interface |
| [gc_ctrl](tasks/level2/gc_ctrl/task.md) | 1 | FSM, External Interface, Tri-State |
| [gfx_cmd_interpreter](tasks/level2/gfx_cmd_interpreter/task.md) | 2 | Testbench, Protected Type |
| [pt_snake](tasks/level2/pt_snake/task.md) | 3 | Project Task, FSM, Interfaces |
| [vga_ctrl](tasks/level2/vga_ctrl/task.md) | 2 | FSM, External Interface, Oscilloscope |
| [sram_ctrl](tasks/level2/sram_ctrl/task.md) | 3 | External Interface, Tri-State, Datasheet |
| [char_lcd_ctrl](tasks/level2/char_lcd_ctrl/task.md) | 3 | FSM, External Interface, Datasheet |

## Level 3

You need at least **3 Skill Points** to be allowed to sign-up for the Level 3 Exam!

| Task | Points | Keywords (Topics) |
|-|-|----|
| [rv_sim](tasks/level3/rv_sim/task.md) | 2 | Simulation, RISC-V |
| [rv_fsm](tasks/level3/rv_fsm/task.md) | 2 | FSM, RISC-V |
| [rv_pl](tasks/level3/rv_pl/task.md) | 2 | RISC-V, Pipeline |
| [vnorm](tasks/level3/vnorm/task.md) | 1 | File I/O, Pipeline, IP Wizard |
| [sorting_network](tasks/level3/sorting_network/task.md) | 1 | Pipeline |

## Level 4

You need at least **3 Skill Points** to be allowed to sign-up for the Level 4 Exam!

| Task | Points | Keywords (Topics) |
|-|-|----|
| [pt_cache](tasks/level4/pt_cache/task.md) | 3 | Project Task, Cache |
| [rv_pl_hazards](tasks/level4/rv_pl_hazards/task.md) | 1 | RISC-V, Hazard Handling |
| [rv_ext_m](tasks/level4/rv_ext_m/task.md) | 2 | FSM, RISC-V Extension |
| [rv_pl](tasks/level3/rv_pl/task.md) | 2 | RISC-V, Pipeline |

