library ieee;
use ieee.std_logic_1164.all;

entity demo_tb is
end entity demo_tb;

architecture bench of demo_tb is

	-- Component declaration for the unit under test
	component demo
		port (
			a : in  std_logic;
			b : in  std_logic;
			x : out std_logic
		);
	end component;

	-- Test signals
	signal a, b : std_logic := '0';
	signal x : std_logic;

begin

	-- Instantiate the unit under test
	uut : demo
	port map (
		a => a,
		b => b,
		x => x
	);

	-- Stimulus process
	stimulus: process
	begin
		report "simulation start";
		
		-- Apply test stimuli
		a <= '0';
		b <= '0';
		wait for 10 ns;
		assert x = '0' report "Test 1 failed" severity error;

		a <= '0';
		b <= '1';
		wait for 10 ns;
		assert x = '0' report "Test 2 failed" severity error;

		a <= '1';
		b <= '0';
		wait for 10 ns;
		assert x = '0' report "Test 3 failed" severity error;

		a <= '1';
		b <= '1';
		wait for 10 ns;
		assert x = '1' report "Test 4 failed" severity error;


		report "simulation end";
		-- End simulation
		wait;
	end process;
end architecture;
