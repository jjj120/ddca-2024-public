
use work.util_pkg.all;

architecture top_arch_demo of top is
begin
	demo_inst : entity work.demo
	port map (
		a => switches(0),
		b => switches(1),
		x => ledg(0)
	);
	
	ledr <= switches;
	ledg(8 downto 1) <= (others=>'0');
	aux <= switches(15 downto 0);

	hex7 <= to_segs(x"d");
	hex6 <= to_segs(x"d");
	hex5 <= to_segs(x"c");
	hex4 <= to_segs(x"a");
	hex3 <= to_segs(x"2");
	hex2 <= to_segs(x"0");
	hex1 <= to_segs(x"2");
	hex0 <= to_segs(x"4");
	
	tx <= rx;
end architecture;

