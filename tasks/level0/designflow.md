[[_TOC_]]

# Introduction
This document outlines the FPGA design flow used in the DDCA Lab course.

Before you start with any other tasks, go through this document in its entirety.
It will show you how to create some important files, without which you cannot proceed to Level 1 tasks!
We know that this is a lot of information, so you might need to refer back to this document later on.
However, it is beneficial to skim through it at least once, such that you have at rough idea where you can find certain information.

The tool flow is intended to work on the TILab computers and the provided VM.
Please note that we **cannot** support custom setups, even if you install the same tool versions (the VM uses 22.1 for Quartus and 20.1 for ModelSim).
If you run a custom installation, make sure that everything also works in the lab (or the VM)!

All screenshots you see in this document, were created using the VM.
If something is not clear to you, or if you want to know further details, please ask a question in our TU Chat channel or consult a tutor during the supervised lab slots.

We might update this document during the course of the semester.

# Simulation

In this lab course we use two different tools for performing simulations on VHDL designs.
[QuestaSim](https://eda.sw.siemens.com/en-US/ic/questa/simulation/) (formerly called ModelSim, the names will be used interchangeably) is a commercial, industry-standard tool provided by Siemens EDA. 
It essentially provides a complete IDE for designing, debugging and testing digital systems and supports VHDL, Verilog and SystemVerilog.
It comes with common IDE features like project management and a source code editor.
Most importantly, it integrates an interactive user interface to the actual simulator which allows tracing and inspecting individual signal values during simulation and also features a waveform viewer for plotting the results over time.

Due to licensing reasons, the provided VM comes with ModelSim-Intel 20.1, a slightly older and feature-reduced version than the one provided in the lab environment.
However, for our purposes they are fully compatible and we don't expect any problems.
Nevertheless, if you suspect that there might be an issue please don't hesitate to contact us!

The other tool we are using is [GHDL](https://github.com/ghdl/ghdl), an open-source VHDL simulator.
In contrast to QuestaSim it does not come with a GUI, but provides a (single) command line tool to compile VHDL designs and execute them (i.e., run the simulation).
To display the simulation results, an external program is required.
For that purpose we use [GTKWave](https://gtkwave.sourceforge.net).
While GHDL does not support all VHDL features yet, it is nevertheless a great tool and perfectly capable of dealing with large and complex designs.

Note that many Linux distributions provide GHDL as a package.
However, it is also very simple to build it yourself (it does not have many dependencies).


## Basic Build Environment

Since we want to be able to easily switch between simulation tools, we cannot rely on e.g., QuestaSim project files to organize our source files.
Hence, we rather use a custom makefile-controlled build and simulation environment that lets us compile and simulate VHDL designs easily with QuestaSim or GHDL.
This also makes the whole setup very version-control-friendly. 

Each design unit in this course (e.g., a task) will usually be associated with a makefile that configures and controls its compilation and simulation.

Throughout this tutorial, we will use the `demo` module as a running example (`demo/src/demo.vhd`).

```vhdl
library ieee;
use ieee.std_logic_1164.all;

entity demo is
	port(
		a  : in  STD_LOGIC;
		b  : in  STD_LOGIC;
		x  : out STD_LOGIC
	);
end entity;

architecture beh of demo is
begin
	x <= a and b;
end architecture;
```

It also comes with a testbench which is located in `demo/tb/demo_tb.vhd`.
The testbench creates an instance of the `demo` module, applies input stimuli to it and checks if its outputs behave correctly.
The module that is tested in a particular testbench is usually referred to as the Unit-Under-Test (UUT).

This separation of VHDL files into a `src` and a `tb` directory is the usual structure used for designs in this course.
Please also adhere to it when you create new design units.
All VHDL files that contain **synthesizable** code (i.e., code that actually describes hardware) are put into the respective `src` directory.
All **test-related** code, which is not necessarily synthesizable, (i.e., testbenches, test-related packages) are placed in the respective `tb` directory.
 
The associated makefile for our `demo` module is shown below:
```makefile
VHDL_FILES = \
	src/demo.vhd \
	tb/demo_tb.vhd

TB=demo_tb

include ../../../util/simple_flow.mk
```

The first thing that a makefile needs to do, is specifying the list of source files that are required to compile a particular design unit in the `VHDL_FILES` variable.
The file paths are relative to the location of the makefile.
Note that the listing order of files in this variable defines the compile order!
If, for example, some file `e.vhd` file includes a package defined in `p.vhd`, `p.vhd` must be compiled first in order to be able to compile `e.vhd`! Hence, `VHDL_FILES` would need to list `p.vhd` **before** `e.vhd`.
Also note the backslashes, which tell make to ignore the following line break (make uses line-based syntax and treats a new line as the end of a statement).

The `TB` variable is set to the name of the testbench entity.
If you have more than one testbench for a module, you can simply change the assignment to this variable in order to simulate another testbench.

Finally, the file `simple_flow.mk` is included.
The path to this file must be specified relative to the makefile. 
This inclusion is what defines the various makefile targets that will be explained in more detail in the next sections and must **always come after** the assignments to the various makefile variables.
Otherwise the variable assignments don't have the correct effect for the targets defined by `simple_flow.mk`.

Running the `help` target gives you an overview of the supported features and targets (and how you can modify the behavior of some of the targets).
```bash
make help
```
Please note that you should not modify the contents of `simple_flow.mk` or other files residing in the same directory.
This will not only interfere with all your other modules' makefiles, but also makes it harder for you to obtain updates to the make infrastructure from us.

## QuestaSim

To run a simulation using QuestaSim, `simple_flow.mk` defines three targets, named `compile`, `sim` and `sim_gui`.
As the name suggests, the `compile` target simply compiles the source files specified in the `VHDL_FILES` variable, and will report syntax-errors and other compile-time problems.
It is automatically executed before a simulation.

### Text-based simulation

The `sim` target runs the simulation in text-mode, i.e., the QuestaSim GUI is not started and no waveforms are shown.
This is the preferred way to run automated test cases on hardware designs, as it is highly inefficient and, therefore, unfeasible to always check the output waveforms after every little change.

Run
```bash
make sim
```
which should yield the following output (only the relevant parts are shown):
```
[...]
# ** Note: simulation start
#    Time: 0 ns  Iteration: 0  Instance: /demo_tb
# ** Note: simulation end
#    Time: 40 ns  Iteration: 0  Instance: /demo_tb
[...]
```

The testbench (feel free to have a look at it!) contains assertions that check if the output behaves as expected.
Since the design does not contain any errors, none of these assertions fail.
The only output we get (at simulation time 0 ns and 40 ns) is from `report` statements.
To get a better understanding of how the testbench works, change the `and` operator in the architecture of the `demo` entity to an `or` operator and run the simulation again.
You should see that now that two assertions fail.

### Graphical Simulation and Debugging

The graphical user interface of QuestaSim is started using the `sim_gui` target.

```bash
make sim_gui
```

We have prepared a [short video tutorial](https://portal.tuwien.tv/View.aspx?id=10928~5h~zBbCwY8QBC&code=UU~hjqTnRsgLZA4MsfPQcJ9zJIpPzzMRnFummhpXqHcTatFmGmSRMMiK4Bjt3p2atFHyMvvohz0HllBmAeVoC&ax=7y~ffRrFlIvoziUGY) that goes through the workflow and some of the key features.

## GHDL

Similar to the QuestaSim targets, `simple_flow.mk` also provides equivalent targets for GHDL prefixed with the letter `g`, namely `gcompile`, `gsim` and `gsim_gui`.

Run
```
make gsim
```
which should yield the same output as the `sim` target, although formatted in a slightly different way:
```
[...]
tb/demo_tb.vhd:35:17:@0ms:(report note): simulation start
tb/demo_tb.vhd:59:17:@40ns:(report note): simulation end
```

Now run
```
make gsim_gui
```
which executes the testbench and then automatically launches GTKWave.

![GTKWave](.mdata/gtkwave.png)

The user interface of GTKWave is (in some ways) quite similar to that of QuestaSim.
The top-left panel shows the hierarchy of the loaded design, with `demo_tb` at the top and `demo` as a sub-module.
You can click on these entries to get a list of the contained signals in the `Signals` panel.
These signal can then be added to the waveform viewer on the right (drag and drop).

Similar to QuestaSim you can save the waveform viewer configuration to a file via `File -> Write Save File`.
In order to automatically load this configuration the next time you run `gsim_gui`, you have to set the variable `GTK_WAVE_FILE` in the makefile.

The way GHDL interacts with GTKWave is via the `*.dump.ghw` file, which (per default) contains the signal traces of **all** signals in the simulated design.
As you might have guessed, this file can easily get very large, especially for large designs and/or simulation runs.
Hence, it is possible to restrict the set of signals recorded by GHDL using a [wave option file](https://ghdl.github.io/ghdl/using/Simulation.html#cmdoption-ghdl-read-wave-opt).
You can create such a file and set the makefile variable `GHDL_WAVE_OPT` to use it.


## Further Topics

### Cleaning up

Often times simulations create output files, like text files or even images.
The default `clean` target does not know about these files, but can be made aware using the makefile variables `CLEAN_FILES` and `CLEAN_DIRS`.
The following example extends the `clean` target to also remove all `png` and `jpg` files as well as the directory `logs`.

```makefile
[...]

CLEAN_FILES=*.png *.jpg
CLEAN_DIRS=logs

include path/to/simple_flow.mk
```

### Ending a Simulation 

Typically a testbench is run/executed until no more signal changes occur anywhere in the design or the testbench.
Be aware, that this is something you have to take care of by ensuring that all your testbench processes eventually end with a `wait` statement.
If you, e.g., create a clock signal (which will be necessary for the majority of the testbenches) you not only have to make sure that this clock signal is turned off once your simulation is supposed to end, but also that the clock generation process ends in a `wait` statement.
If you look at the `stimulus` process of the `demo_tb` testbench, you can observe this. 

An assertion with the severity level `failure` immediately stops the simulation. 


### Special Simulation Targets

Sometimes the need might arise to have multiple different simulation targets in one makefile.
This is supported by our build system.
Assume, for example, you want to have a simulation target `sim_special` that uses a different testbench and waveform viewer configuration file than the normal `sim` target.
Then, you can do the following:

```Makefile
[...]

include path/to/simple_flow.mk

sim_gui_special: TB=special_tb
sim_gui_special: WAVE_FILE=wave_special.do
sim_gui_special: sim_gui
```

Executing the `sim_gui_special` target uses the testbench with the name `special_tb` and loads the waveform file `wave_special.do`.
It does not affect the normal `sim_gui` target.
Note that you can also create a custom target for a purely textual simulation by not setting `WAVE_FILE` for the custom target and using `sim` instead of `sim_gui`.
Be aware that the source file containing `special_tb` must be added to the `VHDL_FILES` list.

The [makefile](../../lib/mem/Makefile) of the `mem` package in the `lib` directory also uses this feature if you want to see it in action.

# Logic Synthesis and Place&Route

In this lab course we are using the [DE2-115](https://www.terasic.com.tw/cgi-bin/page/archive.pl?CategoryNo=&No=502) development board from [Terasic](https://www.terasic.com.tw/), shown in the Figure below.

![The DE2-115 Development Board with Intel Cyclone IV FPGA](.mdata/de2-115.svg)

It features a Intel Cyclone IV FPGA. 
Originally this FPGA was developed by Altera, which has been bought by Intel in 2015. 
So you might see this former name pop up in documentation or manuals from time to time.

Besides the actual FPGA, the board also features a lot of peripheral devices and ports.
These range from simple components, like LEDs, [seven-segment displays](https://en.wikipedia.org/wiki/Seven-segment_display), switches and buttons, to more advanced ICs that implement various types of memories or [DACs](https://en.wikipedia.org/wiki/Digital-to-analog_converter) to generate analog output signals (e.g., VGA).
The figure highlights some of these ICs that we are going to use/interact with in this course.

The EDA software needed for compiling hardware designs (i.e., VHDL code) for Intel FPGAs is called [Quartus Prime](https://www.intel.com/content/www/us/en/products/details/fpga/development-tools/quartus-prime.html).
This chapter will guide you through the process of creating a project, that you will use throughout this course.

To start Quartus, run the following command in a terminal (or use an application menu entry if available):

```bash
quartus --64bit &
```

After startup Quartus should roughly look like the window shown in the following screenshot:

![Quartus Prime main window (no project loaded)](.mdata/quartus_initial.png)

## Quartus Project Creation
To create a new project click on `File->New Project Wizard`, which should result in the following dialog being shown:

![New Project Wizard (Step 1)](.mdata/quartus_npw_1.png)

Click `Next`.

![New Project Wizard (Step 2)](.mdata/quartus_npw_2_path.png)

In this step, you have to configure the directory where the project is stored.
Set it to the `top/quartus` directory of this repository.
The name of the project must be set to `top`. 
For top-level entity `dbg_top` must be selected.

Click `Next`, select `Empty project` in the next step and click `Next` again.

![New Project Wizard (Step 4)](.mdata/quartus_npw_4_files.png)

In this step source files can be added to the project. 
For the DDCA course project you need to add the source files of **all** cores in the `lib` directory (i.e., the `*.vhd` and `*.qxp` files of the individual `src` directories).
For example for the `snes_ctrl` the following files are added:

 * `lib/snes_ctrl/src/snes_ctrl_top.qxp`
 * `lib/snes_ctrl/src/precompiled_snes_ctrl.vhd`
 * `lib/snes_ctrl/src/snes_ctrl_pkg.vhd`

**Do not** add `*.vho` files or any files from a `tb` directory to the Quartus project! Those files are only used for simulation purposes and are not needed for synthesis.

Additionally, add the contents of the `top/src` directory.
Finally, add the relevant files of the `demo` core, i.e.,

 * `lib/tasks/level0/demo/top_arch_demo.vhd`
 * `lib/tasks/level0/demo/src/demo.vhd`

Click `Next`.

![New Project Wizard (Step 5)](.mdata/quartus_npw_5_device.png)

Now the actual FPGA device must be configured. 
Make sure that `Family` select box is set to `Cyclone IV E`.
Choose `EP4CE115F29C7` from the list of available devices.
You can make use of the `Name Filter` text box on the right to limit the number of shown available devices.

Click `Next`.

![New Project Wizard (Step 5)](.mdata/quartus_npw_6_tools.png)

Here, you can simply leave the default settings, i.e., `<None>`.
Click `Next` and `Finish`.

**Hint:**
Quartus project files are text-based (actually they constitute TCL scripts). 
Hence, if you feel comfortable with it, you can simply edit them in any text editor, to e.g., add new files to the project.
If you are interested in how such files look like, feel free to skim the `.qsf` files in the `top/quartus` directory.
Be sure to only do this while the Quartus GUI is **not** running!

## Compiler Settings

With the project being created, we can make some adjustments to the compiler settings. 
Open the project setting dialog under `Assignments -> Settings` and go to the `Compiler Settings` in the list on the left side of the window.
Here you can select the optimization mode and change various other fine-grained synthesis and fitter settings.

![Quartus Compiler Settings Dialog](.mdata/quartus_compiler_settings.png)

The VHDL version that is used for the Quartus project can be configured in the "VHDL input" sub-menu on the left.
Be sure to set it to **VHDL 2008**, otherwise the source files will not compile.

![Quartus VHDL Input Dialog](.mdata/quartus_vhdl_input.png)

## Timing Constraints

Quartus needs to be informed about certain timing parameters and constraints.
The most important one is, arguably, the information about which signals are clocks and at what frequencies they operate.
The timing analyzer can then determine whether the compiled design reaches the performance goals (in terms of achievable maximum frequency) or not.

In Quartus (and also other EDA tools) this is done using a "Synopsys Design Constraints File" or `*.sdc` file.
Hence, create a file named `top.sdc` and place it in the `top/quartus` project directory.
Add the file to your project using the `Project -> Add/Remove Files in Project` menu entry and add the following lines to it. 

```tcl
# Clock constraints
create_clock -name "clk" -period 20.000ns [get_ports {clk}]

# Automatically constrain PLL and other generated clocks
derive_pll_clocks -create_base_clocks

# Automatically calculate clock uncertainty to jitter and other effects.
derive_clock_uncertainty
```

This minimal constraints file specifies that the `clk` input of our design is supplied with a 50 MHz clock signal.
In a later task we will have to extend this file.

## Top-Level Entity

The top-level entity selected during project creation defines the interface of the design to the "outside world".
In our case the top level entity is `dbg_top`.
This module implements a wrapper around the actual top-level entity, which is simply called `top`.
This wrapper has two functions:

* The `dbg_top` entity contains the `dbg_core`, which allows us to control the design in the Remote Lab. 
This is necessary, as it is not possible to physically interact with the board (e.g., press buttons, flip switches) or its peripherals in the Remote Lab.
Using the `dbg_core` we can emulate peripheral components of the `top` entity via a UART interface from a lab computer to an attached board. 
* Since `dbg_top` contains the actual `top` design as an instance, we can use a configuration (in particular `top_conf`) to select different architectures for the `top` entity.
Usually, every task comes with its own top-level architecture, located in the respective task directory, named `top_arch_*.vhd`.

For this tutorial we are using `top_arch_demo`, located in the file `tasks/level0/demo/top_arch_demo.vhd`

```vhdl

use work.util_pkg.all;

architecture top_arch_demo of top is
begin
	demo_inst : entity work.demo
	port map (
		a => switches(0),
		b => switches(1),
		x => ledg(0)
	);
	
	ledr <= switches;
	ledg(8 downto 1) <= (others=>'0');
	aux <= switches(15 downto 0);

	hex7 <= to_segs(x"d");
	hex6 <= to_segs(x"d");
	hex5 <= to_segs(x"c");
	hex4 <= to_segs(x"a");
	hex3 <= to_segs(x"2");
	hex2 <= to_segs(x"0");
	hex1 <= to_segs(x"2");
	hex0 <= to_segs(x"4");
	
	tx <= rx;
end architecture;

```

As you can see this architecture simply instantiates the `demo` entity and connects its inputs to switches.
The output is connected to one of the green LEDs.
Furthermore, it connects the switches to the red LEDs.
Hence, in this design, each red LED can be switched on/off by the switch directly located under it on the board.
The seven-segment displays output a constant value.

As mentioned before, the `top_conf` configuration is used to select a concrete architecture for the `top` entity. 

```vhdl

use work.all;

configuration top_conf of dbg_top is
	for dbg_top_arch
		for top_inst : top
			use entity top(top_arch_demo); -- Change the top architecture here!
		end for;
	end for;
end configuration;
```

You don't need to understand the syntax or the specific semantics of VHDL configuration constructs.
All you must know is how to select a different architecture, to quickly switch between different tasks.

Note that the `dbg_top` design also takes care of synchronizing the `keys` and `switches` inputs to the clock domain of the 50 MHz `clk` signal.
This is necessary, because unsynchonized inputs can cause [metastability](https://en.wikipedia.org/wiki/Metastability_(electronics)) in digital circuits.
For you this means that you can always safely use these inputs in your top-level design without the need for a synchronizer.

## Pinout

What we now have to do now is to add constraints to our project, such that each interface signal of the top-level entity is mapped to the right physical I/O Pin on the FPGA.
However, before we can do that, we first have to run the `Analysis and Elaboration` compilation step.
During this process, Quartus reads all source files and gathers the necessary information about the top-level interface.
To do that select `Processing -> Start -> Start Analysis and Elaboration`.

When complete run `Assignments -> Pin Planner` to open the pin assignment editor.

![Pin Planner without any pin assignments](.mdata/pp_no_assignments.png)

The lower part of this window shows a list of all signal of the top-level entity.
Notice, that for none of them a location is set.

The information, which external signal is connected to which pin of the FPGA, can be found in the [Manual](https://www.terasic.com.tw/cgi-bin/page/archive_download.pl?Language=English&No=502&FID=cd9c7c1feaa2467c58c9aa4cc02131af) of the FPGA Board.
However, to save you some time, we already provide you with most of the pin mappings.
To import them close the Pin Planner and select `Assignments -> Import Assignments`.

![Import Assignments Window](.mdata/import_assignments.png)

Select the file `pinout.qsf` in the `top/quartus` folder and click `OK`.
Afterwards open the Pin Planner again.

![Pin Planner after the import of the pinout](.mdata/pp.png)

Now everything, except for the 50 MHz input clock signal `clk`, is connected. 
Consult the FPGA board manual to find out its exact location (the signal is called `CLOCK_50` in the manual) and assign it using the Pin Planner.
**Be sure to select the correct I/O Standard!**
The default value in the Pin Planner is not correct.

## Phase Locked Loops (PLLs)
Sometimes it is necessary to run a circuit with a different clock frequency than the one provided by the external oscillator.
For that purpose FPGAs usually include PLLs.
PLLs generate a stable clock signal, which can be a rational multiple of the external reference clock.

Quartus provides a wizard that allows you to create and configure a PLL component.
This wizard can be started over the menu `Tools -> IP Catalog`.
On the right-hand side of Quartus, a docked pane called IP Catalog will appear.
Open the tree `Library -> Basic Functions -> Clocks; PLLs and Resets -> PLL` and double click `ALTPLL`.

![IP Catalog side pane in Quartus](.mdata/ip_catalog.png)


In the opening dialog (Save IP Variation) enter the name and path for your PLL implementation file (use `top/src/pll.vhd`).
In the follow-up window select VHDL as IP variation file type and click OK.

**Caution**: If the wizard that shows up after you pressed OK looks like the one in the following figure, you have to resize the window before you proceed (i.e., make the window larger such that the buttons in the lower right corner fit inside the window)!
If you don't resize the window and click on any of the controls inside the wizard, it may freeze and needs to be terminated.
However, note that this bug appears to be fixed in the Quartus version installed in the VM and the lab.
![PLL Wizard Bug](.mdata/pll_wizard_bug.png)

The below set of images illustrates how to generate a simple PLL configuration as required for this lab course (for a single output; you might need more for your specific task!).

The first important setting is the frequency of the input clock, i.e., the external oscillator.
For our board this is 50 MHz.
Leave the other settings and click Next.
![Creating a PLL - Step 1](.mdata/pll2.png)

Our customized PLL component only needs to feature two ports (input and output clock).
Thus, make sure none of the optional inputs / outputs is selected (show in the image below) and click Next.
![Creating a PLL - Step 2](.mdata/pll3.png)

Click Next until you reach the first output clock configuration window (titled `c0 - Core/External Output Clock`).
Now, add the required clock outputs by enabling them and setting their target frequency.
Each clock output has a dedicated configuration window
The following image shows this for a 25 MHz clock.
![Creating a PLL - Step 3](.mdata/pll4.png)

After you configured your output clocks, click Next until you reach the `Summary` window.
It shows the files which are generated by the wizard: The most important file is the VHDL file containing the description of the PLL component based on your settings.
Furthermore, a cmp-file is generated, which provides the component declaration of the PLL.
This declaration can be copied into your top-level design unit to be able to instantiate the PLL.
Click Finish.

![Creating a PLL - Step 4](.mdata/pll5.png)

Finally, a dialog box appears asking if you want to add the IP file to the Quartus project.
Answer the dialog with Yes.
The PLL's VHDL file will then automatically be added to your project.

You have now successfully created an IP variation of the PLL and added it to your project.

## Compilation

With the project fully set up we can now start the actual compilation process, consisting of the steps logic synthesis and place&route.
To initiate this process, use the `Processing -> Start Compilation` menu entry.
The individual compilation steps can be also be started using the `Tasks` panel on the left-hand side of the main window.

During compilation, Quartus outputs warnings and errors it encountered in the `Messages` panel, placed at the bottom of the main window.
Always check those messages as they might indicate problems with your design.
See the [list](#allowed-quartus-warnings) of allowed warnings to determine, which of the messages can be ignored safely.

The compilation was successful if it finished without any error messages. 
The result of the compilation is the bitstream (`*.sof`) file, which can now be loaded onto the FPGA.
It is located in the `top/quartus/output_files` directory.

### Quartus Makefile

You can also start the compilation process from the command line using the Makefile provided in the `top/quartus` directory.
Simply run 

```bash
make quartus
```

to start the process. 
You can also use the `quartus_gui` target to launch Quartus from the command line, automatically loading the project.

### Netlist Viewers

Quartus provides powerful tools to view and analyze the compiled design.
You can start them using the menu items in `Tools -> Netlist Viewers`.
The different viewers show the design at the different stages throughout the compilation process.

The screenshot below shows the `demo` module using the RTL Viewer.

![Quartus RTL Viewer](.mdata/rtl_viewer.png)

### Chip Planner

Using the Chip Planner (`Tools -> Chip Planner`), you can see how your design is placed inside the FPGA. 
It is even possible to make adjustments here or to derive placements constraints.
However, this goes beyond the scope of this course.
Nevertheless, it can be interesting to check it out. 

# Download and Test
After a successful compilation of your design, it is time to download the generated bitstream to the FPGA in order to try it out in hardware.

Depending on whether you work locally in the TILab, or remotely using the Remote Lab, the process to do so differs slightly.

## Local in the TILab

The makefile in the `top/quartus` directory provides the `download` target.
This target takes the generated bitstream file and downloads it to the FPGA board using its JTAG interface. 

```bash
make download 
```

The programming procedure can also be started form the Quartus GUI via the Programmer window (`Tools -> Programmer` or the corresponding button in the toolbar). 
The screenshot below shows how this window looks like:

![Quartus Programmer](.mdata/programmer.png)

Typically, all settings should be correct and the FPGA Board will be detected automatically.
However, If the textbox in the upper-left corner displays the message `No Hardware`, you have to click on the `Hardware Setup` button and connect the board manually.
Make sure `Program/Configure` is checked before you hit the `Start` button to begin programming.

Note that the bitstream is stored in the configuration SRAM of the FPGA, which is volatile memory.
This means that if the board is disconnected from power (i.e., switched off), the configuration data is lost and the FPGA "boots up" with a default design (with a lot of blinking LEDs).
Therefore, after each restart of the board you have to upload design again.

No matter if you use the makefile target or the GUI, programming should not take more than a few seconds.

**Caution:** There is a bug in the JTAG server, which can sometimes cause the programmer to fail or freeze.
In such a case perform the following routine:

 * Switch off the FPGA board (if possible)
 * Open a terminal and execute

```bash
killall jtagd
jtagconfig
```

 * Now switch the board on again. Then execute the command `jtagconfig`. The board should now be detected and you should get an output similar to:

```
1) USB-Blaster [1-3]
  020F70DD   10CL120(Y|Z)/EP3C120/..
```

 * If the command `jtagconfig` does not terminate within two seconds, repeat the whole process. Should you get an error referring to missing permissions, you have to reboot the machine.


**Important**:
Please make sure to always log out when you leave the lab! The missing permissions error mentioned above is caused by multiple people being logged in at the same time.

## Remote Lab

As explained in the course introduction, the whole exercise part of the DDCA lab course can be done remotely via our *Remote Lab*.
Please run the following command on the computer from which you want to access the Remote Lab (i.e., most probably the VM).

```
pip install git+https://git.inf2.tuwien.ac.at/sw/rpatools
```

This command installs the tools `rpa_shell` and `rpa_gui`, RPA standing for Remote Place Assigner.
We have prepared a [short video tutorial](https://tuwel.tuwien.ac.at/mod/opencast/view.php?id=2275019) that shows you how to use these tools and how to interact with the hardware in the Remote Lab.

## UART
As mentioned above, the `dbg_core` in the `dbg_top` design is controlled over a [UART](https://en.wikipedia.org/wiki/Universal_asynchronous_receiver-transmitter) interface.
However, notice that the `top` design also features a UART port (the signals `tx` and `rx`).
This UART port is a "virtual" one implemented by the `dbg_core`.
Hence, data send and received there actually goes through the debug interface (i.e., the `tx` and `rx` signals of the `top` design) and is wrapped in the protocol used by the `dbg_core` to communicate with the host PC.

You don't have to understand how this works.
The only important thing is that, this means that you **cannot** simply use tools like `minicom` or `GTKTerm` to send data to and receive data from your design (i.e., the `top` design).
To do that you have to use the tool `remote.py` available on all lab PCs (local and remote).

Notice that the `top_arch_demo` architecture simply connects the `tx` signal to the `rx` signal.
This has the effect that data send to the `top` design via the `rx` input is simply returned unchanged via the `tx` output (loopback).
We will use this behavior in the following examples.

To send the text `Hello FPGA` to the design use the following command:

```bash
remote.py -u "Hello FPGA"
```

To receive data, again use the `-u` flag, but without the data argument:

```bash
remote.py -u
```

Because of the loopback in the `top` design this command will print out `Hello FPGA`.
To print the received data in hexadecimal form, use the `--hex` command line flag

```bash
remote.py -u --hex
```

Assuming we again send `Hello FPGA`, this command prints `48656c6c6f2046504741` (0x48 is the ASCII code for `H`, 0x65 for `e`, etc.).
To further format the output you can use the `--line-limit` argument:

```bash
remote.py -u --hex --line-limit 2
```

This now prints:

```
48
65
6c
6c
6f
20
46
50
47
41
```

You can also use the `--hex` command line argument when sending data.
The tool then interprets the argument has hexadecimal data.
The following commands are all equivalent, in that they all send the exact same data.

```bash
remote.py -u "Hello FPGA"
remote.py -u --hex "48656c6c6f2046504741"
remote.py -u --hex "48 65 6c 6c 6f 20 46 50 47 41"
remote.py -u --hex "48656c6c6f2046504741 # comment"
```

Be careful with leading zeros.
The following two commands are **not** equivalent.

```bash
remote.py -u --hex "0"
remote.py -u --hex "00000000"
```

The first command sends a single byte with the value zero, the the second one send four zero-bytes.
If the number of hexadecimal digits in a number is odd, an additional leading `0` is assumed.

You can also send complete files in one go, via the `--file` command line flag:
```bash
remote.py -u --file some_file
```

Each line of the input file is processed as if it were directly passed to `remote.py` via the command line.

All these features can also be accessed conveniently via our web interface in the *Remote Lab*.

![UART Web Interface](.mdata/uart_web_interface.png)

# Coding Guidelines
This section specifies the VHDL coding guidelines used in the DDCA lab course.
The use of these rules is mandatory and ignoring them leads to point reductions!

## VHDL Version
This course uses VHDL-2008.

## Reset
**All** registers must be set to a defined value during reset.
This means that every signal that is written in a synchronous process must also get an appropriate reset value!

## Active Clock Edge
This section only applies to **synthesizeable** code, i.e., it does not apply to test code (testbenches).
In the lab course only the **rising edge** is used as the active clock edge.
Hence, you are not allowed to use `falling_edge` anywhere for your design.
Moreover, `rising_edge` must **only** be used on clock signals.

## Packages
Package files should always be named with the suffix `_pkg.vhd` and the package name itself should end in the suffix `_pkg`.
For example, the memory package provided by our library is called `mem_pkg` and resides in the file `mem_pkg.vhd`.

If you need a package body, put it in the **same** file as the actual package.

## Sensitivity Lists
If you use explicit sensitivity lists, they must contain all required signals and must not contain any spurious signals.
Check the Hardware Modeling course material for detailed information about sensitivity lists.
Since the course uses VHDL-2008, you can also employ simplified sensitivity lists via the use of the `all` keyword.
However, be aware that some tasks may disallow this method and require explicit lists!

## Testbenches
Testbench files should always be named with the suffix `_tb.vhd` and stored in a `tb` directory.
The testbench itself should be named after the module it tests, extended with the suffix `_tb`.
For example the testbench of the demo module is named `demo_tb` and placed in the file `tb/demo_tb.vhd`.
A testbench must always contain a correct reset operation, i.e., in the beginning of the simulation, a reset pulse must be applied to the UUT in order to reset all internal registers and output signals of the UUT.
The simulation must not show any undefined signals (i.e., red signal traces) except for a brief period before the reset.
This means that at the beginning of a simulation **all** input signals to the UUT have to be initialized!


Whenever you are asked to provide a report (or something similar requiring simulation screenshots): In simulation screenshots a clock signal, signal names and time axis must always be visible.
Time intervals must always be measured from a signal edge to another edge!
Use the cursor alignment features in QuestaSim to place the cursors exactly on the edges.

Simulations should also not trigger **any** warnings (except for the 0 ps time step or during reset).

## Entities and Architectures
Entities and their respective architectures must always be put into the same file (this rule does not apply to architectures of the `top` entity).
A single file should only contain a single entity.
The file name should be the name of the entity.

## Instances
To avoid the introduction of bugs associated with wrong signal mappings, **only the "named mapping" style** for connecting wires to an instantiated module must be used.
The use of "positional mapping" is, hence, not allowed.
Unused instance outputs must always be explicitly marked with the `open` keyword.

It is also highly recommended to use the "named mapping" style for function and procedure calls with many parameters.

## Indentation
Use **either** tabs or spaces to properly indent and format your code.
**Don't mix** indentation styles in one document!

## Allowed Quartus Warnings
Although your design might be correct, Quartus still outputs some warnings during the compilation process.
The table below lists all allowed warnings, i.e., warnings that won’t have a negative impact on your assessment.
However, all other warnings indicate problems with your design.

|ID| Description  |
|--|--------------|
|10540|VHDL Signal Declaration warning at top.vhd([...]): used explicit default value for signal [...] because signal was never assigned a value|
|12240|Synthesis found one or more imported partitions that will be treated as black boxes for timing analysis during synthesis|
|13009|TRI or OPNDRN buffers permanently enabled.|
|13024|Output pins are stuck at VCC or GND|
|13039|The following bidirectional pins have no drivers|
|15064|"PLL [...]\|altpll:altpll_component\|pll_altpll:auto_generated\|[...]" output port clk[...] feeds output pin [...] via non-dedicated routing -- jitter performance depends on switching rate of other design elements. Use PLL dedicated clock outputs to ensure jitter performance|
|15705|Ignored locations or region assignments to the following nodes|
|15714|Some pins have incomplete I/O assignments. Refer to the I/O Assignment Warnings report for details|
|18236|Number of processors has not been specified which may cause overloading on shared machines. Set the global assignment NUM_PARALLEL_PROCESSORS in your QSF to an appropriate value for best performance.|
|21074|Design contains [...] input pin(s) that do not drive logic|
|169177|[...] pins must meet Intel FPGA requirements for 3.3-, 3.0-, and 2.5-V interfaces. For more information, refer to AN 447: Interfacing Cyclone IV E Devices with 3.3/3.0/2.5-V LVTTL/LVCMOS I/O Systems.|
|171167|Found invalid Fitter assignments. See the Ignored Assignments panel in the Fitter Compilation Report for more information.|
|276020|Inferred RAM node [...] from synchronous design logic. Pass-through logic has been added to match the read-during-write behavior of the original design.|
|276027|Inferred dual-clock RAM node [...] from synchronous design logic. The read-during-write behavior of a dual-clock RAM is undefined and may not match the behavior of the original design.|
|292013|Feature LogicLock is only available with a valid subscription license. You can purchase a software subscription to gain full access to this feature.|

Please take special care of the warnings 13024 (*Output pins are stuck at VCC or GND*) and 21074 (*Design contains input pin(s) that do not drive logic*), as they still might indicate problems with your design.
Thoroughly check which signals these warnings are reported for!
If you have, for example, an input button that should trigger some action in your design, but Quartus reports that it does not *drive any logic*, then there certainly is a problem.
If you intentionally drive some output with a certain constant logic level (for example an unused seven-segment display), then the *stuck at VCC or GND* warning is fine.

Depending on which peripherals are used by a particular top-level architecture, the warning 10540 (*used explicit default value for signal [...]*) might be reported for several signals of the `top` entity.
This is fine, as long as you do not use a particular signal.
Likewise, warning 13039 (*bidirectional pins have no drivers*) can also be ignored for a similar reason.


