library ieee;
    use ieee.std_logic_1164.all;
    use work.task_adder4_pkg.all;
    use work.all;

entity adder4_entity is
    port (
        A    : in  std_logic_vector(3 downto 0);
        B    : in  std_logic_vector(3 downto 0);
        Cin  : in  std_logic;

        S    : out std_logic_vector(3 downto 0);
        Cout : out std_logic
    );
end entity;

architecture adder4_arch of adder4_entity is
    -- for fulladder_inst: fulladder use entity fulladder_entity(fulladder_arch);
    signal C_between : std_logic_vector(3 + 1 downto 0);
begin
    C_between(0) <= Cin;
    Cout         <= C_between(C_between'high);

    generateLoop: for i in A'range generate
        -- These entity instantiations should not be used!
        fulladder_inst: entity work.fulladder_entity(fulladder_arch)
        port map (
                A(i),
                B(i),
                C_between(i),
                S(i),
                C_between(i + 1)
        );
    end generate;
end architecture;
