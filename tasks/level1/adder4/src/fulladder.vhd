library ieee;
    use ieee.std_logic_1164.all;
    use work.task_adder4_pkg.all;
    use work.all;

entity fulladder_entity is
    port (
        A    : in  STD_LOGIC;
        B    : in  STD_LOGIC;
        Cin  : in  STD_LOGIC;

        S    : out STD_LOGIC;
        Cout : out STD_LOGIC
    );
end entity;

architecture fulladder_arch of fulladder_entity is
    for halfadder_inst1: halfadder use entity halfadder_entity(halfadder_arch);
        for halfadder_inst2: halfadder use entity halfadder_entity(halfadder_arch);
            for or_gate_inst: or_gate use entity or_gate_entity(or_gate_arch);

                signal S_1, C_1, C_2 : STD_LOGIC;

            begin
                halfadder_inst1: halfadder
                port map (
                        A, B, S_1, C_1
                );

                halfadder_inst2: halfadder
                port map (
                        Cin, S_1, S, C_2
                );

                or_gate_inst: or_gate
                port map (
                        C_1, C_2, Cout
                );
            end architecture;

            -- configuration adder_conf of adder_entity is
            --     for adder_arch
            --         for xor_gate_inst : xor_gate
            --             use entity xor_gate_entity(xor_gate_arch);
            --         end for;
            --         for and_gate_inst : and_gate
            --             use entity and_gate_entity(and_gate_arch);
            --         end for;
            --     end for;
            -- end configuration adder_conf;
