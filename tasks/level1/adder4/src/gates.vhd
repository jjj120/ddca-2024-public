library ieee;
    use ieee.std_logic_1164.all;

entity xor_gate_entity is
    port (
        A : in  STD_LOGIC;
        B : in  STD_LOGIC;
        Z : out STD_LOGIC
    );
end entity;

architecture xor_gate_arch of xor_gate_entity is
begin
    Z <= (A xor B);
end architecture;

-------------------------
library ieee;
    use ieee.std_logic_1164.all;

entity and_gate_entity is
    port (
        A : in  STD_LOGIC;
        B : in  STD_LOGIC;
        Z : out STD_LOGIC
    );
end entity;

architecture and_gate_arch of and_gate_entity is
begin
    Z <= (A and B);
end architecture;

-------------------------
library ieee;
    use ieee.std_logic_1164.all;

entity or_gate_entity is
    port (
        A : in  STD_LOGIC;
        B : in  STD_LOGIC;
        Z : out STD_LOGIC
    );
end entity;

architecture or_gate_arch of or_gate_entity is
begin
    Z <= (A or B);
end architecture;
