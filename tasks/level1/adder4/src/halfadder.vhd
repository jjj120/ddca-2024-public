library ieee;
    use ieee.std_logic_1164.all;
    use work.task_adder4_pkg.all;
    use work.all;

entity halfadder_entity is
    port (
        A : in  STD_LOGIC;
        B : in  STD_LOGIC;
        S : out STD_LOGIC;
        C : out STD_LOGIC
    );
end entity;

architecture halfadder_arch of halfadder_entity is
    for xor_gate_inst: xor_gate use entity xor_gate_entity(xor_gate_arch);

        for and_gate_inst: and_gate use entity and_gate_entity(and_gate_arch);
        begin
            xor_gate_inst: xor_gate
            port map (
                    A, B, S
            );

            and_gate_inst: and_gate
            port map (
                    A, B, C
            );
        end architecture;

        -- configuration halfadder_conf of halfadder_entity is
        --     for halfadder_arch
        --         for xor_gate_inst : xor_gate
        --             use entity xor_gate_entity(xor_gate_arch);
        --         end for;
        --         for and_gate_inst : and_gate
        --             use entity and_gate_entity(and_gate_arch);
        --         end for;
        --     end for;
        -- end configuration halfadder_conf;
