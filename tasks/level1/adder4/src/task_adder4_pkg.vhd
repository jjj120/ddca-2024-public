library ieee;
use ieee.std_logic_1164.all;

package task_adder4_pkg is
	component xor_gate is 
		port (
			A : in std_logic;
			B : in std_logic;

			Z : out std_logic
		);
	end component;
	
	component and_gate is 
		port (
			A : in std_logic;
			B : in std_logic;

			Z : out std_logic
		);
	end component;

	component or_gate is 
		port (
			A : in std_logic;
			B : in std_logic;

			Z : out std_logic
		);
	end component;

	component halfadder is
		port (
			A : in std_logic;
			B : in std_logic;
	
			S : out std_logic;
			C : out std_logic
		);
	end component;

	component fulladder is
		port (
			A    : in std_logic;
			B    : in std_logic;
			Cin  : in std_logic;
	
			S    : out std_logic;
			Cout : out std_logic
		);
	end component;

	component adder4 is
		port (
			A    : in std_logic_vector(3 downto 0);
			B    : in std_logic_vector(3 downto 0);
			Cin  : in std_logic;
	
			S    : out std_logic_vector(3 downto 0);
			Cout : out std_logic
		);
	end component;
end package;
