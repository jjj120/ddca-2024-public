
# 4-Bit Adder
**Points:** 1 ` | ` **Keywords:** structural modeling, combinational

Your task is to create a 4-bit ripple-carry adder using structural modeling.
Start by checking out the [`task_adder4_pkg.vhd`](src/task_adder4_pkg.vhd) package file and study all the components within.
Then check out the [wikipedia article for adders](https://en.wikipedia.org/wiki/Adder_(electronics)).

Implement fitting entities and architectures for each component in `task_adder4_pkg` in following order:

- First, create implementations (i.e., entities and architectures) for the basic logic gates (i.e., for the `and_gate`, the `xor_gate` as well as the `or_gate` components).

- Now, implement the the `halfadder` by creating instances of the basic gates and connect them accordingly.

- Then, link two half adders and an OR gate to build the `fulladder`.
For reference, look at the circuit diagram of such a full adder below:

![Full Adder](.mdata/fulladder.svg)

- By connecting multiple full adders you can finally create a [4-bit Ripple-carry adder](https://en.wikipedia.org/wiki/Adder_(electronics)#Ripple-carry_adder) and implement the `ader4` entity.

# Testbench

Create a testbench for the `adder4` entity.
A suitable template is already provided in [`tb/adder4_tb.vhd`](tb/adder4_tb.vhd).

The testbench already defines the procedure `test_values` that you have to implement!
To make this work you will have to study and use VHDL type conversions.

Include a few examples in which the result does not overflow (carry output (`Cout`) is low) and one in which it does (carry output (`Cout`) is high).

# Hardware Test

Instantiate your `adder4` entity in the top-level architecture ([`top_arch_adder4.vhd`](top_arch_adder4.vhd)) of the task.
Use the basic board peripherals (i.e., LEDs, switches, seven-segment displays) to enable your circuit to communicate with the "outside world".

Add the required files to your Quartus project and configure the top-level configuration (`top_conf`) accordingly.
Compile the design with Quartus and download it to the FPGA.
Check if it behaves as expected.
