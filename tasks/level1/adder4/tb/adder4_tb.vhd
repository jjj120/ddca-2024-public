library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.task_adder4_pkg.all;
    use work.all;

entity adder4_tb is
end entity;

architecture bench of adder4_tb is
    for uut_adder4: adder4 use entity adder4_entity(adder4_arch);
        signal A, B : std_logic_vector(3 downto 0) := (others => '0');
        signal Cin  : std_logic                    := '0';

        signal S    : std_logic_vector(3 downto 0);
        signal Cout : std_logic;

    begin
        -- Instantiate the unit under test
        uut_adder4: adder4 port map (A, B, Cin, S, Cout);

        -- Stimulus process

        stimulus: process
            function to_std_logic(i : in integer) return std_logic is
            begin
                if i = 0 then
                    return '0';
                end if;
                return '1';
            end function;

            procedure test_values(value_a, value_b, value_c : INTEGER) is
                variable adder_test_vector : std_logic_vector(S'high + 1 downto S'low);
                variable adder_test_value  : integer;
            begin
                A <= std_logic_vector(to_unsigned(value_a, A'high + 1));
                B <= std_logic_vector(to_unsigned(value_b, B'high + 1));
                Cin <= to_std_logic(value_c);

                wait for 10 ns;

                adder_test_vector := (S'high + 1 => Cout,(S'range) => S);
                adder_test_value := to_integer(unsigned(adder_test_vector));

                assert adder_test_value = value_a + value_b + value_c
                    report "Test Fulladder " & to_string(value_a) & " + " & to_string(value_b) & " + " & to_string(value_c) & " failed: " & to_string(Cout) & to_string(S)
                    severity error;
            end procedure;

        begin
            report "Testing";

            for value_a in 0 to (2 **(A'high + 1) - 1) loop
                for value_b in 0 to (2 **(B'high + 1) - 1) loop
                    for value_c in 0 to 1 loop
                        test_values(value_a, value_b, value_c);
                    end loop;
                end loop;
            end loop;

            report "End Testing";
            wait;
        end process;
    end architecture;

