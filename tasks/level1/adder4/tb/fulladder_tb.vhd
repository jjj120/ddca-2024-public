library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.task_adder4_pkg.all;
    use work.all;

entity fulladder_tb is
end entity;

architecture bench of fulladder_tb is
    for uut_halfadder: halfadder use entity halfadder_entity(halfadder_arch);
        for uut_fulladdder: fulladder use entity fulladder_entity(fulladder_arch);

            signal A, B, Cin               : STD_LOGIC := '0';
            signal S, Cout, S_test, C_test : STD_LOGIC;
        begin
            -- Instantiate the unit under test
            uut_halfadder: halfadder port map (A, B, S_test, C_test);
            uut_fulladdder: fulladder port map (A, B, Cin, S, Cout);

            -- Stimulus process

            stimulus: process
                function to_std_logic(i : in integer) return std_logic is
                begin
                    if i = 0 then
                        return '0';
                    end if;
                    return '1';
                end function;

                procedure test_values(value_a, value_b, value_c : INTEGER) is
                    variable fulladder_test_vector : std_logic_vector(1 downto 0);
                    variable halfadder_test_vector : std_logic_vector(1 downto 0);
                begin
                    A <= to_std_logic(value_a);
                    B <= to_std_logic(value_b);
                    Cin <= to_std_logic(value_c);

                    wait for 10 ns;

                    fulladder_test_vector := (1 => Cout, 0 => S);
                    halfadder_test_vector := (1 => C_test, 0 => S_test);

                    assert fulladder_test_vector = std_logic_vector(to_unsigned(value_a + value_b + value_c, 2))
                        report "Test Fulladder " & to_string(value_a) & "+" & to_string(value_b) & "+" & to_string(value_c) & " failed: " & to_string(S) & to_string(Cout)
                        severity error;

                    assert halfadder_test_vector = std_logic_vector(to_unsigned(value_a + value_b, 2))
                        report "Test Halfadder " & to_string(value_a) & "+" & to_string(value_b) & "+" & to_string(value_c) & " failed: " & to_string(S_test) & to_string(C_test)
                        severity error;

                end procedure;
            begin
                report "Testing";

                for value_a in 0 to 1 loop
                    for value_b in 0 to 1 loop
                        for value_c in 0 to 1 loop
                            test_values(value_a, value_b, value_c);
                        end loop;
                    end loop;
                end loop;

                report "Finished";
                wait;
            end process;
        end architecture;
