LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE work.task_adder4_pkg.ALL;
USE work.ALL;

ENTITY gates_tb IS
END ENTITY;

ARCHITECTURE bench OF gates_tb IS
    FOR uut_xor : xor_gate USE ENTITY xor_gate_entity(xor_gate_arch);
    FOR uut_and : and_gate USE ENTITY and_gate_entity(and_gate_arch);
    FOR uut_or : or_gate USE ENTITY or_gate_entity(or_gate_arch);

    SIGNAL A, B : STD_LOGIC := '0';
    SIGNAL XOR_SIG, AND_SIG, OR_SIG : STD_LOGIC;
BEGIN
    -- Instantiate the unit under test
    uut_xor : xor_gate PORT MAP(A, B, XOR_SIG);
    uut_and : and_gate PORT MAP(A, B, AND_SIG);
    uut_or : or_gate PORT MAP(A, B, OR_SIG);

    -- Stimulus process
    stimulus : PROCESS
    BEGIN
        REPORT "Test";

        A <= '0';
        B <= '0';
        WAIT FOR 10 ns;
        ASSERT XOR_SIG = '0' REPORT "0 xor 0 failed";
        ASSERT AND_SIG = '0' REPORT "0 and 0 failed";
        ASSERT OR_SIG = '0' REPORT "0 or 0 failed";

        A <= '1';
        B <= '0';
        WAIT FOR 10 ns;
        ASSERT XOR_SIG = '1' REPORT "1 xor 0 failed";
        ASSERT AND_SIG = '0' REPORT "1 and 0 failed";
        ASSERT OR_SIG = '1' REPORT "1 or 0 failed";

        A <= '0';
        B <= '1';
        WAIT FOR 10 ns;
        ASSERT XOR_SIG = '1' REPORT "0 xor 1 failed";
        ASSERT AND_SIG = '0' REPORT "0 and 1 failed";
        ASSERT OR_SIG = '1' REPORT "0 or 1 failed";

        A <= '1';
        B <= '1';
        WAIT FOR 10 ns;
        ASSERT XOR_SIG = '0' REPORT "1 xor 1 failed";
        ASSERT AND_SIG = '1' REPORT "1 and 1 failed";
        ASSERT OR_SIG = '1' REPORT "1 or 1 failed";

        WAIT;
    END PROCESS;
END ARCHITECTURE;