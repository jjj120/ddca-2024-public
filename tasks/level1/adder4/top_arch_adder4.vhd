library ieee;
	use ieee.std_logic_1164.all;
	use work.util_pkg.all;
	use work.task_adder4_pkg.all;

architecture top_arch_adder4 of top is
	signal S : std_logic_vector(3 downto 0);
	signal Cout : std_logic;
	for adder : adder4 use entity work.adder4_entity(adder4_arch);
begin
	adder : adder4
	port map (
		A => switches(17 downto 14),
		B => switches(3 downto 0),
		Cin => switches(9),
		S => S,
		Cout => Cout
	);
	
	ledr(17 downto 14) <= switches(17 downto 14);
	ledr(3 downto 0) <= switches(3 downto 0);
	ledr(9) <= switches(9);
	
	ledg(3 downto 0) <= S;
	ledg(4) <= Cout;
	
	hex6 <= to_segs(switches(17 downto 14));    -- first digit
	hex4 <= to_segs(switches(3 downto 0));    -- second digit
	hex0 <= to_segs(S);    -- lower res digit
	hex1 <= to_segs(x"1") when Cout = '1' else (others => '1'); -- higher res digit
	
	hex2 <= (others => '1');
	hex3 <= (others => '1');
	hex5 <= (others => '1');
	hex7 <= (others => '1');
end architecture;
