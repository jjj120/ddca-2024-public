library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.math_real.all;
    use work.alu_pkg.all;

entity alu is
    generic (
        DATA_WIDTH : positive := 32
    );
    port (
        op   : in  alu_op_type;
        A, B : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
        R    : out std_logic_vector(DATA_WIDTH - 1 downto 0) := (others => '0');
        Z    : out std_logic                                 := '0'
    );
end entity;

architecture arch of alu is
    signal slt, sltu, sll_s, srl_s, sra_s, add, sub : std_logic_vector(R'range) := (others => '0');
    constant x : integer := positive(ceil(log2(real(DATA_WIDTH)))) - 1;

    function convert_to_std_logic(bool : boolean) return std_logic is
    begin
        if bool then
            return '1';
        else
            return '0';
        end if;
    end function;
begin
    with op select R <=
        B       when ALU_NOP,
        slt     when ALU_SLT,
        sltu    when ALU_SLTU,
        sll_s   when ALU_SLL,
        srl_s   when ALU_SRL,
        sra_s   when ALU_SRA,
        add     when ALU_ADD,
        sub     when ALU_SUB,
        A and B when ALU_AND,
        A or B  when ALU_OR,
        A xor B when ALU_XOR;

    slt(0)                        <= '1' when signed(A) < signed(B) else '0';
    slt(DATA_WIDTH - 1 downto 1)  <= (others => '0');
    sltu(0)                       <= '1' when unsigned(A) < unsigned(B) else '0';
    sltu(DATA_WIDTH - 1 downto 1) <= (others => '0');

    sll_s <= std_logic_vector(shift_left(unsigned(A), to_integer(unsigned(B(x downto 0)))));
    srl_s <= std_logic_vector(shift_right(unsigned(A), to_integer(unsigned(B(x downto 0)))));
    sra_s <= std_logic_vector(shift_right(signed(A), to_integer(unsigned(B(x downto 0)))));

    -- sll_s <= A sll B(x downto 0);
    -- srl_s <= A srl B(x downto 0);
    -- sra_s <= A sra B(x downto 0);
    add <= std_logic_vector(signed(A) + signed(B));
    sub <= std_logic_vector(signed(A) - signed(B));

    with op select Z <=
        not R(0)                        when ALU_SLT,
            not R(0)                    when ALU_SLTU,
            convert_to_std_logic(A = B) when ALU_SUB,
            '-'                         when others;

end architecture;
