
# ALU (Arithmetic logic unit)
**Points:** 2 ` | ` **Keywords:** combinational, arithmetic operations, generics, random testing

[ALUs](https://en.wikipedia.org/wiki/Arithmetic_logic_unit) are combinational circuits that perform arithmetic and bitwise logic operations on integers.
They are an essential part of every processor.
The ALU created in this task will be used in the computer-architecture-realted tasks later on in the course.

Create an ALU with the following interface:

```vhdl
component alu is
	generic (
		DATA_WIDTH : positive := 32
	);
	port (
		op   : in  alu_op_type;
		A, B : in  std_logic_vector(DATA_WIDTH-1 downto 0);
		R    : out std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
		Z    : out std_logic := '0'
	);
end component;
```

The `alu` component is located in the package [`alu_pkg`](src/alu_pkg.vhd), which also declares the enumerated type used for the `op` input.
To specify the bit-width of the ALU the generic `DATA_WIDTH` it used.
This generic, thus, defines the widths of the two operands `A` and `B` as well as the result output `R`.
You may assume that is value is a power of 2.
The ALU also features the output flag `Z`, which is set for certain operations, to e.g., indicate if the result of a subtraction is zero.

The operations of the ALU are defined in the following table:

|op       | R                       | Z          |
|---------|-------------------------|------------|
|ALU_NOP  | B                       | don't care |
|ALU_SLT  | A < B ? 1 : 0, signed   | not R(0)   |
|ALU_SLTU | A < B ? 1 : 0, unsigned | not R(0)   |
|ALU_SLL† | A sll B(x downto 0)     | don't care |
|ALU_SRL  | A srl B(x downto 0)     | don't care |
|ALU_SRA  | A sra B(x downto 0)     | don't care |
|ALU_ADD  | A + B, signed           | don't care |
|ALU_SUB  | A - B, signed           | A = B      |
|ALU_AND  | A and B                 | don't care |
|ALU_OR   | A or B                  | don't care |
|ALU_XOR  | A xor B                 | don't care |

Use `'-'` (i.e., the IEEE 1164 value for don't care) for the "don't care" entries of the `Z` output and not arbitrary values.
Note that the shift operations can be implemented conveniently with the functions `shift_left()` and `shift_right()` from the package `numeric_std`.
The variable `x` used in the table must be derived from the `DATA_WIDTH` generic (for 32 bits its value is 4).

## Testbench
Create a testbench for the `alu` entity.
The file [`tb/alu_tb.vhd`](tb/alu_tb.vhd) already contains a suitable template.

If `DATA_WIDTH` is large, it is not possible to test every possible input value combination.
Hence, we are going to used random input data in this testbench.
For each of the possible values for `op` use 256 random test values for the inputs `A` and `B`.
The testbench template provides the `rand_slv` function that you can use for this purpose.

Please note that the testbench should work for arbitrary values for for the `DATA_WIDTH` constant.

## Hardware Test

Instantiate a 4-bit version of the ALU in the top-level architecture [`top_arch_alu`](top_arch_alu.vhd).

 * connect `switches(3 downto 0)` to `A` and to `hex7` (using the `to_segs` function)
 * connect `switches(7 downto 4)` to `B` and to `hex6` (using the `to_segs` function)
 * connect `R` to `hex5` (using the `to_segs` function)
 * connect `Z` to `ledg(8)`

Set all unused `hex*` outputs to the all-one vector.
Add a process to the top-level architecture that converts `switches(17 downto 14)` to suitable values of the enum type used for the `op` input.

Add the required files to your Quartus project and configure the top-level configuration accordingly.
Compile the design with Quartus and download it to the FPGA.
Check if the design behaves correctly (for a few input values).

