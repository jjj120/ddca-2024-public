
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.math_real.all;

    use work.alu_pkg.all;

entity alu_tb is
end entity;

architecture tb of alu_tb is
    constant DATA_WIDTH : positive := 32;
    signal op   : alu_op_type;
    signal A, B : std_logic_vector(DATA_WIDTH - 1 downto 0) := (others => '0');
    signal R    : std_logic_vector(DATA_WIDTH - 1 downto 0);
    signal Z    : std_logic;
begin

    alu_inst: alu
        generic map (
            DATA_WIDTH => DATA_WIDTH
        )
        port map (
            op => op,
            A  => A,
            B  => B,
            R  => R,
            Z  => Z
        );

    stimuli: process
        variable seed1, seed2 : integer := 1;

        impure function rand_slv(len : positive) return std_logic_vector is
            variable random_real : real;
            variable slv         : std_logic_vector(len - 1 downto 0);
        begin
            for i in slv'range loop
                uniform(seed1, seed2, random_real);
                slv(i) := '1';
                if (random_real > 0.5) then
                    slv(i) := '0';
                end if;
            end loop;
            return slv;
        end function;

        procedure checkSingleOp(A_val : std_logic_vector(DATA_WIDTH - 1 downto 0); B_val : std_logic_vector(DATA_WIDTH - 1 downto 0); op_val : alu_op_type) is
            constant A_val_signed   : signed   := signed(A_val);
            constant B_val_signed   : signed   := signed(B_val);
            constant A_val_unsigned : unsigned := unsigned(A_val);
            constant B_val_unsigned : unsigned := unsigned(B_val);
            constant x              : integer  := positive(ceil(log2(real(DATA_WIDTH)))) - 1;
        begin
            op <= op_val;
            wait for 10 ms;

            case op_val is
                when ALU_NOP =>
                    assert R = B_val
                        report "Failed Test ALU_NOP  (R): A=" & to_string(A_val) & ", B=" & to_string(B_val) & " R=" & to_string(R);

                when ALU_SLT =>
                    if (A_val_signed < B_val_signed) then
                        assert to_integer(unsigned(R)) = 1
                            report "Failed Test ALU_SLT  (R): A=" & to_string(A_val) & ", B=" & to_string(B_val) & " R=" & to_string(R);
                        assert Z = '0'
                            report "Failed Test ALU_SLT  (Z): A=" & to_string(A_val) & ", B=" & to_string(B_val) & " Z=" & to_string(Z);
                    else
                        assert to_integer(unsigned(R)) = 0
                            report "Failed Test ALU_SLT  (R): A=" & to_string(A_val) & ", B=" & to_string(B_val) & " R=" & to_string(R);
                        assert Z = '1'
                            report "Failed Test ALU_SLT  (Z): A=" & to_string(A_val) & ", B=" & to_string(B_val) & " Z=" & to_string(Z);
                    end if;

                when ALU_SLTU =>
                    if (A_val_unsigned < B_val_unsigned) then
                        assert to_integer(unsigned(R)) = 1
                            report "Failed Test ALU_SLTU (R): A=" & to_string(A_val) & ", B=" & to_string(B_val) & " R=" & to_string(R);
                        assert Z = '0'
                            report "Failed Test ALU_SLT  (Z): A=" & to_string(A_val) & ", B=" & to_string(B_val) & " Z=" & to_string(Z);
                    else
                        assert to_integer(unsigned(R)) = 0
                            report "Failed Test ALU_SLTU (R): A=" & to_string(A_val) & ", B=" & to_string(B_val) & " R=" & to_string(R);
                        assert Z = '1'
                            report "Failed Test ALU_SLT  (Z): A=" & to_string(A_val) & ", B=" & to_string(B_val) & " Z=" & to_string(Z);
                    end if;

                when ALU_SLL =>
                    assert (unsigned(R)) = unsigned(shift_left(A_val_unsigned, to_integer(B_val_unsigned(x downto 0))))
                        report "Failed Test ALU_SLL  (R): A=" & to_string(A_val) & ", B=" & to_string(B_val) & " R=" & to_string(R);

                when ALU_SRL =>
                    assert (unsigned(R)) = unsigned(shift_right(A_val_unsigned, to_integer(B_val_unsigned(x downto 0))))
                        report "Failed Test ALU_SRL  (R): A=" & to_string(A_val) & ", B=" & to_string(B_val) & " R=" & to_string(R);

                when ALU_SRA =>
                    assert (signed(R)) = signed(shift_right(A_val_signed, to_integer(B_val_unsigned(x downto 0))))
                        report "Failed Test ALU_SRA  (R): A=" & to_string(A_val) & ", B=" & to_string(B_val) & " R=" & to_string(R);

                when ALU_ADD =>
                    assert (signed(R)) = A_val_signed + B_val_signed
                        report "Failed Test ALU_ADD  (R): A=" & to_string(A_val) & ", B=" & to_string(B_val) & " R=" & to_string(R);

                when ALU_SUB =>
                    assert (signed(R)) = A_val_signed - B_val_signed
                        report "Failed Test ALU_SUB  (R): A=" & to_string(A_val) & ", B=" & to_string(B_val) & " R=" & to_string(R);
                    if (A_val = B_val) then
                        assert Z = '1'
                            report "Failed Test ALU_SUB  (Z): A=" & to_string(A_val) & ", B=" & to_string(B_val) & " Z=" & to_string(Z);
                    else
                        assert Z = '0'
                            report "Failed Test ALU_SUB  (Z): A=" & to_string(A_val) & ", B=" & to_string(B_val) & " Z=" & to_string(Z);
                    end if;

                when ALU_AND =>
                    assert (signed(R)) =(A_val_signed and B_val_signed)
                        report "Failed Test ALU_AND  (R): A=" & to_string(A_val) & ", B=" & to_string(B_val) & " R=" & to_string(R);

                when ALU_OR =>
                    assert (signed(R)) =(A_val_signed or B_val_signed)
                        report "Failed Test ALU_OR   (R): A=" & to_string(A_val) & ", B=" & to_string(B_val) & " R=" & to_string(R);

                when ALU_XOR =>
                    assert (signed(R)) =(A_val_signed xor B_val_signed)
                        report "Failed Test ALU_XOR  (R): A=" & to_string(A_val) & ", B=" & to_string(B_val) & " R=" & to_string(R);

            end case;

        end procedure;

        procedure checkALU(A_val : std_logic_vector(DATA_WIDTH - 1 downto 0); B_val : std_logic_vector(DATA_WIDTH - 1 downto 0)) is
        begin
            A <= A_val;
            B <= B_val;

            for i in alu_op_type'left to alu_op_type'right loop
                checkSingleOp(A_val, B_val, i);
            end loop;
        end procedure;
    begin

        report "Start testing";
        for i in 1 to 256 loop
            checkALU(rand_slv(DATA_WIDTH), rand_slv(DATA_WIDTH));
        end loop;

        report "Finished testing";
        wait;
    end process;
end architecture;

