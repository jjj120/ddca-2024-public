    use ieee.numeric_std.all;
    use work.util_pkg.all;
    use work.alu_pkg.all;

architecture top_arch_alu of top is
    constant DATA_WIDTH : positive := 4;
    signal op   : alu_op_type;
    signal A, B : std_logic_vector(DATA_WIDTH - 1 downto 0) := (others => '0');
    signal R    : std_logic_vector(DATA_WIDTH - 1 downto 0);
    signal Z    : std_logic;

    for alu_inst: alu use entity work.alu(arch);
    begin
        alu_inst: alu
        generic map (DATA_WIDTH)
        port map (
                op, A, B, R, Z
        );

        A <= switches(3 downto 0);
        hex7 <= to_segs(A);

        B <= switches(7 downto 4);
        hex6 <= to_segs(B);
		  
		  ledr(7 downto 0) <= switches(7 downto 0);
		  ledr(17 downto 15) <= switches(17 downto 15);
		  ledg(3 downto 0) <= R;

        hex5 <= to_segs(R);
        ledg(8) <= Z;

        conv_proc: process (all)
		      variable curr_op : alu_op_type;
				constant off : std_logic_vector(6 downto 0) := "1111111";
				constant n : std_logic_vector(6 downto 0) := "0101011";
				constant o : std_logic_vector(6 downto 0) := "0100011";
				constant p : std_logic_vector(6 downto 0) := "0001100";
				constant s : std_logic_vector(6 downto 0) := to_segs(x"5");
				constant l : std_logic_vector(6 downto 0) := "1000111";
				constant t : std_logic_vector(6 downto 0) := "0111001";
				constant u : std_logic_vector(6 downto 0) := "1000001";
				constant r : std_logic_vector(6 downto 0) := "0101111";
				constant a : std_logic_vector(6 downto 0) := to_segs(x"a");
				constant d : std_logic_vector(6 downto 0) := to_segs(x"d");
				constant b : std_logic_vector(6 downto 0) := to_segs(x"b");
				constant x : std_logic_vector(6 downto 0) := "0100001";
		  
        begin
            -- switches(17 downto 15)
            op <= alu_op_type'val(to_integer(unsigned(switches(17 downto 15))));
				curr_op := alu_op_type'val(to_integer(unsigned(switches(17 downto 15))));
				
				hex4 <= (others => '1');
				
				case op is
					when ALU_NOP => 
						hex3 <= off;
						hex2 <= n;
						hex1 <= o;
						hex0 <= p;
					when ALU_SLT => 
						hex3 <= off;
						hex2 <= s;
						hex1 <= l;
						hex0 <= t;
					when ALU_SLTU => 
						hex3 <= s;
						hex2 <= l;
						hex1 <= t;
						hex0 <= u;
					when ALU_SLL => 
						hex3 <= off;
						hex2 <= s;
						hex1 <= l;
						hex0 <= l;
					when ALU_SRL => 
						hex3 <= off;
						hex2 <= s;
						hex1 <= r;
						hex0 <= l;
					when ALU_SRA => 
						hex3 <= off;
						hex2 <= s;
						hex1 <= r;
						hex0 <= a;
					when ALU_ADD => 
						hex3 <= off;
						hex2 <= a;
						hex1 <= d;
						hex0 <= d;
					when ALU_SUB => 
						hex3 <= off;
						hex2 <= s;
						hex1 <= u;
						hex0 <= b;
					when ALU_AND => 
						hex3 <= off;
						hex2 <= a;
						hex1 <= n;
						hex0 <= d;
					when ALU_OR => 
						hex3 <= off;
						hex2 <= off;
						hex1 <= o;
						hex0 <= r;
					when ALU_XOR => 
						hex3 <= off;
						hex2 <= x;
						hex1 <= o;
						hex0 <= r;
					when others => 
						hex3 <= "0111111";
						hex2 <= "0111111";
						hex1 <= "0111111";
						hex0 <= "0111111";
				end case;
				
        end process;

    end architecture;


