library ieee;
    use ieee.std_logic_1164.all;

entity basic is
    port (
        A, B    : in  std_logic;
        X, Y, Z : out std_logic
    );
end entity;

architecture beh of basic is
begin
    X <= A or B;
    Y <= A and B;
    Z <= A xor B;
end architecture;
