
# Basic VHDL Entity
**Points:** 1 ` | ` **Keywords:** combinational

Create a VHDL entity named `basic` with two (single-bit) inputs named `a` and `b` and three (single-bit) outputs named `x`, `y`, and `z`.
Place the entity in the file `src/basic.vhd`.
The outputs shall be assigned values according to the following truth table:

| a | b | x | y | z |
|---|---|---|---|---|
| 0 | 0 | 0 | 0 | 0 |
| 0 | 1 | 1 | 0 | 1 |
| 1 | 0 | 1 | 0 | 1 |
| 1 | 1 | 1 | 1 | 0 |

Identify the basic logic operators and use the respective VHDL operators to generate the outputs `x`, `y`, and `z`.

## Testbench
Create a testbench for the `basic` entity and place it in the file `tb/basic_tb.vhd`.
Apply all possible input values and use assertions to check the outputs.
Create a makefile that runs the simulation.


## Hardware Test
Add a component declaration of the `basic` entity to the declaration section of the top-level architecture [`top_arch_basic.vhd`](top_arch_basic.vhd).
Instatiniate this component with the instance name `basic_a`.
Then add a second instance with the name `basic_b`, but this time instantantiate the entity directly.
Connect the inputs of both instances to switches and their outputs to LEDs.

Add the required files to your Quartus project and configure the top-level configurtion accordingly.
Compile the design with Quartus and download it to the FPGA.
Check if the LEDs react correctly to the state of the switches.
