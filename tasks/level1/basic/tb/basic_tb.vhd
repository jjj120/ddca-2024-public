library ieee;
    use ieee.std_logic_1164.all;

entity basic_tb is
end entity;

architecture bench of basic_tb is
    signal A, B    : std_logic;
    signal X, Y, Z : std_logic;

    component basic is
        port (
            A, B    : in  std_logic;
            X, Y, Z : out std_logic
        );
    end component;

begin

    uud: basic port map (A, B, X, Y, Z);

    stimuli: process
    begin
        report "Testing";

        for a_in in std_logic range '0' to '1' loop
            for b_in in std_logic range '0' to '1' loop
                A <= a_in;
                B <= b_in;

                wait for 10 ns;

                assert X =(a_in or b_in) report "Test " & to_string(A) & " or " & to_string(A) & " failed!";
                assert Y =(a_in and b_in) report "Test " & to_string(A) & " and " & to_string(A) & " failed!";
                assert Z =(a_in xor b_in) report "Test " & to_string(A) & " xor " & to_string(A) & " failed!";

            end loop;
        end loop;

        report "Finished Testing";
        wait;
    end process;

end architecture;
