architecture top_arch_basic of top is
    component basic is
        port (
            A, B    : in  std_logic;
            X, Y, Z : out std_logic
        );
    end component;
begin
    basic_a: basic
        port map (
            A => switches(0),
            B => switches(1),
            X => ledg(0),
            Y => ledg(1),
            Z => ledg(2)
        );

    basic_b: entity work.basic(beh)
        port map (
            A => switches(2),
            B => switches(3),
            X => ledg(3),
            Y => ledg(4),
            Z => ledg(5)
        );
		  
    ledr <= switches;
end architecture;
