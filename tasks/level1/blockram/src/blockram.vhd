library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.task_blockram_pkg.all;

entity blockram is
    generic (
        ADDR_WIDTH : natural := 8;
        DATA_WIDTH : natural := 32
    );
    port (
        clk    : in  std_logic;
        reset  : in  std_logic;

        rdaddr : in  std_logic_vector(ADDR_WIDTH - 1 downto 0);
        rddata : out std_logic_vector(DATA_WIDTH - 1 downto 0);

        wraddr : in  std_logic_vector(ADDR_WIDTH - 1 downto 0);
        wrdata : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
        wr     : in  std_logic
    );
end entity;

architecture beh of blockram is
    type mem is array (0 to (2 ** ADDR_WIDTH) - 1) of std_logic_vector(rddata'range);
begin
    mem_write: process (clk)
        variable mem_block : mem;
    begin
        if rising_edge(clk) then
            rddata <= mem_block(to_integer(unsigned(rdaddr)));

            if wr = '1' then
                mem_block(to_integer(unsigned(wraddr))) := wrdata;
                if rdaddr = wraddr then
                    rddata <= wrdata;
                end if;
            end if;

            if unsigned(rdaddr) = 0 then
                rddata <= (others => '0');
            end if;
        end if;

    end process;
end architecture;

architecture beh_reset of blockram is
    type mem is array (0 to (2 ** ADDR_WIDTH) - 1) of std_logic_vector(rddata'range);
begin
    mem_write: process (clk)
        variable mem_block : mem;
    begin
        if rising_edge(clk) then
            rddata <= mem_block(to_integer(unsigned(rdaddr)));

            if wr = '1' then
                mem_block(to_integer(unsigned(wraddr))) := wrdata;
                if rdaddr = wraddr then
                    rddata <= wrdata;
                end if;
            end if;

            if unsigned(rdaddr) = 0 then
                rddata <= (others => '0');
            end if;

            if reset = '1' then
                for i in 0 to (2 ** ADDR_WIDTH) - 1 loop
                    mem_block(i) := (others => '0');
                end loop;
            end if;
        end if;

    end process;
end architecture;
