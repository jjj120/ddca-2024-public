library ieee;
    use ieee.std_logic_1164.all;

package task_blockram_pkg is
    component blockram is
        generic (
            ADDR_WIDTH : natural := 8;
            DATA_WIDTH : natural := 32
        );
        port (
            clk    : in  std_logic;
            reset  : in  std_logic;

            rdaddr : in  std_logic_vector(ADDR_WIDTH - 1 downto 0);
            rddata : out std_logic_vector(DATA_WIDTH - 1 downto 0);

            wraddr : in  std_logic_vector(ADDR_WIDTH - 1 downto 0);
            wrdata : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
            wr     : in  std_logic
        );
    end component;
end package;
