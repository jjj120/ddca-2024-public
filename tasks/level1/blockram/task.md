
# Block RAM
**Points:** 1 ` | ` **Keywords:** memory

Your task is to create a synchronous memory block.
Start by checking out the component interface:

```vhdl
component blockram is
	generic (
		ADDR_WIDTH : natural := 8;
		DATA_WIDTH : natural := 32
	);
	port (
		clk    : in std_logic;
		reset  : in std_logic;

		rdaddr : in std_logic_vector(ADDR_WIDTH - 1 downto 0);
		rddata : out std_logic_vector(DATA_WIDTH - 1 downto 0);

		wraddr : in std_logic_vector(ADDR_WIDTH - 1 downto 0);
		wrdata : in std_logic_vector(DATA_WIDTH - 1 downto 0);
		wr     : in std_logic
	);
end component;
```

Implement a dual-port RAM (single read, single write) with "write-through".
This means that your RAM is supposed to output the data on `wrdata` on `rddata` immediately (i.e., in the same clock cycle) if the read and write address are the same and a write is performed.
You can achieve this by checking whether `wraddr` and `rdaddr` are equal if `wr` is asserted.
Note that this behavior implies that there must be a combinational path from `wrdata` to `rddata` in your memory.

Additionally, reads on address 0 should always return 0.

Ignore the reset signal for your first implementation!

Refer to the [design recommendations for inferring RAM functions from HDL Code](https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/ug/ug-qpp-design-recommendations.pdf) (Section 1.4.1) for (Intel FPGA) implementation guidelines on memories.

## Testbench

Create a testbench for the `blockram` entity and place it in the file `tb/blockram_tb.vhd`.

Make your testbench save a few data words into your RAM entity and read them back afterwards.
Use assertions to check the data.
Additionally, show that the write-through works properly.

## Hardware Test

Add an instance of the blockram entity to the top-level architecture in `top_arch_blockram.vhd`.

Make the following connections and configurations:

- Make the `ADDR_WIDTH` 4 bits wide
- Make the `DATA_WIDTH` 4 bits wide
- Connect the top-level `clk` signal to the `clk` input
- Connect `keys(0)` to `res_n`
- Connect `switches(3 downto 0)` to `readaddr` and to `hex0`
- Connect `switches(7 downto 4)` to `writeaddr` and to `hex1`
- Connect `switches(11 downto 8)` to `wrdata` and to `hex2`
- Connect `not keys(3)` to `wr` (the keys are active-low, but `wr` is not!)
- Display `rddata` using `hex3`

Use the `to_segs` function to connect the respective signals to the seven-segment displays.

Add the required files to your Quartus project and configure the top-level configuration accordingly.
Compile the design with Quartus and download it to the FPGA.
Check if the `blockram` is able to store a few words and is able to read previously stored ones repeatedly.


Make sure that Quartus successfully inferred a RAM function and an actual memory block is synthesized (box denoted with `SYNC_RAM`). 
To do so open Quartus' RTL viewer and search for your entity.

Lastly, implement an additional architecture `beh_reset` for your `blockram` entity and add reset functionality.
The (active-low) reset should assign 0 to each RAM address.
Instantiate both `blockram` architectures inside your top-level architecture in `top_arch_blockram.vhd`.
Assign the same signals but display `rddata` of the second `blockramm` on `hex5`.
Open Quartus' RTL viewer and study the difference of both `blockram` instances.

