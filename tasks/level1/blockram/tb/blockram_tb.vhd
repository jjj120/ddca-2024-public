library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.math_real.all;
    use work.task_blockram_pkg.all;

entity blockram_tb is

end entity;

architecture bench of blockram_tb is
    constant ADDR_WIDTH  : integer := 4;
    constant DATA_WIDTH  : integer := 4;
    constant CLOCK_CYCLE : time    := 5 ns;

    signal clk    : std_logic                                 := '0';
    signal reset  : std_logic                                 := '0';
    signal rdaddr : std_logic_vector(ADDR_WIDTH - 1 downto 0);
    signal rddata : std_logic_vector(DATA_WIDTH - 1 downto 0) := (others => '0');
    signal wraddr : std_logic_vector(ADDR_WIDTH - 1 downto 0) := (others => '0');
    signal wrdata : std_logic_vector(DATA_WIDTH - 1 downto 0) := (others => '0');
    signal wr     : std_logic                                 := '0';

    signal finished : std_logic := '0';

    for uud_without_reset: blockram use entity work.blockram(beh);
        -- for uud_with_reset: blockram use entity blockram(beh_reset);

    begin
        -- Instantiate the unit under test
        uud_without_reset: blockram
        generic map (
                ADDR_WIDTH,
                DATA_WIDTH
        )
        port map (
                clk    => clk,
                reset  => reset,
                rdaddr => rdaddr,
                rddata => rddata,
                wraddr => wraddr,
                wrdata => wrdata,
                wr     => wr
        );

        -- Stimulus process

        stimulus: process
            procedure write_data(data : std_logic_vector(DATA_WIDTH - 1 downto 0); addr : std_logic_vector(ADDR_WIDTH - 1 downto 0)) is
            begin
                wait until falling_edge(clk);
                wrdata <= data;
                wraddr <= addr;
                wr <= '1';
                wait until rising_edge(clk);
            end procedure;

            procedure read_data(addr : std_logic_vector) is
            begin
                wait until falling_edge(clk);
                rdaddr <= addr;
                wait until rising_edge(clk);
                wait for 0 ns;
            end procedure;

            procedure test_ram is
            begin
                for addr_int in 0 to 2 ** ADDR_WIDTH - 1 loop
                    -- write full of zeros
                    write_data(std_logic_vector(to_unsigned(0, DATA_WIDTH)), std_logic_vector(to_unsigned(addr_int, ADDR_WIDTH)));
                end loop;

                wait until rising_edge(clk);
                wr <= '0';

                for addr_int in 0 to 2 ** ADDR_WIDTH - 1 loop
                    -- check all zeros
                    read_data(std_logic_vector(to_unsigned(addr_int, ADDR_WIDTH)));
                    assert unsigned(rddata) = 0 report "Read not 0 at " & to_string(addr_int);
                end loop;

                for addr_int in 0 to 2 ** ADDR_WIDTH - 1 loop
                    -- write full with its own address
                    write_data(std_logic_vector(to_unsigned(addr_int, DATA_WIDTH)), std_logic_vector(to_unsigned(addr_int, ADDR_WIDTH)));
                end loop;

                wait until rising_edge(clk);
                wr <= '0';

                for addr_int in 0 to 2 ** ADDR_WIDTH - 1 loop
                    -- check all addresses
                    read_data(std_logic_vector(to_unsigned(addr_int, ADDR_WIDTH)));
                    assert unsigned(rddata) = to_unsigned(addr_int, DATA_WIDTH) report "Read not addr at " & to_string(addr_int) & " : " & to_string(rddata);
                end loop;
            end procedure;
        begin
            report "simulation start";
            rdaddr <= (others => '0');

            -- Apply test stimuli
            test_ram;

            wait for CLOCK_CYCLE * 5;
            wrdata <= std_logic_vector(to_unsigned(12, DATA_WIDTH));
            wraddr <= std_logic_vector(to_unsigned(10, DATA_WIDTH));
            rdaddr <= std_logic_vector(to_unsigned(10, DATA_WIDTH));
            wr <= '1';
            wait until falling_edge(clk);

            wait until rising_edge(clk);
            assert rddata = wrdata report "Write through not working";

            finished <= '1';
            report "simulation end";
            -- End simulation
            wait;
        end process;

        clock_proc: process
        begin
            if finished = '1' then
                wait;
            end if;

            clk <= '1';
            wait for CLOCK_CYCLE / 2;
            clk <= '0';
            wait for CLOCK_CYCLE / 2;
        end process;

    end architecture;
