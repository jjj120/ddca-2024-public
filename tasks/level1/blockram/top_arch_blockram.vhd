use work.util_pkg.to_segs;

architecture top_arch_blockram of top is
	 constant zero : std_logic_vector := "1111111";
    constant ADDR_WIDTH  : integer := 4;
    constant DATA_WIDTH  : integer := 4;

    signal rddata : std_logic_vector(DATA_WIDTH - 1 downto 0) := (others => '0');
begin
        blockram_inst: entity work.blockram(beh_reset)
        generic map (
                ADDR_WIDTH,
                DATA_WIDTH
        )
        port map (
                clk    => clk,
                reset  => not keys(0),
                rdaddr => switches(3 downto 0),
                rddata => rddata,
                wraddr => switches(7 downto 4),
                wrdata => switches(11 downto 8),
                wr     => not keys(3)
        );
		  
		  hex3 <= to_segs(rddata);
		  
		  ledr(11 downto 0) <= switches(11 downto 0);
		  ledg(6) <= not keys(3);
		  ledg(0) <= not keys(0);
		  
		  hex0 <= zero;
		  hex1 <= zero;
		  hex2 <= zero;
		  hex4 <= zero;
		  hex5 <= zero;
		  hex6 <= zero;
		  hex7 <= zero;
end architecture;
