onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /blockram_tb/clk
add wave -noupdate /blockram_tb/reset
add wave -noupdate /blockram_tb/rdaddr
add wave -noupdate /blockram_tb/rddata
add wave -noupdate /blockram_tb/wraddr
add wave -noupdate /blockram_tb/wrdata
add wave -noupdate /blockram_tb/wr
add wave -noupdate /blockram_tb/finished
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {420 ns}
