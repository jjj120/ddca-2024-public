library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity adder4 is
	port (
		A : in std_logic_vector(3 downto 0);
		B   : in std_logic_vector(3 downto 0);
		Cin : in std_logic;

		S  : out std_logic_vector(3 downto 0);
		Cout  : out std_logic
	);
end entity;

architecture beh of adder4 is
	signal adder_cout : std_logic_vector(3 downto 0) := "0000";
begin
	chatgpt_solution: process(all)
		variable temp: std_logic_vector(4 downto 0);
		variable cinv: std_logic_vector(3 downto 0);
	begin
		cinv := "000" & cin;
		temp := std_logic_vector(unsigned('0' & a) + unsigned('0' & b) + unsigned(cinv));
		S <= temp(3 downto 0);
		cout <= temp(4);
	end process;
end architecture;


