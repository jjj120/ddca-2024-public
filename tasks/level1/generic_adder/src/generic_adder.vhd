library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.generic_adder_pkg.all;
    use work.task_adder4_pkg.all;

entity generic_adder is
    generic (
        N : positive := 4
    );
    port (
        A    : in  std_logic_vector(N - 1 downto 0);
        B    : in  std_logic_vector(N - 1 downto 0);

        S    : out std_logic_vector(N - 1 downto 0);
        Cout : out std_logic
    );
begin
    assert N = 4 *(N / 4) report "N must devisible by 4!" severity failure;
end entity;

architecture generic_adder_arch of generic_adder is
    signal c_signals : std_logic_vector(N / 4 downto 0) := (others => '0');
    -- for all: adder4 use entity work.adder4_entity(adder4_arch);

begin
    c_signals(0) <= '0';
    Cout         <= c_signals(N / 4);

    gen_loop: for i in 0 to N / 4 - 1 generate
        adder_inst: entity work.adder4_entity(adder4_arch)
        port map (
                A    => A((4 * i) + 3 downto (4 * i)),
                B    => B((4 * i) + 3 downto (4 * i)),
                Cin  => c_signals(i),
                S    => S((4 * i) + 3 downto (4 * i)),
                Cout => c_signals(i + 1)
        );
    end generate;

end architecture;
