library ieee;
    use ieee.std_logic_1164.all;

package generic_adder_pkg is
    component generic_adder is
        generic (
            N : positive := 4
        );
        port (
            A    : in  std_logic_vector(N - 1 downto 0);
            B    : in  std_logic_vector(N - 1 downto 0);

            S    : out std_logic_vector(N - 1 downto 0);
            Cout : out std_logic
        );
    end component;
end package;
