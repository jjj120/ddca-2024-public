
# Generic Adder
**Points:** 2 ` | ` **Keywords:** structural modeling, generics

Your task is to create a generic N-bit adder with the following interface (defined in [`generic_adder_pkg`](src/generic_adder_pkg.vhd)):

```vhdl
component generic_adder is
	generic (
		N : positive := 4
	);
	port (
		A    : in std_logic_vector(N-1 downto 0);
		B    : in std_logic_vector(N-1 downto 0);

		S    : out std_logic_vector(N-1 downto 0);
		Cout : out std_logic
	);
end component;
```

The `generic_adder` must be implemented by chaining together multiple 4-bit adders.
If you completed the `adder4` task you can use the entity you created there.
Otherwise we provide you with an [implementation](src/adder4.vhd).
As a reminder, here it the interface of the `adder4` entity:

```vhdl
entity adder4 is
	port (
		A : in std_logic_vector(3 downto 0);
		B   : in std_logic_vector(3 downto 0);
		Cin : in std_logic;

		S  : out std_logic_vector(3 downto 0);
		Cout  : out std_logic
	);
end entity;
```

Besides its `port` section, the `generic_adder` component also contains a `generic` section, where it defines the generic `N` which determines the bit width of the adder.
Since you are supposed to chain 4-bit adders you have to make sure that the bit width is divisible by 4 at compile time using assertions in the entity.
You can get a hint on how to solve this by checking out the FIFO entities in `lib/mem/`
Place the implementation of the `generic_adder` in the file `src/generic_adder.vhd`

**Hint:**
In order to solve this task you will need to study `for generate` statements.

# Testbench

Create two testbenchs for the your `generic_adder` adder, named [`exhaustive_tb`](tb/exhaustive_tb.vhd) and [`fibonacci_tb`](tb/fibonacci_tb.vhd).

- `exhaustive_tb`: Instantiate an 8-bit adder and create a stimulus process that exhaustively tests whether it correctly calculates all possible A * B possible additions.
Do not forget to also check the correct value of the `Cout` signal.

- `fibonacci_tb`: Instantiate a 32-bit adder and use it to calculate the [fibonacci sequence](https://en.wikipedia.org/wiki/Fibonacci_sequence) starting by adding 0 and 1. 
Stop when the carry out bit is high and report the last calculated number as well as the number of step it took to get there.

# Hardware Test

Integrate your entity into the top-level design (`top_arch_generic_adder.vhd`).
Think of a convenient way to enable your circuit to communicate with the real world.
Compile the design with Quartus and download it to the FPGA.

