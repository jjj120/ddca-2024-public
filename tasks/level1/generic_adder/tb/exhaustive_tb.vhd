library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.generic_adder_pkg.all;

entity exhaustive_tb is
end entity;

architecture bench of exhaustive_tb is
    constant N : positive := 8;
    signal A, B, S : std_logic_vector(N - 1 downto 0);
    signal Cout    : std_logic;
begin
    -- Instantiate the units under test
    uut: generic_adder
        generic map (N)
        port map (
            A, B, S, Cout
        );

    -- Stimulus process

    stimulus: process
        variable sum : unsigned(N - 1 downto 0);
    begin
        report "Simulation start";

        for a_val in 0 to 2 ** N - 1 loop
            for b_val in 0 to 2 ** N - 1 loop
                A <= std_logic_vector(to_unsigned(a_val, N));
                B <= std_logic_vector(to_unsigned(b_val, N));
                wait for 10 ns;

                sum := unsigned(A) + unsigned(B);

                if sum > to_unsigned(2 ** N - 1, N) then
                    assert Cout = '1'
                        report "Test Cout failed: " & to_string(A) & " + " & to_string(B) & ", Cout: " & to_string(Cout);
                end if;
                assert sum(N - 1 downto 0) = unsigned(S)
                    report "Test S failed: " & to_string(A) & " + " & to_string(B) & ", S: " & to_string(S);
            end loop;
        end loop;

        report "Simulation end";
        -- End simulation
        wait;
    end process;
end architecture;

