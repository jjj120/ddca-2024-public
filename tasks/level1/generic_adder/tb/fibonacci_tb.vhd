library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.generic_adder_pkg.all;

entity fibonacci_tb is
end entity;

architecture bench of fibonacci_tb is
    constant N : positive := 32;
    signal A    : std_logic_vector(N - 1 downto 0) := (others => '0');
    signal B    : std_logic_vector(N - 1 downto 0) := (others => '0');
    signal S    : std_logic_vector(N - 1 downto 0);
    signal Cout : std_logic                        := '0';
begin
    -- Instantiate the units under test
    uut: generic_adder
        generic map (N)
        port map (
            A    => A,
            B    => B,
            S    => S,
            Cout => Cout
        );

    -- Stimulus process

    stimulus: process
        variable steps : integer := 0;
    begin
        report "simulation start";

        A <= (others => '0');
        B <= (0 => '1', others => '0');

        while Cout /= '1' loop

            wait for 10 ns;
            steps := steps + 1;

            A <= B;
            B <= S;
        end loop;

        report "Calculated Fibonacci Number " & to_string(Cout) & to_string(S) & " in " & to_string(steps);

        report "simulation end";
        -- End simulation
        wait;
    end process;
end architecture;

