library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.gray_pkg.all;

entity gray is
    generic (
        DATA_WIDTH : integer := 8
    );
    port (
        conversion : in  std_logic;
        number     : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
        result     : out std_logic_vector(DATA_WIDTH - 1 downto 0)
    );
end entity;

architecture arch of gray is
    signal conv_to_binary : std_logic_vector(DATA_WIDTH - 1 downto 0);
    signal conv_to_gray   : std_logic_vector(DATA_WIDTH - 1 downto 0);
begin
    -- conv_to_gray
    conv_to_gray <= (number xor std_logic_vector(shift_left(unsigned(number), 1)));

    -- conv_to_binary
    conv_to_binary(DATA_WIDTH - 1) <= number(DATA_WIDTH - 1);

    gen_bin: for i in DATA_WIDTH - 2 downto 0 generate
        conv_to_binary(i) <= conv_to_binary(i + 1) xor number(i);
    end generate;

    result <= conv_to_gray when conversion = '0' else
              conv_to_binary;

end architecture;
