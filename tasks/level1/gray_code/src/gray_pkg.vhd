library ieee;
use ieee.std_logic_1164.all;

package gray_pkg is
	component gray is
		generic (
			DATA_WIDTH : integer := 8
		);
		port (
			conversion : in std_logic;
			number     : in  std_logic_vector(DATA_WIDTH-1 downto 0);
			result     : out std_logic_vector(DATA_WIDTH-1 downto 0)
		);
	end component;
end package;
