
# Gray Code
**Points:** 2 ` | ` **Keywords:** combinational, generics


## Intro
You might have already heard in other courses that it is sometimes beneficial for a binary code to only change a single bit between two code words. In hardware this is, for example, used for rotary encoder sensors or FIFOs between clock-domain-crossings. However, please note that you are not required to understand this - this background information is just here for completeness and those of you interested in these kinds of things.

One code that satisfies this property is the so-called [Gray Code](https://en.wikipedia.org/wiki/Gray_code), illustrated in the following table. Compare the binary and gray code values an observe that sometimes more than one bit changes between two succeeding entries in the binary column.

| dec. number | binary | gray code|
|-------------|--------|----------|
|      0      |   00   |    00    |
|      1      |   01   |    01    |
|      2      |   10   |    11    |
|      3      |   11   |    10    |

The following Python code shows you how one can convert from binary-encoded to Gray-encoded numbers and vice versa:

```python
def binary_to_gray(number):
  # ^ denotes the bitwise xor, >> a right shift
  return number ^ (number >> 1)

def gray_to_binary(number):
  mask = number
  while not mask = 0:
    mask = mask >> 1
    number = number ^ mask
  return number
```

## Task
Your task is to implement these two conversions in hardware module called `gray` and to test your design for correctness.
The module shall have the following interface:

```vhdl
component gray is
	generic (
		DATA_WIDTH : integer := 8
	);
	port (
		conversion : in std_logic;
		number     : in  std_logic_vector(DATA_WIDTH-1 downto 0);
		result     : out std_logic_vector(DATA_WIDTH-1 downto 0)
	);
end component;
```
The input applied at `number` is, depending on the value of the `conversion` input, treated as binary/Gray-encoded number and converted to Gray/binary-code.
The output `result` provides the result of this conversion.
When `conversion` is low, the performed conversion shall be from binary number to Gray code and, for `conversion` being high, from Gray code to the respective binary number.

**Hints**:
- Use a `generate` loop for the conversion from Gray-code values to the respective binary numbers.
- Keep things as generic as possible. Make use of the `DATA_WIDTH` generic and don't introduce any magic numbers
- Note that simple shifts with a constant shift-width, such as the ones required here, can easily be implemented using concatenation and indexed access of `std_logic_vector`s

### Testbench
Create a testbench for `gray` in [tb/gray_tb](tb/gray_tb).
Test your design for a `DATA_WIDTH` of 8. Your testbench should apply all possible 8-bit values as input for `number` for both conversion types.
Furthermore, implement two functions in your testbench, one per conversion, that takes an input and converts it either from Gray to a binary number or vice versa.
Use these two functions for comparison with the `result` output of the unit-under-test and report deviations.

**Remark:** You might ask yourself, what use it is to implement the same functionality twice. However, recall that a testbench is not required to be synthesizable. With VHDL being a full-featured programming language, we can make use of its more powerful features like loops, file handling and even object-oriented concepts.
In our case here, you can make use of a loop for the conversion - something you were not able to do in `gray.vhd`.
You might want to think about the difference between the two loop structures you likely used in the design and the testbench.

### Hardware test

Once you have implemented and tested your converter, it is time to use it. To do this, instantiate the converter in [top_arch_gray.vhd](top_arch_gray.vhd).
Connect its `result` output to the red LEDs (`ledr`). The `conversion` input shall be connected to `switches(17)`.
Set the `DATA_WIDTH` generic of the converter to a value smaller than 17 (there are only 18 switches and red LEDs).
Connect the remaining `switches` (how many depends on your chosen `DATA_WIDTH`) to the `number` input, beginning with `switches(0)`.
Finally, synthesize your design and, if Quartus only reports allowed warnings, test in on an actual board.
