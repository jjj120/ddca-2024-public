library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.gray_pkg.all;

entity gray_tb is
end entity;

architecture arch of gray_tb is
    constant DATA_WIDTH : integer := 8;
    signal conversion     : std_logic;
    signal number, result : std_logic_vector(DATA_WIDTH - 1 downto 0);
begin

    stim: process is
        function bin_to_gray(num : std_logic_vector(DATA_WIDTH - 1 downto 0)) return std_logic_vector is
        begin
            return num xor (num srl 1);
        end function;

        function gray_to_bin(num : std_logic_vector(DATA_WIDTH - 1 downto 0)) return std_logic_vector is
            variable mask       : std_logic_vector(DATA_WIDTH - 1 downto 0) := num;
            variable number_tmp : std_logic_vector(DATA_WIDTH - 1 downto 0) := num;
        begin
            while not (or mask) = '0' loop
                mask := (mask srl 1);
                number_tmp := number_tmp xor mask;
            end loop;

            return number_tmp;
        end function;

        procedure checkEquality(calc_res : std_logic_vector; conversion_string : string) is
        begin
            assert result = calc_res report "Test " & conversion_string & " failed. input=" & to_string(number) & ", result=" & to_string(result) & ", right calc=" & to_string(calc_res);
        end procedure;

    begin
        report "Testbench start";

        for num in 0 to 2 ** DATA_WIDTH - 1 loop
            -- binary to gray
            conversion <= '0';
            number <= std_logic_vector(to_unsigned(num, DATA_WIDTH));

            wait for 10 ns;
            checkEquality(bin_to_gray(number), "binary to gray");

            -- gray to binary
            conversion <= '1';

            wait for 10 ns;
            checkEquality(gray_to_bin(number), "gray to binary");
        end loop;
        report "Testbench done";
        wait;
    end process;

    uut: gray
        generic map (
            DATA_WIDTH => DATA_WIDTH
        )
        port map (
            conversion => conversion,
            number     => number,
            result     => result
        );
end architecture;
