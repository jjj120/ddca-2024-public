use work.util_pkg.to_segs;

architecture top_arch_gray of top is
signal result : std_logic_vector(15 downto 0);
begin
    converter: entity work.gray
        generic map (16)
        port map (
            conversion => switches(17),
            number     => switches(15 downto 0),
            result     => result
        );

	 ledr(15 downto 0) <= result;
	 
    hex0 <= to_segs(switches(3 downto 0));
    hex1 <= to_segs(switches(7 downto 4));
    hex2 <= to_segs(switches(11 downto 8));
    hex3 <= to_segs(switches(15 downto 12));

    hex4 <= to_segs(result(3 downto 0));
    hex5 <= to_segs(result(7 downto 4));
    hex6 <= to_segs(result(11 downto 8));
    hex7 <= to_segs(result(15 downto 12));
	 
end architecture;
