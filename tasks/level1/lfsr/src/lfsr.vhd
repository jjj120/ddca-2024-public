library ieee;
    use ieee.std_logic_1164.all;

    use work.lfsr_pkg.all;

entity lfsr is
    generic (
        LFSR_WIDTH : integer;
        POLYNOMIAL : std_logic_vector
    );
    port (
        clk       : in  std_logic;                                 -- clock
        res_n     : in  std_logic;                                 -- active low reset
        load_seed : in  std_logic;                                 -- active high trigger to load in new seed
        seed      : in  std_logic_vector(LFSR_WIDTH - 1 downto 0); -- seed to load in
        prdata    : out std_logic                                  -- random bit output
    );
begin
    assert (POLYNOMIAL'high = LFSR_WIDTH - 1) and (POLYNOMIAL'low = 0) report "POLYNOMIAL has to have width LFSR_WIDTH and begin at 0!" severity failure;
end entity;

architecture arch of lfsr is
    signal saved_state : std_logic_vector(LFSR_WIDTH - 1 downto 0) := (others => '1');
begin
    main_proc: process (clk)
        variable random_new_bit : std_logic := '0';
    begin
        if rising_edge(clk) then
            if res_n = '0' then
                saved_state <= (others => '1');
                prdata <= '1';
            elsif load_seed = '1' then
                saved_state <= seed;
            else
                random_new_bit := '0';

                for i in LFSR_WIDTH - 1 downto 0 loop
                    if POLYNOMIAL(i) = '1' then
                        random_new_bit := random_new_bit xor saved_state(i);
                    end if;
                end loop;

                for i in 0 to LFSR_WIDTH - 2 loop
                    saved_state(i + 1) <= saved_state(i);
                end loop;
                saved_state(0) <= random_new_bit;

                prdata <= saved_state(LFSR_WIDTH - 1);
            end if;
        end if;
    end process;
end architecture;
