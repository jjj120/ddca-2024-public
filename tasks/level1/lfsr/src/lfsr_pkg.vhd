library ieee;
use ieee.std_logic_1164.all;

package lfsr_pkg is
	component lfsr is
		generic (
			LFSR_WIDTH : integer;
			POLYNOMIAL : std_logic_vector
		);
		port (
			clk : in std_logic;
			res_n : in std_logic;
			load_seed : in std_logic;
			seed : in std_logic_vector(LFSR_WIDTH-1 downto 0);
			prdata : out std_logic
		);
	end component;
end package;
