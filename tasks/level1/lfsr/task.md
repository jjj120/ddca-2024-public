
# Linear-Feedback Shift Register (LFSR)
**Points:** 2 ` | ` **Keywords:** registers, generics

# Linear Feedback Shift Register (LFSR)

## Intro
[LFSRs](https://en.wikipedia.org/wiki/Linear-feedback_shift_register) are often used in hardware to efficiently generate pseudo-random numbers.
Their basic structure is quite simple.
An LFSR consists of a shift register (i.e., a chain of D flip flops) and some feedback function that provides the next input value to the shift register.
Typically, this feedback function is an XOR function over some bits of the shift register's stored value.

An LFSR has two important properties:
- The so-called *seed* is the initial value of the shift register
- The so-called *generator polynomial* defines which bits are XOR-ed in the feedback function

To illustrate the above, consider the following example:

![LFSR Example](.mdata/lfsr.svg)

Here the seed is 1100.
The polynomial is *x⁴+x²+x¹*, because we XOR the fourth, second and first value of the LFSR (counting starts at 1 and from the right).
For our case, such a generator polynomial has purely binary coefficients, i.e. either a power of *x* is part of the polynomial, or it is not.
Thus, we can encode the polynomial using a simple bit-string, where a 1 at index *i* indicates that the polynomial contains *x^i*.
For the example above, this yields 1011.


## Task
Your task is to implement a simple pseudo-random number generator (PRNG) using an LFSR.
To do this, write a hardware module called `lfsr`, having the following interface:

```vhdl
component lfsr is
	generic (
		LFSR_WIDTH : integer;
		POLYNOMIAL : std_logic_vector
	);
	port (
		clk : in std_logic;
		res_n : in std_logic;
		load_seed : in std_logic;
		seed : in std_logic_vector(LFSR_WIDTH-1 downto 0);
		prdata : out std_logic
	);
end component;
```

`clk` and `res_n` are a clock and an active-low reset signal.
The generic `LFSR_WIDTH` defines the width of the LFSR, i.e., the number of stages in the shift register.
`POLYNOMIAL` is the LFSR's generator polynomial.
Consider the example in the above intro to see how this generic encodes a polynomial and how it shall be used.
When `load_seed` is high, the module loads the value applied at `seed` into its internal shift register.
This allows initializing the LFSR.
Whenever `load_seed` is not high in a cycle, `prdata` outputs a new pseudo random bit.

## Testbench
Write a testbench `lfsr_tb` (use the template in [`tb/lfsr_tb.vhd`](tb/lfsr_tb.vhd)) that monitors the output sequence of `prdata`.
Determine the period of the output sequence generated at `prdata` for a particular generator polynomial and different `seed` values and to record the maximum and minimum observed sequence period.

To do this, your testbench should attach a shift register (similar to the one in `lfsr`, but without feedback) to the `prdata` output of the `lfsr`.
Since we want to monitor the sequences of LFSR values, this testbench shift register must also be `LFSR_WIDTH` bits wide.
To measure the period of your LFSR, take a snapshot of testbench sequence once it is completely filled with data from `prdata`.
Now, count the number of clock cycles until you observe this value again and you will have obtained the LFSR's period for the loaded seed.

Make sure that your testbench checks all seeds in a range automatically during a single run (i.e., you must not enter each seed manually before simulation).
For each checked seed, your testbench should report the seed and the period.
After all seeds got checked, the minimum and maximum period are reported as well.
Hence, a typical simulation output should look similar to the following:

```
seed: [SEED0], period: [PERIOD0]
seed: [SEED1], period: [PERIOD1]
[...]
seed: [SEEDn], period: [PERIODn]
min period: [MIN_PERIOD], max period: [MAX_PERIOD]
```

Below you can find some simulation scenarios (i.e., polynomials and ranges of seeds), allowing you to check if your implementation and testbench are correct.

| Polynomial | Start Seed | End Seed | Min Period | Max Period |
|------------|------------|----------|------------|------------|
| MAX_POLY_8 |     1      |   255    |    255     |     255    |
|   POLY_8   |    13      |   125    |     7      |     105    |
|MAX_POLY_16 |    343     |   379    |   65535    |    65535   |
|  POLY_16   |    678     |   750    |    2047    |    63457   |

The first column of the table are refers to constants defined in the testbench.

**Hints**:
- Since a `LFSR_WIDTH` wide LFSR can only take on *2^LFSR_WIDTH-2* (ignoring 0) values, its maximum period must be *2^LFSR_WIDTH-1*.
- To make sure that your testbench's shift register is completely filled with data from `prdata` when making the snapshot, wait for sufficiently many clock cycles after a seed was loaded (the exact starting value of the sequence does not matter as long as the testbench shift register is filled completely).
- You can generate the simulation output either using `report` statements, or using the `textio` package.
- Note that a `seed` of 0 will, due to the structure of an LFSR, lead to the LFSR always holding 0. Thus, there is no meaningful period of the output sequence.

## Hardware Test
The main part of this task is really implementing the module and a proper testbench.
However, you should of course make sure that your hardware module is actually synthesizable.
In order to do so perform synthesis using Quartus after you are done with the testbench part of this task.
A respective architecture, instantiating your module, is already provided in [top_arch_lfsr.vhd](top_arch_lfsr.vhd).
Make sure that Quartus does only report allowed warnings.
You are not required to load the generated bitstream to the board.
However, if you want you can connect `prdata` to an LED run it on the board and run the design.
Think about what you would expect to see before loading the design onto the board!
