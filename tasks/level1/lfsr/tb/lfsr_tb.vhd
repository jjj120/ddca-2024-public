library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.lfsr_pkg.all;

entity lfsr_tb is
end entity;

architecture tb of lfsr_tb is
    signal clk, res_n : std_logic := '0';

    constant MAX_POLY_8  : std_logic_vector(7 downto 0)  := "10111000";
    constant POLY_8      : std_logic_vector(7 downto 0)  := "10100100";
    constant MAX_POLY_16 : std_logic_vector(15 downto 0) := "1101000000001000";
    constant POLY_16     : std_logic_vector(15 downto 0) := "1101001100001000";

    constant CLOCK_PERIOD : time := 2 ns;
    signal finished : std_logic := '0';

    -- Change as required
    constant LFSR_WIDTH : integer                                   := 8;
    constant POLYNOMIAL : std_logic_vector(LFSR_WIDTH - 1 downto 0) := MAX_POLY_8;

    signal load_seed : std_logic;
    signal seed      : std_logic_vector(LFSR_WIDTH - 1 downto 0) := (others => '0');
    signal prdata    : std_logic;
begin
    stimulus: process is

        procedure get_single_period(min_period, max_period, curr_period : inout natural) is
            variable seed_to_find : std_logic_vector(LFSR_WIDTH - 1 downto 0) := (others => '0');
            variable current_seed : std_logic_vector(LFSR_WIDTH - 1 downto 0) := (others => '0');
        begin
            for i in 0 to 2 * LFSR_WIDTH loop --empty seed out of shift register, then it has to be in a loop
                wait until rising_edge(clk);
            end loop;

            for i in LFSR_WIDTH - 1 downto 0 loop --fill up seeds
                seed_to_find(i) := prdata;
                wait until rising_edge(clk);
            end loop;

            current_seed(LFSR_WIDTH - 1 downto 1) := seed_to_find(LFSR_WIDTH - 2 downto 0);
            current_seed(0) := prdata;
            wait until rising_edge(clk);
            curr_period := 1;

            report to_string(unsigned(current_seed)) & " - " & to_string(unsigned(seed_to_find));

            while current_seed /= seed_to_find loop
                current_seed(LFSR_WIDTH - 1 downto 1) := current_seed(LFSR_WIDTH - 2 downto 0);
                current_seed(0) := prdata;
                curr_period := curr_period + 1;
                wait until rising_edge(clk);
            end loop;

            if curr_period < min_period then
                min_period := curr_period;
            end if;
            if curr_period > max_period then
                max_period := curr_period;
            end if;
        end procedure;

        procedure get_periods(min_seed, max_seed : natural) is
            variable min_period  : integer := integer'high;
            variable max_period  : integer := 0;
            variable curr_period : integer := 0;
        begin
            for i in min_seed to max_seed loop
                seed <= std_logic_vector(to_unsigned(i, LFSR_WIDTH));
                load_seed <= '1';
                wait until rising_edge(clk);
                load_seed <= '0';
                wait until rising_edge(clk);

                curr_period := 0;
                get_single_period(min_period, max_period, curr_period);

                report "seed: " & to_string(i) & ", period: " & to_string(curr_period);
            end loop;

            report "min period: " & to_string(min_period) & ", max period: " & to_string(max_period);
        end procedure;

    begin
        report "start simulation";

        res_n <= '1';
        wait until rising_edge(clk);

        if POLYNOMIAL = MAX_POLY_8 then
            get_periods(1, 255);
        elsif POLYNOMIAL = POLY_8 then
            get_periods(13, 125);
        elsif POLYNOMIAL = MAX_POLY_16 then
            get_periods(343, 379);
        elsif POLYNOMIAL = POLY_16 then
            get_periods(678, 750);
        else
            report "NO SOLUTION DETECTED!" severity failure;
        end if;

        finished <= '1';
        report "finished simulation";
        wait;
    end process;

    uut: lfsr
        generic map (
            LFSR_WIDTH => LFSR_WIDTH,
            POLYNOMIAL => POLYNOMIAL
        )
        port map (
            clk       => clk,
            res_n     => res_n,
            load_seed => load_seed,
            seed      => seed,
            prdata    => prdata
        );

    clk_gen: process is
    begin
        if finished = '1' then
            wait;
        end if;

        clk <= '1';
        wait for CLOCK_PERIOD / 2;
        clk <= '0';
        wait for CLOCK_PERIOD / 2;
    end process;

end architecture;
