use work.lfsr_pkg.all;

architecture top_arch_lfsr of top is
  constant MAX_POLY_8: std_logic_vector(7 downto 0) := "10111000";
begin

  lfsr_inst : lfsr
  generic map(
    LFSR_WIDTH => 8,
    POLYNOMIAL => MAX_POLY_8
  )
  port map(
    clk => clk,
    res_n => keys(0),
    load_seed => keys(3),
    seed => switches(7 downto 0),
    prdata => ledr(0)
  );

end architecture;
