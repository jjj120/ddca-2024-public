library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.math_real.all;
    use work.mergesort_pkg.all;

entity mergesort is
    generic (
        ARRAY_SIZE : natural := 1
    );
    port (
        input_array  : in  integer_vector(ARRAY_SIZE - 1 downto 0);
        output_array : out integer_vector(ARRAY_SIZE - 1 downto 0)
    );
begin
    assert ARRAY_SIZE = natural(2 **(ceil(log2(real(ARRAY_SIZE))))) report "ARRAY_SIZE must be a power of 2!" severity failure;
end entity;

architecture recursive of mergesort is
    signal part1 : integer_vector(ARRAY_SIZE / 2 - 1 downto 0);
    signal part2 : integer_vector(ARRAY_SIZE / 2 - 1 downto 0);

begin

    main_gen: if ARRAY_SIZE = 1 generate
        output_array <= input_array;
    elsif ARRAY_SIZE > 1 generate
        -- split and recursion
        mergesort_1: entity mergesort(recursive)
            generic map (ARRAY_SIZE / 2)
            port map (
                input_array  => input_array(ARRAY_SIZE / 2 - 1 downto 0),
                output_array => part1
            );

        mergesort_2: entity mergesort(recursive)
            generic map (ARRAY_SIZE / 2)
            port map (
                input_array  => input_array(ARRAY_SIZE - 1 downto ARRAY_SIZE / 2),
                output_array => part2
            );

        -- merge output together

        merge_proc: process (part1, part2) is
            variable i_1, i_2 : integer;

            procedure insert_from_1(i_main : integer) is
            begin
                output_array(i_main) <= part1(i_1);
                i_1 := i_1 + 1;
            end procedure;

            procedure insert_from_2(i_main : integer) is
            begin
                output_array(i_main) <= part2(i_2);
                i_2 := i_2 + 1;
            end procedure;
        begin
            i_1 := 0;
            i_2 := 0;

            merge_gen: for i_main in 0 to ARRAY_SIZE - 1 loop
                assert i_1 + i_2 = i_main report "Array indices do not add up!";

                part2_fin: if i_2 >= ARRAY_SIZE / 2 then
                    insert_from_1(i_main);
                    next merge_gen;
                end if;

                part1_fin: if i_1 >= ARRAY_SIZE / 2 then
                    insert_from_2(i_main);
                    next merge_gen;
                end if;

                if part1(i_1) < part2(i_2) then
                    insert_from_1(i_main);
                else
                    insert_from_2(i_main);
                end if;
            end loop;
        end process;
    else generate
        -- Arraysize too small, illegal!
        assert 1 = 0 report "ARRAY_SIZE < 1 !" severity failure;
    end generate;
end architecture;
