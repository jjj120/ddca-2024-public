library ieee;
use ieee.std_logic_1164.all;

package mergesort_pkg is

	component mergesort is
		generic (
			ARRAY_SIZE : natural := 2
		);
		port(
			input_array  : in integer_vector(ARRAY_SIZE-1 downto 0);
			output_array : out integer_vector(ARRAY_SIZE-1 downto 0)
		);
	end component;
	
end package;
