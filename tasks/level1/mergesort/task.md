
# Hardware Mergesort
**Points:** 2 ` | ` **Keywords:** combinational, structural modeling, generics, random testing

### Recursive Instantiation

Your task is to create an integer sorting component.
In particular you are supposed to create a module named `mergesort` that recursively instantiates itself and to effectively implement the [merge sort](https://en.wikipedia.org/wiki/Merge_sort) algorithm.

You have to solve this task using only a single entity with a single architecture.
Place them in the file [`src/mergesort.vhd`](src/mergesort.vhd)
The interface of the `mergesort` component is shown below.

```vhdl
component mergesort is
	generic (
		ARRAY_SIZE : natural := 2
	);
	port(
		input_array  : in integer_vector(ARRAY_SIZE-1 downto 0);
		output_array : out integer_vector(ARRAY_SIZE-1 downto 0)
	);
end component;
```

The datatype `integer_vector` is a standard-type and simply defines an array of integers (similar to how a `std_logic_vector` is an array of `std_logic` elements).
Make sure that the entity enforces `ARRAY_SIZE` to be a power of 2.

Note that this task is for exercise purposes only since you should be already familiar with the merge sort.
In real world sorting circuits you will most likely encounter [sorting networks](https://en.wikipedia.org/wiki/Sorting_network).

## Testbench

Create a testbench for the `mergesort` entity and place it in the file [`tb/mergesort_tb.vhd`](tb/mergesort_tb.vhd).
Check out the `uniform()` procedure of the `math_real` package (already used in provided testbench file) to create pseudo-random numbers.
Fill the `input_array` of the `mergesort` entity with those pseudo-random numbers and show that your entity is able to sort the input.

Make sure your testbench checks whether the output array is sorted or not.
Report the sorted output array.

## Hardware Test

Add an instances of the `mergesort` entity to the top-level architecture in [`top_arch_mergesort.vhd`](top_arch_mergesort.vhd).

- Set `ARRAY_SIZE` to 4
- Use `switches(15 downto 0)` to input 4 4-bit values 
  - Use `switches((i+1)*4-1 downto 4*i)` as the value for `input_array(i)`
  - Note that you will have to perform a type conversion
- Make `hex7` to `hex4` display the 4 unsorted values as hex values (use the `to_segs` function of the [`util`](../../../lib/util/doc.md) package)
- Make `hex3` to `hex0` display the sorted output values

Add the required files to your Quartus project and configure the top-level configuration accordingly.
Compile the design with Quartus and download it to the FPGA. 
Check if the `mergesort` works as intended.
