library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.math_real.all;
    use work.mergesort_pkg.all;

entity mergesort_tb is

end entity;

architecture bench of mergesort_tb is
    constant vector_size : integer := 32;
    constant max_num     : integer := 4096;
    signal input_vector  : integer_vector(vector_size - 1 downto 0);
    signal output_vector : integer_vector(vector_size - 1 downto 0);

begin
    -- Instantiate the unit under test
    uut: entity work.mergesort(recursive) generic map (vector_size) port map (
        input_array  => input_vector,
        output_array => output_vector
    );

    -- Stimulus process

    stimulus: process
        variable seed1, seed2 : integer := 213428;
        impure function generate_random_integer return integer is
            -- max_num is exclusive!
            variable random_variable : real;
        begin
            uniform(seed1, seed2, random_variable);
            return natural(floor(random_variable * real(max_num)));
        end function;

    begin
        report "simulation start";
        -- Apply test stimuli
        for i in vector_size - 1 downto 0 loop
            input_vector(i) <= generate_random_integer;
        end loop;

        -- report to_string(input_vector);
        wait for 10 ns;

        for i in vector_size - 1 downto 1 loop
            assert output_vector(i) >= output_vector(i - 1) report "Array not sorted!" severity error;
        end loop;

        -- report to_string(output_vector);
        report "simulation end";
        -- End simulation
        wait;
    end process;
end architecture;

