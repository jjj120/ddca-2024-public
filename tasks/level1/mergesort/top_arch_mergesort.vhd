use work.util_pkg.to_segs;
use ieee.numeric_std.all;

architecture top_arch_mergesort of top is
	 constant ARRAY_SIZE : integer := 4;
	 signal input_vector : integer_vector(ARRAY_SIZE - 1 downto 0);
	 signal output_vector : integer_vector(ARRAY_SIZE - 1 downto 0);
begin
	 mergesort_inst: entity work.mergesort(recursive) 
	 generic map (ARRAY_SIZE) 
	 port map (
        input_array  => input_vector,
        output_array => output_vector
    );
	 
	 gen_loop : for i in ARRAY_SIZE - 1 downto 0 generate
	     input_vector(i) <= to_integer(unsigned(switches((i+1)*4-1 downto 4*i)));
	 end generate;
	 
	 ledr(15 downto 0) <= switches(15 downto 0);
	 
	 hex7 <= to_segs(switches(15 downto 12));
	 hex6 <= to_segs(switches(11 downto 8));
	 hex5 <= to_segs(switches(7 downto 4));
	 hex4 <= to_segs(switches(3 downto 0));
	 
	 hex3 <= to_segs(std_logic_vector(to_unsigned(output_vector(3), 4)));
	 hex2 <= to_segs(std_logic_vector(to_unsigned(output_vector(2), 4)));
	 hex1 <= to_segs(std_logic_vector(to_unsigned(output_vector(1), 4)));
	 hex0 <= to_segs(std_logic_vector(to_unsigned(output_vector(0), 4)));
	 
end architecture;
