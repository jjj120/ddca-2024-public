library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity pwm_signal_generator is
    generic (
        COUNTER_WIDTH : integer := 8
    );
    port (
        clk     : in  std_logic;                                    -- clock
        res_n   : in  std_logic;                                    -- reset (high active, async)
        en      : in  std_logic;                                    -- enable (high active, sync)
        value   : in  std_logic_vector(COUNTER_WIDTH - 1 downto 0); -- value to print to pwm
        pwm_out : out std_logic                                     -- pwm output
    );
end entity;

architecture arch of pwm_signal_generator is
begin

    process (clk) is
        variable counter : unsigned(COUNTER_WIDTH - 1 downto 0) := (others => '0');
        variable running : boolean                              := false;
    begin
        if rising_edge(clk) then
            if res_n then -- reset
                counter := (others => '0');
                running := false;
                pwm_out <= '0';
            else
                pwm_out <= '0';
                if counter = 0 then
                    running := (en = '1');
                end if;

                if running then
                    if unsigned(counter) >= unsigned(value) then
                        pwm_out <= '1';
                    else
                        pwm_out <= '0';
                    end if;
                end if;
                counter := counter + 1;
            end if;
        end if;
    end process;
end architecture;
