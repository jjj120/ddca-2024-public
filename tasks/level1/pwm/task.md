
# PWM Signal Generator
**Points:** 2 ` | ` **Keywords:** registers

Your task is to implement a simple [PWM](https://en.wikipedia.org/wiki/Pulse-width_modulation) signal generator with the following interface:

```vhdl
entity pwm_signal_generator is
	generic (
		COUNTER_WIDTH : integer := 8
	);
	port (
		clk        : in std_logic;
		res_n      : in std_logic;
		en         : in std_logic;
		value      : in std_logic_vector(COUNTER_WIDTH-1 downto 0);
		pwm_out    : out std_logic
	);
end entity;
```

The core contains an unsigned `COUNTER_WIDTH`-bit wide internal counter register that it initially set to zero (reset value).
Once `en` is set to one it is incremented by one in every clock cycle.
The output `pwm_out` is set to one if this counter is grater or equal to the `value` input.
When the maximum value is reached, the counter wraps around to zero, which means that `pwm_out` is also reset to zero again (unless of course `value` is zero).
This completes one period of the PWM signal.

The `en` signal must only be checked if the counter is zero!
Once the counter is running, it must complete a full period.
Hence, if `en` is only asserted for, e.g., one clock cycle, the `pwm_signal_generator` should only complete a single period and then stop.
If `en` is zero `pwm_out` must be zero (even if `value` is zero).


## Testbench
Create a testbench for the `pwm_signal_generator` entity.
The file `tb/pwm_signal_generator_tb.vhd` already contains a template.
Use the value 8 for `COUNTER_WIDTH`.
Generate a 1 MHz clock signal in the testbench and connect it to the `clk` input.


The testbench contains a procedure with the name `check_pwm_signal` that takes two parameters, which specify the time the PWM signal shall be low and high, respectively.
Implement this procedure and use assertions to check whether the times you measured match the arguments.
You may spend an arbitrary number of PWM periods in this procedure to determine these values
Note that you can use the global predefined function `now` the get the current simulation time:

```vhdl
[...]
procedure p is
	variable t : time;
begin
	t := now;
[...]
```

Loop through every possible value for the `value` signal (starting with 1), setup the `pwm_signal_generator` to generate an appropriate PWM signal and then call the `check_pwm_signal` procedure to check if it is correct.
**Hint**: Note that the `value` input essentially specifies how many clock cycles the `pwm_out` signal must zero within a period.

Also include a testcase that checks if the core reacts correctly when `en` is deasserted while in the middle of a period.

## Hardware Test

PWM signals can be used to control the brightness of LEDs.
Add an instance of your `pwm_signal_generator` to the top-level architecture [top_arch_pwm](top_arch_pwm.vhd).
Set `COUNTER_WIDTH` to 8 and connect the ports as follows:

 * Connect the top-level `clk` signal to the `clk` input
 * Connect `keys(0)` to `res_n`
 * Connect `switches(17)` to `en`
 * Connect `switches(7 downto 0)` to `value`
 * Connect `pwm_out` to `ledg(0)`

Add the required files to your Quartus project and configure the top-level configuration accordingly.
Compile the design with Quartus and download it to the FPGA. 

You should be able to control the brightness of the LED with the switches.
If you run this example in the Remote Lab, you have to look at the camera stream.

