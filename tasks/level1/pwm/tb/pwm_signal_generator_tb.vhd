library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity pwm_signal_generator_tb is
end entity;

architecture testbench of pwm_signal_generator_tb is
    constant COUNTER_WIDTH     : natural := 8;
    constant clock_period_time : time    := 1 ns;
    signal clk, res_n, en : std_logic                                    := '0';
    signal value          : std_logic_vector(COUNTER_WIDTH - 1 downto 0) := (others => '0');
    signal pwm_out        : std_logic;
    signal finished       : boolean                                      := false;
begin

    uut: entity work.pwm_signal_generator
        generic map (
            COUNTER_WIDTH => COUNTER_WIDTH
        )
        port map (
            clk     => clk,
            res_n   => res_n,
            en      => en,
            value   => value,
            pwm_out => pwm_out
        );

    clk_process: process
    begin
        if finished then
            wait;
        end if;

        -- generate a 1 MHz clock signal
        clk <= '1';
        wait for clock_period_time / 2;
        clk <= '0';
        wait for clock_period_time / 2;
    end process;

    stimulus_process: process
        procedure check_pwm_signal(low_time, high_time : time) is
            variable begin_time, switch_time, end_time : time;
        begin
            wait until falling_edge(pwm_out);
            begin_time := now;
            wait until rising_edge(pwm_out);
            switch_time := now;
            wait until falling_edge(pwm_out);
            end_time := now;

            assert switch_time - begin_time = low_time
                report "Low time not right: is " & to_string(switch_time - begin_time) & " should be " & to_string(low_time);

            assert end_time - switch_time = high_time
                report "High time not right: is " & to_string(end_time - switch_time) & " should be " & to_string(high_time);

            -- test enable deassertion in cycle
            wait until falling_edge(pwm_out);
            begin_time := now;
            wait for clock_period_time;
            en <= '0';

            wait until rising_edge(pwm_out);
            switch_time := now;
            wait until falling_edge(pwm_out);
            end_time := now;

            assert switch_time - begin_time = low_time
                report "Low time not right when deasserting early: is " & to_string(switch_time - begin_time) & " should be " & to_string(low_time);

            assert end_time - switch_time = high_time
                report "High time not right when deasserting early: is " & to_string(end_time - switch_time) & " should be " & to_string(high_time);
        end procedure;

        procedure check_value(value_num : std_logic_vector(COUNTER_WIDTH - 1 downto 0)) is
            constant counter_max : std_logic_vector(COUNTER_WIDTH - 1 downto 0) := (others => '1');
            variable low_time_cycles, high_time_cycles : integer;
        begin
            low_time_cycles := to_integer(unsigned(value_num));
            high_time_cycles := to_integer(unsigned(counter_max) + 1 - unsigned(value_num));

            value <= value_num;
            res_n <= '1'; -- reset pwm module
            en <= '0';
            wait for clock_period_time;

            res_n <= '0';
            wait for clock_period_time;

            en <= '1';

            check_pwm_signal(low_time_cycles * clock_period_time, high_time_cycles * clock_period_time);
        end procedure;
    begin

        report "Start testing";

        for i in 1 to 2 ** COUNTER_WIDTH - 1 loop
            check_value(std_logic_vector(to_unsigned(i, COUNTER_WIDTH)));
        end loop;

        finished <= true;

        report "Finished testing";
        wait;
    end process;

end architecture;
