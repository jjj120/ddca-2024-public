architecture top_arch_pwm of top is
    constant COUNTER_WIDTH : integer := 8;
begin

    pwm: entity work.pwm_signal_generator
        generic map (
            COUNTER_WIDTH => COUNTER_WIDTH
        )
        port map (
            clk     => clk,
            res_n   => not keys(3),
            en      => switches(17),
            value   => switches(7 downto 0),
            pwm_out => ledg(0)
        );
	 
	 ledr(17) <= switches(17);
	 ledr(7 downto 0) <= switches(7 downto 0);
	 ledg(6) <= not keys(3);

    hex0 <= (others => '1');
    hex1 <= (others => '1');
    hex2 <= (others => '1');
    hex3 <= (others => '1');
    hex4 <= (others => '1');
    hex5 <= (others => '1');
    hex6 <= (others => '1');
    hex7 <= (others => '1');

end architecture;
