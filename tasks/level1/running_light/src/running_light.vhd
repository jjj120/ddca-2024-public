library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.running_light_pkg.all;

entity running_light is
    generic (
        STEP_TIME  : time    := 1 sec;
        LEDS_WIDTH : integer := 18
    );
    port (
        clk      : in  std_logic;
        res_n    : in  std_logic;
        leds     : out std_logic_vector(LEDS_WIDTH - 1 downto 0);
        key      : in  std_logic;
        switches : in  std_logic_vector(1 downto 0)
    );
end entity;

architecture arch of running_light is
    subtype pattern_t is natural range LEDS_WIDTH - 1 downto 0;
    type fsm_state_t is record
        curr_leds : pattern_t;
        counter   : natural;
    end record;
    signal state, state_next : fsm_state_t := (others => 0);
    constant wait_counter_max : integer := STEP_TIME / 20 ns;
	
	signal pattern1 : std_logic_vector(leds'range) := (others => '0');
	signal pattern2 : std_logic_vector(leds'range) := (others => '0');
begin

    -- Add further registers as needed
    registers: process (clk, res_n) is
    begin
        if (res_n = '0') then
            state <= (others => 0);
        elsif rising_edge(clk) then
            state <= state_next;
        end if;
    end process;

    -- Implement next state and output logic for your pattern

    comb_logic: process (all) is
    begin
        state_next <= state;

        if state.counter >= wait_counter_max then
            state_next.counter <= 0;
            if state.curr_leds + 1 >= LEDS_WIDTH then
                state_next.curr_leds <= 0;
            else
                state_next.curr_leds <= state.curr_leds + 1;
            end if;
        else
            state_next.counter <= state.counter + 1;
            state_next.curr_leds <= state.curr_leds;
        end if;
    end process;
	

    -- pattern1 <= (state.curr_leds => '1', others => '0');
    -- leds <= (0 to state.curr_leds => '1', others => '0');
	
	pattern1 <= std_logic_vector(shift_left(to_unsigned(1, leds'length), state.curr_leds));
	pattern2 <= std_logic_vector(shift_left(to_unsigned(1, leds'length), state.curr_leds + 1) - 1);
	
	leds <= pattern1 when switches(0) else pattern2;
end architecture;
