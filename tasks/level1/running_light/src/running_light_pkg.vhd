library ieee;
use ieee.std_logic_1164.all;

package running_light_pkg is
  component running_light is
    generic (
      STEP_TIME : time := 1 sec;
      LEDS_WIDTH : integer := 18
    );
    port (
      clk      : in std_logic;
      res_n    : in std_logic;
      leds     : out std_logic_vector(LEDS_WIDTH-1 downto 0);
      key      : in std_logic;
      switches : in std_logic_vector(1 downto 0)
    );
  end component;
end package;