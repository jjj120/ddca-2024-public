
# Running Light
**Points:** 3 ` | ` **Keywords:** FSM

## Task
Your task is to implement a `running_light` hardware module (a template is provided in [running_light.vhd](src/running_light.vhd)) that uses a simple state machine to drive a series of LEDs using some pattern.
The module shall have the following interface:

```vhdl
component running_light is
  generic (
    STEP_TIME : time := 1 sec;
    LEDS_WIDTH : integer := 18
  );
  port (
    clk      : in std_logic;
    res_n    : in std_logic;
    leds     : out std_logic_vector(LEDS_WIDTH-1 downto 0);
    key      : in std_logic;
    switches : in std_logic_vector(1 downto 0)
  );
end component;
```

`clk` and `res_n` are a clock and an active-low reset signal.
`leds` is where the module provides the current step of the pattern for the `LED_WIDTH` LEDs it controls.
`key` and `switches` are optional inputs.
They will be used for some bonus modifications to the basic module.
Unless stated otherwise, make sure that each pattern step is visible for `STEP_TIME`.
You can assume that the frequency of `clk` is 50MHz.
After a final pattern step is reached, the pattern shall start again from step 1.
Your controller shall set the LEDs to state 1 of the pattern during reset.

**Hints**:
- You can let a state machine wait for some time using a counter and comparing its value with a threshold
- You can actually do some compile-time calculations with VHDL variables/constants of type `time`. Use this, together with `STEP_TIME` and the known clock period, to calculate a value for the counter threshold (the one mentioned in the previous hint)
- Use the provided `fsm_state_t` type for the state machine's state (of course, you need to add further states)


### Patterns
The following describes some LED patterns.
Start by choosing one that you like (you only have to present one pattern to the tutor in oder to complete this task).
However, you might want to implement more than just one of these patterns in order to practice, as you will then have the possibility to compare your pattern implementations and analyze what changes you needed to make in order to adapt your state machine to another pattern.
Note that all patterns are shown for a `LEDS_WIDTH` of 8.
Make sure your module works for any value of `LEDS_WIDTH`, continuing the pattern in the obvious manner.

The first pattern lights each LED once in a consecutive manner:

![LED Pattern A](.mdata/pattern_a.svg)

The second pattern shows a filling bar of LEDs:

![LED Pattern B](.mdata/pattern_b.svg)


For the third pattern even and odd LEDs are lighted in a consecutive and alternating manner.
I.e., initially the first even and odd `leds(1,0)` are switched on.
Then the odd LED advances: instead of `leds(1)`, `leds(3)` will now be lighted.
Next, the even LED advances: instead of `leds(0)`, `leds(2)` is switched on.
The pattern continues in this manner.
For this pattern, you can assume that `LEDS_WIDTH` only takes on even values (also make sure your testbench adheres to this assumption).
Please note that this pattern is the most challenging of the given ones.
We would therefore advise you to start with one of the other two.

![LED Pattern C](.mdata/pattern_c.svg)

### Bonus
If you want to practice a bit more and make this task a bit more difficult, you can implement the following modifications:
- Instead of running continuously, let a pattern run once whenever `key` is pressed. While your LED controller waits for a key press, it should output step 1 of the pattern
- If `switches(0)` is set in step 1 (i.e., when the pattern starts again), the pattern is shown from left to right. If `switches(0)` is not set, the pattern is shown from right to left (as in the above images)
- If `switches(1)` is set during step 1 (i.e., when the pattern starts again), the final step of the pattern is shown for two times `STEP_TIME`. If `switches(1)` is not set during step 0, the final step is only shown for `STEP_TIME`.

## Testbench
Create a testbench for `running_light` and place it in  [tb/running_light_tb.vhd](tb/running_light_tb.vhd).
Create a waveform configuration for your simulation, such that one can easily observe the pattern steps of the LEDs in the waveform viewer.
Also add the internal state register of you FSM to the viewer.

**Hint**: Make sure to set `STEP_TIME` to a sufficiently small time for your simulation to terminate fast (i.e., just a few clock periods).
Think about how many clock cycles you would need to simulate if `STEP_TIME` was set to one second for a 50MHz clock.

## Hardware test

Once you have implemented and tested your LED controller, instantiate it in the top-level architecture [`top_arch_running_light`](top_arch_running_light.vhd).
Connect `clk` to `clk`, `res_n` to `keys(0)`, `leds` to `ledr`, `switches` to `switches(1 downto 0)` and `key` to `keys(3)`.
Finally, compile your design in Quartus and test in on an actual board.
Do not forget to reset your design (`keys(0)`, right-most button)!
