library ieee;
    use ieee.std_logic_1164.all;

    use work.running_light_pkg.all;

entity running_light_tb is
end entity;

architecture arch of running_light_tb is
    constant STEP_TIME         : time    := 100 ns;
    constant LEDS_WIDTH        : integer := 18;
    constant CLOCK_PERIOD_TIME : time    := 20 ns;

    signal clk, res_n : std_logic;
    signal leds       : std_logic_vector(LEDS_WIDTH - 1 downto 0);
    signal key        : std_logic                    := '0';
    signal switches   : std_logic_vector(1 downto 0) := (others => '0');
    signal finished   : std_logic                    := '0';
    -- Add signals as required
begin

    stimulus: process is
    begin
        -- Reset your module and apply stimuli
        res_n <= '0';
        wait until rising_edge(clk);
        res_n <= '1';

        wait for LEDS_WIDTH * STEP_TIME * 2;

        finished <= '1';
        wait;
    end process;

    -- Instantiate your module
    uut: entity work.running_light
        generic map (
            STEP_TIME  => STEP_TIME,
            LEDS_WIDTH => LEDS_WIDTH
        )
        port map (
            clk      => clk,
            res_n    => res_n,
            leds     => leds,
            key      => key,
            switches => switches
        );

    clk_gen: process is
    begin
        if finished = '1' then
            wait;
        end if;
        -- Generate a 50 MHz clock
        clk <= '1';
        wait for CLOCK_PERIOD_TIME / 2;
        clk <= '0';
        wait for CLOCK_PERIOD_TIME / 2;
    end process;

end architecture;
