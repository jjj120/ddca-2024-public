use work.running_light_pkg.all;

architecture top_arch_running_light of top is
    constant STEP_TIME  : time                         := 500 ms;
    constant LEDS_WIDTH : integer                      := 18;
    constant off        : std_logic_vector(6 downto 0) := (others => '1');
begin

    uut: entity work.running_light
        generic map (
            STEP_TIME  => STEP_TIME,
            LEDS_WIDTH => LEDS_WIDTH
        )
        port map (
            clk      => clk,
            res_n    => keys(0),
            leds     => ledr,
            key      => keys(3),
            switches => switches(1 downto 0)
        );

    hex0 <= off;
    hex1 <= off;
    hex2 <= off;
    hex3 <= off;
    hex4 <= off;
    hex5 <= off;
    hex6 <= off;
    hex7 <= off;

end architecture;
