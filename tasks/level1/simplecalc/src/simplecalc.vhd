
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity simplecalc is
    port (
        clk      : in  std_logic;                    -- clock
        res_n    : in  std_logic;                    -- low active reset

        in_data  : in  std_logic_vector(7 downto 0); -- data input
        save1    : in  std_logic;                    -- toggle to save to op1
        save2    : in  std_logic;                    -- toggle to save to op2

        sub      : in  std_logic;                    -- toggle to subtract

        operand1 : out std_logic_vector(7 downto 0); -- save/output 1
        operand2 : out std_logic_vector(7 downto 0); -- save/output 2
        result   : out std_logic_vector(7 downto 0)  -- result of calculation
    );
end entity;

architecture arch of simplecalc is
    signal last_op1   : std_logic_vector(7 downto 0) := (others => '0');
    signal last_op2   : std_logic_vector(7 downto 0) := (others => '0');
    signal prev_save1 : std_logic;
    signal prev_save2 : std_logic;
begin

    main_proc: process (clk)
    begin
        if rising_edge(clk) then
            if prev_save1 and not save1 then
                last_op1 <= in_data;
            else
                last_op1 <= last_op1;
            end if;

            if prev_save2 and not save2 then
                last_op2 <= in_data;
            else
                last_op2 <= last_op2;
            end if;

            prev_save1 <= save1;
            prev_save2 <= save2;

            if not res_n then
                prev_save1 <= '0';
                prev_save2 <= '0';
                last_op1 <= (others => '0');
                last_op2 <= (others => '0');
            end if;
        end if;
    end process;

    result <= std_logic_vector(unsigned(last_op1) + unsigned(last_op2)) when not sub else
              std_logic_vector(unsigned(last_op1) - unsigned(last_op2));

    operand1 <= last_op1;
    operand2 <= last_op2;
end architecture;
