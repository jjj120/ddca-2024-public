library ieee;
use ieee.std_logic_1164.all;

package task_simplecalc_pkg is
	component simplecalc
		port(
			clk    : in std_logic;
			res_n  : in std_logic;

			in_data : in std_logic_vector(7 downto 0);
			save1   : in std_logic;
			save2   : in std_logic;

			sub     : in std_logic;
			
			operand1 : out std_logic_vector(7 downto 0);
			operand2 : out std_logic_vector(7 downto 0);
			result   : out std_logic_vector(7 downto 0)
		);
	end component;
end package;
