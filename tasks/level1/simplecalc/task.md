
# Simple Calculator
**Points:** 2 ` | ` **Keywords:** registers, arithmetic operations

Your task is to implement a simple calculator with the following interface:

```vhdl
entity simplecalc is
	port(
		clk    : in std_logic;
		res_n  : in std_logic;

		in_data : in std_logic_vector(7 downto 0);
		save1   : in std_logic;
		save2   : in std_logic;

		sub     : in std_logic;

		operand1 : out std_logic_vector(7 downto 0);
		operand2 : out std_logic_vector(7 downto 0);
		result  : out std_logic_vector(7 downto 0)
	);
end entity;
```

The calculator has two internal registers named `operand1` and `operand2`, which can be read via the corresponding outputs of the entity.
Those registers are reset to zero using the active-low reset `res_n`.
The calculator constantly outputs the sum (`sub`=0) or the difference (`sub`=1) of these two operands at the `result` output.

To get new data data into the operand registers, falling transitions have to be applied to the inputs `save1` or `save2`.
When the calculator detects a falling transition on `save1` it stores the current value at `in_data` to the `operand1` register.
With a falling transition on `save2`, `in_data` is written to `operand2`.

The inputs `save1` and `save2` are intended to be connected to (active-low) buttons, a falling input transition would, hence, correspond to a button press.
A button press can be detected by observing a signal change from `1` to `0` between two clock cycles.
Hence, you will need registers for that purpose.
Be sure to also correctly initialize those values during reset.


## Testbench

Create a testbench for the `simplecalc` entity and place it in the file `tb/simplecalc_tb.vhd`.
Let the circuit register a few different input values and check the entity's output for correctness using assertions.
Your testbench shall perform at least four different calculations.

## Hardware Test

Add an instances of the `simplecalc` entity to the top-level architecture in `top_arch_simplecalc.vhd`.

- Connect the clock signal to `clk`
- Connect `keys(0)` to `res_n`
- Connect `switches(7 downto 0)` to `input_data`
- Connent `switches(17)` to `sub`
- Connect `keys(1)` to `save1`
- Connect `keys(2)` to `save2`
- Display `operand1` as a hexadecimal number on `hex7` and `hex6`, i.e., use the `to_segs` function of the [util package](../../../lib/util/doc.md) to display the 4 upper bits of `operand1` on `hex7` and the 4 lower bits on `hex6`
- Display `operand2` as a hexadecimal number on `hex5` and `hex4`
- Display `result` as a hexadecimal number on `hex3` and `hex2`

Add the required files to your Quartus project and configure the top-level configuration accordingly.
Compile the design with Quartus and download it to the FPGA. 
Check if the calculator works as intended.
