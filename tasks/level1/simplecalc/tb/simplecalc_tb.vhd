library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.task_simplecalc_pkg.all;

entity simplecalc_tb is
end entity;

architecture bench of simplecalc_tb is
    constant CLOCK_PERIOD : time := 1 ns;

    signal clk   : std_logic := '0'; -- clock
    signal res_n : std_logic := '1'; -- low active reset

    signal in_data : std_logic_vector(7 downto 0) := (others => '0'); -- data input
    signal save1   : std_logic                    := '1';             -- toggle to save to op1
    signal save2   : std_logic                    := '1';             -- toggle to save to op2

    signal sub : std_logic := '0'; -- toggle to subtract

    signal operand1 : std_logic_vector(7 downto 0) := (others => '0'); -- save/output 1
    signal operand2 : std_logic_vector(7 downto 0) := (others => '0'); -- save/output 2
    signal result   : std_logic_vector(7 downto 0) := (others => '0'); -- result of calculation

    signal finished : std_logic := '0';
begin
    -- Instantiate the unit under test
    uut: entity work.simplecalc(arch) port map (
        clk      => clk,
        res_n    => res_n,
        in_data  => in_data,
        save1    => save1,
        save2    => save2,
        sub      => sub,
        operand1 => operand1,
        operand2 => operand2,
        result   => result
    );
    -- Stimulus process

    stimulus: process
        procedure test_values(val1, val2 : integer) is
        begin
            assert val1 < 2 ** 8 and val2 < 2 ** 8
                report "Values wrong!"
                severity failure;

            save1 <= '1';
            save2 <= '1';

            wait until falling_edge(clk);

            sub <= '0';
            in_data <= std_logic_vector(to_signed(val1, 8));
            save1 <= '0';

            wait until rising_edge(clk);

            in_data <= std_logic_vector(to_signed(val2, 8));
            save2 <= '0';

            wait until rising_edge(clk);
            wait until falling_edge(clk);

            assert signed(operand1) = to_signed(val1, 8) report "Write did not work on op1!";
            assert signed(operand2) = to_signed(val2, 8) report "Write did not work on op2!";
            assert signed(result) = to_signed(val1 + val2, 8) report "Add did not work!";

            sub <= '1';

            wait until falling_edge(clk);
            assert signed(result) = to_signed(val1 - val2, 8) report "Sub did not work!";

        end procedure;

        procedure test_reset is
        begin
            res_n <= '0';
            wait until rising_edge(clk);

            res_n <= '1';
            wait until rising_edge(clk);

            assert signed(operand1) = 0 report "Reset did not work on op1!";
            assert signed(operand2) = 0 report "Reset did not work on op2!";
        end procedure;

    begin
        report "simulation start";

        -- Apply test stimuli
        test_values(11, 6);
        test_values(12, 60);
        test_values(33, 61);
        test_values(23, 12);
        test_reset;

        finished <= '1';
        report "simulation end";
        -- End simulation
        wait;
    end process;

    clock: process
    begin
        if finished then
            wait;
        end if;

        clk <= '1';
        wait for CLOCK_PERIOD / 2;
        clk <= '0';
        wait for CLOCK_PERIOD / 2;
    end process;
end architecture;

