use work.util_pkg.to_segs;

architecture top_arch_simplecalc of top is
    constant off : std_logic_vector(6 downto 0) := "1111111";
    signal result : std_logic_vector(7 downto 0);
    signal op1    : std_logic_vector(7 downto 0);
    signal op2    : std_logic_vector(7 downto 0);
begin

    calc: entity work.simplecalc(arch) port map (
        clk      => clk,
        res_n    => keys(0),
        in_data  => switches(7 downto 0),
        save1    => keys(2),
        save2    => keys(1),
        sub      => switches(17),
        operand1 => op1,
        operand2 => op2,
        result   => result
    );
	 
	 ledr(7 downto 0) <= switches(7 downto 0);
	 ledr(17) <= switches(17);

    hex7 <= to_segs(op1(7 downto 4));
    hex6 <= to_segs(op1(3 downto 0));

    hex5 <= to_segs(op2(7 downto 4));
    hex4 <= to_segs(op2(3 downto 0));

    hex3 <= to_segs(result(7 downto 4));
    hex2 <= to_segs(result(3 downto 0));

    hex1 <= off;
    hex0 <= off;
end architecture;
