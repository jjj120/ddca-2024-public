library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.util_pkg.to_segs;

entity ssdctrl is
    port (
        clk           : in  std_logic;                     -- clock
        res_n         : in  std_logic;                     -- low active reset

        input_data    : in  std_logic_vector(15 downto 0); -- input in binary
        signed_mode   : in  std_logic;                     -- signed or not
        start         : in  std_logic;                     -- start conversion when going high
        busy          : out std_logic;                     -- currently converting

        hex_digit1    : out std_logic_vector(6 downto 0);
        hex_digit10   : out std_logic_vector(6 downto 0);
        hex_digit100  : out std_logic_vector(6 downto 0);
        hex_digit1000 : out std_logic_vector(6 downto 0);
        hex_sign      : out std_logic_vector(6 downto 0)
    );
end entity;

architecture arch of ssdctrl is
    constant SSD_CHAR_OFF  : std_logic_vector(6 downto 0) := "1111111";
    constant SSD_CHAR_DASH : std_logic_vector(6 downto 0) := "0111111";
    constant SSD_CHAR_O    : std_logic_vector(6 downto 0) := "1000000";
    constant SSD_CHAR_F    : std_logic_vector(6 downto 0) := "0001110";
    constant SSD_CHAR_L    : std_logic_vector(6 downto 0) := "1000111";

    constant NUM_LENGTH : natural := 16;

    subtype digit_t is natural range 0 to 9;
    type state_running_t is (IDLE, CONVERTING);

    type state_t is record
        dig1, dig10, dig100, dig1000 : digit_t;
        sign                         : std_logic;
        remaining_number             : integer;
        running                      : state_running_t;
        overflow                     : std_logic;
    end record;

    signal state, state_next : state_t := (
        -- add initial values
        dig1             => 0,
        dig10            => 0,
        dig100           => 0,
        dig1000          => 0,
        sign             => '0',
        remaining_number => 0,
        running          => IDLE,
        overflow         => '0'
    );

begin

    registers: process (clk, res_n) is
    begin
        if (res_n = '0') then
            state.dig1 <= 0;
            state.dig10 <= 0;
            state.dig100 <= 0;
            state.dig1000 <= 0;
            state.sign <= '0';
            state.remaining_number <= 0;
            state.running <= IDLE;
        elsif rising_edge(clk) then
            state <= state_next;
        end if;
    end process;

    combinatorial: process (all) is
        impure function abs_number(number : std_logic_vector(NUM_LENGTH - 1 downto 0)) return natural is
        begin
            if not signed_mode then
                return to_integer(unsigned(number));
            elsif number(NUM_LENGTH - 1) = '1' then
                -- number negative!
                return (- to_integer(signed(number)));
            else
                return to_integer(signed(number));
            end if;
        end function;
    begin
        state_next <= state;

        if (state.running = IDLE) and (start = '1') then
            if signed_mode then
                -- init for signed mode
                state_next.sign <= input_data(NUM_LENGTH - 1);
                state_next.remaining_number <= abs_number(input_data);
            else
                -- init for unsigned mode
                state_next.sign <= '0';
                state_next.remaining_number <= abs_number(input_data);
            end if;

            -- init/reset the rest of the state
            state_next.dig1 <= 0;
            state_next.dig10 <= 0;
            state_next.dig100 <= 0;
            state_next.dig1000 <= 0;
            state_next.running <= CONVERTING;
            state_next.overflow <= '0';

            -- check for overflow
            if abs_number(input_data) >= 10 ** 4 then
                state_next.overflow <= '1';
            end if;
        elsif state.running = CONVERTING then
            if state.overflow = '1' then
                state_next.running <= IDLE;
                -- converting number
            elsif state.remaining_number >= 1000 then
                state_next.remaining_number <= state.remaining_number - 1000;
                state_next.dig1000 <= state.dig1000 + 1;
            elsif state.remaining_number >= 100 then
                state_next.remaining_number <= state.remaining_number - 100;
                state_next.dig100 <= state.dig100 + 1;
            elsif state.remaining_number >= 10 then
                state_next.remaining_number <= state.remaining_number - 10;
                state_next.dig10 <= state.dig10 + 1;
            else
                -- remaining number is digit1, conversion ready
                state_next.dig1 <= state.remaining_number;
                state_next.running <= IDLE;
            end if;
        else
            state_next <= state;
        end if;
    end process;

    -- write out data
    hex_digit1    <= (to_segs(std_logic_vector(to_unsigned(state.dig1, 4)))) when not state.overflow else SSD_CHAR_L;
    hex_digit10   <= (to_segs(std_logic_vector(to_unsigned(state.dig10, 4)))) when not state.overflow else SSD_CHAR_F;
    hex_digit100  <= (to_segs(std_logic_vector(to_unsigned(state.dig100, 4)))) when not state.overflow else SSD_CHAR_O;
    hex_digit1000 <= (to_segs(std_logic_vector(to_unsigned(state.dig1000, 4)))) when not state.overflow else SSD_CHAR_OFF;
    hex_sign      <= SSD_CHAR_DASH when state.sign and not state.overflow else SSD_CHAR_OFF;
    busy          <= '1' when state.running = CONVERTING else '0';
end architecture;
