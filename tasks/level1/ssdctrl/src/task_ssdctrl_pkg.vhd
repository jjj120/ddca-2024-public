library ieee;
    use ieee.std_logic_1164.all;

package task_ssdctrl_pkg is
    component ssdctrl
        port (
            clk           : in  std_logic;
            res_n         : in  std_logic;

            input_data    : in  std_logic_vector(15 downto 0);
            signed_mode   : in  std_logic;
            start         : in  std_logic;
            busy          : out std_logic;

            hex_digit1    : out std_logic_vector(6 downto 0);
            hex_digit10   : out std_logic_vector(6 downto 0);
            hex_digit100  : out std_logic_vector(6 downto 0);
            hex_digit1000 : out std_logic_vector(6 downto 0);
            hex_sign      : out std_logic_vector(6 downto 0)
        );
    end component;
end package;
