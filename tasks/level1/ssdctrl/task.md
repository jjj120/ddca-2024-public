
# Seven-Segment Display Controller
**Points:** 3 ` | ` **Keywords:** FSM, arithmetic operations

Create a [seven-segment display](https://en.wikipedia.org/wiki/Seven-segment_display) controller for displaying signed and unsigned 16-bit binary numbers as 4-digit decimals.



The 16-bit integer value to convert is provided via the `input_data` input.
If `signed_mode` is asserted `input_data` data has to be interpreted as a signed integer in [two's complement](https://en.wikipedia.org/wiki/Two%27s_complement).
The outputs `hex_digit1` to `hex_digit1000` hold the symbols for the four decimal digits of the converted number, while `hex_sign` shows a minus symbol in case `signed_mode` is asserted and `input_data` is negative.

A conversion process is started when the `start` signal is high.
While a conversion is in progress, the `busy` output is one.
A new conversion can only be started if `busy` is zero.


The actual conversion **must not** be implemented using a division operation, but by successively subtracting decimal powers (i.e., once every clock cycles) in a state machine.
You have to implement an FSM (Finite State Machine) which performs this process step by step (i.e., one subtraction per clock cycle) effectively converting the number into a [BCD](https://en.wikipedia.org/wiki/Binary-coded_decimal) (binary coded decimal).

You can follow the following scheme in your FSM to convert an unsigned binary number:

- Start in an initial state and wait for `start` to go high. If it does, go to the next state, otherwise remain in this initial state.
- Reads the current value at `input_data`.
- Check whether it is larger than (10^4-1) (maximum number 4 decimal digits are able to represent).
- If that's the case make the seven-segment display show `" OFL"` (set hex3-0 accordingly).
- Otherwise determine the thousands digit (`hex_digit1000`) by subtracting 1000 each clock cycle until the value is smaller or equal to 999.
- By counting the number of times 1000 could be subtracted, the thousands digit is obtained.
- Assign this counter value to the corresponding `hex_digit1000` by using the `to_segs` function of the [`util`](../../../lib/util/doc.md) package.
- Now, repeat this with 100 for the hundreds digit and 10 for the tens digit.
- The rest corresponds to the ones digit.
- Go back to the initial state and wait for a new conversion to start.

Whenever `signed_mode` input flag is asserted `input_data` needs to be checked whether its smaller than 0 and, if that's the case, converted to a positive number before the decimal conversion.
To make things easier, you are allows to draw the leading zeros when generating the `hex_digit_*` outputs.

Place the `ssdctrl` entity in the file `src/ssdctrl.vhd`.

**Hints:** Do not forget to reset all your registers during the active-low reset (`res_n`) is active!

**Optional:**
If you want you can, add a feature to the FSM that a new conversion is only started if `input_data` changed since the last conversion.

## Testbench

Create a testbench for the `ssdctrl` entity and place it in the file [`tb/ssdctrl_tb.vhd`](tb/ssdctrl_tb.vhd).
Include at least 8 test cases for both signed and unsigned mode.
Moreover, make sure to also test the overflow detection.


## Hardware Test

Add an instances of the `ssdctrl` entity to the top-level architecture in [`top_arch_ssdctrl.vhd`](top_arch_ssdctrl.vhd).

- Connect the clock signal to `clk`
- Connect `keys(0)` to `res_n`
- Use `switches(15 downto 0)` for input `input_data`.
- Use `switches(17)` for input `signed_mode`.
- Set `start` to constant `'1'`
- Set `busy` to `open`
- Connect `hex_*` output to `hex4`-`hex0`.

Add the required files to your Quartus project and configure the top-level configuration accordingly.
Compile the design with Quartus and download it to the FPGA. 
Check if the seven-segment display controller works as intended.

