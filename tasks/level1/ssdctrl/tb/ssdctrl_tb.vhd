library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.task_ssdctrl_pkg.all;
    use work.util_pkg.all;

entity ssdctrl_tb is
end entity;

architecture bench of ssdctrl_tb is
    constant CLOCK_PERIOD_TIME : time := 1 ns;

    signal clk   : std_logic; -- clock
    signal res_n : std_logic; -- low active reset

    signal input_data  : std_logic_vector(15 downto 0) := (others => '0'); -- input in binary
    signal signed_mode : std_logic                     := '0';             -- signed or not
    signal start       : std_logic                     := '0';             -- start conversion when going high
    signal busy        : std_logic;                                        -- currently converting

    signal hex_digit1    : std_logic_vector(6 downto 0);
    signal hex_digit10   : std_logic_vector(6 downto 0);
    signal hex_digit100  : std_logic_vector(6 downto 0);
    signal hex_digit1000 : std_logic_vector(6 downto 0);
    signal hex_sign      : std_logic_vector(6 downto 0);

    signal finished : std_logic := '0';
begin

    -- Instantiate the unit under test
    uut: entity work.ssdctrl
        port map (
            clk           => clk,
            res_n         => res_n,
            input_data    => input_data,
            signed_mode   => signed_mode,
            start         => start,
            busy          => busy,
            hex_digit1    => hex_digit1,
            hex_digit10   => hex_digit10,
            hex_digit100  => hex_digit100,
            hex_digit1000 => hex_digit1000,
            hex_sign      => hex_sign
        );

    -- Stimulus process

    stimulus: process
        procedure apply_number(number : std_logic_vector(15 downto 0); number_signed : boolean) is
        begin
            res_n <= '0';
            wait until rising_edge(clk);
            res_n <= '1';
            input_data <= number;
            signed_mode <= '1' when number_signed
        else
            '0';

            start <= '1';

            wait until rising_edge(clk);
            start <= '0';
            wait until (busy = '0');
        end procedure;

        procedure check_number(number : integer) is
            constant SSD_CHAR_OFF  : std_logic_vector(6 downto 0) := "1111111";
            constant SSD_CHAR_DASH : std_logic_vector(6 downto 0) := "0111111";
            constant SSD_CHAR_O    : std_logic_vector(6 downto 0) := "1000000";
            constant SSD_CHAR_F    : std_logic_vector(6 downto 0) := "0001110";
            constant SSD_CHAR_L    : std_logic_vector(6 downto 0) := "1000111";

            variable dig1, dig10, dig100, dig1000 : std_logic_vector(6 downto 0);
        begin
            -- calculate right digits by using division and modulo operation
            dig1 := to_segs(std_logic_vector(to_unsigned((abs(number / 1) mod 10), 4)));
            dig10 := to_segs(std_logic_vector(to_unsigned((abs(number / 10) mod 10), 4)));
            dig100 := to_segs(std_logic_vector(to_unsigned((abs(number / 100) mod 10), 4)));
            dig1000 := to_segs(std_logic_vector(to_unsigned((abs(number / 1000) mod 10), 4)));

            -- check sign
            if number < 0 and (number /= - 10 ** 4) then
                assert hex_sign = SSD_CHAR_DASH
                    report "Sign not right at " & to_string(number) & ": " & to_string(hex_sign) & ", should be -";
            else
                assert hex_sign = SSD_CHAR_OFF
                    report "Sign not right at " & to_string(number) & ": " & to_string(hex_sign) & ", should be off";
            end if;

            if (number >= 10 ** 4) or (number <= - 10 ** 4) then
                -- check OFL
                assert (hex_digit1000 = SSD_CHAR_OFF)
                    report "OFL (off) not right at " & to_string(number) & ": " & to_string(hex_digit1000) & " should be " & to_string(SSD_CHAR_OFF);
                assert (hex_digit100 = SSD_CHAR_O)
                    report "OFL (O) not right at " & to_string(number) & ": " & to_string(hex_digit100) & " should be " & to_string(SSD_CHAR_O);
                assert (hex_digit10 = SSD_CHAR_F)
                    report "OFL (F) not right at " & to_string(number) & ": " & to_string(hex_digit10) & " should be " & to_string(SSD_CHAR_F);
                assert (hex_digit1 = SSD_CHAR_L)
                    report "OFL (L) not right at " & to_string(number) & ": " & to_string(hex_digit1) & " should be " & to_string(SSD_CHAR_L);
            else
                -- check number
                assert (hex_digit1 = dig1)
                    report "Digit1 not right at " & to_string(number) & ": " & to_string(hex_digit1);
                assert (hex_digit10 = dig10)
                    report "Digit10 not right at " & to_string(number) & ": " & to_string(hex_digit10);
                assert (hex_digit100 = dig100)
                    report "Digit100 not right at " & to_string(number) & ": " & to_string(hex_digit100);
                assert (hex_digit1000 = dig1000)
                    report "Digit1000 not right at " & to_string(number) & ": " & to_string(hex_digit1000);
            end if;

        end procedure;
    begin
        report "simulation start";

        -- check signed
        for num in - 10 ** 4 to 10 ** 4 loop
            apply_number(std_logic_vector(to_signed(num, 16)), true);
            check_number(num);
        end loop;

        -- check unsigned
        for num in 0 to 10 ** 4 loop
            apply_number(std_logic_vector(to_unsigned(num, 16)), false);
            check_number(num);
        end loop;

        finished <= '1';
        report "simulation end";
        -- End simulation
        wait;
    end process;

    clock: process
    begin
        if finished = '1' then
            wait;
        end if;

        clk <= '1';
        wait for CLOCK_PERIOD_TIME / 2;
        clk <= '0';
        wait for CLOCK_PERIOD_TIME / 2;

    end process;
end architecture;

