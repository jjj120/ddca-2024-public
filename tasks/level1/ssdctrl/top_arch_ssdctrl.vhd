use ieee.std_logic_1164.all;
use work.task_ssdctrl_pkg.all;

architecture top_arch_ssdctrl of top is
begin

    ctrl: entity work.ssdctrl
        port map (
            clk           => clk,
            res_n         => keys(0),
            input_data    => switches(15 downto 0),
            signed_mode   => switches(17),
            start         => not keys(3),
            busy          => ledg(8),
            hex_digit1    => hex0,
            hex_digit10   => hex1,
            hex_digit100  => hex2,
            hex_digit1000 => hex3,
            hex_sign      => hex4
        );

    ledr(17) <= switches(17);
    ledr(15 downto 0) <= switches(15 downto 0);
	 
	 ledg(6) <= not keys(3);
	 ledg(0) <= not keys(0);

    hex5 <= "1111111";
    hex6 <= "1111111";
    hex7 <= "1111111";

end architecture;
