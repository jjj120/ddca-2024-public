library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.char_lcd_ctrl_pkg.all;
use work.math_pkg.all;

entity char_lcd_ctrl is
	generic (
		CLK_FREQ     : integer := 50_000_000
	);
	port (
		clk          : in  std_logic;
		res_n        : in  std_logic;
		
		instr_wr     : in std_logic;
		instr        : in instr_t;
		instr_data   : in std_logic_vector(7 downto 0);
		busy         : out std_logic;
		
		lcd_data     : inout std_logic_vector(7 downto 0);
		lcd_en       : out std_logic;
		lcd_rw       : out std_logic;
		lcd_rs       : out std_logic
	);
end entity;


architecture arch of char_lcd_ctrl is 
	constant ROW_COUNT : integer := 2;
	constant COLUMN_COUNT : integer := 16;
begin

end architecture;
