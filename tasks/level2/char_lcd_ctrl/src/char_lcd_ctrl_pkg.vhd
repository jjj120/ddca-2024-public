library ieee;
use ieee.std_logic_1164.all;

package char_lcd_ctrl_pkg is
	-- cursor options
	constant CURSOR_STATE_OFF : std_logic_vector(1 downto 0) := "00";
	constant CURSOR_STATE_ON : std_logic_vector(1 downto 0) := "10";
	constant CURSOR_STATE_BLINK : std_logic_vector(1 downto 0) := "11";

	-- control characters
	constant CHAR_LINEFEED : std_logic_vector(7 downto 0) := x"0A";
	constant CHAR_BACKSPACE : std_logic_vector(7 downto 0) := x"08";
	constant CHAR_CARRIAGE_RETURN : std_logic_vector(7 downto 0) := x"0D";

	subtype instr_t is std_logic_vector(3 downto 0);

	--Instructions for the char_lcd_ctrl
	constant INSTR_NOP : instr_t := (others=>'0');
	constant INSTR_SET_CHAR : instr_t := x"1";
	constant INSTR_CLEAR_SCREEN : instr_t := x"2";
	constant INSTR_SET_CURSOR_POSITION : instr_t := x"3";
	constant INSTR_CFG : instr_t := x"4";
	constant INSTR_DELETE : instr_t := x"5";
	constant INSTR_ADVANCE_CURSOR : instr_t := x"6";
	
	
	component char_lcd_ctrl is
		generic (
			CLK_FREQ : integer
		);
		port (
			clk          : in std_logic;
			res_n        : in std_logic;

			-- internal interface
			instr_wr     : in std_logic;
			instr        : in std_logic_vector(3 downto 0);
			instr_data   : in std_logic_vector(7 downto 0);
			busy         : out std_logic;

			-- external interface to the character LCD
			lcd_data     : inout std_logic_vector(7 downto 0);
			lcd_en       : out std_logic;
			lcd_rw       : out std_logic;
			lcd_rs       : out std_logic
		);
	end component;

end package;







