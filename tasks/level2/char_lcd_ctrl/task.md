
# Character LCD Controller
**Points:** 3 ` | ` **Keywords:** FSM, External Interface, Datasheet

In this task you will implement a Character LCD Controller that implements the interface to the FPGA board's (monochrome) 16x2 character LCD (i.e., the display above the seven-segment displays).

## Task

Implement the `char_lcd_ctrl`, a suitable template is provided in [char_lcd_ctrl.vhd](src/char_lcd_ctrl.vhd).
The module shall have the following interface:

```vhdl
component char_lcd_ctrl is
	generic (
		CLK_FREQ : integer
	);
	port (
		clk          : in std_logic;
		res_n        : in std_logic;

		-- internal interface
		instr_wr     : in std_logic;
		instr        : in std_logic_vector(3 downto 0);
		instr_data   : in std_logic_vector(7 downto 0);
		busy         : out std_logic;

		-- external interface to the character LCD
		lcd_data     : inout std_logic_vector(7 downto 0);
		lcd_en       : out std_logic;
		lcd_rw       : out std_logic;
		lcd_rs       : out std_logic
	);
end component;
```

The signals `clk` and `res_n` are the clock and the active-low reset signal.

The `instr*` signals define a simple instruction-based interface.
To execute an instruction, it must be assigned to the 4-bit instruction code input `instr`.
At the same time (i.e., in the same clock cycle), the corresponding 8-bit instruction data must be applied to the `instr_data` input (if required).
Once the `instr_wr` signal is set to high for **exactly** one clock cycle, the controller acknowledges the reception of the instruction by setting its `busy` output signal in the next clock cycle.
As long as this signal remains high (this can last for multiple clock cycles), no new instruction will be accepted by the controller and the current instruction and data must be kept stable.
Once the `char_lcd_ctrl` is done executing the instruction, the `busy` signal is set to low again.
At this point a new instruction may be issued.

The timing diagram below illustrates the interface's behavior.
It shows two `SET_CHAR` instructions being issued, that write the capital letters 'A' (0x41) and 'B' (0x42).

![Instruction Interface Timing](.mdata/instr_timing.svg)

The `lcd_*` signals implement the (external) interface to the board's character LCD (HD44780).
You can find the exact interface specification int its [datasheet](https://www.sparkfun.com/datasheets/LCD/HD44780.pdf).
The signals used by the `lcd_*` interface are described on page 8.
For initialization and communication with the display use the 8-bit interface.
Set the bits `N` and `F` (*Function Set* command) to 1 (as we want to use two rows and 8-bit characters).

The generic `CLK_FREQ` shall be used to derive the timings specified in the read and write timing characteristics and wave forms (page 49 and 58).

**Remark**: To be on the safe side, waiting a few microseconds longer than the instruction execution time specified in the *Instruction Table* of the datasheet, is highly recommended.
Our tests showed that these values vary across different boards.
We hence recommend a safety margin of 10%.
However, a far more elegant solution to find out whether the display is ready to process the next instruction is by reading its busy flag using the *Read Busy Flag and Address* instruction.

**Hint**: You might want to consider implementing the low-level display interface in a separate entity that is then used by the `char_lcd_ctrl` entity, which in turn implements the (high-level) instruction interface.
This allows you to test the low-level interface implementation independently and thus makes it easier to find errors.
If you do so, be sure to put this entity into a separate file in the `src` directory.

### Functionality
The `char_lcd_ctrl` shall implement a simple cursor interface.
Characters are always written to the current cursor position.
Depending on a configuration flag, the cursor can be incremented automatically after writing a character to the screen.
Note that the cursor can also be freely positioned on the screen using the respective instruction.

When enabled the *automatic increment mode* the controller automatically increments the cursor position by one whenever a character is written (using the `SET_CHAR` instruction).
If the mode is not enabled, the cursor shall remain at its position after writing a character.
Per default, this mode is deactivated.

If the *special character handling* feature is enabled, writing the special characters 0x0A (line feed), 0x08 (backspace) and 0x0D (carriage return) leads to the respective action being performed.
For the line feed, the cursor is set to the next row at column 0.
Furthermore, the row where the cursor is being moved to is cleared.
The carriage return character resets the cursor to column 0 of its current row without altering the characters displayed on the screen.
By writing the backspace character, the character displayed at the current position is erased and the cursor is moved to the previous writing position.

If *special character handling* is deactivated (which is the default configuration), the corresponding characters in the font are normally drawn as any other character.

Note that the cursor is supposed to wrap around, i.e., if the cursor is at last column of the bottom row an increase leads to it being set to the top row and column 0.
Likewise, if the cursor is at column 0 of row 0 and being decreased, it shall change to the last position of the bottom row.

The cursor supports three states (on, off and blinking).

### Supported Instructions
The section lists all instructions supported by the `char_lcd_ctrl`.

#### INSTR_NOP
**Data**:

![INSTR_NOP](.mdata/dont_care.svg)

**Description**: No operation.

#### INSTR_SET_CHAR
**Data**:

![INSTR_SET_CHAR](.mdata/set_char.svg)

**Description**: Writes the specified character on the LCD screen at the current cursor position.
`char` is the ASCII code of the character to be written.

#### INSTR_DELETE
**Data**:

![INSTR_DELETE](.mdata/dont_care.svg)

**Description**: Clears the character at the current cursor position and moves the cursor to the preceding position.

#### INSTR_CLEAR_SCREEN
**Data**:

![INSTR_CLEAR_SCREEN](.mdata/dont_care.svg)

**Description**: Clears the screen and sets the cursor to the position row 0 column 0.

#### INSTR_CFG
**Data**:

![INSTR_CFG](.mdata/cfg.svg)

**Description**: Used to configure the controller.
The automatic increment mode is activated / deactivated by setting the `ai` flag to 1 / 0.
The special character handling feature is activated / deactivated by writing 1 / 0 to the respective flag (`sch`).
The cursor state can be set to `OFF`, `ON` and `BLINKING` using the `cm` option.

#### INSTR_SET_CURSOR_POSITION
**Data**:

![INSTR_SET_CURSOR_POSITION](.mdata/set_cursor_position.svg)

**Description**: Moves the cursor to the specified position.
Only `x` and `y` values corresponding to a valid cursor position on the screen may be issued.


#### INSTR_ADVANCE_CURSOR
**Data**:

![INSTR_ADVANCE_CURSOR](.mdata/dont_care.svg)

**Description**: Advances the cursor to the next position, i.e.,increments it.


Note that you are provided with a [package](./src/char_lcd_ctrl_pkg.vhd) containing the instruction codes, a respective type and constants for the cursor states.

## Testing
Testing the correctness of your low-level LCD driver can be quite challenging, as you can hardly use a simulation without a proper simulation model of the LCD driver.
Instead, if you suspect problems with your implementation of this low-level interface, you can use an oscilloscope to view the signals generated by your design.
To do so connect the signals you want to inspect to the `aux` output in the top-level architecture.
Nevertheless, we still recommend you to create at least some minimal testbench that allows you to check if your LCD interface implementation sets the signals as expected.


### Hardware Test
The file [top_arch_char_lcd_ctrl.vhd](top_arch_char_lcd_ctrl.vhd) contains a template of a `top` architecture using the `char_lcd_ctrl`.

Connects the `char_lcd_ctrl`'s `instr_data` and `instr` inputs to `switches(7 downto 0)` and `switches(11 downto 8)`, respectively.
Pressing `keys(1)` shall issue the instruction and data applied at the switches to the `char_lcd_ctrl`.
Furthermore, pressing `keys(2)` shall lead to the string "Hello World!" being displayed at the current cursor position.

Make sure to test all the instruction and modes (do not forget the supported special characters).

**Hint**: You can use the `remote.py` script's capability to set the buttons and switches to write little test sequences (you can execute this script on any of the lab computers, not just in the Remote Lab).
This way you do not always have to use the GUI to remotely access the switches and buttons while testing.
Furthermore, you can reuse your test sequences easily while debugging.

<!--Create a state machine that reads inputs from a controller (you can use the respective precompiled modules) and lets you interface with the screen.

The up / down button shall be used to select a character of the LCD driver's supported character patterns in the range 0x20-0x7F.
The right button is used to advance the cursor and the left button to delete the previous character (and move the cursor accordingly).
Use the button X to clear the screen, button Y to toggle the special character handling mode and button A to switch between the different cursor modes.
Finally, button B shall allow it to print a character defined by the first eight switches of the board.
Use this to test the right behavior of the special characters.-->
