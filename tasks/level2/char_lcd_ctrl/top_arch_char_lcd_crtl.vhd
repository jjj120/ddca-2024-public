library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.math_pkg.all;
use work.char_lcd_ctrl_pkg.all;

architecture top_arch_char_lcd_ctrl of top is
	signal res_n : std_logic;
	signal instr_wr, busy : std_logic;
	signal instr : instr_t;
	signal instr_data : std_logic_vector(7 downto 0);

	-- TODO: Add further signals, constants, functions etc. as required

begin
	res_n <= keys(0);
	char_lcd_on <= '1';   -- Activate LCD
	char_lcd_blon <= '0'; -- Value not important (see datasheet)

	-- TODO: Generate state machine interfacing with char_lcd_ctrl

	char_lcd_ctrl_inst : char_lcd_ctrl
	generic map (
		CLK_FREQ => 50_000_000
	)
	port map (
		clk          => clk,
		res_n        => res_n,
		instr_wr     => instr_wr,
		instr        => instr,
		instr_data   => instr_data,
		busy         => busy,
		lcd_data     => char_lcd_data,
		lcd_en       => char_lcd_en,
		lcd_rw       => char_lcd_rw,
		lcd_rs       => char_lcd_rs
	);

end architecture;
