library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.gc_ctrl_pkg.all;
    use work.sync_pkg.sync;

entity gc_ctrl is
    generic (
        CLK_FREQ        : integer := 50_000_000;
        SYNC_STAGES     : integer := 2;
        REFRESH_TIMEOUT : integer := 60000;
        POLL_TIMEOUT    : time    := 400 us
    );
    port (
        clk        : in    std_logic;
        res_n      : in    std_logic;

        -- connection to the controller
        data       : inout std_logic;

        -- internal connection
        ctrl_state : out   gc_ctrl_state_t;
        rumble     : in    std_logic
    );
end entity;

architecture arch of gc_ctrl is
    constant CYCLES_PER_PERIOD   : integer := 4 us / (1 sec / CLK_FREQ);
    constant CYCLES_FOR_LOW      : integer := 3 us / (1 sec / CLK_FREQ);
    constant CYCLES_FOR_HIGH     : integer := 1 us / (1 sec / CLK_FREQ);
    constant CYCLES_TO_MIDDLE    : integer := 2 us / (1 sec / CLK_FREQ); -- read data between edge for falling and rising edge
    constant CYCLES_POLL_TIMEOUT : integer := POLL_TIMEOUT / (1 sec / CLK_FREQ);

    signal poll_command : std_logic_vector(0 to 24) := "0100000000000011000000001"; -- reverse index!

    type state_type is (IDLE, WRITING, READING);
    type state_t is record
        state                 : state_type;
        current_bit           : integer;
        clock_counter         : integer;
        idle_counter          : integer;
        button_state_internal : gc_ctrl_state_t; -- internal button state
        button_state_out      : gc_ctrl_state_t; -- button state that is written to outside
        data_out              : std_logic;
    end record;

    signal state, next_state : state_t := (
        state                 => WRITING,
        current_bit           => 0,
        clock_counter         => 0,
        idle_counter          => 0,
        button_state_internal => GC_BUTTONS_RESET_VALUE,
        button_state_out      => GC_BUTTONS_RESET_VALUE,
        data_out              => '0'
    );

    signal data_sync : std_logic;
begin
    poll_command <= "01000000000000110000000" & rumble & "1";
    ctrl_state   <= state.button_state_out;
    data         <= '0' when state.data_out = '0' else 'Z';

    sync_inst: sync
        generic map (
            SYNC_STAGES => SYNC_STAGES,
            RESET_VALUE => '0'
        )
        port map (
            clk      => clk,
            res_n    => res_n,
            data_in  => to_X01(data),
            data_out => data_sync
        );

    registers: process (clk, res_n) is
    begin
        if not res_n then
            state <= (
                state                 => WRITING,
                current_bit           => 0,
                clock_counter         => 0,
                idle_counter          => 0,
                button_state_internal => GC_BUTTONS_RESET_VALUE,
                button_state_out      => GC_BUTTONS_RESET_VALUE,
                data_out              => '0'
            );
        elsif rising_edge(clk) then
            state <= next_state;
        end if;
    end process;

    comb: process (all) is
    begin
        -- get default and always increment counter
        next_state <= state;
        next_state.clock_counter <= state.clock_counter + 1;

        if state.state = READING then
            -- pause the clock counter until falling edge
            if state.clock_counter = 0 and data_sync = '1' then
                next_state.clock_counter <= 0;

                next_state.idle_counter <= state.idle_counter + 1;
                if state.idle_counter >= CYCLES_POLL_TIMEOUT then
                    -- got no command, retry
                    next_state.state <= IDLE;
                    next_state.current_bit <= 0;
                end if;
            end if;
        end if;

        if state.state = WRITING or state.state = READING then
            if state.clock_counter = CYCLES_PER_PERIOD - 1 then
                next_state.current_bit <= state.current_bit + 1;
                next_state.clock_counter <= 0;
                next_state.idle_counter <= 0;
            end if;
        else
            -- state : IDLE
            if state.clock_counter >= REFRESH_TIMEOUT - 1 then
                next_state.current_bit <= 0;
                next_state.clock_counter <= 0;
                next_state.idle_counter <= 0;
                next_state.state <= WRITING;
            end if;
        end if;

        case state.state is
            when WRITING =>
                next_state.data_out <= '0';
                if state.current_bit <= poll_command'high then
                    -- write
                    if poll_command(state.current_bit) = '1' and state.clock_counter > CYCLES_FOR_HIGH then
                        -- high bit
                        next_state.data_out <= '1';
                    elsif not (poll_command(state.current_bit) = '1') and state.clock_counter > CYCLES_FOR_LOW then
                        -- low bit
                        next_state.data_out <= '1';
                    end if;
                else
                    -- end of writing
                    next_state.data_out <= '1'; -- will be converted to 'Z' on data out
                    next_state.current_bit <= poll_command'high + 1;
                    next_state.clock_counter <= 0;
                    next_state.current_bit <= 0;
                    next_state.state <= READING;
                end if;
            when READING =>
                if state.clock_counter = CYCLES_TO_MIDDLE then
                    case state.current_bit is
                        when 0 to 2 =>
                            -- do nothing
                        when 3 =>
                            next_state.button_state_internal.btn_start <= data_sync;
                        when 4 =>
                            next_state.button_state_internal.btn_y <= data_sync;
                        when 5 =>
                            next_state.button_state_internal.btn_x <= data_sync;
                        when 6 =>
                            next_state.button_state_internal.btn_b <= data_sync;
                        when 7 =>
                            next_state.button_state_internal.btn_a <= data_sync;
                        when 8 =>
                            -- do nothing
                        when 9 =>
                            next_state.button_state_internal.btn_l <= data_sync;
                        when 10 =>
                            next_state.button_state_internal.btn_r <= data_sync;
                        when 11 =>
                            next_state.button_state_internal.btn_z <= data_sync;
                        when 12 =>
                            next_state.button_state_internal.btn_up <= data_sync;
                        when 13 =>
                            next_state.button_state_internal.btn_down <= data_sync;
                        when 14 =>
                            next_state.button_state_internal.btn_right <= data_sync;
                        when 15 =>
                            next_state.button_state_internal.btn_left <= data_sync;
                        when 16 to 23 =>
                            next_state.button_state_internal.joy_x(7 - (state.current_bit - 16)) <= data_sync;
                        when 24 to 31 =>
                            next_state.button_state_internal.joy_y(7 - (state.current_bit - 24)) <= data_sync;
                        when 32 to 39 =>
                            next_state.button_state_internal.c_x(7 - (state.current_bit - 32)) <= data_sync;
                        when 40 to 47 =>
                            next_state.button_state_internal.c_y(7 - (state.current_bit - 40)) <= data_sync;
                        when 48 to 55 =>
                            next_state.button_state_internal.trigger_l(7 - (state.current_bit - 48)) <= data_sync;
                        when 56 to 63 =>
                            next_state.button_state_internal.trigger_r(7 - (state.current_bit - 56)) <= data_sync;
                        when 64 =>
                            -- top bit
                        when others =>
                            -- do nothing
                    end case;
                elsif state.clock_counter = CYCLES_PER_PERIOD - 1 and state.current_bit = 63 then
                    next_state.button_state_out <= state.button_state_internal; -- write button state only at the end
                elsif state.clock_counter = CYCLES_PER_PERIOD - 1 and state.current_bit = 64 then
                    -- state transition
                    next_state.current_bit <= 0;
                    next_state.state <= IDLE;
                end if;
            when IDLE =>
                -- do nothing
                null;
        end case;
    end process;
end architecture;
