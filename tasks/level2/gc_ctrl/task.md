
# GameCube Controller
**Points:** 1 ` | ` **Keywords:** FSM, External Interface, Tri-State

In this task you will implement the interface to the GameCube controller by yourself.
A precompiled GameCube controller and its documentation is already provided in [libs/gc_ctrl](../../../libs/gc_ctrl/doc.md).

Check out the controller interface:

```vhdl
component gc_ctrl is
	generic (
		CLK_FREQ : integer := 50_000_000;
		SYNC_STAGES : integer := 2;
		REFRESH_TIMEOUT : integer := 60000
	);
	port (
		clk : in std_logic;
		res_n : in std_logic;

		-- connection to the controller
		data : inout std_logic;

		-- internal connection
		ctrl_state : out gc_ctrl_state_t;
		rumble : in std_logic
	);
end component;
```

The GameCube uses a simple serial interface to transfer the button states, analog triggers and joy stick values over a single (half-duplex) data wire.

The figure below depicts the physical connection between GameCube and the FPGA:

![Physical connection between GameCube controller and FPGA](.mdata/gc_connection.svg)

A pull-up resistor is used to connect the `data` wire to the supply voltage (3.3 V).
This means that unless no communication partner actively drives `data` low, the idle state of the wire is high. 
Although the figure above shows the pull-up resistor as an external component to the FPGA, it is actually an internal resistor integrated into the I/O cell of the chip that can be activated by the FPGA design.
You don’t have to take care for this resistor, since this has already been configured by the pinout file.

The additional 5 V supply line is required to drive the rumble motor in the controller.

To start a new transmission to read the current state (pressed/not pressed) of each button as well as the analog values of the triggers and joy sticks (x and y displacement), a 24-bit long polling command (`0100 00000000 0011 0000 000R`) followed by a single (high) stop-bit must be sent to the controller. 
The individual bits are transmitted using a [pulse width encoding](https://en.wikipedia.org/wiki/Pulse-width_modulation).
The transmission of a single bit takes 4 µs and always starts with a falling edge on `data`. 
A logic zero is represented by a rising edge after 3 µs (measured from the start of the bit, i.e., the falling edge), while for a logic one the rising edge must happen after 1 µs.
Using this scheme the receiver can always synchronize to the falling edge of the `data` signal without the need for an explicit clock signal.
Notice that one bit in the polling command is marked with R. This bit controls the rumble feature of the controller.
Setting it to one will activate rumbling until the next transmission cycle.
The timing diagram below visualizes this protocol.

![GameCube Transmission Protocol](.mdata/gc_timing.svg)

After sending the polling command, the GameCube controller immediately answers with a 64-bit response using the same encoding, again followed by a stop bit.
In order to receive this data, the FPGA has to switch off its output driver (i.e., output ’Z’ on `data` pin) immediately after the end of the polling command.
The table below lists the meaning of the individual bits of the controller’s response.

| Bit   | Content            |
|-------|--------------------|
| 0-2   | unused (always 0)  |
| 3     | Start              |
| 4     | Y                  |
| 5     | X                  |
| 6     | B                  |
| 7     | A                  |
| 8     | unused (always 1)  |
| 9     | L                  |
| 10    | R                  |
| 11    | Z                  |
| 12    | Up                 |
| 13    | Down               |
| 14    | Right              |
| 15    | Left               |
| 16-23 | Joy Stick X-Axis (MSB @ bit 16)  |
| 24-31 | Joy Stick Y-Axis (MSB @ bit 24)  |
| 32-39 | C Stick X-Axis (MSB @ bit 32)    |
| 40-47 | C Stick Y-Axis (MSB @ bit 40)    |
| 48-55 | Left Trigger (MSB @ bit 48)      |
| 56-63 | Right Trigger (MSB @ bit 56)     |
| 64    | Stop bit (always 1)              |


The `gc_ctrl_pkg` provided by the `lib` core already contains a component declaration for the entity you can find in [`gc_ctrl.vhd`](src/gc_ctrl.vhd).

```vhdl
component gc_ctrl is
	generic (
		CLK_FREQ : integer := 50_000_000;
		SYNC_STAGES : integer := 2;
		REFRESH_TIMEOUT : integer := 60000
	);
	port (
		clk : in std_logic;
		res_n : in std_logic;

		-- connection to the controller
		data : inout std_logic;

		-- internal connection
		ctrl_state : out gc_ctrl_state_t;
		rumble : in std_logic
	);
end component;
```

The core shall continuously read out the GameCube controller state and update its `ctrl_state` output accordingly.
This output is a record defined in `gc_ctrl_pkg` and is shown below.

```vhdl
type gc_ctrl_state_t is record
	-- buttons
	btn_up : std_logic;
	btn_down : std_logic;
	btn_left : std_logic;
	btn_right : std_logic;
	btn_a : std_logic;
	btn_b : std_logic;
	btn_x : std_logic;
	btn_y : std_logic;
	btn_z : std_logic;
	btn_start : std_logic;
	btn_l : std_logic;
	btn_r : std_logic;
	-- joysticks
	joy_x : std_logic_vector(7 downto 0);
	joy_y : std_logic_vector(7 downto 0);
	c_x : std_logic_vector(7 downto 0);
	c_y : std_logic_vector(7 downto 0);
	-- trigger
	trigger_l : std_logic_vector(7 downto 0);
	trigger_r : std_logic_vector(7 downto 0);
end record;
```

Each of the physical controller buttons, triggers and joysticks has an entry in this record.
It is your task to map the individual bits received from the `data` wire to this record type.

The inputs `clk` and `res_n` (active-low reset) have their usual function.
The `rumble` input controls the `R` bit in the polling command sent to the controller.

Note that from the point of view of the FPGA the `data` signal is asynchronous.
Therefore, it must be synchronized before it can be processed by your FSM.
Instantiate a synchronizer (see [libs/sync](../../../libs/sync/doc.md)) for this signal.

When you pass the `data` signal to the synchronizer be sure to first preprocess it with the function `to_X01`.
This has no impact on logic synthesis, but is important for the simulation, as will be explained below.


The meanings of the generics are listed in the table below:

| Name              | Description                                                                |
| ----------------- | -------------------------------------------------------------------------- |
| `SYNC_STAGES`     | Number of flip-flop stages used for the synchronization of the data signal |
| `CLK_FREQ`        | Actual frequency of the clk signal given in Hz                             |
| `REFRESH_TIMEOUT` | The timeout in clk cycles the controller should wait in                    |


## Testbench

The file [`tb/gc_ctrl_tb.vhd`](tb/gc_ctrl_tb.vhd) provides a suitable template for your testbench.
The testbench already defines the procedures `send_gc_cntrl_state` and `read_polling_command` that you have to implement in order to communicate with the UUT.

One thing you have to take special care of is how you handle the pull-up resistor in simulation.
This is exactly where the properties of the `std_logic` come into play.
Recall that in contrast to the `std_ulogic` data type (the `u` standing for unresolved), `std_logic` is a resolved type, which means that multiple drivers on a signal can be resolved (although the result of this operation can be `'X'`, e.g., in case of two drivers with `'0'` and `'1'`).
You can use this property in your testbench.
Add a concurrent signal assignment (i.e., outside of all processes) in your testbench's architecture, that constantly drives the `data` signal to weak high.
This way you can then pull the signal low by writing `'0'` to it in the main stimulus process.
Writing `'Z'` will result in a weak high signal level because of the concurrent signal assignment.
This is the reason why the data signal must be passed to the `to_X01` function, because this function converts the weak high signal level to `'1'`.

If your are interested in how this is implemented, you can look at the implementation of the [`std_logic_1164`](https://opensource.ieee.org/vasg/Packages/-/blob/release/ieee/std_logic_1164-body.vhdl?ref_type=heads#L64) package.

Generate 64 random test values and check if the `ctrl_state` output of the UUT matches the value supplied to the `send_gc_cntrl_state` procedure.
Also, verify that the polling command is correct!
The `rumble` input of the UUT shall also be set to a random value for each transmission.

Use the waveform viewer to verify that the timing of the `data` signal.
For the simulation you can set `REFRESH_TIMEOUT` to a small value.

## Hardware Test

Add an instance of the `gc_ctrl` entity to the top-level architecture in [`top_arch_gc_ctrl.vhd`](top_arch_gc_ctrl.vhd).

The figure below shows how the controller needs to be connected to the board's GPIO connector.

![GPIO board connector pinout (top view)](.mdata/gpio_board_connector_pinout.svg)

Assign each **digital** record entry in of the `ctrl_state` output to its own LED. 
Display the press strength for the left and right triggers on `hex7-6` and `hex5-4`.

Use `hex3-2` and `hex1-0` to display for X and Y displacements of the joy sticks respectively.
Use a switch to switch between which joystick is shown on the seven-segment displays 3-0.

Show that your controller is able to detect each button and is able to read the analog values of the triggers and joysticks of the GameCube controller correctly.
