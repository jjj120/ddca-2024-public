library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.gc_ctrl_pkg.all;
    use work.tb_util_pkg.all;

entity gc_ctrl_tb is
end entity;

architecture bench of gc_ctrl_tb is
    constant CLK_PERIOD : time := 20 ns;
    constant TIME_LOW   : time := 3 us;
    constant TIME_HIGH  : time := 1 us;
    constant TIME_READ  : time := 2 us;

    signal clk, res_n : std_logic := '0';
    signal gc_data    : std_logic := 'Z';
    signal rumble     : std_logic := '0';
    signal ctrl_state : gc_ctrl_state_t;

    signal clk_read : std_logic := '0';
    signal fin      : std_logic := '0';

    shared variable rnd : random_t;
begin

    uut: entity work.gc_ctrl
        generic map (
            CLK_FREQ        => 50_000_000,
            SYNC_STAGES     => 2,
            REFRESH_TIMEOUT => 1000
        )
        port map (
            clk        => clk,
            res_n      => res_n,
            data       => gc_data,
            ctrl_state => ctrl_state,
            rumble     => rumble
        );

    gc_data <= 'H'; -- simulate pull up resistor

    -- Stimulus process

    stimulus: process
        procedure write_bit_to_data(data : std_logic) is
        begin
            gc_data <= '0';

            if data then
                wait for TIME_HIGH;
            else
                wait for TIME_LOW;
            end if;

            gc_data <= 'Z';

            if data then
                wait for TIME_LOW;
            else
                wait for TIME_HIGH;
            end if;
        end procedure;

        procedure read_from_data(read_bit : out std_logic) is
        begin
            clk_read <= not clk_read; -- for testing
            wait for TIME_READ;
            read_bit := gc_data;
            clk_read <= not clk_read; -- for testing
            wait for TIME_READ;
        end procedure;

        procedure send_gc_cntrl_state(data : gc_ctrl_state_t) is
            variable bits : std_logic_vector(63 downto 0);
        begin
            bits := to_slv(data);
            -- this procedure simulates the response of the actual controller
            -- only set gc_data to either '0' or 'Z'
            for i in bits'range loop
                write_bit_to_data(bits(i));
            end loop;
            write_bit_to_data('1'); -- stop bit

        end procedure;

        procedure read_polling_command(data : out std_logic_vector(0 to 24)) is
        begin
            -- this is procedure is used to read the polling command produced by the UUT
            -- the first thing this procedure need to do is wait for gc_data to become '0'
            if gc_data /= '0' then
                wait until gc_data = '0';
            end if;

            for i in data'range loop
                read_from_data(data(i));
            end loop;
        end procedure;

        variable test_data    : gc_ctrl_state_t;
        variable poll_command : std_logic_vector(0 to 24);

        procedure test is
        begin
            wait until falling_edge(gc_data);
            read_polling_command(poll_command);
            assert poll_command = "0H000000000000HH00000000H" report "Polling command " & to_string(poll_command) & " not correct for no rumble";

            wait for 1000 * CLK_PERIOD;
            send_gc_cntrl_state(test_data);
            assert ctrl_state = test_data report "ctrl_state not correct: \n" & to_string(ctrl_state);

            wait for 1000 * CLK_PERIOD;
        end procedure;

    begin
        report "simulation start";

        test_data := to_gc_ctrl_state(rnd.gen_slv_01(64)); --generate random test data
        report to_string(test_data);

        res_n <= '0';
        rumble <= '0';

        wait until rising_edge(clk);
        res_n <= '1';

        wait until rising_edge(clk);

        for i in 0 downto 0 loop
            test;
        end loop;

        fin <= '1';
        report "simulation end";
        -- End simulation
        wait;
    end process;

    clock: process is
    begin
        clk <= '1';
        wait for CLK_PERIOD / 2;
        clk <= '0';
        wait for CLK_PERIOD / 2;

        if fin then
            wait;
        end if;
    end process;
end architecture;
