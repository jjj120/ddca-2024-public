library ieee;
    use ieee.std_logic_1164.all;
    use work.gc_ctrl_pkg.all;
    use work.util_pkg.to_segs;

architecture top_arch_gc_ctrl of top is
    signal ctrl_state : gc_ctrl_state_t;
begin

    uut: entity work.gc_ctrl
        -- generic map (
        --     CLK_FREQ        => 50_000_000,
        --     SYNC_STAGES     => 2,
        --     REFRESH_TIMEOUT => 1000
        -- )
        port map (
            clk        => clk,
            res_n      => keys(0),
            data       => gc_data,
            ctrl_state => ctrl_state,
            rumble     => switches(0)
        );

    -- buttons
    ledr(0)  <= ctrl_state.btn_start;
    ledr(1)  <= ctrl_state.btn_y;
    ledr(2)  <= ctrl_state.btn_x;
    ledr(3)  <= ctrl_state.btn_b;
    ledr(4)  <= ctrl_state.btn_a;
    ledr(5)  <= ctrl_state.btn_l;
    ledr(6)  <= ctrl_state.btn_r;
    ledr(7)  <= ctrl_state.btn_z;
    ledr(8)  <= ctrl_state.btn_up;
    ledr(9)  <= ctrl_state.btn_down;
    ledr(10) <= ctrl_state.btn_right;
    ledr(11) <= ctrl_state.btn_left;

    -- analog triggers
    hex7 <= to_segs(ctrl_state.trigger_l(7 downto 4));
    hex6 <= to_segs(ctrl_state.trigger_l(3 downto 0));
    hex5 <= to_segs(ctrl_state.trigger_r(7 downto 4));
    hex4 <= to_segs(ctrl_state.trigger_r(3 downto 0));

    -- joysticks
    hex3 <= to_segs(ctrl_state.joy_x(7 downto 4)) when switches(1) else
            to_segs(ctrl_state.c_x(7 downto 4));

    hex2 <= to_segs(ctrl_state.joy_x(3 downto 0)) when switches(1) else
            to_segs(ctrl_state.c_x(3 downto 0));

    hex1 <= to_segs(ctrl_state.joy_y(7 downto 4)) when switches(1) else
            to_segs(ctrl_state.c_y(7 downto 4));

    hex0 <= to_segs(ctrl_state.joy_y(3 downto 0)) when switches(1) else
            to_segs(ctrl_state.c_y(3 downto 0));

end architecture;
