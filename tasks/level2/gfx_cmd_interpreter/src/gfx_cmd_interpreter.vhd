library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.math_real.all;

    use std.textio.all;
    use ieee.std_logic_textio.all;

    use work.math_pkg.all;
    use work.tb_util_pkg.all;
    use work.gfx_cmd_pkg.all;

entity gfx_cmd_interpreter is
    generic (
        OUTPUT_DIR : string := "./"
    );
    port (
        clk            : in  std_logic;
        gfx_cmd        : in  std_logic_vector(GFX_CMD_WIDTH - 1 downto 0);
        gfx_cmd_wr     : in  std_logic;
        gfx_frame_sync : out std_logic;
        gfx_rd_data    : out std_logic_vector(15 downto 0);
        gfx_rd_valid   : out std_logic := '0'
    );
end entity;

architecture arch of gfx_cmd_interpreter is

    shared variable vram : vram_t;

    type bitmap_descriptor_t is record
        base_address : natural;
        width        : natural;
        height       : natural;
    end record;

    type bitmap_descriptor_vector is array (natural range <>) of bitmap_descriptor_t;

    type coord_t is record
        x, y : integer;
    end record;

    type color_tuple_t is record
        primary, secondary : color_t;
    end record;

    -- Checks if pixel at x, y is inside bitmap b's bounds
    function is_oob(b : bitmap_descriptor_t; c : coord_t) return boolean is
    begin
        return (c.x >= 0 and c.x < b.width) and (c.y >= 0 and c.y < b.height);
    end function;

    -- Check if in bounds and set pixel
    procedure set_pixel(
            b          : bitmap_descriptor_t;
            coord      : coord_t;
            cs         : std_logic;
            color      : color_tuple_t;
            alpha_mode : boolean := false
        ) is
        variable vram_addr   : natural;
        variable alpha_color : color_t;
        variable color_main  : color_t;
    begin
        if not is_oob(b, coord) then
            return;
        end if;

        vram_addr := b.base_address + coord.y * b.width + coord.x;
        if cs = CS_PRIMARY then
            color_main := color.primary;
            alpha_color := color.secondary;
        else
            color_main := color.secondary;
            alpha_color := color.primary;
        end if;

        if not alpha_mode then
            vram.set_byte(vram_addr, color_main);
        elsif (vram.get_byte(vram_addr) /= alpha_color) then
            vram.set_byte(vram_addr, color_main);
        end if;
    end procedure;

    -- TODO: Add further functions, procedures, types and signals to increase readability and reduce code duplication

begin

    init_vram: process
    begin
        vram.init(21);
        wait;
    end process;

    test: process
        -- variables
        variable vram_addr : natural;
        variable m         : std_logic;
        variable n         : std_logic_vector(15 downto 0);
        variable cs        : std_logic; -- color select
        variable mx, my    : std_logic; -- flags if coords should be incremented

        variable bmpidx     : bmpidx_t;                                                 -- index of bitmap to save
        variable bmpidx_int : integer;                                                  -- index of bitmap to save as integer
        variable abd        : bitmap_descriptor_t;                                      -- currently active bitmap
        variable bdt        : bitmap_descriptor_vector(2 ** WIDTH_BMPIDX - 1 downto 0); -- array of all bitmaps
        variable gp         : coord_t;                                                  -- graphic pointer (current pixel)
        variable curr_color : color_tuple_t;                                            -- currently selected colors

        variable maskop : maskop_t;
        variable mask   : mask_t;

        variable out_counter : integer := 0; -- counter for the out filename

        -- procedures
        procedure wait_next_cmd is
        begin
            loop
                wait until rising_edge(clk);
                gfx_rd_valid <= '0';
                gfx_frame_sync <= '0';

                if (gfx_cmd_wr = '1') then
                    return;
                else
                    next;
                end if;
            end loop;
        end procedure;

        procedure read_vram_address(addr : out natural) is
            -- does not wait after reading!
        begin
            wait_next_cmd;
            addr := to_integer(unsigned(gfx_cmd));
            wait_next_cmd;
            addr := addr + 2 ** 16 * to_integer(unsigned(gfx_cmd));
        end procedure;

        procedure draw_hline is
            variable dx : unsigned(15 downto 0);
        begin
            wait_next_cmd;
            dx := unsigned(gfx_cmd);

            for x in gp.x to (gp.x + to_integer(dx) - 1) loop
                set_pixel(abd,(x, gp.y), cs, curr_color);
            end loop;

            if mx then
                gp.x := gp.x + to_integer(dx) - 1;
            end if;
            if my then
                gp.y := gp.y + 1;
            end if;
        end procedure;

        procedure draw_vline is
            variable dy : unsigned(15 downto 0);
        begin
            wait_next_cmd;
            dy := unsigned(gfx_cmd);

            for y in gp.y to (gp.y + to_integer(dy) - 1) loop
                set_pixel(abd,(gp.x, y), cs, curr_color);
            end loop;

            if mx then
                gp.x := gp.x + 1;
            end if;
            if my then
                gp.y := gp.y + to_integer(dy) - 1;
            end if;
        end procedure;

        pure function edge_in(v1, v2, p : coord_t) return boolean is
        begin
            return ((p.x - v1.x) * (v2.y - v1.y) - (p.y - v1.y) * (v2.x - v1.x) >= 0);
        end function;

        pure function triangle_in(v1, v2, v3, p : coord_t) return boolean is
        begin
            return edge_in(v1, v2, p) and edge_in(v2, v3, p) and edge_in(v3, v1, p);
        end function;

        procedure draw_triangle is
            variable mxs    : mxs_t;
            variable mys    : mys_t;
            variable v1, v2 : coord_t;
        begin
            mxs := get_mxs(gfx_cmd);
            mys := get_mys(gfx_cmd);

            wait_next_cmd;
            v1.x := to_integer(signed(gfx_cmd));
            wait_next_cmd;
            v1.y := to_integer(signed(gfx_cmd));
            wait_next_cmd;
            v2.x := to_integer(signed(gfx_cmd));
            wait_next_cmd;
            v2.y := to_integer(signed(gfx_cmd));

            for x in 0 to abd.width - 1 loop
                for y in 0 to abd.width - 1 loop
                    if triangle_in(gp, v1, v2,(x, y)) then
                        set_pixel(abd,(x, y), cs, curr_color);
                    end if;
                end loop;
            end loop;

            if mxs = "01" then
                gp.x := v1.x;
            elsif mxs = "11" then
                gp.x := v2.x;
            end if;

            if mys = "01" then
                gp.y := v1.y;
            elsif mys = "11" then
                gp.y := v2.y;
            end if;
        end procedure;

        function transform_color(color : color_t; mask_val : mask_t; maskop_val : maskop_t) return color_t is
        begin
            case maskop_val is
                when "00" => return color;
                when "01" => return color and mask_val;
                when "10" => return color or mask_val;
                when "11" => return color xor mask_val;
                when others => return color;
            end case;
        end function;

        procedure copy_pixel(bmp_from, bmp_to : bitmap_descriptor_t; coord_from, coord_to : coord_t; alpha_mode : std_logic) is
            variable from_addr, to_addr : integer;
            variable saved_color        : color_t;
        begin
            from_addr := bmp_from.base_address + coord_from.y * bmp_from.width + coord_from.x;
            to_addr := bmp_to.base_address + coord_to.y * bmp_to.width + coord_to.x;

            if is_oob(bmp_from, coord_from) and is_oob(bmp_to, coord_to) then
                saved_color := vram.get_byte(from_addr);

                if alpha_mode = '1' and saved_color = curr_color.secondary then
                    -- ignore alpha channel color
                    return;
                end if;

                saved_color := transform_color(saved_color, mask, maskop);
                vram.set_byte(to_addr, saved_color);
            end if;
        end procedure;

        procedure bb_clip(
                am                    : std_logic;
                rot                   : rot_t;
                coord                 : coord_t;
                bmp_width, bmp_height : integer
            ) is
            variable to_copy   : bitmap_descriptor_t;
            variable dx, dy    : integer;
            variable new_coord : coord_t;
        begin
            to_copy := bdt(bmpidx_int);

            for x_coord in 0 to bmp_width - 1 loop
                for y_coord in 0 to bmp_height - 1 loop
                    if rot = ROT_R0 then
                        -- no rotation, just copy
                        new_coord := (gp.x + x_coord, gp.y + y_coord);
                    elsif rot = ROT_R90 then
                        -- 90° clockwise
                        new_coord := (gp.x + bmp_height - 1 - y_coord, gp.y + x_coord);
                    elsif rot = ROT_R180 then
                        -- 180°
                        new_coord := (gp.x + bmp_width - 1 - x_coord, gp.y + bmp_height - 1 - y_coord);
                    elsif rot = ROT_R270 then
                        -- 90° counter clockwise
                        new_coord := (gp.x + y_coord, gp.y + bmp_width - 1 - x_coord);
                    end if;

                    copy_pixel(to_copy, abd,(coord.x + x_coord, coord.y + y_coord), new_coord, am);
                end loop;
            end loop;

            -- increment at the end
            if rot = "00" or rot = "10" then
                dx := bmp_width;
                dy := bmp_height;
            else
                dx := bmp_height;
                dy := bmp_width;
            end if;

            if mx then
                gp.x := gp.x + dx;
            end if;

            if my then
                gp.y := gp.y + dy;
            end if;
        end procedure;

        procedure exec_bb_clip is
            variable am                    : std_logic;
            variable rot                   : rot_t;
            variable coord                 : coord_t;
            variable bmp_width, bmp_height : std_logic_vector(14 downto 0);
        begin
            am := gfx_cmd(INDEX_AM);
            rot := get_rot(gfx_cmd);

            wait_next_cmd;
            coord.x := to_integer(signed(gfx_cmd(14 downto 0)));
            wait_next_cmd;
            coord.y := to_integer(signed(gfx_cmd(14 downto 0)));
            wait_next_cmd;
            bmp_width := gfx_cmd(14 downto 0);
            wait_next_cmd;
            bmp_height := gfx_cmd(14 downto 0);

            bb_clip(am, rot, coord, to_integer(unsigned(bmp_width)), to_integer(unsigned(bmp_height)));
        end procedure;

        procedure bb_char is
            variable am        : std_logic;
            variable rot       : rot_t;
            variable xoffset   : std_logic_vector(9 downto 0);
            variable charwidth : std_logic_vector(5 downto 0);
        begin
            am := gfx_cmd(INDEX_AM);
            rot := get_rot(gfx_cmd);

            wait_next_cmd;
            xoffset := gfx_cmd(15 downto 6);
            charwidth := gfx_cmd(5 downto 0);

            bb_clip(am, rot,(to_integer(unsigned(xoffset)), 0), to_integer(unsigned(charwidth)), bdt(bmpidx_int).height);
        end procedure;

        procedure bb_full is
            variable am  : std_logic;
            variable rot : rot_t;
        begin
            am := gfx_cmd(INDEX_AM);
            rot := get_rot(gfx_cmd);

            bb_clip(am, rot,(0, 0), bdt(bmpidx_int).width, bdt(bmpidx_int).height);
        end procedure;
    begin
        gfx_frame_sync <= '0';
        gfx_rd_data <= (others => '0');

        loop
            wait_next_cmd;
            m := gfx_cmd(INDEX_M);
            bmpidx := get_bmpidx(gfx_cmd);
            bmpidx_int := to_integer(unsigned(bmpidx));
            cs := gfx_cmd(INDEX_CS);
            mx := gfx_cmd(INDEX_MX);
            my := gfx_cmd(INDEX_MY);

            case get_opcode(gfx_cmd) is
                when OPCODE_NOP =>
                    -- report "OPCODE_NOP";
                    -- do nothing
                    --
                    ------------------------------------------------------------
                    ----------------------  VRAM COMMANDS  ---------------------
                    ------------------------------------------------------------
                when OPCODE_VRAM_READ =>
                    -- report "Got OPCODE_VRAM_READ";
                    read_vram_address(vram_addr);

                    if (m = M_BYTE) then
                        gfx_rd_data <= (7 downto 0 => vram.get_byte(vram_addr), others => '0');
                    else
                        gfx_rd_data <= vram.get_word(vram_addr);
                    end if;
                    gfx_rd_valid <= '1';

                when OPCODE_VRAM_WRITE =>
                    -- report "Got OPCODE_VRAM_WRITE";
                    read_vram_address(vram_addr);

                    wait_next_cmd;
                    if (m = M_BYTE) then
                        vram.set_byte(vram_addr, gfx_cmd(7 downto 0));
                    else
                        assert vram_addr mod 2 = 0 report "vram_addr " & to_string(vram_addr) & " is not even!";
                        vram.set_word(vram_addr, gfx_cmd(15 downto 0));
                    end if;

                when OPCODE_VRAM_WRITE_SEQ =>
                    -- report "Got OPCODE_VRAM_WRITE_SEQ";
                    wait_next_cmd;
                    n := gfx_cmd;
                    read_vram_address(vram_addr);

                    for i in 0 to to_integer(unsigned(n)) - 1 loop
                        if (m = M_BYTE) then
                            wait_next_cmd;
                            vram.set_byte(vram_addr + i, gfx_cmd(7 downto 0));
                        else
                            wait_next_cmd;
                            assert (vram_addr + 2 * i) mod 2 = 0
                                report "word access only on even addresses, seq_write failed on " & to_string(vram_addr + 2 * i) & " with start addr " & to_string(vram_addr);
                            vram.set_word(vram_addr + 2 * i, gfx_cmd);
                        end if;
                    end loop;

                when OPCODE_VRAM_WRITE_INIT =>
                    -- report "Got OPCODE_VRAM_WRITE_INIT";
                    wait_next_cmd;
                    n := gfx_cmd;
                    read_vram_address(vram_addr);
                    wait_next_cmd;

                    for i in 0 to (to_integer(unsigned(n)) - 1) loop
                        if (m = M_BYTE) then
                            vram.set_byte(vram_addr + i, gfx_cmd(7 downto 0));
                        else
                            assert (vram_addr + 2 * i) mod 2 = 0 report "vram_addr " & to_string((vram_addr + 2 * i)) & " is not even!";
                            vram.set_word(vram_addr + 2 * i, gfx_cmd);
                        end if;
                    end loop;

                    -------------------------------------------------------------
                    -----------------------  BMP COMMANDS  ----------------------
                    -------------------------------------------------------------
                when OPCODE_DEFINE_BMP =>
                    -- report "Got OPCODE_DEFINE_BMP";
                    read_vram_address(vram_addr);
                    bdt(bmpidx_int).base_address := vram_addr;

                    wait_next_cmd;
                    bdt(bmpidx_int).width := to_integer(unsigned(gfx_cmd(14 downto 0)));

                    wait_next_cmd;
                    bdt(bmpidx_int).height := to_integer(unsigned(gfx_cmd(14 downto 0)));

                    -- report "Defined Bitmap: base_addr: " & to_string(bdt(to_integer(unsigned(bmpidx))).base_address) & ", width: " & to_string(bdt(to_integer(unsigned(bmpidx))).width) & ", height: " & to_string(bdt(to_integer(unsigned(bmpidx))).height);
                when OPCODE_ACTIVATE_BMP =>
                    -- report "Got OPCODE_ACTIVATE_BMP";
                    abd := bdt(bmpidx_int);

                when OPCODE_DISPLAY_BMP =>
                    -- report "Got OPCODE_DISPLAY_BMP";
                    report "Printing out Bitmap to " & OUTPUT_DIR & to_string(out_counter) & ".ppm" & ": base_addr: " & to_string(bdt(to_integer(unsigned(bmpidx))).base_address) & ", width: " & to_string(bdt(to_integer(unsigned(bmpidx))).width) & ", height: " & to_string(bdt(to_integer(unsigned(bmpidx))).height);
                    if gfx_cmd(INDEX_FS) then
                        -- TODO: Only assert for a single clock cycle
                        gfx_frame_sync <= '1';
                    end if;

                    vram.dump_bitmap(
                        bdt(bmpidx_int).base_address,
                        bdt(bmpidx_int).width,
                        bdt(bmpidx_int).height,
                        OUTPUT_DIR & to_string(out_counter) & ".ppm"
                    );
                    out_counter := out_counter + 1;
                    -------------------------------------------------------------
                    ------------------  GP and COLOR COMMANDS  ------------------
                    -------------------------------------------------------------
                when OPCODE_MOVE_GP =>
                    -- report "Got OPCODE_MOVE_GP";
                    if gfx_cmd(INDEX_REL) then
                        -- relative
                        wait_next_cmd;
                        gp.x := gp.x + to_integer(signed(gfx_cmd));
                        wait_next_cmd;
                        gp.y := gp.y + to_integer(signed(gfx_cmd));
                    else
                        -- absolute
                        wait_next_cmd;
                        gp.x := to_integer(unsigned(gfx_cmd));
                        wait_next_cmd;
                        gp.y := to_integer(unsigned(gfx_cmd));
                    end if;

                    -- report "Moving gp to (" & to_string(gp.x) & ", " & to_string(gp.x) & ")";
                when OPCODE_INC_GP =>
                    -- report "Got OPCODE_INC_GP";
                    if gfx_cmd(INDEX_DIR) then
                        -- direction x
                        gp.y := gp.y + to_integer(signed(get_incvalue(gfx_cmd)));
                    else
                        -- direction y
                        gp.x := gp.x + to_integer(signed(get_incvalue(gfx_cmd)));
                    end if;

                when OPCODE_SET_COLOR =>
                    -- report "Got OPCODE_SET_COLOR";
                    if gfx_cmd(INDEX_CS) = CS_PRIMARY then
                        curr_color.primary := get_color(gfx_cmd);
                    else
                        curr_color.secondary := get_color(gfx_cmd);
                    end if;

                    ------------------------------------------------------------
                    ----------------------  DRAW COMMANDS  ---------------------
                    ------------------------------------------------------------
                when OPCODE_CLEAR =>
                    -- report "Got OPCODE_CLEAR";
                    for x in 0 to abd.width - 1 loop
                        for y in 0 to abd.width - 1 loop
                            set_pixel(abd,(x, y), cs, curr_color);
                        end loop;
                    end loop;

                when OPCODE_SET_PIXEL =>
                    -- report "Got OPCODE_SET_PIXEL";
                    set_pixel(abd, gp, cs, curr_color);
                    if mx then
                        gp.x := gp.x + 1;
                    end if;
                    if my then
                        gp.y := gp.y + 1;
                    end if;

                when OPCODE_DRAW_HLINE =>
                    -- report "Got OPCODE_DRAW_HLINE";
                    draw_hline;

                when OPCODE_DRAW_VLINE =>
                    -- report "Got OPCODE_DRAW_VLINE";
                    draw_vline;

                when OPCODE_DRAW_TRIANGLE =>
                    -- report "Got OPCODE_DRAW_TRIANGLE";
                    draw_triangle;

                when OPCODE_GET_PIXEL =>
                    -- report "Got OPCODE_GET_PIXEL";
                    vram_addr := abd.base_address + gp.y * abd.width + gp.x;
                    gfx_rd_data <= (7 downto 0 => vram.get_byte(vram_addr), others => '0');
                    gfx_rd_valid <= '1';

                    if is_oob(abd, gp) then
                        gfx_rd_data <= (others => '1');
                    end if;

                    if mx then
                        gp.x := gp.x + 1;
                    end if;
                    if my then
                        gp.y := gp.y + 1;
                    end if;

                    ------------------------------------------------------------
                    -----------------------  BB COMMANDS  ----------------------
                    ------------------------------------------------------------
                when OPCODE_SET_BB_EFFECT =>
                    -- report "Got OPCODE_SET_BB_EFFECT";
                    maskop := get_maskop(gfx_cmd);
                    mask := get_mask(gfx_cmd);

                when OPCODE_BB_FULL =>
                    -- report "Got OPCODE_BB_FULL";
                    bb_full;

                when OPCODE_BB_CHAR =>
                    -- report "Got OPCODE_BB_CHAR";
                    bb_char;

                when OPCODE_BB_CLIP =>
                    -- report "Got OPCODE_BB_CLIP";
                    exec_bb_clip;

                when others => report "unknown instruction: " & to_hstring(gfx_cmd) severity error;
            end case;
        end loop;
        wait;
    end process;
end architecture;
