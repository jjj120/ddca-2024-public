
# Graphics Command Interpreter
**Points:** 2 ` | ` **Keywords:** Testbench, Protected Type

The purpose of the `gfx_cmd_interpreter` is to implement a **simulation** model that provides the same interface as the [`vga_gfx_ctrl`](../../../lib/vga_gfx_ctrl/doc.md).
This allows to test modules that interface with the `vga_gfx_ctrl` by simulating them while connected to the `gfx_cmd_interpreter` and dumping the frames as images on the computer running the testbench.
This comes in quite handy for testing modules using the `vga_gfx_ctrl`.
For example, just for the initialization of the `snake` game hundreds of data words are transferred to the graphics controller.
It is therefore not very effective to find bugs by just looking at the graphics command stream.

Note that a consequence of the initial statement is that this module is **not** meant to be synthesizable.
Therefore, there is no need to implement a classical state machine, as you would for "real" hardware, in order to provide the required functionality.

**Important**: Before you continue reading this task description, you should consult the [`vga_gfx_ctrl`](../../../lib/vga_gfx_ctrl/doc.md) documentation to get an overview of the graphics controller interface and its functionality.
You should also have a look at the [`gfx_cmd_pkg`](../../../lib/vga_gfx_ctrl/src/gfx_cmd_pkg.vhd), as it contains important constants and functions that will help you with your implementation.

You are already provided with a [template](./src/gfx_cmd_interpreter.vhd) for the interpreter containing some very basic code and procedure / function skeletons.
You do not have to use the provided code, but we strongly suggest you to.
The interface of the `gfx_cmd_interpreter` entity is

```vhdl
entity gfx_cmd_interpreter is
	generic (
		OUTPUT_DIR : string := "./"
	);
	port (
		clk            : in std_logic;
		gfx_cmd        : in std_logic_vector(GFX_CMD_WIDTH-1 downto 0);
		gfx_cmd_wr     : in std_logic;
		gfx_frame_sync : out std_logic;
		gfx_rd_data    : out std_logic_vector(15 downto 0);
		gfx_rd_valid   : out std_logic
	);
end entity;
```

The `gfx_cmd_interpreter` should simply receive graphics commands via the `gfx_*` signals, and **immediately** execute them by performing the appropriate graphical operations on an internal data structure representing the VRAM used by the `vga_gfx_ctrl`.
This data structure is implemented by the [protected type](https://fpgatutorial.com/vhdl-shared-variable-protected-type/) `vram_t` defined in the [tb_util_pkg](../../../lib/tb_util/src/tb_util_pkg.vhd) package.
Protected types in VHDL are comparable to classes in other programming languages.
However, you do not really need to fully understand this code or the concept of protected types in order to use it.
The code in the template already shows how to integrate it into your `gfx_cmd_interpreter`.

The `vram_t` type features functions to read and write bytes and words from and to VRAM.
Furthermore, it contains a function to dump an image in VRAM to a [PPM image file](https://en.wikipedia.org/wiki/Netpbm).
Note that since PPM is a text based-format, the resulting images can be quite large.

Your `gfx_cmd_interpreter` should support **all** commands supported by the `vga_gfx_ctrl` as given in its [documentation](../../../lib/vga_gfx_ctrl/doc.md).
For the `DRAW_TRIANGLE` command you can implement a simple rasterizing algorithm.
You can find a C++ version of this algorithm, which you can use as a basis for your implementation, at the bottom of this [webpage](https://www.scratchapixel.com/lessons/3d-basic-rendering/rasterization-practical-implementation/rasterization-stage.html).
**Important**: As defined in the `vga_gfx_ctrl`'s [documentation](../../../lib/vga_gfx_ctrl/doc.md), implement the **counter-clockwise** version of the algorithm (this is only important for the edge function).

The `gfx_cmd_interpreter`'s main purpose is to dump frames as images.
This is achieved by dumping the bitmap identified by the `bmpidx` field to a file whenever the `gfx_cmd_interpreter` reads a `DISPLAY_BMP` command.
Also, if the `fs` flag field of the `DISPLAY_BMP` is set, your interpreter shall assert its `gfx_frame_sync` output for a single clock cycle.

Name the dumped frame images as `[N].ppm`, where `[N]` is a number increased with every frame that is dumped (beginning with 0, i.e., the first dumped frame shall be named `0.ppm`).
Your interpreter shall place the created image files in the directory specified by the `OUTPUT_DIR` generic.
Note that, while the `vga_gfx_ctrl` is restricted to bitmaps with a size of 320x240 pixels, the interpreter is not!
The interpreter should be capable of dumping images of arbitrary dimensions.

**Important**:
- Don't hard-code constants in your code!
- Try to make use of functions and procedures, as this will significantly simplify your code (especially try to evade redundant code).

As a primary goal of this task is writing clean and concise code, we might reject your code, although functionally correct, if it resembles spaghetti code or contains too much duplicate code.

## Suggested Implemenation Order

- Develop the interpreter and its testbench in parallel, i.e., whenever you implement a specific command, add testcode to your testbench to check if it works correctly.
- Start out by completing the `VRAM_*` commands. The effects of a write to VRAM can be checked by the read operation.
- Once this works, implement `DEFINE_BMP`, `ACTIVATE_BMP` and `DISPLAY_BMP` (the provided one is not complete!).
- Next, implement the simple drawing commands (like `SET_PIXEL`, `CLEAR` etc.) and the commands used to access the graphics pointer.
- Now, you can implement the advanced drawing commanded (`DRAW_HLINE`, `DRAW_VLINE`, `DRAW_TRIANGLE`).
- Finally, take care of the bit blit commands.

## Testing
Create a testbench to see if **all** the graphics commands are executed correctly (i.e., testbench shall execute each command at least once).
Use the provided [template](./tb/gfx_cmd_interpreter_tb.vhd).


