library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.gfx_cmd_pkg.all;

entity gfx_cmd_interpreter_tb is
end entity;

architecture bench of gfx_cmd_interpreter_tb is
    constant CLK_PERIOD : time   := 20 ns;
    constant OUTPUT_DIR : string := "./images/";
    signal clk            : std_logic;
    signal gfx_cmd        : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0);
    signal gfx_cmd_wr     : std_logic;
    signal gfx_frame_sync : std_logic;
    signal gfx_rd_data    : std_logic_vector(15 downto 0);
    signal gfx_rd_valid   : std_logic;

    signal fin : std_logic := '0';

begin

    testing: process is
        procedure write_command(command : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0)) is
        begin
            gfx_cmd <= command;
            gfx_cmd_wr <= '1';
            wait until rising_edge(clk);
            gfx_cmd_wr <= '0';
        end procedure;

        procedure write_addr(addr : natural) is
            variable addr_vec : std_logic_vector(20 downto 0);
        begin
            addr_vec := std_logic_vector(to_unsigned(addr, 21));
            write_command(addr_vec(15 downto 0));
            write_command((4 downto 0 => addr_vec(20 downto 16), others => '0'));
        end procedure;

        procedure NOP is
        begin
            write_command(create_gfx_instr(OPCODE_NOP));
        end procedure;

        procedure MOVE_GP(rel : std_logic; x, y : integer) is
        begin
            write_command(create_gfx_instr(OPCODE_MOVE_GP, rel => rel));
            write_command(std_logic_vector(to_signed(x, GFX_CMD_WIDTH)));
            write_command(std_logic_vector(to_signed(y, GFX_CMD_WIDTH)));
        end procedure;

        procedure INC_GP(dir : std_logic; incvalue : incvalue_t) is
        begin
            write_command(create_gfx_instr(OPCODE_INC_GP, dir => dir, incvalue => incvalue));
        end procedure;

        procedure CLEAR(cs : std_logic) is
        begin
            write_command(create_gfx_instr(OPCODE_CLEAR, cs => cs));
        end procedure;

        procedure SET_PIXEL(cs, mx, my : std_logic) is
        begin
            write_command(create_gfx_instr(OPCODE_SET_PIXEL, cs => cs, mx => mx, my => my));
        end procedure;

        procedure DRAW_HLINE(cs, mx, my : std_logic; dx : integer) is
        begin
            write_command(create_gfx_instr(OPCODE_DRAW_HLINE, cs => cs, mx => mx, my => my));
            write_command(std_logic_vector(to_signed(dx, GFX_CMD_WIDTH)));
        end procedure;

        procedure DRAW_VLINE(cs, mx, my : std_logic; dy : integer) is
        begin
            write_command(create_gfx_instr(OPCODE_DRAW_VLINE, cs => cs, mx => mx, my => my));
            write_command(std_logic_vector(to_signed(dy, GFX_CMD_WIDTH)));
        end procedure;

        procedure DRAW_TRIANGLE(cs : std_logic; mxs, mys : std_logic_vector(1 downto 0); x1, y1, x2, y2 : integer) is
        begin
            write_command(create_gfx_instr(OPCODE_DRAW_TRIANGLE, cs => cs, mxs => mxs, mys => mys));
            write_command(std_logic_vector(to_unsigned(x1, GFX_CMD_WIDTH)));
            write_command(std_logic_vector(to_unsigned(y1, GFX_CMD_WIDTH)));
            write_command(std_logic_vector(to_unsigned(x2, GFX_CMD_WIDTH)));
            write_command(std_logic_vector(to_unsigned(y2, GFX_CMD_WIDTH)));
        end procedure;

        procedure GET_PIXEL(mx, my : std_logic) is
        begin
            write_command(create_gfx_instr(OPCODE_GET_PIXEL, mx => mx, my => my));
        end procedure;

        procedure VRAM_READ(m : std_logic; addr : natural) is
        begin
            write_command(create_gfx_instr(OPCODE_VRAM_READ, m => m));
            write_addr(addr);
        end procedure;

        procedure VRAM_WRITE(m : std_logic; addr : natural; data : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0)) is
        begin
            write_command(create_gfx_instr(OPCODE_VRAM_READ, m => m));
            write_addr(addr);
            write_command(data);
        end procedure;

        type data_vec is array (natural range <>) of std_logic_vector(GFX_CMD_WIDTH - 1 downto 0);
        procedure VRAM_WRITE_SEQ(m : std_logic; addr : natural; data : data_vec) is
        begin
            write_command(create_gfx_instr(OPCODE_VRAM_WRITE_SEQ, m => m));
            write_command(std_logic_vector(to_unsigned(data'length, 16)));
            write_addr(addr);
            for i in data'range loop
                write_command(data(i));
            end loop;
        end procedure;

        procedure VRAM_WRITE_INIT(m : std_logic; n, addr : integer; data : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0)) is
        begin
            write_command(create_gfx_instr(OPCODE_VRAM_WRITE_INIT, m => m));
            write_command(std_logic_vector(to_unsigned(n, GFX_CMD_WIDTH)));
            write_addr(addr);
            write_command(data);
        end procedure;

        procedure SET_COLOR(cs : std_logic; color : color_t) is
        begin
            write_command(create_gfx_instr(OPCODE_SET_COLOR, cs => cs, color => color));
        end procedure;

        procedure SET_BB_EFFECT(maskop : maskop_t; mask : mask_t) is
        begin
            write_command(create_gfx_instr(OPCODE_SET_BB_EFFECT, maskop => maskop, mask => mask));
        end procedure;

        subtype bmpidx_t_int is natural range 2 ** 3 - 1 downto 0;
        procedure DEFINE_BMP(bmpidx : bmpidx_t_int; addr : integer; width, height : integer) is
        begin
            write_command(create_gfx_instr(OPCODE_DEFINE_BMP, bmpidx => std_logic_vector(to_unsigned(bmpidx, 3))));
            write_addr(addr);
            write_command('0' & std_logic_vector(to_unsigned(width, GFX_CMD_WIDTH - 1)));
            write_command('0' & std_logic_vector(to_unsigned(height, GFX_CMD_WIDTH - 1)));
        end procedure;

        procedure ACTIVATE_BMP(bmpidx : bmpidx_t_int) is
        begin
            write_command(create_gfx_instr(OPCODE_ACTIVATE_BMP, bmpidx => std_logic_vector(to_unsigned(bmpidx, 3))));
        end procedure;

        procedure DISPLAY_BMP(bmpidx : bmpidx_t_int; fs : std_logic) is
        begin
            write_command(create_gfx_instr(OPCODE_DISPLAY_BMP, bmpidx => std_logic_vector(to_unsigned(bmpidx, 3)), fs => fs));
        end procedure;

        procedure BB_FULL(am : std_logic; rot : rot_t; mx, my : std_logic; bmpidx : bmpidx_t_int) is
        begin
            write_command(create_gfx_instr(OPCODE_BB_FULL, am => am, rot => rot, mx => mx, my => my, bmpidx => std_logic_vector(to_unsigned(bmpidx, 3))));
        end procedure;

        procedure BB_CHAR(am : std_logic; rot : rot_t; mx, my : std_logic; bmpidx : bmpidx_t_int; xoffset : std_logic_vector(9 downto 0); charwidth : std_logic_vector(5 downto 0)) is
        begin
            write_command(create_gfx_instr(OPCODE_BB_CHAR, am => am, rot => rot, mx => mx, my => my, bmpidx => std_logic_vector(to_unsigned(bmpidx, 3))));
            write_command(xoffset & charwidth);
        end procedure;

        procedure BB_CLIP(am : std_logic; rot : rot_t; mx, my : std_logic; bmpidx : bmpidx_t_int; x, y, width, height : integer) is
        begin
            write_command(create_gfx_instr(OPCODE_BB_CLIP, am => am, rot => rot, mx => mx, my => my, bmpidx => std_logic_vector(to_unsigned(bmpidx, 3))));
            write_command('0' & std_logic_vector(to_unsigned(x, GFX_CMD_WIDTH - 1)));
            write_command('0' & std_logic_vector(to_unsigned(y, GFX_CMD_WIDTH - 1)));
            write_command('0' & std_logic_vector(to_unsigned(width, GFX_CMD_WIDTH - 1)));
            write_command('0' & std_logic_vector(to_unsigned(height, GFX_CMD_WIDTH - 1)));
        end procedure;

        procedure test_vram_rw is
        begin
            VRAM_WRITE_INIT('1', 700, 0,(others => '1'));
            VRAM_READ('1', 20);
            wait until rising_edge(gfx_rd_valid);
            assert and gfx_rd_data = '1' report "vram read failed";

            DEFINE_BMP(0, 0, 500, 500);
            DISPLAY_BMP(0, '1');

            wait until rising_edge(clk);
            assert gfx_frame_sync = '1' report "gfx_frame_sync not high!";
        end procedure;

        procedure test_basic_drawing is
        begin
            DEFINE_BMP(0, 0, 500, 500);
            ACTIVATE_BMP(0);
            SET_COLOR(CS_PRIMARY, COLOR_RED);
            SET_COLOR(CS_SECONDARY, 8x"49"); -- dark gray
            CLEAR(CS_SECONDARY);
            MOVE_GP('0', 100, 50);
            DRAW_HLINE(CS_PRIMARY, '1', '0', 320);
            SET_COLOR(CS_PRIMARY, COLOR_BLUE);

            DRAW_VLINE(CS_PRIMARY, '0', '1', 150);
            SET_COLOR(CS_PRIMARY, COLOR_CYAN);
            DRAW_TRIANGLE(CS_PRIMARY, "00", "00", 120, 200, 100, 400);
            DISPLAY_BMP(0, '0');
        end procedure;

        procedure test_bb is
        begin
            DEFINE_BMP(1, 500 * 500, 30, 30);
            ACTIVATE_BMP(1);
            SET_COLOR(CS_PRIMARY, COLOR_RED);
            SET_COLOR(CS_SECONDARY, COLOR_GREEN);

            MOVE_GP('0', 5, 1);
            CLEAR(CS_SECONDARY);
            DRAW_VLINE(CS_PRIMARY, '0', '0', 20);

            ACTIVATE_BMP(0);
            SET_BB_EFFECT("01", COLOR_GREEN);
            BB_FULL('0', ROT_R90, '0', '0', 1);
            MOVE_GP('0', 200, 200);
            SET_BB_EFFECT("10", COLOR_GREEN);
            BB_CLIP('1', ROT_R90, '0', '0', 1, 5, 5, 20, 20);
            DISPLAY_BMP(0, '0');
        end procedure;
    begin
        report "start sim";

        -- test_vram_rw;
        test_basic_drawing;
        test_bb;

        fin <= '1';
        report "sim done";
        wait;
    end process;

    uut: entity work.gfx_cmd_interpreter
        generic map (
            OUTPUT_DIR => OUTPUT_DIR
        )
        port map (
            clk            => clk,
            gfx_cmd        => gfx_cmd,
            gfx_cmd_wr     => gfx_cmd_wr,
            gfx_frame_sync => gfx_frame_sync,
            gfx_rd_data    => gfx_rd_data,
            gfx_rd_valid   => gfx_rd_valid
        );

    clock: process is
    begin
        clk <= '1';
        wait for CLK_PERIOD / 2;
        clk <= '0';
        wait for CLK_PERIOD / 2;
        if fin then
            wait;
        end if;
    end process;

end architecture;

