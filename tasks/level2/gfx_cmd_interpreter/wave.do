onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /gfx_cmd_interpreter_tb/uut/clk
add wave -noupdate /gfx_cmd_interpreter_tb/uut/gfx_cmd
add wave -noupdate /gfx_cmd_interpreter_tb/uut/gfx_cmd_wr
add wave -noupdate /gfx_cmd_interpreter_tb/uut/gfx_frame_sync
add wave -noupdate /gfx_cmd_interpreter_tb/uut/gfx_rd_data
add wave -noupdate /gfx_cmd_interpreter_tb/uut/gfx_rd_valid
add wave -noupdate /gfx_cmd_interpreter_tb/uut/vram
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {325500 ps}
