#!/bin/bash

files_that_should_exist="
snake/src/snake_pkg.vhd
snake/src/snake.vhd
snake/src/gfx_init_pkg.vhd
snake/tb/snake_tb.vhd
snake/Makefile
snake_game_field/src/snake_game_field_pkg.vhd
snake_game_field/src/snake_game_field.vhd
snake_game_field/tb/snake_game_field_tb.vhd
snake_game_field/Makefile
top_arch_snake.vhd
"

error_counter=0

#check files that should have been created during exercise 1
for f in $files_that_should_exist; do
	if [ ! -e "$f" ]; then
		echo "Error: file $f does not exist"; 
		let "error_counter++"
	fi;
done;

exit $error_counter
