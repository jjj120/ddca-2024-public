## Packages

### snake_custom_pkg

This package entails the component declaration for the components named later and some useful type definitions. 

### draw_command_pkg

Package with functions and constants for drawing and writing to the screen. 
It entails constants for the second line of the BB_CHAR command for each character. There are also command constants for the commands for gp movement, bb effect, bb cmd and space printing. 

Also there are the lists of commands for drawing the different game over screens (singleplayer and all winning possibilities for multiplayer) and for drawing the pause screen. 

## Components

### prng_vec

prng_vec is a pseudo random number generator for std_logic_vectors. It works just as prng from the prng_pkg. It uses a seed plus the bit index as the seed for each number generator. 

### game_audio_ctrl

game_audio_ctrl is the controller that plays the sounds for eating of the food and on game over. 

It currently plays a tone ladder from D3 to D4 on game over. 

It works as a simple state machine. For the simple tones it just sets the high and low time, pulls up play and continues to hold the high and low time for at least 3 more cycles of the 12 MHz clock (so 15 cycles of the 50 MHz) to wait for the 3 stage synchroniser in the audio controller. 

For the game over tones it at first sets the high and low times for the first two tones, then waits for the sync and then switching to the other synth. It then sets the high and low times for the player that was just turned off. 

It repeats like that for all the tones in the given array. 


### pause_game_over_screen

assert singleplayer to select singleplayer game over screen and not assert it for multiplayer

assert draw_pause for one cycle to draw the pause screen

assert draw_game_over for one cycle to draw the game over screen selected via singleplayer


### food_controller

Simple state machine for searching and removing/placement of the food in the ram. It also tracks the current type of food, when and for how long the special food is there for. It also searches for new food by randomly generating positions and checking if they are free. 



### scoreboard / scorekeep

The scoreboard entails a scorekeeper which saves the current score. 

The controlling of the scorekeeper is a sync process that only gives commands when check_collision is asserted and the snake collided with a food. The collision is saved in the colllision_t type. 

The drawing for the scoreboard is a very simple state machine that loops through all the commands that are needed to draw the scoreboard line at the bottom of the screen. 



### snake
combine all components and implement game logic

![snake automaton](./snake_automaton.svg)