library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.gfx_cmd_pkg.all;
    use work.snake_pkg.all;
    use work.snake_ctrl_pkg.all;
    use work.snake_custom_pkg.all;

package draw_command_pkg is
    type cmd_arr is array (natural range <>) of std_logic_vector;

    procedure write_char_to_display_1(signal gfx_cmd : out std_logic_vector(GFX_CMD_WIDTH - 1 downto 0); signal gfx_cmd_wr : out std_logic; bmpidx : in bmpidx_t := "010"; am : in std_logic := '1'; rot : in rot_t := ROT_R0; my : in std_logic := '0'; mx : in std_logic := '1');
    procedure write_char_to_display_2(signal gfx_cmd : out std_logic_vector(GFX_CMD_WIDTH - 1 downto 0); signal gfx_cmd_wr : out std_logic; char : in string);

    procedure write_digit_to_display_1(signal gfx_cmd : out std_logic_vector(GFX_CMD_WIDTH - 1 downto 0); signal gfx_cmd_wr : out std_logic; bmpidx : in bmpidx_t := "010"; am : in std_logic := '1'; rot : in rot_t := ROT_R0; my : in std_logic := '0'; mx : in std_logic := '1');
    procedure write_digit_to_display_2(signal gfx_cmd : out std_logic_vector(GFX_CMD_WIDTH - 1 downto 0); signal gfx_cmd_wr : out std_logic; num : in integer);

    constant CHAR_0         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(0 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_1         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(1 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_2         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(2 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_3         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(3 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_4         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(4 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_5         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(5 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_6         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(6 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_7         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(7 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_8         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(8 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_9         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(9 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_COLON     : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(10 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_SEMICOLON : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(11 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_SMALLER   : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(12 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_EQUAL     : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(13 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_BIGGER    : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(14 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_QUESTION  : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(15 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_AT        : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(16 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_A         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(17 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_B         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(18 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_C         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(19 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_D         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(20 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_E         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(21 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_F         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(22 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_G         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(23 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_H         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(24 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_I         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(25 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_J         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(26 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_K         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(27 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_L         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(28 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_M         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(29 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_N         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(30 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_O         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(31 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_P         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(32 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_Q         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(33 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_R         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(34 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_S         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(35 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_T         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(36 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_U         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(37 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_V         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(38 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_W         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(39 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_X         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(40 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_Y         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(41 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));
    constant CHAR_Z         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(42 * 8, 10)) & std_logic_vector(to_unsigned(8, 6));

    constant CMD_MOVE_GP_ABS       : std_logic_vector(15 downto 0) := create_gfx_instr(OPCODE_MOVE_GP, rel => '0');
    constant CMD_SET_EFFECT_INVERT : std_logic_vector(15 downto 0) := create_gfx_instr(OPCODE_SET_BB_EFFECT, maskop => MASKOP_XOR, mask => COLOR_WHITE);
    constant CMD_BB_STD_1          : std_logic_vector(15 downto 0) := create_gfx_instr(OPCODE_BB_CHAR, am => '1', rot => ROT_R0, my => '0', mx => '1', bmpidx => "010");
    constant CMD_PRINT_SPACE       : std_logic_vector(15 downto 0) := create_gfx_instr(OPCODE_INC_GP, dir => DIR_X, incvalue => std_logic_vector(to_signed(8, WIDTH_INCVALUE))); -- print space / nothing

    pure function calc_winner(score : score_t; collision : collision_t) return std_logic_vector;

    type game_over_cmds is record
        singleplayer       : cmd_arr(0 to 23);
        multiplayer_p1_won : cmd_arr(0 to 45);
        multiplayer_p2_won : cmd_arr(0 to 45);
        multiplayer_tie    : cmd_arr(0 to 29);
    end record;

    constant CMDS_GAME_OVER : game_over_cmds;

    constant CMDS_PAUSE_SCREEN : cmd_arr(0 to 24);
end package;

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

package body draw_command_pkg is
    procedure write_char_to_display_1(signal gfx_cmd : out std_logic_vector(GFX_CMD_WIDTH - 1 downto 0); signal gfx_cmd_wr : out std_logic; bmpidx : in bmpidx_t := "010"; am : in std_logic := '1'; rot : in rot_t := ROT_R0; my : in std_logic := '0'; mx : in std_logic := '1') is
    begin
        gfx_cmd_wr <= '1';
        gfx_cmd <= create_gfx_instr(OPCODE_BB_CHAR, am => am, rot => rot, my => my, mx => mx, bmpidx => bmpidx);
    end procedure;

    procedure write_char_to_display_2(signal gfx_cmd : out std_logic_vector(GFX_CMD_WIDTH - 1 downto 0); signal gfx_cmd_wr : out std_logic; char : in string) is
    begin
        gfx_cmd_wr <= '1';

        case char is
            when "0" => gfx_cmd <= CHAR_0;
            when "1" => gfx_cmd <= CHAR_1;
            when "2" => gfx_cmd <= CHAR_2;
            when "3" => gfx_cmd <= CHAR_3;
            when "4" => gfx_cmd <= CHAR_4;
            when "5" => gfx_cmd <= CHAR_5;
            when "6" => gfx_cmd <= CHAR_6;
            when "7" => gfx_cmd <= CHAR_7;
            when "8" => gfx_cmd <= CHAR_8;
            when "9" => gfx_cmd <= CHAR_9;
            when ":" => gfx_cmd <= CHAR_COLON;
            when ";" => gfx_cmd <= CHAR_SEMICOLON;
            when "<" => gfx_cmd <= CHAR_SMALLER;
            when "=" => gfx_cmd <= CHAR_EQUAL;
            when ">" => gfx_cmd <= CHAR_BIGGER;
            when "?" => gfx_cmd <= CHAR_QUESTION;
            when "@" => gfx_cmd <= CHAR_AT;
            when "A" => gfx_cmd <= CHAR_A;
            when "B" => gfx_cmd <= CHAR_B;
            when "C" => gfx_cmd <= CHAR_C;
            when "D" => gfx_cmd <= CHAR_D;
            when "E" => gfx_cmd <= CHAR_E;
            when "F" => gfx_cmd <= CHAR_F;
            when "G" => gfx_cmd <= CHAR_G;
            when "H" => gfx_cmd <= CHAR_H;
            when "I" => gfx_cmd <= CHAR_I;
            when "J" => gfx_cmd <= CHAR_J;
            when "K" => gfx_cmd <= CHAR_K;
            when "L" => gfx_cmd <= CHAR_L;
            when "M" => gfx_cmd <= CHAR_M;
            when "N" => gfx_cmd <= CHAR_N;
            when "O" => gfx_cmd <= CHAR_O;
            when "P" => gfx_cmd <= CHAR_P;
            when "Q" => gfx_cmd <= CHAR_Q;
            when "R" => gfx_cmd <= CHAR_R;
            when "S" => gfx_cmd <= CHAR_S;
            when "T" => gfx_cmd <= CHAR_T;
            when "U" => gfx_cmd <= CHAR_U;
            when "V" => gfx_cmd <= CHAR_V;
            when "W" => gfx_cmd <= CHAR_W;
            when "X" => gfx_cmd <= CHAR_X;
            when "Y" => gfx_cmd <= CHAR_Y;
            when "Z" => gfx_cmd <= CHAR_Z;
            when others => gfx_cmd <= (others => '0');
        end case;
    end procedure;

    procedure write_digit_to_display_1(signal gfx_cmd : out std_logic_vector(GFX_CMD_WIDTH - 1 downto 0); signal gfx_cmd_wr : out std_logic; bmpidx : in bmpidx_t := "010"; am : in std_logic := '1'; rot : in rot_t := ROT_R0; my : in std_logic := '0'; mx : in std_logic := '1') is
    begin
        write_char_to_display_1(gfx_cmd, gfx_cmd_wr, bmpidx, am, rot, my, mx);
    end procedure;

    procedure write_digit_to_display_2(signal gfx_cmd : out std_logic_vector(GFX_CMD_WIDTH - 1 downto 0); signal gfx_cmd_wr : out std_logic; num : in integer) is
    begin
        case num is
            when 0 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "0");
            when 1 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "1");
            when 2 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "2");
            when 3 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "3");
            when 4 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "4");
            when 5 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "5");
            when 6 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "6");
            when 7 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "7");
            when 8 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "8");
            when 9 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "9");
            when others => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "?");
        end case;
    end procedure;

    pure function calc_winner(score : score_t; collision : collision_t) return std_logic_vector is
        constant PLAYER1_WON : std_logic_vector(1 to 2) := "10";
        constant PLAYER2_WON : std_logic_vector(1 to 2) := "01";
        constant TIE         : std_logic_vector(1 to 2) := "11";
        constant NO_COLL     : std_logic_vector(1 to 2) := "00";
    begin
        if not collision.occured then
            return NO_COLL;
        end if;

        if score.curr_score_p1 > score.curr_score_p2 and collision.type_of_coll = NORMAL_COLL and collision.source_player = PLAYER2 then
            return PLAYER1_WON;
        end if;

        if score.curr_score_p1 < score.curr_score_p2 and collision.type_of_coll = NORMAL_COLL and collision.source_player = PLAYER1 then
            return PLAYER2_WON;
        end if;

        --report "P1: " & to_string(score.curr_score_p1) & ", P2: " & to_string(score.curr_score_p2);
        --report "Coll " & to_string(collision.type_of_coll) & " - " & to_string(collision.source_player);
        return TIE;
    end function;

    constant CMDS_GAME_OVER : game_over_cmds := (
        singleplayer       => (
            CMD_MOVE_GP_ABS,
            std_logic_vector(to_unsigned(DISPLAY_WIDTH / 2 - 4 * 8 - 4, GFX_CMD_WIDTH)), -- x
            std_logic_vector(to_unsigned(DISPLAY_HEIGHT / 2 - 8 - 2, GFX_CMD_WIDTH)), -- y
            CMD_SET_EFFECT_INVERT,
            CMD_BB_STD_1,
            CHAR_G,
            CMD_BB_STD_1,
            CHAR_A,
            CMD_BB_STD_1,
            CHAR_M,
            CMD_BB_STD_1,
            CHAR_E,
            CMD_PRINT_SPACE,
            CMD_BB_STD_1,
            CHAR_O,
            CMD_BB_STD_1,
            CHAR_V,
            CMD_BB_STD_1,
            CHAR_E,
            CMD_BB_STD_1,
            CHAR_R,
            CMD_MOVE_GP_ABS,
            std_logic_vector(to_unsigned(DISPLAY_WIDTH / 2 - 2 * 8 - 4, GFX_CMD_WIDTH)), -- x
            std_logic_vector(to_unsigned(DISPLAY_HEIGHT / 2 + 2, GFX_CMD_WIDTH)) -- y
        ),
        multiplayer_p1_won => (
            CMD_MOVE_GP_ABS,
            std_logic_vector(to_unsigned(DISPLAY_WIDTH / 2 - 4 * 8 - 4, GFX_CMD_WIDTH)), -- x
            std_logic_vector(to_unsigned(DISPLAY_HEIGHT / 2 - 8 - 2, GFX_CMD_WIDTH)), -- y
            CMD_SET_EFFECT_INVERT,
            CMD_BB_STD_1,
            CHAR_G,
            CMD_BB_STD_1,
            CHAR_A,
            CMD_BB_STD_1,
            CHAR_M,
            CMD_BB_STD_1,
            CHAR_E,
            CMD_PRINT_SPACE,
            CMD_BB_STD_1,
            CHAR_O,
            CMD_BB_STD_1,
            CHAR_V,
            CMD_BB_STD_1,
            CHAR_E,
            CMD_BB_STD_1,
            CHAR_R,
            CMD_MOVE_GP_ABS,
            std_logic_vector(to_unsigned(DISPLAY_WIDTH / 2 - 6 * 8, GFX_CMD_WIDTH)), -- x
            std_logic_vector(to_unsigned(DISPLAY_HEIGHT / 2 + 2, GFX_CMD_WIDTH)), -- y
            CMD_BB_STD_1,
            CHAR_P,
            CMD_BB_STD_1,
            CHAR_L,
            CMD_BB_STD_1,
            CHAR_A,
            CMD_BB_STD_1,
            CHAR_Y,
            CMD_BB_STD_1,
            CHAR_E,
            CMD_BB_STD_1,
            CHAR_R,
            CMD_PRINT_SPACE,
            CMD_BB_STD_1,
            CHAR_1,
            CMD_PRINT_SPACE,
            CMD_BB_STD_1,
            CHAR_W,
            CMD_BB_STD_1,
            CHAR_O,
            CMD_BB_STD_1,
            CHAR_N
        ),
        multiplayer_p2_won => (
            CMD_MOVE_GP_ABS,
            std_logic_vector(to_unsigned(DISPLAY_WIDTH / 2 - 4 * 8 - 4, GFX_CMD_WIDTH)), -- x
            std_logic_vector(to_unsigned(DISPLAY_HEIGHT / 2 - 8 - 2, GFX_CMD_WIDTH)), -- y
            CMD_SET_EFFECT_INVERT,
            CMD_BB_STD_1,
            CHAR_G,
            CMD_BB_STD_1,
            CHAR_A,
            CMD_BB_STD_1,
            CHAR_M,
            CMD_BB_STD_1,
            CHAR_E,
            CMD_PRINT_SPACE,
            CMD_BB_STD_1,
            CHAR_O,
            CMD_BB_STD_1,
            CHAR_V,
            CMD_BB_STD_1,
            CHAR_E,
            CMD_BB_STD_1,
            CHAR_R,
            CMD_MOVE_GP_ABS,
            std_logic_vector(to_unsigned(DISPLAY_WIDTH / 2 - 6 * 8, GFX_CMD_WIDTH)), -- x
            std_logic_vector(to_unsigned(DISPLAY_HEIGHT / 2 + 2, GFX_CMD_WIDTH)), -- y
            CMD_BB_STD_1,
            CHAR_P,
            CMD_BB_STD_1,
            CHAR_L,
            CMD_BB_STD_1,
            CHAR_A,
            CMD_BB_STD_1,
            CHAR_Y,
            CMD_BB_STD_1,
            CHAR_E,
            CMD_BB_STD_1,
            CHAR_R,
            CMD_PRINT_SPACE,
            CMD_BB_STD_1,
            CHAR_2,
            CMD_PRINT_SPACE,
            CMD_BB_STD_1,
            CHAR_W,
            CMD_BB_STD_1,
            CHAR_O,
            CMD_BB_STD_1,
            CHAR_N
        ),
        multiplayer_tie    => (
            CMD_MOVE_GP_ABS,
            std_logic_vector(to_unsigned(DISPLAY_WIDTH / 2 - 4 * 8 - 4, GFX_CMD_WIDTH)), -- x
            std_logic_vector(to_unsigned(DISPLAY_HEIGHT / 2 - 8 - 2, GFX_CMD_WIDTH)), -- y
            CMD_SET_EFFECT_INVERT,
            CMD_BB_STD_1,
            CHAR_G,
            CMD_BB_STD_1,
            CHAR_A,
            CMD_BB_STD_1,
            CHAR_M,
            CMD_BB_STD_1,
            CHAR_E,
            CMD_PRINT_SPACE,
            CMD_BB_STD_1,
            CHAR_O,
            CMD_BB_STD_1,
            CHAR_V,
            CMD_BB_STD_1,
            CHAR_E,
            CMD_BB_STD_1,
            CHAR_R,
            CMD_MOVE_GP_ABS,
            std_logic_vector(to_unsigned(DISPLAY_WIDTH / 2 - 8 - 4, GFX_CMD_WIDTH)), -- x
            std_logic_vector(to_unsigned(DISPLAY_HEIGHT / 2 + 2, GFX_CMD_WIDTH)), -- y
            CMD_BB_STD_1,
            CHAR_T,
            CMD_BB_STD_1,
            CHAR_I,
            CMD_BB_STD_1,
            CHAR_E
        )
    );

    constant CMDS_PAUSE_SCREEN : cmd_arr(0 to 24) := (
        CMD_MOVE_GP_ABS,
        std_logic_vector(to_unsigned(DISPLAY_WIDTH / 2 - 5 * 8 - 4, GFX_CMD_WIDTH)),
        std_logic_vector(to_unsigned(DISPLAY_HEIGHT / 2 - 8 - 2, GFX_CMD_WIDTH)),
        CMD_SET_EFFECT_INVERT,
        CMD_BB_STD_1,
        CHAR_G,
        CMD_BB_STD_1,
        CHAR_A,
        CMD_BB_STD_1,
        CHAR_M,
        CMD_BB_STD_1,
        CHAR_E,
        CMD_PRINT_SPACE,
        CMD_BB_STD_1,
        CHAR_P,
        CMD_BB_STD_1,
        CHAR_A,
        CMD_BB_STD_1,
        CHAR_U,
        CMD_BB_STD_1,
        CHAR_S,
        CMD_BB_STD_1,
        CHAR_E,
        CMD_BB_STD_1,
        CHAR_D
    );
end package body;
