library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.math_pkg.log2c;
    use work.gfx_cmd_pkg.all;
    use work.snake_pkg.all;
    use work.snake_ctrl_pkg.all;
    use work.snake_custom_pkg.all;

entity food_controller is
    port (
        -- generic controlling
        clk              : in  std_logic;
        res_n            : in  std_logic;
        busy             : out std_logic;
        load_random_seed : in  std_logic;

        -- ram controlling
        ram_rd           : out std_logic := '0';
        ram_rd_pos       : out vec2d_t   := NULL_VEC;
        ram_rd_data      : in  entry_t;
        ram_wr           : out std_logic := '0';
        ram_wr_pos       : out vec2d_t   := NULL_VEC;
        ram_wr_data      : out entry_t   := (others => '0');

        -- specific
        start            : in  std_logic;
        pause            : in  std_logic;
        search_new_pos   : in  std_logic;
        write_new_food   : in  std_logic;
        collision        : in  collision_t
    );
end entity;

architecture beh of food_controller is
    type curr_state_t is (IDLE, SEARCHING_START, SEARCHING, REMOVING, PLACING);
    type state_t is record
        state                        : curr_state_t;
        curr_food_type               : food_t;
        curr_food_pos                : vec2d_t;
        next_food_pos                : vec2d_t;
        next_pos_ok                  : std_logic;
        removed_prev                 : std_logic;
        special_food_counter         : natural;
        special_food_timeout_counter : natural;
    end record;

    constant DEFAULT_STATE : state_t := (
        state                        => IDLE,
        curr_food_type               => REGULAR_FOOD,
        curr_food_pos                => NULL_VEC,
        next_food_pos                => NULL_VEC,
        next_pos_ok                  => '0',
        removed_prev                 => '1',
        special_food_counter         => 0,
        special_food_timeout_counter => 0
    );

    signal state, next_state : state_t := DEFAULT_STATE;
    signal rnd_pos           : vec2d_t := NULL_VEC;
    signal rnd_pos_cleaned   : vec2d_t := NULL_VEC;

    constant RND_SEED_1 : std_logic_vector(63 downto 0) := "1110010100000111011111010110011010111011111101001100101101011001";
    constant RND_SEED_2 : std_logic_vector(63 downto 0) := "1100010010000001111010101111100110110111011000110100011000100110";

begin
    busy            <= '0' when state.state = IDLE else '1';
    rnd_pos_cleaned <= make_pos_in_bounds_of_gamefield(rnd_pos);

    registers: process (clk, res_n) is
    begin
        if not res_n then
            state <= DEFAULT_STATE;
        elsif rising_edge(clk) then
            state <= next_state;
        end if;
    end process;

    next_state_logic: process (state, rnd_pos_cleaned, search_new_pos, write_new_food, pause, start) is
        variable next_food_type : food_t;

        procedure search is
        begin
            if get_entry_type(ram_rd_data) = BLANK_ENTRY then
                -- found working entry
                next_state.next_pos_ok <= '1';
                next_state.state <= IDLE;
                return;
            end if;

            -- try next entry
            ram_rd <= '1';
            ram_rd_pos <= rnd_pos_cleaned;

            next_state.next_food_pos <= rnd_pos_cleaned;
        end procedure;

        procedure remove is
        begin
            if state.special_food_timeout_counter = SPECIAL_FOOD_TIMEOUT_FRAMES and state.curr_food_type = SPECIAL_FOOD then
                -- remove previous before writing new one
                ram_wr <= '1';
                ram_wr_pos <= state.curr_food_pos;
                ram_wr_data <= create_game_field_entry((others => '0'));

                next_state.removed_prev <= '1';
            end if;

            next_state.state <= PLACING;
        end procedure;

        procedure place is
        begin
            next_state.special_food_timeout_counter <= state.special_food_timeout_counter + 1;

            if not state.next_pos_ok then
                assert 1 = 0 report "NO GOOD PLACEMENT AVAILIBLE!" severity failure;
            end if;

            if start = '1' or state.removed_prev = '1' or (collision.occured = '1' and (collision.type_of_coll = COLL_FOOD_NORMAL or collision.type_of_coll = COLL_FOOD_SPECIAL)) then
                -- write next food
                ram_wr <= '1';
                ram_wr_pos <= state.next_food_pos;
                ram_wr_data <= create_game_field_entry(food_to_entry_type(next_food_type));

                next_state.next_pos_ok <= '0';

                next_state.curr_food_pos <= state.next_food_pos;
                next_state.curr_food_type <= next_food_type;

                next_state.special_food_counter <= state.special_food_counter + 1;

                if next_food_type = SPECIAL_FOOD then
                    -- reset food counter
                    next_state.special_food_counter <= 0;
                    next_state.special_food_timeout_counter <= 0;
                end if;

                next_state.removed_prev <= '0';
            end if;

            next_state.state <= IDLE;
        end procedure;
    begin
        next_state <= state;

        if state.special_food_counter = SPECIAL_FOOD_THRESHOLD then
            next_food_type := SPECIAL_FOOD;
        else
            next_food_type := REGULAR_FOOD;
        end if;

        ram_wr <= '0';
        ram_rd <= '0';
        ram_rd_pos <= NULL_VEC;
        ram_wr_pos <= NULL_VEC;
        ram_wr_data <= (others => '0');

        case state.state is
            when IDLE =>
                if search_new_pos then
                    if start then
                        next_state <= DEFAULT_STATE;
                    end if;
                    next_state.state <= SEARCHING_START;
                elsif write_new_food then
                    next_state.state <= REMOVING;
                end if;

            when SEARCHING_START =>
                ram_rd <= '1';
                ram_rd_pos <= rnd_pos_cleaned;

                next_state.next_food_pos <= rnd_pos_cleaned;
                next_state.state <= SEARCHING;

            when SEARCHING =>
                search;

            when REMOVING =>
                remove;

            when PLACING =>
                place;

        end case;

        -- do nothing if paused
        if pause and not start then
            next_state <= state;
        end if;
    end process;

    prng_vec_inst_x: prng_vec
        generic map (
            RND_VEC_WIDTH => VEC2D_X_WIDTH
        )
        port map (
            clk       => clk,
            res_n     => res_n,
            load_seed => load_random_seed,
            seed      => RND_SEED_1(7 downto 0),
            en        => '1',
            rnd_data  => rnd_pos.x
        );

    prng_vec_inst_y: prng_vec
        generic map (
            RND_VEC_WIDTH => VEC2D_Y_WIDTH
        )
        port map (
            clk       => clk,
            res_n     => res_n,
            load_seed => load_random_seed,
            seed      => RND_SEED_2(7 downto 0),
            en        => '1',
            rnd_data  => rnd_pos.y
        );
end architecture;
