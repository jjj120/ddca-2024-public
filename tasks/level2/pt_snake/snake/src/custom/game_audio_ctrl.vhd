library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.math_real.all;
    use work.snake_custom_pkg.all;
    use work.audio_ctrl_pkg.all;

entity game_audio_ctrl is
    port (
        -- generic controlling
        clk            : in  std_logic;
        res_n          : in  std_logic;
        busy           : out std_logic;
        gfx_frame_sync : in  std_logic;

        collision      : in  collision_t;
        synth_ctrl     : out synth_ctrl_vec_t(0 to 1)
    );
end entity;

architecture beh of game_audio_ctrl is
    pure function freq_to_slv(freq : real) return std_logic_vector is
    begin
        return std_logic_vector(to_unsigned(integer(ceil((((1_000_000.0 / freq) / 2.0) / (1_000_000.0 / 8000.0)))), 8));
    end function;

    constant TONE_FOOD_SPECIAL : std_logic_vector(7 downto 0) := freq_to_slv(391.9954); -- Hz, G4
    constant TONE_FOOD_NORMAL  : std_logic_vector(7 downto 0) := freq_to_slv(329.6276); -- Hz, E4

    type tones_arr_t is array (natural range <>) of std_logic_vector(7 downto 0);

    constant GAME_OVER_FREQS : tones_arr_t(0 to 7) := (
        freq_to_slv(146.8324), -- D3
        freq_to_slv(164.8138), -- E3
        freq_to_slv(174.6141), -- F3
        freq_to_slv(195.9977), -- G3
        freq_to_slv(220.0000), -- A3
        freq_to_slv(246.9417), -- H3
        freq_to_slv(261.6256), -- C3
        freq_to_slv(293.6648) -- D4
    );

    constant FOOD_PLAY_TIME_CYCLES      : natural := integer(200.0e-3 / 20.0e-9); -- 0.2  sec
    constant GAME_OVER_PLAY_TIME_CYCLES : natural := integer(250.0e-3 / 20.0e-9); -- 0.25 sec

    type fsm_state_t is (IDLE, PLAY_NORMAL_FOOD, PLAY_SPECIAL_FOOD, PLAY_GAME_OVER, PLAY_GAME_OVER_TONES, WAIT_FOR_NEXT_FRAME);

    type state_t is record
        state        : fsm_state_t;
        tone_counter : natural;
        clk_counter  : natural;
        synth_ctrl   : synth_ctrl_vec_t(1 downto 0);
    end record;

    constant SYNTH_OFF : synth_ctrl_t := (
        play      => '0',
        high_time => (others => '1'),
        low_time  => (others => '1')
    );

    constant BOTH_SYNTH_OFF : synth_ctrl_vec_t(0 to 1) := (others => SYNTH_OFF);

    constant DEFAULT_STATE : state_t := (
        state        => IDLE,
        tone_counter => 0,
        clk_counter  => 0,
        synth_ctrl   => BOTH_SYNTH_OFF
    );

    signal state, next_state : state_t := DEFAULT_STATE;
begin
    synth_ctrl <= state.synth_ctrl;
    busy       <= '0' when state.state = IDLE or state.state = WAIT_FOR_NEXT_FRAME else '1';

    registers: process (clk, res_n) is
    begin
        if not res_n then
            state <= DEFAULT_STATE;
        elsif rising_edge(clk) then
            state <= next_state;
        end if;
    end process;

    next_state_logic: process (all) is
        pure function lsb_of_natural(num : natural) return natural is
        begin
            return to_integer(to_unsigned(num, 8) and "00000001");
        end function;

        pure function not_lsb_of_natural(num : natural) return natural is
        begin
            return to_integer(not(to_unsigned(num, 8) and "00000001"));
        end function;

    begin
        next_state <= state;

        case state.state is
            when IDLE =>
                next_state.clk_counter <= 0;
                next_state.synth_ctrl(0).play <= '0';
                next_state.synth_ctrl(1).play <= '0';

                if collision.occured then
                    if collision.type_of_coll = COLL_FOOD_NORMAL then
                        next_state.state <= PLAY_NORMAL_FOOD;
                    elsif collision.type_of_coll = COLL_FOOD_SPECIAL then
                        next_state.state <= PLAY_SPECIAL_FOOD;
                    else
                        next_state.state <= PLAY_GAME_OVER;
                    end if;
                end if;

            when PLAY_NORMAL_FOOD =>
                next_state.clk_counter <= state.clk_counter + 1;

                case state.clk_counter is
                    when 0 =>
                        next_state.synth_ctrl(0).high_time <= TONE_FOOD_NORMAL;
                        next_state.synth_ctrl(0).low_time <= TONE_FOOD_NORMAL;

                    when 1 to 15 =>
                        -- wait for at least 3 cycles of the 12 MHz clock
                        null;

                    when 16 to 16 + FOOD_PLAY_TIME_CYCLES =>
                        next_state.synth_ctrl(0).play <= '1';

                    when others =>
                        next_state.state <= WAIT_FOR_NEXT_FRAME;
                end case;

            when PLAY_SPECIAL_FOOD =>
                next_state.clk_counter <= state.clk_counter + 1;

                case state.clk_counter is
                    when 0 =>
                        next_state.synth_ctrl(0).high_time <= TONE_FOOD_SPECIAL;
                        next_state.synth_ctrl(0).low_time <= TONE_FOOD_SPECIAL;

                    when 1 to 15 =>
                        -- wait for at least 3 cycles of the 12 MHz clock
                        null;

                    when 16 to 16 + FOOD_PLAY_TIME_CYCLES =>
                        next_state.synth_ctrl(0).play <= '1';

                    when others =>
                        next_state.state <= WAIT_FOR_NEXT_FRAME;
                end case;

            when PLAY_GAME_OVER =>
                next_state.clk_counter <= state.clk_counter + 1;

                case state.clk_counter is
                    when 0 =>
                        next_state.synth_ctrl(0).high_time <= GAME_OVER_FREQS(0);
                        next_state.synth_ctrl(0).low_time <= GAME_OVER_FREQS(0);

                        next_state.synth_ctrl(1).high_time <= GAME_OVER_FREQS(1);
                        next_state.synth_ctrl(1).low_time <= GAME_OVER_FREQS(1);

                        next_state.tone_counter <= 1;

                    when 1 to 15 =>
                        -- wait for at least 3 cycles of the 12 MHz clock
                        null;

                    when 16 to 16 + GAME_OVER_PLAY_TIME_CYCLES - 1 =>
                        next_state.synth_ctrl(0).play <= '1';

                    when others =>
                        next_state.state <= PLAY_GAME_OVER_TONES;
                        next_state.clk_counter <= 0;
                end case;

            when PLAY_GAME_OVER_TONES =>
                next_state.clk_counter <= state.clk_counter + 1;

                if state.clk_counter >= GAME_OVER_PLAY_TIME_CYCLES then
                    next_state.clk_counter <= 0;
                    next_state.tone_counter <= state.tone_counter + 1;

                    if state.tone_counter = GAME_OVER_FREQS'high then
                        next_state.state <= WAIT_FOR_NEXT_FRAME;
                    end if;
                end if;

                if state.clk_counter = 0 then
                    next_state.synth_ctrl(lsb_of_natural(state.tone_counter)).play <= '1';

                    next_state.synth_ctrl(not_lsb_of_natural(state.tone_counter)).play <= '0';
                    if state.tone_counter + 1 <= GAME_OVER_FREQS'high then
                        next_state.synth_ctrl(not_lsb_of_natural(state.tone_counter)).high_time <= GAME_OVER_FREQS(state.tone_counter + 1);
                        next_state.synth_ctrl(not_lsb_of_natural(state.tone_counter)).low_time <= GAME_OVER_FREQS(state.tone_counter + 1);
                    end if;
                end if;

            when WAIT_FOR_NEXT_FRAME =>
                next_state.synth_ctrl(0).play <= '0';
                next_state.synth_ctrl(1).play <= '0';
                if gfx_frame_sync then
                    next_state.state <= IDLE;
                end if;

            when others =>
        end case;
    end process;
end architecture;
