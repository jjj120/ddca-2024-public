library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.gfx_cmd_pkg.all;
    use work.snake_pkg.all;
    use work.snake_ctrl_pkg.all;
    use work.snake_custom_pkg.all;
    use work.draw_command_pkg.all;

entity pause_game_over_screen is
    port (
        -- generic controlling
        clk            : in  std_logic;
        res_n          : in  std_logic;
        busy           : out std_logic;

        -- direct screen control
        gfx_cmd        : out std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := (others => '0');
        gfx_cmd_wr     : out std_logic                                    := '0';
        gfx_cmd_full   : in  std_logic;

        -- specific
        singleplayer   : in  std_logic;
        draw_pause     : in  std_logic;
        draw_game_over : in  std_logic;
        score          : in  score_t;
        collision      : in  collision_t
    );
end entity;

architecture beh of pause_game_over_screen is
    type curr_state_t is (IDLE, DRAWING);
    type drawing_mode_t is (PAUSE_SCREEN, GAME_OVER_SCREEN);

    type state_t is record
        cmd_idx       : integer;
        winner        : std_logic_vector(1 downto 0);
        drawing_mode  : drawing_mode_t;
        single_player : std_logic;
        state         : curr_state_t;
    end record;

    constant DEFAULT_STATE : state_t := (0, "00", PAUSE_SCREEN, '0', IDLE);
    signal state, next_state : state_t := DEFAULT_STATE;

    type decimal_printer_signal_t is record
        gfx_cmd    : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0);
        gfx_cmd_wr : std_logic;
        start      : std_logic;
        busy       : std_logic;
        number     : integer;
    end record;

    signal decimal_printer_sig : decimal_printer_signal_t;

begin
    busy <= '0' when state.state = IDLE else
            '1';

    decimal_printer_inst: entity work.decimal_printer
        port map (
            clk          => clk,
            res_n        => res_n,
            gfx_cmd      => decimal_printer_sig.gfx_cmd,
            gfx_cmd_wr   => decimal_printer_sig.gfx_cmd_wr,
            gfx_cmd_full => gfx_cmd_full,
            start        => decimal_printer_sig.start,
            busy         => decimal_printer_sig.busy,
            number       => std_logic_vector(to_unsigned(decimal_printer_sig.number, 16)),
            bmpidx       => "010"
        );

    drawing_fsm_registers: process (clk, res_n) is
    begin
        if not res_n then
            state <= DEFAULT_STATE;
        elsif rising_edge(clk) then
            state <= next_state;
        end if;
    end process;

    drawing_fsm_logic: process (state, draw_game_over, draw_pause, collision, singleplayer, score) is
        procedure execute_commands_game_over_singleplayer is
        begin
            if gfx_cmd_full then
                -- wait for the gfx controller to be ready
                next_state <= state;
                return;
            end if;

            gfx_cmd_wr <= '1';
            next_state.cmd_idx <= state.cmd_idx + 1;

            case state.cmd_idx is
                when CMDS_GAME_OVER.singleplayer'range => gfx_cmd <= CMDS_GAME_OVER.singleplayer(state.cmd_idx);

                when CMDS_GAME_OVER.singleplayer'high + 1 => -- print numbers
                    decimal_printer_sig.number <= score.curr_score_p1;
                    decimal_printer_sig.start <= '1';

                when CMDS_GAME_OVER.singleplayer'high + 2 =>
                    decimal_printer_sig.number <= score.curr_score_p1;
                    decimal_printer_sig.start <= '0';

                    gfx_cmd <= decimal_printer_sig.gfx_cmd;
                    gfx_cmd_wr <= decimal_printer_sig.gfx_cmd_wr;

                    -- wait for the decimal printer to finish
                    if decimal_printer_sig.busy then
                        next_state <= state;
                        return;
                    end if;

                    next_state <= DEFAULT_STATE;

                when others => null; --assert 0 = 1 report "Got unknown command index: " & to_string(state.cmd_idx) severity failure;
            end case;
        end procedure;

        procedure execute_commands_game_over_multiplayer is
        begin
            if gfx_cmd_full then
                -- wait for the gfx controller to be ready
                next_state <= state;
                return;
            end if;

            gfx_cmd_wr <= '1';
            next_state.cmd_idx <= state.cmd_idx + 1;

            case state.winner is
                when "10" => -- PLAYER 1 won
                    case state.cmd_idx is
                        when CMDS_GAME_OVER.multiplayer_p1_won'range =>
                            gfx_cmd <= CMDS_GAME_OVER.multiplayer_p1_won(state.cmd_idx);
                            if state.cmd_idx = CMDS_GAME_OVER.multiplayer_p1_won'high then
                                next_state <= DEFAULT_STATE;
                            end if;
                        when others =>
                            --assert 0 = 1 report "Got unknown command index: " & to_string(state.cmd_idx) severity failure;
                            next_state <= DEFAULT_STATE;

                    end case;

                when "01" => -- PLAYER 2 won
                    case state.cmd_idx is
                        when CMDS_GAME_OVER.multiplayer_p2_won'range =>
                            gfx_cmd <= CMDS_GAME_OVER.multiplayer_p2_won(state.cmd_idx);
                            if state.cmd_idx = CMDS_GAME_OVER.multiplayer_p2_won'high then
                                next_state <= DEFAULT_STATE;
                            end if;
                        when others =>
                            --assert 0 = 1 report "Got unknown command index: " & to_string(state.cmd_idx) severity failure;
                            next_state <= DEFAULT_STATE;

                    end case;

                when "11" => -- TIE
                    case state.cmd_idx is
                        when CMDS_GAME_OVER.multiplayer_tie'range =>
                            gfx_cmd <= CMDS_GAME_OVER.multiplayer_tie(state.cmd_idx);
                            if state.cmd_idx = CMDS_GAME_OVER.multiplayer_tie'high then
                                next_state <= DEFAULT_STATE;
                            end if;
                        when others =>
                            --assert 0 = 1 report "Got unknown command index: " & to_string(state.cmd_idx) severity failure;
                            next_state <= DEFAULT_STATE;

                    end case;
                when others => -- no collision occured, should not happen
                    next_state <= DEFAULT_STATE;
            end case;
        end procedure;

        procedure execute_commands_pause_screen is
        begin
            if gfx_cmd_full then
                -- wait for the gfx controller to be ready
                next_state <= state;
                return;
            end if;

            gfx_cmd_wr <= '1';
            next_state.cmd_idx <= state.cmd_idx + 1;

            case state.cmd_idx is
                when CMDS_PAUSE_SCREEN'range =>
                    gfx_cmd <= CMDS_PAUSE_SCREEN(state.cmd_idx);
                    if state.cmd_idx = CMDS_PAUSE_SCREEN'high then
                        next_state <= DEFAULT_STATE;
                    end if;
                when others =>
                    --assert 0 = 1 report "Got unknown command index: " & to_string(state.cmd_idx) severity failure;
                    next_state <= DEFAULT_STATE;

            end case;
        end procedure;
    begin
        -- prevent inferred latches
        next_state <= state;
        gfx_cmd_wr <= '0';
        gfx_cmd <= (others => '0');

        decimal_printer_sig.start <= '0';
        decimal_printer_sig.number <= 0;

        case state.state is
            when IDLE =>
                next_state <= DEFAULT_STATE;
                if draw_game_over then
                    next_state.state <= DRAWING;
                    next_state.drawing_mode <= GAME_OVER_SCREEN;
                    next_state.winner <= calc_winner(score, collision);
                elsif draw_pause then
                    next_state.state <= DRAWING;
                    next_state.drawing_mode <= PAUSE_SCREEN;
                end if;

            when DRAWING =>
                if state.drawing_mode = GAME_OVER_SCREEN then
                    if singleplayer then
                        execute_commands_game_over_singleplayer;
                    else
                        execute_commands_game_over_multiplayer;
                    end if;
                elsif state.drawing_mode = PAUSE_SCREEN then
                    execute_commands_pause_screen;
                end if;
        end case;
    end process;
end architecture;
