library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.math_real.all;
    use work.prng_pkg.all;

entity prng_vec is
    generic (
        RND_VEC_WIDTH : natural
    );
    port (
        clk       : in  std_logic;
        res_n     : in  std_logic;
        load_seed : in  std_logic;
        seed      : in  std_logic_vector(7 downto 0);
        en        : in  std_logic;
        rnd_data  : out std_logic_vector(RND_VEC_WIDTH - 1 downto 0)
    );
end entity;

architecture beh of prng_vec is
begin
    prng_gen: for i in RND_VEC_WIDTH - 1 downto 0 generate
        prng_inst: prng
            port map (
                clk       => clk,
                res_n     => res_n,
                load_seed => load_seed,
                seed      => std_logic_vector(unsigned(seed) + to_unsigned(i, 7)),
                en        => en,
                prdata    => rnd_data(i)
            );
    end generate;
end architecture;
