library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.gfx_cmd_pkg.all;
    use work.snake_pkg.all;
    use work.snake_ctrl_pkg.all;
    use work.snake_custom_pkg.all;
    use work.draw_command_pkg.all;

entity scoreboard is
    port (
        -- generic controlling
        clk             : in     std_logic;
        res_n           : in     std_logic;
        busy            : out    std_logic                                    := '0';

        -- direct screen control
        gfx_cmd         : out    std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := (others => '0');
        gfx_cmd_wr      : out    std_logic                                    := '0';
        gfx_cmd_full    : in     std_logic;

        -- specific
        collision       : in     collision_t;
        check_collision : in     std_logic;
        singleplayer    : in     std_logic;
        draw            : in     std_logic;
        save_highscore  : in     std_logic;
        reset_scores    : in     std_logic;
        score           : buffer score_t                                      := (others => 0)
    );
end entity;

architecture beh of scoreboard is
    signal scorekeep_cmd_exec : std_logic;
    signal scorekeep_cmd      : scorekeep_cmd_t;

    type decimal_printer_signal_t is record
        gfx_cmd    : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0);
        gfx_cmd_wr : std_logic;
        start      : std_logic;
        busy       : std_logic;
        number     : integer;
    end record;

    signal decimal_printer_sig : decimal_printer_signal_t;

    type curr_state_t is (IDLE, DRAWING);
    type drawing_state_t is record
        command_idx : integer;
    end record;

    constant MODE_SINGLEPLAYER : std_logic_vector(1 downto 0) := "10";
    constant MODE_MULTIPLAYER  : std_logic_vector(1 downto 0) := "01";

    type state_t is record
        state         : curr_state_t;
        drawing_state : drawing_state_t;
        single_multi  : std_logic_vector(1 downto 0); -- look at MODE_* constants
    end record;

    constant DEFAULT_STATE : state_t := (
        state         => IDLE,
        drawing_state => (
            command_idx => 0
        ),
        single_multi  => "00"
    );
    signal state, next_state : state_t := DEFAULT_STATE;
begin
    busy <= '0' when state.state = IDLE else
            '1';

    scorekeep_inst: scorekeep
        port map (
            clk      => clk,
            res_n    => res_n,
            cmd_exec => scorekeep_cmd_exec,
            cmd      => scorekeep_cmd,
            score    => score
        );

    scorekeep_proc: process (clk) is
    begin
        if rising_edge(clk) then
            scorekeep_cmd_exec <= '0';
            scorekeep_cmd <= SCOREKEEP_NOP;

            if check_collision and collision.occured then
                scorekeep_cmd_exec <= '1';
                if collision.type_of_coll = COLL_FOOD_NORMAL then
                    if collision.source_player = PLAYER1 then
                        -- player 1
                        scorekeep_cmd <= SCOREKEEP_ADD_NORMAL_P1;
                    else
                        -- player 2
                        scorekeep_cmd <= SCOREKEEP_ADD_NORMAL_P2;
                    end if;
                elsif collision.type_of_coll = COLL_FOOD_SPECIAL then
                    if collision.source_player = PLAYER1 then
                        -- player 1
                        scorekeep_cmd <= SCOREKEEP_ADD_SPECIAL_P1;
                    else
                        -- player 2
                        scorekeep_cmd <= SCOREKEEP_ADD_SPECIAL_P2;
                    end if;
                end if;
            end if;

            if save_highscore then
                scorekeep_cmd_exec <= '1';
                scorekeep_cmd <= SCOREKEEP_SAVE_HIGHSCORE;
            elsif reset_scores then
                scorekeep_cmd_exec <= '1';
                scorekeep_cmd <= SCOREKEEP_RESET_SCORES;
            end if;
        end if;
    end process;

    decimal_printer_inst: entity work.decimal_printer
        port map (
            clk          => clk,
            res_n        => res_n,
            gfx_cmd      => decimal_printer_sig.gfx_cmd,
            gfx_cmd_wr   => decimal_printer_sig.gfx_cmd_wr,
            gfx_cmd_full => gfx_cmd_full,
            start        => decimal_printer_sig.start,
            busy         => decimal_printer_sig.busy,
            number       => std_logic_vector(to_unsigned(decimal_printer_sig.number, 16)),
            bmpidx       => "010"
        );

    drawing_fsm_registers: process (clk, res_n) is
    begin
        if not res_n then
            state <= DEFAULT_STATE;
        elsif rising_edge(clk) then
            state <= next_state;
        end if;
    end process;

    drawing_fsm_logic: process (all) is
        procedure execute_commands_multiplayer is
        begin
            if gfx_cmd_full then
                -- wait for the gfx controller to be ready
                next_state <= state;
                return;
            end if;

            gfx_cmd_wr <= '1';
            next_state.drawing_state.command_idx <= state.drawing_state.command_idx + 1;

            case state.drawing_state.command_idx is
                when 0 => gfx_cmd <= create_gfx_instr(OPCODE_MOVE_GP, rel => '0'); -- move gp to the bottom
                when 1 => gfx_cmd <= std_logic_vector(to_unsigned(0, GFX_CMD_WIDTH)); -- x = 0
                when 2 => gfx_cmd <= std_logic_vector(to_unsigned(DISPLAY_HEIGHT - SNAKE_CHAR_SIZE, GFX_CMD_WIDTH)); -- y = field height

                when 3 => gfx_cmd <= create_gfx_instr(OPCODE_DRAW_HLINE, cs => CS_SECONDARY, mx => '0', my => '0'); -- draw horizontal white line
                when 4 => gfx_cmd <= std_logic_vector(to_unsigned(DISPLAY_WIDTH, GFX_CMD_WIDTH)); -- draw over whole display

                when 5 => gfx_cmd <= create_gfx_instr(OPCODE_MOVE_GP, rel => '0'); -- move gp to first char
                when 6 => gfx_cmd <= std_logic_vector(to_unsigned(20, GFX_CMD_WIDTH)); -- x
                when 7 => gfx_cmd <= std_logic_vector(to_unsigned(DISPLAY_HEIGHT - SNAKE_CHAR_SIZE + 2, GFX_CMD_WIDTH)); -- y

                when 8 => gfx_cmd <= create_gfx_instr(OPCODE_SET_BB_EFFECT, maskop => MASKOP_XOR, mask => COLOR_WHITE); -- set mask that font is white

                when 9 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 10 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "P");
                when 11 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 12 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "L");
                when 13 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 14 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "A");
                when 15 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 16 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "Y");
                when 17 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 18 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "E");
                when 19 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 20 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "R");

                when 21 => gfx_cmd <= create_gfx_instr(OPCODE_INC_GP, dir => DIR_X, incvalue => std_logic_vector(to_signed(5, WIDTH_INCVALUE))); -- print space / nothing

                when 22 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 23 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "1");
                when 24 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 25 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, ":");

                when 26 => gfx_cmd <= create_gfx_instr(OPCODE_INC_GP, dir => DIR_X, incvalue => std_logic_vector(to_signed(5, WIDTH_INCVALUE))); -- print space / nothing

                when 27 => -- print numbers
                    decimal_printer_sig.number <= score.curr_score_p1;
                    decimal_printer_sig.start <= '1';

                when 28 =>
                    decimal_printer_sig.number <= score.curr_score_p1;
                    decimal_printer_sig.start <= '0';

                    gfx_cmd <= decimal_printer_sig.gfx_cmd;
                    gfx_cmd_wr <= decimal_printer_sig.gfx_cmd_wr;

                    -- wait for the decimal printer to finish
                    if decimal_printer_sig.busy then
                        next_state <= state;
                        return;
                    end if;

                when 29 => gfx_cmd <= create_gfx_instr(OPCODE_INC_GP, dir => DIR_X, incvalue => std_logic_vector(to_signed(8 * 5, WIDTH_INCVALUE))); -- print space / nothing

                when 30 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 31 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "P");
                when 32 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 33 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "L");
                when 34 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 35 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "A");
                when 36 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 37 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "Y");
                when 38 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 39 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "E");
                when 40 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 41 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "R");

                when 42 => gfx_cmd <= create_gfx_instr(OPCODE_INC_GP, dir => DIR_X, incvalue => std_logic_vector(to_signed(5, WIDTH_INCVALUE))); -- print space / nothing

                when 43 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 44 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "2");
                when 45 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 46 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, ":");

                when 47 => gfx_cmd <= create_gfx_instr(OPCODE_INC_GP, dir => DIR_X, incvalue => std_logic_vector(to_signed(5, WIDTH_INCVALUE))); -- print space / nothing

                when 48 => -- print numbers
                    decimal_printer_sig.number <= score.curr_score_p2;
                    decimal_printer_sig.start <= '1';

                when 49 =>
                    decimal_printer_sig.number <= score.curr_score_p2;
                    decimal_printer_sig.start <= '0';

                    gfx_cmd <= decimal_printer_sig.gfx_cmd;
                    gfx_cmd_wr <= decimal_printer_sig.gfx_cmd_wr;

                    -- wait for the decimal printer to finish
                    if decimal_printer_sig.busy then
                        next_state <= state;
                        return;
                    end if;

                    next_state <= DEFAULT_STATE; -- drawing ready

                when others => null; --assert 0 = 1 report "Got unknown command index: " & to_string(state.drawing_state.command_idx) severity failure;
            end case;
        end procedure;

        procedure execute_commands_singleplayer is
        begin
            if gfx_cmd_full then
                -- wait for the gfx controller to be ready
                next_state <= state;
                return;
            end if;

            gfx_cmd_wr <= '1';
            next_state.drawing_state.command_idx <= state.drawing_state.command_idx + 1;

            case state.drawing_state.command_idx is
                when 0 => gfx_cmd <= create_gfx_instr(OPCODE_MOVE_GP, rel => '0'); -- move gp to the bottom
                when 1 => gfx_cmd <= std_logic_vector(to_unsigned(0, GFX_CMD_WIDTH)); -- x = 0
                when 2 => gfx_cmd <= std_logic_vector(to_unsigned(DISPLAY_HEIGHT - SNAKE_CHAR_SIZE, GFX_CMD_WIDTH)); -- y = field height

                when 3 => gfx_cmd <= create_gfx_instr(OPCODE_DRAW_HLINE, cs => CS_SECONDARY, mx => '0', my => '0'); -- draw horizontal white line
                when 4 => gfx_cmd <= std_logic_vector(to_unsigned(DISPLAY_WIDTH, GFX_CMD_WIDTH)); -- draw over whole display

                when 5 => gfx_cmd <= create_gfx_instr(OPCODE_MOVE_GP, rel => '0'); -- move gp to first char
                when 6 => gfx_cmd <= std_logic_vector(to_unsigned(20, GFX_CMD_WIDTH)); -- x
                when 7 => gfx_cmd <= std_logic_vector(to_unsigned(DISPLAY_HEIGHT - SNAKE_CHAR_SIZE + 2, GFX_CMD_WIDTH)); -- y

                when 8 => gfx_cmd <= create_gfx_instr(OPCODE_SET_BB_EFFECT, maskop => MASKOP_XOR, mask => COLOR_WHITE); -- set mask that font is white

                when 9 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 10 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "S");
                when 11 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 12 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "C");
                when 13 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 14 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "O");
                when 15 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 16 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "R");
                when 17 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 18 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "E");
                when 19 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 20 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, ":");

                when 21 => gfx_cmd <= create_gfx_instr(OPCODE_INC_GP, dir => DIR_X, incvalue => std_logic_vector(to_signed(5, WIDTH_INCVALUE))); -- print space / nothing

                when 22 => -- print numbers
                    decimal_printer_sig.number <= score.curr_score_p1;
                    decimal_printer_sig.start <= '1';

                when 23 =>
                    decimal_printer_sig.number <= score.curr_score_p1;
                    decimal_printer_sig.start <= '0';

                    gfx_cmd <= decimal_printer_sig.gfx_cmd;
                    gfx_cmd_wr <= decimal_printer_sig.gfx_cmd_wr;

                    -- wait for the decimal printer to finish
                    if decimal_printer_sig.busy then
                        next_state <= state;
                        return;
                    end if;

                when 24 => gfx_cmd <= create_gfx_instr(OPCODE_INC_GP, dir => DIR_X, incvalue => std_logic_vector(to_signed(10 * 5, WIDTH_INCVALUE))); -- print space / nothing

                when 25 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 26 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "H");
                when 27 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 28 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "I");
                when 29 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 30 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "G");
                when 31 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 32 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "H");
                when 33 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 34 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "S");
                when 35 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 36 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "C");
                when 37 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 38 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "O");
                when 39 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 40 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "R");
                when 41 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 42 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, "E");
                when 43 => write_char_to_display_1(gfx_cmd, gfx_cmd_wr);
                when 44 => write_char_to_display_2(gfx_cmd, gfx_cmd_wr, ":");

                when 45 => gfx_cmd <= create_gfx_instr(OPCODE_INC_GP, dir => DIR_X, incvalue => std_logic_vector(to_signed(5, WIDTH_INCVALUE))); -- print space / nothing

                when 46 => -- print numbers
                    decimal_printer_sig.number <= score.highscore;
                    decimal_printer_sig.start <= '1';

                when 47 =>
                    decimal_printer_sig.number <= score.highscore;
                    decimal_printer_sig.start <= '0';

                    gfx_cmd <= decimal_printer_sig.gfx_cmd;
                    gfx_cmd_wr <= decimal_printer_sig.gfx_cmd_wr;

                    -- wait for the decimal printer to finish
                    if decimal_printer_sig.busy then
                        next_state <= state;
                        return;
                    end if;

                    next_state <= DEFAULT_STATE; -- drawing ready

                when others => null; --assert 0 = 1 report "Got unknown command index: " & to_string(state.drawing_state.command_idx) severity failure;
            end case;
        end procedure;

    begin
        -- prevent inferred latches
        next_state <= state;
        gfx_cmd_wr <= '0';
        gfx_cmd <= (others => '0');

        decimal_printer_sig.start <= '0';
        decimal_printer_sig.number <= 0;

        case state.state is
            when IDLE =>
                next_state <= DEFAULT_STATE;
                if draw then
                    if singleplayer then
                        next_state.state <= DRAWING;
                        next_state.single_multi <= MODE_SINGLEPLAYER;
                    else
                        next_state.state <= DRAWING;
                        next_state.single_multi <= MODE_MULTIPLAYER;
                    end if;
                end if;
            when DRAWING =>
                if state.single_multi = MODE_SINGLEPLAYER then
                    execute_commands_singleplayer;
                elsif state.single_multi = MODE_MULTIPLAYER then
                    execute_commands_multiplayer;
                end if;
        end case;
    end process;
end architecture;
