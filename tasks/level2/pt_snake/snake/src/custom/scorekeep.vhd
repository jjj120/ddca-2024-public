library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.gfx_cmd_pkg.all;
    use work.snake_pkg.all;
    use work.snake_ctrl_pkg.all;
    use work.snake_custom_pkg.all;

entity scorekeep is
    port (
        clk      : in  std_logic;
        res_n    : in  std_logic;

        cmd_exec : in  std_logic;
        cmd      : in  scorekeep_cmd_t;
        score    : out score_t
    );
end entity;

architecture beh of scorekeep is
    signal curr_score, next_score : score_t := (others => 0);
begin
    registers: process (clk, res_n) is
    begin
        if not res_n then
            curr_score <= (others => 0);
        elsif rising_edge(clk) then
            curr_score <= next_score;
        end if;
    end process;

    score <= curr_score;

    logic: process (all) is
    begin
        next_score <= curr_score;
        if cmd_exec then
            case cmd is
                when SCOREKEEP_ADD_NORMAL_P1 =>
                    next_score.curr_score_p1 <= curr_score.curr_score_p1 + FOOD_POINTS;
                when SCOREKEEP_ADD_NORMAL_P2 =>
                    next_score.curr_score_p2 <= curr_score.curr_score_p2 + FOOD_POINTS;

                when SCOREKEEP_ADD_SPECIAL_P1 =>
                    next_score.curr_score_p1 <= curr_score.curr_score_p1 + SPECIAL_FOOD_POINTS;
                when SCOREKEEP_ADD_SPECIAL_P2 =>
                    next_score.curr_score_p2 <= curr_score.curr_score_p2 + SPECIAL_FOOD_POINTS;

                when SCOREKEEP_SAVE_HIGHSCORE =>
                    -- only save highscore from p1 because this is a singleplayer feature
                    if curr_score.curr_score_p1 > curr_score.highscore then
                        next_score.highscore <= curr_score.curr_score_p1;
                    end if;
                when SCOREKEEP_RESET_SCORES =>
                    next_score.curr_score_p1 <= 0;
                    next_score.curr_score_p2 <= 0;
                when others =>
                    assert 1 = 0 report "GOT UNKNOWN COMMAND IN SCOREKEEP: " severity failure;
            end case;
        end if;
    end process;
end architecture;
