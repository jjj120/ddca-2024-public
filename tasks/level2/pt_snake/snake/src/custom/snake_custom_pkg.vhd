library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.audio_ctrl_pkg.all;
    use work.gfx_cmd_pkg.all;
    use work.snake_pkg.all;
    use work.snake_ctrl_pkg.all;
    use work.math_pkg.all;

package snake_custom_pkg is
    type vec2d_array_t is array (natural range <>) of vec2d_t;

    constant FRAMES_BETWEEN_MOVEMENTS      : natural                   := 5;
    constant INITIAL_SNAKE_HEAD_DIRECTIONS : direction_array_t(0 to 1) := (LEFT, LEFT);
    constant INITIAL_SNAKE_HEAD_POSITIONS  : vec2d_array_t(0 to 1)     := (create_vec2d(10, 10), create_vec2d(10, 5));
    constant INITIAL_BODY_LENGTH           : integer_vector(0 to 1)    := (5, 5);
    constant FOOD_POINTS                   : natural                   := 1;
    constant SPECIAL_FOOD_POINTS           : natural                   := 5;
    constant SPECIAL_FOOD_THRESHOLD        : natural                   := 5;
    constant SPECIAL_FOOD_TIMEOUT_FRAMES   : natural                   := SNAKE_FIELD_WIDTH * 3;

    constant GFX_NO_COMMAND : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := (others => '0');
    constant NULL_ENTRY     : entry_t                                      := (others => '0');

    type score_t is record
        curr_score_p1 : integer;
        curr_score_p2 : integer;
        highscore     : integer;
    end record;

    type collision_type_t is (NORMAL_COLL, HEADON_COLL, COLL_FOOD_NORMAL, COLL_FOOD_SPECIAL);

    type collision_t is record
        occured       : std_logic;
        source_player : player_t;
        type_of_coll  : collision_type_t;
    end record;

    constant NO_COLLISION : collision_t := (
        occured       => '0',
        source_player => PLAYER1,
        type_of_coll  => NORMAL_COLL
    );

    component pause_game_over_screen is
        port (
            -- generic controlling
            clk            : in  std_logic;
            res_n          : in  std_logic;
            busy           : out std_logic;

            -- direct screen control
            gfx_cmd        : out std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := (others => '0');
            gfx_cmd_wr     : out std_logic                                    := '0';
            gfx_cmd_full   : in  std_logic;

            -- specific
            singleplayer   : in  std_logic;
            draw_pause     : in  std_logic;
            draw_game_over : in  std_logic;
            score          : in  score_t;
            collision      : in  collision_t
        );
    end component;

    type food_t is (REGULAR_FOOD, SPECIAL_FOOD);

    pure function food_to_entry_type(food : food_t) return entry_type_t;

    component food_controller is
        port (
            -- generic controlling
            clk              : in  std_logic;
            res_n            : in  std_logic;
            busy             : out std_logic;
            load_random_seed : in  std_logic;

            -- ram controlling
            ram_rd           : out std_logic;
            ram_rd_pos       : out vec2d_t;
            ram_rd_data      : in  entry_t;
            ram_wr           : out std_logic;
            ram_wr_pos       : out vec2d_t;
            ram_wr_data      : out entry_t;

            -- specific
            start            : in  std_logic;
            search_new_pos   : in  std_logic;
            write_new_food   : in  std_logic;
            pause            : in  std_logic;
            collision        : in  collision_t
        );
    end component;

    component scoreboard is
        port (
            -- generic controlling
            clk             : in  std_logic;
            res_n           : in  std_logic;
            busy            : out std_logic;

            -- direct screen control
            gfx_cmd         : out std_logic_vector(GFX_CMD_WIDTH - 1 downto 0);
            gfx_cmd_wr      : out std_logic;
            gfx_cmd_full    : in  std_logic;

            -- specific
            collision       : in  collision_t;
            check_collision : in  std_logic;
            singleplayer    : in  std_logic;
            draw            : in  std_logic;
            save_highscore  : in  std_logic;
            reset_scores    : in  std_logic;
            score           : out score_t
        );
    end component;

    type scorekeep_cmd_t is (SCOREKEEP_NOP, SCOREKEEP_ADD_NORMAL_P1, SCOREKEEP_ADD_NORMAL_P2, SCOREKEEP_ADD_SPECIAL_P1, SCOREKEEP_ADD_SPECIAL_P2, SCOREKEEP_SAVE_HIGHSCORE, SCOREKEEP_RESET_SCORES);

    component scorekeep is
        port (
            clk      : in  std_logic;
            res_n    : in  std_logic;

            cmd_exec : in  std_logic;
            cmd      : in  scorekeep_cmd_t;
            score    : out score_t
        );
    end component;

    component prng_vec is
        generic (
            RND_VEC_WIDTH : natural
        );
        port (
            clk       : in  std_logic;
            res_n     : in  std_logic;
            load_seed : in  std_logic;
            seed      : in  std_logic_vector(7 downto 0);
            en        : in  std_logic;
            rnd_data  : out std_logic_vector(RND_VEC_WIDTH - 1 downto 0)
        );
    end component;

    component game_audio_ctrl is
        port (
            -- generic controlling
            clk            : in  std_logic;
            res_n          : in  std_logic;
            busy           : out std_logic;
            gfx_frame_sync : in  std_logic; -- to only play once per frame

            collision      : in  collision_t;
            synth_ctrl     : out synth_ctrl_vec_t(0 to 1)
        );
    end component;

    pure function make_pos_in_bounds_of_gamefield(position : vec2d_t) return vec2d_t;

    constant SNAKE_FIELD_WIDTH_ADDR_WIDTH  : integer := log2c(SNAKE_FIELD_WIDTH);
    constant SNAKE_FIELD_HEIGHT_ADDR_WIDTH : integer := log2c(SNAKE_FIELD_HEIGHT);

end package;

package body snake_custom_pkg is
    pure function food_to_entry_type(food : food_t) return entry_type_t is
    begin
        if food = REGULAR_FOOD then
            return FOOD_REGULAR;
        else
            return FOOD_SPECIAL;
        end if;
    end function;

    pure function make_pos_in_bounds_of_gamefield(position : vec2d_t) return vec2d_t is
        variable to_return : vec2d_t;
    begin
        to_return := (
            -- clip to fit within the power of 2 just above the max value
            position.x(SNAKE_FIELD_WIDTH_ADDR_WIDTH - 1 downto 0),
            position.y(SNAKE_FIELD_HEIGHT_ADDR_WIDTH - 1 downto 0)
        );

        if unsigned(position.x) >= SNAKE_FIELD_WIDTH then
            -- if still to large substract max value once, this is enough to surely in bounds
            to_return.x := std_logic_vector(unsigned(to_return.x) - SNAKE_FIELD_WIDTH);
        end if;

        if unsigned(position.y) >= SNAKE_FIELD_HEIGHT then
            -- if still to large substract max value once, this is enough to surely in bounds
            to_return.y := std_logic_vector(unsigned(to_return.y) - SNAKE_FIELD_HEIGHT);
        end if;

        return to_return;
    end function;

end package body;
