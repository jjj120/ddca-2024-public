library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.gfx_cmd_pkg.all;
use work.snake_ctrl_pkg.all;
use work.audio_ctrl_pkg.all;
use work.snake_pkg.all;

entity precompiled_snake is
	port (
		clk : in std_logic;
		res_n : in std_logic;
		
		--connection to the gfx cmd controller
		gfx_cmd        : out std_logic_vector(GFX_CMD_WIDTH-1 downto 0);
		gfx_cmd_wr     : out std_logic;
		gfx_cmd_full   : in std_logic;
		gfx_rd_valid   : in std_logic;
		gfx_rd_data    : in std_logic_vector(15 downto 0);
		gfx_frame_sync : in std_logic;
		
		--connections to the controllers
		cntrl1 : in snake_ctrl_t;
		cntrl2 : in snake_ctrl_t;
		
		--connection to the audio controller
		synth_ctrl : out synth_ctrl_vec_t(0 to 1)
	); 
end entity;

architecture arch of precompiled_snake is
	component snake_top is
		port (
			clk : in std_logic;
			res_n : in std_logic;
			
			--connection to the gfx cmd controller
			gfx_cmd        : out std_logic_vector(GFX_CMD_WIDTH-1 downto 0);
			gfx_cmd_wr     : out std_logic;
			gfx_cmd_full   : in std_logic;
			gfx_rd_valid   : in std_logic;
			gfx_rd_data    : in std_logic_vector(15 downto 0);
			gfx_frame_sync : in std_logic;
			
			--connections to the controllers
			cntrl1 : in snake_ctrl_t;
			cntrl2 : in snake_ctrl_t;
			
			--connection to the audio controller
			synth_ctrl : out synth_ctrl_vec_t(0 to 1)
		); 
	end component;
begin
	snake_top_inst : snake_top
	port map (
		clk            => clk,
		res_n          => res_n,
		gfx_cmd        => gfx_cmd,
		gfx_cmd_wr     => gfx_cmd_wr,
		gfx_cmd_full   => gfx_cmd_full,
		gfx_rd_valid   => gfx_rd_valid,
		gfx_rd_data    => gfx_rd_data,
		gfx_frame_sync => gfx_frame_sync,
		cntrl1         => cntrl1,
		cntrl2         => cntrl2,
		synth_ctrl     => synth_ctrl
	); 
end architecture;
