library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.math_pkg.all;
    use work.gfx_cmd_pkg.all;
    use work.gfx_init_pkg.all;
    use work.snake_ctrl_pkg.all;
    use work.snake_pkg.all;
    use work.snake_game_field_pkg.all;
    use work.audio_ctrl_pkg.all;
    use work.prng_pkg.all;

    use work.snake_custom_pkg.all;

entity snake is
    port (
        clk            : in  std_logic;
        res_n          : in  std_logic;

        --connection to the gfx cmd controller
        gfx_cmd        : out std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := (others => '0');
        gfx_cmd_wr     : out std_logic                                    := '0';
        gfx_cmd_full   : in  std_logic;
        gfx_rd_valid   : in  std_logic;
        gfx_rd_data    : in  std_logic_vector(15 downto 0);
        gfx_frame_sync : in  std_logic;

        --connections to the controllers
        cntrl1         : in  snake_ctrl_t;
        cntrl2         : in  snake_ctrl_t;

        --connection to the audio controller
        synth_ctrl     : out synth_ctrl_vec_t(0 to 1);
        dbg_head       : out vec2d_t
    );
end entity;

architecture arch of snake is
    type main_state_t is (INIT, IDLE, RUNNING, RUNNING_START, PAUSE, PAUSE_START, GAME_OVER, GAME_OVER_START);

    type fsm_state_t is (
            GFX_INIT, WAIT_GFX_INIT,
            RESET, CLEAR_GAME_FIELD, WAIT_CLEAR_GAME_FIELD,

            -- Draw the game
            DO_FRAME_SYNC, WAIT_FRAME_SYNC, SWITCH_FRAME_BUFFER, CLEAR_SCREEN, WAIT_FRAMES,

            -- Idle state: random snake movement
            IDLE_PLACE_SNAKE, IDLE_PLACE_SNAKE_WAIT,
            IDLE_CHECK_GAME_START, IDLE_PROCESS_INPUTS, IDLE_DRAW_GAME,
            IDLE_MOVE_HEAD, IDLE_WAIT_MOVE_HEAD, IDLE_MOVE_TAIL, IDLE_WAIT_MOVE_TAIL,

            -- Running state: controlled movement with one snake
            RUNNING_START_SINGLEPLAYER, RUNNING_START_MULTIPLAYER, RUNNING_PLACE_SNAKE, RUNNING_PLACE_SNAKE_WAIT,
            RUNNING_PROCESS_INPUTS, RUNNING_CHECK_COLLISION_READ_P1, RUNNING_CHECK_COLLISION_READ_P2, RUNNING_CHECK_COLLISION_MULTIPLAYER, RUNNING_CHECK_COLLISION_SINGLEPLAYER,
            RUNNING_DRAW_GAME, RUNNING_DRAW_GAME_WAIT, RUNNING_DRAW_SCOREBOARD, RUNNING_DRAW_SCOREBOARD_WAIT,
            RUNNING_MOVE_TAIL, RUNNING_WAIT_MOVE_TAIL, RUNNING_MOVE_HEAD, RUNNING_WAIT_MOVE_HEAD,

            -- Food states: search and place food
            FOOD_SEARCH_START, FOOD_SEARCH, FOOD_SEARCH_WAIT, FOOD_PLACE_START, FOOD_PLACE, FOOD_PLACE_WAIT,

            -- other states:
            PAUSE_DRAW, PAUSE_WAIT_DRAW, PAUSE_PROCESS_INPUTS, PAUSE_WAIT_BUTTON_RELEASE,
            GAME_OVER_DRAW, GAME_OVER_WAIT_DRAW, GAME_OVER_PROCESS_INPUTS, GAME_OVER_WAIT_BUTTON_RELEASE
        );

    type ram_permission_t is (RAM_PERM_NONE, RAM_PERM_SNAKE_CTRL, RAM_PERM_FOOD_CONTROLLER, RAM_PERM_COLL_DETECTION);
    type gfx_permission_t is (GFX_PERM_NONE, GFX_PERM_PAUSE, GFX_PERM_SCOREBOARD, GFX_PERM_GAME_FIELD, GFX_PERM_INIT, GFX_PERM_DRAW);

    -- wires for the draw commands to send commands to the gfx controller
    type draw_gfx_ctrl_t is record
        gfx_cmd_wr : std_logic;
        gfx_cmd    : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0);
    end record;

    type state_t is record
        main_state            : main_state_t;
        state                 : fsm_state_t;
        movement_direction_p1 : direction_t;
        movement_direction_p2 : direction_t;
        head_pos_p1           : vec2d_t;
        head_pos_p2           : vec2d_t;
        mem_at_new_p1         : entry_t;
        singleplayer          : std_logic;
        curr_player           : player_t;
        collision             : collision_t;
        frame_counter         : std_logic_vector(max(log2c(FRAMES_BETWEEN_MOVEMENTS) - 1, 0) downto 0);
        frame_buffer_selector : std_logic;
        skip_move             : unsigned(1 downto 0);
        ram_permission        : ram_permission_t;
        gfx_permission        : gfx_permission_t;
        draw_gfx_ctrl         : draw_gfx_ctrl_t;
        starting              : std_logic;
    end record;

    constant RESET_STATE : state_t := (
        main_state            => INIT,
        state                 => GFX_INIT,
        movement_direction_p1 => LEFT,
        movement_direction_p2 => LEFT,
        head_pos_p1           => INITIAL_SNAKE_HEAD_POSITIONS(0),
        head_pos_p2           => INITIAL_SNAKE_HEAD_POSITIONS(1),
        mem_at_new_p1         => (others => '0'),
        singleplayer          => '0',
        curr_player           => PLAYER1,
        collision             => NO_COLLISION,
        frame_counter         => (others => '0'),
        frame_buffer_selector => '0',
        skip_move             => "00",
        ram_permission        => RAM_PERM_NONE,
        gfx_permission        => GFX_PERM_INIT,
        draw_gfx_ctrl         => (
            gfx_cmd_wr => '0',
            gfx_cmd    => (others => '0')
        ),
        starting              => '0'
    );

    signal state, next_state : state_t := RESET_STATE;

    -- wires to gfx cmd initializer block
    type gfx_init_ctrl_t is record
        initializer_busy, initializer_start : std_logic;
        initializer_cmd_wr                  : std_logic;
        initializer_cmd                     : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0);
    end record;
    signal gfx_init_ctrl : gfx_init_ctrl_t := (
        initializer_busy   => '0',
        initializer_start  => '0',
        initializer_cmd_wr => '0',
        initializer_cmd    => (others => '0')
    );

    -- wires to snake game field
    type snake_game_field_ctrl_t is record
        ram_wr, ram_rd           : std_logic;
        ram_wr_pos, ram_rd_pos   : vec2d_t;
        ram_wr_data, ram_rd_data : std_logic_vector(SNAKE_FIELD_DATA_WIDTH - 1 downto 0);
        ram_busy                 : std_logic;
        draw, busy, clear        : std_logic;
        gfx_cmd_wr               : std_logic;
        gfx_cmd                  : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0);
    end record;
    signal snake_game_field_ctrl : snake_game_field_ctrl_t := (
        ram_wr      => '0',
        ram_rd      => '0',
        ram_wr_pos  => NULL_VEC,
        ram_rd_pos  => NULL_VEC,
        ram_wr_data => (others => '0'),
        ram_rd_data => (others => '0'),
        ram_busy    => '0',
        draw        => '0',
        busy        => '0',
        clear       => '0',
        gfx_cmd_wr  => '0',
        gfx_cmd     => (others => '0')
    );

    -- wires to snake module
    type snake_ctrl_wires_t is record
        ram_wr, ram_rd         : std_logic;
        ram_wr_pos, ram_rd_pos : vec2d_t;
        ram_wr_data            : std_logic_vector(SNAKE_FIELD_DATA_WIDTH - 1 downto 0);
        done                   : std_logic;
        init                   : std_logic;
        movement_direction     : direction_t;
        move_head              : std_logic;
        move_tail              : std_logic;
        init_head_position     : vec2d_t;
        init_len               : std_logic_vector(3 downto 0);
    end record;
    signal snake_ctrl_wires : snake_ctrl_wires_t := (
        ram_wr             => '0',
        ram_rd             => '0',
        ram_wr_pos         => NULL_VEC,
        ram_rd_pos         => NULL_VEC,
        ram_wr_data        => (others => '0'),
        done               => '0',
        init               => '0',
        movement_direction => LEFT,
        move_head          => '0',
        move_tail          => '0',
        init_head_position => INITIAL_SNAKE_HEAD_POSITIONS(0),
        init_len           => std_logic_vector(to_unsigned(INITIAL_BODY_LENGTH(0), 4))
    );

    signal rnd_vec   : std_logic_vector(1 downto 0) := "00";
    signal load_seed : std_logic                    := '0';

    -- wires to scoreboard
    type scoreboard_ctrl_t is record
        busy            : std_logic;
        gfx_cmd_wr      : std_logic;
        gfx_cmd         : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0);
        check_collision : std_logic;
        draw            : std_logic;
        save_highscore  : std_logic;
        reset_scores    : std_logic;
        score           : score_t;
    end record;
    signal scoreboard_ctrl : scoreboard_ctrl_t := (
        busy            => '0',
        gfx_cmd_wr      => '0',
        gfx_cmd         => (others => '0'),
        check_collision => '0',
        draw            => '0',
        save_highscore  => '0',
        reset_scores    => '0',
        score           => (
            curr_score_p1 => 0,
            curr_score_p2 => 0,
            highscore     => 0
        )
    );

    -- wires to pause_game_over_screen
    type pause_game_over_screen_ctrl_t is record
        busy           : std_logic;
        gfx_cmd_wr     : std_logic;
        gfx_cmd        : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0);
        draw_pause     : std_logic;
        draw_game_over : std_logic;
    end record;
    signal pause_game_over_screen_ctrl : pause_game_over_screen_ctrl_t := (
        busy           => '0',
        gfx_cmd_wr     => '0',
        gfx_cmd        => (others => '0'),
        draw_pause     => '0',
        draw_game_over => '0'
    );

    -- wires to food_controller
    type food_controller_ctrl_t is record
        busy                   : std_logic;
        ram_wr, ram_rd         : std_logic;
        ram_wr_pos, ram_rd_pos : vec2d_t;
        ram_wr_data            : std_logic_vector(SNAKE_FIELD_DATA_WIDTH - 1 downto 0);
        pause                  : std_logic;
        search_new_pos         : std_logic;
        write_new_food         : std_logic;
    end record;
    signal food_controller_ctrl : food_controller_ctrl_t := (
        busy           => '0',
        ram_wr         => '0',
        ram_rd         => '0',
        ram_wr_pos     => NULL_VEC,
        ram_rd_pos     => NULL_VEC,
        ram_wr_data    => (others => '0'),
        pause          => '0',
        search_new_pos => '0',
        write_new_food => '0'
    );

    type ram_coll_det_t is record
        ram_rd     : std_logic;
        ram_rd_pos : vec2d_t;
    end record;
    signal ram_coll_det : ram_coll_det_t := (
        ram_rd     => '0',
        ram_rd_pos => NULL_VEC
    );

    signal init_head_pos1_gen : vec2d_t := INITIAL_SNAKE_HEAD_POSITIONS(0);

    constant rnd_seed_1 : std_logic_vector(63 downto 0) := "0110010100011111010010101111010101010110010000010010010001100011";
    constant rnd_seed_2 : std_logic_vector(63 downto 0) := "0011111010100001100111111000010100011111111100101011001001110110";

begin
    snake_game_field_ctrl.ram_busy <= gfx_init_ctrl.initializer_busy or scoreboard_ctrl.busy or pause_game_over_screen_ctrl.busy or food_controller_ctrl.busy or snake_game_field_ctrl.busy;
    food_controller_ctrl.pause     <= '1' when state.main_state = PAUSE or state.main_state = PAUSE_START else '0';

    dbg_head <= state.head_pos_p1;

    -- selectors for gfx access
    with state.gfx_permission select gfx_cmd_wr <=
        '0'                                    when GFX_PERM_NONE,
        pause_game_over_screen_ctrl.gfx_cmd_wr when GFX_PERM_PAUSE,
        scoreboard_ctrl.gfx_cmd_wr             when GFX_PERM_SCOREBOARD,
        snake_game_field_ctrl.gfx_cmd_wr       when GFX_PERM_GAME_FIELD,
        gfx_init_ctrl.initializer_cmd_wr       when GFX_PERM_INIT,
        state.draw_gfx_ctrl.gfx_cmd_wr         when GFX_PERM_DRAW;

    with state.gfx_permission select gfx_cmd <=
        GFX_NO_COMMAND                      when GFX_PERM_NONE,
        pause_game_over_screen_ctrl.gfx_cmd when GFX_PERM_PAUSE,
        scoreboard_ctrl.gfx_cmd             when GFX_PERM_SCOREBOARD,
        snake_game_field_ctrl.gfx_cmd       when GFX_PERM_GAME_FIELD,
        gfx_init_ctrl.initializer_cmd       when GFX_PERM_INIT,
        state.draw_gfx_ctrl.gfx_cmd         when GFX_PERM_DRAW;

    -- selectors for ram access
    with state.ram_permission select snake_game_field_ctrl.ram_rd <=
        '0'                         when RAM_PERM_NONE,
        snake_ctrl_wires.ram_rd     when RAM_PERM_SNAKE_CTRL,
        food_controller_ctrl.ram_rd when RAM_PERM_FOOD_CONTROLLER,
        ram_coll_det.ram_rd         when RAM_PERM_COLL_DETECTION;

    with state.ram_permission select snake_game_field_ctrl.ram_rd_pos <=
        NULL_VEC                        when RAM_PERM_NONE,
        snake_ctrl_wires.ram_rd_pos     when RAM_PERM_SNAKE_CTRL,
        food_controller_ctrl.ram_rd_pos when RAM_PERM_FOOD_CONTROLLER,
        ram_coll_det.ram_rd_pos         when RAM_PERM_COLL_DETECTION;

    with state.ram_permission select snake_game_field_ctrl.ram_wr <=
        '0'                         when RAM_PERM_NONE,
        snake_ctrl_wires.ram_wr     when RAM_PERM_SNAKE_CTRL,
        food_controller_ctrl.ram_wr when RAM_PERM_FOOD_CONTROLLER,
        '0'                         when RAM_PERM_COLL_DETECTION;

    with state.ram_permission select snake_game_field_ctrl.ram_wr_pos <=
        NULL_VEC                        when RAM_PERM_NONE,
        snake_ctrl_wires.ram_wr_pos     when RAM_PERM_SNAKE_CTRL,
        food_controller_ctrl.ram_wr_pos when RAM_PERM_FOOD_CONTROLLER,
        NULL_VEC                        when RAM_PERM_COLL_DETECTION;

    with state.ram_permission select snake_game_field_ctrl.ram_wr_data <=
        NULL_ENTRY                       when RAM_PERM_NONE,
        snake_ctrl_wires.ram_wr_data     when RAM_PERM_SNAKE_CTRL,
        food_controller_ctrl.ram_wr_data when RAM_PERM_FOOD_CONTROLLER,
        NULL_ENTRY                       when RAM_PERM_COLL_DETECTION;

    -- selector for movement direction of snake_ctrl
    snake_ctrl_wires.movement_direction <= state.movement_direction_p1 when state.curr_player = PLAYER1 else state.movement_direction_p2;
    snake_ctrl_wires.init_head_position <= make_pos_in_bounds_of_gamefield(init_head_pos1_gen) when state.curr_player = PLAYER1 and state.singleplayer = '1' else INITIAL_SNAKE_HEAD_POSITIONS(0) when state.curr_player = PLAYER1 else INITIAL_SNAKE_HEAD_POSITIONS(1); -- singleplayer placement is random, multiplayer not

    snake_ctrl_wires.init_len <= x"8" when state.main_state = IDLE else std_logic_vector(to_unsigned(INITIAL_BODY_LENGTH(0), 4)) when state.curr_player = PLAYER1 else std_logic_vector(to_unsigned(INITIAL_BODY_LENGTH(1), 4));

    ----------------------------------------------------------------------------

    sync: process (clk, res_n)
    begin
        if (res_n = '0') then
            state <= RESET_STATE;
        elsif (rising_edge(clk)) then
            state <= next_state;
        end if;
    end process;

    next_state_logic: process (all)
        variable tmp_dir                : direction_t;
        variable new_pos_p1, new_pos_p2 : vec2d_t;

        pure function get_new_head_pos(prev_pos : vec2d_t; new_dir : direction_t) return vec2d_t is
        begin
            return move_vec2d_and_wrap(
                prev_pos,
                new_dir,
                SNAKE_FIELD_WIDTH - 1,
                SNAKE_FIELD_HEIGHT - 2
            );
        end function;
    begin
        next_state <= state;

        tmp_dir := state.movement_direction_p1;
        new_pos_p1 := get_new_head_pos(state.head_pos_p1, state.movement_direction_p1);
        new_pos_p2 := get_new_head_pos(state.head_pos_p2, state.movement_direction_p2);

        load_seed <= '0';
        gfx_init_ctrl.initializer_start <= '0';

        next_state.gfx_permission <= GFX_PERM_GAME_FIELD;
        next_state.ram_permission <= RAM_PERM_SNAKE_CTRL;

        snake_ctrl_wires.move_head <= '0';
        snake_ctrl_wires.move_tail <= '0';
        snake_ctrl_wires.init <= '0';

        snake_game_field_ctrl.draw <= '0';
        snake_game_field_ctrl.clear <= '0';

        pause_game_over_screen_ctrl.draw_game_over <= '0';
        pause_game_over_screen_ctrl.draw_pause <= '0';

        scoreboard_ctrl.draw <= '0';
        scoreboard_ctrl.check_collision <= '0';
        scoreboard_ctrl.reset_scores <= '0';
        scoreboard_ctrl.save_highscore <= '0';

        ram_coll_det.ram_rd <= '0';
        ram_coll_det.ram_rd_pos <= NULL_VEC;

        food_controller_ctrl.search_new_pos <= '0';
        food_controller_ctrl.write_new_food <= '0';

        case state.state is
            when GFX_INIT =>
                next_state.gfx_permission <= GFX_PERM_INIT;
                gfx_init_ctrl.initializer_start <= '1';
                next_state.state <= WAIT_GFX_INIT;

            when WAIT_GFX_INIT =>
                next_state.gfx_permission <= GFX_PERM_INIT;
                if not gfx_init_ctrl.initializer_busy then
                    next_state.state <= RESET;
                end if;

            when RESET =>
                -- TODO: Add other resets
                load_seed <= '1';
                scoreboard_ctrl.reset_scores <= '1';
                next_state.state <= CLEAR_GAME_FIELD;
                next_state.frame_counter <= (others => '0');

            when CLEAR_GAME_FIELD =>
                if state.singleplayer then
                    scoreboard_ctrl.save_highscore <= '1';
                end if;
                if not snake_game_field_ctrl.busy then
                    snake_game_field_ctrl.clear <= '1';
                    next_state.state <= WAIT_CLEAR_GAME_FIELD;
                end if;

            when WAIT_CLEAR_GAME_FIELD =>
                if not snake_game_field_ctrl.busy then
                    if state.main_state = RUNNING then
                        next_state.state <= RUNNING_PROCESS_INPUTS;
                    elsif state.main_state = RUNNING_START then
                        next_state.main_state <= RUNNING;
                        next_state.state <= RUNNING_PLACE_SNAKE;
                    elsif state.main_state = GAME_OVER or state.main_state = GAME_OVER_START then
                        next_state.state <= GAME_OVER_DRAW;
                    else
                        next_state.state <= IDLE_PLACE_SNAKE;
                        next_state.main_state <= IDLE;
                    end if;
                end if;

                ----------------------------------------------
                --             DRAWING TO SCREEN            --
                ----------------------------------------------
            when DO_FRAME_SYNC =>
                -- reset starting frame
                next_state.starting <= '0';

                -- wait until game field is done drawing
                next_state.gfx_permission <= GFX_PERM_GAME_FIELD;

                if not snake_game_field_ctrl.busy then
                    if not gfx_cmd_full then
                        next_state.gfx_permission <= GFX_PERM_DRAW;
                        next_state.draw_gfx_ctrl.gfx_cmd_wr <= '1';
                        next_state.draw_gfx_ctrl.gfx_cmd <= create_gfx_instr(
                            opcode => OPCODE_DISPLAY_BMP,
                            fs     => '1',
                            bmpidx => (0 => state.frame_buffer_selector, others => '0')
                        );
                        next_state.state <= WAIT_FRAME_SYNC;
                    end if;
                end if;

            when WAIT_FRAME_SYNC =>
                next_state.gfx_permission <= GFX_PERM_GAME_FIELD;

                if (gfx_frame_sync = '1') then
                    next_state.state <= SWITCH_FRAME_BUFFER;
                    next_state.frame_buffer_selector <= not state.frame_buffer_selector;
                end if;

            when SWITCH_FRAME_BUFFER =>
                if not gfx_cmd_full then
                    next_state.gfx_permission <= GFX_PERM_DRAW;
                    next_state.draw_gfx_ctrl.gfx_cmd_wr <= '1';
                    next_state.draw_gfx_ctrl.gfx_cmd <= create_gfx_instr(
                        opcode => OPCODE_ACTIVATE_BMP,
                        bmpidx => (0 => state.frame_buffer_selector, others => '0')
                    );
                    next_state.state <= CLEAR_SCREEN;
                end if;

            when CLEAR_SCREEN =>
                if not gfx_cmd_full then
                    next_state.gfx_permission <= GFX_PERM_DRAW;
                    next_state.draw_gfx_ctrl.gfx_cmd_wr <= '1';
                    next_state.draw_gfx_ctrl.gfx_cmd <= create_gfx_instr(
                        opcode => OPCODE_CLEAR,
                        cs     => CS_PRIMARY
                    );

                    -- go to the right draw states
                    if state.main_state = PAUSE_START then
                        next_state.state <= PAUSE_DRAW;
                        next_state.main_state <= PAUSE;
                    elsif state.main_state = PAUSE then
                        next_state.state <= PAUSE_PROCESS_INPUTS;
                    elsif state.main_state = GAME_OVER_START then
                        next_state.state <= GAME_OVER_PROCESS_INPUTS;
                        next_state.main_state <= GAME_OVER;
                    elsif state.main_state = GAME_OVER then
                        next_state.state <= GAME_OVER_DRAW;
                    else
                        next_state.state <= WAIT_FRAMES;
                    end if;
                end if;

            when WAIT_FRAMES =>
                if (unsigned(state.frame_counter) = FRAMES_BETWEEN_MOVEMENTS - 1) then
                    next_state.frame_counter <= (others => '0');
                    if state.main_state = RUNNING then
                        next_state.state <= RUNNING_PROCESS_INPUTS;
                    elsif state.main_state = RUNNING_START then
                        next_state.main_state <= RUNNING;
                        next_state.state <= RUNNING_PLACE_SNAKE;
                    else
                        next_state.state <= IDLE_CHECK_GAME_START;
                    end if;
                else
                    next_state.frame_counter <= std_logic_vector(unsigned(state.frame_counter) + 1);
                    if state.main_state = RUNNING then
                        next_state.state <= RUNNING_DRAW_GAME;
                    else
                        next_state.state <= IDLE_DRAW_GAME;
                    end if;
                end if;

                ----------------------------------------------
                --             IDLE MODE STATES             --
                ----------------------------------------------
            when IDLE_PLACE_SNAKE =>
                next_state.ram_permission <= RAM_PERM_SNAKE_CTRL;
                snake_ctrl_wires.init <= '1';
                next_state.curr_player <= PLAYER1;

                next_state.movement_direction_p1 <= LEFT;
                next_state.state <= IDLE_PLACE_SNAKE_WAIT;

            when IDLE_PLACE_SNAKE_WAIT =>
                if (snake_ctrl_wires.done = '1') then
                    next_state.state <= IDLE_DRAW_GAME;
                end if;

            when IDLE_CHECK_GAME_START =>
                if cntrl1.btn_a or cntrl2.btn_a then
                    next_state.state <= RUNNING_START_SINGLEPLAYER;
                    next_state.main_state <= RUNNING_START;
                elsif cntrl1.btn_b or cntrl2.btn_b then
                    next_state.state <= RUNNING_START_MULTIPLAYER;
                    next_state.main_state <= RUNNING_START;
                else
                    next_state.state <= IDLE_PROCESS_INPUTS;
                end if;

            when IDLE_PROCESS_INPUTS =>
                case rnd_vec is
                    when "00" => tmp_dir := UP;
                    when "01" => tmp_dir := DOWN;
                    when "10" => tmp_dir := LEFT;
                    when others => tmp_dir := RIGHT;
                end case;

                -- skip move is used to prevent the snake from running back where it comes from
                -- that the snake does not collide with itself, it only changes direction every two steps
                next_state.skip_move <= state.skip_move + 1;

                if (tmp_dir /= not state.movement_direction_p1 and state.skip_move = "10") then
                    next_state.movement_direction_p1 <= tmp_dir;
                    next_state.skip_move <= "00";
                end if;
                next_state.state <= IDLE_MOVE_HEAD;

            when IDLE_MOVE_HEAD =>
                next_state.ram_permission <= RAM_PERM_SNAKE_CTRL;
                snake_ctrl_wires.move_head <= '1';
                next_state.state <= IDLE_WAIT_MOVE_HEAD;

            when IDLE_WAIT_MOVE_HEAD =>
                if (snake_ctrl_wires.done = '1') then
                    next_state.state <= IDLE_MOVE_TAIL;
                end if;

            when IDLE_MOVE_TAIL =>
                snake_ctrl_wires.move_tail <= '1';
                next_state.state <= IDLE_WAIT_MOVE_TAIL;

            when IDLE_WAIT_MOVE_TAIL =>
                if (snake_ctrl_wires.done = '1') then
                    next_state.state <= IDLE_DRAW_GAME;
                end if;

            when IDLE_DRAW_GAME =>
                next_state.gfx_permission <= GFX_PERM_GAME_FIELD;
                if not snake_game_field_ctrl.busy then
                    snake_game_field_ctrl.draw <= '1';
                    next_state.state <= DO_FRAME_SYNC;
                end if;

                ----------------------------------------------
                --                GAME OVER                 --
                ----------------------------------------------
            when GAME_OVER_DRAW =>
                next_state.gfx_permission <= GFX_PERM_PAUSE;
                pause_game_over_screen_ctrl.draw_game_over <= '1';
                next_state.state <= GAME_OVER_WAIT_DRAW;

            when GAME_OVER_WAIT_DRAW =>
                next_state.gfx_permission <= GFX_PERM_PAUSE;
                if not pause_game_over_screen_ctrl.busy then
                    next_state.state <= DO_FRAME_SYNC;
                end if;

            when GAME_OVER_PROCESS_INPUTS =>
                if cntrl1.btn_start or cntrl2.btn_start then
                    scoreboard_ctrl.reset_scores <= '1';
                    next_state.collision.occured <= '0';
                    next_state.state <= GAME_OVER_WAIT_BUTTON_RELEASE;
                end if;

            when GAME_OVER_WAIT_BUTTON_RELEASE =>
                if not (cntrl1.btn_start or cntrl2.btn_start) then
                    next_state.state <= CLEAR_GAME_FIELD;
                    next_state.main_state <= IDLE;
                end if;

                ----------------------------------------------
                --                  PAUSED                  --
                ----------------------------------------------
            when PAUSE_DRAW =>
                next_state.gfx_permission <= GFX_PERM_PAUSE;
                pause_game_over_screen_ctrl.draw_pause <= '1';
                next_state.state <= PAUSE_WAIT_DRAW;
                next_state.main_state <= PAUSE;

            when PAUSE_WAIT_DRAW =>
                next_state.gfx_permission <= GFX_PERM_PAUSE;
                if not pause_game_over_screen_ctrl.busy and not (cntrl1.btn_start or cntrl2.btn_start) then
                    -- only state transition if both buttons got released
                    next_state.state <= DO_FRAME_SYNC;
                end if;

            when PAUSE_PROCESS_INPUTS =>
                if cntrl1.btn_start or cntrl2.btn_start then
                    next_state.state <= PAUSE_WAIT_BUTTON_RELEASE;
                end if;

            when PAUSE_WAIT_BUTTON_RELEASE =>
                if not (cntrl1.btn_start or cntrl2.btn_start) then
                    next_state.state <= RUNNING_DRAW_GAME;
                    next_state.main_state <= RUNNING;
                end if;

                ----------------------------------------------
                --            GAME RUNNING STATES           --
                ----------------------------------------------
            when RUNNING_START_SINGLEPLAYER =>
                next_state.starting <= '1';
                next_state.main_state <= RUNNING_START;
                next_state.singleplayer <= '1';
                next_state.curr_player <= PLAYER1;
                next_state.state <= CLEAR_GAME_FIELD;

            when RUNNING_START_MULTIPLAYER =>
                next_state.starting <= '1';
                next_state.main_state <= RUNNING_START;
                next_state.singleplayer <= '0';
                next_state.curr_player <= PLAYER1;
                next_state.state <= CLEAR_GAME_FIELD;

            when RUNNING_PLACE_SNAKE =>
                snake_ctrl_wires.init <= '1';
                next_state.movement_direction_p1 <= LEFT;
                next_state.movement_direction_p2 <= LEFT;

                if state.curr_player = PLAYER1 then
                    if state.singleplayer then
                        next_state.head_pos_p1 <= make_pos_in_bounds_of_gamefield(init_head_pos1_gen);
                    else
                        next_state.head_pos_p1 <= INITIAL_SNAKE_HEAD_POSITIONS(0);
                    end if;
                else
                    next_state.head_pos_p2 <= INITIAL_SNAKE_HEAD_POSITIONS(1);
                end if;

                next_state.state <= RUNNING_PLACE_SNAKE_WAIT;

            when RUNNING_PLACE_SNAKE_WAIT =>
                if (snake_ctrl_wires.done = '1') then
                    if state.singleplayer = '1' or state.curr_player = PLAYER2 then
                        next_state.curr_player <= PLAYER1;
                        next_state.state <= RUNNING_PROCESS_INPUTS;
                    else
                        next_state.curr_player <= PLAYER2;
                        next_state.state <= RUNNING_PLACE_SNAKE;
                    end if;
                end if;

            when RUNNING_PROCESS_INPUTS =>
                next_state.state <= RUNNING_CHECK_COLLISION_READ_P1;
                next_state.curr_player <= PLAYER1;

                if cntrl1.btn_start or cntrl2.btn_start then
                    next_state.state <= CLEAR_SCREEN;
                    next_state.main_state <= PAUSE_START;
                end if;

                case state.movement_direction_p1 is
                    when UP | DOWN =>
                        if cntrl1.btn_left or (state.singleplayer and cntrl2.btn_left) then
                            next_state.movement_direction_p1 <= LEFT;
                        elsif cntrl1.btn_right or (state.singleplayer and cntrl2.btn_right) then
                            next_state.movement_direction_p1 <= RIGHT;
                        end if;
                    when LEFT | RIGHT =>
                        if cntrl1.btn_up or (state.singleplayer and cntrl2.btn_up) then
                            next_state.movement_direction_p1 <= UP;
                        elsif cntrl1.btn_down or (state.singleplayer and cntrl2.btn_down) then
                            next_state.movement_direction_p1 <= DOWN;
                        end if;
                end case;

                case state.movement_direction_p2 is
                    when UP | DOWN =>
                        if cntrl2.btn_left then
                            next_state.movement_direction_p2 <= LEFT;
                        elsif cntrl2.btn_right then
                            next_state.movement_direction_p2 <= RIGHT;
                        end if;
                    when LEFT | RIGHT =>
                        if cntrl2.btn_up then
                            next_state.movement_direction_p2 <= UP;
                        elsif cntrl2.btn_down then
                            next_state.movement_direction_p2 <= DOWN;
                        end if;
                end case;

                next_state.ram_permission <= RAM_PERM_COLL_DETECTION;

            when RUNNING_CHECK_COLLISION_READ_P1 =>
                next_state.ram_permission <= RAM_PERM_COLL_DETECTION;
                ram_coll_det.ram_rd <= '1';
                ram_coll_det.ram_rd_pos <= new_pos_p1;
                next_state.state <= RUNNING_CHECK_COLLISION_READ_P2;

            when RUNNING_CHECK_COLLISION_READ_P2 =>
                next_state.ram_permission <= RAM_PERM_COLL_DETECTION;
                next_state.mem_at_new_p1 <= snake_game_field_ctrl.ram_rd_data;

                ram_coll_det.ram_rd <= '1';
                ram_coll_det.ram_rd_pos <= new_pos_p2;
                next_state.state <= RUNNING_CHECK_COLLISION_MULTIPLAYER;

                if state.singleplayer then
                    next_state.state <= RUNNING_CHECK_COLLISION_SINGLEPLAYER;
                end if;

            when RUNNING_CHECK_COLLISION_SINGLEPLAYER =>
                next_state.state <= RUNNING_MOVE_TAIL;
                ram_coll_det.ram_rd <= '0';
                next_state.collision.occured <= '0';

                case get_entry_type(state.mem_at_new_p1) is
                    when FOOD_REGULAR =>
                        next_state.collision.occured <= '1';
                        next_state.collision.source_player <= PLAYER1;
                        next_state.collision.type_of_coll <= COLL_FOOD_NORMAL;

                    when FOOD_SPECIAL =>
                        next_state.collision.occured <= '1';
                        next_state.collision.source_player <= PLAYER1;
                        next_state.collision.type_of_coll <= COLL_FOOD_SPECIAL;

                    when SNAKE_HEAD | SNAKE_STRAIGHT | SNAKE_BEND =>
                        -- p1 ran into obstacle
                        next_state.collision.occured <= '1';
                        next_state.collision.source_player <= PLAYER1;
                        next_state.collision.type_of_coll <= NORMAL_COLL;

                        next_state.state <= CLEAR_GAME_FIELD;
                        next_state.main_state <= GAME_OVER_START;

                    when others =>
                        null;
                end case;

            when RUNNING_CHECK_COLLISION_MULTIPLAYER =>
                -- check if there is something at the next position
                -- next_state.ram_permission <= RAM_PERM_SNAKE_CTRL;
                next_state.state <= RUNNING_MOVE_TAIL;
                ram_coll_det.ram_rd <= '0';
                next_state.collision.occured <= '0';

                -- heads ran to the same field
                if new_pos_p1 = new_pos_p2 then
                    -- report "1: " & to_string(new_pos_p1.x) & ", " & to_string(new_pos_p1.y) & " = " & to_string(new_pos_p2.x) & ", " & to_string(new_pos_p2.y);
                    next_state.collision.occured <= '1';
                    next_state.collision.type_of_coll <= HEADON_COLL;

                    next_state.state <= CLEAR_GAME_FIELD;
                    next_state.main_state <= GAME_OVER_START;
                end if;

                -- food collisions
                if get_entry_type(state.mem_at_new_p1) = FOOD_REGULAR then
                    next_state.collision.occured <= '1';
                    next_state.collision.source_player <= PLAYER1;
                    next_state.collision.type_of_coll <= COLL_FOOD_NORMAL;

                elsif get_entry_type(state.mem_at_new_p1) = FOOD_SPECIAL then
                    next_state.collision.occured <= '1';
                    next_state.collision.source_player <= PLAYER1;
                    next_state.collision.type_of_coll <= COLL_FOOD_SPECIAL;

                elsif get_entry_type(snake_game_field_ctrl.ram_rd_data) = FOOD_REGULAR then
                    next_state.collision.occured <= '1';
                    next_state.collision.source_player <= PLAYER2;
                    next_state.collision.type_of_coll <= COLL_FOOD_NORMAL;

                elsif get_entry_type(snake_game_field_ctrl.ram_rd_data) = FOOD_SPECIAL then
                    next_state.collision.occured <= '1';
                    next_state.collision.source_player <= PLAYER2;
                    next_state.collision.type_of_coll <= COLL_FOOD_SPECIAL;
                end if;

                -- obstacle collisions
                if (get_entry_type(state.mem_at_new_p1) = SNAKE_HEAD or get_entry_type(state.mem_at_new_p1) = SNAKE_STRAIGHT or get_entry_type(state.mem_at_new_p1) = SNAKE_BEND) and (get_entry_type(snake_game_field_ctrl.ram_rd_data) = SNAKE_HEAD or get_entry_type(snake_game_field_ctrl.ram_rd_data) = SNAKE_STRAIGHT or get_entry_type(snake_game_field_ctrl.ram_rd_data) = SNAKE_BEND) then
                    -- p1 and p2 ran into obstacle at the same time
                    -- report "2";
                    next_state.collision.occured <= '1';
                    next_state.collision.type_of_coll <= HEADON_COLL;

                    next_state.state <= CLEAR_GAME_FIELD;
                    next_state.main_state <= GAME_OVER_START;

                elsif get_entry_type(state.mem_at_new_p1) = SNAKE_HEAD or get_entry_type(state.mem_at_new_p1) = SNAKE_STRAIGHT or get_entry_type(state.mem_at_new_p1) = SNAKE_BEND then
                    -- p1 ran into obstacle
                    next_state.collision.occured <= '1';
                    next_state.collision.source_player <= PLAYER1;
                    next_state.collision.type_of_coll <= NORMAL_COLL;

                    next_state.state <= CLEAR_GAME_FIELD;
                    next_state.main_state <= GAME_OVER_START;

                elsif get_entry_type(snake_game_field_ctrl.ram_rd_data) = SNAKE_HEAD or get_entry_type(snake_game_field_ctrl.ram_rd_data) = SNAKE_STRAIGHT or get_entry_type(snake_game_field_ctrl.ram_rd_data) = SNAKE_BEND then
                    -- p2 ran into obstacle
                    next_state.collision.occured <= '1';
                    next_state.collision.source_player <= PLAYER2;
                    next_state.collision.type_of_coll <= NORMAL_COLL;

                    next_state.state <= CLEAR_GAME_FIELD;
                    next_state.main_state <= GAME_OVER_START;
                end if;

                -- tail collision in case the other snake ate something and would get longer
                if get_entry_type(state.mem_at_new_p1) = SNAKE_TAIL and (get_entry_type(snake_game_field_ctrl.ram_rd_data) = FOOD_REGULAR or get_entry_type(snake_game_field_ctrl.ram_rd_data) = FOOD_SPECIAL) then
                    -- p1 ran into tail of eating p2
                    next_state.collision.occured <= '1';
                    next_state.collision.source_player <= PLAYER1;
                    next_state.collision.type_of_coll <= NORMAL_COLL;

                    next_state.state <= CLEAR_GAME_FIELD;
                    next_state.main_state <= GAME_OVER_START;
                elsif get_entry_type(snake_game_field_ctrl.ram_rd_data) = SNAKE_TAIL and (get_entry_type(state.mem_at_new_p1) = FOOD_REGULAR or get_entry_type(state.mem_at_new_p1) = FOOD_SPECIAL) then
                    -- p2 ran into tail of eating p1
                    next_state.collision.occured <= '1';
                    next_state.collision.source_player <= PLAYER2;
                    next_state.collision.type_of_coll <= NORMAL_COLL;

                    next_state.state <= CLEAR_GAME_FIELD;
                    next_state.main_state <= GAME_OVER_START;
                end if;

            when RUNNING_DRAW_GAME =>
                if not snake_game_field_ctrl.busy then
                    snake_game_field_ctrl.draw <= '1';
                    next_state.state <= RUNNING_DRAW_GAME_WAIT;
                end if;

            when RUNNING_DRAW_GAME_WAIT =>
                if not snake_game_field_ctrl.busy then
                    next_state.state <= RUNNING_DRAW_SCOREBOARD;
                end if;

            when RUNNING_DRAW_SCOREBOARD =>
                next_state.gfx_permission <= GFX_PERM_SCOREBOARD;
                scoreboard_ctrl.draw <= '1';
                next_state.state <= RUNNING_DRAW_SCOREBOARD_WAIT;

            when RUNNING_DRAW_SCOREBOARD_WAIT =>
                next_state.gfx_permission <= GFX_PERM_SCOREBOARD;
                if not scoreboard_ctrl.busy then
                    next_state.state <= DO_FRAME_SYNC;
                end if;

            when RUNNING_MOVE_TAIL =>
                if state.curr_player = PLAYER1 then
                    scoreboard_ctrl.check_collision <= '1'; -- scoreboard should check collision here
                end if;

                next_state.ram_permission <= RAM_PERM_SNAKE_CTRL;

                if state.collision.occured = '1' and (state.collision.type_of_coll = COLL_FOOD_NORMAL or state.collision.type_of_coll = COLL_FOOD_SPECIAL) and state.collision.source_player = state.curr_player then
                    -- skip moving tail to get longer
                    next_state.state <= RUNNING_MOVE_TAIL;
                    next_state.curr_player <= PLAYER2;

                    if state.singleplayer = '1' or (state.singleplayer = '0' and state.curr_player = PLAYER2) then
                        -- player 1 already drawn
                        next_state.curr_player <= PLAYER1;
                        next_state.state <= RUNNING_MOVE_HEAD;
                    end if;
                else
                    snake_ctrl_wires.move_tail <= '1';
                    next_state.state <= RUNNING_WAIT_MOVE_TAIL;
                end if;

            when RUNNING_WAIT_MOVE_TAIL =>
                next_state.ram_permission <= RAM_PERM_SNAKE_CTRL;
                if (snake_ctrl_wires.done = '1') then
                    next_state.state <= RUNNING_MOVE_TAIL;
                    next_state.curr_player <= PLAYER2;

                    if state.singleplayer = '1' or (state.singleplayer = '0' and state.curr_player = PLAYER2) then
                        -- player 1 already drawn
                        next_state.curr_player <= PLAYER1;
                        next_state.state <= RUNNING_MOVE_HEAD;
                    end if;
                end if;

            when RUNNING_MOVE_HEAD =>
                next_state.ram_permission <= RAM_PERM_SNAKE_CTRL;
                snake_ctrl_wires.move_head <= '1';

                if state.curr_player = PLAYER1 then
                    next_state.head_pos_p1 <= new_pos_p1;
                else
                    next_state.head_pos_p2 <= new_pos_p2;
                end if;

                next_state.state <= RUNNING_WAIT_MOVE_HEAD;

            when RUNNING_WAIT_MOVE_HEAD =>
                next_state.ram_permission <= RAM_PERM_SNAKE_CTRL;
                if (snake_ctrl_wires.done = '1') then
                    if state.singleplayer = '1' or state.curr_player = PLAYER2 then
                        -- both players have been drawn
                        next_state.curr_player <= PLAYER1;
                        next_state.state <= FOOD_SEARCH_START;
                    else
                        -- player 2 has to be drawn
                        next_state.curr_player <= PLAYER2;
                        next_state.state <= RUNNING_MOVE_HEAD;
                    end if;
                end if;

                ----------------------------------------------
                --                   FOOD                   --
                ----------------------------------------------
            when FOOD_SEARCH_START =>
                next_state.ram_permission <= RAM_PERM_FOOD_CONTROLLER;
                next_state.state <= FOOD_SEARCH;

            when FOOD_SEARCH =>
                next_state.ram_permission <= RAM_PERM_FOOD_CONTROLLER;
                food_controller_ctrl.search_new_pos <= '1';
                next_state.state <= FOOD_SEARCH_WAIT;

            when FOOD_SEARCH_WAIT =>
                next_state.ram_permission <= RAM_PERM_FOOD_CONTROLLER;
                if not food_controller_ctrl.busy then
                    next_state.state <= FOOD_PLACE_START;
                end if;

            when FOOD_PLACE_START =>
                next_state.ram_permission <= RAM_PERM_FOOD_CONTROLLER;
                next_state.state <= FOOD_PLACE;

            when FOOD_PLACE =>
                next_state.ram_permission <= RAM_PERM_FOOD_CONTROLLER;
                food_controller_ctrl.write_new_food <= '1';
                next_state.state <= FOOD_PLACE_WAIT;

            when FOOD_PLACE_WAIT =>
                next_state.ram_permission <= RAM_PERM_FOOD_CONTROLLER;
                if not food_controller_ctrl.busy then
                    next_state.state <= RUNNING_DRAW_GAME;
                end if;

                ----------------------------------------------
                --                  OTHERS                  --
                ----------------------------------------------
            when others => null;

        end case;
    end process;

    scoreboard_inst: scoreboard
        port map (
            clk             => clk,
            res_n           => res_n,
            busy            => scoreboard_ctrl.busy,
            gfx_cmd         => scoreboard_ctrl.gfx_cmd,
            gfx_cmd_wr      => scoreboard_ctrl.gfx_cmd_wr,
            gfx_cmd_full    => gfx_cmd_full,
            collision       => state.collision,
            check_collision => scoreboard_ctrl.check_collision,
            singleplayer    => state.singleplayer,
            draw            => scoreboard_ctrl.draw,
            save_highscore  => scoreboard_ctrl.save_highscore,
            reset_scores    => scoreboard_ctrl.reset_scores,
            score           => scoreboard_ctrl.score
        );

    pause_game_over_screen_inst: pause_game_over_screen
        port map (
            clk            => clk,
            res_n          => res_n,
            busy           => pause_game_over_screen_ctrl.busy,
            gfx_cmd        => pause_game_over_screen_ctrl.gfx_cmd,
            gfx_cmd_wr     => pause_game_over_screen_ctrl.gfx_cmd_wr,
            gfx_cmd_full   => gfx_cmd_full,
            singleplayer   => state.singleplayer,
            draw_pause     => pause_game_over_screen_ctrl.draw_pause,
            draw_game_over => pause_game_over_screen_ctrl.draw_game_over,
            score          => scoreboard_ctrl.score,
            collision      => state.collision
        );

    food_controller_inst: food_controller
        port map (
            clk              => clk,
            res_n            => res_n,
            busy             => food_controller_ctrl.busy,
            load_random_seed => load_seed,
            ram_rd           => food_controller_ctrl.ram_rd,
            ram_rd_pos       => food_controller_ctrl.ram_rd_pos,
            ram_rd_data      => snake_game_field_ctrl.ram_rd_data,
            ram_wr           => food_controller_ctrl.ram_wr,
            ram_wr_pos       => food_controller_ctrl.ram_wr_pos,
            ram_wr_data      => food_controller_ctrl.ram_wr_data,
            start            => state.starting,
            pause            => food_controller_ctrl.pause,
            search_new_pos   => food_controller_ctrl.search_new_pos,
            write_new_food   => food_controller_ctrl.write_new_food,
            collision        => state.collision
        );

    game_field_inst: snake_game_field
        port map (
            clk          => clk,
            res_n        => res_n,
            draw         => snake_game_field_ctrl.draw,
            bmpidx       => "011",
            clear        => snake_game_field_ctrl.clear,
            busy         => snake_game_field_ctrl.busy,
            gfx_cmd      => snake_game_field_ctrl.gfx_cmd,
            gfx_wr       => snake_game_field_ctrl.gfx_cmd_wr,
            gfx_cmd_full => gfx_cmd_full,
            rd           => snake_game_field_ctrl.ram_rd,
            rd_pos       => snake_game_field_ctrl.ram_rd_pos,
            rd_data      => snake_game_field_ctrl.ram_rd_data,
            wr           => snake_game_field_ctrl.ram_wr,
            wr_pos       => snake_game_field_ctrl.ram_wr_pos,
            wr_data      => snake_game_field_ctrl.ram_wr_data
        );

    snake_ctrl_inst: snake_ctrl
        port map (
            clk                 => clk,
            res_n               => res_n,
            busy                => snake_game_field_ctrl.ram_busy,
            rd                  => snake_ctrl_wires.ram_rd,
            rd_pos              => snake_ctrl_wires.ram_rd_pos,
            rd_data             => snake_game_field_ctrl.ram_rd_data,
            wr                  => snake_ctrl_wires.ram_wr,
            wr_pos              => snake_ctrl_wires.ram_wr_pos,
            wr_data             => snake_ctrl_wires.ram_wr_data,
            player              => state.curr_player,
            init                => snake_ctrl_wires.init,
            init_head_position  => snake_ctrl_wires.init_head_position,
            init_head_direction => LEFT,
            init_body_length    => snake_ctrl_wires.init_len,
            movement_direction  => snake_ctrl_wires.movement_direction,
            move_head           => snake_ctrl_wires.move_head,
            move_tail           => snake_ctrl_wires.move_tail,
            done                => snake_ctrl_wires.done
        );

    prng_inst0: prng
        port map (
            clk       => clk,
            res_n     => res_n,
            load_seed => load_seed,
            seed      => (others => '0'),
            en        => '1',
            prdata    => rnd_vec(0)
        );

    prng_inst1: prng
        port map (
            clk       => clk,
            res_n     => res_n,
            load_seed => load_seed,
            seed      => x"42",
            en        => '1',
            prdata    => rnd_vec(1)
        );

    prng_vec_inst_x: prng_vec
        generic map (
            RND_VEC_WIDTH => VEC2D_X_WIDTH
        )
        port map (
            clk       => clk,
            res_n     => res_n,
            load_seed => load_seed,
            seed      => rnd_seed_1(7 downto 0),
            en        => '1',
            rnd_data  => init_head_pos1_gen.x
        );

    prng_vec_inst_y: prng_vec
        generic map (
            RND_VEC_WIDTH => VEC2D_Y_WIDTH
        )
        port map (
            clk       => clk,
            res_n     => res_n,
            load_seed => load_seed,
            seed      => rnd_seed_2(7 downto 0),
            en        => '1',
            rnd_data  => init_head_pos1_gen.y
        );

    game_audio_ctrl_inst: game_audio_ctrl
        port map (
            clk            => clk,
            res_n          => res_n,
            busy           => open, -- irrelevant since nobody else accesses the synth
            gfx_frame_sync => gfx_frame_sync,
            collision      => state.collision,
            synth_ctrl     => synth_ctrl
        );

    gfx_initializer: block
        signal instr_cnt     : integer := 0;
        signal instr_cnt_nxt : integer := 0;

        signal gfx_initializer_cmd_nxt : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0);
        signal is_running, running_nxt : std_logic;
    begin
        gfx_init_ctrl.initializer_busy <= is_running;

        registers: process (clk, res_n)
        begin
            if (res_n = '0') then
                instr_cnt <= 0;
                gfx_init_ctrl.initializer_cmd <= (others => '0');
                is_running <= '0';
            elsif (rising_edge(clk)) then
                gfx_init_ctrl.initializer_cmd <= gfx_initializer_cmd_nxt;
                instr_cnt <= instr_cnt_nxt;
                is_running <= running_nxt;
            end if;
        end process;

        next_state_logic_init: process (all)
        begin
            gfx_init_ctrl.initializer_cmd_wr <= '0';

            instr_cnt_nxt <= instr_cnt;
            gfx_initializer_cmd_nxt <= gfx_init_ctrl.initializer_cmd;
            running_nxt <= is_running;

            if (gfx_init_ctrl.initializer_start = '1') then
                instr_cnt_nxt <= 1;
                running_nxt <= '1';
                gfx_initializer_cmd_nxt <= GFX_INIT_CMDS(0);
            end if;

            if (is_running = '1') then
                if (gfx_cmd_full = '0') then
                    gfx_init_ctrl.initializer_cmd_wr <= '1';

                    if (instr_cnt = 0) then
                        running_nxt <= '0';
                    elsif (instr_cnt = GFX_INIT_CMDS'length - 1) then
                        instr_cnt_nxt <= 0;
                        gfx_initializer_cmd_nxt <= GFX_INIT_CMDS(instr_cnt);
                    else
                        gfx_initializer_cmd_nxt <= GFX_INIT_CMDS(instr_cnt);
                        instr_cnt_nxt <= instr_cnt + 1;
                    end if;
                end if;
            end if;
        end process;
    end block;

end architecture;
