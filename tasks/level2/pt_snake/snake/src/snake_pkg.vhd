library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.gfx_cmd_pkg.all;
use work.audio_ctrl_pkg.all;

package snake_pkg is

	type snake_ctrl_t is
	record
		btn_a : std_logic;
		btn_b : std_logic;
		btn_start : std_logic;
		btn_left : std_logic;
		btn_right : std_logic;
		btn_up : std_logic;
		btn_down : std_logic;
	end record;
	constant SNAKE_CTRL_NULL : snake_ctrl_t := (others => '0');

	component snake is
		port (
			clk   : in std_logic;
			res_n : in std_logic;
			gfx_cmd        : out std_logic_vector(GFX_CMD_WIDTH-1 downto 0);
			gfx_cmd_wr     : out std_logic;
			gfx_cmd_full   : in std_logic;
			gfx_rd_valid   : in std_logic;
			gfx_rd_data    : in std_logic_vector(15 downto 0);
			gfx_frame_sync : in std_logic;
			cntrl1 : in snake_ctrl_t;
			cntrl2 : in snake_ctrl_t;
			synth_ctrl : out synth_ctrl_vec_t(0 to 1)
		);
	end component;

	component precompiled_snake is
		port (
			clk   : in std_logic;
			res_n : in std_logic;
			gfx_cmd        : out std_logic_vector(GFX_CMD_WIDTH-1 downto 0);
			gfx_cmd_wr     : out std_logic;
			gfx_cmd_full   : in std_logic;
			gfx_rd_valid   : in std_logic;
			gfx_rd_data    : in std_logic_vector(15 downto 0);
			gfx_frame_sync : in std_logic;
			cntrl1 : in snake_ctrl_t;
			cntrl2 : in snake_ctrl_t;
			synth_ctrl : out synth_ctrl_vec_t(0 to 1)
		);
	end component;
end package;

