library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.math_pkg.all;
    use work.snake_ctrl_pkg.all;
    use work.snake_pkg.all;
    use work.snake_game_field_pkg.all;
    use work.gfx_cmd_pkg.all;
    use work.gfx_init_pkg.all;
    use work.snake_custom_pkg.all;

entity pause_game_over_screen_tb is
end entity;

architecture tb of pause_game_over_screen_tb is
    constant CLK_PERIOD : time := 10 ns;

    signal stop_clk   : boolean   := false;
    signal clk, res_n : std_logic := '0';

    signal busy           : std_logic;
    signal collision      : collision_t;
    signal score          : score_t;
    signal singleplayer   : std_logic;
    signal draw_pause     : std_logic := '0';
    signal draw_game_over : std_logic := '0';

    signal gfx_cmd_full     : std_logic := '0';
    signal gfx_init_running : std_logic;
    signal gfx_cmd_init     : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0);
    signal gfx_wr_init      : std_logic;
    signal gfx_cmd_in       : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0);
    signal gfx_wr_in        : std_logic;
    signal gfx_cmd_out      : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0);
    signal gfx_wr_out       : std_logic;
begin
    clk_stim: process is
    begin
        clk <= '0';
        wait for CLK_PERIOD / 2;
        clk <= '1';
        wait for CLK_PERIOD / 2;

        if stop_clk then
            wait;
        end if;
    end process;

    gfx_cmd_in <= gfx_cmd_init when gfx_init_running else gfx_cmd_out;
    gfx_wr_in  <= gfx_wr_init when gfx_init_running else gfx_wr_out;

    stim: process is
        procedure save_bmp is
        begin
            gfx_init_running <= '1';
            gfx_cmd_init <= create_gfx_instr(OPCODE_DISPLAY_BMP, fs => '0', bmpidx => "000");
            wait until rising_edge(clk);
            gfx_init_running <= '0';
        end procedure;

        procedure send_clear is
        begin
            gfx_init_running <= '1';
            gfx_cmd_init <= create_gfx_instr(OPCODE_CLEAR, cs => CS_PRIMARY);
            wait until rising_edge(clk);
            gfx_init_running <= '0';
        end procedure;
    begin
        report "START SIM";
        -- Initialize the gfx_cmd_interpreter (look at how this is done in the provided snake template)
        -- init the vram of the gfx_cmd_interpreter
        report "start init";
        res_n <= '0';
        gfx_init_running <= '1';
        gfx_wr_init <= '1';

        for inst_idx in GFX_INIT_CMDS'range loop
            gfx_cmd_init <= GFX_INIT_CMDS(inst_idx);
            wait until rising_edge(clk);
        end loop;

        -- gfx_cmd_init <= create_gfx_instr(OPCODE_ACTIVATE_BMP, bmpidx => "000");
        -- wait until rising_edge(clk);
        gfx_init_running <= '0';
        res_n <= '1';

        wait until rising_edge(clk);

        send_clear;

        report "init ready";

        collision <= (
            occured       => '1',
            source_player => PLAYER1,
            type_of_coll  => COLL_FOOD_NORMAL
        );

        score <= (
            curr_score_p1 => 25,
            curr_score_p2 => 35,
            highscore     => 120
        );

        -- singleplayer game over
        singleplayer <= '1';

        draw_game_over <= '1';
        wait until rising_edge(clk);
        draw_game_over <= '0';

        wait until rising_edge(clk);
        if busy then
            wait until not busy;
        end if;

        save_bmp;

        send_clear;

        -- multiplayer game over p2 wins
        singleplayer <= '0';

        collision.source_player <= PLAYER1;
        collision.type_of_coll <= NORMAL_COLL;

        collision.occured <= '1';
        draw_game_over <= '1';
        wait until rising_edge(clk);
        draw_game_over <= '0';
        collision.occured <= '0';

        wait until rising_edge(clk);
        if busy then
            wait until not busy;
        end if;

        save_bmp;

        send_clear;

        -- multiplayer game over tie
        singleplayer <= '0';

        collision.source_player <= PLAYER2;
        collision.type_of_coll <= NORMAL_COLL;

        collision.occured <= '1';
        draw_game_over <= '1';
        wait until rising_edge(clk);
        draw_game_over <= '0';
        collision.occured <= '0';

        wait until rising_edge(clk);
        if busy then
            wait until not busy;
        end if;

        save_bmp;

        send_clear;

        -- pause screen
        draw_pause <= '1';
        wait until rising_edge(clk);
        draw_pause <= '0';

        wait until rising_edge(clk);
        if busy then
            wait until not busy;
        end if;

        save_bmp;

        report "SIM DONE";
        stop_clk <= true;
        wait;
    end process;

    pause_game_over_screen_inst: pause_game_over_screen
        port map (
            clk            => clk,
            res_n          => res_n,
            busy           => busy,
            gfx_cmd        => gfx_cmd_out,
            gfx_cmd_wr     => gfx_wr_out,
            gfx_cmd_full   => gfx_cmd_full,
            singleplayer   => singleplayer,
            draw_pause     => draw_pause,
            draw_game_over => draw_game_over,
            score          => score,
            collision      => collision
        );

    gfx_cmd_interpreter_inst: entity work.gfx_cmd_interpreter
        generic map (
            OUTPUT_DIR => "./images/"
        )
        port map (
            clk            => clk,
            gfx_cmd        => gfx_cmd_in,
            gfx_cmd_wr     => gfx_wr_in,
            gfx_rd_valid   => open,
            gfx_rd_data    => open,
            gfx_frame_sync => open
        );

end architecture;
