library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.math_pkg.all;
    use work.snake_ctrl_pkg.all;
    use work.snake_pkg.all;
    use work.snake_game_field_pkg.all;
    use work.gfx_cmd_pkg.all;
    use work.gfx_init_pkg.all;
    use work.snake_custom_pkg.all;

entity scoreboard_tb is
end entity;

architecture tb of scoreboard_tb is
    constant CLK_PERIOD : time := 10 ns;

    signal stop_clk   : boolean   := false;
    signal clk, res_n : std_logic := '0';

    signal busy           : std_logic;
    signal collision      : collision_t;
    signal save_highscore : std_logic := '0';
    signal reset_scores   : std_logic := '0';
    signal score          : score_t;
    signal singleplayer   : std_logic := '0';
    signal draw           : std_logic := '0';

    signal gfx_cmd_full     : std_logic := '0';
    signal gfx_init_running : std_logic;
    signal gfx_cmd_init     : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0);
    signal gfx_wr_init      : std_logic;
    signal gfx_cmd_in       : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0);
    signal gfx_wr_in        : std_logic;
    signal gfx_cmd_out      : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0);
    signal gfx_wr_out       : std_logic;
begin
    clk_stim: process is
    begin
        clk <= '0';
        wait for CLK_PERIOD / 2;
        clk <= '1';
        wait for CLK_PERIOD / 2;

        if stop_clk then
            wait;
        end if;
    end process;

    gfx_cmd_in <= gfx_cmd_init when gfx_init_running else gfx_cmd_out;
    gfx_wr_in  <= gfx_wr_init when gfx_init_running else gfx_wr_out;

    stim: process is
        procedure save_bmp is
        begin
            gfx_init_running <= '1';
            gfx_cmd_init <= create_gfx_instr(OPCODE_DISPLAY_BMP, fs => '0', bmpidx => "000");
            wait until rising_edge(clk);
            gfx_init_running <= '0';
        end procedure;

        procedure send_clear is
        begin
            gfx_init_running <= '1';
            gfx_cmd_init <= create_gfx_instr(OPCODE_CLEAR, cs => CS_PRIMARY);
            wait until rising_edge(clk);
            gfx_init_running <= '0';
        end procedure;
    begin
        report "START SIM";
        -- Initialize the gfx_cmd_interpreter (look at how this is done in the provided snake template)
        -- init the vram of the gfx_cmd_interpreter
        report "start init";
        res_n <= '0';
        gfx_init_running <= '1';
        gfx_wr_init <= '1';

        for inst_idx in GFX_INIT_CMDS'range loop
            gfx_cmd_init <= GFX_INIT_CMDS(inst_idx);
            wait until rising_edge(clk);
        end loop;

        -- gfx_cmd_init <= create_gfx_instr(OPCODE_ACTIVATE_BMP, bmpidx => "000");
        -- wait until rising_edge(clk);
        gfx_init_running <= '0';
        res_n <= '1';

        wait until rising_edge(clk);

        send_clear;

        report "init ready";

        collision <= (
            occured       => '1',
            source_player => PLAYER1,
            type_of_coll  => COLL_FOOD_NORMAL
        );

        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);

        collision.type_of_coll <= COLL_FOOD_SPECIAL;

        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);

        report "Score P1: " & to_string(score.curr_score_p1);

        collision.occured <= '0';
        save_highscore <= '1';
        wait until rising_edge(clk);
        save_highscore <= '0';

        collision.occured <= '1';
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        collision.occured <= '0';

        singleplayer <= '1';
        draw <= '1';
        wait until rising_edge(clk);
        draw <= '0';

        wait until rising_edge(clk);
        if busy then
            wait until not busy;
        end if;

        save_bmp;

        send_clear;

        singleplayer <= '0';
        draw <= '1';
        wait until rising_edge(clk);
        draw <= '0';

        wait until rising_edge(clk);
        if busy then
            wait until not busy;
        end if;

        save_bmp;

        report "SIM DONE";
        stop_clk <= true;
        wait;
    end process;

    scoreboard_inst: scoreboard
        port map (
            clk             => clk,
            res_n           => res_n,
            busy            => busy,
            gfx_cmd         => gfx_cmd_out,
            gfx_cmd_wr      => gfx_wr_out,
            gfx_cmd_full    => gfx_cmd_full,
            collision       => collision,
            check_collision => collision.occured,
            singleplayer    => singleplayer,
            draw            => draw,
            save_highscore  => save_highscore,
            reset_scores    => reset_scores,
            score           => score
        );

    gfx_cmd_interpreter_inst: entity work.gfx_cmd_interpreter
        generic map (
            OUTPUT_DIR => "./images/"
        )
        port map (
            clk            => clk,
            gfx_cmd        => gfx_cmd_in,
            gfx_cmd_wr     => gfx_wr_in,
            gfx_rd_valid   => open,
            gfx_rd_data    => open,
            gfx_frame_sync => open
        );

end architecture;
