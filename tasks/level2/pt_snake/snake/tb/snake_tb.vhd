library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.math_pkg.all;
    use work.snake_ctrl_pkg.all;
    use work.snake_pkg.all;
    use work.snake_game_field_pkg.all;
    use work.gfx_cmd_pkg.all;
    use work.gfx_init_pkg.all;
    use work.audio_ctrl_pkg.all;

entity snake_tb is
end entity;

architecture tb of snake_tb is
    constant CLK_PERIOD : time := 10 ns;

    signal stop_clk   : boolean   := false;
    signal clk, res_n : std_logic := '0';

    signal gfx_cmd                      : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0);
    signal gfx_cmd_wr                   : std_logic;
    signal gfx_cmd_full                 : std_logic := '0';
    signal gfx_rd_valid, gfx_frame_sync : std_logic;
    signal gfx_rd_data                  : std_logic_vector(15 downto 0);

    signal synth_ctrl : synth_ctrl_vec_t(0 to 1);

    constant SNAKE_CTRL_NULL : snake_ctrl_t := (others => '0');
    signal cntrl1, cntrl2 : snake_ctrl_t := SNAKE_CTRL_NULL;

    -- Add further signals and constants as needed

begin
    clk_stim: process is
    begin
        clk <= '0';
        wait for CLK_PERIOD / 2;
        clk <= '1';
        wait for CLK_PERIOD / 2;

        if stop_clk then
            wait;
        end if;
    end process;

    stim: process is
        -- You might want to write some procedures for common functionality like waiting for x frames
        procedure reset_inputs is
        begin
            cntrl1 <= SNAKE_CTRL_NULL;
            cntrl2 <= SNAKE_CTRL_NULL;
        end procedure;

        procedure wait_next_input_frame is
        begin
            for i in 0 to 5 loop
                wait until gfx_frame_sync;
            end loop;
            wait for 1 ns;
            reset_inputs;
        end procedure;

        procedure test_multiplayer is
        begin
            -- wait until gfx_frame_sync;
            cntrl1.btn_b <= '1';
            wait_next_input_frame;
            cntrl1.btn_down <= '1';
            wait_next_input_frame;
            wait_next_input_frame;
            wait_next_input_frame;
            wait_next_input_frame;
            cntrl1.btn_right <= '1';

            for i in 0 to 15 loop
                wait_next_input_frame;
            end loop;

            cntrl1.btn_down <= '1';
            wait_next_input_frame;
            cntrl1.btn_left <= '1';

            for i in 0 to 6 loop
                wait_next_input_frame;
            end loop;

            cntrl1.btn_up <= '1';
            wait_next_input_frame;

            for i in 0 to 10 loop
                wait_next_input_frame;
            end loop;

            wait_next_input_frame;
            cntrl1.btn_down <= '1';
            wait_next_input_frame;
            cntrl1.btn_left <= '1';
            wait_next_input_frame;
            cntrl1.btn_up <= '1';
            wait_next_input_frame;
            cntrl1.btn_right <= '1';

            wait_next_input_frame;
            cntrl1.btn_start <= '1';

            wait_next_input_frame;
            cntrl1.btn_a <= '1';

            for i in 0 to 6 loop
                wait_next_input_frame;
            end loop;
        end procedure;

        procedure test_singleplayer_highscore is
        begin
            cntrl1.btn_a <= '1';
            wait_next_input_frame;

            for i in 0 to 7 loop
                wait_next_input_frame;
            end loop;

            cntrl1.btn_up <= '1';

            for i in 0 to 5 loop
                wait_next_input_frame;
            end loop;

            wait_next_input_frame;
            cntrl1.btn_down <= '1';
            wait_next_input_frame;
            cntrl1.btn_left <= '1';
            wait_next_input_frame;
            cntrl1.btn_up <= '1';
            wait_next_input_frame;
            cntrl1.btn_right <= '1';
            wait_next_input_frame;
            cntrl1.btn_down <= '1';

            wait for 10000 * CLK_PERIOD;
            reset_inputs;

            cntrl1.btn_start <= '1';
            wait for 10000 * CLK_PERIOD;
            reset_inputs;
            wait for 10000 * CLK_PERIOD;

            -- wait_next_input_frame;
            cntrl1.btn_a <= '1';

            for i in 0 to 6 loop
                wait_next_input_frame;
            end loop;
        end procedure;

        procedure test_idle is
        begin
            for i in 0 to 50 loop
                wait_next_input_frame;
            end loop;
        end procedure;

        procedure test_coll is
        begin
            cntrl1.btn_a <= '1';
            wait_next_input_frame;

            cntrl1.btn_down <= '1';
            wait_next_input_frame;
            wait_next_input_frame;
            wait_next_input_frame;
            cntrl1.btn_right <= '1';
            wait_next_input_frame;
            cntrl1.btn_up <= '1';
            -- wait_next_input_frame;
            cntrl1.btn_left <= '1';
            wait_next_input_frame;

            for i in 0 to 6 loop
                wait_next_input_frame;
            end loop;
        end procedure;
    begin
        report "SIM START";
        res_n <= '1';

        wait for 1 ns;

        test_coll;

        -- wait for 10 sec;
        report "SIM STOP";
        stop_clk <= true;
        wait;
    end process;

    uut: snake
        port map (
            clk            => clk,
            res_n          => res_n,
            gfx_cmd        => gfx_cmd,
            gfx_cmd_wr     => gfx_cmd_wr,
            gfx_cmd_full   => gfx_cmd_full,
            gfx_rd_valid   => gfx_rd_valid,
            gfx_rd_data    => gfx_rd_data,
            gfx_frame_sync => gfx_frame_sync,
            cntrl1         => cntrl1,
            cntrl2         => cntrl2,
            synth_ctrl     => synth_ctrl
        );

    gfx_cmd_interpreter_inst: entity work.gfx_cmd_interpreter
        generic map (
            OUTPUT_DIR => "./images/"
        )
        port map (
            clk            => clk,
            gfx_cmd        => gfx_cmd,
            gfx_cmd_wr     => gfx_cmd_wr,
            gfx_rd_valid   => gfx_rd_valid,
            gfx_rd_data    => gfx_rd_data,
            gfx_frame_sync => gfx_frame_sync
        );

end architecture;
