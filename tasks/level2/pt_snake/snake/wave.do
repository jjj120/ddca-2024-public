onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /snake_tb/uut/clk
add wave -noupdate /snake_tb/uut/res_n
add wave -noupdate /snake_tb/uut/gfx_cmd
add wave -noupdate /snake_tb/uut/gfx_cmd_wr
add wave -noupdate /snake_tb/uut/gfx_cmd_full
add wave -noupdate /snake_tb/uut/gfx_rd_valid
add wave -noupdate /snake_tb/uut/gfx_rd_data
add wave -noupdate /snake_tb/uut/gfx_frame_sync
add wave -noupdate /snake_tb/uut/cntrl1
add wave -noupdate /snake_tb/uut/cntrl2
add wave -noupdate /snake_tb/uut/synth_ctrl
add wave -noupdate -childformat {{/snake_tb/uut/state.frame_counter -radix unsigned}} -expand -subitemconfig {/snake_tb/uut/state.collision -expand /snake_tb/uut/state.frame_counter {-radix unsigned}} /snake_tb/uut/state
add wave -noupdate /snake_tb/uut/gfx_init_ctrl
add wave -noupdate -expand /snake_tb/uut/snake_game_field_ctrl
add wave -noupdate -childformat {{/snake_tb/uut/snake_ctrl_wires.wr_pos -radix unsigned} {/snake_tb/uut/snake_ctrl_wires.rd_pos -radix unsigned}} -expand -subitemconfig {/snake_tb/uut/snake_ctrl_wires.wr_pos {-radix unsigned} /snake_tb/uut/snake_ctrl_wires.rd_pos {-radix unsigned}} /snake_tb/uut/snake_ctrl_wires
add wave -noupdate /snake_tb/uut/rnd_vec
add wave -noupdate /snake_tb/uut/load_seed
add wave -noupdate -expand /snake_tb/uut/scoreboard_ctrl
add wave -noupdate -expand /snake_tb/uut/pause_game_over_screen_ctrl
add wave -noupdate -expand /snake_tb/uut/food_controller_ctrl
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 2} {0 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 190
configure wave -valuecolwidth 223
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {60972 ns}
