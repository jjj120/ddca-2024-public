onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /snake_tb/clk
add wave -noupdate /snake_tb/res_n
add wave -noupdate -radix binary /snake_tb/gfx_cmd
add wave -noupdate /snake_tb/gfx_cmd_wr
add wave -noupdate /snake_tb/gfx_cmd_full
add wave -noupdate /snake_tb/gfx_rd_valid
add wave -noupdate /snake_tb/gfx_frame_sync
add wave -noupdate /snake_tb/gfx_rd_data
add wave -noupdate /snake_tb/synth_ctrl
add wave -noupdate /snake_tb/cntrl1
add wave -noupdate /snake_tb/cntrl2
add wave -noupdate -expand -subitemconfig {/snake_tb/uut/game_field_inst/state.drawing_state {-height 16 -childformat {{/snake_tb/uut/game_field_inst/state.drawing_state.idx -radix unsigned}} -expand} /snake_tb/uut/game_field_inst/state.drawing_state.coord {-height 16 -childformat {{/snake_tb/uut/game_field_inst/state.drawing_state.coord.x -radix unsigned} {/snake_tb/uut/game_field_inst/state.drawing_state.coord.y -radix unsigned}} -expand} /snake_tb/uut/game_field_inst/state.drawing_state.coord.x {-height 16 -radix unsigned} /snake_tb/uut/game_field_inst/state.drawing_state.coord.y {-height 16 -radix unsigned} /snake_tb/uut/game_field_inst/state.drawing_state.idx {-height 16 -radix unsigned}} /snake_tb/uut/game_field_inst/state
add wave -noupdate /snake_tb/uut/game_field_inst/rd_pos
add wave -noupdate /snake_tb/uut/game_field_inst/wr_pos
add wave -noupdate /snake_tb/uut/game_field_inst/ram_rd1_addr
add wave -noupdate /snake_tb/uut/game_field_inst/ram_rd1_data
add wave -noupdate /snake_tb/uut/game_field_inst/ram_rd1
add wave -noupdate /snake_tb/uut/game_field_inst/ram_wr2_addr
add wave -noupdate /snake_tb/uut/game_field_inst/ram_wr2_data
add wave -noupdate /snake_tb/uut/game_field_inst/ram_wr2
add wave -noupdate /snake_tb/uut/draw
add wave -noupdate /snake_tb/uut/busy
add wave -noupdate -expand -subitemconfig {/snake_tb/uut/state.frame_counter -expand} /snake_tb/uut/state
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {58928 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 190
configure wave -valuecolwidth 223
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {60972 ns}
