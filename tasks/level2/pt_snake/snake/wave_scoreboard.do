onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /scoreboard_tb/clk
add wave -noupdate /scoreboard_tb/res_n
add wave -noupdate /scoreboard_tb/busy
add wave -noupdate /scoreboard_tb/collision
add wave -noupdate /scoreboard_tb/save_highscore
add wave -noupdate /scoreboard_tb/reset_scores
add wave -noupdate /scoreboard_tb/score
add wave -noupdate /scoreboard_tb/draw_singleplayer
add wave -noupdate /scoreboard_tb/draw_multiplayer
add wave -noupdate /scoreboard_tb/gfx_cmd_full
add wave -noupdate /scoreboard_tb/gfx_init_running
add wave -noupdate /scoreboard_tb/gfx_cmd_init
add wave -noupdate /scoreboard_tb/gfx_wr_init
add wave -noupdate /scoreboard_tb/gfx_cmd_in
add wave -noupdate /scoreboard_tb/gfx_wr_in
add wave -noupdate /scoreboard_tb/gfx_cmd_out
add wave -noupdate /scoreboard_tb/gfx_wr_out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {16945000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 190
configure wave -valuecolwidth 223
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {18060 ns}
