library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.math_real.all;

    use work.mem_pkg.all;
    use work.math_pkg.all;
    use work.gfx_cmd_pkg.all;
    use work.snake_ctrl_pkg.all;
    use work.snake_game_field_pkg.all;

entity snake_game_field is
    generic (
        PLAYER1_COLOR : color_t := COLOR_RED;
        PLAYER2_COLOR : color_t := COLOR_GREEN;
        FOOD_COLOR    : color_t := COLOR_YELLOW
    );
    port (
        clk          : in  std_logic;
        res_n        : in  std_logic;

        draw         : in  std_logic                                    := '0';
        bmpidx       : in  std_logic_vector(WIDTH_BMPIDX - 1 downto 0)  := (others => '0');
        clear        : in  std_logic                                    := '0';
        busy         : out std_logic                                    := '0';

        gfx_cmd      : out std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := (others => '0');
        gfx_wr       : out std_logic                                    := '0';
        gfx_cmd_full : in  std_logic                                    := '0';

        rd           : in  std_logic;
        rd_pos       : in  vec2d_t;
        rd_data      : out entry_t                                      := (others => '0');

        wr           : in  std_logic;
        wr_pos       : in  vec2d_t;
        wr_data      : in  entry_t
    );
end entity;

architecture arch of snake_game_field is
    constant SINGLE_FIELD_WIDTH  : integer := DISPLAY_WIDTH / SNAKE_FIELD_WIDTH;
    constant SINGLE_FIELD_HEIGHT : integer := DISPLAY_HEIGHT / SNAKE_FIELD_HEIGHT;
    constant BLANK_MEM_ENTRY     : entry_t := (others => '0');

    type current_state_t is (IDLE, DRAWING, CLEARING);
    type command_type_t is (SET_GP_1, SET_GP_2, SET_GP_3, READ_MEM, SET_EFFECT_COLOR, EXEC_BB_COLOR, EXEC_BB_2_COLOR);
    /*
		Steps needed to write to display: 
			- SET_GP_*:			Set the gp to the beginning of the next line (only on line starts!)
									- SET_GP_1: send command
									- SET_GP_2: send x coord
									- SET_GP_3: send y coord
			- READ_MEM: 		Send read command to RAM
			- SET_EFFECT_COLOR:	Set effect to "or PLAYER_COLOR"
			- EXEC_BB_COLOR:	Execute BB with the right image
			- EXEC_BB_2_COLOR:	Execute BB with the right image offset
									- increment counter to continue
									- set nextstate to READ_MEM
	*/
    type drawing_state_t is record
        coord     : vec2d_t;
        cmd       : command_type_t;
        idx       : bmpidx_t;
        mem_entry : entry_t;
    end record;

    type state_t is record
        state         : current_state_t;
        drawing_state : drawing_state_t;
    end record;

    constant RAM_ADDR_WIDTH : integer := log2c(SNAKE_FIELD_WIDTH * SNAKE_FIELD_HEIGHT);

    signal ram_read_internal_addr  : std_logic_vector(RAM_ADDR_WIDTH - 1 downto 0)         := (others => '0');
    signal ram_read_internal_en    : std_logic                                             := '0';
    signal ram_write_internal_addr : std_logic_vector(RAM_ADDR_WIDTH - 1 downto 0)         := (others => '0');
    signal ram_write_internal_data : std_logic_vector(SNAKE_FIELD_DATA_WIDTH - 1 downto 0) := (others => '0');
    signal ram_write_internal_en   : std_logic                                             := '0';

    -- signals for communicating with ram
    signal ram_rd1_addr : std_logic_vector(RAM_ADDR_WIDTH - 1 downto 0)         := (others => '0');
    signal ram_rd1_data : std_logic_vector(SNAKE_FIELD_DATA_WIDTH - 1 downto 0) := (others => '0');
    signal ram_rd1      : std_logic                                             := '0';
    signal ram_wr2_addr : std_logic_vector(RAM_ADDR_WIDTH - 1 downto 0)         := (others => '0');
    signal ram_wr2_data : std_logic_vector(SNAKE_FIELD_DATA_WIDTH - 1 downto 0) := (others => '0');
    signal ram_wr2      : std_logic                                             := '0';

    constant DEFAULT_STATE : state_t := (
        state         => IDLE,
        drawing_state => (
            coord     => NULL_VEC,
            cmd       => SET_GP_1,
            idx       => (others => '0'),
            mem_entry => BLANK_MEM_ENTRY
        )
    );

    signal state, next_state : state_t := DEFAULT_STATE;

    pure function vec_to_addr(addr : vec2d_t) return std_logic_vector is
    begin
        return std_logic_vector(to_unsigned(to_integer(unsigned(addr.y)) * SNAKE_FIELD_WIDTH + to_integer(unsigned(addr.x)), RAM_ADDR_WIDTH));
    end function;

begin
    dp_ram_1c1r1w_inst: dp_ram_1c1r1w
        generic map (
            ADDR_WIDTH => RAM_ADDR_WIDTH,
            DATA_WIDTH => SNAKE_FIELD_DATA_WIDTH
        )
        port map (
            clk      => clk,
            rd1_addr => ram_rd1_addr,
            rd1_data => ram_rd1_data,
            rd1      => ram_rd1,
            wr2_addr => ram_wr2_addr,
            wr2_data => ram_wr2_data,
            wr2      => ram_wr2
        );

    ram_rd1_addr <= ram_read_internal_addr when busy else vec_to_addr(rd_pos);
    ram_wr2_addr <= ram_write_internal_addr when busy else vec_to_addr(wr_pos);
    rd_data      <= ram_rd1_data;
    ram_wr2_data <= ram_write_internal_data when busy else wr_data;
    ram_rd1      <= ram_read_internal_en when busy else rd;
    ram_wr2      <= ram_write_internal_en when busy else wr;

    busy <= '0' when state.state = IDLE else '1';

    registers: process (clk) is
    begin
        if rising_edge(clk) then
            if not res_n then
                -- reset all
                state <= DEFAULT_STATE;
            else
                state <= next_state;
            end if;
        end if;
    end process;

    -- next state logic

    next_state_logic: process (state, bmpidx, clear, draw) is
        procedure write_to_ram(addr : vec2d_t; entry : entry_t) is
        begin
            ram_write_internal_en <= '1';
            ram_write_internal_addr <= vec_to_addr(addr);
            ram_write_internal_data <= entry;
        end procedure;

        pure function clip_for_command(data : natural) return std_logic_vector is
        begin
            return std_logic_vector(to_unsigned(data, GFX_CMD_WIDTH));
        end function;

        procedure next_pixel is
        begin
            -- increment indices and set draw commands
            next_state.drawing_state.coord.x <= std_logic_vector(unsigned(state.drawing_state.coord.x) + 1);
            next_state.drawing_state.cmd <= READ_MEM;

            if unsigned(state.drawing_state.coord.x) >= SNAKE_FIELD_WIDTH - 1 then
                next_state.drawing_state.coord.x <= (others => '0');
                next_state.drawing_state.cmd <= SET_GP_1; -- new line, the gp has to be moved!

                if unsigned(state.drawing_state.coord.y) < SNAKE_FIELD_HEIGHT - 1 then
                    next_state.drawing_state.coord.y <= std_logic_vector(unsigned(state.drawing_state.coord.y) + 1);
                else
                    next_state <= DEFAULT_STATE;
                end if;
            end if;
        end procedure;

        procedure send_draw_commands is
        begin
            if gfx_cmd_full then
                -- wait for next cycle if no command can be issued
                next_state <= state;
                return;
            end if;

            gfx_wr <= '1';
            case state.drawing_state.cmd is
                when SET_GP_1 =>
                    -- Set the gp to the beginning of the next line (only on line starts!)
                    gfx_cmd <= create_gfx_instr(OPCODE_MOVE_GP, rel => '0');
                    next_state.drawing_state.cmd <= SET_GP_2;

                when SET_GP_2 =>
                    gfx_cmd <= clip_for_command(to_integer(unsigned(state.drawing_state.coord.x) * SINGLE_FIELD_WIDTH));
                    next_state.drawing_state.cmd <= SET_GP_3;

                when SET_GP_3 =>
                    gfx_cmd <= clip_for_command(to_integer(unsigned(state.drawing_state.coord.y) * SINGLE_FIELD_HEIGHT));
                    next_state.drawing_state.cmd <= READ_MEM;

                when READ_MEM =>
                    -- issue read command to ram
                    ram_read_internal_en <= '1';
                    ram_read_internal_addr <= vec_to_addr(state.drawing_state.coord);
                    gfx_cmd <= create_gfx_instr(OPCODE_NOP);
                    next_state.drawing_state.cmd <= SET_EFFECT_COLOR;

                when SET_EFFECT_COLOR =>
                    -- read from RAM
                    next_state.drawing_state.mem_entry <= ram_rd1_data;

                    -- select what color to draw
                    if get_entry_type(ram_rd1_data) = BLANK_ENTRY then
                        -- Set effect to "and BLACK" for setting black bg
                        gfx_cmd <= create_gfx_instr(OPCODE_SET_BB_EFFECT, maskop => MASKOP_AND, mask => COLOR_BLACK);
                    elsif get_entry_type(ram_rd1_data) = FOOD_REGULAR or get_entry_type(ram_rd1_data) = FOOD_SPECIAL then
                        -- Set effect to "or FOOD_COLOR" 
                        gfx_cmd <= create_gfx_instr(OPCODE_SET_BB_EFFECT, maskop => MASKOP_OR, mask => FOOD_COLOR);
                    else
                        -- Set effect to "or PLAYER_COLOR"
                        if get_player(ram_rd1_data) = PLAYER1 then
                            gfx_cmd <= create_gfx_instr(OPCODE_SET_BB_EFFECT, maskop => MASKOP_OR, mask => PLAYER1_COLOR);
                        else
                            gfx_cmd <= create_gfx_instr(OPCODE_SET_BB_EFFECT, maskop => MASKOP_OR, mask => PLAYER2_COLOR);
                        end if;
                    end if;
                    next_state.drawing_state.cmd <= EXEC_BB_COLOR;

                when EXEC_BB_COLOR =>
                    gfx_cmd <= create_gfx_instr(OPCODE_BB_CHAR, am => '1', rot => get_rotation(state.drawing_state.mem_entry), mx => '1', my => '0', bmpidx => state.drawing_state.idx);
                    next_state.drawing_state.cmd <= EXEC_BB_2_COLOR;

                when EXEC_BB_2_COLOR =>
                    gfx_cmd(15 downto 6) <= get_bmp_offset(state.drawing_state.mem_entry);
                    gfx_cmd(5 downto 0) <= std_logic_vector(to_unsigned(10, 6));
                    next_pixel;

            end case;
        end procedure;

    begin
        -- next state logic
        next_state <= state;
        gfx_cmd <= (others => '0');
        gfx_wr <= '0';

        ram_read_internal_addr <= (others => '0');
        ram_write_internal_addr <= (others => '0');
        ram_write_internal_data <= (others => '0');
        ram_read_internal_en <= '0';
        ram_write_internal_en <= '0';

        case state.state is
            when IDLE =>

                if clear then
                    next_state <= DEFAULT_STATE;
                    next_state.state <= CLEARING;
                elsif draw then
                    next_state <= DEFAULT_STATE;
                    next_state.state <= DRAWING;
                    next_state.drawing_state.idx <= bmpidx;
                end if;

            when DRAWING =>
                send_draw_commands;

            when CLEARING =>
                write_to_ram(state.drawing_state.coord, BLANK_MEM_ENTRY);
                next_pixel;

            when others =>
                next_state <= DEFAULT_STATE;

        end case;
    end process;

end architecture;
