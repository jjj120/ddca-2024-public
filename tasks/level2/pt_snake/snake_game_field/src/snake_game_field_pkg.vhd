library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.snake_ctrl_pkg.all;
use work.gfx_cmd_pkg.all;

package snake_game_field_pkg is
	component snake_game_field is
		generic (
			PLAYER1_COLOR : color_t := COLOR_RED;
			PLAYER2_COLOR : color_t := COLOR_GREEN;
			FOOD_COLOR    : color_t := COLOR_YELLOW
		);
		port (
			clk   : in std_logic;
			res_n : in std_logic;

			draw   : in std_logic;
			bmpidx : in std_logic_vector(WIDTH_BMPIDX-1 downto 0);
			clear  : in std_logic;
			busy   : out std_logic;

			gfx_cmd       : out std_logic_vector(GFX_CMD_WIDTH-1 downto 0);
			gfx_wr        : out std_logic;
			gfx_cmd_full  : in std_logic;

			rd      : in std_logic;
			rd_pos  : in vec2d_t;
			rd_data : out entry_t;

			wr      : in std_logic;
			wr_pos  : in vec2d_t;
			wr_data : in entry_t
		);
	end component;
end package;
