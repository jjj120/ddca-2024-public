library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.math_pkg.all;
    use work.gfx_cmd_pkg.all;
    use work.snake_ctrl_pkg.all;
    use work.snake_game_field_pkg.all;
    use work.gfx_init_pkg.all;

entity snake_game_field_tb is
end entity;

architecture tb of snake_game_field_tb is
    constant CLOCK_PERIOD       : time    := 20 ns;
    constant ENTRY_FOOD_REGULAR : entry_t := create_game_field_entry(FOOD_REGULAR);
    constant ENTRY_FOOD_SPECIAL : entry_t := create_game_field_entry(FOOD_SPECIAL);

    signal clk   : std_logic := '1';
    signal res_n : std_logic := '0';

    signal draw   : std_logic                                   := '0';
    signal bmpidx : std_logic_vector(WIDTH_BMPIDX - 1 downto 0) := "011";
    signal clear  : std_logic                                   := '0';
    signal busy   : std_logic                                   := '0';

    signal gfx_cmd_out  : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := (others => '0');
    signal gfx_wr_out   : std_logic                                    := '0';
    signal gfx_cmd_full : std_logic                                    := '0';

    signal rd      : std_logic := '0';
    signal rd_pos  : vec2d_t   := (others => (others => '0'));
    signal rd_data : entry_t   := (others => '0');
    signal wr      : std_logic := '0';
    signal wr_pos  : vec2d_t   := (others => (others => '0'));
    signal wr_data : entry_t   := (others => '0');

    signal gfx_cmd_init     : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := (others => '0');
    signal gfx_wr_init      : std_logic                                    := '0';
    signal gfx_init_running : std_logic                                    := '0';

    signal gfx_cmd_in : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0) := (others => '0');
    signal gfx_wr_in  : std_logic                                    := '0';
    signal fin        : boolean                                      := false;
begin
    clk_stim: process is
    begin
        clk <= '1';
        wait for CLOCK_PERIOD / 2;
        clk <= '0';
        wait for CLOCK_PERIOD / 2;

        if fin then
            wait;
        end if;
    end process;

    game_field_inst: snake_game_field
        port map (
            clk          => clk,
            res_n        => res_n,
            draw         => draw,
            bmpidx       => bmpidx,
            clear        => clear,
            busy         => busy,
            gfx_cmd      => gfx_cmd_out,
            gfx_wr       => gfx_wr_out,
            gfx_cmd_full => gfx_cmd_full,
            rd           => rd,
            rd_pos       => rd_pos,
            rd_data      => rd_data,
            wr           => wr,
            wr_pos       => wr_pos,
            wr_data      => wr_data
        );

    gfx_cmd_interpreter_inst: entity work.gfx_cmd_interpreter
        generic map (
            OUTPUT_DIR => "./images/"
        )
        port map (
            clk            => clk,
            gfx_cmd        => gfx_cmd_in,
            gfx_cmd_wr     => gfx_wr_in,
            gfx_frame_sync => open,
            gfx_rd_data    => open,
            gfx_rd_valid   => open
        );

    gfx_cmd_in <= gfx_cmd_init when gfx_init_running else gfx_cmd_out;
    gfx_wr_in  <= gfx_wr_init when gfx_init_running else gfx_wr_out;

    stim: process is
        procedure report_main(test : string) is
        begin
            report "---------------------------- " & test & " ----------------------------";
        end procedure;

        pure function xy_to_vec(x, y : natural) return vec2d_t is
            variable to_return : vec2d_t;
        begin
            -- report to_string(x) & " into " & to_string(VEC2D_X_WIDTH) & ", " & to_string(y) & " into " & to_string(VEC2D_Y_WIDTH);
            to_return.x := std_logic_vector(to_unsigned(x, VEC2D_X_WIDTH));
            to_return.y := std_logic_vector(to_unsigned(y, VEC2D_Y_WIDTH));
            return to_return;
        end function;

        procedure write_to_game_state(coord : vec2d_t; entry : entry_t) is
        begin
            if busy then
                wait until not busy;
            end if;

            wr_pos <= coord;
            wr_data <= entry;
            wr <= '1';

            wait until rising_edge(clk);
            wr <= '0';
        end procedure;

        procedure read_from_game_state(coord : vec2d_t) is
        begin
            if busy then
                wait until not busy;
            end if;

            rd_pos <= coord;
            rd <= '1';
            wait until rising_edge(clk);
            wait until rising_edge(clk);
            rd <= '0';
        end procedure;

        procedure send_draw is
        begin
            draw <= '1';
            wait until rising_edge(clk);
            draw <= '0';
            wait until rising_edge(clk);
            if busy then
                report "Waiting for draw";
                wait until not busy;
            end if;
            report "Drawing ready";
        end procedure;

        procedure send_clear is
        begin
            clear <= '1';
            wait until rising_edge(clk);
            clear <= '0';
            wait until rising_edge(clk);
            if busy then
                report "Waiting for clean";
                wait until not busy;
            end if;
            report "Cleaning ready";
        end procedure;

        procedure save_bmp is
        begin
            gfx_init_running <= '1';
            gfx_cmd_init <= create_gfx_instr(OPCODE_DISPLAY_BMP, fs => '0', bmpidx => "000");
            wait until rising_edge(clk);
            gfx_init_running <= '0';
        end procedure;

        pure function test_entry_from_coord(x, y : integer) return entry_t is
        begin
            return std_logic_vector(to_unsigned(x + 1, SNAKE_FIELD_DATA_WIDTH));
        end function;

        procedure test_ram_access is
        begin
            for x in 0 to SNAKE_FIELD_WIDTH - 1 loop
                for y in 0 to SNAKE_FIELD_HEIGHT - 1 loop
                    write_to_game_state(xy_to_vec(x, y), test_entry_from_coord(x, y));
                end loop;
            end loop;

            for y in 0 to SNAKE_FIELD_HEIGHT - 1 loop
                for x in 0 to SNAKE_FIELD_WIDTH - 1 loop
                    read_from_game_state(xy_to_vec(x, y));
                    assert rd_data = test_entry_from_coord(x, y) report "RAM read failed at " & to_string(x) & ", " & to_string(y) & ": " & to_string(rd_data) & " != " & to_string(test_entry_from_coord(x, y)) severity error;
                end loop;
            end loop;

            report_main("RAM TEST FINISHED");
            send_clear;
        end procedure;
    begin
        report_main("START SIM");
        -- Initialize the gfx_cmd_interpreter (look at how this is done in the provided snake template)
        -- init the vram of the gfx_cmd_interpreter
        report "start init";
        res_n <= '0';
        gfx_init_running <= '1';
        gfx_wr_init <= '1';

        for inst_idx in GFX_INIT_CMDS'range loop
            gfx_cmd_init <= GFX_INIT_CMDS(inst_idx);
            wait until rising_edge(clk);
        end loop;

        -- gfx_cmd_init <= create_gfx_instr(OPCODE_ACTIVATE_BMP, bmpidx => "000");
        -- wait until rising_edge(clk);
        gfx_init_running <= '0';
        res_n <= '1';

        wait until rising_edge(clk);

        report "init ready";

        test_ram_access;
        -- Test your snake_game_field
        -- player 1
        write_to_game_state(xy_to_vec(2, 4), '1' & SNAKE_HEAD_UP);
        write_to_game_state(xy_to_vec(2, 5), '1' & SNAKE_BODY_VERTICAL);
        write_to_game_state(xy_to_vec(2, 6), '1' & SNAKE_BODY_VERTICAL);
        write_to_game_state(xy_to_vec(2, 7), '1' & SNAKE_BODY_DOWN_RIGHT);
        write_to_game_state(xy_to_vec(3, 7), '1' & SNAKE_BODY_HORIZONTAL);
        write_to_game_state(xy_to_vec(4, 7), '1' & SNAKE_BODY_HORIZONTAL);
        write_to_game_state(xy_to_vec(5, 7), '1' & SNAKE_BODY_HORIZONTAL);
        write_to_game_state(xy_to_vec(6, 7), '1' & SNAKE_BODY_HORIZONTAL);
        write_to_game_state(xy_to_vec(7, 7), '1' & SNAKE_TAIL_RIGHT);

        -- player 2
        write_to_game_state(xy_to_vec(10, 11), '0' & SNAKE_HEAD_LEFT);
        write_to_game_state(xy_to_vec(11, 11), '0' & SNAKE_BODY_HORIZONTAL);
        write_to_game_state(xy_to_vec(12, 11), '0' & SNAKE_BODY_HORIZONTAL);
        write_to_game_state(xy_to_vec(13, 11), '0' & SNAKE_BODY_UP_LEFT);
        write_to_game_state(xy_to_vec(13, 12), '0' & SNAKE_BODY_VERTICAL);
        write_to_game_state(xy_to_vec(13, 13), '0' & SNAKE_BODY_VERTICAL);
        write_to_game_state(xy_to_vec(13, 14), '0' & SNAKE_TAIL_DOWN);

        -- food
        write_to_game_state(xy_to_vec(15, 20), ENTRY_FOOD_REGULAR);
        write_to_game_state(xy_to_vec(20, 20), ENTRY_FOOD_SPECIAL);

        send_draw;
        save_bmp;

        gfx_init_running <= '1';
        gfx_cmd_init <= create_gfx_instr(OPCODE_CLEAR, CS_PRIMARY);
        wait until rising_edge(clk);
        gfx_init_running <= '0';

        write_to_game_state(xy_to_vec(2, 3), '1' & SNAKE_HEAD_UP);
        write_to_game_state(xy_to_vec(2, 4), '1' & SNAKE_BODY_VERTICAL);
        write_to_game_state(xy_to_vec(6, 7), '1' & SNAKE_TAIL_RIGHT);
        write_to_game_state(xy_to_vec(7, 7),(others => '0'));

        write_to_game_state(xy_to_vec(10, 10), '0' & SNAKE_HEAD_UP);
        write_to_game_state(xy_to_vec(10, 11), '0' & SNAKE_BODY_DOWN_RIGHT);
        write_to_game_state(xy_to_vec(13, 13), '0' & SNAKE_TAIL_DOWN);
        write_to_game_state(xy_to_vec(13, 14),(others => '0'));

        send_draw;
        save_bmp;

        fin <= true;
        report_main("SIM FINISHED");
        wait;
    end process;
end architecture;
