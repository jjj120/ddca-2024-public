onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /snake_game_field_tb/clk
add wave -noupdate /snake_game_field_tb/res_n
add wave -noupdate /snake_game_field_tb/draw
add wave -noupdate /snake_game_field_tb/bmpidx
add wave -noupdate /snake_game_field_tb/clear
add wave -noupdate /snake_game_field_tb/busy
add wave -noupdate /snake_game_field_tb/gfx_cmd_out
add wave -noupdate /snake_game_field_tb/gfx_wr_out
add wave -noupdate /snake_game_field_tb/gfx_cmd_full
add wave -noupdate /snake_game_field_tb/rd
add wave -noupdate /snake_game_field_tb/rd_pos
add wave -noupdate /snake_game_field_tb/rd_data
add wave -noupdate /snake_game_field_tb/wr
add wave -noupdate /snake_game_field_tb/wr_pos
add wave -noupdate /snake_game_field_tb/wr_data
add wave -noupdate /snake_game_field_tb/gfx_cmd_in
add wave -noupdate /snake_game_field_tb/gfx_wr_in
add wave -noupdate -expand -subitemconfig {/snake_game_field_tb/game_field_inst/state.drawing_state -expand /snake_game_field_tb/game_field_inst/state.drawing_state.coord {-height 16 -childformat {{/snake_game_field_tb/game_field_inst/state.drawing_state.coord.x -radix unsigned} {/snake_game_field_tb/game_field_inst/state.drawing_state.coord.y -radix unsigned -childformat {{/snake_game_field_tb/game_field_inst/state.drawing_state.coord.y(4) -radix decimal} {/snake_game_field_tb/game_field_inst/state.drawing_state.coord.y(3) -radix decimal} {/snake_game_field_tb/game_field_inst/state.drawing_state.coord.y(2) -radix decimal} {/snake_game_field_tb/game_field_inst/state.drawing_state.coord.y(1) -radix decimal} {/snake_game_field_tb/game_field_inst/state.drawing_state.coord.y(0) -radix decimal}}}} -expand} /snake_game_field_tb/game_field_inst/state.drawing_state.coord.x {-height 16 -radix unsigned} /snake_game_field_tb/game_field_inst/state.drawing_state.coord.y {-height 16 -radix unsigned -childformat {{/snake_game_field_tb/game_field_inst/state.drawing_state.coord.y(4) -radix decimal} {/snake_game_field_tb/game_field_inst/state.drawing_state.coord.y(3) -radix decimal} {/snake_game_field_tb/game_field_inst/state.drawing_state.coord.y(2) -radix decimal} {/snake_game_field_tb/game_field_inst/state.drawing_state.coord.y(1) -radix decimal} {/snake_game_field_tb/game_field_inst/state.drawing_state.coord.y(0) -radix decimal}}} /snake_game_field_tb/game_field_inst/state.drawing_state.coord.y(4) {-height 16 -radix decimal} /snake_game_field_tb/game_field_inst/state.drawing_state.coord.y(3) {-height 16 -radix decimal} /snake_game_field_tb/game_field_inst/state.drawing_state.coord.y(2) {-height 16 -radix decimal} /snake_game_field_tb/game_field_inst/state.drawing_state.coord.y(1) {-height 16 -radix decimal} /snake_game_field_tb/game_field_inst/state.drawing_state.coord.y(0) {-height 16 -radix decimal} /snake_game_field_tb/game_field_inst/state.drawing_state.mem_entry -expand} /snake_game_field_tb/game_field_inst/state
add wave -noupdate -radix unsigned /snake_game_field_tb/game_field_inst/ram_rd1_addr
add wave -noupdate /snake_game_field_tb/game_field_inst/ram_rd1_data
add wave -noupdate /snake_game_field_tb/game_field_inst/ram_rd1
add wave -noupdate -radix unsigned /snake_game_field_tb/game_field_inst/ram_wr2_addr
add wave -noupdate /snake_game_field_tb/game_field_inst/ram_wr2_data
add wave -noupdate /snake_game_field_tb/game_field_inst/ram_wr2
add wave -noupdate /snake_game_field_tb/game_field_inst/dp_ram_1c1r1w_inst/ram
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 2} {66919891 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 128
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {70308 ns}
