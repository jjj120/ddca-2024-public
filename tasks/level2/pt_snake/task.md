
# Project Task - Snake
**Points:** 3 ` | ` **Keywords:** Project Task, FSM, Interfaces

The goal of the first Project Task is to implement a [Snake Game](https://en.wikipedia.org/wiki/Snake_(video_game_genre)) featuring a single- and two-player mode in hardware.
Note that this being a Project Task (and thus possibly contributing points for your final grade), it will be more elaborate than other tasks.
Since we would like as many of you as possible to attempt this task, as you will learn a lot doing it, we decided to award a serious attempt at the project task sufficiently many points to be able to attend the Level 2 exam.
This way, if you spend time working on this task but fail to finish it, you don't risk not being able to pass this level.

In order to be awarded these points, you have to complete the `snake_game_field` (described below) and present it to a tutor.
Note that you can do this at any time, completely independent of the state of the remaining part of the project task, and continue working on it after the presentation.

## Implementation Sequence
We strongly recommend you to adhere to the following order of steps:

1. Implement the [gfx_cmd_interpreter](../gfx_cmd_interpreter/task.md) task. You will need this to properly simulate and test your `snake_game_field` and `snake` implementations.
2. Implement the `snake_game_field` module. You will need this entity for your `snake` implementation.
4. Present your `snake_game_field` to a tutor in order to pass this level and get valuable feedback before continuing with the more complex game logic.
3. Implement the remaining `snake` game.

## Snake Game Field
The `snake_game_field` module holds the current state of a `snake` game field (a grid of rows and columns) and also interfaces with the `vga_gfx_ctrl` in order to draw it to a screen.

### External Interface
```vhdl
component snake_game_field is
	generic (
		PLAYER1_COLOR : color_t := COLOR_RED;
		PLAYER2_COLOR : color_t := COLOR_GREEN;
		FOOD_COLOR    : color_t := COLOR_YELLOW
	);
	port (
		clk   : in std_logic;
		res_n : in std_logic;

		draw   : in std_logic;
		bmpidx : in std_logic_vector(WIDTH_BMPIDX-1 downto 0);
		clear  : in std_logic;
		busy   : out std_logic;

		gfx_cmd       : out std_logic_vector(GFX_CMD_WIDTH-1 downto 0);
		gfx_wr        : out std_logic;
		gfx_cmd_full  : in std_logic;

		rd      : in std_logic;
		rd_pos  : in vec2d_t;
		rd_data : out entry_t;

		wr      : in std_logic;
		wr_pos  : in vec2d_t;
		wr_data : in entry_t
	);
end component;
```
The signals `clk` and `res_n` respectively represent a clock and an active-low reset signal.

For drawing, the `snake_game_field` expects a bitmap containing 10-bit wide "characters" for the snake and food at the `vga_gfx_ctrl`'s bitmap index `bmpidx`.
The following image shows an example bitmap (the order of the indicidual images is important!):

![Example Snake Bitmap](.mdata/snake10.svg)

However, note that the template provided for the `snake` game already contains code that transmits the above bitmap to `vga_gfx_ctrl`.

Since the final game shall support up to two players, it must be possible to draw the snakes of the two players in distinct colors in order to distinguish them.
Snake entries belonging to player 1 / 2 are drawn using the color `PLAYER1_COLOR` / `PLAYER2_COLOR`.
Food entries are drawn in the color `FOOD_COLOR`.

The input `draw` is set for exactly one clock cycle to signal the `snake_game_field` to draw the current game state to `vga_gfx_ctrl` connected to it via the `gfx_*` signals.
Refer to the [vga_gfx_ctrl documentation](../../../lib/vga_gfx_ctrl/doc.md) for details on the respective interface.

Similarly, `clear` is set for exactly one clock cycle to signal the `snake_game_field` to clear its internal game state memory.
After the clearing, all entries of the memory must contain the `BLANK_ENTRY` entry (contained in [snake_ctrl_pkg](../../../lib/snake_ctrl/src/snake_ctrl_pkg.vhd)).

During both, the drawing and the clearing operation, the output signal `busy` stays high until the respective operation has completed.
Until this is the case (i.e., while `busy` is set), no new operation must be requested (neither via `draw`, `clear` nor via the memory interface `rd*`/`wr*`).

The read / write memory interface (`rd*`/`wr*`) allows external modules, such as `snake_ctrl` or `snake`, to access the game state memory.
This is, for example, required for initializing the game field, checking for collisions and to move a snake.
The memory interface must thus adhere to the one expected by `snake_ctrl`.
The following two timing diagrams illustrate this:

![Game Field Read Timing](.mdata/game_field_read_timing.svg)

![Game Field Write Timing](.mdata/game_field_write_timing.svg)

Note that the interface uses the `vec2d_t` type, instead of a regular memory address, for convenience.
You can find this type and related utility functions in [snake_ctrl_pkg](../../../lib/snake_ctrl/src/snake_ctrl_pkg.vhd).

### Internal Structure
The `snake` game you will implement uses a grid-based game field, that contains `SNAKE_FIELD_WIDTH` columns and `SNAKE_FIELD_HEIGHT` rows.
Each entry of this game field contains the information whether the field is currently inhabited by food (`FOOD_*`), a snake (`SNAKE_*`) or is empty (`BLANK_ENTRY`).
It furthers contains information about which snake "character" it contains (head, tail, body straight or body bent), its rotation (for drawing the respective "character" the right way round) and the player to which the snake belongs to (PLAYER1 or PLAYER2).
You will find a type, `entry_t`, for such entries, as well as useful functions related to it (for creating, and extracting fields from, such entries) in [snake_ctrl_pkg](../../../lib/snake_ctrl/src/snake_ctrl_pkg.vhd).
This package also provides the constants referenced above.
Note that as a consequence of the above description, you have to instantiate a suitable memory (refer to the [mem_pkg](../../../lib/mem/src/mem_pkg.vhd) package we provide you with).

For the drawing and clearing functionality, create an FSM that interfaces with the internal memory and the `vga_gfx_ctrl`.
You can draw snake and food "characters" using the `vga_gfx_ctrl`'s bit blit commands.
To get the respective character of the bitmap at index `bmpidx` you can use the `get_bmp_offset` function.
Furthermore, you can assume that the active bitmap of the graphics controller is the one used to draw on when `draw` becomes asserted.
However, make sure to satisfy this assumption in your `snake` implementation.
Game field entries with the value `BLANK_ENTRY` should not be drawn (you still have to correctly advance the graphics pointer though).

Useful graphics commands: `MOVE_GP`, `INC_GP`, `SET_BB_EFFECT`, `BB_CHAR`

Useful functions ([snake_ctrl_pkg](../../../lib/snake_ctrl/src/snake_ctrl_pkg.vhd)): `create_game_field_entry`, `get_entry_type`, `get_player`, `get_rotation`, `get_bmp_offset`

Useful functions ([gfx_cmd_pkg](../../../lib/vga_gfx_ctrl/src/gfx_cmd_pkg.vhd)): `create_gfx_instr`

### Simulation
Create a testbench `snake_game_field_tb` for the `snake_game_field` (you are provided with a [template](./snake_game_field/tb/snake_game_field_tb.vhd)).
Make sure you properly test the clearing operation and the correct behavior of the `busy` signal (as other modules will later rely on this).

For the clearing, you can either test this by using the memory interface to directly access the internal game field memory, or by using the `gfx_cmd_interpreter` (if available) to draw this memory content.

Note that testing the drawing functionality without the `gfx_cmd_interpreter` can be very hard.
Thus, if you have not implemented the interpreter, you can do this testing instead in hardware (to some extent).

**Hints**:
Ideally your testbench shall connect the UUT to the `gfx_cmd_interpreter` and test all of its functionality.
To properly initialize the `gfx_cmd_interpreter`, you have to send it the sequence of commands defined in the [`gfx_init_pkg`](snake/src/gfx_init_pkg.vhd) package.
Among other things this initialization routine, performs the following steps:
 * copying the snake character bitmap shown above to Video RAM and setting it up as index 3 in the Bitmap Descriptor Table (BDT)
 * setting up a bitmap with the resolution 320x240 at index 0 in the BDT
 * setting the primary / secondary color to black / white
 * setting the active bitmap index to index 0
The template of the [`snake`](snake/src/snake.vhd) module also executes these commands first.
You can have a look how this is done there.

### Hardware Testing
As you might have noticed, there is no dedicated top-level architecture file for the `snake_game_field`, and you are also not supposed to create one!
Instead, after finishing your implementation, create a PLL as described in the [Designflow Tuorial](../../level0/designflow/designflow.md).
Your PLL should provide two clock outputs.
The first output must be 12 MHz (for the audio controller used later) and the second one 25 MHz (for the VGA display controller).

In addition to creating the PLL, also add the following line to the end of the SDC file you created:

```
set_false_path -from [get_clocks {clk}] -to [get_clocks {top_inst|pll_inst|altpll_component|auto_generated|pll1|clk[0]}];
```

This command prevents the timing analyzer to report problems for signals crossing between the 50 MHz system clock domain and the 12 MHz audio clock domain.
Do not add a similar rule for the 25 MHz though!

After that you can synthesize and run the basic architecture of the `snake` game you are provided with in `pt_snake/snake/top_arch_snake.vhd`.
If your `snake_game_field` is working properly, you should see a randomly moving snake (without any food).

## Snake
You are already provided with a template of the Snake game ([snake.vhd](./snake/src/snake.vhd)) implementing only some basic functionality, like initializing the `vga_gfx_ctrl` and moving a snake of length 7 (including head and tail) randomly.
Before you start your own implementation, we strongly recommend to study the provided architecture and understand it fully before you make any modifications.
Your task is to extend this architecture to the full version of the game.

The following image gives a glimpse of what the final game could look like:

![Snake Final Game Example](.mdata/snake_example.png)

### Reference
Note that you are provided with a reference bitstream of the complete game (`ref.sof`).
You can use this as a guide for how the game should behave.
To load it to the board, use the `download_ref` target of the [Makefile](Makefile).

However, note that your solution does not have to be a pixel accurate copy of the reference solution, as long as all the required game mechanics are implemented correctly.

### External Interface
```vhdl
component snake is
	port (
		clk   : in std_logic;
		res_n : in std_logic;
		gfx_cmd        : out std_logic_vector(GFX_CMD_WIDTH-1 downto 0);
		gfx_cmd_wr     : out std_logic;
		gfx_cmd_full   : in std_logic;
		gfx_rd_valid   : in std_logic;
		gfx_rd_data    : in std_logic_vector(15 downto 0);
		gfx_frame_sync : in std_logic;
		cntrl1 : in snake_ctrl_t;
		cntrl2 : in snake_ctrl_t;
		synth_ctrl : out synth_ctrl_vec_t(0 to 1)
	);
end component;
```

The signals `clk` and `res_n` respectively represent a clock and an active-low reset signal.

The `gfx_*` signals are the interface to the `vga_cfx_ctrl` used for drawing to the screen.
Refer to its [documentation](../../../lib/vga_gfx_ctrl/doc.md) for the signals' meanings.

To control the game, the `snake` entity features the two inputs `cntrl1` and `cntrl2` of the type `snake_ctrl_t`, which are generic controller inputs.
Arbitrary signals, like a real physical controller or just some switches, can be connected to these generic controllers in the `top` architecture.

`synth_cntrl` is the signal used to interface with the audio controller (`audio_ctrl`).
Be referred to its [documentation](../../../lib/audio_ctrl/doc.md) for the interface description.

### Basic Game States
The below figure shows a state diagram describing the overall behavior of the game and the different states it can be in.
Note that the game states don't necessarily directly correspond to a state machine in your code with exactly these states.

![Snake Game States](.mdata/snake_game_state.svg)


### Idle
The game starts in the `IDLE` state, where it should display a snake of constant length moving randomly on the screen (nothing else should be drawn).

Use a snake length of 10 (including head and tail) and make sure that the random movement never leads to a situation where the snake eats its own tail (or body).

Note that the snake, when reaching a boundary of the game field, wraps around (i.e. if it moves across the right game field boundary, it continues at the left boundary).

Use the `prng` module to determine new directions the snake should move to (the provided code already does this, but try to understand it).
The snake shall be moved in a random direction by one `snake_game_field` grid cell every `FRAMES_BETWEEN_MOVEMENTS` frames.

A pressed `btn_a` / `btn_b` on **either** of the two controllers during the `IDLE` state shall switch the game into the single- / two-player mode of the `RUNNING` state.

### Running
For the single-player mode, after the game changes from `IDLE` to `RUNNING`, the snake is reset to some initial position, head direction and length (set via the internal `INITIAL_SNAKE_*` constants, see the table further below).

Afterwards, the movement of the snake can be controlled by the `btn_[up|down|left|right]` keys (on **both** controllers).
Every `FRAMES_BETWEEN_MOVEMENTS` frames the snake shall be moved by one grid cell of the `snake_game_field`.
Unless a `btn_[up|down|left|right]` button is pressed, the snake moves in the same direction as during the last movement (the direction its head faces in).
Note that when the snake is, e.g., moving up, pressing down has no effect.

A food item is eaten when the head of the snake is moved to the position of this item on the screen.
This causes the snake to grow by one element (use the `snake_ctrl` module to implement this).
Food eaten by the snake grants the player points which you should track via a player score.

For the two-player mode, two snakes with different colors shall be visible (one for each player), which are controlled via `cntrl1` and `cntrl2`, respectively.
To make things easier to debug, you can map the buttons of a single controller to the two generic controllers.

The game mechanic in the two player mode basically stays the same as in the single player version.
The only differences are that now only one player can catch the food and that a snake can not only collide with itself and the food, but also with the other snake.
You also need to track two player scores now (described in more detail further below).

### Game Over
In the single-player mode, the game ends when the head of the snake is moved to a position on the screen that is occupied by another part of its body.
In this case, the snake movement stops immediately and the game switches to the `GAME_OVER` state.
Print an appropriate message on the screen showing the achieved score. E.g.:

![Single Player Game Over](.mdata/single_player_go.png)

In the two-player mode, the game ends when one snake either crashes into its own body or the other player's snake.
To win the game, a player must have more points then the other, and the other player must have crashed the snake.
Otherwise, it is a tie.
Print an appropriate message on the screen. E.g.:

![Two Player Game Over](.mdata/two_player_go.png)

For both modes, pressing the `btn_start` button on either generic controller (on the game over screen) returns the game to the `IDLE` state.

### Paused
Pressing the `btn_start` button (on either controller) while the game is in the `RUNNING` state, switches the game into the `PAUSED` state (independent of single- / two-player mode).
The movement of the snake(s) is stopped and any `btn_[up|right|left|right]` input is ignored.
Pressing `btn_start`again should resume the game.
Note that the disappearance timeout for visible special food items should also be paused when the game is paused.

### Scores
For the single-player mode you need to keep track of a player score and a high score.
The high score is the best score achieved so far in any round (since the game started, i.e., since reset).

Normal food eaten by the snake adds `FOOD_POINTS` to the player's score.
When eaten, special food items bring more points than normal ones (`SPECIAL_FOOD_POINTS`).
The score and the high score shall be shown on the screen while the game is in the `RUNNING` state (thus not during `PAUSED` and `GAME_OVER`).
This should look similar to the following image:

![Single Player Mode Score](.mdata/single_player_score.png)

After each game (i.e. when the game state becomes `GAME_OVER`), check whether the achieved score is greater than the current high score and update it accordingly.
During `RESET`, the high score value is reset to zero. 

For the two-player mode, you must keep track of two player scores.
However, there is no high score.
The two player scores increase in the obvious manner and must both be visible on the screen during the `RUNNING` state.
The following shows an example image:

![Two Player Mode Scores](.mdata/two_player_scores.png)

For both game modes, print the score in the bottom row of the game field row and ensure that neither snake nor food can be in this row!
This means that in the `RUNNING` state the game field is, effectively, only `SNAKE_FIELD_HEIGHT-1` rows high!

**Hint**: Use a five-digit representation to display the high score (leading zeros may be displayed), as the provided [decimal_printer](../../../lib/decimal_printer/doc.md) is capable of that.


### Food Placements
Use PRNGs (two `prng` instances are already in the provided code) to randomly place the food items on the screen (you may also use your LFSR from Level 1).
There are two types of food items in the game: normal and special ones.
Normal food stays on the screen until it is eaten by a snake.
If a certain number of normal food items (`SPECIAL_FOOD_THRESHOLD`) has been eaten, a special food item should appear (use a different symbol and / or color for this purpose (you can add a generic to the `snake_game_field` for an additional color)).
However, special food items don't stay on the screen indefinitely, but only for a certain amount of frames (`SPECIAL_FOOD_TIMEOUT_FRAMES`).
If no player manages to catch it in time, it should disappear and a new normal food item should appear on some other random position on the screen.
Whenever a food item is eaten or disappears, a new one shall appear immediately (within the next few frames).
Note that this means that there always only exists exactly one food item.

To generate random positions for the food placement you need to generate random numbers between 0 and 31 (32 columns) for the x coordinate and 0 and 22 (24 rows minus one for the score) for the y coordinate.
Generating a random x-coordinate is straightforward.
However, for the y-coordinate, you may use the following approach (presented as Python code) to generate numbers in the range 0 to 23 (you still need to deal with the case 23):

```python
def rnd_row(x): # x must be a 5-bit number (i.e., [0;31]) 
	return ((x<<1) + (x)) >> 2
```

Note that, assuming x is evenly distributed, these functions don't yield evenly distributed random values.
However, they are sufficient for our purpose.

A problem that will arise is that you might randomly select a position on the screen that is already occupied by a body part of a snake.
Hence, before you use a random position, you will have to check whether it is free.
If it is not free, move to a neighboring field and check again, until you find a free spot.
The search for new free (random) places shall be performed in the idle time between snake movements.

### Sound Output
The snake game shall play sounds at the following occasions (while it is in the `RUNNING` state):

- The player eats a normal food item: Play a single tone for ~0.2 s
- The player eats a special food item: Play a (different) single tone for ~0.2 s
- The Game is over: Play three successive single tones (~0.25 s each) with increasing frequency

Use the [audio_ctrl](../../../lib/audio_ctrl/doc.md) module.

### Initialization
Note that the architecture already contains code to initialize the `vga_gfx_ctrl`.
In essence, the respective block reads a sequence of graphics commands from memory and writes them to the graphics controller via the `gfx_*` signals.
The command sequence sets the primary / secondary color to black / white (0x0 / 0xff) and creates two empty bitmaps of size 320x240 at indices 0 and 1 in the BDT.
These two bit maps will be your frame buffers used for double buffering.
Furthermore, the sequence also contains commands to define and initialize the following two bitmaps at the indices 2 and 3, respectively:

![8x8 Pixel Character Font](.mdata/font.svg)

![Example Snake Bitmap](.mdata/snake10.svg)

The first one is a bitmap containing a font containing 8x8 pixel characters, that can be used to print text via the `BB_CHAR` command.
The second one is the bitmap containing the 10x10 pixel game assets for drawing the snake and food, already shown above.

### Constants
Introduce the constants listed in the table below to conveniently change the different aspects of the game.
Try to find good values for these constants to make the game nicely playable.

| Constant | Description |
| -------- | ----------- |
| `FRAMES_BETWEEN_MOVEMENTS` | The number of frames (i.e. `gfx_frame_sync` pulses) between updates to the snake position |
| `INITIAL_SNAKE_HEAD_DIRECTIONS` | The initial head directions of the snakes in the `RUNNING` state |
| `INITIAL_SNAKE_HEAD_POSITIONS` | The initial head positions of the snakes in the `RUNNING` state |
| `INITIAL_BODY_LENGTH` | The initial body length of the snakes in the `RUNNING` state. This value can be between 0 and 15, not including head and tail. |
| `FOOD_POINTS` | The number of points added to the player's score when a normal food item is eaten. |
| `SPECIAL_FOOD_THRESHOLD` | The number of normal food items that have to be collected for a special food item to appear. |
| `SPECIAL_FOOD_POINTS` | The number of food points added to the player's score when a special food item is eaten. This value should be larger than `FOOD_POINTS`. |
| `SPECIAL_FOOD_TIMEOUT_FRAMES` | The amount of frames a special food item should be displayed before it disappears. |

### Architecture
We recommend you to use the provided architecture as a basis for your implementation.
Furthermore, use the `snake_ctrl` module to control the movement of the snake(s) and the `snake_game_field` to manage the game's game field.

You may also create new entities or blocks that implement parts of the described game behavior and instantiate them in the `snake` module (similar to what is already done with the `snake_ctrl` and `snake_game_field` modules).
However, if you do so, put all additional entities in the `snake/src/` directory (this is important for the submission target to work properly!).

We suggest the following implementation sequence:
- Modify the provided `IDLE` code to implement the required `IDLE` behavior. This will provide you with an understanding of the existing architecture.
- Add a simple single-player `RUNNING` behavior where a snake is initialized, drawn and controllable via the controller(s).
- Add the random placement of normal food and eating functionality.
- Add the score overlay.
- Add a collision detection to detect the game over condition and game over overlay.
- Implement transition from `GAME_OVER` to `IDLE`.
- Add special food functionality.
- Implement the pause mode.
- Add audio output.
- Implement the two-player mode.

**Hints**:
- For the game messages and scores you will need multiple graphics commands.
However, instead of creating dedicated states for them, use arrays of commands and loop through them.
This will make your code a lot conciser and easier to maintain.
- Debugging issues of your game in hardware can be very tough.
Instead, we strongly recommend you to implement the `gfx_cmd_interpreter` and to use is for debugging.
- In case you still want to do some debugging in hardware, you can use the unused inputs of the controllers to, e.g., enable some special debug outputs or set the game to specific states.
You do not have to remove such functionality for the submission, just make sure the required inputs and the game logic work as required.
- Try to outline your state machine before you start coding.
The required FSM will contain multiple states and has to deal with many interfaces.
Just starting to write code, will likely not lead to a clean / working design.
- We recommend you to use the two-process design style for the FSM and to use records for complex registers.

### Simulation
You should use the `gfx_cmd_interpreter`, if you implemented it, to simulate and test your implementation of the `snake` game.
The interpreter, in combination with some procedures (e.g., for waiting for frames and applying inputs concisely) is quite powerful.
Use it to simulate your game in different scenarios (single-player, two-player, idle, different game over cases etc.) and harness the interpreter's feature to dump frames as images to test your design.
Also note that the Makefile features a `video` target that creates a video out of all the dumped frames in the directory.
This way you can easily view complex scenarios, consisting of hundreds of frames.

In case you did not implement the `gfx_cmd_interpreter` you can hardly test your game using a simulation.
Continue with the hardware testing section then (and be again strongly recommended to implement the interpreter).

### Hardware Testing
Once you are confident that your game logic works, you can synthesize and test it on the hardware.
You are already provided with top-level architecture in the file [`top_arch_snake.vhd`](top_arch_snake.vhd)  that instantiates the `precompiled_snake` module, as well as the `vga_gfx_ctrl`, `precompiled_audio_ctrl_2s` and `precompiled_gc_ctrl`.
The `precompiled_snake` module represents a reference implementation of the `snake` module.
You can use this module to test if you configured the PLL correctly (some of the other tasks will also make use of it to test parts the `vga_cfx_ctrl`).
To run you own implementation change the architecture, such that the `snake` module is instantiated.
`cntrl1` is connected to the controller while `cntrl2` connects to some switches.
Feel free to change this mapping as desired.

The architecture also already instantiates the PLL you created before (you might need to change the name to fit to the one you gave it) and synchronizers where required.
Recall that the first PLL output must be a 12 MHz clock for the `audio_ctrl`, and the second output must be a 25 MHz for the `vga_gfx_ctrl`.

## Submission
Use the `submission` target of the [Makefile](Makefile) to create a submission archive containing the required files.
The target will first check for the existence of some expected files and report an error if something is missing.
If everything worked correctly, a `submission.tar.gz` archive will have been created.
Upload this archive to the respective TUWEL submission activity.
