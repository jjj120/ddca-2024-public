library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.snake_pkg.all;
    use work.gfx_cmd_pkg.all;
    use work.vga_gfx_ctrl_pkg.all;
    use work.sync_pkg.all;
    use work.gc_ctrl_pkg.all;
    use work.audio_ctrl_pkg.all;
    use work.snake_ctrl_pkg.all;
    use work.util_pkg.all;

architecture top_arch_snake of top is
    constant SYNC_STAGES : integer := 2;

    component pll_snake is
        port (
            inclk0 : in  std_logic := '0';
            c0     : out std_logic;
            c1     : out std_logic
        );
    end component;

    signal audio_clk, display_clk     : std_logic;
    signal display_res_n, audio_res_n : std_logic;
    signal res_n                      : std_logic;

    signal cntrl1, cntrl2 : snake_ctrl_t;
    signal gc_ctrl_state  : gc_ctrl_state_t;

    signal gfx_cmd        : std_logic_vector(GFX_CMD_WIDTH - 1 downto 0);
    signal gfx_cmd_wr     : std_logic;
    signal gfx_cmd_full   : std_logic;
    signal gfx_rd_data    : std_logic_vector(15 downto 0);
    signal gfx_rd_valid   : std_logic;
    signal gfx_frame_sync : std_logic;

    signal synth_ctrl : synth_ctrl_vec_t(0 to 1);
    signal dbg_head   : vec2d_t;
begin
    -- map keys
    ledg(8) <= gfx_cmd_full when switches(0) else '0';
    ledg(7) <= gfx_cmd_wr when switches(0) else '0';
    ledg(6) <= gfx_frame_sync when switches(0) else '0';

    hex0 <= to_segs(gfx_cmd(3 downto 0)) when switches(0) else (others => '1');
    hex1 <= to_segs(gfx_cmd(7 downto 4)) when switches(0) else (others => '1');
    hex2 <= to_segs(gfx_cmd(11 downto 8)) when switches(0) else (others => '1');
    hex3 <= to_segs(gfx_cmd(15 downto 12)) when switches(0) else (others => '1');
    hex4 <= to_segs(dbg_head.x(3 downto 0)) when switches(0) else (others => '1');
    hex5 <= to_segs("000" & dbg_head.x(VEC2D_X_WIDTH - 1 downto 4)) when switches(0) else (others => '1');
    hex6 <= to_segs(dbg_head.y(3 downto 0)) when switches(0) else (others => '1');
    hex7 <= to_segs("000" & dbg_head.y(VEC2D_Y_WIDTH - 1 downto 4)) when switches(0) else (others => '1');

    ledr <= switches;

    -- Make sure the component name matches your created pll
    pll_inst: pll_snake
        port map (
            inclk0 => clk,
            c0     => audio_clk, --12 MHz
            c1     => display_clk -- 25 MHz
        );

    -- Instantiate controller(s) and assign to snake input signals
    gc_ctrl_inst: gc_ctrl
        port map (
            clk        => clk,
            res_n      => res_n,
            data       => gc_data,
            ctrl_state => gc_ctrl_state,
            rumble     => gc_ctrl_state.btn_z
        );

    -- player 1 gets left half of controller
    cntrl1.btn_a     <= gc_ctrl_state.btn_up;
    cntrl1.btn_b     <= gc_ctrl_state.btn_left;
    cntrl1.btn_start <= gc_ctrl_state.btn_right;
    cntrl1.btn_left  <= '1' when unsigned(gc_ctrl_state.joy_x) < x"70" else '0';
    cntrl1.btn_right <= '1' when unsigned(gc_ctrl_state.joy_x) > x"90" else '0';
    cntrl1.btn_down  <= '1' when unsigned(gc_ctrl_state.joy_y) < x"70" else '0';
    cntrl1.btn_up    <= '1' when unsigned(gc_ctrl_state.joy_y) > x"90" else '0';

    -- player 2 gets right half of controller
    cntrl2.btn_a     <= gc_ctrl_state.btn_a;
    cntrl2.btn_b     <= gc_ctrl_state.btn_b;
    cntrl2.btn_start <= gc_ctrl_state.btn_start;
    cntrl2.btn_left  <= '1' when unsigned(gc_ctrl_state.c_x) < x"70" else '0';
    cntrl2.btn_right <= '1' when unsigned(gc_ctrl_state.c_x) > x"90" else '0';
    cntrl2.btn_down  <= '1' when unsigned(gc_ctrl_state.c_y) < x"70" else '0';
    cntrl2.btn_up    <= '1' when unsigned(gc_ctrl_state.c_y) > x"90" else '0';

    reset_sync: sync
        generic map (
            SYNC_STAGES => SYNC_STAGES,
            RESET_VALUE => '0'
        )
        port map (
            clk      => clk,
            res_n    => '1',
            data_in  => keys(0),
            data_out => res_n
        );

    display_reset_sync: sync
        generic map (
            SYNC_STAGES => SYNC_STAGES,
            RESET_VALUE => '0'
        )
        port map (
            clk      => display_clk,
            res_n    => '1',
            data_in  => keys(0),
            data_out => display_res_n
        );

    audio_reset_sync: sync
        generic map (
            SYNC_STAGES => SYNC_STAGES,
            RESET_VALUE => '0'
        )
        port map (
            clk      => audio_clk,
            res_n    => '1',
            data_in  => keys(0),
            data_out => audio_res_n
        );

    game_inst: entity work.snake
        port map (
            clk            => clk,
            res_n          => res_n,
            -- connection to the gfx cmd controller
            gfx_cmd        => gfx_cmd,
            gfx_cmd_wr     => gfx_cmd_wr,
            gfx_cmd_full   => gfx_cmd_full,
            gfx_rd_valid   => gfx_rd_valid,
            gfx_rd_data    => gfx_rd_data,
            gfx_frame_sync => gfx_frame_sync,
            -- connections to the controllers
            cntrl1         => cntrl1,
            cntrl2         => cntrl2,
            -- connection to the audio_ctrl
            synth_ctrl     => synth_ctrl,
            dbg_head       => dbg_head
        );

    vga_gfx_ctrl_inst: vga_gfx_ctrl
        port map (
            clk             => clk,
            res_n           => res_n,
            display_clk     => display_clk,
            display_res_n   => display_res_n,
            gfx_cmd         => gfx_cmd,
            gfx_cmd_wr      => gfx_cmd_wr,
            gfx_cmd_full    => gfx_cmd_full,
            gfx_rd_valid    => gfx_rd_valid,
            gfx_rd_data     => gfx_rd_data,
            gfx_frame_sync  => gfx_frame_sync,
            sram_dq         => sram_dq,
            sram_addr       => sram_addr,
            sram_ub_n       => sram_ub_n,
            sram_lb_n       => sram_lb_n,
            sram_we_n       => sram_we_n,
            sram_ce_n       => sram_ce_n,
            sram_oe_n       => sram_oe_n,
            vga_hsync       => vga_hsync,
            vga_vsync       => vga_vsync,
            vga_dac_clk     => vga_dac_clk,
            vga_dac_blank_n => vga_dac_blank_n,
            vga_dac_sync_n  => vga_dac_sync_n,
            vga_dac_r       => vga_dac_r,
            vga_dac_g       => vga_dac_g,
            vga_dac_b       => vga_dac_b
        );

    audio_ctrl_inst: precompiled_audio_ctrl_2s
        port map (
            clk            => audio_clk,
            res_n          => audio_res_n,
            wm8731_sdat    => wm8731_sdat,
            wm8731_sclk    => wm8731_sclk,
            wm8731_xck     => wm8731_xck,
            wm8731_dacdat  => wm8731_dacdat,
            wm8731_daclrck => wm8731_daclrck,
            wm8731_bclk    => wm8731_bclk,
            synth_ctrl     => synth_ctrl
        );

end architecture;
