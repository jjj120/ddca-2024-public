
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.snes_ctrl_pkg.all;

entity snes_ctrl is
    generic (
        CLK_FREQ        : integer := 50000000;
        CLK_OUT_FREQ    : integer := 1000000;
        REFRESH_TIMEOUT : integer := 1000
    );
    port (
        clk        : in  std_logic;
        res_n      : in  std_logic;

        --interface to snes controller
        snes_clk   : out std_logic := '1';
        snes_latch : out std_logic := '0';
        snes_data  : in  std_logic;

        -- button outputs
        ctrl_state : out snes_ctrl_state_t
    );
begin
    assert CLK_OUT_FREQ <= 500_000 report "CLK_OUT_FREQ (" & integer'image(CLK_OUT_FREQ) & ") should not be higher than 500kHz!" severity failure;
end entity;

architecture rtl of snes_ctrl is
    constant CLK_OUT_CYCLE_LEN : integer := CLK_FREQ / CLK_OUT_FREQ;

    type read_letter_t is (
            START,
            R_B,
            R_Y,
            R_SE,
            R_ST,
            R_UP,
            R_DOWN,
            R_LEFT,
            R_RIGHT,
            R_A,
            R_X,
            R_L,
            R_R,
            R_ONE0,
            R_ONE1,
            R_ONE2,
            R_ONE3,
            R_ONE4,
            IDLE
        );

    type state_t is record
        wait_counter : integer;
        reading      : read_letter_t;
        btn_state    : snes_ctrl_state_t;
        latch        : std_logic;
        clk_out      : std_logic;
    end record;

    signal state, next_state : state_t := (
        wait_counter => 0,
        reading      => START,
        btn_state    => (others => '0'),
        latch        => '0',
        clk_out      => '1'
    );
begin
    registers: process (clk) is
    begin
        if rising_edge(clk) then
            state <= next_state;
            if not res_n then
                state <= (
                    wait_counter => 0,
                    reading      => START,
                    btn_state    => (others => '0'),
                    latch        => '0',
                    clk_out      => '1'
                );
            end if;
        end if;
    end process;

    process (all) is
        procedure read_btn(signal state_btn : out std_logic; next_state_reading : in read_letter_t) is
        begin
            if state.wait_counter = 0 then
                next_state.clk_out <= '0';
            elsif state.wait_counter = CLK_OUT_CYCLE_LEN / 2 then
                next_state.clk_out <= '1';
                state_btn <= not snes_data;
            elsif state.wait_counter = CLK_OUT_CYCLE_LEN then
                next_state.wait_counter <= 0;
                next_state.reading <= next_state_reading;
            end if;
        end procedure;

        procedure discard_ones(next_state_reading : in read_letter_t) is
        begin
            if state.wait_counter = 0 then
                next_state.clk_out <= '0';
            elsif state.wait_counter = CLK_OUT_CYCLE_LEN / 2 then
                next_state.clk_out <= '1';
            elsif state.wait_counter = CLK_OUT_CYCLE_LEN - 1 then
                next_state.wait_counter <= 0;
                next_state.reading <= next_state_reading;
            end if;
        end procedure;
    begin
        next_state <= state;
        next_state.wait_counter <= state.wait_counter + 1;

        case state.reading is
            when START =>
                -- pull latch
                if state.wait_counter = 0 then
                    next_state.latch <= '1';
                elsif state.wait_counter = CLK_OUT_CYCLE_LEN / 2 then
                    next_state.latch <= '0';
                elsif state.wait_counter = CLK_OUT_CYCLE_LEN - 1 then
                    next_state.wait_counter <= 0;
                    next_state.reading <= R_B;
                end if;
            when R_B =>
                read_btn(next_state.btn_state.btn_b, R_Y);
            when R_Y =>
                read_btn(next_state.btn_state.btn_y, R_SE);
            when R_SE =>
                read_btn(next_state.btn_state.btn_select, R_ST);
            when R_ST =>
                read_btn(next_state.btn_state.btn_start, R_UP);
            when R_UP =>
                read_btn(next_state.btn_state.btn_up, R_DOWN);
            when R_DOWN =>
                read_btn(next_state.btn_state.btn_down, R_LEFT);
            when R_LEFT =>
                read_btn(next_state.btn_state.btn_left, R_RIGHT);
            when R_RIGHT =>
                read_btn(next_state.btn_state.btn_right, R_A);
            when R_A =>
                read_btn(next_state.btn_state.btn_a, R_X);
            when R_X =>
                read_btn(next_state.btn_state.btn_x, R_L);
            when R_L =>
                read_btn(next_state.btn_state.btn_l, R_R);
            when R_R =>
                read_btn(next_state.btn_state.btn_r, R_ONE0);
            when R_ONE0 =>
                discard_ones(R_ONE1);
            when R_ONE1 =>
                discard_ones(R_ONE2);
            when R_ONE2 =>
                discard_ones(R_ONE3);
            when R_ONE3 =>
                discard_ones(R_ONE4);
            when R_ONE4 =>
                discard_ones(IDLE);
            when IDLE =>
                if state.wait_counter = 0 then
                    next_state.clk_out <= '1';
                elsif state.wait_counter = REFRESH_TIMEOUT then
					next_state.wait_counter <= 0;
                    next_state.reading <= START;
                end if;
            when others =>
                assert 1 = 0 report "GOT NON EXISTENT STATE!" severity failure;
        end case;
    end process;

    snes_latch <= state.latch;
    snes_clk   <= state.clk_out;
    ctrl_state <= state.btn_state;
end architecture;
