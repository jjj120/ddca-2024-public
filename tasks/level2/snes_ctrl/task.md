
# SNES Controller
**Points:** 1 ` | ` **Keywords:** FSM, External Interface

Your task is to create module that implements the [SNES (Super Nintendo Entertainment System)](https://en.wikipedia.org/wiki/Super_Nintendo_Entertainment_System) controller interface.
A precompiled version of this core and its documentation is available in [`lib/snes_ctrl`](../../../lib/snes_ctrl/).

The SNES controller, as well as its predecessor the NES controller, are based on a simple parallel-in/serial-out shift register (like the CMOS 4021 shift register).
The controller uses a serial interface consisting of three signals (`clk`, `data` and `latch`), as shown in the timing diagram in figure below.

![SNES controller serial interface protocol](.mdata/snes_timing.svg)

To start a new transmission reading the state (pressed/not pressed) of each button, a pulse must be generated on the `latch` signal. 
This pulse causes the shift register inside the controller to latch the current state of every button (parallel load). 
Now a clock signal can be applied at the `clk` input to serially shift out the button state over the `data` signal. 
Note that the `data` signal changes with the rising edge of clk. 
Hence, the `data` signal is always sampled right before the next rising edge indicated by the orange lines. 
This way the input signal at `data` has the maximum time to stabilize (setup constraint). 
Although the SNES Controller only has 12 buttons, 16 data bits are transmitted (this is due to its internal structure consisting of 2 8 bit shift registers). 
The last four data bits always read 1.

The `snes_ctrl_pkg` provided by the `lib` core already contains a component declaration for the entity you can find in [`snes_ctrl.vhd`](src/snes_ctrl.vhd).

```vhdl
component snes_ctrl is
	generic (
		CLK_FREQ : integer := 50000000;
		CLK_OUT_FREQ : integer := 1000000;
		REFRESH_TIMEOUT : integer := 1000
	);
	port (
		clk : in std_logic;
		res_n : in std_logic;
		snes_clk : out std_logic;
		snes_latch : out std_logic;
		snes_data : in std_logic;
		ctrl_state : out snes_ctrl_state_t
	);
end component;
```

The core shall continuously read out the controller state and update its `ctrl_state` output accordingly. 
This output is a record defined in `snes_ctrl_pkg` and is shown below.

```vhdl
type snes_ctrl_state_t is record
	btn_up     : std_logic;
	btn_down   : std_logic;
	btn_left   : std_logic;
	btn_right  : std_logic;
	btn_a      : std_logic;
	btn_b      : std_logic;
	btn_x      : std_logic;
	btn_y      : std_logic;
	btn_l      : std_logic;
	btn_r      : std_logic;
	btn_start  : std_logic;
	btn_select : std_logic;
end record;
```

Each of the physical controller buttons has an entry in this record.
It is your task to map the individual bits received from the `snes_data` wire to this record type.
**Important**: If a specific button is pressed on the controller the respective bit transmitted on `snes_data` will be set to 0. 

The inputs `clk` and `res_n` (active-low reset) have their usual function.
The meanings of the generics are listed in the table below:

| Name | Description |
| -- | --------- |
| `CLK_FREQ` | Actual clock frequency of the `clk` signal given in Hz |
| `CLK_OUT_FREQ` | The desired clock frequency that should be generated for the `snes_clk` signal in Hz. Don't use frequencies higher than 500 kHz. |
| `REFRESH_TIMEOUT` | The timeout in `clk` cycles the controller should wait in between button readouts. Set the default value of this generic to the equivalent of 8 ms. |

The pulse on the `snes_latch` wire that starts each transmission shall have the same width as the clock pulse on `snes_clk`. 

Note that you need a synchronizer for the `snes_data` input.
You can use the [`sync`](../../../lib/sync/doc.md) lib core.

## Testbench

The file [`tb/snes_ctrl_tb.vhd`](tb/snes_ctrl_tb.vhd) provides a suitable template for your testbench.
The testbench already defines the procedure `generate_snes_data_input`  that takes a `snes_ctrl_state_t` record as input parameter.
Implement this procedure, such that it generates the correct `snes_data` waveform for the specified controller state.

Generate 64 random test values and check if the `ctrl_state` output of the UUT matches the value supplied to the `generate_snes_data_input` procedure.

**Hint:** Use the rising edge of the `snes_latch` signal to check if the current output value of `ctrl_state` is correct and then call `generate_snes_data_input` with new random input data.

Use the waveform viewer to verify that the period on the `snes_clk` signal adheres to the `CLK_OUT_FREQ` generic.
For the simulation you can set `REFRESH_TIMEOUT` to a small value.

## Hardware Test

Add an instance of your `snes_ctrl` to the top-level architecture in [`top_arch_snes_ctrl.vhd`](top_arch_snes_ctrl.vhd).

The figure below shows how the controller needs to be connected to the board's GPIO connector.

![GPIO board connector pinout (top view)](.mdata/gpio_board_connector_pinout.svg)

Be careful when connecting it!
Connecting it in the wrong way can damage the board or the controller. 
Consult a tutor if you are unsure.

Assign each record entry in of the `ctrl_state` output to its own LED. 
Show that your controller is able to detect each button of the SNES controller correctly.
