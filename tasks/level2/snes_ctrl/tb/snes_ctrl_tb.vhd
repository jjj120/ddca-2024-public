library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.tb_util_pkg.all;
    use work.snes_ctrl_pkg.all;

entity snes_ctrl_tb is

end entity;

architecture bench of snes_ctrl_tb is
    constant CLK_PERIOD : time := 20 ns;

    signal clk        : std_logic := '0';
    signal res_n      : std_logic := '1';
    signal snes_clk   : std_logic;
    signal snes_latch : std_logic;
    signal snes_data  : std_logic := '1';
    signal ctrl_state : snes_ctrl_state_t; -- := (others => '0');

    signal fin : std_logic := '0';

    shared variable rnd : random_t;
begin

    uut: snes_ctrl
        generic map (
            CLK_FREQ        => 50_000_000,
            CLK_OUT_FREQ    => 100_000,
            REFRESH_TIMEOUT => 1000
        )
        port map (
            clk        => clk,
            res_n      => res_n,
            snes_clk   => snes_clk,
            snes_latch => snes_latch,
            snes_data  => snes_data,
            ctrl_state => ctrl_state
        );

    -- Stimulus process

    stimulus: process
        procedure check_equality(v1, v2 : std_logic) is
        begin
            assert not v1 = v2 report "Button not right: not " & to_string(v1) & " = " & to_string(v2);
        end procedure;

        procedure generate_snes_data_input(buttons : snes_ctrl_state_t) is
        begin
            -- wait for the falling edge on the snes_latch signal
            snes_data <= not buttons.btn_b;
            wait until rising_edge(snes_latch);

            -- create input data in snes_data
            wait until rising_edge(snes_clk);

            snes_data <= not buttons.btn_y;
            wait until rising_edge(snes_clk);

            snes_data <= not buttons.btn_select;
            wait until rising_edge(snes_clk);

            snes_data <= not buttons.btn_start;
            wait until rising_edge(snes_clk);

            snes_data <= not buttons.btn_up;
            wait until rising_edge(snes_clk);

            snes_data <= not buttons.btn_down;
            wait until rising_edge(snes_clk);

            snes_data <= not buttons.btn_left;
            wait until rising_edge(snes_clk);

            snes_data <= not buttons.btn_right;
            wait until rising_edge(snes_clk);

            snes_data <= not buttons.btn_a;
            wait until rising_edge(snes_clk);

            snes_data <= not buttons.btn_x;
            wait until rising_edge(snes_clk);

            snes_data <= not buttons.btn_l;
            wait until rising_edge(snes_clk);

            snes_data <= not buttons.btn_r;
            wait until rising_edge(snes_clk);

            snes_data <= '1';
            wait until rising_edge(snes_clk);
            wait until rising_edge(snes_clk);
            wait until rising_edge(snes_clk);
            wait until rising_edge(snes_clk);

            assert buttons = ctrl_state report "buttons do not match";
        end procedure;

        variable input : std_logic_vector(11 downto 0);
    begin
        report "simulation start";

        res_n <= '0';
        wait until rising_edge(clk);

        res_n <= '1';

        --generate random input data
        input := rnd.gen_slv_01(12);
        report to_string(input);
        generate_snes_data_input(to_snes_ctrl_state(input));
        -- generate_snes_data_input(to_snes_ctrl_state("101010101010"));
        wait for 10 us;
        fin <= '1';
        report "simulation end";
        -- End simulation
        wait;
    end process;

    clock: process is
    begin
        clk <= '1';
        wait for CLK_PERIOD / 2;
        clk <= '0';
        wait for CLK_PERIOD / 2;

        if fin then
            wait;
        end if;
    end process;
end architecture;

