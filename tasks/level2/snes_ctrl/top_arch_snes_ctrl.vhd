library ieee;
    use ieee.std_logic_1164.all;

    use work.snes_ctrl_pkg.all;

architecture top_arch_snes_ctrl of top is
    signal ctrl_state : snes_ctrl_state_t;
begin
    uut: snes_ctrl
        generic map (
            CLK_FREQ        => 50_000_000,
            CLK_OUT_FREQ    => 100_000,
            REFRESH_TIMEOUT => 1000
        )
        port map (
            clk        => clk,
            res_n      => keys(0),
            snes_clk   => snes_clk,
            snes_latch => snes_latch,
            snes_data  => snes_data,
            ctrl_state => ctrl_state
        );

    ledr(0)  <= ctrl_state.btn_b;
    ledr(1)  <= ctrl_state.btn_y;
    ledr(2)  <= ctrl_state.btn_select;
    ledr(3)  <= ctrl_state.btn_start;
    ledr(4)  <= ctrl_state.btn_up;
    ledr(5)  <= ctrl_state.btn_down;
    ledr(6)  <= ctrl_state.btn_left;
    ledr(7)  <= ctrl_state.btn_right;
    ledr(8)  <= ctrl_state.btn_a;
    ledr(9)  <= ctrl_state.btn_x;
    ledr(10) <= ctrl_state.btn_l;
    ledr(11) <= ctrl_state.btn_r;

    hex0 <= "1111111";
    hex1 <= "1111111";
    hex2 <= "1111111";
    hex3 <= "1111111";
    hex4 <= "1111111";
    hex5 <= "1111111";
    hex6 <= "1111111";
    hex7 <= "1111111";

end architecture;
