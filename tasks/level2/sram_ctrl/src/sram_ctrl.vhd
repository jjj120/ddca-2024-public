
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.math_pkg.all;
use work.mem_pkg.all;
use work.sram_ctrl_pkg.all;

entity sram_ctrl is
	generic (
		ADDR_WIDTH : integer := 21;
		DATA_WIDTH : integer := 16;
		WR_BUF_SIZE : integer := 8
	);
	port (
		clk : in  std_logic;
		res_n : in std_logic;

		-- write port (buffered)
		wr_addr : in std_logic_vector(ADDR_WIDTH-1 downto 0);
		wr_data : in std_logic_vector(DATA_WIDTH-1 downto 0);
		wr      : in std_logic;
		wr_access_mode : in sram_access_mode_t;
		wr_empty : out std_logic;
		wr_full : out std_logic;
		wr_half_full : out std_logic;
	
		-- read port 1 (high priority)
		rd1_addr  : in std_logic_vector(ADDR_WIDTH-1 downto 0);
		rd1       : in std_logic;
		rd1_access_mode : in sram_access_mode_t;
		rd1_busy  : out std_logic;
		rd1_data  : out std_logic_vector(DATA_WIDTH-1 downto 0);
		rd1_valid : out std_logic;
		
		-- read port 2 (low priority)
		rd2_addr  : in std_logic_vector(ADDR_WIDTH-1 downto 0);
		rd2       : in std_logic;
		rd2_access_mode : in sram_access_mode_t;
		rd2_busy  : out std_logic;
		rd2_data  : out std_logic_vector(DATA_WIDTH-1 downto 0);
		rd2_valid : out std_logic;
		
		-- external interface to SRAM
		sram_dq : inout std_logic_vector(DATA_WIDTH-1 downto 0);
		sram_addr : out std_logic_vector(ADDR_WIDTH-2 downto 0);
		sram_ub_n : out std_logic;
		sram_lb_n : out std_logic;
		sram_we_n : out std_logic;
		sram_ce_n : out std_logic;
		sram_oe_n : out std_logic
	);
begin
	assert DATA_WIDTH = 16 severity failure;
end entity;


architecture arch of sram_ctrl is
	
	--next SRAM signals for the SRAM output registers 
	signal sram_addr_nxt : std_logic_vector(sram_addr'range);
	signal sram_ub_n_nxt : std_logic;
	signal sram_lb_n_nxt : std_logic;
	signal sram_we_n_nxt : std_logic;
	signal sram_ce_n_nxt : std_logic;
	signal sram_oe_n_nxt : std_logic;
	signal sram_drive_data : std_logic;
	signal sram_drive_data_nxt : std_logic;
	signal sram_data_out : std_logic_vector(sram_dq'range);
	signal sram_data_out_nxt : std_logic_vector(sram_dq'range);
	
begin

	sync : process(clk, res_n)
	begin
		if (res_n = '0') then
			sram_addr <= (others => '0');
			sram_ub_n <= '1';
			sram_lb_n <= '1';
			sram_we_n <= '1';
			sram_ce_n <= '1';
			sram_oe_n <= '1';
			sram_drive_data <= '0';
			sram_data_out <= (others => '0');
		elsif (rising_edge(clk)) then
			sram_addr <= sram_addr_nxt;
			sram_ub_n <= sram_ub_n_nxt;
			sram_lb_n <= sram_lb_n_nxt;
			sram_we_n <= sram_we_n_nxt;
			sram_ce_n <= sram_ce_n_nxt;
			sram_oe_n <= sram_oe_n_nxt;
			sram_drive_data <= sram_drive_data_nxt;
			sram_data_out <= sram_data_out_nxt; 
		end if;
	end process;
	
	sram_dq <= sram_data_out when sram_drive_data = '1' else (others=>'Z');
	

end architecture;

