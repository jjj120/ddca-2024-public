
# SRAM Controller
**Points:** 3 ` | ` **Keywords:** External Interface, Tri-State, Datasheet

Your task is to create a controller for the DE2-115's 2MB [SRAM](https://de.wikipedia.org/wiki/Static_random-access_memory) (IS61WV102416BLL).
A precompiled version of this core is already provided in [`lib/sram_ctrl`](../../../lib/sram_ctrl/doc.md), which is used by the [`vga_gfx_ctrl`](../../../vga_gfx_ctrl/doc.md) to access this memory.
You are supposed to implement your own version of this controller.

Start by checking out the [documentation](../../../lib/sram_ctrl/doc.md) of the precompiled controller.
The component declaration of the SRAM controller you shall implement is shown below:

```vhdl
component sram_ctrl is
	generic (
		ADDR_WIDTH : integer := 21;
		DATA_WIDTH : integer := 16;
		WR_BUF_SIZE : integer := 8
	);
	port (
		clk : in std_logic;
		res_n : in std_logic;

		-- write port (buffered)
		wr_addr : in std_logic_vector(ADDR_WIDTH-1 downto 0);
		wr_data : in std_logic_vector(DATA_WIDTH-1 downto 0);
		wr : in std_logic;
		wr_access_mode : in sram_access_mode_t;
		wr_empty : out std_logic;
		wr_full : out std_logic;
		wr_half_full : out std_logic;

		-- read port 1 (high priority)
		rd1_addr : in std_logic_vector(ADDR_WIDTH-1 downto 0);
		rd1 : in std_logic;
		rd1_access_mode : in sram_access_mode_t;
		rd1_busy : out std_logic;
		rd1_data : out std_logic_vector(DATA_WIDTH-1 downto 0);
		rd1_valid : out std_logic;

		-- read port 2 (low priority)
		rd2_addr : in std_logic_vector(ADDR_WIDTH-1 downto 0);
		rd2 : in std_logic;
		rd2_access_mode : in sram_access_mode_t;
		rd2_busy : out std_logic;
		rd2_data : out std_logic_vector(DATA_WIDTH-1 downto 0);
		rd2_valid : out std_logic;

		-- external interface to SRAM
		sram_dq : inout std_logic_vector(DATA_WIDTH-1 downto 0);
		sram_addr : out std_logic_vector(ADDR_WIDTH-2 downto 0);
		sram_ub_n : out std_logic;
		sram_lb_n : out std_logic;
		sram_we_n : out std_logic;
		sram_ce_n : out std_logic;
		sram_oe_n : out std_logic
	);
end component;
```

You can assume that the generics `ADDR_WIDTH` and `DATA_WIDTH` are always 21 and 16, respectively.
Furthermore, assume that the `clk` input is always driven by a 50 MHz clock signal (this information is important to be able to fulfill the timing constraints for the SRAM).
As usual `res_n` is an active-low reset input.

As the `sram_ctrl` documentation specifies, the write operations are buffered using a FIFO.
To implement this behavior, use a FIFO from the `mem_pkg` provided in [`lib/mem`](../../../lib/mem/doc.md).
Set its size according to the value of the generic `WR_BUF_SIZE`.


Also study the [SRAM Datasheet](https://tuwel.tuwien.ac.at/mod/resource/view.php?id=2205575) provided under *Additional Material* on TUWEL, to learn how the `sram_*` signals have to be operated.

As you can see, the data port of the SRAM (`sram_dq`) requires tri-state buffer control (indicated by the `inout` direction).
Hence, by default the data pins shall be assigned the `std_logic` value `'Z'` and **only** driven actively (i.e., set to `'1'` or `'0'`) when the controller writes data to memory.
Reading and writing at the same time is **not** possible!

All `sram_*` output signals shall be fed directly from registers.
The implementation [template](src/sram_ctrl.vhd) already contains code for that.
Don't generate any of those signals combinationally!

The inputs `rd1_addr`, `rd2_addr` and `wr_addr` specify byte addresses.
However, the SRAM is word-addressed, meaning that each memory location stores 16 bit of data.
Hence, notice that the width of the actual SRAM address signal (`sram_addr`) is one less than the one of the input address signals.
Individual bytes in the SRAM can be accessed using the (active-low) byte-select signals `sram_ub_n` (upper byte) and `sram_lb_n` (lower byte).

In case of a `BYTE` access to the write port of the SRAM controller the byte in `wr_data(7 downto 0)` shall be written to the SRAM address `wr_addr(20 downto 1)`.
If `wr_addr(0)` is set the upper byte must be selected, otherwise the lower byte.
For a `WORD` access, both bytes have to be selected.

**Hints**:

* You don't need any synchronizers for this task.
* When reading you can simply always set `sram_ce_n`, `sram_oe_n`, `sram_lb_n` and `sram_ub_n` low. You will need a multiplexer to the relay the correct part of `sram_data` to either `rd1_data` or `rd2_data`.
* When reading the timing diagrams and tables in the datasheet, always check whether a given parameter (e.g., the Read Cycle Time tRC) is a maximum or minimum value!
* The SRAM can be read in a single clock cycle. However, this does not mean that there is no latency involved from the point of view of the SRAM Controller's read ports.
* Writing the SRAM takes multiple clock cycles. You can use a simple state machine for that. Be sure to assert `rd1_busy` and `rd2_busy` while a writing to SRAM, to prevent new read operations form being issued.


## Testbench

Create a testbench for the `sram_ctrl` entity and place it in the file [`tb/sram_ctrl_tb.vhd`](tb/sram_ctrl_tb.vhd).

Show that your controller adheres to the timing specification of the SRAM datasheet for reads and writes, respectively.
More precisely, for writes, verify that your state machine complies with setup and hold time constraints of the memory.
Additionally, show how your controller handles simultaneous reads on both ports.

## Hardware Test

Add an instances of the `sram_ctrl` entity to the top-level architecture in [`top_arch_sram_ctrl.vhd`](top_arch_sram_ctrl.vhd).

Implement a simple state machine that first writes an 8 bit counter to the first 256 byte addresses of the SRAM controller.
Then it read them back and check whether the data is as expected.
Use LEDs to output the result of your check.

Once you are confident that your `sram_ctrl` works, integrate it into the [`vga_gfx_ctrl`](../../../lib/vga_gfx_ctrl/doc.md) and replace the precompiled version.
The best way to do this is to setup the [top-level architecture](../pt_snake/top_arch_snake.vhd) of the `snake` game Project Task.
This architecture contains a working version of the game which interacts with the `vga_gfx_ctrl`.
Change the [`vga_gfx_ctrl.vhd`](../../../lib/vga_gfx_ctrl/src/vga_gfx_ctrl.vhd) to use your SRAM Controller instead of the precompiled one and check if the design still works after doing so.

Additionally you can also add more elaborate test cases that test simultaneous reading on both read ports, interleaving reads and writes and so on.

