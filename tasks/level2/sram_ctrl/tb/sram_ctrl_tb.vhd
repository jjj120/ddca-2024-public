library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.sram_ctrl_pkg.all;
use work.tb_util_pkg.all;

entity sram_ctrl_tb is
	
end entity;

architecture bench of sram_ctrl_tb is

begin

	-- Instantiate the unit under test

	-- Stimulus process
	stimulus: process
	begin
		report "simulation start";
		
		-- Apply test stimuli

		
		wait for 10 ns;
		assert 1 = 0 report "Test x failed" severity error;

		report "simulation end";
		-- End simulation
		wait;
	end process;
end architecture;

