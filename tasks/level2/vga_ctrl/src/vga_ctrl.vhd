library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity vga_ctrl is
    generic (
        H_FRONT_PORCH  : integer; -- in clk cycles
        H_BACK_PORCH   : integer; -- in clk cycles
        H_SYNC_PULSE   : integer; -- in clk cycles
        H_VISIBLE_AREA : integer; -- the horizontal resolution
        V_FRONT_PORCH  : integer; -- in lines
        V_BACK_PORCH   : integer; -- in lines
        V_SYNC_PULSE   : integer; -- in lines
        V_VISIBLE_AREA : integer  -- the vertical resolution
    );
    port (
        clk             : in std_logic; -- 25 MHz
        res_n           : in std_logic; -- low active reset

        frame_start     : out std_logic;
        pix_data_r      : in std_logic_vector(7 downto 0); -- red pixel data
        pix_data_g      : in std_logic_vector(7 downto 0); -- green pixel data
        pix_data_b      : in std_logic_vector(7 downto 0); -- blue pixel data
        pix_ack         : out std_logic;                   -- read next pixel value from FIFO 

        -- connection to VGA connector/DAC
        vga_hsync       : out std_logic;
        vga_vsync       : out std_logic;
        vga_dac_clk     : out std_logic;
        vga_dac_blank_n : out std_logic;
        vga_dac_sync_n  : out std_logic;
        vga_dac_r       : out std_logic_vector(7 downto 0);
        vga_dac_g       : out std_logic_vector(7 downto 0);
        vga_dac_b       : out std_logic_vector(7 downto 0)
    );
end entity;

architecture arch of vga_ctrl is
    constant COLOR_DEPTH : integer := 8; --bit
    constant V_HEIGHT    : integer := V_VISIBLE_AREA + V_FRONT_PORCH + V_SYNC_PULSE + V_BACK_PORCH;
    constant H_WIDTH     : integer := H_VISIBLE_AREA + H_FRONT_PORCH + H_SYNC_PULSE + H_BACK_PORCH;

    type color_t is record
        r, g, b : std_logic_vector(COLOR_DEPTH - 1 downto 0);
    end record;

    type state_t is record
        h_px          : integer;
        v_px          : integer;
    end record;

    signal state, next_state : state_t := (
        h_px => 0,
        v_px => V_VISIBLE_AREA
    );
begin
    registers : process (clk)
    begin
        if rising_edge(clk) then
            if res_n = '0' then
                state <= (
                    h_px => 0,
                    v_px => V_VISIBLE_AREA
                );
            else
                state <= next_state;
            end if;
        end if;
    end process;

    next_state_logic : process (all)
    begin
        next_state <= state;

        next_state.h_px <= state.h_px + 1;

        -- end of line
        if state.h_px >= H_WIDTH - 1 then
            next_state.h_px <= 0;
            next_state.v_px <= state.v_px + 1;
        end if;
        
        -- end of frame
        if state.v_px >= V_HEIGHT - 1 and state.h_px >= H_WIDTH - 1 then
            next_state.h_px <= 0;
            next_state.v_px <= 0;
        end if;
    end process;

    vga_dac_clk <= clk;
    vga_dac_sync_n <= '1';

    output_logic : process (all)
    begin
        -- low active sync pulses
        if state.h_px >= H_VISIBLE_AREA + H_FRONT_PORCH and state.h_px < H_VISIBLE_AREA + H_FRONT_PORCH + H_SYNC_PULSE - 1 then
            vga_hsync <= '0';
        else 
            vga_hsync <= '1';
        end if;
        
        -- low active sync pulses
        if state.v_px >= V_VISIBLE_AREA + V_FRONT_PORCH and state.v_px < V_VISIBLE_AREA + V_FRONT_PORCH + V_SYNC_PULSE - 1 then
            vga_vsync <= '0';
        else 
            vga_vsync <= '1';
        end if;

        -- get next pixel during each line except for the last pixel
        if (state.v_px < V_VISIBLE_AREA and state.h_px < H_VISIBLE_AREA) and not (state.v_px = V_VISIBLE_AREA - 1 and state.h_px = H_VISIBLE_AREA - 1) then
            pix_ack <= '1';
        else
            pix_ack <= '0';
        end if;

        -- set color and blank
        if state.v_px < V_VISIBLE_AREA and state.h_px < H_VISIBLE_AREA then
            vga_dac_r <= pix_data_r;
            vga_dac_g <= pix_data_g;
            vga_dac_b <= pix_data_b;
            vga_dac_blank_n <= '1';
        else
            -- blank region
            vga_dac_r <= (others => '0');
            vga_dac_g <= (others => '0');
            vga_dac_b <= (others => '0');
            vga_dac_blank_n <= '0';
        end if;


        -- pull next frame start to on at the beginning first line after the visible area
        if state.v_px = V_VISIBLE_AREA then
            frame_start <= '1';
        else 
            frame_start <= '0';
        end if;
    end process;
end architecture;