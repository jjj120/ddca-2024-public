
# VGA Controller
**Points:** 2 ` | ` **Keywords:** FSM, External Interface, Oscilloscope

Your task is to create a VGA controller to interface with DE115-2's VGA connector and display images on external screens.
A precompiled version of this core can be found in [lib/vga_ctrl](../../../lib/vga_ctrl/doc.md).
The `vga_gfx_ctrl` uses this core to generate its output.
If you have a working implementation you can replace the precompiled core that it uses now with your own version.

## Task Description

The `vga_ctrl_pkg` package already includes a component declaration for the entity that you have to implement and place in `src/vga_ctrl.vhd`.

```vhdl
component vga_ctrl is
	generic (
		H_FRONT_PORCH  : integer; -- in clk cycles
		H_BACK_PORCH   : integer; -- in clk cycles
		H_SYNC_PULSE   : integer; -- in clk cycles
		H_VISIBLE_AREA : integer; -- the horizontal resolution
		V_FRONT_PORCH  : integer; -- in lines
		V_BACK_PORCH   : integer; -- in lines
		V_SYNC_PULSE   : integer; -- in lines
		V_VISIBLE_AREA : integer  -- the vertical resolution
	);
	port (
		clk     : in  std_logic;
		res_n   : in  std_logic;

		frame_start : out std_logic;
		pix_data_r  : in std_logic_vector(7 downto 0); -- red pixel data
		pix_data_g  : in std_logic_vector(7 downto 0); -- green pixel data
		pix_data_b  : in std_logic_vector(7 downto 0); -- blue pixel data
		pix_ack     : out std_logic; -- read next pixel value from FIFO 

		-- connection to VGA connector/DAC
		vga_hsync       : out std_logic;
		vga_vsync       : out std_logic;
		vga_dac_clk     : out std_logic;
		vga_dac_blank_n : out std_logic;
		vga_dac_sync_n  : out std_logic;
		vga_dac_r       : out std_logic_vector(7 downto 0);
		vga_dac_g       : out std_logic_vector(7 downto 0);
		vga_dac_b       : out std_logic_vector(7 downto 0)
	);
end component;
```

We will first explain how the VGA protocol works (external interface) and then how the actual pixel data is fed into  the`vga_ctrl`.

In the following we will explain the VGA protocol for the case of the resolution 640x480, since this is what we we will use to test the implementation (also the `vga_gfx_ctrl` uses this resolution).
However, all relevant signal timings are configurable using generics, so the core should also work for other resolutions as well.

The frequency of the `clk` signal in this example is expected to be 25 MHz, instead of the 50 MHz used in most other tasks.
You will need to instantiate a Pll, in order to generate this signal.

### External Interface
The VGA Controller has to communicate with the ADV7123 digital-to-analog converter (DAC) to generate the appropriate RGB analog component video signal, which is then output at the VGA connector of the board.
The actual color information for the individual pixels is transmitted using three analog signals (one for each color channel).
Since the FPGA cannot directly output analog voltages, the external DAC is required, which is connected to the FPGA via the `vga_dac_*` signals.

The figure below shows the physical video interface of the FPGA board.

![Video output circuitry of the FPGA board](.mdata/vga_output_circuit.svg)

Video information is transmitted frame by frame with a rate of approximately 60 frames per second. 
A frame is transmitted using a series of horizontal lines (in our case 525) starting at the top of the frame. 
Only a subset of these lines (in our case 480) actually represent the visible image, the rest is only needed for synchronization. 

A line has a duration of 800 25 MHz clock cycles.
Note that our video mode actually requires a pixel clock frequency of 25.175 MHz.
However, for the sake of simplicity we use 25 MHz, as most displays are fine with this slightly off-spec frequency.
We define the beginning of a frame with its visible portion. 
However, it's worth noting that online explanations may use alternative reference points.
Hence, pixel information is only transmitted in the first 640 cycles of each line.
The figure below shows the timing of a frame.

![Synchronization signal timing](.mdata/sync_signal_timing.svg)

The names and the values of the symbols A-H can be found in
the table below.

| Symbol| Parameter| Value (640x480) | Unit |
|-|------| ---------------| ------------------ | 
| A | Horizontal Visible Area | 640 | cycles|
| B | Horizontal Front Porch | 16 | cycles|
| C | Horizontal Sync. Pulse Width | 96 | cycles|
| D | Horizontal Back Porch | 48 | cycles|
| E | Vertical Visible Area | 480 | lines|
| F | Vertical Front Porch | 10 | lines|
| G | Vertical Sync. Pulse Width | 2 | lines|
| H | Vertical Back Porch | 33 | lines|


```vhdl
component vga_ctrl is
	generic (
		H_FRONT_PORCH  : integer; -- in clk cycles
		H_BACK_PORCH   : integer; -- in clk cycles
		H_SYNC_PULSE   : integer; -- in clk cycles
		H_VISIBLE_AREA : integer; -- the horizontal resolution
		V_FRONT_PORCH  : integer; -- in lines
		V_BACK_PORCH   : integer; -- in lines
		V_SYNC_PULSE   : integer; -- in lines
		V_VISIBLE_AREA : integer  -- the vertical resolution
	);
	port (
		clk     : in  std_logic;
		res_n   : in  std_logic;

		frame_start : out std_logic;
		pix_data_r  : in std_logic_vector(7 downto 0); -- red pixel data
		pix_data_g  : in std_logic_vector(7 downto 0); -- green pixel data
		pix_data_b  : in std_logic_vector(7 downto 0); -- blue pixel data
		pix_ack     : out std_logic; -- read next pixel value from FIFO 

		-- connection to VGA connector/DAC
		vga_hsync       : out std_logic;
		vga_vsync       : out std_logic;
		vga_dac_clk     : out std_logic;
		vga_dac_blank_n : out std_logic;
		vga_dac_sync_n  : out std_logic;
		vga_dac_r       : out std_logic_vector(7 downto 0);
		vga_dac_g       : out std_logic_vector(7 downto 0);
		vga_dac_b       : out std_logic_vector(7 downto 0)
	);
end component;
```

The figure below shows the transmission of two successive visible lines. During the horizontal and vertical blanking region, the analog signals must stay at 0V.

![Timing diagram showing two visible lines](.mdata/rgb_traces.svg)

When interfacing with the ADV7123 DAC, directly connect `clk` to the output `vga_dac_clk`.
The `vga_dac_sync_n` signal is not needed for this exercise and can be permanently set to `'1'`. 
Be sure to accommodate the fact that the digital-to-analog conversion is not instantaneous and delay your synchronization signals appropriately.
The datasheet for the DAC is available in TUWEL.

### Internal Interface

Data is fed into the `vga_ctrl` using the signals `pix_data_{r,g,b}`. 
The output signal `pix_ack` is used by the controller to indicate that the current pixel value has been processed and that the next one is needed.
Whenever it is asserted, a new pixel value is made available in the next cycle.
The timing diagram illustrates this behavior.

![Timing diagram showing](.mdata/data_timing.svg)

This interface is essentially the read port of a [FIFO](../../../lib/mem/doc.md) with first-word-fall-through behavior.
However, the valid signal is omitted here, because it is guaranteed that this FIFO will never run empty during a frame.
Hence, `pix_ack` must be asserted for exactly 640*480 cycles during one frame.

Additionally, your `vga_ctrl` also needs to produce the `frame_start` signal to indicate when a new frame is about to begin.
This gives the circuit that feeds the `vga_ctrl` time to fetch the required data (e.g., from external memory).
To do that `frame_start` needs to be asserted **at least** 64 cycles before the first pixel value is expected at `pix_data_{r,g,b}`.
You can, e.g., set it at the beginning of the last line.

## Testbench

Create a testbench for the `vga_ctrl` entity and place it in the file [`tb/vga_ctrl_tb.vhd`](tb/vga_ctrl_tb.vhd).

Add an instance of your `vga_ctrl` (the UUT) and connect it to an instance of the Test Pattern Generator (`tpg`) provided in the [`lib/vga_ctrl`](../../../lib/vga_ctrl/doc.md).

Check the output signals generated by your core either by checking the signal traces in the waveform viewer or by adding assertions to your code that verify certain aspects of the protocol (e.g., check for how many cycles in a frame the `pix_ack` signal was high).

## Hardware Test

Add an instance of the `vga_ctrl` and the `tpg` to the top-level architecture in [`top_arch_vga_ctrl.vhd`](top_arch_vga_ctrl.vhd).
Create a PLL (see the [Desginflow Tutorial](../../level0/designflow.md)) and configure it to output a 25 MHz clock.
Use this clock for the `clk` inputs of both the `vga_ctrl` and the `tpg`.

Make your design show this test picture on the screen attached to the FPGA board.

If that works you can also integrate you `vga_ctrl` into the [`vga_gfx_ctrl`](../../../lib/vga_gfx_ctrl/doc.md) and replace the `precompiled_vga_ctrl_640x480`.
The best way to do this is to setup the [top-level architecture](../pt_snake/top_arch_snake.vhd) of the `snake` game Project Task.
This architecture contains a working version of the game which interacts with the `vga_gfx_ctrl`.
Change the [`vga_gfx_ctrl.vhd`](../../../lib/vga_gfx_ctrl/src/vga_gfx_ctrl.vhd) to use your VGA Controller instead of the precompiled one and check if the design still works after doing so.

## Oscilloscope Measurement

Perform an oscilloscope measurement to demonstrate the correctness of the VGA signal you are generating.

If you are working in the TILab, connect the oscilloscope to an FPGA board using the provided cable.
Connect the three color channels and the horizontal synchronization signal to the four inputs of the device.

If you want to use the Remote Lab you have to use the host `ti50`. The board located at this host is already connected to an oscilloscope that can be controlled using our web interface.

Channels 1 to 3 of this oscilloscope are connected to the R, G and B channels of the VGA signal in this order, while channel 4 is connected to the horizontal synchronization (`hsync`) signal.

Make a measurement showing one of the visible lines that goes through the gradient area of the test image.
Use markers to measure the length of the whole line as well as the length of the synchronization pulse.
Make a screenshot of the measurement and show it to a tutor during the exercise interview.
The oscilloscope in the TILab is able to write to USB flash drives.
