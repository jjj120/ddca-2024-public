library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.vga_ctrl_pkg.all;

entity vga_ctrl_tb is
end entity;

architecture bench of vga_ctrl_tb is
	constant CLK_PERIOD     : time                         := 40 ns; -- 25 MHz
	signal fin              : std_logic                    := '0';

	-- values for 640x480
	constant H_FRONT_PORCH  : integer                      := 16;  -- in clk cycles
	constant H_BACK_PORCH   : integer                      := 48;  -- in clk cycles
	constant H_SYNC_PULSE   : integer                      := 96;  -- in clk cycles
	constant H_VISIBLE_AREA : integer                      := 640; -- the horizontal resolution
	constant V_FRONT_PORCH  : integer                      := 10;  -- in lines
	constant V_BACK_PORCH   : integer                      := 33;  -- in lines
	constant V_SYNC_PULSE   : integer                      := 2;   -- in lines
	constant V_VISIBLE_AREA : integer                      := 480; -- the vertical resolution

	signal clk              : std_logic                    := '0';
	signal res_n            : std_logic                    := '0';

	signal frame_start      : std_logic                    := '0';
	signal pix_data_r       : std_logic_vector(7 downto 0) := (others => '0'); -- red pixel data
	signal pix_data_g       : std_logic_vector(7 downto 0) := (others => '0'); -- green pixel data
	signal pix_data_b       : std_logic_vector(7 downto 0) := (others => '0'); -- blue pixel data
	signal pix_ack          : std_logic                    := '0';             -- read next pixel value from FIFO 

	-- connection to VGA connector/DAC
	signal vga_hsync        : std_logic                    := '1';
	signal vga_vsync        : std_logic                    := '1';
	signal vga_dac_clk      : std_logic                    := '1';
	signal vga_dac_blank_n  : std_logic                    := '1';
	signal vga_dac_sync_n   : std_logic                    := '1';
	signal vga_dac_r        : std_logic_vector(7 downto 0) := (others => '0');
	signal vga_dac_g        : std_logic_vector(7 downto 0) := (others => '0');
	signal vga_dac_b        : std_logic_vector(7 downto 0) := (others => '0');
begin
	-- Instantiate the unit under test
	uut : vga_ctrl
	generic map(
		H_FRONT_PORCH  => H_FRONT_PORCH,
		H_BACK_PORCH   => H_BACK_PORCH,
		H_SYNC_PULSE   => H_SYNC_PULSE,
		H_VISIBLE_AREA => H_VISIBLE_AREA,
		V_FRONT_PORCH  => V_FRONT_PORCH,
		V_BACK_PORCH   => V_BACK_PORCH,
		V_SYNC_PULSE   => V_SYNC_PULSE,
		V_VISIBLE_AREA => V_VISIBLE_AREA
	)
	port map(
		clk             => clk,
		res_n           => res_n,
		frame_start     => frame_start,
		pix_data_r      => pix_data_r,
		pix_data_g      => pix_data_g,
		pix_data_b      => pix_data_b,
		pix_ack         => pix_ack,
		vga_hsync       => vga_hsync,
		vga_vsync       => vga_vsync,
		vga_dac_clk     => vga_dac_clk,
		vga_dac_blank_n => vga_dac_blank_n,
		vga_dac_sync_n  => vga_dac_sync_n,
		vga_dac_r       => vga_dac_r,
		vga_dac_g       => vga_dac_g,
		vga_dac_b       => vga_dac_b
	);

	testpattern_gen : tpg
	port map(
		clk         => clk,
		res_n       => res_n,
		frame_start => frame_start,
		pix_ack     => pix_ack,
		pix_data_r  => pix_data_r,
		pix_data_g  => pix_data_g,
		pix_data_b  => pix_data_b
	);
	-- Stimulus process
	stimulus : process
	begin
		report "simulation start";
		res_n <= '1';

		-- Apply test stimuli
		for i in 0 to (H_VISIBLE_AREA + H_FRONT_PORCH + H_SYNC_PULSE + H_BACK_PORCH) * (V_VISIBLE_AREA + V_FRONT_PORCH + V_SYNC_PULSE + V_BACK_PORCH) + 10 loop
			wait until rising_edge(clk);
		end loop;

		fin <= '1';
		report "simulation end";
		-- End simulation
		wait;
	end process;

	clock : process is
	begin
		clk <= '1';
		wait for CLK_PERIOD / 2;
		clk <= '0';
		wait for CLK_PERIOD / 2;

		if fin then
			wait;
		end if;
	end process;
end architecture;
