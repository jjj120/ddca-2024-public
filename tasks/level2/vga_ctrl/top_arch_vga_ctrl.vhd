library ieee;
use ieee.std_logic_1164.all;

use work.vga_ctrl_pkg.all;
architecture top_arch_vga_ctrl of top is
    signal vga_clk          : std_logic                    := '0';
    signal res_n            : std_logic                    := '1';

    -- values for 640x480
    constant H_FRONT_PORCH  : integer                      := 16;
    constant H_BACK_PORCH   : integer                      := 48;
    constant H_SYNC_PULSE   : integer                      := 96;
    constant H_VISIBLE_AREA : integer                      := 640;
    constant V_FRONT_PORCH  : integer                      := 10;
    constant V_BACK_PORCH   : integer                      := 33;
    constant V_SYNC_PULSE   : integer                      := 2;
    constant V_VISIBLE_AREA : integer                      := 480;

    -- connection to testpicture generator
    signal frame_start      : std_logic                    := '0';
    signal pix_data_r       : std_logic_vector(7 downto 0) := (others => '0'); -- red pixel data
    signal pix_data_g       : std_logic_vector(7 downto 0) := (others => '0'); -- green pixel data
    signal pix_data_b       : std_logic_vector(7 downto 0) := (others => '0'); -- blue pixel data
    signal pix_ack          : std_logic                    := '0';             -- read next pixel value from FIFO 
begin
    -- instantiate pll for slower clock
    pll_inst : entity work.pll_vga port map(
        inclk0 => clk,
        c0     => vga_clk
        );

    -- Instantiate the unit under test
    uut : vga_ctrl
    generic map(
        H_FRONT_PORCH  => H_FRONT_PORCH,
        H_BACK_PORCH   => H_BACK_PORCH,
        H_SYNC_PULSE   => H_SYNC_PULSE,
        H_VISIBLE_AREA => H_VISIBLE_AREA,
        V_FRONT_PORCH  => V_FRONT_PORCH,
        V_BACK_PORCH   => V_BACK_PORCH,
        V_SYNC_PULSE   => V_SYNC_PULSE,
        V_VISIBLE_AREA => V_VISIBLE_AREA
    )
    port map(
        clk             => vga_clk,
        res_n           => res_n,
        frame_start     => frame_start,
        pix_data_r      => pix_data_r,
        pix_data_g      => pix_data_g,
        pix_data_b      => pix_data_b,
        pix_ack         => pix_ack,
        vga_hsync       => vga_hsync,
        vga_vsync       => vga_vsync,
        vga_dac_clk     => vga_dac_clk,
        vga_dac_blank_n => vga_dac_blank_n,
        vga_dac_sync_n  => vga_dac_sync_n,
        vga_dac_r       => vga_dac_r,
        vga_dac_g       => vga_dac_g,
        vga_dac_b       => vga_dac_b
    );

    testpattern_gen : tpg
    port map(
        clk         => vga_clk,
        res_n       => res_n,
        frame_start => frame_start,
        pix_ack     => pix_ack,
        pix_data_r  => pix_data_r,
        pix_data_g  => pix_data_g,
        pix_data_b  => pix_data_b
    );

    res_n <= keys(0);
end architecture;
