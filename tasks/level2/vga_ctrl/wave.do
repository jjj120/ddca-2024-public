onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /vga_ctrl_tb/clk
add wave -noupdate /vga_ctrl_tb/res_n
add wave -noupdate /vga_ctrl_tb/frame_start
add wave -noupdate /vga_ctrl_tb/pix_data_r
add wave -noupdate /vga_ctrl_tb/pix_data_g
add wave -noupdate /vga_ctrl_tb/pix_data_b
add wave -noupdate /vga_ctrl_tb/pix_ack
add wave -noupdate /vga_ctrl_tb/vga_hsync
add wave -noupdate /vga_ctrl_tb/vga_vsync
add wave -noupdate /vga_ctrl_tb/vga_dac_clk
add wave -noupdate /vga_ctrl_tb/vga_dac_blank_n
add wave -noupdate /vga_ctrl_tb/vga_dac_sync_n
add wave -noupdate /vga_ctrl_tb/vga_dac_r
add wave -noupdate /vga_ctrl_tb/vga_dac_g
add wave -noupdate /vga_ctrl_tb/vga_dac_b
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {12902442 ns}
