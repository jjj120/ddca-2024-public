library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.rv_sys_pkg.all;
    use work.rv_core_pkg.all;
    use work.rv_util_pkg.all;
    use work.math_pkg.log2c;
    use work.alu_pkg.all;

entity rv_fsm is
    port (
        clk      : in  std_logic;
        res_n    : in  std_logic;
        -- Interface to instruction memory
        imem_out : out mem_out_t;
        imem_in  : in  mem_in_t;
        -- Interface to data memory
        dmem_out : out mem_out_t;
        dmem_in  : in  mem_in_t
    );
end entity;

architecture arch of rv_fsm is
    constant ADDR_WIDTH : integer := log2c(REG_COUNT);

    ------------------------------------------------------------------------
    -- 							  REGISTERS
    ------------------------------------------------------------------------
    type register_sig_t is record
        rd_addr : std_logic_vector(ADDR_WIDTH - 1 downto 0);
        rd      : std_logic;
        wr_addr : std_logic_vector(ADDR_WIDTH - 1 downto 0);
        wr_data : std_logic_vector(DATA_WIDTH - 1 downto 0);
        wr      : std_logic;
    end record;

    constant REG_NO_INTERACTION : register_sig_t := (
        rd_addr => (others => '0'),
        rd      => '0',
        wr_addr => (others => '0'),
        wr_data => (others => '0'),
        wr      => '0'
    );

    signal registers     : register_sig_t                            := REG_NO_INTERACTION;
    signal registers_out : std_logic_vector(DATA_WIDTH - 1 downto 0) := (others => '0');

    alias rfrd is registers.rd;
    alias rfwr is registers.wr;

    ------------------------------------------------------------------------
    -- 								  ALU
    ------------------------------------------------------------------------
    type alu_sig_in_t is record
        op   : alu_op_type;
        A, B : std_logic_vector(DATA_WIDTH - 1 downto 0);
    end record;

    type alu_sig_out_t is record
        R : std_logic_vector(DATA_WIDTH - 1 downto 0);
        Z : std_logic;
    end record;

    constant ALU_NO_INTERACTION : alu_sig_in_t := (
        op => ALU_NOP,
        A  => (others => '0'),
        B  => (others => '0')
    );

    signal alu_in  : alu_sig_in_t  := ALU_NO_INTERACTION;
    signal alu_out : alu_sig_out_t := (
        R => (others => '0'),
        Z => '0'
    );

    alias z     is alu_out.Z;
    alias aluop is alu_in.op;

    ------------------------------------------------------------------------
    -- 							WORKING REGISTERS
    ------------------------------------------------------------------------
    alias pc_t is data_t;

    signal pc : pc_t    := (others => '0');
    signal ir : instr_t := (others => '0');
    signal rs : data_t  := (others => '0');
    signal nx : pc_t    := (others => '0');
    signal wb : data_t  := (others => '0');

    signal next_pc : pc_t    := (others => '0');
    signal next_ir : instr_t := (others => '0');
    signal next_rs : data_t  := (others => '0');
    signal next_nx : pc_t    := (others => '0');
    signal next_wb : data_t  := (others => '0');

    signal en_pc : bit := '0';
    signal en_ir : bit := '0';
    signal en_rs : bit := '0';
    signal en_nx : bit := '0';
    signal en_wb : bit := '0';

    ------------------------------------------------------------------------
    -- 							  MUX SIGNALS
    ------------------------------------------------------------------------
    subtype mux_ctrl_wide_t is std_logic_vector(1 downto 0);

    constant MUX_MA_PC : mux_ctrl_wide_t := "00";
    constant MUX_MA_RF : mux_ctrl_wide_t := "01";
    constant MUX_MA_RS : mux_ctrl_wide_t := "10";

    constant MUX_MB_4   : mux_ctrl_wide_t := "00";
    constant MUX_MB_RF  : mux_ctrl_wide_t := "01";
    constant MUX_MB_IMM : mux_ctrl_wide_t := "10";

    constant MUX_MS_RS1 : std_logic := '0';
    constant MUX_MS_RS2 : std_logic := '1';

    constant MUX_MP_NX : std_logic := '0';
    constant MUX_MP_WB : std_logic := '1';

    constant MUX_MW_DMEM : std_logic := '0';
    constant MUX_MW_WB   : std_logic := '1';

    signal ma, mb     : mux_ctrl_wide_t := "00";
    signal ms, mp, mw : std_logic       := '0';

    ------------------------------------------------------------------------
    -- 							 OTHER SIGNALS
    ------------------------------------------------------------------------
    signal immediate     : data_t    := (others => '0');
    signal instr         : instr_t   := (others => '0');
    signal get_new_instr : std_logic := '0';

    alias opcode is instr(WIDTH_OPCODE + INDEX_OPCODE - 1 downto INDEX_OPCODE);
    alias funct3 is instr(WIDTH_FUNCT3 + INDEX_FUNCT3 - 1 downto INDEX_FUNCT3);
    alias funct7 is instr(WIDTH_FUNCT7 + INDEX_FUNCT7 - 1 downto INDEX_FUNCT7);

    ------------------------------------------------------------------------
    -- 							 FSM STATES
    ------------------------------------------------------------------------
    type fsm_state is (
            STATE_IDLE,
            STATE_LOAD_INSTR,
            STATE_LOAD_RS1,
            STATE_LOAD_RS2,
            STATE_ALU_OP,
            STATE_DMEM_OP,
            STATE_WRITE_BACK
        );

    type state_t is record
        state : fsm_state;
    end record;

    constant DEFAULT_FSM_STATE : state_t := (
        state => STATE_IDLE
    );

    signal state, next_state : state_t;

    ------------------------------------------------------------------------
    -- 						   MEMORY INTERFACE
    ------------------------------------------------------------------------
    signal dop : memu_op_t := MEMU_NOP;

    signal dmem_rd_data : data_t;

    alias ibusy is imem_in.busy;
    signal dbusy : std_logic := '0';

    ------------------------------------------------------------------------
    -- 						      DEBUGGING
    ------------------------------------------------------------------------
    -- pragma translate_off
    -- string signal for debugging in Questa Sim
    signal instr_test_string : string(1 to 200) := (others => ' ');
    -- pragma translate_on
begin
    -- pragma translate_off
    dbg: process (all) is
    begin
        instr_test_string <= (others => ' ');
        instr_test_string(to_inst_string(instr)'range) <= to_inst_string(instr);
    end process;
    -- pragma translate_on
    instr <= ir when get_new_instr = '0' else swap_endianness(imem_in.rddata);

    -- combinatorial logic for getting parts of instruction
    immediate <= get_immediate(instr);

    imem_out.address <= pc(RV_SYS_ADDR_WIDTH + 1 downto 2);
    imem_out.wrdata  <= (others => '0');
    imem_out.wr      <= '0';
    imem_out.byteena <= "1111";
    next_ir          <= swap_endianness(imem_in.rddata);

    registers.wr_addr <= get_rd(instr);

    -- MUX MA and MB
    alu_in.A <= pc when ma = MUX_MA_PC else registers_out when ma = MUX_MA_RF else rs when ma = MUX_MA_RS else (others => '0');
    alu_in.B <= 32x"4" when mb = MUX_MB_4 else registers_out when mb = MUX_MB_RF else immediate when mb = MUX_MB_IMM else (others => '0');

    -- MUX MS
    registers.rd_addr <= get_rs1(instr) when ms = MUX_MS_RS1 else get_rs2(instr) when ms = MUX_MS_RS2 else (others => '0');

    -- MUX MP
    next_pc <= nx when mp = MUX_MP_NX else wb when mp = MUX_MP_WB else (others => '0');

    -- MUX MW
    registers.wr_data <= dmem_rd_data when mw = MUX_MW_DMEM else wb when mw = MUX_MW_WB else (others => '0');

    next_nx <= alu_out.R;
    next_wb <= alu_out.R;
    next_rs <= registers_out;

    rf_ram_inst: entity work.rf_ram
        generic map (
            ADDR_WIDTH => ADDR_WIDTH,
            DATA_WIDTH => DATA_WIDTH
        )
        port map (
            clk     => clk,
            rd_addr => registers.rd_addr,
            rd_data => registers_out,
            rd      => registers.rd,
            wr_addr => registers.wr_addr,
            wr_data => registers.wr_data,
            wr      => registers.wr
        );

    alu_inst: entity work.alu
        generic map (
            DATA_WIDTH => DATA_WIDTH
        )
        port map (
            op => alu_in.op,
            A  => alu_in.A,
            B  => alu_in.B,
            R  => alu_out.R,
            Z  => alu_out.Z
        );

    dmem_memu_inst: memu
        port map (
            op      => dop,
            addr    => wb,
            wrdata  => registers_out,
            rddata  => dmem_rd_data,
            busy    => dbusy,
            xl      => open,
            xs      => open,
            mem_in  => dmem_in,
            mem_out => dmem_out
        );

    working_registers_proc: process (clk, res_n)
    begin
        if res_n /= '1' then
            ir <= (others => '0');
            nx <= (others => '0');
            pc <= (others => '0');
            rs <= (others => '0');
            wb <= (others => '0');

            state <= DEFAULT_FSM_STATE;
        elsif rising_edge(clk) then
            state <= next_state;
            -- per default no change, only change if enable of the register is high
            ir <= ir;
            nx <= nx;
            pc <= pc;
            rs <= rs;
            wb <= wb;

            if en_ir then
                ir <= next_ir;
            end if;
            if en_nx then
                nx <= next_nx;
            end if;
            if en_pc then
                pc <= next_pc;
                pc(0) <= '0';
            end if;
            if en_rs then
                rs <= next_rs;
            end if;
            if en_wb then
                wb <= next_wb;
            end if;
        end if;
    end process;

    fsm_next_state_logic: process (all) is
        pure function get_alu_op(instr_in : instr_t) return alu_op_type is
        begin
            case get_funct3(instr_in) is
                when FUNCT3_ADD_SUB =>
                    if get_opcode(instr_in) = OPCODE_OP then
                        case get_funct7(instr_in) is
                            when FUNCT7_ADD => return ALU_ADD;
                            when FUNCT7_SUB => return ALU_SUB;
                            when others => assert 1 = 0 report "Invalid funct7 in get_alu_op add" severity failure;
                        end case;
                    else -- immediate opcode
                        return ALU_ADD;
                    end if;
                when FUNCT3_SRL_SRA =>
                    if get_opcode(instr_in) = OPCODE_OP then
                        case get_funct7(instr_in) is
                            when FUNCT7_SRL => return ALU_SRL;
                            when FUNCT7_SRA => return ALU_SRA;
                            when others => assert 1 = 0 report "Invalid funct7 in get_alu_op shift" severity failure;
                        end case;
                    else -- immediate opcode
                        case get_immediate(instr_in)(10) is
                            when '0' => return ALU_SRL;
                            when '1' => return ALU_SRA;
                            when others => assert 1 = 0 report "Invalid funct7 in get_alu_op shift_imm" severity failure;
                        end case;
                    end if;
                when FUNCT3_SLL => return ALU_SLL;
                when FUNCT3_SLT => return ALU_SLT;
                when FUNCT3_SLTU => return ALU_SLTU;
                when FUNCT3_XOR => return ALU_XOR;
                when FUNCT3_OR => return ALU_OR;
                when FUNCT3_AND => return ALU_AND;
                when others => return ALU_NOP;
            end case;
            return ALU_NOP;
        end function;

        pure function get_memu_access_type(instr_in : instr_t) return memu_access_type_t is
        begin
            case get_funct3(instr_in) is
                when FUNCT3_LB => return MEM_B; -- FUNCT3_LB = FUNCT3_SB
                when FUNCT3_LH => return MEM_H; -- FUNCT3_LH = FUNCT3_SH
                when FUNCT3_LW => return MEM_W; -- FUNCT3_LW = FUNCT3_SW
                when FUNCT3_LBU => return MEM_BU;
                when FUNCT3_LHU => return MEM_HU;
                when others => assert 1 = 0 report "Invalid funct3 in get_memu_access_type" severity failure;
            end case;
            return MEM_W;
        end function;
    begin
        next_state <= state;

        -- disable getting new values on all registers
        en_ir <= '0';
        en_nx <= '0';
        en_pc <= '0';
        en_rs <= '0';
        en_wb <= '0';
        rfrd <= '0';
        rfwr <= '0';

        ma <= (others => '0');
        mb <= (others => '0');
        ms <= '0';
        mp <= '0';
        mw <= '0';

        imem_out.rd <= '0';
        dop <= MEMU_NOP;
        aluop <= ALU_NOP;
        get_new_instr <= '0';

        case state.state is
            when STATE_IDLE =>
                next_state.state <= STATE_LOAD_INSTR;

            when STATE_LOAD_INSTR =>
                imem_out.rd <= '1';
                next_state.state <= STATE_LOAD_RS1;
                en_ir <= '1';
                get_new_instr <= '1';

                -- increase pc by 4 and write to nx and wb
                aluop <= ALU_ADD;
                ma <= MUX_MA_PC;
                mb <= MUX_MB_4;
                en_nx <= '1';
                en_wb <= '1'; -- save pc + 4 into wb register to use in jump instruction

            when STATE_LOAD_RS1 =>
                en_ir <= '1';
                get_new_instr <= '1';
                -- pragma translate_off
                report to_inst_string(instr) & gen_dbg_string(pc, instr, 2);
                -- pragma translate_on
                case get_opcode(instr) is -- catch the fresh new instruction that is not in the ir
                    when OPCODE_BRANCH | OPCODE_OP =>
                        next_state.state <= STATE_LOAD_RS2;
                    when OPCODE_JAL | OPCODE_JALR | OPCODE_LUI | OPCODE_AUIPC | OPCODE_STORE | OPCODE_LOAD | OPCODE_OP_IMM =>
                        next_state.state <= STATE_ALU_OP;
                    when OPCODE_FENCE =>
                        next_state.state <= STATE_LOAD_INSTR;
                    when 7x"0" =>
                        assert 1 = 0 report "GOT ALL ZEROS OPCODE" severity failure;
                    when others =>
                        assert 1 = 0 report "INVALID OPCODE IN STATE_LOAD_RS1" severity failure;
                end case;

                rfrd <= '1';
                ms <= MUX_MS_RS1;

            when STATE_LOAD_RS2 =>
                en_rs <= '1'; -- save rs1 from previous read
                next_state.state <= STATE_ALU_OP;
                ms <= MUX_MS_RS2;
                rfrd <= '1';

            when STATE_ALU_OP =>
                case opcode is
                    when OPCODE_LUI | OPCODE_AUIPC | OPCODE_JAL | OPCODE_JALR | OPCODE_OP | OPCODE_OP_IMM =>
                        next_state.state <= STATE_WRITE_BACK;
                    when OPCODE_BRANCH =>
                        next_state.state <= STATE_WRITE_BACK;
                    when OPCODE_LOAD | OPCODE_STORE =>
                        next_state.state <= STATE_DMEM_OP;
                    when others => assert 1 = 0 report "Invalid opcode in STATE_ALU_OP" severity failure;
                end case;

                case opcode is
                    when OPCODE_LUI =>
                        aluop <= ALU_NOP; -- write immediate through to wb register
                        mb <= MUX_MB_IMM;
                        en_wb <= '1';

                    when OPCODE_AUIPC =>
                        aluop <= ALU_ADD;
                        ma <= MUX_MA_PC;
                        mb <= MUX_MB_IMM;
                        en_wb <= '1';

                    when OPCODE_BRANCH =>
                        aluop <= ALU_ADD;
                        ma <= MUX_MA_PC;
                        mb <= MUX_MB_IMM;
                        en_wb <= '1';

                    when OPCODE_LOAD | OPCODE_STORE =>
                        aluop <= ALU_ADD;
                        ma <= MUX_MA_RF;
                        mb <= MUX_MB_IMM;
                        en_wb <= '1';

                    when OPCODE_JAL =>
                        aluop <= ALU_ADD;
                        ma <= MUX_MA_PC;
                        mb <= MUX_MB_IMM;
                        en_nx <= '1';

                    when OPCODE_JALR =>
                        aluop <= ALU_ADD;
                        ma <= MUX_MA_RF;
                        mb <= MUX_MB_IMM;
                        en_nx <= '1';

                    when OPCODE_OP =>
                        aluop <= get_alu_op(instr);
                        ma <= MUX_MA_RS;
                        mb <= MUX_MB_RF;
                        en_wb <= '1';

                    when OPCODE_OP_IMM =>
                        aluop <= get_alu_op(instr);
                        ma <= MUX_MA_RF;
                        mb <= MUX_MB_IMM;
                        en_wb <= '1';

                    when others => assert 1 = 0 report "Invalid opcode in STATE_ALU_OP" severity failure;
                end case;

            when STATE_DMEM_OP =>
                case opcode is
                    when OPCODE_STORE =>
                        -- load data from rs2 to write to dmem
                        ms <= MUX_MS_RS2;
                        rfrd <= '1';

                    when OPCODE_LOAD =>
                        -- load data from dmem
                        dop.rd <= '1';
                        dop.access_type <= get_memu_access_type(instr);

                    when others => assert 1 = 0 report "Invalid opcode in STATE_DMEM_OP" severity failure;
                end case;
                next_state.state <= STATE_WRITE_BACK;

            when STATE_WRITE_BACK =>
                next_state.state <= STATE_LOAD_INSTR;
                en_pc <= '1';

                case opcode is
                    when OPCODE_AUIPC | OPCODE_JAL | OPCODE_JALR | OPCODE_OP | OPCODE_OP_IMM | OPCODE_LUI =>
                        rfwr <= '1';
                        mw <= MUX_MW_WB;
                        mp <= MUX_MP_NX;

                    when OPCODE_STORE => -- write to dmem (read from registers was in cycle before)
                        dop.wr <= '1';
                        dop.access_type <= get_memu_access_type(instr);

                    when OPCODE_LOAD => -- dmem read already done, only assert dmem access_type
                        dop.access_type <= get_memu_access_type(instr);
                        rfwr <= '1';
                        mw <= MUX_MW_DMEM;

                    when OPCODE_BRANCH =>
                        ma <= MUX_MA_RS;
                        mb <= MUX_MB_RF;

                        assert z = '1' or z = '0' report "invalid z";

                        mp <= MUX_MP_NX; -- default NOT branch
                        case funct3 is
                            when FUNCT3_BEQ =>
                                aluop <= ALU_SUB; -- sub to compare equality
                                if z = '1' then
                                    mp <= MUX_MP_WB; -- branch
                                end if;
                            when FUNCT3_BNE =>
                                aluop <= ALU_SUB; -- sub to compare equality
                                if z = '0' then
                                    mp <= MUX_MP_WB; -- branch
                                end if;
                            when FUNCT3_BLT =>
                                aluop <= ALU_SLT;
                                if z = '0' then
                                    mp <= MUX_MP_WB; -- branch
                                end if;
                            when FUNCT3_BGE =>
                                aluop <= ALU_SLT;
                                if z = '1' then
                                    mp <= MUX_MP_WB; -- branch
                                end if;
                            when FUNCT3_BLTU =>
                                aluop <= ALU_SLTU;
                                if z = '0' then
                                    mp <= MUX_MP_WB; -- branch
                                end if;
                            when FUNCT3_BGEU =>
                                aluop <= ALU_SLTU;
                                if z = '1' then
                                    mp <= MUX_MP_WB; -- branch
                                end if;

                            when others => assert 1 = 0 report "Got unknown func3 in branch" severity failure;
                        end case;
                    when others => null;
                end case;

            when others => assert 1 = 0 report "Got unknown fsm state" severity failure;
        end case;
    end process;
end architecture;
