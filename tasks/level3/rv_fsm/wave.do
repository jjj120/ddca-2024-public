onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /rv_fsm_tb/clk
add wave -noupdate /rv_fsm_tb/res_n
add wave -noupdate /rv_fsm_tb/imem_in
add wave -noupdate /rv_fsm_tb/dmem_in
add wave -noupdate /rv_fsm_tb/imem_out
add wave -noupdate -expand /rv_fsm_tb/dmem_out
add wave -noupdate /rv_fsm_tb/uut/instr
add wave -noupdate /rv_fsm_tb/uut/immediate
add wave -noupdate /rv_fsm_tb/uut/pc
add wave -noupdate /rv_fsm_tb/uut/ir
add wave -noupdate /rv_fsm_tb/uut/rs
add wave -noupdate /rv_fsm_tb/uut/nx
add wave -noupdate /rv_fsm_tb/uut/wb
add wave -noupdate /rv_fsm_tb/uut/en_pc
add wave -noupdate /rv_fsm_tb/uut/en_ir
add wave -noupdate /rv_fsm_tb/uut/en_rs
add wave -noupdate /rv_fsm_tb/uut/en_nx
add wave -noupdate /rv_fsm_tb/uut/en_wb
add wave -noupdate /rv_fsm_tb/uut/state
add wave -noupdate /rv_fsm_tb/uut/alu_in
add wave -noupdate -expand /rv_fsm_tb/uut/alu_out
add wave -noupdate -expand /rv_fsm_tb/uut/registers
add wave -noupdate /rv_fsm_tb/uut/registers_out
add wave -noupdate /rv_fsm_tb/uut/instr_test_string
add wave -noupdate /rv_fsm_tb/uut/dmem_memu_inst/xl
add wave -noupdate /rv_fsm_tb/uut/dmem_memu_inst/xs
add wave -noupdate /rv_fsm_tb/uut/ma
add wave -noupdate /rv_fsm_tb/uut/mb
add wave -noupdate /rv_fsm_tb/uut/ms
add wave -noupdate /rv_fsm_tb/uut/mp
add wave -noupdate /rv_fsm_tb/uut/mw
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {6128 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 149
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {6 us} {6400 ns}
