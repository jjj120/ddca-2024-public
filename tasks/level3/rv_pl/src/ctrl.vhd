library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.rv_core_pkg.all;
    use work.rv_pl_op_pkg.all;

entity rv_pl_ctrl is
    port (
        clk       : in  std_logic;
        res_n     : in  std_logic;

        fe        : out ctrl_t;
        dec       : out ctrl_t;
        exec      : out ctrl_t;
        mem       : out ctrl_t;
        wb        : out ctrl_t;

        d2e       : in  d2e_t;     -- from decode stage
        e2m       : in  e2m_t;     -- from execute stage
        m2f       : in  m2f_t;     -- from memory stage

        imem_busy :     std_logic; -- from fetch stage
        dmem_busy :     std_logic  -- from memory stage
    );
end entity;

architecture simple of rv_pl_ctrl is
    signal state : std_logic_vector(3 downto 0);
    signal stall : std_logic;
begin
    stall <= imem_busy or dmem_busy;

    sync: process (clk, res_n)
    begin
        if res_n = '0' then
            state <= (0 => '1', others => '0');
        elsif rising_edge(clk) then
            if stall = '0' then
                state <= state(0) & state(3 downto 1);
            end if;
        end if;
    end process;

    process (all)
    begin
        fe.flush <= '1';
        fe.stall <= '1';

        if stall = '0' then
            if state(0) = '1' then
                fe.stall <= '0';
                fe.flush <= '0';
            end if;

            if m2f.branch = '1' then
                fe.stall <= '0';
            end if;
        end if;
    end process;

    dec.stall  <= stall;
    exec.stall <= stall;
    mem.stall  <= stall;
    wb.stall   <= stall;

    dec.flush  <= '0';
    exec.flush <= '0';
    mem.flush  <= '0';
    wb.flush   <= '0';
end architecture;

architecture hazard_handling of rv_pl_ctrl is
    -- the hazard_handling architecture somehow doesnt work, I dont know why
    signal mem_busy : std_logic := '0';
begin
    process (all)
        variable is_load_hazard : boolean;
    begin
        mem_busy <= imem_busy or dmem_busy;

        fe.stall <= mem_busy;
        dec.stall <= mem_busy;
        exec.stall <= mem_busy;
        mem.stall <= mem_busy;
        wb.stall <= mem_busy;

        fe.flush <= '0';
        dec.flush <= '0';
        exec.flush <= '0';
        mem.flush <= '0';
        wb.flush <= '0';

        if m2f.branch then
            -- branch taken, fetch, decode and exec have to be flushed
            fe.flush <= '1';
            dec.flush <= '1';
            exec.flush <= '1';
            -- report "Ctrl at " & to_string(now) & ": Flush fetch, decode and exec";
        end if;

        is_load_hazard := ((e2m.wb_op.src = SEL_MEM_RESULT and e2m.wb_op.write = '1') and (e2m.mem_op.memu_op.rd = '1') and ((e2m.wb_op.rd = d2e.exec_op.rs1) or (e2m.wb_op.rd = d2e.exec_op.rs2)) and m2f.branch = '0');

        if is_load_hazard then
            fe.stall <= '1';
            dec.stall <= '1';
            exec.flush <= '1';
        end if;
    end process;
end architecture;
