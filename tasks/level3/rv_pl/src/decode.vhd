library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.rv_core_pkg.all;
    use work.rv_sys_pkg.all;
    use work.rv_alu_pkg.all;
    use work.rv_pl_op_pkg.all;
    use work.rv_util_pkg.all;

entity rv_pl_decode is
    port (
        clk     : in  std_logic;
        res_n   : in  std_logic;

        ctrl    : in  ctrl_t;   -- from ctrl unit
        f2d     : in  f2d_t;    -- from fetch stage
        w2d     : in  regwr_t;  -- from writeback stage
        d2e     : out d2e_t;    -- to execute stage

        exc_dec : out std_logic -- decode exception
    );
end entity;

architecture rtl of rv_pl_decode is
    signal pc    : data_t  := (others => '0');
    signal instr : instr_t := (others => '0');

    alias aluop   is d2e.exec_op.alu_op;
    alias alu_a   is d2e.exec_op.alu_a_src;
    alias alu_b   is d2e.exec_op.alu_b_src;
    alias brt_src is d2e.exec_op.brt_src;
    alias mem_op  is d2e.mem_op;
    alias wb_op   is d2e.wb_op;

    ------------------------------------------------------------------------
    -- 						      DEBUGGING
    ------------------------------------------------------------------------
    -- pragma translate_off
    -- string signal for debugging in Questa Sim
    signal instr_test_string : string(1 to 200) := (others => ' ');
    -- pragma translate_on
begin
    -- pragma translate_off
    dbg: process (all) is
    begin
        instr_test_string <= (others => ' ');
        instr_test_string(to_inst_string(instr)'range) <= to_inst_string(instr);
    end process;

    -- pragma translate_on
    process (all) is
        pure function get_alu_op(instr_in : instr_t) return rv_alu_op_t is
        begin
            case get_funct3(instr_in) is
                when FUNCT3_ADD_SUB =>
                    if get_opcode(instr_in) = OPCODE_OP then
                        case get_funct7(instr_in) is
                            when FUNCT7_ADD => return ALU_ADD;
                            when FUNCT7_SUB => return ALU_SUB;
                            when others => assert 1 = 0 report "Invalid funct7 in get_alu_op add" severity failure;
                        end case;
                    else -- immediate opcode
                        return ALU_ADD;
                    end if;
                when FUNCT3_SRL_SRA =>
                    if get_opcode(instr_in) = OPCODE_OP then
                        case get_funct7(instr_in) is
                            when FUNCT7_SRL => return ALU_SRL;
                            when FUNCT7_SRA => return ALU_SRA;
                            when others => assert 1 = 0 report "Invalid funct7 in get_alu_op shift" severity failure;
                        end case;
                    else -- immediate opcode
                        case get_immediate(instr_in)(10) is
                            when '0' => return ALU_SRL;
                            when '1' => return ALU_SRA;
                            when others => assert 1 = 0 report "Invalid funct7 in get_alu_op shift_imm" severity failure;
                        end case;
                    end if;
                when FUNCT3_SLL => return ALU_SLL;
                when FUNCT3_SLT => return ALU_SLT;
                when FUNCT3_SLTU => return ALU_SLTU;
                when FUNCT3_XOR => return ALU_XOR;
                when FUNCT3_OR => return ALU_OR;
                when FUNCT3_AND => return ALU_AND;
                when others => return ALU_NOP;
            end case;
            return ALU_NOP;
        end function;
    begin
        exc_dec <= '0';
        d2e.pc <= pc;

        -- dont write to rs1_data and rs2_data
        d2e.exec_op.alu_op <= ALU_NOP;
        d2e.exec_op.alu_a_src <= SEL_PC;
        d2e.exec_op.alu_b_src <= SEL_IMM;
        d2e.exec_op.brt_src <= SEL_PC_PLUS_IMM;
        d2e.exec_op.rs1 <= (others => '0');
        d2e.exec_op.rs2 <= (others => '0');
        d2e.exec_op.imm <= (others => '0');

        d2e.mem_op <= MEM_NOP;
        d2e.wb_op <= WB_NOP;

        -- pragma translate_off
        if unsigned(pc) /= 0 then
            report to_inst_string(instr) & gen_dbg_string(pc, instr);
        end if;
        -- pragma translate_on
        case get_opcode(instr) is
            when OPCODE_LOAD =>
                brt_src <= SEL_RS1_DATA_PLUS_IMM;
                mem_op.memu_op.access_type <= get_memu_access_type(instr);
                mem_op.memu_op.rd <= '1';
                wb_op.write <= '1';
                wb_op.src <= SEL_MEM_RESULT;

            when OPCODE_STORE =>
                mem_op.memu_op.access_type <= get_memu_access_type(instr);
                mem_op.memu_op.wr <= '1';
                brt_src <= SEL_RS1_DATA_PLUS_IMM;

            when OPCODE_BRANCH =>
                case get_funct3(instr) is
                    when FUNCT3_BEQ =>
                        aluop <= ALU_SUB; -- sub to compare equality
                        mem_op.branch_op <= BR_BRANCH_IF_Z;
                    when FUNCT3_BNE =>
                        aluop <= ALU_SUB; -- sub to compare equality
                        mem_op.branch_op <= BR_BRANCH_IF_NOT_Z;
                    when FUNCT3_BLT =>
                        aluop <= ALU_SLT;
                        mem_op.branch_op <= BR_BRANCH_IF_NOT_Z;
                    when FUNCT3_BGE =>
                        aluop <= ALU_SLT;
                        mem_op.branch_op <= BR_BRANCH_IF_Z;
                    when FUNCT3_BLTU =>
                        aluop <= ALU_SLTU;
                        mem_op.branch_op <= BR_BRANCH_IF_NOT_Z;
                    when FUNCT3_BGEU =>
                        aluop <= ALU_SLTU;
                        mem_op.branch_op <= BR_BRANCH_IF_Z;

                    when others => exc_dec <= '1';
                end case;

                alu_a <= SEL_RS1_DATA;
                alu_b <= SEL_RS2_DATA;
                brt_src <= SEL_PC_PLUS_IMM;

            when OPCODE_JALR =>
                brt_src <= SEL_RS1_DATA_PLUS_IMM;
                mem_op.branch_op <= BR_BRANCH;
                aluop <= ALU_ADD;
                alu_a <= SEL_PC;
                alu_b <= SEL_4;

                wb_op.write <= '1';
                wb_op.src <= SEL_ALU_RESULT;

            when OPCODE_JAL =>
                aluop <= ALU_ADD;
                brt_src <= SEL_PC_PLUS_IMM;
                mem_op.branch_op <= BR_BRANCH;
                alu_a <= SEL_PC;
                alu_b <= SEL_4;

                wb_op.write <= '1';
                wb_op.src <= SEL_ALU_RESULT;

            when OPCODE_OP_IMM =>
                aluop <= get_alu_op(instr);
                alu_a <= SEL_RS1_DATA;
                alu_b <= SEL_IMM;

                wb_op.write <= '1';
                wb_op.src <= SEL_ALU_RESULT;

            when OPCODE_OP =>
                aluop <= get_alu_op(instr);
                alu_a <= SEL_RS1_DATA;
                alu_b <= SEL_RS2_DATA;

                wb_op.write <= '1';
                wb_op.src <= SEL_ALU_RESULT;

            when OPCODE_AUIPC =>
                aluop <= ALU_ADD;
                alu_a <= SEL_PC;
                alu_b <= SEL_IMM;

                wb_op.write <= '1';
                wb_op.src <= SEL_ALU_RESULT;

            when OPCODE_LUI =>
                aluop <= ALU_NOP;
                alu_b <= SEL_IMM;

                wb_op.write <= '1';
                wb_op.src <= SEL_ALU_RESULT;

            when OPCODE_FENCE =>
                aluop <= ALU_NOP;

            when others => exc_dec <= '1';
        end case;

        d2e.exec_op.imm <= get_immediate(instr);
        wb_op.rd <= get_rd(instr);

        case get_inst_type(instr) is
            when FORMAT_R | FORMAT_S | FORMAT_B =>
                d2e.exec_op.rs1 <= get_rs1(instr);
                d2e.exec_op.rs2 <= get_rs2(instr);
            when FORMAT_I =>
                d2e.exec_op.rs1 <= get_rs1(instr);
                d2e.exec_op.rs2 <= ZERO_REG;
            when FORMAT_U | FORMAT_J =>
                d2e.exec_op.rs1 <= ZERO_REG;
                d2e.exec_op.rs2 <= ZERO_REG;
            when others => exc_dec <= '1';
        end case;
    end process;

    registers_proc: process (clk, res_n) is
    begin
        if res_n /= '1' then
            -- reset
            pc <= (others => '0');
            instr <= (others => '0');
        elsif rising_edge(clk) then
            if ctrl.flush then
                pc <= (others => '0');
                instr <= NOP_INSTR;
            elsif ctrl.stall then
                pc <= pc;
                instr <= instr;
            else
                pc <= f2d.pc;
                instr <= f2d.instr;
            end if;
        end if;
    end process;

    rv_pl_regfile_inst: entity work.rv_pl_regfile
        port map (
            clk      => clk,
            res_n    => res_n,
            stall    => ctrl.stall,
            rd_addr1 => get_rs1(f2d.instr),
            rd_data1 => d2e.exec_op.rs1_data,
            rd_addr2 => get_rs2(f2d.instr),
            rd_data2 => d2e.exec_op.rs2_data,
            wr_addr  => w2d.reg,
            wr_data  => w2d.data,
            wr       => w2d.write
        );

end architecture;
