library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.rv_core_pkg.all;
    use work.rv_sys_pkg.all;
    use work.rv_alu_pkg.all;
    use work.rv_pl_op_pkg.all;

entity rv_pl_exec is
    port (
        clk   : in  std_logic;
        res_n : in  std_logic;

        ctrl  : in  ctrl_t;  -- from ctrl unit
        d2e   : in  d2e_t;   -- from decode stage
        e2m   : out e2m_t;   -- to memory stage
        m2e   : in  regwr_t; -- form memory stage (forwarding)
        w2e   : in  regwr_t  -- form writeback stage (forwarding)
    );
end entity;

architecture rtl of rv_pl_exec is
    signal pc      : data_t    := (others => '0');
    signal exec_op : exec_op_t := EXEC_NOP;
    signal mem_op  : mem_op_t  := MEM_NOP;
    signal wb_op   : wb_op_t   := WB_NOP;

    signal rs1_data : data_t;
    signal rs2_data : data_t;

    -- ALU signals
    signal A, B : data_t := (others => '0');

    -- Adder signals
    signal branch_part_1 : data_t;
begin
    with exec_op.alu_a_src select A <=
        pc                      when SEL_PC,
        rs1_data                when SEL_RS1_DATA,
                (others => '0') when others;

    with exec_op.alu_b_src select B <=
        exec_op.imm             when SEL_IMM,
        rs2_data                when SEL_RS2_DATA,
        32x"4"                  when SEL_4,
                (others => '1') when others;

    with exec_op.brt_src select branch_part_1 <=
        pc                      when SEL_PC_PLUS_IMM,
        exec_op.rs1_data        when SEL_RS1_DATA_PLUS_IMM,
                (others => '0') when others;

    -- forwarding multiplexers
    rs1_data <= m2e.data when m2e.reg = exec_op.rs1 else w2e.data when w2e.reg = exec_op.rs1 else exec_op.rs1_data;
    rs2_data <= m2e.data when m2e.reg = exec_op.rs2 else w2e.data when w2e.reg = exec_op.rs2 else exec_op.rs2_data;

    e2m.pc            <= pc;
    e2m.branch_target <= std_logic_vector(unsigned(branch_part_1) + unsigned(exec_op.imm));
    e2m.mem_data      <= exec_op.rs2_data;
    e2m.mem_op        <= mem_op;
    e2m.wb_op         <= wb_op;

    registers_proc: process (clk, res_n) is
    begin
        if res_n /= '1' then
            -- reset
            pc <= (others => '0');
            exec_op <= EXEC_NOP;
            mem_op <= MEM_NOP;
            wb_op <= WB_NOP;

        elsif rising_edge(clk) then
            if ctrl.flush then
                exec_op <= EXEC_NOP;
                mem_op <= MEM_NOP;
                wb_op <= WB_NOP;
            elsif ctrl.stall then
                pc <= pc;
                exec_op <= exec_op;
                mem_op <= mem_op;
                wb_op <= wb_op;
            else
                pc <= d2e.pc;
                exec_op <= d2e.exec_op;
                mem_op <= d2e.mem_op;
                wb_op <= d2e.wb_op;
            end if;
        end if;
    end process;

    rv_alu_inst: entity work.rv_alu
        port map (
            op => exec_op.alu_op,
            A  => A,
            B  => B,
            R  => e2m.alu_result,
            Z  => e2m.alu_zero
        );
end architecture;
