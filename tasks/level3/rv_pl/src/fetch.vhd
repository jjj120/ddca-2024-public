library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.rv_core_pkg.all;
    use work.rv_sys_pkg.all;
    use work.rv_pl_op_pkg.all;
    use work.rv_util_pkg.all;

entity rv_pl_fetch is
    port (
        clk      : in  std_logic;
        res_n    : in  std_logic;

        ctrl     : in  ctrl_t;    -- from control unit
        m2f      : in  m2f_t;     -- from memory stage
        f2d      : out f2d_t;     -- to decode stage

        mem_out  : out mem_out_t; -- to rv_sys
        mem_in   : in  mem_in_t;  -- from rv_sys

        mem_busy : out std_logic
    );
end entity;

architecture rtl of rv_pl_fetch is
    signal pc, next_pc : data_t;
    signal flush       : std_logic := '0';
    signal pc_plus_4   : data_t    := (others => '0');
begin
    pc_plus_4 <= std_logic_vector(unsigned(pc) + 4);
    next_pc   <= m2f.branch_target when m2f.branch = '1' else pc_plus_4;

    mem_out.address <= byte_to_word_addr(next_pc);
    mem_out.rd      <= not(ctrl.stall or (not res_n));
    mem_out.wr      <= '0';
    mem_out.byteena <= "1111";
    mem_out.wrdata  <= (others => '0');

    mem_busy <= mem_in.busy;

    f2d.pc    <= pc;
    f2d.instr <= NOP_INSTR when flush else flip_endianness(mem_in.rddata);

    registers_proc: process (clk, res_n) is
    begin
        if res_n /= '1' then
            -- reset
            pc <= std_logic_vector(to_signed(- 4, DATA_WIDTH));
            flush <= '0';
        elsif rising_edge(clk) then
            flush <= ctrl.flush;

            if ctrl.stall then
                pc <= pc;
            else
                pc <= next_pc;
            end if;
        end if;
    end process;
end architecture;
