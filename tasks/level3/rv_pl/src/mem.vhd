library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.rv_core_pkg.all;
    use work.rv_sys_pkg.all;
    use work.rv_pl_op_pkg.all;

entity rv_pl_mem is
    port (
        clk       : in  std_logic;
        res_n     : in  std_logic;

        ctrl      : in  ctrl_t;                   -- from control unit
        e2m       : in  e2m_t;                    -- from execute stage
        m2f       : out m2f_t;                    -- to fetch stage
        m2w       : out m2w_t;                    -- to writeback stage
        m2e       : out regwr_t;                  -- to execute stage (forwarding)

        mem_out   : out mem_out_t := MEM_OUT_NOP; -- to rv_sys 
        mem_in    : in  mem_in_t  := MEM_IN_NOP;  -- from rv_sys

        mem_busy  : out std_logic := '0';         -- to control unit

        exc_load  : out std_logic := '0';         -- load exception
        exc_store : out std_logic := '0'          -- store exception
    );
end entity;

architecture rtl of rv_pl_mem is
    signal pc            : data_t    := (others => '0');
    signal branch_target : data_t    := (others => '0');
    signal alu_result    : data_t    := (others => '0');
    signal alu_zero      : std_logic := '0';
    signal mem_data      : data_t    := (others => '0');
    signal mem_op        : mem_op_t  := MEM_NOP;
    signal wb_op         : wb_op_t   := WB_NOP;

    signal memu_op_filtered : memu_op_t;

    signal mem_result : data_t := (others => '0');

    constant REGISTER_ZERO : reg_address_t := (others => '0');
begin
    m2f.branch_target <= branch_target;
    m2f.branch        <= alu_zero     when mem_op.branch_op = BR_BRANCH_IF_Z else
                         not alu_zero when mem_op.branch_op = BR_BRANCH_IF_NOT_Z else
                         '1'          when mem_op.branch_op = BR_BRANCH else
                         '0';

    m2w.alu_result <= alu_result;
    m2w.pc         <= pc;
    m2w.wb_op      <= wb_op;
    m2w.mem_result <= mem_result;

    m2e.write <= wb_op.write;
    m2e.data  <= (others => '0') when wb_op.rd = REGISTER_ZERO else -- filter out data for x0
                alu_result       when wb_op.src = SEL_ALU_RESULT else
                    (others => '0');

    registers_proc: process (clk, res_n) is
    begin
        if res_n /= '1' then
            -- reset
            pc <= (others => '0');
            branch_target <= (others => '0');
            alu_result <= (others => '0');
            alu_zero <= '0';
            mem_data <= (others => '0');
            mem_op <= MEM_NOP;
            wb_op <= WB_NOP;
            memu_op_filtered <= MEMU_NOP;

        elsif rising_edge(clk) then
            if ctrl.flush then
                pc <= (others => '0');
                branch_target <= (others => '0');
                alu_result <= (others => '0');
                alu_zero <= '0';
                mem_data <= (others => '0');
                mem_op <= MEM_NOP;
                wb_op <= WB_NOP;
                memu_op_filtered <= MEMU_NOP;
            elsif ctrl.stall then
                pc <= pc;
                branch_target <= branch_target;
                alu_result <= alu_result;
                alu_zero <= alu_zero;
                mem_data <= mem_data;
                mem_op <= mem_op;
                wb_op <= wb_op;

                memu_op_filtered.rd <= '0';
                memu_op_filtered.wr <= '0';
                memu_op_filtered.access_type <= memu_op_filtered.access_type;
            else
                pc <= e2m.pc;
                branch_target <= e2m.branch_target;
                alu_result <= e2m.alu_result;
                alu_zero <= e2m.alu_zero;
                mem_data <= e2m.mem_data;
                mem_op <= e2m.mem_op;
                wb_op <= e2m.wb_op;
                memu_op_filtered <= e2m.mem_op.memu_op;
            end if;
        end if;
    end process;

    memu_inst: memu
        port map (
            op      => memu_op_filtered,
            addr    => branch_target,
            wrdata  => mem_data,
            rddata  => mem_result,
            busy    => mem_busy,
            xl      => exc_load,
            xs      => exc_store,
            mem_in  => mem_in,
            mem_out => mem_out
        );
end architecture;
