library ieee;
    use ieee.std_logic_1164.all;

    use work.rv_core_pkg.all;
    use work.rv_sys_pkg.all;
    use work.rv_pl_op_pkg.all;

entity rv_pl is
    port (
        clk      : in  std_logic;
        res_n    : in  std_logic;

        -- instruction interface
        imem_out : out mem_out_t;
        imem_in  : in  mem_in_t;

        -- data interface
        dmem_out : out mem_out_t;
        dmem_in  : in  mem_in_t
    );
end entity;

architecture arch of rv_pl is
    signal imem_busy, dmem_busy         : std_logic;
    signal exc_dec, exc_load, exc_store : std_logic;

    signal f2d  : f2d_t;
    signal d2e  : d2e_t;
    signal e2m  : e2m_t;
    signal m2f  : m2f_t;
    signal m2w  : m2w_t;
    signal w2de : regwr_t; -- normal writeback and forwarding
    signal m2e  : regwr_t; -- forwarding

    -- control unit
    signal ctrl_fe   : ctrl_t;
    signal ctrl_dec  : ctrl_t;
    signal ctrl_exec : ctrl_t;
    signal ctrl_mem  : ctrl_t;
    signal ctrl_wb   : ctrl_t;
begin
    rv_pl_ctrl_inst: entity work.rv_pl_ctrl(simple) port map (
        clk       => clk,
        res_n     => res_n,
        fe        => ctrl_fe,
        dec       => ctrl_dec,
        exec      => ctrl_exec,
        mem       => ctrl_mem,
        wb        => ctrl_wb,
        d2e       => d2e,
        e2m       => e2m,
        m2f       => m2f,
        imem_busy => imem_busy,
        dmem_busy => dmem_busy
    );

    rv_pl_fetch_inst: entity work.rv_pl_fetch
        port map (
            clk      => clk,
            res_n    => res_n,
            ctrl     => ctrl_fe,
            m2f      => m2f,
            f2d      => f2d,
            mem_out  => imem_out,
            mem_in   => imem_in,
            mem_busy => imem_busy
        );

    rv_pl_decode_inst: entity work.rv_pl_decode
        port map (
            clk     => clk,
            res_n   => res_n,
            ctrl    => ctrl_dec,
            f2d     => f2d,
            w2d     => w2de,
            d2e     => d2e,
            exc_dec => exc_dec
        );

    rv_pl_exec_inst: entity work.rv_pl_exec
        port map (
            clk   => clk,
            res_n => res_n,
            ctrl  => ctrl_exec,
            d2e   => d2e,
            e2m   => e2m,
            m2e   => m2e,
            w2e   => w2de
        );

    rv_pl_mem_inst: entity work.rv_pl_mem
        port map (
            clk       => clk,
            res_n     => res_n,
            ctrl      => ctrl_mem,
            e2m       => e2m,
            m2f       => m2f,
            m2w       => m2w,
            m2e       => m2e,
            mem_out   => dmem_out,
            mem_in    => dmem_in,
            mem_busy  => dmem_busy,
            exc_load  => exc_load,
            exc_store => exc_store
        );

    rv_pl_wb_inst: entity work.rv_pl_wb
        port map (
            clk   => clk,
            res_n => res_n,
            ctrl  => ctrl_wb,
            m2w   => m2w,
            w2de  => w2de
        );
end architecture;
