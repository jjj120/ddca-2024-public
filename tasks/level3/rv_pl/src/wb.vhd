library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.rv_core_pkg.all;
    use work.rv_sys_pkg.all;
    use work.rv_pl_op_pkg.all;

entity rv_pl_wb is
    port (
        clk   : in  std_logic;
        res_n : in  std_logic;

        ctrl  : in  ctrl_t; -- from control unit
        m2w   : in  m2w_t;  -- from memory stage
        w2de  : out regwr_t -- to decode stage and execute stage (fowarding)
    );
end entity;

architecture rtl of rv_pl_wb is
    signal pc         : data_t;
    signal wb_op      : wb_op_t;
    signal alu_result : data_t;
    signal mem_result : data_t;

    constant REGISTER_ZERO : reg_address_t := (others => '0');
begin
    w2de.reg   <= wb_op.rd;
    w2de.write <= wb_op.write;
    w2de.data  <= (others => '0') when wb_op.rd = REGISTER_ZERO else -- filter out data for x0
                 alu_result       when wb_op.src = SEL_ALU_RESULT else
                 mem_result       when wb_op.src = SEL_MEM_RESULT else
                     (others => '0');

    registers_proc: process (clk, res_n) is
    begin
        if res_n /= '1' then
            -- reset
            pc <= (others => '0');
            wb_op <= WB_NOP;
            alu_result <= (others => '0');
            mem_result <= (others => '0');

        elsif rising_edge(clk) then
            if ctrl.flush then
                pc <= (others => '0');
                wb_op <= WB_NOP;
                alu_result <= (others => '0');
                mem_result <= (others => '0');
            elsif ctrl.stall then
                pc <= pc;
                wb_op <= wb_op;
                alu_result <= alu_result;
                mem_result <= mem_result;
            else
                pc <= m2w.pc;
                wb_op <= m2w.wb_op;
                alu_result <= m2w.alu_result;
                mem_result <= m2w.mem_result;
            end if;
        end if;
    end process;
end architecture;
