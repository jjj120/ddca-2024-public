onerror {resume}
quietly virtual signal -install /rv_pl_tb/rv_pl_inst { (context /rv_pl_tb/rv_pl_inst )(imem_out & imem_in & dmem_out & dmem_in & imem_busy & dmem_busy & exc_load & exc_store )} TopIO
quietly WaveActivateNextPane {} 0
add wave -noupdate /rv_pl_tb/rv_pl_inst/clk
add wave -noupdate /rv_pl_tb/rv_pl_inst/res_n
add wave -noupdate -divider {from Fetch}
add wave -noupdate -color Red -expand -subitemconfig {/rv_pl_tb/rv_pl_inst/f2d.pc {-color Red} /rv_pl_tb/rv_pl_inst/f2d.instr {-color Red}} /rv_pl_tb/rv_pl_inst/f2d
add wave -noupdate -color Red /rv_pl_tb/rv_pl_inst/rv_pl_fetch_inst/next_pc
add wave -noupdate -divider {from Decode}
add wave -noupdate -color Green /rv_pl_tb/rv_pl_inst/rv_pl_decode_inst/instr_test_string
add wave -noupdate -color Green -expand -subitemconfig {/rv_pl_tb/rv_pl_inst/d2e.pc {-color Green} /rv_pl_tb/rv_pl_inst/d2e.exec_op {-color Green -expand} /rv_pl_tb/rv_pl_inst/d2e.exec_op.alu_op {-color Green} /rv_pl_tb/rv_pl_inst/d2e.exec_op.alu_a_src {-color Green} /rv_pl_tb/rv_pl_inst/d2e.exec_op.alu_b_src {-color Green} /rv_pl_tb/rv_pl_inst/d2e.exec_op.brt_src {-color Green} /rv_pl_tb/rv_pl_inst/d2e.exec_op.rs1 {-color Green} /rv_pl_tb/rv_pl_inst/d2e.exec_op.rs2 {-color Green} /rv_pl_tb/rv_pl_inst/d2e.exec_op.rs1_data {-color Green} /rv_pl_tb/rv_pl_inst/d2e.exec_op.rs2_data {-color Green} /rv_pl_tb/rv_pl_inst/d2e.exec_op.imm {-color Green} /rv_pl_tb/rv_pl_inst/d2e.mem_op {-color Green} /rv_pl_tb/rv_pl_inst/d2e.wb_op {-color Green}} /rv_pl_tb/rv_pl_inst/d2e
add wave -noupdate -expand -group {Register File} -color Green /rv_pl_tb/rv_pl_inst/rv_pl_decode_inst/rv_pl_regfile_inst/rd_addr1
add wave -noupdate -expand -group {Register File} -color Green /rv_pl_tb/rv_pl_inst/rv_pl_decode_inst/rv_pl_regfile_inst/rd_data1
add wave -noupdate -expand -group {Register File} -color Green /rv_pl_tb/rv_pl_inst/rv_pl_decode_inst/rv_pl_regfile_inst/rd_addr2
add wave -noupdate -expand -group {Register File} -color Green /rv_pl_tb/rv_pl_inst/rv_pl_decode_inst/rv_pl_regfile_inst/rd_data2
add wave -noupdate -expand -group {Register File} -color Green /rv_pl_tb/rv_pl_inst/rv_pl_decode_inst/rv_pl_regfile_inst/wr_addr
add wave -noupdate -expand -group {Register File} -color Green /rv_pl_tb/rv_pl_inst/rv_pl_decode_inst/rv_pl_regfile_inst/wr_data
add wave -noupdate -expand -group {Register File} -color Green /rv_pl_tb/rv_pl_inst/rv_pl_decode_inst/rv_pl_regfile_inst/wr
add wave -noupdate -expand -group {Register File} -color Green /rv_pl_tb/rv_pl_inst/rv_pl_decode_inst/rv_pl_regfile_inst/rf
add wave -noupdate -divider {from Execute}
add wave -noupdate -color Blue -expand -subitemconfig {/rv_pl_tb/rv_pl_inst/e2m.pc {-color Blue} /rv_pl_tb/rv_pl_inst/e2m.branch_target {-color Blue} /rv_pl_tb/rv_pl_inst/e2m.alu_result {-color Blue} /rv_pl_tb/rv_pl_inst/e2m.alu_zero {-color Blue} /rv_pl_tb/rv_pl_inst/e2m.mem_data {-color Blue} /rv_pl_tb/rv_pl_inst/e2m.mem_op {-color Blue} /rv_pl_tb/rv_pl_inst/e2m.wb_op {-color Blue}} /rv_pl_tb/rv_pl_inst/e2m
add wave -noupdate -color Blue /rv_pl_tb/rv_pl_inst/rv_pl_exec_inst/exec_op
add wave -noupdate -color Blue /rv_pl_tb/rv_pl_inst/rv_pl_exec_inst/A
add wave -noupdate -color Blue /rv_pl_tb/rv_pl_inst/rv_pl_exec_inst/B
add wave -noupdate -divider {from Memory}
add wave -noupdate -color {Medium Orchid} -expand -subitemconfig {/rv_pl_tb/rv_pl_inst/m2f.branch_target {-color {Medium Orchid}} /rv_pl_tb/rv_pl_inst/m2f.branch {-color {Medium Orchid}}} /rv_pl_tb/rv_pl_inst/m2f
add wave -noupdate -color {Medium Orchid} -expand -subitemconfig {/rv_pl_tb/rv_pl_inst/m2w.pc {-color {Medium Orchid}} /rv_pl_tb/rv_pl_inst/m2w.wb_op {-color {Medium Orchid}} /rv_pl_tb/rv_pl_inst/m2w.alu_result {-color {Medium Orchid}} /rv_pl_tb/rv_pl_inst/m2w.mem_result {-color {Medium Orchid}}} /rv_pl_tb/rv_pl_inst/m2w
add wave -noupdate -divider {from Writeback}
add wave -noupdate -color Gold -expand -subitemconfig {/rv_pl_tb/rv_pl_inst/w2de.write {-color Gold} /rv_pl_tb/rv_pl_inst/w2de.reg {-color Gold} /rv_pl_tb/rv_pl_inst/w2de.data {-color Gold}} /rv_pl_tb/rv_pl_inst/w2de
add wave -noupdate -divider -height 36 <NULL>
add wave -noupdate -divider TopIO
add wave -noupdate -color Cyan -expand -subitemconfig {/rv_pl_tb/rv_pl_inst/imem_out.address {-color Cyan} /rv_pl_tb/rv_pl_inst/imem_out.rd {-color Cyan} /rv_pl_tb/rv_pl_inst/imem_out.wr {-color Cyan} /rv_pl_tb/rv_pl_inst/imem_out.byteena {-color Cyan} /rv_pl_tb/rv_pl_inst/imem_out.wrdata {-color Cyan}} /rv_pl_tb/rv_pl_inst/imem_out
add wave -noupdate -color Cyan -expand -subitemconfig {/rv_pl_tb/rv_pl_inst/imem_in.busy {-color Cyan} /rv_pl_tb/rv_pl_inst/imem_in.rddata {-color Cyan}} /rv_pl_tb/rv_pl_inst/imem_in
add wave -noupdate -color Cyan -expand -subitemconfig {/rv_pl_tb/rv_pl_inst/dmem_out.address {-color Cyan} /rv_pl_tb/rv_pl_inst/dmem_out.rd {-color Cyan} /rv_pl_tb/rv_pl_inst/dmem_out.wr {-color Cyan} /rv_pl_tb/rv_pl_inst/dmem_out.byteena {-color Cyan} /rv_pl_tb/rv_pl_inst/dmem_out.wrdata {-color Cyan}} /rv_pl_tb/rv_pl_inst/dmem_out
add wave -noupdate -color Cyan -expand -subitemconfig {/rv_pl_tb/rv_pl_inst/dmem_in.busy {-color Cyan} /rv_pl_tb/rv_pl_inst/dmem_in.rddata {-color Cyan}} /rv_pl_tb/rv_pl_inst/dmem_in
add wave -noupdate -color Cyan /rv_pl_tb/rv_pl_inst/imem_busy
add wave -noupdate -color Cyan /rv_pl_tb/rv_pl_inst/dmem_busy
add wave -noupdate -color Cyan /rv_pl_tb/rv_pl_inst/exc_load
add wave -noupdate -color Cyan /rv_pl_tb/rv_pl_inst/exc_store
add wave -noupdate -divider Controlling
add wave -noupdate -color Orange -expand -subitemconfig {/rv_pl_tb/rv_pl_inst/ctrl_fe.stall {-color Orange} /rv_pl_tb/rv_pl_inst/ctrl_fe.flush {-color Orange}} /rv_pl_tb/rv_pl_inst/ctrl_fe
add wave -noupdate -color Orange /rv_pl_tb/rv_pl_inst/ctrl_dec
add wave -noupdate -color Orange /rv_pl_tb/rv_pl_inst/ctrl_exec
add wave -noupdate -color Orange /rv_pl_tb/rv_pl_inst/ctrl_mem
add wave -noupdate -color Orange /rv_pl_tb/rv_pl_inst/ctrl_wb
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {103 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 237
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {1163 ns}
