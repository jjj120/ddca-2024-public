library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.rv_sys_pkg.all;
    use work.rv_core_pkg.all;
    use work.rv_util_pkg.all;

    use work.rv_ext_m_pkg.all;

entity rv_sim is
    generic (
        CLK_FREQ : positive
    );
    port (
        clk      : in  std_logic;
        res_n    : in  std_logic;
        -- Interface to instruction memory
        imem_out : out mem_out_t;
        imem_in  : in  mem_in_t;
        -- Interface to data memory
        dmem_out : out mem_out_t;
        dmem_in  : in  mem_in_t
    );
end entity;

architecture beh of rv_sim is
    constant CLK_PERIOD : time := 1000 ms / CLK_FREQ;

    function get_byteena(funct3 : funct3_t; mem_addr : data_t) return std_logic_vector is
        type byteena_mask_t is array (0 to 3) of std_logic_vector(3 downto 0);
        constant BYTE_EN_MASK : byteena_mask_t := ("1000", "0100", "0010", "0001");
        constant HW_EN_MASK   : byteena_mask_t := ("1100", "1100", "0011", "0011");
    begin
        case funct3(1 downto 0) is
            when "00" => return BYTE_EN_MASK(to_integer(unsigned(mem_addr(1 downto 0)))); -- byte addressing
            when "01" => return HW_EN_MASK(to_integer(unsigned(mem_addr(1 downto 0)))); -- halfword addressing
            when others => return "1111"; -- word addressing
        end case;

    end function;

    function byte_to_word_addr(data : data_t) return mem_address_t is
    begin
        return data(RV_SYS_ADDR_WIDTH + 1 downto 2);
    end function;

    pure function mul_opcode_to_op(funct3_in : funct3_t) return ext_m_op_t is
    begin
        case funct3_in is
            when FUNCT3_MUL => return M_MUL;
            when FUNCT3_MULH => return M_MULH;
            when FUNCT3_MULHSU => return M_MULHSU;
            when FUNCT3_MULHU => return M_MULHU;
            when FUNCT3_DIV => return M_DIV;
            when FUNCT3_DIVU => return M_DIVU;
            when FUNCT3_REM => return M_REM;
            when FUNCT3_REMU => return M_REMU;
            when others => report "Unknown funct3 for mul" severity failure;
        end case;
    end function;

    type registers_t is array (REG_COUNT - 1 downto 0) of data_t;

    type multiplier_t is record
        A, B  : std_logic_vector(DATA_WIDTH - 1 downto 0);
        op    : ext_m_op_t;
        start : std_logic;
    end record;

    constant MULTIPLIER_NOP : multiplier_t := (
        A     => (others => '0'),
        B     => (others => '0'),
        op    => M_MUL,
        start => '0'
    );

    signal multiplier : multiplier_t                              := MULTIPLIER_NOP;
    signal mult_res   : std_logic_vector(DATA_WIDTH - 1 downto 0) := (others => '0');
    signal mult_busy  : std_logic                                 := '0';
begin
    main: process is
        variable registers : registers_t := (others => (others => '0'));
        variable pc        : data_t      := (others => '0');
        variable instr     : instr_t;
        variable opcode    : opcode_t;
        variable funct3    : funct3_t;
        variable funct7    : funct7_t;
        variable immediate : data_t;

        variable rd  : integer;
        variable rs1 : integer;
        variable rs2 : integer;

        -- Drive dmem_out and return rddata in the correct byte ordering and with the correct extending bytes if required
        procedure read_from_dmem(constant funct : funct3_t; constant byte_addr : data_t; variable rddata : out data_t) is
            variable rd_out   : data_t;
            variable lsb_addr : integer := 0;
        begin
            while dmem_in.busy loop
                wait for CLK_PERIOD;
            end loop;

            dmem_out <= (
                address => byte_to_word_addr(byte_addr),
                rd      => '1',
                wr      => '0',
                byteena => get_byteena(funct, byte_addr),
                wrdata  => (others => '0')
            );

            wait for CLK_PERIOD;

            while dmem_in.busy loop
                wait for CLK_PERIOD;
            end loop;

            dmem_out <= MEM_OUT_NOP;

            -- flip endianness
            rd_out := data_t'(dmem_in.rddata(7 downto 0) & dmem_in.rddata(15 downto 8) & dmem_in.rddata(23 downto 16) & dmem_in.rddata(31 downto 24));

            -- shift and sign extend data
            case funct3(1 downto 0) is
                when "00" => -- byte addressing
                    lsb_addr := to_integer(unsigned(byte_addr(1 downto 0)));
                    rd_out := (
                        DATA_WIDTH / 4 - 1 downto 0 => rd_out((lsb_addr + 1) * 8 - 1 downto (lsb_addr) * 8),
                        others                      => ((not funct(WIDTH_FUNCT3 - 1)) and rd_out((lsb_addr + 1) * 8 - 1))
                    );

                when "01" => -- halfword addressing
                    lsb_addr := to_integer(unsigned(byte_addr(1 downto 1) & '0'));
                    rd_out := (
                        DATA_WIDTH / 2 - 1 downto 0 => rd_out((lsb_addr + 2) * 8 - 1 downto (lsb_addr) * 8),
                        others                      => ((not funct(WIDTH_FUNCT3 - 1)) and rd_out((lsb_addr + 2) * 8 - 1))
                    );

                when others => -- word addressing
                    rd_out := rd_out;
            end case;

            rddata := rd_out;
        end procedure;

        -- Write wrdata (processor byte ordering) to dmem_out
        procedure write_to_dmem(constant funct : funct3_t; constant byte_addr : data_t; constant wrdata : data_t) is
            variable wrdata_mod : data_t  := (others => '0');
            variable lsb_addr   : integer := 0;
        begin
            case funct3(1 downto 0) is
                when "00" => -- byte addressing
                    lsb_addr := to_integer(unsigned(byte_addr(1 downto 0)));
                    wrdata_mod := std_logic_vector(shift_left(unsigned(wrdata), lsb_addr * 8));

                when "01" => -- halfword addressing
                    lsb_addr := to_integer(unsigned(byte_addr(1 downto 1) & '0'));
                    wrdata_mod := std_logic_vector(shift_left(unsigned(wrdata), lsb_addr * 8));

                when others => -- word addressing
                    wrdata_mod := wrdata;
            end case;

            while dmem_in.busy loop
                wait for CLK_PERIOD;
            end loop;

            dmem_out <= (
                address => byte_to_word_addr(byte_addr),
                rd      => '0',
                wr      => '1',
                byteena => get_byteena(funct, byte_addr),
                wrdata  => data_t'(wrdata_mod(7 downto 0) & wrdata_mod(15 downto 8) & wrdata_mod(23 downto 16) & wrdata_mod(31 downto 24))
            );

            wait for CLK_PERIOD;

            dmem_out <= MEM_OUT_NOP;
        end procedure;

        -- Read the address in the imem the pc currently points to and return it in rddata
        procedure read_from_imem(variable rddata : out instr_t) is
        begin
            while imem_in.busy loop
                wait for CLK_PERIOD;
            end loop;

            imem_out.rd <= '1';
            imem_out.address <= pc(imem_out.address'length + 1 downto 2);

            wait for CLK_PERIOD;

            while imem_in.busy loop
                wait for CLK_PERIOD;
            end loop;

            imem_out.rd <= '0';
            -- flip endianness
            rddata := instr_t'(imem_in.rddata(7 downto 0) & imem_in.rddata(15 downto 8) & imem_in.rddata(23 downto 16) & imem_in.rddata(31 downto 24));
        end procedure;

        procedure fail(message : string) is
        begin
            assert 1 = 0 report "FAIL: " & message & " -- opcode: " & to_string(opcode) & ", funct3: " & to_string(funct3) & ", funct7: " & to_string(funct7) & ", " & to_string(instr) severity failure;
        end procedure;

        procedure print_opcode(instr : instr_t) is
            variable new_pc : data_t := std_logic_vector(unsigned(pc) - 4);
        begin
            if unsigned(new_pc) = 0 then
                return;
            end if;

            if instr = 32x"13" then
                report to_inst_string(instr) & gen_dbg_string(pc, instr, 3);
                return;
            end if;

            report to_inst_string(instr) & gen_dbg_string(new_pc, instr);

            -- case get_opcode(instr) is
            --     when OPCODE_LOAD => report to_inst_string(instr) & gen_dbg_string(new_pc, instr);
            --     when OPCODE_STORE => report to_inst_string(instr) & gen_dbg_string(new_pc, instr);
            --     when OPCODE_BRANCH => report to_inst_string(instr) & gen_dbg_string(new_pc, instr);
            --     when OPCODE_JALR => report to_inst_string(instr) & gen_dbg_string(new_pc, instr);
            --     when OPCODE_JAL => report to_inst_string(instr) & gen_dbg_string(new_pc, instr);
            --     when OPCODE_OP_IMM => report to_inst_string(instr) & gen_dbg_string(new_pc, instr);
            --     when OPCODE_OP => report to_inst_string(instr) & gen_dbg_string(new_pc, instr);
            --     when OPCODE_AUIPC => report to_inst_string(instr) & gen_dbg_string(new_pc, instr);
            --     when OPCODE_LUI => report to_inst_string(instr) & gen_dbg_string(new_pc, instr);
            --     when OPCODE_FENCE => report to_inst_string(instr) & gen_dbg_string(new_pc, instr);
            --     when others => fail("Unknown opcode");
            -- end case;
        end procedure;

        procedure get_next_instruction is
        begin
            read_from_imem(instr);
            registers(0) := (others => '0');
            pc := std_logic_vector(unsigned(pc) + 4);
            opcode := get_opcode(instr);
            funct3 := get_funct3(instr);
            funct7 := get_funct7(instr);
            immediate := get_immediate(instr);
            rd := to_integer(unsigned(get_rd(instr)));
            rs1 := to_integer(unsigned(get_rs1(instr)));
            rs2 := to_integer(unsigned(get_rs2(instr)));
            print_opcode(instr);
        end procedure;
    begin
        dmem_out <= MEM_OUT_NOP;
        imem_out <= MEM_OUT_NOP;
        wait until res_n;
        wait until rising_edge(clk);
        wait for 1 ns;

        loop
            if not res_n then
                pc := (others => '0');
                registers := (others => (others => '0'));
                wait for CLK_PERIOD;
                next;
            end if;

            dmem_out <= MEM_OUT_NOP;
            get_next_instruction;
            case opcode is
                when OPCODE_LOAD =>
                    read_from_dmem(funct3, std_logic_vector(signed(registers(rs1)) + signed(immediate)), registers(rd));

                when OPCODE_STORE =>
                    write_to_dmem(funct3, std_logic_vector(unsigned(registers(rs1)) + unsigned(immediate)), registers(rs2));

                when OPCODE_BRANCH =>
                    case funct3 is
                        when FUNCT3_BEQ =>
                            if registers(rs1) = registers(rs2) then
                                pc := std_logic_vector(unsigned(pc) - 4 + unsigned(immediate));
                            end if;

                        when FUNCT3_BNE =>
                            -- report "UART_TEST check: " & to_hstring(registers(rs1)) & " /= " & to_hstring(registers(rs2));
                            if registers(rs1) /= registers(rs2) then
                                -- report "UART_TEST 1: " & to_hstring(registers(rs1)) & " /= " & to_hstring(registers(rs2));
                                pc := std_logic_vector(unsigned(pc) - 4 + unsigned(immediate));
                            end if;

                        when FUNCT3_BLT =>
                            if signed(registers(rs1)) < signed(registers(rs2)) then
                                pc := std_logic_vector(unsigned(pc) - 4 + unsigned(immediate));
                            end if;

                        when FUNCT3_BGE =>
                            if signed(registers(rs1)) >= signed(registers(rs2)) then
                                pc := std_logic_vector(unsigned(pc) - 4 + unsigned(immediate));
                            end if;

                        when FUNCT3_BLTU =>
                            if unsigned(registers(rs1)) < unsigned(registers(rs2)) then
                                pc := std_logic_vector(unsigned(pc) - 4 + unsigned(immediate));
                            end if;

                        when FUNCT3_BGEU =>
                            if unsigned(registers(rs1)) >= unsigned(registers(rs2)) then
                                pc := std_logic_vector(unsigned(pc) - 4 + unsigned(immediate));
                            end if;

                        when others => fail("Got unknown funct3 for branch.");
                    end case;

                when OPCODE_JALR =>
                    assert funct3 = "000" report "funct3 is not 000 for jalr" severity failure;
                    registers(rd) := pc;
                    pc := std_logic_vector(signed(immediate) + signed(registers(rs1)));
                    pc(0) := '0';

                when OPCODE_JAL =>
                    registers(rd) := pc;
                    pc := std_logic_vector(unsigned(pc) - 4 + unsigned(immediate));

                when OPCODE_OP_IMM =>
                    case funct3 is
                        when FUNCT3_SLTI => registers(rd) := (0 => (signed(registers(rs1)) ?< signed(immediate)), others => '0');
                        when FUNCT3_SLTIU => registers(rd) := (0 => (unsigned(registers(rs1)) ?< unsigned(immediate)), others => '0');
                        when FUNCT3_ADDI => registers(rd) := std_logic_vector(signed(registers(rs1)) + signed(immediate));
                        when FUNCT3_XORI => registers(rd) := std_logic_vector(signed(registers(rs1)) xor signed(immediate));
                        when FUNCT3_ORI => registers(rd) := std_logic_vector(signed(registers(rs1)) or signed(immediate));
                        when FUNCT3_ANDI => registers(rd) := std_logic_vector(signed(registers(rs1)) and signed(immediate));
                        when FUNCT3_SLLI => registers(rd) := std_logic_vector(shift_left(signed(registers(rs1)), to_integer(unsigned(immediate(4 downto 0)))));

                        when FUNCT3_SRLI_SRAI =>
                            if not immediate(10) then
                                registers(rd) := std_logic_vector(shift_right(unsigned(registers(rs1)), to_integer(unsigned(immediate(4 downto 0)))));
                            else
                                registers(rd) := std_logic_vector(shift_right(signed(registers(rs1)), to_integer(unsigned(immediate(4 downto 0)))));
                            end if;

                        when others => fail("Got unknown funct3 for op.");
                    end case;

                when OPCODE_OP =>
                    if not (funct7 = "0000000" or funct7 = FUNCT7_EXT_M or (funct3 = FUNCT3_ADD_SUB and funct7 = FUNCT7_SUB) or (funct3 = FUNCT3_SRL_SRA and funct7 = FUNCT7_SRA)) then
                        fail("Got unknown funct7 for op.");
                    end if;

                    if funct7 = FUNCT7_EXT_M then
                        -- Multiply extension
                        multiplier.op <= mul_opcode_to_op(funct3);
                        multiplier.A <= registers(rs1);
                        multiplier.B <= registers(rs2);
                        multiplier.start <= '1';
                        wait for CLK_PERIOD;
                        multiplier.start <= '0';
                        while mult_busy loop
                            wait for CLK_PERIOD;
                        end loop;
                        registers(rd) := mult_res;
                    else
                        case funct3 is
                            when FUNCT3_SLT =>
                                if signed(registers(rs1)) < signed(registers(rs2)) then
                                    registers(rd) := std_logic_vector(to_unsigned(1, DATA_WIDTH));
                                else
                                    registers(rd) := std_logic_vector(to_unsigned(0, DATA_WIDTH));
                                end if;

                            when FUNCT3_SLTU =>
                                if unsigned(registers(rs1)) < unsigned(registers(rs2)) then
                                    registers(rd) := std_logic_vector(to_unsigned(1, DATA_WIDTH));
                                else
                                    registers(rd) := std_logic_vector(to_unsigned(0, DATA_WIDTH));
                                end if;

                            when FUNCT3_ADD_SUB =>
                                if funct7 = FUNCT7_ADD then
                                    registers(rd) := std_logic_vector(signed(registers(rs1)) + signed(registers(rs2)));
                                elsif funct7 = FUNCT7_SUB then
                                    registers(rd) := std_logic_vector(signed(registers(rs1)) - signed(registers(rs2)));
                                end if;

                            when FUNCT3_SLL => registers(rd) := std_logic_vector(shift_left(unsigned(registers(rs1)), to_integer(unsigned(registers(rs2)(4 downto 0)))));
                            when FUNCT3_SRL_SRA =>
                                if funct7 = FUNCT7_SRL then
                                    registers(rd) := std_logic_vector(shift_right(unsigned(registers(rs1)), to_integer(unsigned(registers(rs2)(4 downto 0)))));
                                elsif funct7 = FUNCT7_SRA then
                                    registers(rd) := std_logic_vector(shift_right(signed(registers(rs1)), to_integer(unsigned(registers(rs2)(4 downto 0)))));
                                end if;

                            when FUNCT3_XOR => registers(rd) := std_logic_vector(signed(registers(rs1)) xor signed(registers(rs2)));
                            when FUNCT3_OR => registers(rd) := std_logic_vector(signed(registers(rs1)) or signed(registers(rs2)));
                            when FUNCT3_AND => registers(rd) := std_logic_vector(signed(registers(rs1)) and signed(registers(rs2)));
                            when others => fail("Got unknown funct3 for op.");
                        end case;
                    end if;

                when OPCODE_AUIPC =>
                    registers(rd) := std_logic_vector(unsigned(pc) - 4 + unsigned(immediate));

                when OPCODE_LUI =>
                    registers(rd) := immediate;

                when OPCODE_FENCE =>
                    -- do nothing
                    null;

                when others => fail("Got unknown opcode.");
            end case;
        end loop;
        wait;
    end process;

    rv_ext_m_inst: entity work.rv_ext_m
        generic map (
            DATA_WIDTH => DATA_WIDTH
        )
        port map (
            clk   => clk,
            res_n => res_n,
            A     => multiplier.A,
            B     => multiplier.B,
            R     => mult_res,
            op    => multiplier.op,
            start => multiplier.start,
            busy  => mult_busy
        );

end architecture;
