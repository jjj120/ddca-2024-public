
# RISC-V Simulation
**Points:** 2 ` | ` **Keywords:** Simulation, RISC-V

By now you have already experienced how useful simulations are when designing (and especially debugging) hardware.
The goal of this task is to create a simulation model (`rv_sim`) of the RV32I RISC-V ISA, you might implement in actual hardware in other tasks.

**Important**: A brief description of the ISA can be found in [rv_core](../../../lib/rv_core/doc.md).

## Task
Implement the `rv_sim` module (a template is provided in [rv_sim.vhd](./src/rv_sim.vhd)).
Note that this module is purely for simulation.
Hence, you are not restricted to writing synthesizable code.

The module has the following interface:

```vhdl
entity rv_sim is
	generic (
		CLK_FREQ : positive
	);
	port (
		clk      : in std_logic;
		res_n    : in std_logic;
		-- Interface to instruction memory
		imem_out : out mem_out_t;
		imem_in  : in mem_in_t;
		-- Interface to data memory
		dmem_out : out mem_out_t;
		dmem_in  : in mem_in_t
	);
end entity;
```

The signals `clk` and `res_n` are the clock and the active-low reset signal.
The `[i|d]mem_out` outputs of type `mem_out_t` are used to interface with the **i**nstruction and **d**ata memory, respectively.
The `[i|d]mem_in` inputs of type `mem_in_t` forward signals from the respective memory to the processor.

You can find the `mem_out_t` and `mem_in_t` types in [rv_sys_pkg](../../../lib/rv_sys/src/rv_sys_pkg.vhd) and a description of their members in the corresponding [documentation](../../../lib/rv_sys/doc.md).

Your processor shall read instructions from the instruction memory (starting at address 0) and execute them immediately.
Unless, you have to access the data memory this shall happen in the same clock cycle.

Don't use any sub modules for this task.
Implement everything inside the `rv_sim` module using procedures and functions.

**Hints**:

- As writing to the instruction memory is not required, some signals of the interface are not used (select appropriate default values).
- Be careful that the individual bytes after reading from / writing to a memory are in correct order (the processor uses a little-endian byte ordering).
- As this is just a simulation model you can decide freely how to implement the given ISA without adhering to a specific RISC-V architecture you might already know (e.g., a pipeline or FSM implementation).
- Try to keep the amount of redundant code to a minimum by creating and reusing suitable functions and procedures.
The provided template already contains the skeletons of some suggested functions and procedures.
- The [rv_core_pkg](../../../lib/rv_core/src/rv_core_pkg.vhd) contains useful constants, types and functions for the decoding of instructions.

## Testbench
Create a testbench as explained in the [rv_sys](../../../lib/rv_sys/doc.md) documentation.


In order to test your processor, run programs on it and check if they work as expected (you can observe the register contents, memory interfaces and - if available - UART output).
Start with verifying basic functionality such as arithmetic, memory and jump instructions and then continue with more elaborate programs.

We recommend also putting your own programs in the [`software`](../../../lib/rv_sys/software) folder of the `rv_sys` lib core, as you might use them for other tasks as well.

**Remark**: The assembly code files are written to also work on a RISC-V pipeline that does not deal with hazards and hence contains `NOP` instructions after jumps.
Since hazards are of no concern for your simulation model (which executes instructions immediately), such `NOP` instructions should not be read and executed by your processor.
