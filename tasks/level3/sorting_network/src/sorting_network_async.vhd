library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.sorting_network_pkg.all;

entity sorting_network_async is
    port (
        clk            : in  std_logic;
        res_n          : in  std_logic;

        -- UART data streamer interface
        unsorted_ready : out std_logic;
        unsorted_data  : in  word_array(0 to 9);
        unsorted_valid : in  std_logic;

        sorted_ready   : in  std_logic;
        sorted_data    : out word_array(0 to 9);
        sorted_valid   : out std_logic
    );
end entity;

architecture arch_async of sorting_network_async is

    type registers_t is array (natural range <>) of word_array(0 to 9);

    constant DEFAULT_REGISTERS : registers_t(1 to 8) := (others => (others => (others => '0')));

    signal registers : registers_t(1 to 8) := DEFAULT_REGISTERS;
begin
    /*
	Sorting Network:
		1: [(0,8),(1,9),(2,7),(3,5),(4,6)]
		2: [(0,2),(1,4),(5,8),(7,9)]
		3: [(0,3),(2,4),(5,7),(6,9)]
		4: [(0,1),(3,6),(8,9)]
		5: [(1,5),(2,3),(4,8),(6,7)]
		6: [(1,2),(3,5),(4,6),(7,8)]
		7: [(2,3),(4,5),(6,7)]
		8: [(3,4),(5,6)]
*/
    registers_proc: process (all) is
        procedure sort_two(
                signal in_arr           :     word_array(0 to 9);
                signal out_arr          : out word_array(0 to 9);
                       in1_idx, in2_idx :     integer
            ) is
        begin
            if unsigned(in_arr(in1_idx)) < unsigned(in_arr(in2_idx)) then
                out_arr(in1_idx) <= in_arr(in1_idx);
                out_arr(in2_idx) <= in_arr(in2_idx);
            else
                out_arr(in1_idx) <= in_arr(in2_idx);
                out_arr(in2_idx) <= in_arr(in1_idx);
            end if;
        end procedure;

        procedure sort is
        begin
            -- default connections
            registers(1) <= unsorted_data;
            registers(2) <= registers(1);
            registers(3) <= registers(2);
            registers(4) <= registers(3);
            registers(5) <= registers(4);
            registers(6) <= registers(5);
            registers(7) <= registers(6);
            registers(8) <= registers(7);

            -- stage 0 (input) to stage 1
            -- [(0,8),(1,9),(2,7),(3,5),(4,6)]
            sort_two(unsorted_data, registers(1), 0, 8);
            sort_two(unsorted_data, registers(1), 1, 9);
            sort_two(unsorted_data, registers(1), 2, 7);
            sort_two(unsorted_data, registers(1), 3, 5);
            sort_two(unsorted_data, registers(1), 4, 6);

            -- stage 1 to 2
            -- [(0,2),(1,4),(5,8),(7,9)]
            sort_two(registers(1), registers(2), 0, 2);
            sort_two(registers(1), registers(2), 1, 4);
            sort_two(registers(1), registers(2), 5, 8);
            sort_two(registers(1), registers(2), 7, 9);

            -- stage 2 to 3
            -- [(0,3),(2,4),(5,7),(6,9)]
            sort_two(registers(2), registers(3), 0, 3);
            sort_two(registers(2), registers(3), 2, 4);
            sort_two(registers(2), registers(3), 5, 7);
            sort_two(registers(2), registers(3), 6, 9);

            -- stage 3 to 4
            -- [(0,1),(3,6),(8,9)]
            sort_two(registers(3), registers(4), 0, 1);
            sort_two(registers(3), registers(4), 3, 6);
            sort_two(registers(3), registers(4), 8, 9);

            -- stage 4 to 5
            -- [(1,5),(2,3),(4,8),(6,7)]
            sort_two(registers(4), registers(5), 1, 5);
            sort_two(registers(4), registers(5), 2, 3);
            sort_two(registers(4), registers(5), 4, 8);
            sort_two(registers(4), registers(5), 6, 7);

            -- stage 5 to 6
            -- [(1,2),(3,5),(4,6),(7,8)]
            sort_two(registers(5), registers(6), 1, 2);
            sort_two(registers(5), registers(6), 3, 5);
            sort_two(registers(5), registers(6), 4, 6);
            sort_two(registers(5), registers(6), 7, 8);

            -- stage 6 to 7
            -- [(2,3),(4,5),(6,7)]
            sort_two(registers(6), registers(7), 2, 3);
            sort_two(registers(6), registers(7), 4, 5);
            sort_two(registers(6), registers(7), 6, 7);

            -- stage 7 to 8
            -- [(3,4),(5,6)]
            sort_two(registers(7), registers(8), 3, 4);
            sort_two(registers(7), registers(8), 5, 6);
        end procedure;
    begin
        sort;
    end process;

    sorted_data    <= registers(registers'high);
    sorted_valid   <= unsorted_valid;
    unsorted_ready <= '1';
end architecture;
