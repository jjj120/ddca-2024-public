library ieee;
use ieee.std_logic_1164.all;

package sorting_network_pkg is
	type word_array is array(natural range <>) of std_logic_vector(31 downto 0);

	component sorting_network is
		port (
			clk      : in std_logic;
			res_n    : in std_logic;
			
			-- UART data streamer interface
			unsorted_ready   : out std_logic;
			unsorted_data    : in word_array(0 to 9);
			unsorted_valid   : in std_logic;
	
			sorted_ready     : in std_logic;
			sorted_data      : out word_array(0 to 9);
			sorted_valid     : out std_logic
		);
	end component;
end package;

