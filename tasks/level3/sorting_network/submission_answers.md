- What is your design's `Fmax`?
104.55 MHz

- What is your design's `Critical-Path`?
The register switch operations

- Why does your sorting network supposedly sort the first inputs (hint: check [`uart_data_streamer`](../../../lib/uart_data_streamer/doc.md))?
- Why does your sorting network fail to successfully sort the second inputs?
Because the first input is there for long enough, but the second one is not, it does not get stable. 
