
# Sorting Network
**Points:** 1 ` | ` **Keywords:** Pipeline

In this task you will design a pipeline to improve the performance of a given sorting network in order to improve its throughput and operation clock frequency.
Start by making yourself familiar with the concepts of [sorting networks](https://en.wikipedia.org/wiki/Sorting_network).

## Task

Your task is to implement a sorting network for 10 inputs using 29 CEs (compare/exchange elements) and 8 layers (`N10L29D8`).
For reference check out [this list](https://bertdobbelaere.github.io/sorting_networks.html) of `Smallest and fastest sorting networks for a given number of inputs`.

The circuit shall be implemented in the `sorting_network` module, which has the following interface:

```vhdl
component sorting_network is
	port (
		clk      : in std_logic;
		res_n    : in std_logic;

		-- UART data streamer interface
		unsorted_ready   : out std_logic;
		unsorted_data    : in word_array(0 to 9);
		unsorted_valid   : in std_logic;

		sorted_ready     : in std_logic;
		sorted_data      : out word_array(0 to 9);
		sorted_valid     : out std_logic
	);
end component;
```

As usual, `clk` and `res_n` are a clock and an active-low reset signal.
The rest of the interface conforms to the interface of the [`uart_data_streamer`](../../../lib/uart_data_streamer/doc.md).
As long as the sorting network asserts `unsorted_ready` it signals that it is ready to consume new data.
A high signal value at `unsorted_valid` indicates that `unsorted_data` holds a valid array of ten 32-bit vectors that shall be sorted.


The `uart_data_streamer` asserts `sorted_ready` whenever it is ready to consume data.
For this task, you may assume that this signal is always one (i.e., the `uart_data_streamer` is always ready to accept data produced by your network).
Your sorting network should assert `sorted_valid` whenever it is done sorting its input.
Whenever `sorted_valid` is asserted, `sorted_data` must hold valid data.

You are already provided with the [top_arch_sorting_network](top_arch_sorting_network.vhd) file, containing a top-level architecture that instantiates your design and the `uart_data_streamer` and connects them to each other.
Additionally, we provide you with a PLL that generates the desired 150 MHz clock frequency.
There is no need to change the given top-level architecture or PLL.

## Starting Point

Begin with a simple non-pipelined (combinational) design of a [`N10L29D8`](https://bertdobbelaere.github.io/sorting_networks.html#N10L29D8) sorting network.
Implement this design in the provided [`sorting_network.vhd`](src/sorting_network.vhd) file.
In this version of the circuit, simply connect `sorted_valid` directly to `unsorted_valid`.
At this point in time you don't need the clock signal yet.

## Testbench

Implement the testbench ([sorting_network_tb.vhd](./tb/sorting_network_tb.vhd)) to test your `sorting_network` entity.
Make sure your design is able to successfully sort its input.

## Hardware Test

Once your testbench validates your `sorting_network`'s sorting capability, synthesize your design and download the resulting bitstream onto the board.
Utilize the UART interface to send data to be sorted.
Consult the [Design Flow Tutorial](../../level0/designflow.md) to learn how to do that.

Perform the following experiment:

- Reset your design and assert `halt` (put `switches(0)` high) for the `uart_data_streamer`.
- Send two different inputs to be sorted (e.g. numbers and strings).
- Deassert `halt` such that the `uart_data_streamer` sends both input arrays in consecutive clock cycles to your network and check the results.
- Check Quartus' timing analyzer and note down your `Fmax` and `Worst-Case Timing Paths`.

On submission try to prepare answers for following questions:

- Why does your sorting network supposedly sort the first inputs (hint: check [`uart_data_streamer`](../../../lib/uart_data_streamer/doc.md)) ?
- Why does your sorting network fail to successfully sort the second inputs?
- What is your design's `Fmax`?
- What is your design's `Critical-Path`?

**Hint:** You can use the following test inputs for the UART (send them as text):

```
0003000900070005000600010000000800020004
wisppavelushgermcozyhazeyarnployvastdusk
```

The sorting network will treat them as 4-letter words and, hence, sort them alphabetically.

## Fix your design:

In principle there are two ways to fix incorrect output values of the circuit:

- Only sample the output of the combinational sorting network once every few clock cycles, and prevent new data from entering the network in the meantime.
- Introduce work-steps (pipeline stages) and additionally improve throughput of the circuit.

### Delayed Sampling

When `unsorted_valid` is asserted, deassert `unsorted_ready` (in the next clock cycle), such that the `uart_data_streamer` stops providing new data.
Simultaneously, you have postpone asserting `sorted_valid` such that the output of the network is only sampled after a certain number of clock cycles, which represent the combinational delay of the sorting network.
Afterwards, assert `unsorted_ready` again to indicate to the `uart_data_streamer` that new data can be issued.
Find the required number of cycles to sort consecutive input arrays without errors.


Your circuit should now be able to correctly sort the values in the scenario described above.
However, this approach does not make use of the higher clock frequency provided by the PLL.
We could also just used a lower frequency in the first place.
Additionally, Quartus still complains about timing violations, if we do not manually provide information about this timing path!

This approach is not required for your submission.
However, it might help you debug your design and give you the insight that a simulation without delay information does not reflect the real world.

### Pipelining

By introducing pipeline stages in the combinational sorting network, it can be operated with a higher clock frequency and improved throughput.
The individual layers of the sorting network are perfect candidates for such stages.

Thus, introduce registers between the layers in order to allow for pipelined operation of the circuit.
This way you reduce the length of the critical path to one layer of the network instead of the whole circuit.
Additionally, your sorting network is now able to consume and output data in every clock cycle (`unsorted_ready` is always high).
This also enables your network to effectively sort multiple input arrays at the same time.
Assert `sorted_valid` for each clock cycle where your circuit actually produces valid (sorted) output data.

Test your design in the testbench before synthesizing it.
Specifically check, whether your sorting network asserts `sorted_valid` in the correct clock cycles.

When done correctly, your pipelined design should be able to run on a 150 MHz clock without timing violations.
If not, always check for the critical path and try to optimize your design.


**Hint:** `sorted_valid` should "follow" the input data through the pipeline stages in your network. Use a shift register for this purpose.

