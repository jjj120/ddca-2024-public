library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.std_logic_textio.all;

library std;
    use std.textio.all;

    use work.sorting_network_pkg.all;

entity sorting_network_tb is
    generic (
        INPUT_FILE : string := "./tb/input.txt"
    );
end entity;

architecture arch of sorting_network_tb is
    constant CLK_PERIOD : time := 20 ns;
    signal clk, res_n : std_logic := '0';

    file f_in : text;

    signal unsorted_ready : std_logic;
    signal unsorted_data  : word_array(0 to 9);
    signal unsorted_valid : std_logic;
    signal sorted_ready   : std_logic := '1';
    signal sorted_data    : word_array(0 to 9);
    signal sorted_valid   : std_logic;

    function hex_to_slv(hex : string; width : integer) return std_logic_vector is
        variable ret_value : std_logic_vector(width - 1 downto 0) := (others => '0');
        variable temp      : std_logic_vector(3 downto 0);
    begin
        for i in 0 to hex'length - 1 loop
            case hex(hex'high - i) is
                when '0' => temp := x"0";
                when '1' => temp := x"1";
                when '2' => temp := x"2";
                when '3' => temp := x"3";
                when '4' => temp := x"4";
                when '5' => temp := x"5";
                when '6' => temp := x"6";
                when '7' => temp := x"7";
                when '8' => temp := x"8";
                when '9' => temp := x"9";
                when 'a' | 'A' => temp := x"a";
                when 'b' | 'B' => temp := x"b";
                when 'c' | 'C' => temp := x"c";
                when 'd' | 'D' => temp := x"d";
                when 'e' | 'E' => temp := x"e";
                when 'f' | 'F' => temp := x"f";
                when others => report "Conversion Error: char: " & hex(hex'high - i) severity error;
            end case;
            ret_value((i + 1) * 4 - 1 downto i * 4) := temp;
        end loop;
        return ret_value;
    end function;

    impure function get_next_valid_line(file f : text) return line is
        variable l : line;
    begin

        -- Find next non-empty line that is not a comment (starting with #)
        while (not endfile(f)) loop
            readline(f, l);
            if l'length = 0 then
                next;
            elsif l(1) = '#' then
                next;
            else
                return l;
            end if;
        end loop;

        return l;
    end function;

    impure function read_next_line(file f : text) return word_array is
        variable l         : line;
        variable str       : string(1 to 8);
        variable to_return : word_array(0 to 9) := (others => (others => '0'));
    begin
        for i in to_return'range loop
            l := get_next_valid_line(f);
            read(l, str);
            to_return(i) := hex_to_slv(str, 32);
        end loop;

        return to_return;
    end function;

    procedure print_arr(arr : word_array) is
    begin
        for i in arr'range loop
            report "0x" & to_hstring(arr(i)) & " -- 0b" & to_string(arr(i));
        end loop;
    end procedure;
begin

    sorting_network_inst: sorting_network
        port map (
            clk            => clk,
            res_n          => res_n,
            unsorted_ready => unsorted_ready,
            unsorted_data  => unsorted_data,
            unsorted_valid => unsorted_valid,
            sorted_ready   => sorted_ready,
            sorted_data    => sorted_data,
            sorted_valid   => sorted_valid
        );

    send_input: process is
    begin
        res_n <= '0';
        unsorted_valid <= '0';
        unsorted_data <= (others => (others => '0'));
        wait until rising_edge(clk);
        res_n <= '1';
        wait until rising_edge(clk);
        wait for 1 ns;

        -- Open INPUT_FILE, read data in there and apply it to vnorm's vector_in input
        file_open(f_in, INPUT_FILE, read_mode);
        report "Start writing";

        while not endfile(f_in) loop
            if not unsorted_ready then
                wait for CLK_PERIOD;
                next;
            end if;

            unsorted_data <= read_next_line(f_in);
            unsorted_valid <= '1';
            wait for CLK_PERIOD;
            unsorted_valid <= '0';
            -- wait for 80 * CLK_PERIOD;
        end loop;

        report "Finished writing";

        wait for 50 * CLK_PERIOD;
        report "Testbench done";
        std.env.stop;
        wait;
    end process;

    read_output: process is
        variable counter : integer := 0;
    begin
        -- report "Start reading";
        loop
            wait until rising_edge(clk);
            wait for 1 ns;

            if sorted_valid = '1' then
                for i in 1 to 9 loop
                    assert unsigned(sorted_data(i - 1)) <= unsigned(sorted_data(i))
                        report to_string(counter) & ": not sorted: ";

                    if not (unsigned(sorted_data(i - 1)) <= unsigned(sorted_data(i))) then
                        print_arr(sorted_data);
                    end if;
                end loop;

                counter := counter + 1;
            end if;
        end loop;

        wait;
    end process;

    clk_gen: process is
    begin
        clk <= '1';
        wait for CLK_PERIOD / 2;
        clk <= '0';
        wait for CLK_PERIOD / 2;
    end process;
end architecture;
