library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.sorting_network_pkg.all;
use work.util_pkg.all;
use work.uart_data_streamer_pkg.all;

architecture top_arch_sorting_network of top is
	constant DATA_BYTES : positive := 4;
	constant OUTPUT_STREAM_DATA_BYTES : positive := 10 * DATA_BYTES;
	constant OUTPUT_STREAM_DATA_WIDTH : positive := OUTPUT_STREAM_DATA_BYTES * 8;
	constant INPUT_STREAM_DATA_BYTES : positive := OUTPUT_STREAM_DATA_BYTES;
	constant INPUT_STREAM_DATA_WIDTH : positive := INPUT_STREAM_DATA_BYTES * 8;
	
	signal os_valid, is_valid, os_ready,  is_ready : std_logic := '0';
	signal os_data : std_logic_vector(OUTPUT_STREAM_DATA_WIDTH - 1 downto 0) := (others => '0');
	signal sorted_array : word_array(0 to OUTPUT_STREAM_DATA_WIDTH / DATA_BYTES / 8 -1) := (others => (others => '0'));

	component pll_150MHz is
		port (
			inclk0  : in std_logic  := '0';
			c0      : out std_logic 
		);
	end component;

	signal clk_150MHz : std_logic := '0';

	function gen_array_from_vector(x: std_logic_vector) return word_array is
		variable res : word_array(0 to OUTPUT_STREAM_DATA_WIDTH / DATA_BYTES / 8 - 1);
	begin
		for i in 0 to res'high loop
			res(i) := x((DATA_BYTES * 8) * (i+1) -  1 downto (DATA_BYTES * 8) * (i));
		end loop;

		return res;
	end function;

	function gen_vector_from_array(x: word_array) return std_logic_vector is
		variable res : std_logic_vector(OUTPUT_STREAM_DATA_WIDTH - 1 downto 0);
	begin
		for i in 0 to x'high loop
			res((DATA_BYTES * 8) * (i+1) -  1 downto (DATA_BYTES * 8) * (i)) := x(i);
		end loop;

		return res;
	end function;
begin
	pll_inst: pll_150MHz
	port map(
		inclk0 => clk,
		c0 => clk_150MHz
	);

	uart_data_streamer_inst : uart_data_streamer
	generic map (
		CLK_FREQ                 => 150_000_000,
		RX_FIFO_DEPTH            => 8,
		TX_FIFO_DEPTH            => 8,
		BAUD_RATE                => 9600,
		OUTPUT_STREAM_DATA_BYTES => OUTPUT_STREAM_DATA_BYTES,
		INPUT_STREAM_DATA_BYTES  => INPUT_STREAM_DATA_BYTES,
		BYTE_ORDER => LITTLE_ENDIAN
	)
	port map (
		clk   => clk_150MHz,
		res_n => keys(0),

		-- UART 
		rx => rx,
		tx => tx,

		halt => switches(0),
		rx_full => LEDG(0),

		-- output stream -- from rx to core
		os_valid => os_valid, 
		os_data => os_data,
		os_ready => os_ready, 

		-- input stream -- from core to tx
		is_valid => is_valid, 
		is_data  =>  gen_vector_from_array(sorted_array), 
		is_ready => is_ready 
	);

	LEDG(1) <= os_valid;
	LEDG(2) <= is_ready;

	sorter : sorting_network
	port map (
		clk   => clk_150MHz,
		res_n => keys(0),

		unsorted_ready    => os_ready,
		unsorted_data     => gen_array_from_vector(os_data),
		unsorted_valid    => os_valid,

		sorted_ready    => is_ready,
		sorted_data     => sorted_array,
		sorted_valid    => is_valid
	);
end architecture;
