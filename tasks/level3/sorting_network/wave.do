onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /sorting_network_tb/clk
add wave -noupdate /sorting_network_tb/res_n
add wave -noupdate /sorting_network_tb/unsorted_ready
add wave -noupdate /sorting_network_tb/unsorted_data
add wave -noupdate /sorting_network_tb/unsorted_valid
add wave -noupdate /sorting_network_tb/sorted_ready
add wave -noupdate /sorting_network_tb/sorted_data
add wave -noupdate /sorting_network_tb/sorted_valid
add wave -noupdate -expand /sorting_network_tb/sorting_network_inst/registers
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {2542050 ps}
