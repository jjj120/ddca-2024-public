library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

library lpm;
    use lpm.all;
    -- use lpm.lpm_components.all;

library altera_mf;
    use altera_mf.all;
    -- use altera_mf.altera_mf_components.all;
    use work.vnorm_pkg.all;

entity vnorm is
    port (
        clk        : in  std_logic;
        res_n      : in  std_logic;

        vector_in  : in  vec3_t;
        valid_in   : in  std_logic;

        vector_out : out vec3_t;
        valid_out  : out std_logic
    );
end entity;

architecture arch of vnorm is
    constant MULT_PL_DEPTH : integer := 1;
    constant SUM_PL_DEPTH  : integer := 2;
    constant SQRT_PL_DEPTH : integer := 16;
    constant DIV_PL_DEPTH  : integer := 48;

    constant PIPELINE_DEPTH : integer := MULT_PL_DEPTH + SUM_PL_DEPTH + SQRT_PL_DEPTH + DIV_PL_DEPTH + 2;

    type registers_t is record
        vec_squared   : vec3_t;
        num_added_xy  : q16_16_t;
        num_to_add_z  : q16_16_t;
        num_added_all : q16_16_t;
        num_sqrt      : q16_16_t;
        valid_shift   : std_logic_vector(0 to PIPELINE_DEPTH - 1);
        input_shift   : vec3_arr_t(0 to PIPELINE_DEPTH - DIV_PL_DEPTH - 1);
    end record;

    signal vec_squared_x_to_truncate : std_logic_vector(63 downto 0);
    signal vec_squared_y_to_truncate : std_logic_vector(63 downto 0);
    signal vec_squared_z_to_truncate : std_logic_vector(63 downto 0);
    signal root_res                  : std_logic_vector(23 downto 0);
    signal vec_out_x_to_truncate     : std_logic_vector(47 downto 0);
    signal vec_out_y_to_truncate     : std_logic_vector(47 downto 0);
    signal vec_out_z_to_truncate     : std_logic_vector(47 downto 0);

    constant DEFAULT_REGISTERS : registers_t := (
        vec_squared   => (others => (others => '0')),
        num_added_xy  => (others => '0'),
        num_to_add_z  => (others => '0'),
        num_added_all => (others => '0'),
        num_sqrt      => (others => '0'),
        valid_shift   => (others => '0'),
        input_shift   => (others => (others => (others => '0')))
    );

    signal registers, registers_next : registers_t := DEFAULT_REGISTERS;

    component altsqrt
        generic (
            pipeline     : natural;
            q_port_width : natural;
            r_port_width : natural;
            width        : natural;
            lpm_type     : string
        );
        port (
            aclr      : in  std_logic;
            clk       : in  std_logic;
            radical   : in  std_logic_vector(47 downto 0);
            q         : out std_logic_vector(23 downto 0);
            remainder : out std_logic_vector(24 downto 0);
            ena       : in  std_logic
        );
    end component;
begin

    registers_next.input_shift(1 to registers.input_shift'high) <= (registers.input_shift(0 to registers.input_shift'high - 1));
    registers_next.input_shift(0)                               <= vector_in;

    registers_next.valid_shift(1 to registers.valid_shift'high) <= (registers.valid_shift(0 to registers.valid_shift'high - 1));
    registers_next.valid_shift(0)                               <= valid_in;

    valid_out <= registers.valid_shift(registers.valid_shift'high);

    registers_proc: process (clk, res_n) is
    begin
        if not res_n then
            registers <= DEFAULT_REGISTERS;
        elsif rising_edge(clk) then
            registers <= registers_next;
        end if;
    end process;

    vnorm_lpm_mult_inst_x: entity work.vnorm_lpm_mult
        port map (
            aclr   => not res_n,
            clock  => clk,
            dataa  => vector_in.x,
            result => vec_squared_x_to_truncate
        );

    vnorm_lpm_mult_inst_y: entity work.vnorm_lpm_mult
        port map (
            aclr   => not res_n,
            clock  => clk,
            dataa  => vector_in.y,
            result => vec_squared_y_to_truncate
        );
    vnorm_lpm_mult_inst_z: entity work.vnorm_lpm_mult
        port map (
            aclr   => not res_n,
            clock  => clk,
            dataa  => vector_in.z,
            result => vec_squared_z_to_truncate
        );

    registers_next.vec_squared.x <= vec_squared_x_to_truncate(47 downto 16);
    registers_next.vec_squared.y <= vec_squared_y_to_truncate(47 downto 16);
    registers_next.vec_squared.z <= vec_squared_z_to_truncate(47 downto 16);

    -- addition
    registers_next.num_added_xy  <= std_logic_vector(signed(registers.vec_squared.x) + signed(registers.vec_squared.y));
    registers_next.num_to_add_z  <= registers.vec_squared.z;
    registers_next.num_added_all <= std_logic_vector(signed(registers.num_added_xy) + signed(registers.num_to_add_z));

    -- sqrt
    ALTSQRT_component: ALTSQRT
        generic map (
            pipeline     => 16,
            q_port_width => 24,
            r_port_width => 25,
            width        => 48,
            lpm_type     => "ALTSQRT"
        )
        port map (
            aclr      => not res_n,
            clk       => clk,
            radical   => std_logic_vector'(registers.num_added_all & 16x"0"),
            q         => root_res,
            remainder => open,
            ena       => '1'
        );

    registers_next.num_sqrt <= std_logic_vector'(8x"0" & root_res);

    -- divide
    vnorm_lpm_divide_inst_x: entity work.vnorm_lpm_divide
        port map (
            aclr     => not res_n,
            clock    => clk,
            numer    => std_logic_vector'(registers.input_shift(registers.input_shift'high).x & 16x"0"),
            denom    => std_logic_vector'(16x"0" & registers.num_sqrt),
            quotient => vec_out_x_to_truncate,
            remain   => open
        );

    vnorm_lpm_divide_inst_y: entity work.vnorm_lpm_divide
        port map (
            aclr     => not res_n,
            clock    => clk,
            numer    => std_logic_vector'(registers.input_shift(registers.input_shift'high).y & 16x"0"),
            denom    => std_logic_vector'(16x"0" & registers.num_sqrt),
            quotient => vec_out_y_to_truncate,
            remain   => open
        );

    vnorm_lpm_divide_inst_z: entity work.vnorm_lpm_divide
        port map (
            aclr     => not res_n,
            clock    => clk,
            numer    => std_logic_vector'(registers.input_shift(registers.input_shift'high).z & 16x"0"),
            denom    => std_logic_vector'(16x"0" & registers.num_sqrt),
            quotient => vec_out_z_to_truncate,
            remain   => open
        );

    vector_out.x <= vec_out_x_to_truncate(31 downto 0);
    vector_out.y <= vec_out_y_to_truncate(31 downto 0);
    vector_out.z <= vec_out_z_to_truncate(31 downto 0);

    -- pragma synthesis_off
    -- lbl: process (valid_out) is
    --     constant EPSILON : real := 0.01;
    -- begin
    --     if rising_edge(valid_out) then
    --         assert abs (to_real(registers.vec_squared.x) - to_real(vector_in.x) ** 2) < EPSILON report "Value Check: " & "x squared error" severity error;
    --         assert abs (to_real(registers.vec_squared.y) - to_real(vector_in.y) ** 2) < EPSILON report "Value Check: " & "y squared error" severity error;
    --         assert abs (to_real(registers.vec_squared.z) - to_real(vector_in.z) ** 2) < EPSILON report "Value Check: " & "z squared error" severity error;
    --         assert abs (to_real(registers.num_added_all) - to_real(vector_in.x) ** 2 - to_real(vector_in.y) ** 2 - to_real(vector_in.z) ** 2) < EPSILON report "Value Check: " & "registers.num_added_all error" severity error;
    --         assert abs (to_real(registers.num_sqrt) ** 2 - (to_real(registers.num_added_all))) < EPSILON report "Value Check: " & "sqrt error" severity error;
    --         assert abs (to_real(vector_out.x) - to_real(vector_in.x) / to_real(registers.num_sqrt)) < EPSILON report "Value Check: " & "x divide error" severity error;
    --         assert abs (to_real(vector_out.y) - to_real(vector_in.y) / to_real(registers.num_sqrt)) < EPSILON report "Value Check: " & "y divide error" severity error;
    --         assert abs (to_real(vector_out.z) - to_real(vector_in.z) / to_real(registers.num_sqrt)) < EPSILON report "Value Check: " & "z divide error" severity error;
    --     end if;
    -- end process;
    -- pragma synthesis_on
end architecture;
