
# Vector Normalization
**Points:** 1 ` | ` **Keywords:** File I/O, Pipeline, IP Wizard


In this task you will design a simple pipeline, that computes the vector norm of 3D-vectors with high throughput.
A normalized vector is a vector with the same direction as original one but with a length of 1 (a unit vector).
Given a vector 

$$
\mathbf{v}=\begin{pmatrix}
x & y & z
\end{pmatrix}^T
$$

, the associated unit vector $\mathbf{\hat v}$ is given by

$$
\mathbf{\hat v}=\frac{\mathbf{v}}{|\mathbf{v}|}=\frac{1}{\sqrt{x^2+y^2+z^2}}*
\begin{pmatrix}
x\\
y\\
z
\end{pmatrix}
$$


## Task

The described normalization operation shall be implemented in the `vnorm` module, which has the following interface:

```vhdl
component vnorm is
	port (
		clk : in std_logic;
		res_n : in std_logic;

		vector_in : in vec3_t;
		valid_in  : in std_logic;

		vector_out : out vec3_t;
		valid_out  : out std_logic
	);
end component;
```

As usual, `clk` and `res_n` are a clock and an active-low reset signal.
Whenever `valid_in` is high, `vector_in` holds a valid vector that shall be normalized.
Similarly, whenever the `vnorm` module outputs a valid, normalized, vector at `vector_out`, `valid_out` shall be asserted.
You can find the `vec3_t` type in [vnorm_pkg](./src/vnorm_pkg.vhd).

### Pipeline
In order to achieve a high throughput, you have to implement a pipeline.

All calculations have to be carried out with fixed point arithmetic in the Q16.16 format (signed).
You can find information about the Q notation for fixed point numbers [here](https://en.wikipedia.org/wiki/Q_(number_format)).
In essence, for 32-bit words the most significant 16 bit are the integer and the least significant 16 bit the fractional part.
For example, the 32-bit word 0x00018000 encodes the real number 1.5.
You can convert a Q16.16 number *q* to the respective real number by multiplying it with $2^{-16}$.
You do not have to take care of overflows and rounding as the test data which we provide you with does not trigger overflows.

---
**Hint**:
The great advantage of the fixed point format over the floating point number representation is that all mathematical operations are carried out using simple integer arithmetic.
Consider two Q16.16 numbers $q_1=a\cdot 2^{-16},q_2=b\cdot 2^{-16}$.
Adding them does not require any special considerations, as a simple 32-bit signed integer addition is used.
If we disregard the output carry, the result is again a Q16.16 number: $q_1+q_2=a\cdot 2^{-16}+b\cdot 2^{-16}=(a+b)\cdot 2^{-16}$.
For multiplication, you need to be a little bit more careful as the result of multiplying two 32-bit integers is 64 bits wide.
In terms of fixed point arithmetic the resulting number is in the Q32.32 format: $q_1\cdot q_2=a\cdot 2^{-16}\cdot b\cdot 2^{-16}=(a\cdot b)\cdot 2^{-32}$.
As we disregard overflows, in this case you can simply take the "middle" 32 bits (i.e., bits 47 to 16) as the Q16.16 result.

Think about how this data format influences the division and square root operations.

---

The following figure shows the **datapath** of the pipeline you shall implement.
It does not show any of the required pipeline registers.
Note that you have to take care to keep input data you need in later pipeline stages, as the input can change with every clock cycle.
Further, note that you also need a shift register to generate `valid_out` out of `valid_in`.

![Circuit Overview](.mdata/pl.svg)

For the multipliers, square root and dividers use the respective IP cores (`LPM_MULT`, `ALTSQRT`, `LPM_DIVIDE`), provided by the FPGA vendor (Intel).
You will find a short description on how to instantiate them below.
Also note that the IP-cores themselves are also pipelined (which are not shown in the above figure).

**Hint**:
As a first step, you can implement the data path without any pipeline registers or IP cores for the arithmetic operations.
Simply create a combinational model of the data path by performing the required calculations using the `real` data type.
The `vnorm_pkg` provides appropriate functions to convert fixed point values to and from `real` values.
Using this model you can then implement and validate your testbench.
Moreover, it also yields values for the intermediate results of the calculation that you can use to debug the actual pipeline.

However, keep in mind that due to rounding this calculation method yields results that can be slightly off (by one bit).
Hence, the comparison function in the testbench must account for these slight deviations.

### Instantiating an IP Core
To use one of the IP cores provided in Quartus' IP catalog, first click on `View -> Utility Windows -> IP Catalog`.
Now you can use the search field to search for an IP core (e.g. `LPM_MULT`) and double-click it.
Quartus will now start the process of creating an IP variation file, i.e., a specific instantiation of the generic IP core tailored to your demands.
Set a suitable directory to hold this IP variation file, e.g. in `top/src` and set the file type to `VHDL`.

Next, Quartus will open a wizard that allows you to configure your IP variation.
Click through the wizard and set the parameters as you see fit for your pipeline.
Finally, Quartus will ask you if you want to add the IP variation file to the project.
Keep this on `Yes` and `Finish` the wizard.

You can now instantiate your IP variation in your design.

Use the following pipeline depths for the different IP cores:

| IP | Pipeline depth |
| -- | -------------- |
| LPM_MULT | 1 |
| ALTSQRT | 16 |
| LPM_DIVIDE | 48 |

**Note**: Unfortunately the [documentation](https://www.intel.com/content/www/us/en/docs/programmable/683490/20-3/altsqrt-integer-square-root-ip-core.html) of the `ALTSQRT` module provided by Intel is not entirely clear / correct regarding the `remainder` output and how it is related to the `R_PORT_WIDTH` generic.
Instead, be referred to the older, but correct, [documentation](https://application-notes.digchip.com/038/38-21645.pdf) provided by Altera and set the `R_PORT_WIDTH` generic to the value corresponding to `WIDTH/2+1`.

## Testbench
Create a testbench that reads 3D vectors provided in the file [input.txt](./tb/input.txt).
The file contains vectors with each of their x,y,z components in a dedicated line, given as 32-bit hexadecimal numbers.
I.e., a vector is made up of three lines of the input file.
Furthermore, the input file can contain comment lines, where the line starts with '#' and also empty lines.
Make sure to skip them when reading the input data.

In each clock cycle, read the next input vector and apply it to your `vnorm` instance until you reach the end of the file.

To check the correctness of your design, you are also given a [ref.txt](./tb/ref.txt) file that contains the normalized output vectors of a reference model in the same format as the `input.txt` file.
Compare the vectors contained in this file with the outputs of your `vnorm`.
The [vnorm_pkg](./src/vnorm_pkg.vhd) overloads the `=` operator in order to easily compare two vectors for equality.
Use an assertion for the comparison and report any deviations from the reference output.

You can find a basic introduction to handling files in VHDL testbenches in the slides of the Hardware Modeling lecture and online (e.g. [here](https://vhdlguide.com/2017/08/10/files-theory-examples/)).

**Note**: The last reference value is wrong on purpose to allow you to test if your testbench / assertion correctly detects deviations.

## Hardware Test
You are already provided with the [top_arch_vnorm](top_arch_vnorm.vhd) file, containing a top architecture that instantiates your design and the [uart_data_streamer](../../../lib/uart_data_streamer/doc.md) and connects them to each other.

Synthesize your design and make sure that no timing violations are reported!
Then, after downloading the resulting bitstream to the board, you can use the UART interface to send vectors to and receive vectors from your design.
Consult the [Design Flow Tutorial](../../level0/designflow.md) to learn how to do that.

**Hint:** You can directly send the `input.txt` file to the board and create an output file that you can compare to `ref.txt` (e.g., using `diff`).

