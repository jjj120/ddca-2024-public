library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.std_logic_textio.all;
    use ieee.math_real.all;

library std;
    use std.textio.all;

    use work.vnorm_pkg.all;

entity vnorm_tb is
    generic (
        INPUT_FILE : string := "./tb/input.txt";
        REF_FILE   : string := "./tb/ref.txt"
    );
end entity;

architecture arch of vnorm_tb is
    constant CLK_PERIOD : time := 20 ns;
    signal clk, res_n : std_logic;

    signal vec_in, vec_out     : vec3_t    := (others => (others => '0'));
    signal valid_in, valid_out : std_logic := '0';

    file f_in  : text;
    file f_ref : text;

    impure function get_next_valid_line(file f : text) return line is
        variable l : line;
    begin

        -- Find next non-empty line that is not a comment (starting with #)
        while (not endfile(f)) loop
            readline(f, l);
            if l'length = 0 then
                next;
            elsif l(1) = '#' then
                next;
            else
                return l;
            end if;
        end loop;

        return l;
    end function;

    function hex_to_slv(hex : string; width : integer) return std_logic_vector is
        variable ret_value : std_logic_vector(width - 1 downto 0) := (others => '0');
        variable temp      : std_logic_vector(3 downto 0);
    begin
        for i in 0 to hex'length - 1 loop
            case hex(hex'high - i) is
                when '0' => temp := x"0";
                when '1' => temp := x"1";
                when '2' => temp := x"2";
                when '3' => temp := x"3";
                when '4' => temp := x"4";
                when '5' => temp := x"5";
                when '6' => temp := x"6";
                when '7' => temp := x"7";
                when '8' => temp := x"8";
                when '9' => temp := x"9";
                when 'a' | 'A' => temp := x"a";
                when 'b' | 'B' => temp := x"b";
                when 'c' | 'C' => temp := x"c";
                when 'd' | 'D' => temp := x"d";
                when 'e' | 'E' => temp := x"e";
                when 'f' | 'F' => temp := x"f";
                when others => report "Conversion Error: char: " & hex(hex'high - i) severity error;
            end case;
            ret_value((i + 1) * 4 - 1 downto i * 4) := temp;
        end loop;
        return ret_value;
    end function;

    impure function read_next_vector(file f : text) return vec3_t is
        variable l      : line;
        variable str    : string(1 to 8);
        variable vector : vec3_t;
        variable status : boolean;
    begin

        -- Read next vector from file f (can be multiple lines!)
        l := get_next_valid_line(f);
        assert l'length <= str'length report "String does not fit for x!";
        read(l, str, status);
        assert status report "Read failed at reading x!";
        vector.x := hex_to_slv(str, 32);

        l := get_next_valid_line(f);
        assert l'length <= str'length report "String does not fit for y!";
        read(l, str, status);
        assert status report "Read failed at reading y!";
        vector.y := hex_to_slv(str, 32);

        l := get_next_valid_line(f);
        assert l'length <= str'length report "String does not fit for z!";
        read(l, str, status);
        assert status report "Read failed at reading z!";
        vector.z := hex_to_slv(str, 32);

        return vector;
    end function;

begin

    uut: entity work.vnorm
        port map (
            clk        => clk,
            res_n      => res_n,
            valid_in   => valid_in,
            vector_in  => vec_in,
            valid_out  => valid_out,
            vector_out => vec_out
        );

    stimulus: process is
    begin
        res_n <= '0';
        valid_in <= '0';
        vec_in <= ZERO_VEC;
        wait until rising_edge(clk);
        res_n <= '1';
        wait until rising_edge(clk);
        wait for 1 ns;

        -- Open INPUT_FILE, read data in there and apply it to vnorm's vector_in input
        file_open(f_in, INPUT_FILE, read_mode);
        report "Start writing";

        while not endfile(f_in) loop
            vec_in <= read_next_vector(f_in);
            -- report "vec_len: " & to_string(sqrt(to_real(vec_in.x) * to_real(vec_in.x) + to_real(vec_in.y) * to_real(vec_in.y) + to_real(vec_in.z) * to_real(vec_in.z)));
            valid_in <= '1';
            wait for CLK_PERIOD;
            valid_in <= '0';
            -- wait for 80 * CLK_PERIOD;
        end loop;

        report "Finished writing";
        wait;
    end process;

    score: process is
        variable counter  : integer := 0;
        variable next_vec : vec3_t  := (others => (others => '0'));

        function d_num_to_string(num : q16_16_t) return string is
        begin
            return to_string(to_real(num));
        end function;

        function to_real_vec_string(vec : vec3_t) return string is
        begin
            return "(X: " & d_num_to_string(vec.x) & ", Y: " & d_num_to_string(vec.y) & ", Z: " & d_num_to_string(vec.z) & ")";
        end function;

        function calc_error(vec1, vec2 : vec3_t) return vec3_t is
            variable vec_err : vec3_t;
        begin
            vec_err.x := (to_q16_16_t(abs(to_real(vec1.x) - to_real(vec2.x))));
            vec_err.y := (to_q16_16_t(abs(to_real(vec1.y) - to_real(vec2.y))));
            vec_err.z := (to_q16_16_t(abs(to_real(vec1.z) - to_real(vec2.z))));
            return vec_err;
        end function;

        function calc_len(vec : vec3_t) return real is
        begin
            return sqrt(to_real(vec.x) * to_real(vec.x) + to_real(vec.y) * to_real(vec.y) + to_real(vec.z) * to_real(vec.z));
        end function;

    begin

        --  Open REF_FILE, read data, assert that vnorm's vector_out is correct using an assertion
        file_open(f_ref, REF_FILE, read_mode);

        counter := 0;

        -- report "Start reading";
        while not endfile(f_ref) loop
            wait until rising_edge(clk);
            wait for 1 ns;

            if valid_out = '1' then
                next_vec := read_next_vector(f_ref);

                if endfile(f_ref) then
                    assert vec_out /= next_vec report "Last one was true but should be false!";
                    exit;
                end if;

                assert vec_out = next_vec report to_string(counter) & ": vec_out " & to_string(vec_out) & " -- " & to_real_vec_string(vec_out);
                assert vec_out = next_vec report to_string(counter) & ": vec_ref " & to_string(next_vec) & " -- " & to_real_vec_string(next_vec) & " do not match.";
                assert vec_out = next_vec report to_string(counter) & ": error:  " & to_string(calc_error(vec_out, next_vec)) & " -- " & to_real_vec_string(calc_error(vec_out, next_vec));
                assert vec_out = next_vec report to_string(counter) & ": len out " & to_string(calc_len(vec_out));
                assert vec_out = next_vec report "";
                counter := counter + 1;
            end if;
        end loop;

        report "Testbench done";
        std.env.stop;
        wait;
    end process;

    clk_gen: process is
    begin
        clk <= '1';
        wait for CLK_PERIOD / 2;
        clk <= '0';
        wait for CLK_PERIOD / 2;
    end process;
end architecture;
