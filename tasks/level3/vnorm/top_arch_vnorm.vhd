library ieee;
use ieee.std_logic_1164.all;

use work.vnorm_pkg.all;
use work.uart_data_streamer_pkg.all;

architecture top_arch_vnorm of top is
	signal valid_out : std_logic;
	signal vector_in, vector_out : vec3_t;
	signal os_valid : std_logic;
	signal os_data, is_data : std_logic_vector(95 downto 0);
	signal os_data_rev : std_logic_vector(95 downto 0);

	function reverse_bytes(x: std_logic_vector; width: positive) return std_logic_vector is
		variable reversed : std_logic_vector(width-1 downto 0);
	begin
		for i in 0 to width/8-1 loop
			reversed(8*(i+1)-1 downto 8*i) := x(width-1-8*i downto width-8*(i+1));
		end loop;

		return reversed;
	end function;

begin

	vnorm_inst : vnorm
	port map(
		clk => clk,
		res_n => keys(0),

		vector_in => vector_in,
		valid_in  => os_valid,

		vector_out => vector_out,
		valid_out  => valid_out
	);

	os_data_rev <= reverse_bytes(os_data, 96);

	vector_in.x <= os_data_rev(31 downto 0);
	vector_in.y <= os_data_rev(63 downto 32);
	vector_in.z <= os_data_rev(95 downto 64);

	is_data(95 downto 64) <= reverse_bytes(vector_out.x, 32);
	is_data(63 downto 32) <= reverse_bytes(vector_out.y, 32);
	is_data(31 downto 0)  <= reverse_bytes(vector_out.z, 32);

	uart_data_streamer_inst : uart_data_streamer
	generic map (
		CLK_FREQ => 50_000_000,
		BAUD_RATE => 9600,
		RX_FIFO_DEPTH => 64,
		TX_FIFO_DEPTH => 64,
		OUTPUT_STREAM_DATA_BYTES => 12,
		INPUT_STREAM_DATA_BYTES => 12
	)
	port map (
		clk   => clk,
		res_n => keys(0),

		rx => rx,
		tx => tx,

		halt => switches(0),
		rx_full => LEDG(0),

		os_valid => os_valid,
		os_data  => os_data,
		os_ready => '1',

		is_valid => valid_out,
		is_data  => is_data,
		is_ready => open
	);

end architecture;
