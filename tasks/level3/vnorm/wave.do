onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /vnorm_tb/clk
add wave -noupdate /vnorm_tb/res_n
add wave -noupdate /vnorm_tb/vec_in
add wave -noupdate /vnorm_tb/valid_in
add wave -noupdate /vnorm_tb/uut/valid_out
add wave -noupdate /vnorm_tb/uut/vector_out
add wave -noupdate /vnorm_tb/uut/vec_squared_x_to_truncate
add wave -noupdate /vnorm_tb/uut/vec_squared_y_to_truncate
add wave -noupdate /vnorm_tb/uut/vec_squared_z_to_truncate
add wave -noupdate /vnorm_tb/uut/root_res
add wave -noupdate /vnorm_tb/uut/vec_out_x_to_truncate
add wave -noupdate /vnorm_tb/uut/vec_out_y_to_truncate
add wave -noupdate /vnorm_tb/uut/vec_out_z_to_truncate
add wave -noupdate -expand -subitemconfig {/vnorm_tb/uut/registers.input_shift -expand} /vnorm_tb/uut/registers
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {197 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 156
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {2205 ns}
