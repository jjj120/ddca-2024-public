import subprocess

def count_mem_accesses() -> int:
    # Define the bash command as a single string
    command = "make clean sim | grep DMEM | grep -v -i 'addr=0x3FF' | wc -l"
    
    # Use subprocess to execute the command and capture the output
    result = subprocess.run(command, shell=True, capture_output=True, text=True)
    
    # Check if the command was successful
    if result.returncode == 0:
        # Parse and return the output as an integer
        return int(result.stdout.strip())
    else:
        # Handle the error case
        raise RuntimeError(f"Command failed with return code {result.returncode}: {result.stderr}")

def update_constants_in_file(file_path, new_ways, new_sets):
    # Define the lines to search for and their replacement formats
    ways_line_prefix = "constant WAYS : natural :="
    sets_line_prefix = "constant SETS : natural :="
    
    # Read the contents of the file
    with open(file_path, 'r') as file:
        lines = file.readlines()
    
    # Iterate through the lines and update the specified lines
    updated_lines = []
    for line in lines:
        if line.strip().startswith(ways_line_prefix):
            updated_line = f"{ways_line_prefix} {new_ways};\n"
            updated_lines.append(updated_line)
        elif line.strip().startswith(sets_line_prefix):
            updated_line = f"{sets_line_prefix} {new_sets};\n"
            updated_lines.append(updated_line)
        else:
            updated_lines.append(line)
        
    # Write the updated contents back to the file
    with open(file_path, 'w') as file:
        file.writelines(updated_lines)

def set_arch(file_path: str, arch: bool) -> None:
    # Define the lines to search for and their replacement formats
    arch_line_prefix = "cache_dmem_inst: entity work.cache("
    arch_line_bypass = "    cache_dmem_inst: entity work.cache(bypass) generic map (\n"
    arch_line_arch = "    cache_dmem_inst: entity work.cache(arch) generic map (\n"
    
    # Read the contents of the file
    with open(file_path, 'r') as file:
        lines = file.readlines()
    
    # Iterate through the lines and update the specified lines
    updated_lines = []
    for line in lines:
        if line.strip().startswith(arch_line_prefix):
            if arch:
                updated_line = arch_line_arch
            else:
                updated_line = arch_line_bypass
            updated_lines.append(updated_line)
        else:
            updated_lines.append(line)

    # Write the updated contents back to the file
    with open(file_path, 'w') as file:
        file.writelines(updated_lines)

# Example usage
try:
    set_arch("tb/pl_cache_tb.vhd", False)
    print(f"Bypass: {count_mem_accesses()}")
    set_arch("tb/pl_cache_tb.vhd", True)

    for ways in [1, 2, 4]:
        for sets in [1, 2, 4, 8, 16]:
            update_constants_in_file("src/cache_pkg.vhd", ways, sets)
            print(f"Ways: {ways}, Sets: {sets}: {count_mem_accesses()}")
            
except Exception as e:
    print(f"An error occurred: {e}")