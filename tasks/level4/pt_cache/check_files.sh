#!/bin/bash

files_that_should_exist="
./src/cache_pkg.vhd
./src/cache.vhd
./src/data_cache.vhd
./src/data_way.vhd
./src/mgmt_cache.vhd
./src/mgmt_way.vhd
./src/repl.vhd
./src/cache_pll.vhd
top_arch_cache.vhd
"

error_counter=0

#check files that should have been created during exercise 1
for f in $files_that_should_exist; do
	if [ ! -e "$f" ]; then
		echo "Error: file $f does not exist"; 
		let "error_counter++"
	fi;
done;

exit $error_counter
