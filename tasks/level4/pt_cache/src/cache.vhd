library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.std_logic_misc.all;

    use work.mem_pkg.all;
    use work.rv_sys_pkg.all;
    use work.cache_pkg.all;

entity cache is
    generic (
        ADDR_MASK : mem_address_t := (others => '1')
    );
    port (
        clk          : in  std_logic;
        res_n        : in  std_logic;
        cpu_to_cache : in  mem_out_t;
        cache_to_cpu : out mem_in_t;
        cache_to_mem : out mem_out_t;
        mem_to_cache : in  mem_in_t
    );
end entity;

architecture arch of cache is
    type fsm_state_t is (
            IDLE,
            READ_CACHE,
            READ_MEM_START,
            READ_MEM,
            WRITE_BACK_START,
            WRITE_BACK,
            DIRECT_WRITE_START,
            DIRECT_WRITE
        );

    constant DEFAULT_FSM_STATE : fsm_state_t := IDLE;

    signal state, next_state : fsm_state_t := DEFAULT_FSM_STATE;

    alias address : mem_address_t is cpu_to_cache.address;
    alias tag     : tag_t         is address(RV_SYS_ADDR_WIDTH - 1 downto SETS_LD);
    alias index   : index_t       is address(SETS_LD - 1 downto 0);

    type data_cache_t is record
        wr       : std_logic;
        byteena  : mem_byteena_t;
        data_in  : mem_data_t;
        data_out : mem_data_t;
    end record;

    constant DEFAULT_DATA_CACHE : data_cache_t := (
        wr       => '0',
        byteena  => (others => '0'),
        data_in  => (others => '0'),
        data_out => (others => '0')
    );
    signal data : data_cache_t := DEFAULT_DATA_CACHE;

    type management_t is record
        rd            : std_logic;
        mgmt_info_in  : mgmt_info_t;
        wr            : std_logic;
        mgmt_info_out : mgmt_info_t;
        way_out       : way_t;
        hit_out       : std_logic;
    end record;

    constant DEFAULT_MGMT_INFO : mgmt_info_t := (
        valid => '0',
        dirty => '0',
        tag   => (others => '0')
    );

    constant DEFAULT_MANAGEMENT : management_t := (
        rd            => '0',
        mgmt_info_in  => DEFAULT_MGMT_INFO,
        wr            => '0',
        mgmt_info_out => DEFAULT_MGMT_INFO,
        way_out       => (others => '0'),
        hit_out       => '0'
    );

    signal management : management_t := DEFAULT_MANAGEMENT;

    pure function check_addr_bypass(addr : mem_address_t) return boolean is
    begin
        return or_reduce(addr and not ADDR_MASK) = '1';
    end function;

    -- pragma translate_off
    pure function dbg_string(addr : mem_address_t; hit, wr, rd : std_logic; used_data : mem_data_t; way : way_t; byteena : mem_byteena_t; curr_time : time) return string is
    begin
        if WAYS = 1 then
            return ht & time'image(curr_time) & ": Addr: 0x" & to_hstring(addr) & "(Index: " & to_hstring(get_index(addr)) & ", tag: " & to_hstring(get_tag(addr)) & ", byteena: " & to_string(byteena) & ") Hit: " & to_string(hit) & " wr: " & to_string(wr) & " rd: " & to_string(rd) & " Data: " & to_hstring(used_data) & " (" & to_string(used_data) & ")";
        else
            return ht & time'image(curr_time) & ": Addr: 0x" & to_hstring(addr) & "(Index: " & to_hstring(get_index(addr)) & ", tag: " & to_hstring(get_tag(addr)) & ", byteena: " & to_string(byteena) & ", way: " & to_hstring(way) & ") Hit: " & to_string(hit) & " wr: " & to_string(wr) & " rd: " & to_string(rd) & " Data: " & to_hstring(used_data) & " (" & to_string(used_data) & ")";
        end if;
    end function;
    -- pragma translate_on
begin
    comb_logic: process (all) is
    begin
        next_state <= state;

        management.wr <= '0';
        management.rd <= '0'; -- always read management data
        management.mgmt_info_in <= management.mgmt_info_out; -- default write back what came out
        management.mgmt_info_in.tag <= tag;

        data.byteena <= cpu_to_cache.byteena;
        data.data_in <= (others => '0');
        data.wr <= '0';

        cache_to_mem <= MEM_OUT_NOP;

        cache_to_mem.address <= cpu_to_cache.address;
        cache_to_mem.byteena <= "1111"; -- always read and write whole words
        cache_to_mem.wrdata <= (others => '0');
        cache_to_mem.rd <= '0';
        cache_to_mem.wr <= '0';

        cache_to_cpu.busy <= '1';
        cache_to_cpu.rddata <= data.data_out;

        -- if ADDR_MASK matches, overwrite all
        if check_addr_bypass(cpu_to_cache.address) then
            -- pragma translate_off
            if cpu_to_cache.rd or cpu_to_cache.wr then
                report "Cache: Bypass: " & dbg_string(cpu_to_cache.address, '0', cpu_to_cache.wr, cpu_to_cache.rd, cpu_to_cache.wrdata,(others => 'X'), cpu_to_cache.byteena, now);
            end if;
            -- pragma translate_on
            cache_to_cpu <= mem_to_cache;
            cache_to_mem <= cpu_to_cache;
        else

            case state is
                when IDLE =>
                    cache_to_cpu.busy <= '0';

                    if cpu_to_cache.wr then
                        if management.hit_out then
                            -- pragma translate_off
                            report "Cache: Write: " & dbg_string(cpu_to_cache.address, management.hit_out, cpu_to_cache.wr, cpu_to_cache.rd, cpu_to_cache.wrdata, management.way_out, cpu_to_cache.byteena, now);
                            -- pragma translate_on
                            management.mgmt_info_in.dirty <= '1';
                            management.mgmt_info_in.valid <= '1';
                            management.wr <= '1';

                            data.wr <= '1';
                            data.data_in <= cpu_to_cache.wrdata;
                        else
                            -- write directly to memory
                            cache_to_cpu.busy <= '1';
                            next_state <= DIRECT_WRITE_START;
                        end if;
                    end if;

                    if cpu_to_cache.rd then
                        cache_to_cpu.busy <= '1';
                        if management.hit_out then
                            next_state <= READ_CACHE;
                            -- directly write out data
                            -- report "Cache: Cache read: " & dbg_string(cpu_to_cache.address, management.hit_out, cpu_to_cache.wr, cpu_to_cache.rd, data.data_out, management.way_out, cpu_to_cache.byteena, now);
                            -- management.rd <= '1';
                        else
                            -- no hit in cache
                            if management.mgmt_info_out.dirty then
                                -- if data is dirty, first write back, then replace
                                next_state <= WRITE_BACK_START;
                            else
                                next_state <= READ_MEM_START;
                            end if;
                        end if;
                    end if;

                when READ_CACHE =>
                    -- pragma translate_off
                    report "Cache: Cache read: " & dbg_string(cpu_to_cache.address, management.hit_out, cpu_to_cache.wr, cpu_to_cache.rd, data.data_out, management.way_out, cpu_to_cache.byteena, now);
                    -- pragma translate_on
                    cache_to_cpu.rddata <= data.data_out;
                    management.rd <= '1';
                    next_state <= IDLE;

                when READ_MEM_START =>
                    cache_to_mem.address <= cpu_to_cache.address;
                    cache_to_mem.rd <= '1';
                    next_state <= READ_MEM;

                when READ_MEM =>
                    if not mem_to_cache.busy then
                        management.mgmt_info_in <= (
                            valid => '1',
                            dirty => '0',
                            tag   => get_tag(cpu_to_cache.address)
                        );
                        management.wr <= '1';

                        data.data_in <= mem_to_cache.rddata;
                        data.byteena <= "1111";
                        data.wr <= '1';

                        cache_to_cpu.rddata <= mem_to_cache.rddata;

                        next_state <= IDLE;

                        -- pragma translate_off
                        report "Cache: Mem-Read: " & dbg_string(cpu_to_cache.address, management.hit_out, cpu_to_cache.wr, cpu_to_cache.rd, mem_to_cache.rddata, management.way_out, cpu_to_cache.byteena, now);
                        -- pragma translate_on
                    end if;

                when WRITE_BACK_START =>
                    next_state <= WRITE_BACK;
                    cache_to_mem.address <= std_logic_vector'(management.mgmt_info_out.tag & index);
                    cache_to_mem.wr <= '1';

                    -- data to write back
                    cache_to_mem.wrdata <= data.data_out;
                    cache_to_mem.byteena <= "1111";

                when WRITE_BACK =>
                    if not mem_to_cache.busy then
                        -- management.mgmt_info_in <= (
                        --     valid => '0',
                        --     dirty => '0',
                        --     tag   => (others => '0')
                        -- );
                        -- management.wr <= '1';
                        next_state <= READ_MEM_START;

                        -- pragma translate_off
                        report "Cache: Writeback: " & dbg_string(std_logic_vector'(management.mgmt_info_out.tag & index), management.hit_out, cpu_to_cache.wr, cpu_to_cache.rd, data.data_out, management.way_out, cpu_to_cache.byteena, now);
                        -- pragma translate_on
                    end if;

                when DIRECT_WRITE_START =>
                    next_state <= DIRECT_WRITE;
                    cache_to_mem.address <= cpu_to_cache.address;
                    cache_to_mem.wr <= '1';

                    -- write through (because of ADDR_MASK) or cache miss
                    cache_to_mem.wrdata <= cpu_to_cache.wrdata;
                    cache_to_mem.byteena <= cpu_to_cache.byteena;

                when DIRECT_WRITE =>
                    if not mem_to_cache.busy then
                        -- pragma translate_off
                        report "Cache: D-Write: " & dbg_string(cpu_to_cache.address, management.hit_out, cpu_to_cache.wr, cpu_to_cache.rd, cpu_to_cache.wrdata, management.way_out, cpu_to_cache.byteena, now);
                        -- pragma translate_on
                        next_state <= IDLE;
                    end if;

                when others => report "UNKNOWN FSM STATE!" severity failure;
            end case;
        end if;
    end process;

    registers_fsm: process (clk, res_n) is
    begin
        if res_n /= '1' then
            state <= DEFAULT_FSM_STATE;
        elsif rising_edge(clk) then
            state <= next_state;
        end if;
    end process;

    data_cache_inst: entity work.data_cache
        port map (
            clk      => clk,
            wr       => data.wr,
            way      => management.way_out,
            index    => index,
            byteena  => data.byteena,
            data_in  => data.data_in,
            data_out => data.data_out
        );

    mgmt_cache_inst: entity work.mgmt_cache
        port map (
            clk           => clk,
            res_n         => res_n,
            index         => index,
            rd            => management.rd,
            mgmt_info_in  => management.mgmt_info_in,
            wr            => management.wr,
            mgmt_info_out => management.mgmt_info_out,
            way_out       => management.way_out,
            hit_out       => management.hit_out
        );

end architecture;

architecture bypass of cache is
begin
    cache_to_mem <= cpu_to_cache;
    cache_to_cpu <= mem_to_cache;
end architecture;
