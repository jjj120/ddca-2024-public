library ieee;
    use ieee.std_logic_1164.all;

    use work.math_pkg.all;
    use work.rv_sys_pkg.all;

package cache_pkg is
	-- Change these two constant for different cache configurations
	constant WAYS : natural := 2; -- 1, 2, 4
	constant SETS : natural := 8; -- power of two

    constant WAYS_LD    : natural := log2c(WAYS);
    constant SETS_LD    : natural := log2c(SETS);
    constant INDEX_SIZE : natural := SETS_LD;
    constant TAG_SIZE   : natural := RV_SYS_ADDR_WIDTH - INDEX_SIZE;
    subtype ways_range is natural range 0 to WAYS - 1;
    subtype sets_range is natural range 0 to SETS - 1;

    subtype tag_t is std_logic_vector(TAG_SIZE - 1 downto 0);
    subtype index_t is std_logic_vector(INDEX_SIZE - 1 downto 0);
    subtype way_t is std_logic_vector(WAYS_LD - 1 downto 0);

    type mgmt_info_t is record
        valid : std_logic;
        dirty : std_logic;
        tag   : tag_t;
    end record;

    type mgmt_info_arr_t is array (integer range <>) of mgmt_info_t;

    pure function get_index(addr : mem_address_t) return index_t;
    pure function get_tag(addr : mem_address_t) return tag_t;
end package;

package body cache_pkg is
    pure function get_index(addr : mem_address_t) return index_t is
    begin
        return addr(SETS_LD - 1 downto 0);
    end function;

    pure function get_tag(addr : mem_address_t) return tag_t is
    begin
        return addr(RV_SYS_ADDR_WIDTH - 1 downto SETS_LD);
    end function;
end package body;
