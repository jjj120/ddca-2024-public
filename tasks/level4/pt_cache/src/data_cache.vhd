library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.mem_pkg.all;
    use work.rv_sys_pkg.all;
    use work.cache_pkg.all;

entity data_cache is
    port (
        clk      : in  std_logic;
        wr       : in  std_logic;
        way      : in  way_t;
        index    : in  index_t;
        byteena  : in  mem_byteena_t;
        data_in  : in  mem_data_t;
        data_out : out mem_data_t
    );
end entity;

architecture arch of data_cache is
    signal data_out_arr : mem_data_array_t(ways_range);
    signal wr_arr       : std_logic_vector(ways_range);
begin
    gen_mapping: if WAYS = 1 generate
        wr_arr   <= (others => wr);
        data_out <= data_out_arr(0);
    else generate
        process (all) is
        begin
            wr_arr <= (others => '0');
            wr_arr(to_integer(unsigned(way))) <= wr;
        end process;
        data_out <= data_out_arr(to_integer(unsigned(way)));
    end generate;

    ways_storage_gen: for i in ways_range generate
        data_way_inst: entity work.data_way
            port map (
                clk      => clk,
                wr       => wr_arr(i),
                index    => index,
                byteena  => byteena,
                data_in  => data_in,
                data_out => data_out_arr(i)
            );
    end generate;
end architecture;
