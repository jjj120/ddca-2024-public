library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.mem_pkg.all;
    use work.rv_sys_pkg.all;
    use work.cache_pkg.all;

entity data_way is
    port (
        clk      : in  std_logic;
        wr       : in  std_logic;
        index    : in  index_t;
        byteena  : in  mem_byteena_t;
        data_in  : in  mem_data_t;
        data_out : out mem_data_t
    );
end entity;

architecture arch of data_way is
    signal data_from_ram, data_to_ram : mem_data_t;
    constant BYTE_WIDTH : integer := RV_SYS_DATA_WIDTH / byteena'length;
begin
    data_out <= data_from_ram;

    data_to_ram <= data_in;

    ram_gen: for i in 0 to byteena'length - 1 generate
        dp_ram_1c1r1w_rdw_inst: dp_ram_1c1r1w_rdw
            generic map (
                ADDR_WIDTH => INDEX_SIZE,
                DATA_WIDTH => BYTE_WIDTH
            )
            port map (
                clk      => clk,
                rd1_addr => index,
                rd1_data => data_from_ram((i + 1) * BYTE_WIDTH - 1 downto i * BYTE_WIDTH),
                wr2_addr => index,
                wr2_data => data_to_ram((i + 1) * BYTE_WIDTH - 1 downto i * BYTE_WIDTH),
                wr2      => byteena(i) and wr
            );
    end generate;
end architecture;
