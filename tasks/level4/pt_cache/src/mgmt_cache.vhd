library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.std_logic_misc.all;

    use work.mem_pkg.all;
    use work.cache_pkg.all;
    use work.math_pkg.log2c;

entity mgmt_cache is
    port (
        clk           : in  std_logic;
        res_n         : in  std_logic;
        index         : in  index_t;
        rd            : in  std_logic;
        mgmt_info_in  : in  mgmt_info_t;
        wr            : in  std_logic;
        mgmt_info_out : out mgmt_info_t;
        way_out       : out way_t;
        hit_out       : out std_logic
    );
end entity;

architecture arch of mgmt_cache is
    signal mgmt_info_arr : mgmt_info_arr_t(ways_range);
    signal way           : way_t;
    signal hit           : std_logic := '0';
    signal wr_arr        : std_logic_vector(ways_range);
    signal wr_repl       : std_logic;

    signal valid_arr           : std_logic_vector(ways_range);
    signal dirty_arr           : std_logic_vector(ways_range);
    signal replace_out_arr     : std_logic_vector(ways_range); -- named from pov of memory
    signal replace_between_arr : std_logic_vector(ways_range);
    signal replace_in_arr      : std_logic_vector(ways_range); -- named from pov of memory

    signal match_arr : std_logic_vector(ways_range) := (others => '0');
begin
    filter_fully_ass_cache: if WAYS = 1 generate
        mgmt_info_out <= mgmt_info_arr(0);
        way_out       <= (others => '0');
        wr_arr        <= (others => wr);

        hit_out <= '1' when mgmt_info_arr(0).tag = mgmt_info_in.tag and mgmt_info_arr(0).valid = '1' else '0';

        -- replace data irrelevant, way_out is fixed
        replace_in_arr <= (others => '0');
        wr_repl        <= '0';
    else generate

        mgmt_info_out <= mgmt_info_arr(to_integer(unsigned(way))); -- when rd else (tag => (others => '0'), others => '0'); -- TODO: ignore rd? just always give out data at index?
        way_out       <= way;

        process (all) is
            pure function flip(data : std_logic_vector) return std_logic_vector is
                variable to_return : std_logic_vector(data'range) := data;
            begin
                for i in data'range loop
                    to_return(data'high - i) := data(i);
                end loop;
                return to_return;
            end function;
        begin
            -- report to_string(wr) & to_string(rd);
            hit <= or_reduce(match_arr);
            way <= (others => '0');

            if hit then
                for i in ways_range loop
                    if match_arr(i) then
                        way <= std_logic_vector(to_unsigned(i, WAYS_LD));
                    end if;
                end loop;
                -- way <= std_logic_vector(to_unsigned(log2c(to_integer(unsigned(flip(match_arr)))), WAYS_LD));
            else
                -- no hit, replacement needed
                for i in ways_range loop
                    if replace_out_arr(i) then
                        way <= std_logic_vector(to_unsigned(i, WAYS_LD));
                    end if;
                end loop;
                -- way <= std_logic_vector(to_unsigned(log2c(to_integer(unsigned(flip(replace_out_arr)))), WAYS_LD));
            end if;

            replace_in_arr <= (others => '0');
            replace_in_arr(to_integer(unsigned(way))) <= '1'; -- track which data was accessed last

            -- write new replacement data if write or read hit access occured
            wr_repl <= wr or (rd and hit_out);

            -- write to management data
            wr_arr <= (others => '0');
            wr_arr(to_integer(unsigned(way))) <= wr;

            hit_out <= hit;
        end process;

        repl_inst: entity work.repl
            generic map (
                WAYS => WAYS
            )
            port map (
                valid_in    => valid_arr,
                dirty_in    => dirty_arr,
                replace_in  => replace_between_arr,
                replace_out => replace_out_arr
            );
    end generate;

    ways_mgmt_gen: for i in ways_range generate
        mgmt_way_inst: entity work.mgmt_way
            port map (
                clk           => clk,
                res_n         => res_n,
                index         => index,
                wr            => wr_arr(i),
                wr_repl       => wr_repl,
                mgmt_info_in  => mgmt_info_in,
                replace_in    => replace_in_arr(i),
                mgmt_info_out => mgmt_info_arr(i),
                replace_out   => replace_between_arr(i)
            );

        valid_arr(i) <= mgmt_info_arr(i).valid;
        dirty_arr(i) <= mgmt_info_arr(i).dirty;

        match_arr(i) <= '1' when valid_arr(i) = '1' and mgmt_info_arr(i).tag = mgmt_info_in.tag else '0';
    end generate;
end architecture;
