library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.mem_pkg.all;
    use work.cache_pkg.all;

entity mgmt_way is
    port (
        clk           : in  std_logic;
        res_n         : in  std_logic;
        index         : in  index_t;
        wr            : in  std_logic;
        wr_repl       : in  std_logic;
        mgmt_info_in  : in  mgmt_info_t;
        replace_in    : in  std_logic;
        mgmt_info_out : out mgmt_info_t;
        replace_out   : out std_logic
    );
end entity;

architecture arch of mgmt_way is
    constant EMPTY_MGMT_INFO_ARR : mgmt_info_arr_t(sets_range) := (
        others => (
            valid => '0',
            dirty => '0',
            tag   => (others => '0')
        )
    );

    signal mgmt_info_reg : mgmt_info_arr_t(sets_range)  := EMPTY_MGMT_INFO_ARR;
    signal replace_info  : std_logic_vector(sets_range) := (others => '0');
begin
    mgmt_info_out <= mgmt_info_reg(to_integer(unsigned(index)));
    replace_out   <= replace_info(to_integer(unsigned(index)));

    process (clk, res_n) is
    begin
        if res_n /= '1' then
            mgmt_info_reg <= EMPTY_MGMT_INFO_ARR;
            replace_info <= (others => '0');
        elsif rising_edge(clk) then
            mgmt_info_reg <= mgmt_info_reg;
            replace_info <= replace_info;

            if wr then
                mgmt_info_reg(to_integer(unsigned(index))) <= mgmt_info_in;
            end if;

            if wr_repl then
                replace_info(to_integer(unsigned(index))) <= replace_in;
            end if;
        end if;
    end process;
end architecture;
