library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.rv_sys_pkg.all;

entity precompiled_rv_core is
	port (
		clk      : in std_logic;
		res_n    : in std_logic;
		imem_out : out mem_out_t;
		imem_in  : in mem_in_t;
		dmem_out : out mem_out_t;
		dmem_in  : in mem_in_t
	); 
end entity;

architecture arch of precompiled_rv_core is
	component rv_core_top is
		port (
			clk      : in std_logic;
			res_n    : in std_logic;
			imem_out : out mem_out_t;
			imem_in  : in mem_in_t;
			dmem_out : out mem_out_t;
			dmem_in  : in mem_in_t
		); 
	end component;
begin
	rv_core_top_inst : rv_core_top
	port map (
			clk      => clk,
			res_n    => res_n,
			imem_out => imem_out,
			imem_in  => imem_in,
			dmem_out => dmem_out,
			dmem_in  => dmem_in
	); 
end architecture;

