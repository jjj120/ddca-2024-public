library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.std_logic_misc.all;

entity repl is
    generic (
        WAYS : natural
    );
    port (
        valid_in    : in  std_logic_vector(0 to WAYS - 1);
        dirty_in    : in  std_logic_vector(0 to WAYS - 1);
        replace_in  : in  std_logic_vector(0 to WAYS - 1);
        replace_out : out std_logic_vector(0 to WAYS - 1)
    );
end entity;

architecture arch of repl is
begin
    fully_assosiative_gen: if WAYS = 1 generate
        -- no replacement policy needed
        replace_out <= "1";
    end generate;

    two_way_set_assosiative_gen: if WAYS = 2 generate
        process (all) is
            constant REPLACE_0 : std_logic_vector(0 to WAYS - 1) := "10";
            constant REPLACE_1 : std_logic_vector(0 to WAYS - 1) := "01";
        begin
            if not valid_in(0) then
                replace_out <= REPLACE_0;
            elsif not valid_in(1) then
                replace_out <= REPLACE_1;
            else
                -- both entries are valid
                replace_out <= not replace_in; -- in replace_in the newest entry is 1, so the one that is 0 should be replaced.
            end if;
        end process;
    end generate;

    four_way_set_assosiative_gen: if WAYS = 4 generate
        process (all) is
            constant REPLACE_0 : std_logic_vector(0 to WAYS - 1) := "1000";
            constant REPLACE_1 : std_logic_vector(0 to WAYS - 1) := "0100";
            constant REPLACE_2 : std_logic_vector(0 to WAYS - 1) := "0010";
            constant REPLACE_3 : std_logic_vector(0 to WAYS - 1) := "0001";
        begin
            if or_reduce(not valid_in) then
                -- at least one is not valid
                for i in valid_in'range loop
                    replace_out(i) <= or_reduce(std_logic_vector'(valid_in(0 to i - 1))) and not valid_in(i);
                end loop;
            else
                -- all entries valid
                if replace_in(0) or replace_in(1) then
                    if dirty_in(0) then
                        replace_out <= REPLACE_1;
                    else
                        -- no dirty replacement bit => replace arbitrary entry in group 1
                        replace_out <= REPLACE_0;
                    end if;
                else
                    if dirty_in(2) then
                        replace_out <= REPLACE_3;
                    else
                        -- no dirty replacement bit => replace arbitrary entry in group 2
                        replace_out <= REPLACE_2;
                    end if;
                end if;
            end if;
        end process;
    end generate;

    gen_failure: if WAYS /= 1 and WAYS /= 2 and WAYS /= 4 generate
        failure_proc: process (all) is
        begin
            report "WAYS NUMBER INVALID!" severity failure;
        end process;
    end generate;
end architecture;
