
# Project Task - Cache
**Points:** 3 ` | ` **Keywords:** Project Task, Cache

In modern processor implementations, CPU cores are usually much faster than the corresponding main memory.
This discrepancy in performance requires the processor to be stalled for many cycles for each main memory access.
A common strategy to cope with this performance gap is caching.
Caching provides a fast but small memory that holds copies of memory areas of the main memory that are often accessed by the processor.
This enables the processor to perform fast accesses on heavily used locations while having a huge amount of cheap main memory available.

For a refresher on caches be referred to the material of the computer architecture lecture and the excellent book *Computer Organization and Design* by David Patterson and John Hennessy.
The book is available in the [Catalog Plus](https://catalogplus.tuwien.at/permalink/f/8j3js/UTW_alma51138543780003336).

The goal of this project task is to implement a cache for the data memory that can be used with the RV core(s) you implemented in previous tasks.

Your cache should

- support one (direct-mapped), two and four ways (set-associative)
- write-around on miss, i.e., if there is a write to a memory location that is currently not in the cache (write miss), the cache should be bypassed and the write operation directly performed on the main memory (without transferring the data to the cache first).
- write-back on hit, i.e., if there is a write to a cached memory location, only the cache entry is updated.
The respective location in the main memory will only be updated when the cache entry is evicted.

## Interface
The `cache` module shall provide the following interface:

```vhdl
entity cache is
	generic (
		ADDR_MASK : mem_address_t := (others => '1')
	);
	port (
		clk          : in std_logic;
		res_n        : in std_logic;
		cpu_to_cache : in  mem_out_t;
		cache_to_cpu : out mem_in_t;
		cache_to_mem : out mem_out_t;
		mem_to_cache : in  mem_in_t
	);
end entity;
```

The signals `clk` and `res_n` are the usual clock and active-low reset signals.

The `cpu_to_cache` and `cache_to_cpu` signals are used to connect a RV core's data memory interface to the cache.
The `cache_to_mem` and `mem_to_cache` signals are the interface between the cache and a connected memory.
The respective types are the ones defined in [rv_sys_pkg](../../../lib/rv_sys/src/rv_sys_pkg.vhd).

The generic `ADDR_MASK` allows configuring an address range where memory accesses made by the core bypass the cached and hence, are directly forwarded to the memory.
This is necessary when accessing devices like memory-mapped peripherals, like the UART or the GPIOs.
If *any* bit of the address applied at `cpu_to_cache` is set, which is **not** set in `ADDR_MASK`, the access shall bypass the cache.
To bypass accesses to the devices featured in [rv_sys](../../../lib/rv_sys/doc.md), set `ADDR_MASK` to `14x0FFF`.

To configure the cache, you have to set the `WAYS` and `SETS` constants in the [cache_pkg](./src/cache_pkg.vhd).
Note that this is not a good design style, but unfortunately necessary in our case.
The reason is that we want to use the different types defined in the package throughout the whole hierarchy, but the types depend on the `WAYS` and `SETS` constants.
While there is a clean way to deal with this very problem in the form of VHDL-2008's generic packages, Quartus does, unfortunately, not support the use of this feature in its Lite and Standard versions.
An alternative approach to deal with this scenario is to use basic types, like `std_logic_vector`, instead of the used subtypes and records.
As this would make the ports less concise and reduce the general readability, we opted for not making the cache generic with respect to the ways and sets.
The major drawback of this approach is that you can only instantiate a single cache configuration at a time.
However, this restriction is not relevant for this course.


## Internal Interface
Internally, the cache can be considered to consist of three major parts.

- The *cache controller* is an FSM that is responsible for handling accesses to the cache by the core and accessing the main memory.
- The *cache management information* is used provide the cache controller with information about the cache's state.
- The *cache data* is the memory used for actually caching the data.

### Cache Controller
As stated above, the cache controller shall be an FSM.
It has to deal with reads from and writes to the data memory issued by the connected processor core.

#### Memory Read
Whenever the processor reads from the data memory, without being bypassed, the controller checks if the corresponding data can be found in the cache.
In case it can (read hit), the cached data will be returned.
Note that the cache controller is only allowed to raise the `busy` flag of `cache_to_cpu` for **at most** one clock cycle for a read hit.

In case the corresponding data is currently not in the cache (read miss), the controller fetches the data from the connected memory and stores it at the corresponding entry in the cache.
Furthermore, the fetched data is provided to the processor.

#### Memory Write
When the processor writes to the data memory (without bypassing the cache), and the respective data is currently cached (write hit), **only** update the cache entry and mark it as *dirty*.
Note that in case of a write hit, writing to the cache must be handled immediately (i.e., `busy` must not be raised).

If the write destination is not cached (write miss), the cache controller shall directly write to the connected memory.
The cached data shall be left unchanged by a write miss (i.e., **no** fetch on write).

---

We recommend you to implement your FSM with the states listed below.
However, you are not required to use this structure, if you want to implement it differently.

- `IDLE`: No memory operation is on-going
- `READ_CACHE`: Read access to the cache
- `READ_MEM_START`: First cycle of a memory read
- `READ_MEM`: Wait for the memory read to finish and store the result in the cache
- `WRITE_BACK_START`: First cycle of a write back to the memory (upon eviction of a dirty cache entry)
- `WRITE_BACK`: Finishing the write operation

### Cache Management Information
The cache controller requires management information in order to work correctly.
In more detail, the cache controller needs to know if an entry is valid, dirty or selected for replacement (in case of a read miss on a filled cache) and also its tag.
Furthermore, the cache management shall also determine if a memory access is a hit.

We already provide you with two template files for the cache management ([mgmt_cache.vhd](./src/mgmt_cache.vhd) and [mgmt_way.vhd](./src/mgmt_way.vhd)).

The purpose of the `mgmt_cache` entity is to facilitate accesses to the cache management information (for all ways).

It implements the following interface:

```vhdl
entity mgmt_cache is
	port (
		clk           : in std_logic;
		res_n         : in std_logic;
		index         : in index_t;
		rd            : in std_logic;
		mgmt_info_in  : in mgmt_info_t;
		wr            : in std_logic;
		mgmt_info_out : out mgmt_info_t;
		way_out       : out way_t;
		hit_out       : out std_logic
	);
end entity;
```

The signal `index` is the index of the set to be accessed and `wr` / `rd` are asserted when writing to / reading from the management information.
Your entity shall search for entries in all ways and check if an access is a hit.
If it is a hit, `hit_out` is asserted and `way_out` is set to the way where the hit occurred.
If an access is not a hit, and a cache entry needs to get evicted, `way_out` shall be set to the selected way.
The `mgmt_info_{in,out}` signals provide the management information to be written (`_in`), or the one returned by a read (`_out`), respectively.
The respective type, `mgmt_info_t` can be found in [cache_pkg](./src/cache_pkg.vhd).

The `mgmt_way` module mainly acts as a storage for saving / accessing the management information of a single way.
Implement this storage using registers, as it is required that the stored management information can be reset.
Otherwise, previously cached values might be used, although the processor was reset.

The interface of this module is mostly the same as the one of `mgmt_cache`, except that it also contains the replacement information (see below).

#### Replacement
In case of a read miss, where there is no free cache entry available for the new data to be requested, information about which entry needs to be replaced is necessary.
As a strategy to cope with this, implement the least-recently-used (LRU) policy, which has proven as very efficient in practical applications.

Implement this functionality in [repl.vhd](./src/repl.vhd), featuring the following interface:

```vhdl
entity repl is
	generic (
		WAYS : natural
	);
	port (
		valid_in    : in std_logic_vector(WAYS-1 downto 0);
		dirty_in    : in std_logic_vector(WAYS-1 downto 0);
		replace_in  : in std_logic_vector(WAYS-1 downto 0);
		replace_out : out std_logic_vector(WAYS-1 downto 0)
	);
end entity;
```

The signals `valid_in`, `dirty_in` and `replace_in` are the valid, dirty and current replacement information of all ways of the currently selected set, while `replace_out` is the new replacement information for all ways of this set.

For a 2-way set associative cache, a potential implementation could be setting the replacement bit accordingly when accessing an entry.
Start by using invalid entries (use the lowest way ID in case multiple invalid entries are present) to avoid eviction of valid entries.
If only valid entries are present, evict the one which has not been used for the longest time.

Implementing the complete LRU scheme for more than two ways is complicated and requires more management information.
Therefore, a simplified strategy should be used for the 4-way cache.
For this, the four ways are partitioned into two groups (grouping ways 0 & 1 and ways 2 & 3).
As a first priority, invalid entries should be used (irrespective of the group) to avoid eviction of valid entries.
If only valid entries are present, evict an entry from the group which has not been used for the longest time.
Within this group, evicting dirty entries should be avoided.

---

**Note**: The meaning of reading and writing for the management information differs from read and write accesses to the connected memory.
For example, when writing to the memory (store), it is first required to check if the corresponding entry is present and valid in the cache (i.e., read access to the management information).
Afterwards, in case of a write hit, the management information might have to be updated (dirty flag).
Similarly, a read to the memory system (load) requires finding out if the corresponding entry is present and valid in the cache and might require writing to the management information in case an entry has to be evicted.

### Cache Data
Lastly, the cache also needs a data storage to actually store the cached data.
Similar to the structure for the cache management information, implement this in two entities (skeletons are provided in [data_cache.vhd](./src/data_cache.vhd) and [data_way.vhd](./src/data_way.vhd)).

The `data_cache` module facilitates accesses to the data storage of the different cache ways.
It features the following interface:

```vhdl
entity data_cache is
	port (
		clk      : in std_logic;
		wr       : in std_logic;
		way      : in way_t;
		index    : in index_t;
		byteena  : in mem_byteena_t;
		data_in  : in mem_data_t;
		data_out : out mem_data_t
	);
end entity;
```

If `wr` is asserted, `data_in` shall be written to the set specified by `index` of `way`.
The signal `data_out` returns the data stored at set `index` of `way`.
The `byteena` (byte enable) signal is used for sub-word writes.
If bit *i* of `byteena` is asserted, the byte *i* of input data `data_in` shall be written to byte *i* of the indexed set.
Make sure that the other bytes, i.e. the ones where the respective bit in `byteena` is not set, remain untouched by a write operation.

The `data_way` module encapsulates the data storage required for the cache entries of one way.
Its interface is identical to the one of `data_cache`, except for the absence of `way`.

**Important**: As the storage required for the data is typically larger than the one required for the management information, instantiate memory blocks for the data storage.
Use the provided `dp_ram_1c1r1w_rdw` RAM implementation of the [mem_pkg](../../../lib/mem/doc.md), which is a RAM featuring new-data-read-during-write behavior.

## Testbench
Simulate your cache together with an RV core of your choice (in case you implemented several), as described in the [rv_sys](../../../lib/rv_sys/doc.md) documentation.
Hence, add an instance of a RV core as well as a cache instance to the testbench template.
However, make sure to set the `DMEM_DELAY` constant of `rv_sys` to some positive value other than 0 (or test multiple ones) in order to simulate a slower data memory.

In order to test if your cache works, simulate runs of different programs on your systems and check if they behave as expected (pick ones featuring UART output).
However, since it might be hard to identify specific events (read / write miss / hit) in your cache, you might want to create some simple test programs, or a dedicated testbench for your cache, that leads to the desired event.

**Note**: There appears to be a problem with GHDL dumping signals when entities are instantiated inside generate statements.
However, this does not affect the `gsim` target, but only the `gsim_gui` one.
Furthermore, it should be possible to dump signals using GHDL when using a file that explicitly lists the signals to be dumped (which must not contain a signal from an entity not instantiated).
However, as this is rather inconvenient, we recommend you to use QuestaSim (i.e., the `sim_gui` target) instead.

## Hardware Test
Use the provided top-level architecture, [top_arch_cache](top_arch_cache.vhd) and synthesize the RV system.
Note that we provide you with a pre-compiled core (used per default) in case you only implemented `rv_sim`.
If you want to use one of your own cores, feel free to replace it in the top architecture though.

Then, download the bitstream to the FPGA and run some of the provided C programs.
Observe the UART output to see if they execute correctly with the cache in place.

**Important**:
- For both the testbench and the hardware test, you should test different cache configurations.
Try the direct-mapped, 2-way and 4-way set-associative caches with different amounts of sets.
- The top architecture we provide you with uses a PLL to clock the cache and the RV core with 70 MHz.
If you observe timing requirements not being met (especially with the `rv_fsm`), consider using the pre-compiled core or your implementation of `rv_pl`.
- If you still want to experience the success of having developed the complete system yourselves, you can use the 50 MHz `clk` in combination with `rv_fsm` in a distinct top architecture.
However, note that we will base the grading on your cache being able to run at 70 MHz.

## Evaluation
In total, you can achieve up to 12.5 points on this project task.
The following table coarsely lists how the different parts of the task contribute to the overall score:

|    Functionality      | Points |
| --------------------- | ------ |
| Direct-mapped cache   | 6.5   |
| 2-way set associative cache | 3  |
| 4-way set associative cache | 3  |

However, note that in order to achieve **any** points at all, your design has to work on hardware and must not produce any of the disallowed warnings (this is especially true for latches)!
In case you feel like your design produces a warning that does not hint at a problem with your code, ask in the TU chat.

Note that we will deduce points based on errors in your implementation
The amount depends on the severity and the effort required to fix them.

In order to get the skill points for the Project Task at least the direct-mapped cache has to work.

### Further Allowed Warnings
During this task you might encounter some warnings, as a result of rigorously using generics, that are not on the list of allowed ones in the [designflow tutorial](../../level0/designflow.md).
The following ones are alright for the design units of **the cache**:

|ID| Description  |
|--|--------------|
|10445|VHDL Subtype or Type Declaration warning at [...]: subtype or type has null range|
|10296|VHDL warning at [...]: ignored assignment of value to null range|

## Submission
Use the `submission` target of the [Makefile](Makefile) to create a submission archive containing the required files.
The target will first check for the existence of some expected files and report an error if something is missing.
If everything worked correctly, a `submission.tar.gz` archive will have been created.
Upload this archive to the respective TUWEL submission activity.

---

**Hints**:

- The [cache.vhd](./src/cache.vhd) file contains a `bypass` architecture that simply connects the core and data memory signals without interfering with it.
You can use this to easily disable your cache during testing.
- Use `for generate` constructs for instantiations or operations performed on all ways in order to mitigate code duplication (also making it generic for different cache configurations).
- You can use `or_reduce` to combine signals you have to generate for every way (e.g., to check whether there was a hit in *any* way).
- Instantiate `mgmt_way` and `data_way` multiple times for the set-associative cache configurations.
- The cache shall adhere to the same protocol as the data memory, i.e., from the core's point of view it should not be possible to distinguish between cache and data memory (except for the shorter access times of course)
