library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.math_real.all;
    use ieee.std_logic_textio.all;

library std; -- for Printing
    use std.env.all;
    use std.textio.all;

    use work.rv_sys_pkg.all;
    use work.tb_util_pkg.all;

entity pl_cache_tb_small is
    generic (
        ELF_FILE         : string;
        SIM_STOP_TIME_US : integer
    );
end entity;

architecture arch of pl_cache_tb_small is
    constant CLK_PERIOD          : time    := 20 ns;
    constant MEMORY_DELAY_CYCLES : integer := 10;

    signal clk, res_n   : std_logic;
    signal cpu_to_cache : mem_out_t := MEM_OUT_NOP;
    signal cache_to_cpu : mem_in_t  := MEM_IN_NOP;
    signal cache_to_mem : mem_out_t := MEM_OUT_NOP;
    signal mem_to_cache : mem_in_t  := MEM_IN_NOP;

    procedure print_line(s : string) is
    begin
        report s;
    end procedure;

begin
    cache_mem_inst: entity work.cache(arch) generic map (
        ADDR_MASK => 14x"0FFF"
    ) port map (
        clk          => clk,
        res_n        => res_n,
        cpu_to_cache => cpu_to_cache,
        cache_to_cpu => cache_to_cpu,
        cache_to_mem => cache_to_mem,
        mem_to_cache => mem_to_cache
    );

    mem_reply: process is
        variable data : mem_data_array_t(2 ** 14 - 1 downto 0) := (others => (others => '0'));

        variable seed1, seed2 : integer := 999;
        impure function rand_slv(len : integer) return std_logic_vector is
            variable r   : real;
            variable slv : std_logic_vector(len - 1 downto 0);
        begin
            for i in slv'range loop
                uniform(seed1, seed2, r);
                slv(i) := '1' when r > 0.5 else '0';
            end loop;
            return slv;
        end function;

        procedure write_to_register(addr : mem_address_t; input_data : mem_data_t; byteena : mem_byteena_t) is
        begin
            if byteena(3) then
                data(to_integer(unsigned(addr)))(31 downto 24) := input_data(31 downto 24);
            end if;
            if byteena(2) then
                data(to_integer(unsigned(addr)))(23 downto 16) := input_data(23 downto 16);
            end if;
            if byteena(1) then
                data(to_integer(unsigned(addr)))(15 downto 8) := input_data(15 downto 8);
            end if;
            if byteena(0) then
                data(to_integer(unsigned(addr)))(7 downto 0) := input_data(7 downto 0);
            end if;
        end procedure;

    begin
        wait until res_n;

        for i in data'range loop
            data(i) := std_logic_vector(to_unsigned(i, RV_SYS_DATA_WIDTH));
        end loop;

        loop

            if cache_to_mem.rd then
                mem_to_cache.busy <= '1';
                wait for MEMORY_DELAY_CYCLES * CLK_PERIOD;
                mem_to_cache.busy <= '0';
                mem_to_cache.rddata <= data(to_integer(unsigned(cache_to_mem.address)));
            end if;

            if cache_to_mem.wr then
                mem_to_cache.busy <= '1';
                write_to_register(cache_to_mem.address, cache_to_mem.wrdata, cache_to_mem.byteena);
                wait for MEMORY_DELAY_CYCLES / 2 * CLK_PERIOD;
                mem_to_cache.busy <= '0';
            end if;

            -- wait for CLK_PERIOD;
            wait until rising_edge(clk);
        end loop;
    end process;

    main: process is
        procedure request_read(addr : mem_address_t) is
        begin
            cpu_to_cache.rd <= '1';
            cpu_to_cache.wr <= '0';
            cpu_to_cache.address <= addr;
            wait for CLK_PERIOD;
            cpu_to_cache.rd <= '0';
            cpu_to_cache.wr <= '0';

            while cache_to_cpu.busy loop
                wait for CLK_PERIOD;
            end loop;
            wait for 0 ns;
            report "Read: 0x" & to_hstring(cache_to_cpu.rddata) & " from 0x" & to_hstring(addr);
        end procedure;

        procedure request_write(addr : mem_address_t; data : mem_data_t; byteena : mem_byteena_t) is
        begin
            cpu_to_cache.rd <= '0';
            cpu_to_cache.wr <= '1';
            cpu_to_cache.address <= addr;
            cpu_to_cache.wrdata <= data;
            cpu_to_cache.byteena <= byteena;
            report "Write 0x" & to_hstring(data) & " to 0x" & to_hstring(addr) & " with byteena " & to_string(byteena);

            wait for CLK_PERIOD;
            cpu_to_cache.rd <= '0';
            cpu_to_cache.wr <= '0';

            while cache_to_cpu.busy loop
                wait for CLK_PERIOD;
            end loop;
            wait for 0 ns;
        end procedure;

        procedure check_output(data_ref : mem_data_t) is
        begin
            assert cache_to_cpu.rddata = data_ref report to_hstring(cache_to_cpu.rddata) & " should be " & to_hstring(data_ref) severity error;
        end procedure;
    begin
        res_n <= '0';
        wait until rising_edge(clk);
        res_n <= '1';
        wait until rising_edge(clk);
        -- wait for 5 ns;
        request_read((others => '0'));
        check_output((others => '0'));

        request_read((others => '0'));
        check_output((others => '0'));

        request_read(14x"123");
        check_output(32x"123");

        request_read(14x"123");
        check_output(32x"123");

        request_write(14x"110", 32x"0", "1111");
        request_write(14x"110", 32x"12345678", "1100");
        request_read(14x"110");
        check_output(32x"12340000");

        request_write(14x"110", 32x"12345678", "0011");
        request_read(14x"110");

        request_read(14x"210");
        check_output(32x"210");

        request_read(14x"310");
        check_output(32x"310");

        request_read(14x"410");
        check_output(32x"410");

        request_read(14x"510");
        check_output(32x"510");

        request_read(14x"610"); -- writeback should occure
        check_output(32x"610");

        request_read(14x"110");
        check_output(32x"12345678");

        -- wait for SIM_STOP_TIME_US * 1 us;
        print_line("Simulation done");

        std.env.stop;
    end process;

    clk_gen: process is
    begin
        clk <= '1';
        wait for CLK_PERIOD / 2;
        clk <= '0';
        wait for CLK_PERIOD / 2;
    end process;
end architecture;
