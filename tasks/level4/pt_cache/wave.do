onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /pl_cache_tb/clk
add wave -noupdate /pl_cache_tb/res_n
add wave -noupdate /pl_cache_tb/rv_pl_inst/rv_pl_ctrl_inst/stall
add wave -noupdate /pl_cache_tb/rv_pl_inst/rv_pl_decode_inst/instr_test_string
add wave -noupdate -divider {IMEM Cache}
add wave -noupdate -color Blue /pl_cache_tb/cache_imem_inst/update_input_registers
add wave -noupdate -color Blue -expand -subitemconfig {/pl_cache_tb/cache_imem_inst/cpu_to_cache.address {-color Blue -height 16} /pl_cache_tb/cache_imem_inst/cpu_to_cache.rd {-color Blue -height 16} /pl_cache_tb/cache_imem_inst/cpu_to_cache.wr {-color Blue -height 16} /pl_cache_tb/cache_imem_inst/cpu_to_cache.byteena {-color Blue -height 16} /pl_cache_tb/cache_imem_inst/cpu_to_cache.wrdata {-color Blue -height 16}} /pl_cache_tb/cache_imem_inst/cpu_to_cache
add wave -noupdate -color Blue -expand -subitemconfig {/pl_cache_tb/cache_imem_inst/cache_to_cpu.busy {-color Blue -height 16} /pl_cache_tb/cache_imem_inst/cache_to_cpu.rddata {-color Blue -height 16}} /pl_cache_tb/cache_imem_inst/cache_to_cpu
add wave -noupdate -color Blue /pl_cache_tb/cache_imem_inst/cache_to_mem
add wave -noupdate -color Blue /pl_cache_tb/cache_imem_inst/mem_to_cache
add wave -noupdate -color Blue /pl_cache_tb/cache_imem_inst/state
add wave -noupdate -color Blue -expand -subitemconfig {/pl_cache_tb/cache_imem_inst/data.index {-color Blue -height 16} /pl_cache_tb/cache_imem_inst/data.wr {-color Blue -height 16} /pl_cache_tb/cache_imem_inst/data.byteena {-color Blue -height 16} /pl_cache_tb/cache_imem_inst/data.data_in {-color Blue -height 16} /pl_cache_tb/cache_imem_inst/data.data_out {-color Blue -height 16}} /pl_cache_tb/cache_imem_inst/data
add wave -noupdate -color Blue -expand -subitemconfig {/pl_cache_tb/cache_imem_inst/management.index {-color Blue -height 16} /pl_cache_tb/cache_imem_inst/management.rd {-color Blue -height 16} /pl_cache_tb/cache_imem_inst/management.mgmt_info_in {-color Blue -height 16} /pl_cache_tb/cache_imem_inst/management.wr {-color Blue -height 16} /pl_cache_tb/cache_imem_inst/management.mgmt_info_out {-color Blue -height 16} /pl_cache_tb/cache_imem_inst/management.way_out {-color Blue -height 16} /pl_cache_tb/cache_imem_inst/management.hit_out {-color Blue -height 16}} /pl_cache_tb/cache_imem_inst/management
add wave -noupdate -color Blue -expand -subitemconfig {/pl_cache_tb/cache_imem_inst/cpu_to_cache_register.address {-color Blue} /pl_cache_tb/cache_imem_inst/cpu_to_cache_register.rd {-color Blue} /pl_cache_tb/cache_imem_inst/cpu_to_cache_register.wr {-color Blue} /pl_cache_tb/cache_imem_inst/cpu_to_cache_register.byteena {-color Blue} /pl_cache_tb/cache_imem_inst/cpu_to_cache_register.wrdata {-color Blue}} /pl_cache_tb/cache_imem_inst/cpu_to_cache_register
add wave -noupdate -divider {DMEM Cache}
add wave -noupdate -color Firebrick /pl_cache_tb/cache_dmem_inst/update_input_registers
add wave -noupdate -color Firebrick -expand -subitemconfig {/pl_cache_tb/cache_dmem_inst/cpu_to_cache.address {-color Firebrick -height 16} /pl_cache_tb/cache_dmem_inst/cpu_to_cache.rd {-color Firebrick -height 16} /pl_cache_tb/cache_dmem_inst/cpu_to_cache.wr {-color Firebrick -height 16} /pl_cache_tb/cache_dmem_inst/cpu_to_cache.byteena {-color Firebrick -height 16} /pl_cache_tb/cache_dmem_inst/cpu_to_cache.wrdata {-color Firebrick -height 16}} /pl_cache_tb/cache_dmem_inst/cpu_to_cache
add wave -noupdate -color Firebrick -expand -subitemconfig {/pl_cache_tb/cache_dmem_inst/cache_to_cpu.busy {-color Firebrick -height 16} /pl_cache_tb/cache_dmem_inst/cache_to_cpu.rddata {-color Firebrick -height 16}} /pl_cache_tb/cache_dmem_inst/cache_to_cpu
add wave -noupdate -color Firebrick /pl_cache_tb/cache_dmem_inst/cache_to_mem
add wave -noupdate -color Firebrick /pl_cache_tb/cache_dmem_inst/mem_to_cache
add wave -noupdate -color Firebrick /pl_cache_tb/cache_dmem_inst/state
add wave -noupdate -color Firebrick -expand -subitemconfig {/pl_cache_tb/cache_dmem_inst/data.index {-color Firebrick -height 16} /pl_cache_tb/cache_dmem_inst/data.wr {-color Firebrick -height 16} /pl_cache_tb/cache_dmem_inst/data.byteena {-color Firebrick -height 16} /pl_cache_tb/cache_dmem_inst/data.data_in {-color Firebrick -height 16} /pl_cache_tb/cache_dmem_inst/data.data_out {-color Firebrick -height 16}} /pl_cache_tb/cache_dmem_inst/data
add wave -noupdate -color Firebrick -expand -subitemconfig {/pl_cache_tb/cache_dmem_inst/management.index {-color Firebrick -height 16} /pl_cache_tb/cache_dmem_inst/management.rd {-color Firebrick -height 16} /pl_cache_tb/cache_dmem_inst/management.mgmt_info_in {-color Firebrick -height 16} /pl_cache_tb/cache_dmem_inst/management.wr {-color Firebrick -height 16} /pl_cache_tb/cache_dmem_inst/management.mgmt_info_out {-color Firebrick -height 16} /pl_cache_tb/cache_dmem_inst/management.way_out {-color Firebrick -height 16} /pl_cache_tb/cache_dmem_inst/management.hit_out {-color Firebrick -height 16}} /pl_cache_tb/cache_dmem_inst/management
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {190 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 183
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {982 ns}
