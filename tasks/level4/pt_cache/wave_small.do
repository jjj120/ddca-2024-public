onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /pl_cache_tb_small/clk
add wave -noupdate /pl_cache_tb_small/res_n
add wave -noupdate -expand /pl_cache_tb_small/cache_to_cpu
add wave -noupdate /pl_cache_tb_small/cache_mem_inst/state
add wave -noupdate -expand /pl_cache_tb_small/cpu_to_cache
add wave -noupdate /pl_cache_tb_small/cache_mem_inst/output_reg
add wave -noupdate -divider -height 36 internal
add wave -noupdate /pl_cache_tb_small/cache_mem_inst/data
add wave -noupdate /pl_cache_tb_small/cache_mem_inst/management
add wave -noupdate -divider -height 36 others
add wave -noupdate -expand /pl_cache_tb_small/cache_to_mem
add wave -noupdate -expand /pl_cache_tb_small/mem_to_cache
add wave -noupdate -expand /pl_cache_tb_small/cache_mem_inst/cpu_to_cache_register
add wave -noupdate /pl_cache_tb_small/cache_mem_inst/update_input_registers
add wave -noupdate -divider -height 36 management
add wave -noupdate /pl_cache_tb_small/cache_mem_inst/mgmt_cache_inst/index
add wave -noupdate /pl_cache_tb_small/cache_mem_inst/mgmt_cache_inst/way_out
add wave -noupdate /pl_cache_tb_small/cache_mem_inst/mgmt_cache_inst/hit_out
add wave -noupdate /pl_cache_tb_small/cache_mem_inst/mgmt_cache_inst/wr_arr
add wave -noupdate /pl_cache_tb_small/cache_mem_inst/mgmt_cache_inst/mgmt_info_in
add wave -noupdate /pl_cache_tb_small/cache_mem_inst/mgmt_cache_inst/mgmt_info_out
add wave -noupdate -divider {management internal}
add wave -noupdate -expand /pl_cache_tb_small/cache_mem_inst/mgmt_cache_inst/mgmt_info_arr
add wave -noupdate -radix binary /pl_cache_tb_small/cache_mem_inst/mgmt_cache_inst/wr_repl_arr
add wave -noupdate -radix binary /pl_cache_tb_small/cache_mem_inst/mgmt_cache_inst/valid_arr
add wave -noupdate -radix binary /pl_cache_tb_small/cache_mem_inst/mgmt_cache_inst/dirty_arr
add wave -noupdate -radix binary /pl_cache_tb_small/cache_mem_inst/mgmt_cache_inst/replace_out_arr
add wave -noupdate -radix binary /pl_cache_tb_small/cache_mem_inst/mgmt_cache_inst/replace_between_arr
add wave -noupdate -radix binary /pl_cache_tb_small/cache_mem_inst/mgmt_cache_inst/replace_in_arr
add wave -noupdate -divider repl
add wave -noupdate -radix binary /pl_cache_tb_small/cache_mem_inst/mgmt_cache_inst/repl_inst/valid_in
add wave -noupdate -radix binary /pl_cache_tb_small/cache_mem_inst/mgmt_cache_inst/repl_inst/dirty_in
add wave -noupdate /pl_cache_tb_small/cache_mem_inst/mgmt_cache_inst/repl_inst/replace_in
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1838 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 135
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {9 ns} {3563 ns}
