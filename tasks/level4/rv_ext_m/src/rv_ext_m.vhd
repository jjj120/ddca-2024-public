library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

library lpm;
    use lpm.lpm_components.all;

    use work.math_pkg.all;
    use work.rv_ext_m_pkg.all;
    use work.rv_core_pkg.all;

entity rv_ext_m is
    generic (
        DATA_WIDTH : natural := 32
    );
    port (
        clk   : in  std_logic;
        res_n : in  std_logic;
        A, B  : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
        R     : out std_logic_vector(DATA_WIDTH - 1 downto 0);
        op    : in  ext_m_op_t;
        start : in  std_logic;
        busy  : out std_logic
    );
end entity;

architecture arch of rv_ext_m is
    constant DIV_PL_DEPTH : integer                                   := DATA_WIDTH;
    constant MINUS_ONE    : std_logic_vector(DATA_WIDTH - 1 downto 0) := (others => '1');
    constant MIN_VALUE    : std_logic_vector(DATA_WIDTH - 1 downto 0) := '1' & (DATA_WIDTH - 2 downto 0 => '0');
    constant ZERO         : std_logic_vector(DATA_WIDTH - 1 downto 0) := (others => '0');

    signal div_res, div_rem : std_logic_vector(DATA_WIDTH - 1 downto 0);
    signal mul_res          : std_logic_vector(2 * DATA_WIDTH - 1 downto 0);
    signal opA, opB         : std_logic_vector(DATA_WIDTH - 1 downto 0);

    type fsm_state_t is (IDLE, WAIT_DIV);
    type state_t is record
        state        : fsm_state_t;
        prev_op      : ext_m_op_t;
        wait_counter : integer;
    end record;

    constant DEFAULT_STATE : state_t := (
        state        => IDLE,
        prev_op      => M_MUL,
        wait_counter => 0
    );

    signal state, next_state           : state_t                   := DEFAULT_STATE;
    signal output_reg, next_output_reg : std_logic_vector(R'range) := ZERO;
    signal bypass_output_reg           : std_logic                 := '0';
begin
    R <= next_output_reg when bypass_output_reg else output_reg;

    /*
	Possible OPs:
		M_MUL: 	  Multiply, 							return lower 32 Bits
		M_MULH:	  Multiply signed, 						return upper 32 Bits
		M_MULHU:  Multiply unsigned, 					return upper 32 Bits
		M_MULHSU: Multiply, rs1 unsigned, rs2 signed, 	return upper 32 Bits
		M_DIV: 	  Divide signed, 						rs1/rs2
		M_DIVU:   Divide unsigned, 						rs1/rs2
		M_REM:	  Remainder of signed division, 		rs1/rs2
		M_REMU:	  Remainder of unsigned division, 		rs1/rs2
	*/

    comb_logic: process (all) is
        pure function twos_complement(num : std_logic_vector) return std_logic_vector is
        begin
            return std_logic_vector(unsigned(not num) + 1);
        end function;

        pure function absolute(num : std_logic_vector) return std_logic_vector is
        begin
            if num(num'high) then
                return twos_complement(num);
            else
                return num;
            end if;
        end function;
    begin
        next_state <= state;
        bypass_output_reg <= '0';
        next_output_reg <= ZERO;
        opA <= A;
        opB <= B;

        busy <= start when state.state = IDLE else '1';

        case state.state is
            when IDLE =>
                next_state.wait_counter <= 0;
                next_state.prev_op <= op;

                case op is
                    when M_MUL =>
                        bypass_output_reg <= start;
                        next_output_reg <= mul_res(DATA_WIDTH - 1 downto 0);

                    when M_MULH =>
                        opA <= absolute(A);
                        opB <= absolute(B);
                        if A(DATA_WIDTH - 1) xor B(DATA_WIDTH - 1) then
                            -- result is negative
                            next_output_reg <= twos_complement(mul_res)(2 * DATA_WIDTH - 1 downto DATA_WIDTH);
                        else
                            -- result is positive
                            next_output_reg <= mul_res(2 * DATA_WIDTH - 1 downto DATA_WIDTH);
                        end if;
                        -- next_output_reg <= mul_res(2 * DATA_WIDTH - 1 downto DATA_WIDTH);
                        bypass_output_reg <= start;

                    when M_MULHU =>
                        -- operands remain
                        opA <= A;
                        opB <= B;
                        bypass_output_reg <= start;
                        next_output_reg <= mul_res(2 * DATA_WIDTH - 1 downto DATA_WIDTH);

                    when M_MULHSU =>
                        opA <= absolute(A); -- only convert signed to its absolute value
                        bypass_output_reg <= start;
                        if A(DATA_WIDTH - 1) then
                            -- result is negative
                            next_output_reg <= twos_complement(mul_res)(2 * DATA_WIDTH - 1 downto DATA_WIDTH);
                        else
                            next_output_reg <= mul_res(2 * DATA_WIDTH - 1 downto DATA_WIDTH);
                        end if;

                    when M_DIV | M_DIVU | M_REM | M_REMU =>
                        if op = M_DIV or op = M_REM then
                            opA <= absolute(A);
                            opB <= absolute(B);
                        end if;

                        if start then
                            next_state.state <= WAIT_DIV;
                            next_state.wait_counter <= 0;
                        end if;

                        -- Handle special divide cases
                        if (A = MIN_VALUE and B = MINUS_ONE) and op = M_DIV then
                            -- overflow
                            next_state.state <= IDLE;
                            bypass_output_reg <= '1';
                            next_output_reg <= MIN_VALUE;
                        elsif (A = MIN_VALUE and B = MINUS_ONE) and op = M_REM then
                            -- overflow
                            opB <= (0 => '1', others => '0'); -- so the div core does not divide by 0
                            next_state.state <= IDLE;
                            bypass_output_reg <= '1';
                            next_output_reg <= ZERO;
                        elsif B = ZERO then
                            -- divide by 0
                            opB <= (0 => '1', others => '0'); -- so the div core does not divide by 0
                            next_state.state <= IDLE;
                            bypass_output_reg <= '1';

                            case op is
                                when M_DIV | M_DIVU => next_output_reg <= MINUS_ONE;
                                when M_REM | M_REMU => next_output_reg <= (others => 'X');
                                when others => report "unknown div op" severity failure;
                            end case;
                        end if;

                    when others =>
                        report "Unknown op in idle state" severity failure;
                end case;

            when WAIT_DIV =>
                next_state.wait_counter <= state.wait_counter + 1;
                if state.wait_counter = DIV_PL_DEPTH - 1 then
                    bypass_output_reg <= '1';

                    case state.prev_op is
                        when M_DIV =>
                            if A(DATA_WIDTH - 1) xor B(DATA_WIDTH - 1) then
                                -- result is negative
                                next_output_reg <= twos_complement(div_res);
                            else
                                -- result is positive
                                next_output_reg <= div_res;
                            end if;

                        when M_DIVU =>
                            -- unsigned operation, no need to change result
                            next_output_reg <= div_res;
                        when M_REM =>
                            if A(DATA_WIDTH - 1) = '1' then
                                -- result is negative
                                next_output_reg <= twos_complement(div_rem);
                            else
                                -- result is positive
                                next_output_reg <= div_rem;
                            end if;
                        when M_REMU =>
                            -- unsigned operation, no need to change remainder
                            next_output_reg <= div_rem;
                        when others =>
                            report "Unkown op in wait div state" severity failure;
                    end case;

                    next_state <= DEFAULT_STATE;
                end if;

            when others =>
                report "Unknown fsm state!" severity failure;
        end case;
    end process;

    registers: process (clk, res_n) is
    begin
        if not res_n then
            state <= DEFAULT_STATE;
            output_reg <= ZERO;
        elsif rising_edge(clk) then
            state <= next_state;
            if bypass_output_reg then
                output_reg <= next_output_reg;
            end if;
        end if;
    end process;

    lpm_mult_inst: lpm_mult
        generic map (
            LPM_WIDTHA         => DATA_WIDTH,
            LPM_WIDTHB         => DATA_WIDTH,
            LPM_WIDTHP         => 2 * DATA_WIDTH,
            LPM_REPRESENTATION => "UNSIGNED",
            LPM_PIPELINE       => 0
        )
        port map (
            clock  => clk,
            aclr   => not res_n,
            clken  => '1',
            dataa  => opA,
            datab  => opB,
            sum    => open,
            result => mul_res
        );

    lpm_div_inst: lpm_divide
        generic map (
            LPM_WIDTHN          => DATA_WIDTH,
            LPM_WIDTHD          => DATA_WIDTH,
            LPM_NREPRESENTATION => "UNSIGNED",
            LPM_DREPRESENTATION => "UNSIGNED",
            LPM_PIPELINE        => DIV_PL_DEPTH
        )
        port map (
            clock    => clk,
            aclr     => not res_n,
            clken    => '1',
            numer    => opA,
            denom    => opB,
            quotient => div_res,
            remain   => div_rem
        );

end architecture;
