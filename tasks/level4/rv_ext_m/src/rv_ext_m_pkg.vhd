library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.rv_core_pkg.all;

package rv_ext_m_pkg is

	type ext_m_op_t is (
		M_MUL,
		M_MULH,
		M_MULHU,
		M_MULHSU,
		M_DIV,
		M_DIVU,
		M_REM,
		M_REMU
	);

end package;

