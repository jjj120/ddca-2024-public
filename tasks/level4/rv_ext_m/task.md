
# RISC-V M Extension
**Points:** 2 ` | ` **Keywords:** FSM, RISC-V Extension

As you might have noticed, the ISA specified in [rv_core](../../../lib/rv_core/doc.md) does not contain instructions for integer multiplication and division.
Currently, such operations are handled by the compiler by building them out of a combination of available instructions (typically shift and addition).
Obviously, such multiplication and division operations are quite expensive in terms of clock cycles required.
However, some applications might depend on multiplications and / or divisions to be fast (e.g., for signal processing applications).
This is typically achieved by equipping a processor with dedicated multiplication /division instructions that use hardware multipliers and dividers.

In the case of the RISC-V ISA, integer multiplication, division and remainder instructions are specified in the [**M Extension**](https://five-embeddev.com/riscv-user-isa-manual/Priv-v1.12/m.html).

## Task
Your task is to implement the M extension of the RISC-V instruction set.
Before you proceed, study the specification linked above.

The M extension shall be implemented in the `rv_ext_m` module, which has the following interface:

```vhdl
entity rv_ext_m is
	generic (
		DATA_WIDTH : natural := 32
	);
	port (
		clk    : in std_logic;
		res_n  : in std_logic;
		A, B   : in std_logic_vector(DATA_WIDTH-1 downto 0);
		R      : out std_logic_vector(DATA_WIDTH-1 downto 0);
		op     : in ext_m_op_t;
		start  : in std_logic;
		busy   : out std_logic
	);
end entity;
```

As usual, `clk` and `res_n` are a clock and an active-low reset signal.
The inputs `A` and `B` are the operands on which the module will perform its operation, whereas `R` is the corresponding result.
The `op` input allows to set the desired operation.
You can find the declaration of this type in [rv_ext_m_pkg](./src/rv_ext_m_pkg.vhd).
The `start` input is set whenever an operation shall start.
Note that `A`, `B` and `op` must be valid `start` is set.
The `busy` signal is set while the module is currently executing an operation.
Once `busy` becomes low again (i.e. when an operation has completed), `R` is set to the respective operation's result.
The generic `DATA_WIDTH` allows to set the bit width of the operands and the result.

### Internal Structure
To implement the extension create an FSM that interfaces with a connected RV core and uses the provided instantiations of the `LPM_MULT` and `LPM_DIV` IP cores in [rv_ext_m.vhd](./src/rv_ext_m.vhd).
**These instantiations must not be changed**.

As you might have noticed, the two IP core instantiations are set to `"UNSIGNED"`.
Thus, they will only operate correctly if their inputs are unsigned as well.
However, the instructions defined by the M extension require the multiplication / division of signed numbers and a mix of signed and unsigned (for `MULHSU`) as well.
To achieve this, add logic for converting inputs and the result of the operation whenever required.
Furthermore, take care to implement the special cases of the division instructions as defined in the extension specification.

As we do not want to waste clock cycles, make sure that `busy` and `R` are updated as soon as possible, i.e., do not simply always set it after the amount of clock cycles it takes for the division to finish (which is the slowest operation).
For the multiplication and the special cases of the division, the results can be determined in a single clock cycle and `done` and `R` should be set accordingly.

**Hints**:
- Note that for the `MUL` instruction the signedness of the operands does not matter, as we only care about the lower 32 bit of the 64 bit result.
- For the other operations, recall that signed numbers use [two's complement](https://en.wikipedia.org/wiki/Two%27s_complement) (it is easy to invert numbers and to detect if they are negative)
- You are already provided with some utility constants in [rv_ext_m.vhd](./src/rv_ext_m.vhd).
- Make sure to add an output register for `R` after the final, optional, inversion of the IP core output.

### Testbench (Optional)
Since debugging based on the execution of C programs alone can be quite hard, you might want to create a testbench (a skeleton is already provided in [`ext_m_tb`](./tb/rv_ext_m_tb.vhd)).
The generic allows you to set the data width (e.g. 4) such that you can exhaustively check all operations for all possible input values.

**Hints**:
- Make use of the fact that you can simply use the multiplication and division operator in your testbench.
However, take care that your testbench handles the special cases for division and the varying signedness of operands / operations correctly.
- You might want to use the `rem` operator.

### Integration
Integrate your M-extension implementation in either the `rv_sim` or the `rv_fsm` core.
Run the `ext_m` test program on your processor.
This program tests the M extension instructions using a combination of directed and random test cases.
Verify that it reports the successful execution of all tests via UART.

In case you want to create your own C programs for testing, make sure to tell the compiler that it can use the M extension instructions by setting the architecture of your core as in the `ext_m` program's Makefile.
For assembly programs, you can directly use the additional instructions.

#### Pipeline Integration (Optional)
For the `rv_pl` core, intricate changes to the pipeline are necessary in order to support multi-cycle instructions.
As this is more demanding than simply integrating the M-extension into the `rv_sim` or `rv_fsm` cores, and requires a sound understanding of the pipeline, you will be awarded an additional skillpoint (i.e. 3 in total) upon successful integration into the pipeline.

The following changes are required:

- In your decode stage, detect if an instruction is an M extension instruction and propagate this information, together with the specific M instruction, to the execute stage.
To do this, add additional fields to the `d2e_t` type in the [`op_pkg`](../../level3/rv_pl/src/op_pkg.vhd).
- Your execute stage has to instantiate `rv_ext_m` and set the `start` signal when required.
Furthermore, add an additional output to the `exec` entity that provides the control with the `busy` signal of the `rv_ext_m` instance.
You also need an additional MUX to forward the correct value to the memory stage.
- In `ctrl`, stall the pipeline during the time it takes `rv_ext_m` to finish its current operation.

As written above, make sure that the `ext_m` program runs successfully on your processor.