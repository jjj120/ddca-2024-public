library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.rv_core_pkg.all;
    use work.rv_ext_m_pkg.all;

entity rv_ext_m_tb is
end entity;

architecture tb of rv_ext_m_tb is
    constant CLK_PERIOD : time    := 20 ns;
    constant DATA_WIDTH : natural := 6;
    signal clk, res_n  : std_logic  := '0';
    signal A, B, R     : std_logic_vector(DATA_WIDTH - 1 downto 0);
    signal op          : ext_m_op_t := M_MUL;
    signal start, busy : std_logic;

    constant MINUS_ONE : std_logic_vector(DATA_WIDTH - 1 downto 0) := (others => '1');
    constant MIN_VALUE : std_logic_vector(DATA_WIDTH - 1 downto 0) := '1' & (DATA_WIDTH - 2 downto 0 => '0');
    constant ZERO      : std_logic_vector(DATA_WIDTH - 1 downto 0) := (others => '0');

begin

    stim: process is
        procedure wait_not_busy is
        begin
            start <= '1';
            wait until rising_edge(clk);
            start <= '0';
            while busy loop
                wait for CLK_PERIOD;
            end loop;
        end procedure;

        pure function split_double_data_string(bin_data : std_logic_vector(DATA_WIDTH * 2 - 1 downto 0)) return string is
            variable to_return : string(1 to DATA_WIDTH * 2 + 1) := (others => ' ');
        begin
            for i in 1 to DATA_WIDTH * 2 loop
                if i <= DATA_WIDTH then
                    to_return(i) := to_string(bin_data)(i);
                else
                    to_return(i + 1) := to_string(bin_data)(i);
                end if;
            end loop;

            return to_return;
        end function;

        pure function gen_error_str(error_message : string; num1_int, num2_int, to_cmp_int : integer; to_cmp : std_logic_vector(DATA_WIDTH * 2 - 1 downto 0); res : std_logic_vector(DATA_WIDTH - 1 downto 0); calc : string) return string is
        begin
            return error_message & ": " & to_string(num1_int) & " " & calc & " " & to_string(num2_int) & " = " & to_string(to_integer(signed(res))) & ", should be " & to_string(to_cmp_int) & " (" & to_string(res) & " -- " & split_double_data_string(to_cmp) & ")";
        end function;

        procedure multiply(num1, num2 : std_logic_vector(DATA_WIDTH - 1 downto 0)) is
            variable res_to_cmp : std_logic_vector(2 * DATA_WIDTH downto 0);
        begin
            A <= num1;
            B <= num2;
            op <= M_MUL;
            wait_not_busy;
            res_to_cmp := std_logic_vector(to_signed(to_integer(signed(num1)) * to_integer(signed(num2)), DATA_WIDTH * 2 + 1));
            assert res_to_cmp(DATA_WIDTH - 1 downto 0) = R
                report gen_error_str("Multiplication MUL failed", to_integer(signed(num1)), to_integer(signed(num2)), to_integer(signed(res_to_cmp(DATA_WIDTH - 1 downto 0))), res_to_cmp, R, "*");

            op <= M_MULH;
            wait_not_busy;
            res_to_cmp := std_logic_vector(to_signed(to_integer(signed(num1)) * to_integer(signed(num2)), DATA_WIDTH * 2 + 1));
            assert res_to_cmp(DATA_WIDTH * 2 - 1 downto DATA_WIDTH) = R
                report gen_error_str("Multiplication MULH failed", to_integer(signed(num1)), to_integer(signed(num2)), to_integer(signed(res_to_cmp(DATA_WIDTH * 2 - 1 downto DATA_WIDTH))), res_to_cmp, R, "*");

            op <= M_MULHU;
            wait_not_busy;
            res_to_cmp := std_logic_vector(to_signed(to_integer(unsigned(num1)) * to_integer(unsigned(num2)), DATA_WIDTH * 2 + 1));
            assert res_to_cmp(DATA_WIDTH * 2 - 1 downto DATA_WIDTH) = R
                report gen_error_str("Multiplication MULHU failed", to_integer(unsigned(num1)), to_integer(unsigned(num2)), to_integer(unsigned(res_to_cmp(DATA_WIDTH * 2 - 1 downto DATA_WIDTH))), res_to_cmp, R, "*");

            op <= M_MULHSU;
            wait_not_busy;
            res_to_cmp := std_logic_vector(to_signed(to_integer(signed(num1)) * to_integer(unsigned(num2)), DATA_WIDTH * 2 + 1));
            assert res_to_cmp(DATA_WIDTH * 2 - 1 downto DATA_WIDTH) = R
                report gen_error_str("Multiplication MULHSU failed", to_integer(signed(num1)), to_integer(unsigned(num2)), to_integer(signed(res_to_cmp(DATA_WIDTH * 2 - 1 downto DATA_WIDTH))), res_to_cmp, R, "*");
            A <= (others => '0');
            B <= (others => '0');
            op <= M_MUL;
        end procedure;

        procedure divide(num1, num2 : std_logic_vector(DATA_WIDTH - 1 downto 0)) is
        begin
            if num2 = 6x"0" then
                -- break if div by 0 happens
                return;
            end if;

            A <= num1;
            B <= num2;
            op <= M_DIV;
            wait_not_busy;
            assert std_logic_vector(signed(num1) / signed(num2)) = R
                report "Division DIV failed: " & to_string(to_integer(signed(num1))) & " / " & to_string(to_integer(signed(num2))) & " = " & to_string(to_integer(signed(R))) & ", should be " & to_string(to_integer(signed(num1) / signed(num2))) & " (" & to_string(signed(num1) / signed(num2)) & " -- " & to_string(R) & ")";

            op <= M_DIVU;
            wait_not_busy;
            assert std_logic_vector(unsigned(num1) / unsigned(num2)) = R
                report "Division DIVU failed: " & to_string(to_integer(unsigned(num1))) & " / " & to_string(to_integer(unsigned(num2))) & " = " & to_string(to_integer(signed(R))) & ", should be " & to_string(to_integer(unsigned(num1) / unsigned(num2))) & " (" & to_string(unsigned(num1) / unsigned(num2)) & " -- " & to_string(R) & ")";

            op <= M_REM;
            wait_not_busy;
            assert std_logic_vector(signed(num1) rem signed(num2)) = R
                report "Remainder REM failed: " & to_string(to_integer(signed(num1))) & " rem " & to_string(to_integer(signed(num2))) & " = " & to_string(to_integer(signed(R))) & ", should be " & to_string(to_integer(signed(num1) rem signed(num2))) & " (" & to_string(signed(num1) rem signed(num2)) & " -- " & to_string(R) & ")";

            op <= M_REMU;
            wait_not_busy;
            assert std_logic_vector(unsigned(num1) rem unsigned(num2)) = R
                report "Remainder REMU failed: " & to_string(to_integer(unsigned(num1))) & " rem " & to_string(to_integer(unsigned(num2))) & " = " & to_string(to_integer(signed(R))) & ", should be " & to_string(to_integer(unsigned(num1) rem unsigned(num2))) & " (" & to_string(signed(num1) rem signed(num2)) & " -- " & to_string(R) & ")";

            A <= (others => '0');
            B <= (others => '0');
            op <= M_MUL;
        end procedure;
    begin
        res_n <= '0';
        op <= M_MUL;
        start <= '0';
        A <= (others => '0');
        B <= (others => '0');
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        res_n <= '1';
        wait until rising_edge(clk);

        for number1 in - 2 ** (DATA_WIDTH - 1) + 1 to 2 ** (DATA_WIDTH - 1) - 1 loop
            for number2 in - 2 ** (DATA_WIDTH - 1) + 1 to 2 ** (DATA_WIDTH - 1) - 1 loop
                multiply(std_logic_vector(to_signed(number1, DATA_WIDTH)), std_logic_vector(to_signed(number2, DATA_WIDTH)));
                divide(std_logic_vector(to_signed(number1, DATA_WIDTH)), std_logic_vector(to_signed(number2, DATA_WIDTH)));
            end loop;
        end loop;

        -- test div by 0
        for num1 in - 2 ** (DATA_WIDTH - 1) + 1 to 2 ** (DATA_WIDTH - 1) - 1 loop
            A <= std_logic_vector(to_signed(num1, DATA_WIDTH));
            B <= 6x"0";
            op <= M_DIV;
            wait_not_busy;
            assert MINUS_ONE = R
                report "Division DIV by 0 failed: " & to_string(to_integer(signed(A))) & " / " & to_string(to_integer(signed(B))) & " = " & to_string(to_integer(signed(R))) & ", should be " & to_string(to_integer(signed(MINUS_ONE)));

            op <= M_DIVU;
            wait_not_busy;
            assert MINUS_ONE = R
                report "Division DIVU by 0 failed: " & to_string(to_integer(unsigned(A))) & " / " & to_string(to_integer(unsigned(B))) & " = " & to_string(to_integer(unsigned(R))) & ", should be " & to_string(to_integer(unsigned(MINUS_ONE)));

        end loop;

        report "Testbench done";
        std.env.stop;
        wait;
    end process;

    generate_clk: process
    begin
        while True loop
            clk <= '0', '1' after CLK_PERIOD / 2;
            wait for CLK_PERIOD;
        end loop;
        wait;
    end process;

    uut: entity work.rv_ext_m
        generic map (
            DATA_WIDTH => DATA_WIDTH
        )
        port map (
            clk   => clk,
            res_n => res_n,
            A     => A,
            B     => B,
            R     => R,
            op    => op,
            start => start,
            busy  => busy
        );

end architecture;
