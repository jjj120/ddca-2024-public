onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /rv_ext_m_tb/clk
add wave -noupdate /rv_ext_m_tb/res_n
add wave -noupdate -radix binary /rv_ext_m_tb/A
add wave -noupdate -radix binary /rv_ext_m_tb/B
add wave -noupdate -radix binary /rv_ext_m_tb/R
add wave -noupdate /rv_ext_m_tb/op
add wave -noupdate /rv_ext_m_tb/start
add wave -noupdate /rv_ext_m_tb/busy
add wave -noupdate /rv_ext_m_tb/uut/state
add wave -noupdate /rv_ext_m_tb/uut/output_reg
add wave -noupdate /rv_ext_m_tb/uut/next_output_reg
add wave -noupdate /rv_ext_m_tb/uut/bypass_output_reg
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {408544 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {408197 ns} {409053 ns}
