
# RISC-V Pipeline Hazard Handling
**Points:** 1 ` | ` **Keywords:** RISC-V, Hazard Handling

In this task the data and control hazards in the RISC-V pipeline, developed in the [`rv_pl`](../../level3/rv_pl/task.md) task, are resolved.
Hence, the `rv_pl` task is a strict prerequisite for this task.
With the pipeline modifications of this task being implemented, the performance of your processor will greatly increase, facilitating a throughput of up to one instruction per clock cycle.


## Task

The modifications required to handle hazards in your `rv_pl` will affect the execute and memory stages as well as the control unit.

The first step of this task is to create a new control unit that does not insert three `nop` instructions after each actual instruction.
Do this by adding a new architecture, called `hazard_handling`, to the control unit (`rv_pl_ctrl`) of the pipeline.

Your control unit shall set the `flush` signals of all stages to `'0'` and stall all stages when `imem_busy` or `dmem_busy` are asserted (by asserting the `stall` flags).
Of course these changes completely break the pipeline, as it is subject to hazards now.
While you will deal with those next, you can still execute (assembly) programs on it, if you manually insert three `nop` instructions after each instruction.
This can be very helpful for testing forwarding and branch hazard handling separately.
Note that, strictly speaking you only need three `nop` instructions if there is a dependency between two successive instructions.
However, it does not hurt to always insert them anyway.

**Important**: Don't change the provided `simple` architecture of the control unit (`rv_pl_ctrl`)!
Your processor must, at any time, still work with this architecture version.

### Forwarding

Consider the assembly program below and note that the `and` instruction uses the results of the two preceding `addi` instructions.


```assembly
addi x1, x0, 7
addi x2, x0, 5
and  x1, x2, x1
nop
nop
```

Assume that we feed these instructions directly into our pipeline with the modified control unit.
We now have the situation that the results of the `andi` instructions are not available in the register file when the `and` instruction reaches the execute stage.
The reason being, that the value of register `x1` is still in the writeback stage, while the value of register `x2` is in the memory stage.
For a correct operation, these values must be forwarded to the execute stage.
While forwarding increases the complexity of a pipeline, it is usually more efficient to resolve this hazard in hardware than having the compiler reorder code and insert `nop` instructions where necessary.


To fix this issue, implement an appropriate forwarding logic in the execute stage.
The required logic consists of two main parts, which have to be replicated for both of the possible source registers of an instruction (i.e., `rs1` and `rs2`).

 * **Control logic**: Compare the information currently stored in the execute stage's pipeline register, about which register has been read in the decode stage, with the information provided by the inputs `m2e` (from the memory stage) and `w2e` (from the writeback stage) to decide whether forwarding a value is necessary.
 Be sure you write the correct data to `m2e` in the memory stage!
 * **Multiplexers**: Add additional multiplexers in front of the ALU. In case you determine that there is an updated value for a particular source register available in the memory or writeback stage, use these multiplexes to relay (i.e., *foward*) this value to the ALU inputs.
 This will result in the ALU using this value instead of the one read from the register file (stored in the execute stage's pipeline register).

**Important**: Be careful not to erroneously forward values for register `x0`!

Note that forwarding a result of a memory load to an instruction executed immediately after the load instruction is not possible (we only forward `alu_result` in the memory stage).
Therefore, in such situations the pipeline has to be stalled until forwarding the correct data is possible.
Note that not every load instruction causes a hazard and that the pipeline should only be stalled if it is necessary (i.e., the loaded value is actually used when executing the next instruction).
Add this functionality to your `hazard_handling` control unit.


### Branch Hazards

When taking a branch in the memory stage, the fetch, decode and execute stages already hold instructions that follow the branch.
This means that the instructions in these stages need to be flushed in case of a branch being taken.
Hence, add this functionality to your control unit.
When implemented correctly, the assembly code in the listing below does not increment registers `x1`, `x2` or `x3`.

```assembly
loop:	j loop
		addi x1, x1, 1
		addi x2, x2, 1
		addi x3, x3, 1
```

## Testbench and Hardware Test
Simulation and hardware testing work exactly as described in the [`rv_fsm`](../rv_fsm/task.md) task.


