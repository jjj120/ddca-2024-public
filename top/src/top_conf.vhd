
use work.all;

configuration top_conf of dbg_top is
	for dbg_top_arch
		for top_inst : top
			use entity top(top_arch_rv_pl); -- Change the top architecture here!
		end for;
	end for;
end configuration;
